/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   4/23/2018
 * @description Testing various pieces of code for new functionalities. Displaying various temporary results. Small volume mass updates.
 * 
 * Script Name  : CLGX2_RL_Capacity_Tree
 * Script File  : CLGX2_RL_Capacity_Tree.js
 * Script ID    : customscript_clgx2_rl_capacity_tree
 * Deployment ID: customdeploy_clgx2_rl_capacity_tree
 * Internal URL	: /app/site/hosting/restlet.nl?script=1515&deploy=1
 */


define(['N', 'N/auth', 'N/config', 'N/crypto', 'N/currency', 'N/email', 'N/encode', 'N/error', 'N/file', 'N/format', 'N/http', 'N/https', 'N/plugin', 'N/record', 'N/redirect', 'N/render', 'N/runtime', 'N/search', 'N/sso', 'N/task', 'N/transaction', 'N/ui', 'N/ui/dialog', 'N/ui/message', 'N/ui/serverWidget', 'N/url', 'N/workflow', 'N/xml',
	"/SuiteScripts/clgx/libraries/lodash.min", 
	"/SuiteScripts/clgx/libraries/moment.min"
	],

function(N, auth, config, crypto, currency, email, encode, error, file, format, http, https, plugin, record, redirect, render, runtime, search, sso, task, transaction, ui, dialog, message, serverWidget, url, workflow, xml, _, moment) {
   
    function doGet(request) {

    	var scriptObj = runtime.getCurrentScript();
	    
    		var json = request.json || 'no';
		var tree = request.tree  || 21;
		
		// json files ids from /clgx/capacity/json/ folder
		var arr = [0,10696302,10696303,10696304,10696305,10696306,10696307,10696308,10696309,10696310,10696411,10696412,10696301,10696413,10696414,10696415,10696416,10696417,10696418,10696419,10694689,10696420,10696421,10696422,10696423,10696424,10696425,10696426,10696427,10696428,10696429,10696430,10696431];
    	
    		var user = runtime.getCurrentUser();
    		var userid = user.id;
    		var role = user.role;
    	
    		//if (role == 244332431){
    		if (role == -5 || role == 3 || role == 18 || role == 1052){
    			
	    	var powers = get_powers (tree);
			var pair_panel_ids = _.compact(_.uniqBy(_.map(powers, 'pair_panel_id')));
			powers = set_pair_chains (powers, pair_panel_ids);
			
			var all_chains = _.uniqBy(_.map(powers, 'power_chain'));
			var all_chains_ids = _.uniqBy(_.map(powers, 'power_chain_id'));
		    for ( var i = 0; i < all_chains.length; i++ ) {
				powers = add_fail_kw (powers, all_chains[i]);
			}
			
		    var devices = get_devices (tree, powers, all_chains, all_chains_ids);
		    
			var devices_no_powers = _.filter(devices, function(arr){
				return (arr.powers.length == 0);
			});
			var ids = _.map(devices_no_powers, 'device_id');
			
			devices = set_devices_no_powers (devices, powers, ids, all_chains, all_chains_ids);
			
			usage = 5000 - parseInt(scriptObj.getRemainingUsage());
			var capacity = {
					"usage"     : usage,
					"created"   : moment().format('MM/DD/YYYY, HH:mm:ss'),
					"devices"   : devices
			}

			// create current processed tree json
			var fileObj = file.create({
				name: tree + '.json',
			    fileType: file.Type.PLAINTEXT,
			    contents: JSON.stringify(capacity),
			    encoding: file.Encoding.UTF8,
			    folder: 7788385,
			    isOnline: false
			});
			var fileId = fileObj.save();
		
    		} else {
    			
    			var fileObj = file.load({id: arr[tree]});
			var capacity = JSON.parse(fileObj.getContents());
    		}
    		
    		var org_chart = get_org_chart (capacity.devices);
    		
    		if(json == 'yes'){
    			return JSON.stringify(capacity);
    			
    		} else {
    			var org_chart = get_org_chart (capacity.devices);
    			var fileObj = file.load({id: 10707382});
    			var html = fileObj.getContents();
    			html = html.replace(new RegExp('{dataChart}','g'), org_chart);
    			return html;
    		}
    		
    }

    function get_org_chart (devices){
    	
	    	// construct the orgChart
	    	var arrData = [];
	    	var arrNode = [];
	        arrNode[0] = 'Child';
	        arrNode[1] = 'Parent';
	        arrData.push(arrNode);
	
	    	for ( var i = 0; devices != null && i < devices.length; i++ ) {
	    		var arrNode = [];
	    	    arrNode[0] = get_node_html (devices[i]);
	    	    var parent = _.find(devices, function(arr){ return (arr.device_id == devices[i].parent_id); });
	    	    if(parent != null){
	    	    	arrNode[1] = get_node_html (parent);
	    	    }
	    	    else{
	    	    	arrNode[1] = '';
	    	    }
	    	    arrData.push(arrNode);
	    	}
	    	return JSON.stringify(arrData);
    }


	function get_powers (tree) {
		var powers = [];
		var results = search.load({ type: "customrecord_clgx_power_circuit", id: "customsearch_clgx_dw_capacity_powers" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_dcim_device_pwr_tree", join: "custrecord_clgx_dcim_device", operator: "ANYOF", values: tree }));
		results.run().each(function(result) {
			var power_kw = parseFloat(result.getValue(result.columns[5])) || 0;
			var pair_kw = parseFloat(result.getValue(result.columns[9])) || 0;
			powers.push({
				"power_panel_id"     : parseInt(result.getValue(result.columns[0])),
				//"power_panel"        : result.getText(result.columns[0]),
				"power_chain_id"     : result.getValue(result.columns[1]),
				"power_chain"        : result.getText(result.columns[1]),
				"power_id"           : parseInt(result.getValue(result.columns[2])),
				"power"              : result.getValue(result.columns[3]),
				"power_kva"          : round(parseFloat(result.getValue(result.columns[4]))) || 0,
				"power_kw"           : round(power_kw),
				"pair_panel_id"      : parseInt(result.getValue(result.columns[6])),
				//"pair_panel"         : result.getText(result.columns[6]),
				"pair_chain"         : "",
				"pair_id"            : parseInt(result.getValue(result.columns[7])),
				//"pair"               : result.getText(result.columns[7]),
				"pair_kva"           : round(parseFloat(result.getValue(result.columns[8]))) || 0,
				"pair_kw"            : round(pair_kw),
				"all_kw"             : round(power_kw + pair_kw),
				"failed_arr"        : []
			});
            return true;
		});
		return powers;
	}
	
	
	function set_pair_chains (powers, pair_panel_ids) {
		
		var pair_panels = [];
		if(pair_panel_ids && pair_panel_ids.length > 0){
			var results = search.load({ type: "customrecord_clgx_dcim_devices", id: "customsearch_clgx_dw_capacity_chains" });
			results.filters = [search.createFilter({ name: "internalid", operator: "ANYOF", values: pair_panel_ids })];
			results.run().each(function(result) {
				pair_panels.push({
					"pair_panel_id"     : parseInt(result.getValue(result.columns[0])),
					"pair_chain"        : result.getText(result.columns[1])
				});
		        return true;
			});
			
			for ( var i = 0; i < powers.length; i++ ) {
				var obj = _.find(pair_panels, function(arr){ return (arr.pair_panel_id == powers[i].pair_panel_id) ; });
				if(obj){
					powers[i].pair_chain = obj.pair_chain
				}
			}
		}
		return powers;
	}
	
	function add_fail_kw (powers, fail_chain) {
		for ( var i = 0; i < powers.length; i++ ) {
			if(powers[i].pair_chain == fail_chain){
				powers[i].failed_arr.push({
					"chain" : fail_chain,
					"kw"    : powers[i].power_kw + powers[i].pair_kw
				});
				//powers[i].failed_arr.push(powers[i].power_kw + powers[i].pair_kw);
			} else {
				powers[i].failed_arr.push({
					"chain" : fail_chain,
					"kw"    : round(powers[i].power_kw)
				});
				//powers[i].failed_arr.push(powers[i].power_kw);
			}
		}
		return powers;
	}
	
    
    
	function get_devices(tree, powers, all_chains, all_chains_ids) {
    	
		var devices = [];
		var results = search.load({ type: "customrecord_clgx_dcim_devices", id: "customsearch_clgx_dw_capacity_devices" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_dcim_device_pwr_tree", operator: "ANYOF", values: tree }));
		results.run().each(function(result) {
			
			var device_id    = parseInt(result.getValue(result.columns[0]));
			var category_id  = parseInt(result.getValue(result.columns[6])) || 0;
			var load        = parseFloat(result.getValue(result.columns[14])) || 0;
			var loadow      = parseFloat(result.getValue(result.columns[7]))  || 0;
			var currentow   = parseFloat(result.getValue(result.columns[8]))  || 0;
			var capacity    = parseFloat(result.getValue(result.columns[9])) || 0;
			var failow      = parseFloat(result.getValue(result.columns[10])) || 0;
			var kwsab       = parseFloat(result.getValue(result.columns[11]))  || 0;
			
			var chains      = result.getText(result.columns[12]);
			
			var available = 0;
			/*
			if(category_id == 1 || category_id == 3){
				var available = capacity - kwsab;
			} 
			else if (category_id > 0 && category_id != 1 && category_id != 3) {
				var available = capacity - loadow;
			} 
			else {
			}
			*/
			
			var device = {
				//"index"         : i,
				"device_id"     : device_id,
				"device"        : result.getValue(result.columns[1]),
				"is_root"        : result.getValue(result.columns[13]),
				"parent_id"     : parseInt(result.getValue(result.columns[2])),
				//"parent"        : result.getText(result.columns[2]),
				"chains_str"    : chains,
				"chains"        : chains.split(","),
				"pair_id"       : parseInt(result.getValue(result.columns[3])),
				//"pair"          : result.getText(result.columns[3]),
				//"fam_id"        : parseInt(result.getValue(result.columns[4])),
				"virtual"       : result.getValue(result.columns[5]),
				"category_id"    : category_id,
				"category"      : result.getText(result.columns[6]) || '',
				
				"load"          : round(load),
				"loadow"        : round(loadow),
				"currentow"     : round(currentow),
				
				"failow"        : round(failow),
				"kwsab"         : round(kwsab),
				"available"     : round(available),
				"capacity"      : round(capacity),
				
				"sold"          : 0,
				"sold_ab"       : 0,

				"used_a"        : 0,
				"used_ab"       : 0,
				"failed_max"    : 0,
				"failed_arr"    : [],
				"fam_powers"    : count_fam_powers(device_id, category_id),
				"nbr_powers"    : 0,
				"powers"        : []
			};
			
			var obj = _.find(powers, function(arr){ return (arr.power_panel_id == device_id) ; });
			if(obj){
				
				var panel_fail_kw_arr = [];
				for ( var j = 0; j < all_chains_ids.length; j++ ) {
					panel_fail_kw_arr.push({
						"chain_id"  : parseInt(all_chains_ids[j]),
						"chain"     : all_chains[j],
						"kw"        : 0
					});
				}
				
				var panel_powers = _.filter(powers, function(arr){
					return (arr.power_panel_id == device_id);
				});
				if(panel_powers && panel_powers.length > 0){
					
					
					var used_kw = 0;
					var used_ab = 0;
					var kva = 0;
					var kva_ab = 0;
					for ( var j = 0; j < panel_powers.length; j++ ) {
						
						used_kw += panel_powers[j].power_kw;
						used_ab += panel_powers[j].power_kw;
						if(panel_powers[j].pair_kw){
							used_ab += panel_powers[j].pair_kw;
						}
						kva += panel_powers[j].power_kva;
						//kva_ab += panel_powers[j].power_kva;
						//if(panel_powers[j].pair_kva){
						//	kva_ab += panel_powers[j].pair_kva;
						//}
						
						for ( var k = 0; k < all_chains.length; k++ ) {
							panel_fail_kw_arr[k].kw  += panel_powers[j].failed_arr[k].kw;
							//panel_fail_kw_arr[k] += panel_powers[j].failed_arr[k];
						}
						
					}
					for ( var k = 0; k < panel_fail_kw_arr.length; k++ ) {
						panel_fail_kw_arr[k].kw = round(panel_fail_kw_arr[k].kw)
					}
					
					device.used_a      = round(used_kw);
					device.used_ab     = round(used_ab);
					device.sold        = round(kva);
					device.sold_ab     = round(kva);
					device.failed_max  = round(_.max(_.map(panel_fail_kw_arr, 'kw')));
					device.failed_arr  = panel_fail_kw_arr;
					device.nbr_powers  = panel_powers.length;
					device.powers      = panel_powers;
				} 
			}
			devices.push(device);
			return true;
		});
		return devices;
    }
    
    
	function set_devices_no_powers (devices, powers, ids, all_chains, all_chains_ids) {
    	
	    	for ( var i = 0; i < devices.length; i++ ) {
	    		
	    		if (_.indexOf(ids, devices[i].device_id) > -1) {
	    			
					var failed_arr = [];
					for ( var j = 0; j < all_chains_ids.length; j++ ) {
						failed_arr.push({
							"chain_id"  : parseInt(all_chains_ids[j]),
							"chain"     : all_chains[j],
							"kw"        : 0
						});
					}
					
	    			var used_a = 0;
	    			var used_ab = 0;
	    			var sold = 0;
	    			var sold_ab = 0;
	    			var nbr_powers = 0;
		    		for ( var j = 0; j < devices[i].chains.length; j++ ) {
		    			
					var chain_powers = _.filter(powers, function(arr){
						return (arr.power_chain == devices[i].chains[j]);
					});
					used_a += _.sumBy(chain_powers, 'power_kw');
					used_ab += _.sumBy(chain_powers, 'power_kw');
					used_ab += _.sumBy(chain_powers, 'pair_kw');
					
					for ( var k = 0; k < chain_powers.length; k++ ) {
						
						// add used kw to the corresponding single failure scenario array element
						for ( var l = 0; l < all_chains.length; l++ ) {
							failed_arr[l].kw  += chain_powers[k].failed_arr[l].kw;
						}
						
						// calculate sold - if both power and pair chains are among this device chains add just 1/2 of power_kva, if not, add all power_kva
						if ((_.indexOf(devices[i].chains, chain_powers[k].power_chain) > -1) && (_.indexOf(devices[i].chains, chain_powers[k].pair_chain) > -1)){
							sold += chain_powers[k].power_kva / 2;
							sold_ab += chain_powers[k].power_kva;
						} else {
							sold += chain_powers[k].power_kva;
							sold_ab += chain_powers[k].power_kva;
						}
					}
					nbr_powers += chain_powers.length;
					for ( var k = 0; k < failed_arr.length; k++ ) {
						failed_arr[k].kw = round(failed_arr[k].kw)
					}
		    		}
	    			
		    		devices[i].used_a       = round(used_a);
		    		devices[i].used_ab      = round(used_ab);
		    		devices[i].sold         = round(sold);
		    		devices[i].sold_ab      = round(sold_ab);
		    		
		    		devices[i].failed_max   = round(_.max(_.map(failed_arr, 'kw')));
		    		devices[i].failed_arr   = failed_arr;
		    		devices[i].nbr_powers   = nbr_powers;
	    		}
	    		

	    	}
    	
    		return devices;
    }
    
    
	function count_fam_powers(device_id, category_id){
		
		var field = null;
		if(category_id == 3){field = "custrecord_clgx_power_panel_pdpm";} // Panel
		if(category_id == 1){field = "custrecord_cologix_power_ups_rect";} // UPS
		if(category_id == 8 || category_id == 12){field = "custrecord_clgx_power_generator";} // Generator

		var fam_id = 0;
		if(field) {
			var columns    = search.lookupFields({type: 'customrecord_clgx_dcim_devices', id: device_id, columns: ["custrecord_clgx_dcim_device_fam"]});
			var fam_id     = null;
			if(columns && columns["custrecord_clgx_dcim_device_fam"] && columns["custrecord_clgx_dcim_device_fam"][0]){
				fam_id     = columns["custrecord_clgx_dcim_device_fam"][0].value;
			}
		}
		var count = 0;
		if(field && fam_id) {
			var results = search.load({ type: "customrecord_clgx_power_circuit", id: "customsearch_clgx_dw_capacity_powers" });
			results.filters.push(search.createFilter({ name: field, operator: "ANYOF", values: fam_id }));
			results.run().each(function(result) {
				count += 1;
	            return true;
			});
		}
		return count;
	}


	function get_node_html(objDevice){
	    
		var devicecss = 'light_gray';
		
		if(objDevice.virtual == 'T'){
			devicecss = ' class="white"';
		}
		
		else if (objDevice.category_id == 8 || objDevice.category_id == 12){
			devicecss = ' class="dark_gray"';
		}
		else if (objDevice.category_id == 1 || objDevice.category_id == 2){
			devicecss = ' class="dark_green"';
		}
		else if (objDevice.category_id == 3 || objDevice.category_id == 11){
			devicecss = ' class="light_green"';
		}
		else if (objDevice.category_id == 5){
			devicecss = ' class="dark_blue"';
		}
		else if (objDevice.category_id == 4 || objDevice.category_id == 6 || objDevice.category_id == 7){
			devicecss = ' class="light_blue"';
		}
		else if (objDevice.category_id == 0){
			devicecss = ' class="light_gray"';
		}
		else {
			devicecss = 'light_gray';
		}
		
		var available_used = round(objDevice.capacity - objDevice.failed_max);
		var available_sold = round(objDevice.capacity - objDevice.sold);
		
		var html = '';
		html += '<table align="center" width="100%"' + devicecss + '><tr class="bb2 ac hd0">';
		html += '<td colspan="3" nowrap><a href="/app/common/custom/custrecordentry.nl?rectype=218&id=' + objDevice.device_id + '" " target="_blank">' + objDevice.device + '</a></td>';
		
		html += '</tr><tr class="bb">';
		//html += '<td nowrap class="al">Category</td>';
		html += '<td class="ar" colspan="3" nowrap>' + objDevice.category + '</td>';

		html += '</tr><tr class="bb hd3">';
		html += '<td nowrap class="al" colspan="2" align="left">Power Chains</td>';
		html += '<td class="ar" align="right">' + objDevice.chains_str + '</td>';
		html += '</tr>';

		html += '</tr><tr class="bb">';
		html += '<td nowrap class="al" colspan="2" align="left">Derated Capacity</td>';
		html += '<td class="ar" align="right">' + objDevice.capacity + '</td>';
			
		//if(objDevice.load  > 0){
			html += '</tr><tr class="bb">';
			html += '<td nowrap class="al" colspan="2" align="left">Actual Load (Modius)</td>';
			html += '<td class="ar" align="right">' + objDevice.load + '</td>';
		//}

		//html += '</tr><tr class="bb hd1">';
		//html += '<td nowrap class="al" colspan="2">Load w/ Failover</td>';
		//html += '<td class="ar">' + objDevice.failow + '</td>';

		if(available_sold > 0){
			html += '</tr><tr class="bb hd2">';
		}
		else{
			html += '</tr><tr class="bb hd3">';
		}
		html += '<td nowrap class="al" colspan="2" align="left">Available to Sell (Derated - MSF Sold)</td>';
		html += '<td class="ar" align="right">' + available_sold + '</td>';
		html += '</tr>';

			
		if(available_used > 0){
			html += '</tr><tr class="bb hd2">';
		}
		else{
			html += '</tr><tr class="bb hd3">';
		}
		html += '<td nowrap class="al" colspan="2" align="left">Available to Use (Derated - MSF Used)</td>';
		html += '<td class="ar" align="right">' + available_used + '</td>';
		html += '</tr>';

		//if(objDevice.count_pwrs){
			html += '<tr class="bb hd3"></tr>';
			html += '<tr class="bb hd0">';
			//html += '<td nowrap class="al" align="left">Powers (Chains - ' + objDevice.nbr_powers + ' | FAM - ' + objDevice.fam_powers + ')</td>';
			html += '<td nowrap class="al" align="left">Powers</td>';
			html += '<td nowrap class="al" align="right">Used</td>';
			html += '<td nowrap class="al" align="right">Sold</td>';
			html += '</tr>';
			/*
			html += '<tr class="bb hd3"></tr>';
			html += '<tr class="bb hd3">';
			html += '<td nowrap class="al" align="left">Total Failure (All other chains)</td>';
			html += '<td nowrap class="al" align="right">' + objDevice.used_ab + '</td>';
			html += '<td nowrap class="al" align="right">' + objDevice.sold_ab + '</td>';
			html += '</tr>';
			*/
			html += '<tr class="bb hd3"></tr>';
			html += '<tr class="bb hd3">';
			html += '<td nowrap class="al" align="left">Max Single Failure (MSF)</td>';
			html += '<td nowrap class="al" align="right">' + objDevice.failed_max + '</td>';
			html += '<td nowrap class="al" align="right">NA</td>';
			html += '</tr>';
			
			html += '<tr class="bb hd3"></tr>';
			html += '<tr class="bb hd1">';
			
			if(objDevice.category_id == 3){
				html += '<td nowrap class="al" align="left">Sum Up (Panel powers)</td>';
			}
			else{
				html += '<td nowrap class="al" align="left">Sum Up (All <span class="hd3">' + objDevice.chains_str + '</span> powers)</td>';
			}
			
			html += '<td nowrap class="al" align="right">' + objDevice.used_a + '</td>';
			html += '<td nowrap class="al" align="right">' + objDevice.sold + '</td>';
			html += '</tr>';
			for ( var i = 0; objDevice.powers != null && i < objDevice.powers.length; i++ ) {
				html += '<tr>';
				html += '<td nowrap class="al" align="left"><span class="hd3">' + objDevice.powers[i].power_chain + '</span> - <a href="/app/common/custom/custrecordentry.nl?rectype=17&id=' + objDevice.powers[i].power_id + '" " target="_blank">' + objDevice.powers[i].power + '</a></td>';
				html += '<td nowrap class="al" align="right">' + objDevice.powers[i].power_kw + '</td>';
				html += '<td nowrap class="al" align="right">' + objDevice.powers[i].power_kva + '</td>';
				html += '</tr>';
			}
		//}
		
		html += '</table>';

		return html;
	}

	
	function round(value) {
		  //return Number(Math.round(value+'e'+2)+'e-'+2);
		  return parseFloat((parseFloat(value)).toFixed(4))
	}

	
    function doPut(requestBody) {}
    function doPost(requestBody) {}
    function doDelete(requestParams) {}
    return {
        'get': doGet,
        'put': doPut,
        'post': doPost,
        'delete': doDelete
    };
    
});
