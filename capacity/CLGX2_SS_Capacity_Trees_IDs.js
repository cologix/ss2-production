/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_Capacity_Trees_IDs.js
 * Script Name	:	CLGX2_SS_Capacity_Trees_IDs
 * Script ID	:   customscript_clgx2_ss_capacity_trees_ids
 * Deployment ID:   customdeploy_clgx2_ss_capacity_trees_ids
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min"
		
	],
function(file, task, https, record, runtime, search, _, moment) {
	
	function execute(context) {
		
		log.debug({ title: "Start", details: "====================================================" });
		
		var response = context.response;
		var request = context.request;
		
		var ids = [];
		var results = search.create({
			type: 'customrecord_clgx_power_capacity_tree',
			columns: [{"name":"internalid"}]
		});
		results.run().each(function(result) {
			ids.push(parseInt(result.getValue({ name: "internalid"})));
            return true;
		});
		
		var fileObj = file.create({
			name: 'ids.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(ids),
		    encoding: file.Encoding.UTF8,
		    folder: 7788385,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
		var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
		scriptTask.scriptId = 1517;
		scriptTask.deploymentId = 'customdeploy_clgx2_ss_capacity_trees';
		var scriptTaskId = scriptTask.submit();
		
		log.debug({ title: "Finish", details: "====================================================" });
    }
	
    return {
        execute: execute
    };
    

});


