/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/error", "N/search"],
function(error, search) {
    function beforeSubmit(scriptContext) {
    	try {
    		var nr = scriptContext.newRecord;
        	log.debug({ title: "", details: nr });
    	} catch(ex) {
    		log.error({ title: "Error", details: ex });
    	}
    }

    /**
	 * Checks if the record being saved is currently sitting unprocessed in the queue.
	 * 
	 * @access private
	 * @function recordExistsInQueue
	 * 
	 * @libraries N/search, N/error
	 * 
	 * @param {number} recordId   - The Internal ID of the current record.
	 * @param {string} recordType - The record type of the current record.
	 * @param {string} eventType - The event type of the current record.
	 * @return void
	 */
	function recordExistsInQueue(recordId, recordType, eventType) {
		var s = search.create({ 
			type: "customrecord_clgx_dw_record_queue",
			columns: ["internalid"],
			filters: [["custrecord_clgx_dw_rq_hist", "isnot", "T"], "and",
			["custrecord_clgx_dw_rq_rt", "contains", recordType], "and",
			["custrecord_clgx_dw_rq_et", "contains", eventType], "and",
			["custrecord_clgx_dw_rq_rid", "equalto", recordId]]
		});
		var count = 0;
		s.run().each(function(result) { count = count + 1; });
		if(count > 0) {
			log.debug({ title: "recordExistsInQueue", details: "Skipping " + recordType + " (" + recordId + ")" + ". Record exists and has not been processed." });
			throw error.create({ name: "CLGX_ERROR", message: "Record exists in the queue.", notifyOff: true });
		}
	}
    
    return {
        beforeSubmit: beforeSubmit
    };
    
});
