/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Katy Jones katy.jones@cologix.com 
 * @date 7/31/2019
 */
define(['N/ui/serverWidget', 'N/record'],
/**
 * @param {record} record
 * @param {serverWidget} serverWidget
 */
function(serverWidget, record) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(context) {
    	
    	//TEST
    	
    	var solution = context.newRecord;
    	var fieldName = 'longdescription';
    	//gets the text value from the native NetSuite description field
    	var longDescriptionValue = solution.getValue('longdescription');
    	var updatedLongDescription = longDescriptionValue;
    	
    	//Moves images that are between </li> and <li> tags into the </li> tag to prevent corrupted formatting of the list values
    	var findImage = updatedLongDescription.match(/<\/li>[\s]*?<br>[\s]*?<img(.*)=\">/g);
    	
    	if(findImage != null){
	    	if(findImage.length>=1){
	    		for(i=0; i<findImage.length; i++){
	    			
	    			var imageURL = findImage[i].match(/<img(.*)=\">/g);
	    	    	    	    	
	    			var closeListTag = '</li>';
	    	    	var movedImage = imageURL + closeListTag;
	    	    	
	    	    	updatedLongDescription = updatedLongDescription.replace(findImage[i],movedImage);
	    		}
	    	}
    	}
    	
    	//Moves <div> tags and their contents that are between </li> and <li> tags into the </li> tag to prevent corrupted formatting of note <div> tags
    	var findOutsideText = updatedLongDescription.match(/<\/li>[\s]*?<br>[\s]*?<div(.*)<\/div>/g);
    	
    	if(findOutsideText != null){
	    	if(findOutsideText.length>=1){
	    		for(i=0; i<findOutsideText.length; i++){
	    			
	    			var divContent = findOutsideText[i].match(/<div(.*)<\/div>/g);
	    	    	    	    	
	    			var closeListTag = '</li>';
	    	    	var movedDiv = divContent + closeListTag;
	    	    	
	    	    	updatedLongDescription = updatedLongDescription.replace(findOutsideText[i],movedDiv);
	    		}
	    	}
    	}

    	
    	//replaces values that are often corrupted in the TinyMCE Editor with a consumable version
    	updatedLongDescription = updatedLongDescription.replace(/<font size="2">/g, ' ');
    	updatedLongDescription = updatedLongDescription.replace(/<dates>/g, '<div id="dates">');
    	updatedLongDescription = updatedLongDescription.replace(/<\/dates>/g, '</div>');
    	updatedLongDescription = updatedLongDescription.replace(/<br>/g, ' ');
    	   	
    	var nativeField = context.form.getField(fieldName);
    	 
    	var customSolutionEditor = context.form.addField({
	    	 id: 'custpage_longdescription',
	    	 label: 'Custom Solution Editor Test',
	    	 type: serverWidget.FieldType.LONGTEXT
    	});
    	
    	context.form.insertField({
    		field : customSolutionEditor,
    		nextfield : 'longdescription'
    	});
    	
    	//sets the value of the editor to the updated long description
    	customSolutionEditor.defaultValue = updatedLongDescription;
    	 
    	nativeField.updateDisplayType({displayType: serverWidget.FieldDisplayType.HIDDEN});

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(context) {
    	var solution = context.newRecord;
    	var fieldName = 'longdescription'; 	
    	
    	//grabs the value in the TinyMCE
    	updatedLongDescription = solution.getValue('custpage_longdescription');
    	
    	//updates native settings to the TinyMCE editor to ensure compatability with the KB
    	updatedLongDescription = updatedLongDescription.replace(/<strong[\s\S]*?>/g, '<b>');
    	updatedLongDescription = updatedLongDescription.replace(/<\/strong>/g, '<\/b>');
    	updatedLongDescription = updatedLongDescription.replace(/<em[\s\S]*?>/g, '<i>');
    	updatedLongDescription = updatedLongDescription.replace(/<\/em>/g, '<\/i>');
    	updatedLongDescription = updatedLongDescription.replace(/<li>/g, '<li style="padding-bottom: 10px; bottom-padding: 10px;">');
    	updatedLongDescription = updatedLongDescription.replace(/<span style="font-size: small;">/g, ' ');
    	
    	//saves the TinyMCE contents to the native field so that the changes are reflected in the KB
    	solution.setValue(fieldName, updatedLongDescription);
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */

    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit
    };
    
});
