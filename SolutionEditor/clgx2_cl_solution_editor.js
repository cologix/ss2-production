/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 * @author Katy Jones katy.jones@cologix.com 
 * @date 7/31/2019
 */
define(['N/record'],
/**
 * @param {record} record
 */
function(record) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(context) {
    	var d = document;
    	var sc2=d.createElement('script');
    	                 
    	sc2.src = "https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js";
    	d.getElementsByTagName('*')[1].appendChild(sc2);                       

    	var fieldName = 'custpage_longdescription';

    	var t = setInterval(function() {
    		try {
    			  if (tinymce) {
    					clearInterval(t);

    					// initialize tinymce and tell it to target an existing text field
    			 
    					tinymce.init({selector: 'textarea#' + fieldName,
//    						  content_css : "https://1337135-sb1.app.netsuite.com/core/media/media.nl?id=16923790&c=1337135_SB1&h=c71b1b64690b11a62e12&mv=jxdry0o0&_xt=.css&fcts=20190626145212&whence=",
    						  fix_list_elements: true,
    						  branding: false,
    						  resize: 'both',
    						  width: 800,
    						  height: 500,
    						  menubar: 'file edit insert view format table tools help',
    						  plugins: ['code', 'hr', 'table', 'image', 'link', 'advlist', 'lists'],
    						  toolbar: ['code | hr | bold | italic | underline | strikethrough | alignleft | aligncenter | alignright | alignjustify | formatselect | fontselect | fontsizeselect | cut | copypaste | indent | outdent | indentblockquote | undo | redo | removeformat',
    							  'table | image | myCustomToolbarButton | noteFormatButton | solutionFormatButton | link | linkFormatButton | numlist | bullist'
    							  ],
    						  default_link_target: "_blank",
    						  formats: {
    							  noteformat: {inline: 'span', styles: {display: 'inline-block', border: '1px solid #0067B1', backgroundColor: '#EDF1F7', padding: '8px', margin: '5px'}},
    							  fullsolutionformat: {block: 'div', styles: {width: '80%', minWidth: '200px', fontSize: '12px'}},
    							  linktextformat: {inline: 'span', styles: {color: '#0067B1', fontWeight: 'bold'}},
    							  bold : {inline : 'b'},
    							  italic : {inline : 'i'},
    							  underline : {inline : 'u'},
    							  },
    						  style_formats: [
    							  {title: 'Note Format', inline: 'span', styles: {display: 'inline-block', border: '1px solid #0067B1', backgroundColor: '#EDF1F7', padding: '8px', margin: '5px'}},

    							  ],
    					  	  setup: function (editor) {
	    					  		editor.ui.registry.addButton('myCustomToolbarButton', {
	    						    text: 'Add Timestamps',
	    						    onAction: function (_) {
	    						    	editor.insertContent('<div style="background-color: #F2F4F6; padding-top: 5px; padding-bottom: 5px; width: 80%; min-width: 200px"> <div id="dates"><i>Created: <b></b></i> - <i>Last Updated: <b></b></i></div></div><br>');
	    						    	}
	    					  		});
	    					  		editor.ui.registry.addButton('noteFormatButton', {
	      						      text: 'Note Format',
	      						      onAction: function (_) {
	      						    	 editor.formatter.apply('noteformat');
	      						      }
	      						    });
	    					  		editor.ui.registry.addButton('solutionFormatButton', {
		      						      text: 'Solution Format',
		      						      onAction: function (_) {
		      						    	 editor.formatter.apply('fullsolutionformat');
		      						      }
		      						});
	    					  		editor.ui.registry.addButton('linkFormatButton', {
		      						      text: 'Link Style',
		      						      onAction: function (_) {
		      						    	 editor.formatter.apply('linktextformat');
		      						      }
		      						});
    						  }
    						  
    						  
    						  
    					});
    			  }
    		} catch (e1) {
    			  ;
    		}
    	},10);
    }

    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    
    function saveRecord(context) {
    	var solution = context.currentRecord;
    	
    	var fieldName = 'custpage_longdescription';
    	
    	//saves the contents of the editor to the native netsuite field
    	solution.setValue(fieldName, tinymce.editors[fieldName].getContent());
    	
    	return true;
	}

    return {
        pageInit: pageInit,
        saveRecord: saveRecord
    };
    
});
