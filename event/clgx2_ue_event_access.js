/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime'],
/**
 * @param {record} record
 */
function(record, runtime) {

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(context) {
    	var event = context.newRecord;
    	var user = runtime.getCurrentUser();
    	var role = user.role;
    	log.debug(role);
    	var accessLevel = event.getValue({fieldId: 'accesslevel'});
    	if(role == 1010){
	    	if(accessLevel == 'BUSY'){
	    		event.setValue({
	    			fieldId : 'accesslevel',
	    			value : 'PUBLIC'
	    		});
	    	}
    	}
    }

    return {
//        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
//        afterSubmit: afterSubmit
    };
    
});
