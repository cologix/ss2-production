/**
 * @NApiVersion 2.x
 * @NScriptType MassUpdateScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   12/14/2017
 * @filename CLGX2_MU_Event.js
 */
define(["N/record"],
function(record) {
    
    /**
     * Definition of Mass Update trigger point.
     *
     * @param {Object} params
     * @param {string} params.type - Record type of the record being processed by the mass update
     * @param {number} params.id - ID of the record being processed by the mass update
     *
     * @since 2016.1
     */
    function each(params) {
    	try {
    		var rec = record.load({ type: params.type, id: params.id });
        	rec.save({ enableSourcing: false, ignoreMandatoryFields: true });
    	} catch(ex) {
    		log.debug({ title: params.type + " - " + params.id, details: ex });
    	}
    }

    return {
        each: each
    };
    
});
