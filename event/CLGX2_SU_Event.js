/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   12/13/2017
 * @description 
 */
define(["N/runtime", "N/search"],
function(runtime, search) {
	
	/**
	 * Returns a contact from the NetSuite account based on the contact's email.
	 * 
	 * @param {string} contactEmail - The contact's email.
	 * @returns {integer}
	 */
	function returnContact(contactEmail) {
		if(contactEmail != null && contactEmail != "") {
			try {
				var columnArray = new Array();
				var filterArray = new Array();
				
				columnArray.push(search.createColumn({ name: "internalid" }));
				filterArray.push(search.createFilter({ name: "email", operator: "contains", values: [contactEmail] }));
				
				var results = search.create({ type: "contact", filters: filterArray, columns: columnArray });
				var id = -1;
				
				results.run().each(function(result) {
					id = result.getValue("internalid");
					return true;
				});
				
				return id;
			} catch(ex) {
				log.debug({ title: "CLGX2_SU_Event - contactExists", details: ex });
			}
		}
		
		return -1;
	}
	
	
	/**
	 * Before Submit Entry Point
	 * 
	 * @param {Object} scriptContext
	 * @return null
	 */
    function beforeSubmit(scriptContext) {
    	var currentRecord = scriptContext.newRecord;
        var userRuntime   = runtime.getCurrentUser();
        
        //23728: Outlook External Organizer
        if(currentRecord.getValue("organizer") == 23738) {
           	var organizerEmail = currentRecord.getValue("custevent_outlook_external_orga_email");
           	var contactID      = returnContact(organizerEmail);

           	if(contactID > -1) {
           		var nextLine = currentRecord.getLineCount({ sublistId: "attendee" });
           		currentRecord.insertLine({ sublistId: "attendee", line: nextLine });
           		currentRecord.setSublistValue({ sublistId: "attendee", fieldId: "attendee", line: nextLine, value: contactID });
           	}
        }
    }

    return {
        beforeSubmit: beforeSubmit
    };
    
});
