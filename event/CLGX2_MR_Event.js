/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 * 
 * @author   Alex Guzenski - alex.guzenski@cologix.com
 * @date     12/14/2017
 * @filename CLGX2_MU_Event.js
 */
define(["N/record", "N/search"],
function(record, search) {
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {
    	return search.load({ type: "calendarevent", id: "customsearch_clgx_mr_event" });
    }

    
    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */
    function map(context) {
    	var recordObject = JSON.parse(context.value);
    	context.write(context.key, recordObject.values.internalid.value);
    }

    
    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
     * @since 2015.1
     */
    function reduce(context) {
    	var rec = record.load({ type: "calendarevent", id: context.key });
    	//var id  = rec.save({ enableSourcing: false, ignoreMandatoryFields: true });
    	log.debug({ title: "CLGX2_MR_Event - reduce", details: "calendarevent - " + id });
    }


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
     * @since 2015.1
     */
    function summarize(summary) {

    }

    
    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
