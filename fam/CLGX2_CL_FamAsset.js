/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 3/6/2019
 * 
 * @ExecutionContext Client (Edit)
 */
define(["/SuiteScripts/clgx/libraries/moment.min", "N/format"],
function(moment, format) {
    function fieldChanged(context) {
    	try {
    		if(context.fieldId == "custrecord_clgx_faq_in_service_date") {
        		var cr                    = context.currentRecord;
        		var inServiceDate         = cr.getValue("custrecord_clgx_faq_in_service_date");
        		var depreciationStartDate = calculateDepreciationStartDate(inServiceDate);
        		
        		cr.setValue({ 
        			fieldId: "custrecord_clgx_faq_depreciation_start", 
        			value: new Date(depreciationStartDate)
        		});
        	}
    	} catch(ex) {
    		log.error({ title: "fieldChanged - Error", details: ex });
    	}
    }
    
    
    /**
     * Calculates the depreciation start date from the transaction date.
     * 
     * @access private
     * @function calculateDepreciationStartDate
     * 
     * @param {string} transactionDate
     * @return {string}
     */
    function calculateDepreciationStartDate(transactionDate) {
    	if(transactionDate) {
    		return moment(transactionDate).startOf("month").add(1, "month").format("M/D/YYYY");
    	}
    }

    return {
        fieldChanged: fieldChanged
    };
    
});
