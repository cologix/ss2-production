/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/9/2018
 */
define(["N/task", "N/runtime", "N/search", "N/file", "N/record", "/SuiteScripts/clgx/libraries/moment.min", "N/format"],
function(task, runtime, search, file, record, moment, format) {
	/**
	 * @typedef ParamObject
	 * @property {string} savedSearchID -
	 * @property {string} sectionType - 
	 * @property {string} actionType - 
	 * @property {number} fileID - 
	 * @property {number} initialIndex - 
	 * @property {number} currentIndex - 
	 * @property {number} pageSize - The number of records returned per page.
	 */
	
    function execute(scriptContext) {
    	var rt = runtime.getCurrentScript();
    	
    	var paramObject              = new Object();
    	paramObject.savedSearchID    = "";
    	paramObject.sectionType      = rt.getParameter("custscript_clgx2_1774_st");
    	paramObject.actionType       = rt.getParameter("custscript_clgx2_1774_at");
    	paramObject.fileID           = parseInt(rt.getParameter("custscript_clgx2_1774_fid"));
    	paramObject.initialIndex     = parseInt(rt.getParameter("custscript_clgx2_1774_idx"));
    	paramObject.currentIndex     = 0;
    	paramObject.pageSize         = parseInt(rt.getParameter("custscript_clgx2_1774_ps"));
    	
    	if(paramObject.sectionType == "delete" && paramObject.actionType == "delete") {
    		paramObject.savedSearchID = "customsearch_clgx_faq_unprocessed_merged";
			var reschedule            = deleteQueueRecords(paramObject);
    		
    		paramObject.savedSearchID = "customsearch_clgx_faq_unprocessed";
			var reschedule            = deleteQueueRecords(paramObject);
			
			if(reschedule) {
				scheduleScript(paramObject);
			}
    	}
    	
    	if(paramObject.sectionType == "create" && paramObject.actionType == "file") {
    		paramObject.savedSearchID = "customsearch_clgx_cip_cfo_faui";
			var reschedule            = createIDFile(paramObject);
			
			if(reschedule) { scheduleScript(paramObject); }
    	}
    	
    	if(paramObject.sectionType == "populate") {
    		if(paramObject.actionType == "file") {
    			paramObject.savedSearchID = "customsearch_clgx_cip_cfo_faui";
    			var reschedule            = createIDFile(paramObject);
    			
    			if(!reschedule) { 
    				paramObject.currentIndex = 0;
    				paramObject.initialIndex = 0;
    				paramObject.actionType   = "create";
    			}
    			
    			scheduleScript(paramObject);
    		} else if(paramObject.actionType == "create") {
    			paramObject.savedSearchID = "customsearch_clgx_cip_cfo_faui";
    			var reschedule            = createQueueRecords(paramObject);
    			
    			if(reschedule) { 
    				scheduleScript(paramObject);
    			}
    		}
    	}
    	
    	if(paramObject.sectionType == "reset") {
    		if(paramObject.actionType == "delete") {
    			paramObject.savedSearchID = "customsearch_clgx_faq_unprocessed_merged";
    			var reschedule            = clearMergeFromRecords(paramObject);
    			
    			paramObject.savedSearchID = "customsearch_clgx_faq_unprocessed";
    			var reschedule            = deleteQueueRecords(paramObject);
    			
    			if(!reschedule) {
    				paramObject.currentIndex = 0;
    				paramObject.initialIndex = 0;
    				paramObject.actionType   = "file"; 
    			}
    			
    			scheduleScript(paramObject);
    		} else if(paramObject.actionType == "file") {
    			paramObject.savedSearchID = "customsearch_clgx_cip_cfo_faui";
    			var reschedule            = createIDFile(paramObject);
    			
    			if(!reschedule) {
    				paramObject.currentIndex = 0;
    				paramObject.initialIndex = 0;
    				paramObject.actionType   = "create";
    				
    				log.debug({ title: "reset-file-create", details: paramObject });
    			}
    			
    			scheduleScript(paramObject);
    		} else if(paramObject.actionType == "create") {
    			paramObject.savedSearchID = "customsearch_clgx_cip_cfo_faui";
    			
    			var reschedule = createQueueRecords(paramObject);
    			if(reschedule) {
    				log.debug({ title: "Reset", details: "create-reschedule" });
    				scheduleScript(paramObject);
    			}
    		}
    	}    	
    }
    
    
    /**
     * Handles initial file creation and rescheduling.
     */
    function createIDFile(paramObject) {
    	try {
    		var recordIDArray   = new Array();
        	
        	var processResults  = null;
        	var existingContent = "";
        	var globalIndex     = 0;
        	
        	var filters         = new Array();
        	var columns         = new Array();
        	var ids             = new Array();
        	
        	var searchObject  = search.load({ id: paramObject.savedSearchID }).run();
        	var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
        	
        	processResults = processResultSet(paramObject, searchResults);
        	
        	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
        		recordIDArray = processResults.ids;
        	} else {
        		var existingFile = file.load({ id: paramObject.fileID });
        		existingContent  = JSON.parse(existingFile.getContents());
        		recordIDArray    = recordIDArray.concat(existingContent).concat(processResults.ids);
        	}
        	
        	while (processResults.nextPage == true) {
        		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
        		processResults = processResultSet(paramObject, searchResults);
        		recordIDArray = recordIDArray.concat(processResults.ids);
        	}
        	
        	var fileObject = file.create({ 
    			name: "famqueue_ids.json", 
    			fileType: file.Type.PLAINTEXT,
    			folder: 12955861,
    			contents: JSON.stringify(recordIDArray)
    		});
        	
        	paramObject.fileID = fileObject.save();
        	
        	return processResults.reschedule;
    	} catch(ex) {
    		log.debug({ title: "createIDFile - Error", details: ex });
    	}
    }
    
    
    /**
     * Processes each page of results.
     */
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var ids          = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		var currentDate      = new Date().getTime();
    		
    		var projectEndDate   = searchResults[r].getValue(searchResults[r].columns[21]) || 0;
    		var projectEndDateTS = new Date(projectEndDate).getTime();
    		
    		var inServiceDate    = searchResults[r].getValue(searchResults[r].columns[6]) || 0;
    		var inServiceDateTS  = new Date(inServiceDate).getTime();
    		
    		if(projectEndDateTS > inServiceDateTS) {
    			inServiceDate = projectEndDate;
    		}

    		var oacAmount         = (parseFloat(searchResults[r].getValue(searchResults[r].columns[10])) * parseFloat(searchResults[r].getValue(searchResults[r].columns[11])));
    		var transactionAmount = parseFloat(searchResults[r].getValue(searchResults[r].columns[10]));
    		
    		if(searchResults[r].getValue(searchResults[r].columns[23])) {
    			var transactionLine = parseInt(searchResults[r].getValue(searchResults[r].columns[1]));
    			var recordObject    = record.load({ type: "advintercompanyjournalentry", id: parseInt(searchResults[r].getValue(searchResults[r].columns[0])) });
    			
    			var lineCredit      = parseFloat(recordObject.getSublistValue({ sublistId: "line", fieldId: "credit", line: transactionLine })) * -1;
    			var lineDebit       = parseFloat(recordObject.getSublistValue({ sublistId: "line", fieldId: "debit", line: transactionLine }));
    			var lineTotal       = 0.00;
    			
    			if(lineDebit) {
    				lineTotal = lineDebit;
    			} else {
    				lineTotal = lineCredit;
    			}
    			
    			var baseCurrency    = parseInt(recordObject.getSublistValue({ sublistId: "line", fieldId: "linebasecurrency", line: transactionLine }));
    			transactionAmount   = lineTotal;
    			oacAmount           = lineTotal;
    		}
    		
    		ids.push({ 
    			custrecord_clgx_faq_tran              : parseInt(searchResults[r].getValue(searchResults[r].columns[0])), 
    			custrecord_clgx_faq_tran_line         : parseInt(searchResults[r].getValue(searchResults[r].columns[1])),
    			custrecord_clgx_faq_job               : parseInt(searchResults[r].getValue(searchResults[r].columns[2])),
    			custrecord_clgx_faq_job_subsidiary    : searchResults[r].getText(searchResults[r].columns[3]),
    			custrecord_clgx_faq_tran_subsidiary   : parseInt(searchResults[r].getValue(searchResults[r].columns[4])),
    			custrecord_clgx_faq_in_service_date   : new Date(inServiceDate).toJSON(),
    			custrecord_clgx_faq_depreciation_start: calculateDepreciationStartDate(inServiceDate),
    			custrecord_clgx_faq_tran_amount       : transactionAmount,
    			custrecord_clgx_faq_memo_line         : searchResults[r].getValue(searchResults[r].columns[13]),
    			custrecord_clgx_faq_oac               : oacAmount,
    			custrecord_clgx_faq_description       : searchResults[r].getValue(searchResults[r].columns[15]),
    			custrecord_clgx_faq_tran_location     : parseInt(searchResults[r].getValue(searchResults[r].columns[16])),
    			custrecord_clgx_faq_tran_facility     : parseInt(searchResults[r].getValue(searchResults[r].columns[17])),
    			custrecord_clgx_faq_tran_facility_name: searchResults[r].getText(searchResults[r].columns[17]),
    			custrecord_clgx_faq_class             : parseInt(searchResults[r].getValue(searchResults[r].columns[18])),
    			custrecord_clgx_faq_department        : searchResults[r].getValue(searchResults[r].columns[19]),
    			custrecord_clgx_faq_cip_type          : parseInt(searchResults[r].getValue(searchResults[r].columns[20])),
    			custrecord_clgx_faq_tran_currency     : baseCurrency != null ? baseCurrency : ""
    		});
    		
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }
    
    
    /**
     * Loads a JSON file from the file cabinet and adds the records to the data warehouse record queue.
     */
    function createQueueRecords(paramObject) {
    	try {
    		var usage    = runtime.getCurrentScript().getRemainingUsage();
    		
    		var idFile   = file.load({ id: paramObject.fileID });
    		var ids      = JSON.parse(idFile.getContents());
    		var idLength = ids.length;
    		
    		paramObject.currentIndex = paramObject.initialIndex;
    		
    		for(var i = parseInt(paramObject.initialIndex); i < idLength; i++) {
    			var cipItemType           = ids[i].custrecord_clgx_faq_cip_type;
    			var depreciationStartDate = ids[i].custrecord_clgx_faq_depreciation_start;
    			
    			var assetFacility         = getFacilityByLocation(ids[i].custrecord_clgx_faq_tran_facility_name);
    			var assetType             = getAssetType(cipItemType);
    			
    			var q = record.create({ type: "customrecord_clgx_fam_ast_queue" });
        		q.setValue({ fieldId: "custrecord_clgx_faq_tran",               value: ids[i].custrecord_clgx_faq_tran  });
        		q.setValue({ fieldId: "custrecord_clgx_faq_tran_subsidiary",    value: ids[i].custrecord_clgx_faq_tran_subsidiary });
        		q.setValue({ fieldId: "custrecord_clgx_faq_tran_line",          value: ids[i].custrecord_clgx_faq_tran_line });
        		q.setValue({ fieldId: "custrecord_clgx_faq_tran_parent",        value: ids[i].custrecord_clgx_faq_tran });
        		q.setValue({ fieldId: "custrecord_clgx_faq_job",                value: ids[i].custrecord_clgx_faq_job });
        		q.setValue({ fieldId: "custrecord_clgx_faq_job_subsidiary",     value: ids[i].custrecord_clgx_faq_job_subsidiary });
        		q.setValue({ fieldId: "custrecord_clgx_faq_tran_amount",        value: ids[i].custrecord_clgx_faq_tran_amount });
        		q.setValue({ fieldId: "custrecord_clgx_faq_memo_line",          value: ids[i].custrecord_clgx_faq_memo_line });
        		q.setValue({ fieldId: "custrecord_clgx_faq_oac",                value: ids[i].custrecord_clgx_faq_oac });
        		q.setValue({ fieldId: "custrecord_clgx_faq_description",        value: ids[i].custrecord_clgx_faq_description });
        		q.setValue({ fieldId: "custrecord_clgx_faq_tran_location",      value: ids[i].custrecord_clgx_faq_tran_location });
        		q.setValue({ fieldId: "custrecord_clgx_faq_tran_facility",      value: ids[i].custrecord_clgx_faq_tran_facility });
        		q.setValue({ fieldId: "custrecord_clgx_faq_asset_facility",     value: assetFacility });
        		q.setValue({ fieldId: "custrecord_clgx_faq_class",              value: ids[i].custrecord_clgx_faq_class });
        		q.setValue({ fieldId: "custrecord_clgx_faq_department",         value: departmentMap(ids[i].custrecord_clgx_faq_department) });
        		q.setValue({ fieldId: "custrecord_clgx_faq_cip_type",           value: cipItemType });
        		q.setValue({ fieldId: "custrecord_clgx_faq_in_service_date",    value: new Date(ids[i].custrecord_clgx_faq_in_service_date) });
        		q.setValue({ fieldId: "custrecord_clgx_faq_depreciation_start", value: format.parse({ value: depreciationStartDate, type: format.Type.DATE }) });
        		//q.setValue({ fieldId: "custrecord_clgx_faq_cust_loc",           value: customerLocationMap(ids[i].custrecord_clgx_faq_job_subsidiary) });
        		
        		if(assetType == 5) { //If leasehold, calculate the asset lifetime.
        			var leaseEndDate = getFacilityEndDate(assetFacility);
        			q.setValue({ fieldId: "custrecord_clgx_faq_asset_lifetime", value: caculateAssetLifetime(depreciationStartDate, leaseEndDate) });
        			//log.debug({ title: "custrecord_clgx_faq_asset_lifetime",    details: caculateAssetLifetime(depreciationStartDate, leaseEndDate) });
        		}
        		
        		//log.debug({ title: "custrecord_clgx_faq_depreciation_start", details: depreciationStartDate });
        		
        		/*log.debug({ title: "custrecord_clgx_faq_tran",               details: ids[i].custrecord_clgx_faq_tran  });
        		log.debug({ title: "custrecord_clgx_faq_tran_line",          details: ids[i].custrecord_clgx_faq_tran_line });
        		log.debug({ title: "custrecord_clgx_faq_tran_parent",        details: ids[i].custrecord_clgx_faq_tran });
        		log.debug({ title: "custrecord_clgx_faq_job",                details: ids[i].custrecord_clgx_faq_job });
        		log.debug({ title: "custrecord_clgx_faq_job_subsidiary",     details: ids[i].custrecord_clgx_faq_job_subsidiary });
        		log.debug({ title: "custrecord_clgx_faq_tran_amount",        details: ids[i].custrecord_clgx_faq_tran_amount });
        		log.debug({ title: "custrecord_clgx_faq_memo_line",          details: ids[i].custrecord_clgx_faq_memo_line });
        		log.debug({ title: "custrecord_clgx_faq_oac",                details: ids[i].custrecord_clgx_faq_oac });
        		log.debug({ title: "custrecord_clgx_faq_description",        details: ids[i].custrecord_clgx_faq_description });
        		log.debug({ title: "custrecord_clgx_faq_tran_location",      details: ids[i].custrecord_clgx_faq_tran_location });
        		log.debug({ title: "custrecord_clgx_faq_tran_facility",      details: ids[i].custrecord_clgx_faq_tran_facility });
        		log.debug({ title: "custrecord_clgx_faq_asset_facility",     details: assetFacility });
        		log.debug({ title: "custrecord_clgx_faq_class",              details: ids[i].custrecord_clgx_faq_class });
        		log.debug({ title: "custrecord_clgx_faq_department",         details: departmentMap(ids[i].custrecord_clgx_faq_department) });
        		log.debug({ title: "custrecord_clgx_faq_cip_type",           details: cipItemType });
        		log.debug({ title: "custrecord_clgx_faq_in_service_date",    details: ids[i].custrecord_clgx_faq_in_service_date });
        		log.debug({ title: "custrecord_clgx_faq_in_service_date",    details: format.parse({ value: ids[i].custrecord_clgx_faq_in_service_date, type: format.Type.DATE }) });
        		log.debug({ title: "custrecord_clgx_faq_depreciation_start", details: format.parse({ value: depreciationStartDate, type: format.Type.DATE }) });
        		log.debug({ title: "custrecord_clgx_faq_cust_loc",           details: customerLocationMap(ids[i].custrecord_clgx_faq_job_subsidiary) });*/
        		
        		q.save({ ignoreMandatoryFields: false, enableSourcing: true });
        		
        		paramObject.currentIndex = paramObject.currentIndex + 1;
        		
        		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
        			break;
        		}
    		}
    		
    		if(usage <= 200) {
    			return true; //Reschedule
    		}
    	} catch(ex) {
    		log.debug({ title: "createQueueRecords - Error", details: ex });
    	}
    	
    	return false;
    }
    
    
    /**
     * Loads a saved search and deletes the specified records from the data warehouse record queue.
     * 
     * @access private
     * @function deleteQueueRecords
     * 
     * @param {ParamObejct} paramObject
     * 
     * @return {Boolean}
     */
    function deleteQueueRecords(paramObject) {
    	try {
    		var usage        = runtime.getCurrentScript().getRemainingUsage();
    		
    		var searchObject = search.load({ id: paramObject.savedSearchID });
    		searchObject.run().each(function(result) {    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
        			return false;
        		}

    			record.delete({ type: "customrecord_clgx_fam_ast_queue", id: result.getValue(result.columns[0]) }); //Update the cancelled checkbox with submitFields.
    			
    			/*record.submitFields({ 
    				type: "customrecord_clgx_fam_ast_queue", 
    				id: result.getValue(result.columns[0]), 
    				values: { custrecord_clgx_faq_cancelled: true } 
    			});*/
    			
    			return true;
    		});
    		
    		if(usage <= 200) {
    			return true; //Reschedule
    		}
    	} catch(ex) {
    		log.debug({ title: "deleteQueueRecords - Error", details: ex });
    	}
    	
    	return false;
    }
    
    
    /**
     * Loads a saved search and clears the merge information from the queue records.
     * 
     * @access private
     * @function clearMergeFromRecords
     * 
     * @param {ParamObejct} paramObject
     * 
     * @return {Boolean}
     */
    function clearMergeFromRecords(paramObject) {
    	try {
    		var usage        = runtime.getCurrentScript().getRemainingUsage();
    		
    		var searchObject = search.load({ id: paramObject.savedSearchID });
    		searchObject.run().each(function(result) {    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
        			return false;
        		}

    			record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: result.getValue(result.columns[0]), values: { custrecord_clgx_faq_merged_into: "" } });
    			
    			return true;
    		});
    		
    		if(usage <= 200) {
    			return true; //Reschedule
    		}
    	} catch(ex) {
    		log.debug({ title: "deleteQueueRecords - Error", details: ex });
    	}
    	
    	return false;
    }
    
    
    /**
     * @access private
     * @function scheduleScript
     * 
     * @param {ParamObject} paramObject
     * 
     * @return null
     */
    function scheduleScript(paramObject) {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = runtime.getCurrentScript().id;
    		t.params       = {
        		custscript_clgx2_1774_st : paramObject.sectionType,
        		custscript_clgx2_1774_at : paramObject.actionType,
        		custscript_clgx2_1774_fid: paramObject.fileID,
        		custscript_clgx2_1774_idx: paramObject.currentIndex,
        		custscript_clgx2_1774_ps : paramObject.pageSize
    		};
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }
    
    
    /**
     * Maps inactive department internal ids to their replacements.
     * 
     * @access private
     * @function departmentMap
     * 
     * @param {number} departmentID
     * @return {string}
     */
    function departmentMap(departmentID) {
    	if(departmentID) {
    		if(departmentID == 1 || departmentID == 22 || departmentID == 12 || departmentID == 15 || departmentID == 14 || departmentID == 3 || departmentID == 20) {
    			return 45;
    		}
    		
    		if(departmentID == 24 || departmentID == 25) {
    			return 9;
    		}
    		
    		return departmentID;
    	}
    }
    
    
    /**
     * Returns a facility internalid from the location name.
     * 
     * @access private
     * @function getFacilityByLocation
     * 
     * @param {string} locationName
     * @return {number}
     */
    function getFacilityByLocation(locationName) {
    	if(locationName) {
    		var columns = new Array();
    		columns.push(search.createColumn("internalid"));
    		columns.push(search.createColumn("name"));
    		
    		var filters = new Array();
    		filters.push(search.createFilter({ name: "name", operator: search.Operator.CONTAINS, values: [locationName] }));
    		filters.push(search.createFilter({ name: "name", operator: search.Operator.DOESNOTCONTAIN, values: ["CRI"] }));
    		filters.push(search.createFilter({ name: "isinactive", operator: search.Operator.IS, values: false }));
    		
    		var facilityID = null;
    		var searchObject = search.create({ type: "customrecord_cologix_facility", columns: columns, filters: filters });
    		searchObject.run().each(function(result) {
    			facilityID = parseInt(result.getValue("internalid"));
    		});
    		
    		return facilityID;
    	}
    }
    
    
    /**
     * Returns the asset lifetime in months from the depreciation start date to the facility lease end date.
     * 
     * @access private
     * @function caculateAssetLifetime
     * 
     * @param {string} startDate - Depreciation start date.
     * @param {string} endDate - Facility lease end date.
     */
    function caculateAssetLifetime(startDate, endDate) {
    	if(startDate && endDate) {
    		return Math.round(moment(endDate).diff(startDate, "months", true));
    	}
    }
    
    
    /**
     * Calculates the depreciation start date from the transaction date.
     * 
     * @access private
     * @function calculateDepreciationStartDate
     * 
     * @param {string} transactionDate
     * @return {string}
     */
    function calculateDepreciationStartDate(transactionDate) {
    	if(transactionDate) {
    		return moment(transactionDate).startOf("month").add(1, "month").format("M/D/YYYY");
    	}
    }
    
    
    /**
     * Returns the facility lease end date.
     * 
     * @access private
     * @function getFacilityEndDate
     * 
     * @param {number} facilityID
     * @return {string}
     */
    function getFacilityEndDate(facilityID) {
    	if(facilityID) {
    		var recordObject = record.load({ type: "customrecord_cologix_facility", id: facilityID });
    		return recordObject.getValue("custrecord_clgx_lease_end");
    	}
    }
    
    
    /**
     * Returns the asset type from the CIP item type.
     * 
     * @access private
     * @function getAssetType
     * 
     * @param {number} cipItemID
     * @return {number}
     */
    function getAssetType(cipItemID) {
    	if(cipItemID) {
    		var recordObject = record.load({ type: "customrecord_clgx_cip_item_type", id: cipItemID });
    		return parseInt(recordObject.getValue("custrecord_clgx_cip_asset_type"));
    	}
    }
    
    
    /**
     * Returns the internal id of the location based on the subsidiary name.
     * 
     * @access private
     * @function customerLocationMap
     * 
     * @param {string} subsidiaryText
     * 
     * @return {number}
     */
    function customerLocationMap(subsidiaryText) {
    	if(subsidiaryText) {
    		return subsidiaryText.indexOf("Canada") == -1 ? 2790 : 2789; //True: US, False: Canada
    	}
    }
    
    
    /**
     * Returns the percentage complete from the script processing.
     * 
     * @access private
     * @function percentageComplete
     * 
     * @param {Number} cutoff - The number of remaining usage points in which the script will cut off execution and reschedule itself.
     */
    function percentageComplete(cutoff) {
    	if(!cutoff) { cutoff = 0; }
    	return ((runtime.getCurrentScript().getRemainingUsage() * 100) / (10000 - cutoff));
    }
    
    return {
        execute: execute
    };
    
});