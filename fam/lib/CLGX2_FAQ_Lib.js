/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 3/13/2019
 */
define(["N/search", "/SuiteScripts/clgx/libraries/lodash.min", "N/task", "N/email", "N/runtime", "N/record", "/SuiteScripts/clgx/libraries/moment.min"],
function(search, _, task, email, runtime, record, moment) {
	/**
     * Returns a JSON string of queue records.
     * 
     * @access public
     * @function getQueueRecords
     * 
     * @param {string} searchID
     * @param {Object} filterArray 
     * @return {Object}
     */
    function getQueueRecords(searchID, filterArray) {
    	try {
    		var filters = new Array();

    		var finalObject  = new Array();
    		var searchObject = search.load({ id: searchID });
    		
    		if(filterArray) {
    			_.forEach(filterArray, function(object, key) {
    				searchObject.filters.push(search.createFilter({ name: object.name, operator: object.operator, values: [parseInt(object.value)] }));
    			});
    		}
    		
    		searchObject.run().each(function(result) {
    			finalObject.push({
    				isparent           : 0,
    				selected           : 0,
    				haserror           : (result.getValue("custrecord_clgx_faq_haserror") === true ? 1 : 0),
    				parentid           : 0,
    				queueid            : parseInt(result.getValue("internalid")),
    				sourcetransaction  : result.getText("custrecord_clgx_faq_tran_parent"),
    				sourcetransactionid: parseInt(result.getValue("custrecord_clgx_faq_tran_parent")),
    				projectname        : result.getText("custrecord_clgx_faq_job"),
    				subsidiary         : result.getText("custrecord_clgx_faq_tran_subsidiary"),
    				inservicedate      : result.getValue("custrecord_clgx_faq_in_service_date"),
    				assetoriginalcost  : parseFloat(result.getValue("custrecord_clgx_faq_oac")).toFixed(2),
    				sourcevendor       : result.getText("custrecord_clgx_faq_pur_vendor"),
    				transactiontype    : result.getText("custrecord_clgx_faq_tran_type"),
    				cipitemtype        : result.getText("custrecord_clgx_faq_cip_type"),
    				lineitemmemo       : unescape(encodeURIComponent(result.getValue("custrecord_clgx_faq_memo_line"))),
    				currency           : result.getValue("custrecord_clgx_faq_tran_currency"),
    				accountingperiod   : result.getText("custrecord_clgx_faq_tran_period"),
    				accountingperiodid : result.getValue("custrecord_clgx_faq_tran_period")
    			});
    			
    			return true;
    		});
    		
    		return finalObject;
    	} catch(ex) {
    		log.error({ title: "getQueueRecords - Error", details: ex });
    	}
    }
    
    
    /**
     * Returns a JSON string of queue records.
     * 
     * @access public
     * @function getQueueRecords
     * 
     * @param {string} searchID
     * @param {Object} filterArray 
     * @return {Object}
     */
    function getBufferedQueueRecords(searchID, filterArray, pageObject) {
    	try {
    		var paramObject = new Object();
    		paramObject.currentIndex = 0;
    		paramObject.pageSize     = 1000;
    		
    		var filters = new Array();

    		var finalObject  = new Array();
    		var searchObject = search.load({ id: searchID });
    		
    		if(filterArray) {
    			_.forEach(filterArray, function(object, key) {
    				searchObject.filters.push(search.createFilter({ name: object.name, operator: object.operator, values: [parseInt(object.value)] }));
    			});
    		}
    		
    		searchObject = searchObject.run();
    		
    		var searchResults = searchObject.getRange({ start: 0, end: 1000 });
    		processResults    = processResultSet(paramObject, searchResults);
    		finalObject       = processResults.ids;
    		
        	while (processResults.nextPage == true) {
        		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
        		processResults = processResultSet(paramObject, searchResults);
        		finalObject    = finalObject.concat(processResults.ids);
        	}
    		
    		return finalObject;
    	} catch(ex) {
    		log.error({ title: "getBufferedQueueRecords - Error", details: ex });
    	}
    }
    
    
    /**
     * Processes each page of results.
     */
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var ids          = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		ids.push({
				isparent           : 0,
				selected           : 0,
				haserror           : (searchResults[r].getValue("custrecord_clgx_faq_haserror") === true ? 1 : 0),
				parentid           : 0,
				queueid            : parseInt(searchResults[r].getValue("internalid")),
				sourcetransaction  : searchResults[r].getText("custrecord_clgx_faq_tran_parent"),
				sourcetransactionid: parseInt(searchResults[r].getValue("custrecord_clgx_faq_tran_parent")),
				projectname        : searchResults[r].getText("custrecord_clgx_faq_job"),
				subsidiary         : searchResults[r].getText("custrecord_clgx_faq_tran_subsidiary"),
				inservicedate      : searchResults[r].getValue("custrecord_clgx_faq_in_service_date"),
				assetoriginalcost  : parseFloat(searchResults[r].getValue("custrecord_clgx_faq_oac")).toFixed(2),
				sourcevendor       : searchResults[r].getText("custrecord_clgx_faq_pur_vendor"),
				transactiontype    : searchResults[r].getText("custrecord_clgx_faq_tran_type"),
				cipitemtype        : searchResults[r].getText("custrecord_clgx_faq_cip_type"),
				lineitemmemo       : searchResults[r].getValue("custrecord_clgx_faq_memo_line").replace(/[^\x20-\x7E]+/g, ""),
				currency           : searchResults[r].getValue("custrecord_clgx_faq_tran_currency"),
				accountingperiod   : searchResults[r].getText("custrecord_clgx_faq_tran_period"),
				accountingperiodid : searchResults[r].getValue("custrecord_clgx_faq_tran_period")
			});
    		
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }
    
    
    
    
    
    /**
     * Returns a single record as an object.
     * 
     * @access public
     * @function getQueueRecord
     * 
     * @param {Number} recordID
     * @return {Object}
     */
    function getQueueRecord(recordID) {
    	if(recordID) {
    		var recordObject = record.load({ type: "customrecord_clgx_fam_ast_queue", id: recordID });
    		
    		return {
    			isparent           : 0,
    			selected           : 0,
				parentid           : 0,
				queueid            : recordID,
				sourcetransaction  : recordObject.getText("custrecord_clgx_faq_tran_parent"),
				sourcetransactionid: parseInt(recordObject.getValue("custrecord_clgx_faq_tran_parent")),
				projectname        : recordObject.getText("custrecord_clgx_faq_job"),
				subsidiary         : recordObject.getText("custrecord_clgx_faq_tran_subsidiary"),
				inservicedate      : moment(recordObject.getValue("custrecord_clgx_faq_in_service_date")).format("M/D/YYYY"),
				assetoriginalcost  : parseFloat(recordObject.getValue("custrecord_clgx_faq_oac")).toFixed(2),
				sourcevendor       : recordObject.getText("custrecord_clgx_faq_pur_vendor"),
				transactiontype    : recordObject.getText("custrecord_clgx_faq_tran_type"),
				cipitemtype        : recordObject.getText("custrecord_clgx_faq_cip_type"),
				lineitemmemo       : recordObject.getValue("custrecord_clgx_faq_memo_line"),
				currency           : recordObject.getValue("custrecord_clgx_faq_tran_currency")
    		};
    	}
    }
    
    
    /**
     * Updates an object property with the given value.
     * 
     * @access public
     * @function updateObjectProperty
     * 
     * @param {Object|Array} paramObject.records
     * @param {Object} paramObject.property
     * @param {Object} paramObject.value
     * @return {Object}
     */
    function updateObjectProperty(paramObject) {
    	if(paramObject) {
    		if(paramObject.records.length != null) {
    			_.forEach(paramObject.records, function(value, key) {
            		paramObject.records[key][paramObject.property] = paramObject.value;
            	});
    		} else {
            	paramObject.records[paramObject.property] = paramObject.value;
    		}
    		
    		return paramObject.records;
    	}
    }
    
    
    /**
     * Returns a JSON object for filtering on transaction type.
     * 
     * @access public
     * @function filterTransactionTypes
     * 
     * @param {object} recordArray
     * @return {string}
     */
    function filterTransactionTypes(recordArray) {
    	if(recordArray) {
    		var finalArray         = new Array();
    		var recordObject       = JSON.parse(recordArray);
    		var recordObjectLength = recordObject.length;
    		
    		for(var r = 0; r < recordObjectLength; r++) {
    			finalArray.push({ id: recordObject[r].transactiontype, text: recordObject[r].transactiontype });
    		}
    		
    		return JSON.stringify(_.uniq(finalArray));
    	}
    }
    
    
    function filterProjects(recordArray) {
    	if(recordArray) {
    		var finalArray         = new Array();
    		var recordObject       = JSON.parse(recordArray);
    		var recordObjectLength = recordObject.length;
    		
    		for(var r = 0; r < recordObjectLength; r++) {
    			finalArray.push({ id: recordObject[r].transactiontype, text: recordObject[r].transactiontype });
    		}
    		
    		return JSON.stringify(_.uniq(finalArray));
    	}
    }
    
    
    /**
     * Builds an array of filters to be returned to the UI.
     * 
     * @access public
     * @function buildFilter
     * 
     * @param {FilterParamObject} paramObject
     * @returns {String}
     */
    /*function buildFilter(paramObject) {
    	if(paramObject) {
    		var finalArray         = new Array();
    		var recordObject       = JSON.parse(paramObject.records);
    		var recordObjectLength = recordObject.length;
    		
    		for(var r = 0; r < recordObjectLength; r++) {
    			finalArray.push({ id: recordObject[r][paramObject.dataIndex], text: recordObject[r][paramObject.dataIndex] });
    		}
    		
    		return JSON.stringify(_.uniq(finalArray));
    	}
    }*/
    
    
    /**
     * Builds an array of filters to be returned to the UI.
     * 
     * @access public
     * @function buildFilter
     * 
     * @param {FilterParamObject} paramObject
     * @returns {String}
     */
    function buildListFilter(paramObject) {
    	if(paramObject) {
    		var finalArray         = new Array();
    		var recordObject       = paramObject.records;
    		var recordObjectLength = recordObject.length;
    		
    		for(var r = 0; r < recordObjectLength; r++) {
    			if(recordObject[r][paramObject.dataIndex]) {
    				finalArray.push({ id: recordObject[r][paramObject.dataIndex], text: recordObject[r][paramObject.dataIndex] });
    			}
    		}
    		
    		return _.uniqBy(finalArray, "id");
    	}
    }
    
    
    /**
     * @access public
     * @function scheduleScript
     * 
     * @param {ParamObject} paramObject
     * 
     * @return null
     */
    function scheduleScript(paramObject) {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = paramObject.scriptId;
    		t.deploymentId = paramObject.deploymentId;
    		t.params       = paramObject.params;
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }
    
    
    /**
     * Sends an email to the user who triggered an action.
     * 
     * @access public
     * @function sendEmailUpdate
     * 
     * @param {string} actionName
     * 
     * @return null
     */
    function sendEmailUpdate(actionName) {
    	if(actionName) {
    		try {
        		var userId = runtime.getCurrentUser().id;
        		email.send({
        			author: userId,
        			recipients: [userId],
        			replyTo: "no-reply@cologix.com",
        			subject: actionName + " - Processing Complete",
        			body: actionName + " has completed processing."
        		});
        	} catch(ex) {
        		log.error({ title: "sendEmailUpdate - Error", details: ex });
        	}
    	}
    }
    
    

    
    /**
     * Maps inactive department internal ids to their replacements.
     * 
     * @access private
     * @function departmentMap
     * 
     * @param {number} departmentID
     * @return {string}
     */
    function departmentMap(departmentID) {
    	if(departmentID) {
    		
    		if(departmentID == 1 || departmentID == 22 || departmentID == 12 || departmentID == 15 || departmentID == 14 || departmentID == 3 || departmentID == 20) {
    			return 45;
    		} else if(departmentID == 24 || departmentID == 3) {
    			return 9;
    		}
    		
    		return departmentID;
    	}
    }
    
    
    /**
     * Returns a facility internalid from the location id.
     * 
     * @access private
     * @function getFacilityByLocation
     * 
     * @libraries N/search
     * 
     * @param {Number} locationID
     * @return {number}
     */
    function getFacilityByLocation(locationID) {
    	if(locationID) {
    		var columns = new Array();
    		columns.push(search.createColumn("internalid"));
    		columns.push(search.createColumn("name"));
    		columns.push(search.createColumn("custrecord_clgx_lease_end"));
    		
    		var filters = new Array();
    		filters.push(search.createFilter({ name: "custrecord_clgx_facility_location", operator: search.Operator.IS, values: [locationID] }));
    		filters.push(search.createFilter({ name: "isinactive", operator: search.Operator.IS, values: false }));
    		
    		var facilityID = null;
    		var searchObject = search.create({ type: "customrecord_cologix_facility", columns: columns, filters: filters });
    		searchObject.run().each(function(result) {
    			facilityID = parseInt(result.getValue("internalid"));
    		});
    		
    		return facilityID;
    	}
    }
    
    
    /**
     * Returns whether or not a script is running.
     * 
     * @access public
     * @function isScriptInstanceRunning
     * 
     * @param {string} searchName
     * @return {boolean}
     */
    function isScriptInstanceRunning(searchName, filterArray) {
    	if(searchName) {
    		try {
    			var recordCount  = 0;
        		var searchObject = search.load({ id: searchName });
        		
        		if(filterArray) {
        			_.forEach(filterArray, function(object, key) {
        				if(object.join) {
        					searchObject.filters.push(search.createFilter({ name: object.name, join: object.join, operator: object.operator, values: [object.value] }));
        				} else {
        					searchObject.filters.push(search.createFilter({ name: object.name, operator: object.operator, values: [object.value] }));
        				}
        			});
        		}
        		
        		searchObject.run().each(function(result) {
        			recordCount = recordCount + 1;
        			return true;
        		});
        		
        		if(recordCount > 0) {
        			return "true";
        		} else {
        			return "false";
        		}
    		} catch(ex) {
    			log.error({ title: "isScriptInstanceRunning - Error", details: ex });
    		}
    	}
    }

    
    /**
     * Returns information on a list item based on internal id.
     * 
     * @access public
     * @function getListItem
     * 
     * @param {Object}  paramObject
     * @param {String}  paramObject.type - The internal id of the list to search.
     * @param {Integer} paramObject.id - The internal id of the list item.
     * 
     * @return {Object}
     */
    function getListItem(paramObject) {
    	if(paramObject) {
    		var finalObject = new Object();
    		
    		var filters = new Array();
    		filters.push(search.createFilter({ name: "internalid", operator: "is", values: paramObject.id }));
    		
    		var columns = new Array();
    		columns.push(search.createColumn("internalid"));
    		columns.push(search.createColumn("isinactive"));
    		columns.push(search.createColumn("name"));
    		
    		var searchObject = search.create({ type: paramObject.type, columns: columns, filters: filters });
    		searchObject.run().each(function(result) {
    			finalObject = {
    				id      : result.getValue("internalid"),
    				name    : result.getValue("name"),
    				inactive: result.getValue("isinactive")
    			};
    		});
    		
    		return finalObject;
    	}
    }
    
    function escapeUnicode(string) {
    	return string.replace(/[\s\S]/g, function(character) {
    		var escape = character.charCodeAt().toString(16),
    		    longhand = escape.length > 2;
    		return '\\' + (longhand ? 'u' : 'x') + ('0000' + escape).slice(longhand ? -4 : -2);
    	});
    }
    
    return {
    	getQueueRecords: getQueueRecords,
    	getBufferedQueueRecords: getBufferedQueueRecords,
    	getQueueRecord: getQueueRecord,
    	updateObjectProperty: updateObjectProperty,
    	filterTransactionTypes: filterTransactionTypes,
    	scheduleScript: scheduleScript,
    	sendEmailUpdate: sendEmailUpdate,
    	buildListFilter: buildListFilter,
    	departmentMap: departmentMap,
    	getFacilityByLocation: getFacilityByLocation,
    	isScriptInstanceRunning: isScriptInstanceRunning,
    	getListItem: getListItem
    };
    
});
