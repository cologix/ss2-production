/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define([
	"N/search", 
	"N/record", 
	"N/runtime", 
	"/SuiteScripts/clgx/fam/lib/CLGX2_FAQ_Lib", 
	"/SuiteScripts/clgx/libraries/lodash.min",
	"/SuiteScripts/clgx/libraries/moment.min",
	"N/email"
],
function(search, record, runtime, lib, _, moment, email) {
    function execute(context) {
    	try {
    		var rt = runtime.getCurrentScript();
        	
        	var paramObject           = new Object();
        	paramObject.savedSearchID = "";
        	paramObject.actionType    = rt.getParameter("custscript_clgx_1778_at");
        	paramObject.parentID      = rt.getParameter("custscript_clgx_1778_pid") || null;
        	paramObject.queueIDs      = rt.getParameter("custscript_clgx_1778_qids") || null;
        	paramObject.jeDate        = moment(rt.getParameter("custscript_clgx_1778_jed")).format("M/D/YYYY") || null;
        	
        	if(paramObject.actionType == "fam") {
        		generateFams(paramObject);
        		
        		lib.scheduleScript({ scriptId: "1840", params: { custscript_clgx_1778_at: "journal", custscript_clgx_1778_jed: paramObject.jeDate, custscript_clgx_1778_qids: paramObject.queueIDs } });
        	}
        	
        	if(paramObject.actionType == "journal") {
        		var journalObject = generateJournalObjects(paramObject);
        		var journals      = createJournalEntries(paramObject, journalObject);
        		
        		if(journals && journals.length > 0) {
        			sendEmail({
            			subject: "Your FAM records have been processed",
            			body: "The records you scheduled have finished processing.<br/><br/>Generated journal entries:<br /> " + buildJournalList(journals) + "<br/><br/>Please refer to the following saved search for today's generated assets: <a href=\"https://1337135.app.netsuite.com/app/common/search/searchresults.nl?searchid=7432&saverun=T&whence=\">CLGX_FAQ_ProcessedToday</a>"
            		});
        		} else {
        			sendEmail({
            			subject: "Some FAM records have not been processed",
            			body: "Some FAM records have not generated. Please check the Pending Asset Generation tab of FAUI and contact your administrator."
            		});
        		}
        	}
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }
    
    
    /**
     * Generates the FAM records.
     * 
     * @access private
     * @function generateFams
     * 
     * @param {ParamObject} paramObject
     * @return null
     */
    function generateFams(paramObject) {
    	if(paramObject) {
    		paramObject.savedSearchID = "customsearch_clgx_faq_r2p";
    		
    		var queueRecords          = JSON.parse(paramObject.queueIDs);
    		var usage                 = runtime.getCurrentScript().getRemainingUsage();
    		
    		var searchObject = search.load({ id: paramObject.savedSearchID });
    		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyof", values: queueRecords }));
    		
    		searchObject.run().each(function(result) {
    			var famQueueID = parseInt(result.getValue("internalid"));
    			var famID      = createFamRecord(result);
    			
    			updateQueueRecordWithFAM(famQueueID, famID);
    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 500) {
    				return false;
    			}
    			
    			return true;
    		});
    		
    		//Reschedule
    	}
    }
    
    
    /**
     * Generates a fam record.
     * 
     * @access private
     * @function createFamRecord
     * 
     * @param {Object} resultObject
     *
     * @return {number}
     */
    function createFamRecord(resultObject) {
    	try {
    		var transactionSubsidiary     = resultObject.getValue("custrecord_clgx_faq_tran_subsidiary");
    		var transactionSubsidiaryText = resultObject.getText("custrecord_clgx_faq_tran_subsidiary");
    		var jobSubsidiary             = resultObject.getValue("custrecord_clgx_faq_job_subsidiary");
    		
    		var recordObject = record.create({ type: "customrecord_ncfar_asset" });
    		recordObject.setValue({ fieldId: "altname",                       value: resultObject.getText("custrecord_clgx_faq_cip_type") });
    		recordObject.setValue({ fieldId: "custrecord_assetdescr",         value: resultObject.getText("custrecord_clgx_faq_cip_type") });
    		
    		if(transactionSubsidiaryText == jobSubsidiary) {
    			recordObject.setValue({ fieldId: "custrecord_assetproject", value: resultObject.getValue("custrecord_clgx_faq_job") });
    		}
    		
    		recordObject.setValue({ fieldId: "custrecord_assettype",          value: resultObject.getValue("custrecord_clgx_faq_asset_type") });
    		recordObject.setValue({ fieldId: "custrecord_assetcost",          value: resultObject.getValue("custrecord_clgx_faq_oac") });
    		recordObject.setValue({ fieldId: "custrecord_assetresidualvalue", value: "0.00" });
    		recordObject.setValue({ fieldId: "custrecord_assetaccmethod",     value: resultObject.getValue("custrecord_clgx_faq_accounting_method") });
    		recordObject.setValue({ fieldId: "custrecord_assetlifetime",      value: resultObject.getValue("custrecord_clgx_faq_asset_lifetime") });
    		recordObject.setValue({ fieldId: "custrecord_asset_customer",     value: resultObject.getValue("custrecord_clgx_faq_cust_loc") });
    		recordObject.setValue({ fieldId: "custrecord_assetdepartment",    value: resultObject.getValue("custrecord_clgx_faq_department") });
    		recordObject.setValue({ fieldId: "custrecord_assetclass",         value: resultObject.getValue("custrecord_clgx_faq_class") });
    		recordObject.setValue({ fieldId: "custrecord_assetlocation",      value: resultObject.getValue("custrecord_clgx_faq_tran_location") });
    		recordObject.setValue({ fieldId: "custrecord_assetfacility",      value: resultObject.getValue("custrecord_clgx_faq_asset_facility") });
    		recordObject.setValue({ fieldId: "custrecord_assetsubsidiary",    value: transactionSubsidiary });
    		recordObject.setValue({ fieldId: "custrecord_assetpurchasedate",  value: new Date(resultObject.getValue("custrecord_clgx_faq_pur_date")) });
    		recordObject.setValue({ fieldId: "custrecord_assetdeprstartdate", value: new Date(resultObject.getValue("custrecord_clgx_faq_depreciation_start")) });
    		recordObject.setValue({ fieldId: "custrecord_assetdepractive",    value: 1 }); //True
    		recordObject.setValue({ fieldId: "custrecord_assetdeprrules",     value: resultObject.getValue("custrecord_clgx_faq_dep_rules") });
    		recordObject.setValue({ fieldId: "custrecord_assettaxcategory",   value: resultObject.getValue("custrecord_clgx_faq_tax_cat") });
    		recordObject.setValue({ fieldId: "custrecord_assetmainacc",       value: resultObject.getValue("custrecord_clgx_faq_asset_account") });
    		recordObject.setValue({ fieldId: "custrecord_assetsourcetrn",     value: resultObject.getValue("custrecord_clgx_faq_tran_parent") });
    		recordObject.setValue({ fieldId: "custrecord_assetsourcetrnline", value: resultObject.getValue("custrecord_clgx_faq_tran_line") });
    		
    		
    		/*log.debug({ title: "custrecord_clgx_faq_cip_type", details: resultObject.getText("custrecord_clgx_faq_cip_type") });
    		log.debug({ title: "custrecord_clgx_faq_description", details: resultObject.getValue("custrecord_clgx_faq_description") });
    		log.debug({ title: "custrecord_clgx_faq_job", details: resultObject.getValue("custrecord_clgx_faq_job") });
    		log.debug({ title: "custrecord_clgx_faq_asset_type", details: resultObject.getValue("custrecord_clgx_faq_asset_type") });
    		log.debug({ title: "custrecord_clgx_faq_oac", details: resultObject.getValue("custrecord_clgx_faq_oac") });
    		log.debug({ title: "custrecord_clgx_faq_accounting_method", details: resultObject.getValue("custrecord_clgx_faq_accounting_method") });
    		log.debug({ title: "custrecord_clgx_faq_asset_lifetime", details: resultObject.getValue("custrecord_clgx_faq_asset_lifetime") });
    		log.debug({ title: "custrecord_clgx_faq_cust_loc", details: resultObject.getValue("custrecord_clgx_faq_cust_loc") });
    		log.debug({ title: "custrecord_clgx_faq_department", details: resultObject.getValue("custrecord_clgx_faq_department") });
    		log.debug({ title: "custrecord_clgx_faq_class", details: resultObject.getValue("custrecord_clgx_faq_class") });
    		log.debug({ title: "custrecord_clgx_faq_tran_location", details: resultObject.getValue("custrecord_clgx_faq_tran_location") });
    		log.debug({ title: "custrecord_clgx_faq_tran_facility", details: resultObject.getValue("custrecord_clgx_faq_tran_facility") });
    		log.debug({ title: "custrecord_clgx_faq_tran_subsidiary", details: resultObject.getValue("custrecord_clgx_faq_tran_subsidiary") });
    		log.debug({ title: "custrecord_clgx_faq_pur_date", details: resultObject.getValue("custrecord_clgx_faq_pur_date") });
    		log.debug({ title: "custrecord_clgx_faq_depreciation_start", details: resultObject.getValue("custrecord_clgx_faq_depreciation_start") });
    		log.debug({ title: "custrecord_clgx_faq_dep_rules", details: resultObject.getValue("custrecord_clgx_faq_dep_rules") });
    		log.debug({ title: "custrecord_clgx_faq_tax_cat", details: resultObject.getValue("custrecord_clgx_faq_tax_cat") });
    		log.debug({ title: "custrecord_clgx_faq_asset_account", details: resultObject.getValue("custrecord_clgx_faq_asset_account") });
    		log.debug({ title: "custrecord_clgx_faq_tran_parent", details: resultObject.getValue("custrecord_clgx_faq_tran_parent") });
    		log.debug({ title: "custrecord_clgx_faq_tran_line", details: resultObject.getValue("custrecord_clgx_faq_tran_line") });*/
    		
    		return recordObject.save({ enableSourcing: true, ignoreMandatoryFields: false });
    	} catch(ex) {
    		log.error({ title: "createFamRecord - Error", details: ex });
    	}
    }
    
    
    /**
     * Updates a queue record with the newly created fam record id.
     * 
     * @access private
     * @function updateQueueRecordWithFAM
     * 
     * @param {number} recordID
     * @param {number} famID
     * 
     * @return null
     */
    function updateQueueRecordWithFAM(recordID, famID) {
    	if(recordID && famID) {
    		try {
        		record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: recordID, values: { custrecord_clgx_faq_fam_asset: famID } });
        	} catch(ex) {
        		log.error({ title: "updateQueueRecordWithFAM - Error", details: ex });
        	}
    	}
    }
    
    
    /**
     * Updates a queue record with the newly created journal entry id.
     * 
     * @access private
     * @function updateQueueRecordWithJE
     * 
     * @param {number} recordID
     * @param {number} jeID
     * 
     * @return null
     */
    function updateQueueRecordWithJE(recordID, jeID) {
    	if(recordID && jeID) {
    		try {
        		record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: recordID, values: { custrecord_clgx_faq_journal_entry: jeID, custrecord_clgx_faq_processed: true } });
        	} catch(ex) {
        		log.error({ title: "updateQueueRecordWithJE - Error", details: ex });
        	}
    	}
    }
    
    
    
    /**
     * Updates a queue record with an error message if it failed to process.
     * 
     * @access private
     * @function updateQueueRecordWithError
     * 
     * @param {Number} recordID
     * @param {Object} errorObject
     * 
     * @return undefined
     */
    function updateQueueRecordWithError(recordID, errorObject) {
    	if(recordID && errorObject) {
    		try {
        		record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: recordID, values: { custrecord_clgx_faq_error_message: JSON.stringify(errorObject), custrecord_clgx_faq_haserror: true } });
        	} catch(ex) {
        		log.error({ title: "updateQueueRecordWithError - Error", details: ex });
        	}
    	}
    }
    
    
    
    /**
     * Generates the journal records.
     * 
     * @access private
     * @function generateJournals
     * 
     * @param {ParamObject} paramObject
     * @return {Object}
     */
    function generateJournalObjects(paramObject) {
    	if(paramObject) {
    		paramObject.savedSearchID = "customsearch_clgx_faq_r2p_j";
    		
    		var queueRecords          = JSON.parse(paramObject.queueIDs);
    		var usage                 = runtime.getCurrentScript().getRemainingUsage();
    		
    		var faqRecordCredits = new Array();
    		var faqRecordDebits  = new Array();
    		var faqRecordUpdates = new Array();
    		
    		var searchObject = search.load({ id: paramObject.savedSearchID });
    		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyof", values: queueRecords }));
    		
    		searchObject.run().each(function(result) {
    			var jobID   = result.getValue("custrecord_clgx_faq_job");
    			var jobName = result.getText("custrecord_clgx_faq_job");
    			var oac     = parseFloat(result.getValue("custrecord_clgx_faq_oac")).toFixed(2);
    			
    			
    			var tmpCredit = {
        			faq_id        : parseInt(result.getValue("internalid")),
        			subsidiary    : result.getValue("custrecord_clgx_faq_tran_subsidiary"),
        			job           : jobID,
        			job_subsidiary: result.getValue("custrecord_clgx_faq_job_subsidiary"),
        			account       : parseInt(result.getValue("custrecord_clgx_faq_asset_account")),
        			debit         : oac,
        			credit        : null,
        			memo          : "Transfer CIP to In-Service Assets - " + paramObject.jeDate + " - " + jobName,
        			class         : parseInt(result.getValue("custrecord_clgx_faq_class")),
        			department    : parseInt(result.getValue("custrecord_clgx_faq_department")),
        			location      : parseInt(result.getValue("custrecord_clgx_faq_tran_location"))
        		};
    			
    			
    			var tmpDebit = {
        			faq_id        : parseInt(result.getValue("internalid")),
        			subsidiary    : result.getValue("custrecord_clgx_faq_tran_subsidiary"),
        			job           : jobID,
        			job_subsidiary: result.getValue("custrecord_clgx_faq_job_subsidiary"),
        			account       : 406, //CIP Account
        			debit         : null,
        			credit        : oac,
        			memo          : "Transfer CIP to In-Service Assets - " + paramObject.jeDate + " - " + jobName,
        			class         : parseInt(result.getValue("custrecord_clgx_faq_class")),
        			department    : parseInt(result.getValue("custrecord_clgx_faq_department")),
        			location      : parseInt(result.getValue("custrecord_clgx_faq_tran_location"))
        		};
    			
    			
    			var tmpUpdate = {
    				faq_id    : parseInt(result.getValue("internalid")), 
    				subsidiary: result.getValue("custrecord_clgx_faq_tran_subsidiary")
    			};
    			
    			
    			if(isNegative(oac)) {
    				tmpCredit.debit  = oac;
    				tmpCredit.credit = null;
    				
    				tmpDebit.debit  = null;
    				tmpDebit.credit = oac;
    			}
    			
    			faqRecordCredits = pushObjectToArray(faqRecordCredits, tmpCredit);
    			faqRecordDebits = pushObjectToArray(faqRecordDebits, tmpDebit);
    			faqRecordUpdates.push(tmpUpdate);
    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 500) {
    				return false;
    			}
    			
    			return true;
    		});
    		
    		var mergedRows = _.concat(faqRecordCredits, faqRecordDebits);
    		var groupedRows = _.groupBy(mergedRows, "subsidiary");
    		
    		return { updates: _.groupBy(faqRecordUpdates, "subsidiary"), rows: _.groupBy(mergedRows, "subsidiary") };
    	}
    }

    
    /**
     * Creates a journal entry record.
     * 
     * @access private
     * @function createJournalEntry
     * 
     * @param {Object} journalObject
     */
    function createJournalEntries(paramObject, journalObject) {
    	if(journalObject.rows) {
    		var finalJournals = new Array();
    		_.forEach(journalObject.rows, function(headerValue, headerKey) {
    			var recordObject = record.create({ type: "journalentry", isDynamic: true });
    			
    			//Header
    			recordObject.setValue({ fieldId: "exchangerate",                  value: "1" });
    			recordObject.setValue({ fieldId: "trandate",                      value: new Date(paramObject.jeDate) });
    			recordObject.setValue({ fieldId: "custbody_clgx_inservice_entry", value: true });
    			recordObject.setValue({ fieldId: "subsidiary",                    value: headerKey });
    			
    			//Line Items
    			_.forEach(headerValue, function(lineValue, lineKey) {
    				recordObject.selectNewLine({ sublistId: "line"});
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "account",                         value: lineValue.account });
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "debit",                           value: lineValue.debit });
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "credit",                          value: lineValue.credit });
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "custcol_clgx_journal_project_id", value: lineValue.job });
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "memo",                            value: lineValue.memo });
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "department",                      value: lineValue.department });
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "class",                           value: lineValue.class });
    				recordObject.setCurrentSublistValue({ sublistId: "line", fieldId: "location",                        value: lineValue.location });
    				recordObject.commitLine({ sublistId: "line" });
    			});
    			
    			var journalEntryID = recordObject.save();
    			finalJournals.push(journalEntryID);
    			
    			//Update queue records
    			_.forEach(journalObject.updates[headerKey], function(updateValue, updateKey) {
    				updateQueueRecordWithJE(updateValue.faq_id, journalEntryID);
    			});
    		}); 
    		
    		return finalJournals;
    	}
    }
    
    
    /**
     * Returns whether or not a number is negative.
     * 
     * @access private
     * @function isNegative
     * 
     * @param {Number} number
     * @return {Boolean}
     */
    function isNegative(number) {
    	if(number < 0) {
    		return true;
    	}
    	
    	return false;
    }
    
    
    /**
     * Merges existing objects into an array while summing the credits or debits if the object already exists in the array.
     * 
     * @access private
     * @function pushObjectToArray
     * 
     * @param {Object} destinationObject
     * @param {Object} sourceObject
     * 
     * @return {Object}
     */
    function pushObjectToArray(destinationArray, sourceObject) {
    	if(destinationArray && sourceObject) {
    		var index = _.findIndex(destinationArray, { account: sourceObject.account, job: sourceObject.job, department: sourceObject.department, class: sourceObject.class, location: sourceObject.location });
    		if(index != -1) {
    			if(destinationArray[index].credit != null) {
    				var existingCredit = parseFloat(destinationArray[index].credit);
    				var newCredit      = parseFloat(sourceObject.credit);
    				
    				destinationArray[index].credit = parseFloat(existingCredit + newCredit).toFixed(2);
    			} else if(destinationArray[index].debit != null) {
    				var existingDebit = parseFloat(destinationArray[index].debit);
    				var newDebit      = parseFloat(sourceObject.debit);
    				
    				destinationArray[index].debit = parseFloat(existingDebit + newDebit).toFixed(2);
    			}
    		} else {
    			destinationArray.push(sourceObject);
    		}
    		
    		return destinationArray;
    	}
    }
    
    
    /**
     * Sends an email to the current logged in user that the queue has finished processing.
     * 
     * @access private
     * @function sendEmail
     * 
     * @param {Object} paramObject
     * @return null
     */
    function sendEmail(paramObject) {
    	if(paramObject) {
    		var currentUser = runtime.getCurrentUser();
    		
    		email.send({
    			author    : currentUser.id,
    			recipients: [currentUser.id],
    			subject   : paramObject.subject,
    			body      : paramObject.body
    		});
    	}
    }
    
    
    /**
     * Returns a string of URLs created from the list of journal entry internal ids.
     * 
     * @access private
     * @function buildJournalList
     * 
     * @param {Object} journalArray
     * @return {String}
     */
    function buildJournalList(journalArray) {
    	if(journalArray) {
    		var finalString  = "";
    		var journalCount = journalArray.length;
    		for(var j = 0; j < journalCount; j++) {
    			finalString += "https://1337135.app.netsuite.com/app/accounting/transactions/journal.nl?id=" + journalArray[j] + "&whence=<br/>";
    		}
    		
    		return finalString;
    	}
    }
    
    return {
        execute: execute
    };
    
});
