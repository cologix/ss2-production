/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski
 * @date 3/4/2019
 * 
 * @ExecutionContext Server (Edit)
 */
define(["N/record", "N/search", "N/format", "/SuiteScripts/clgx/libraries/moment.min", "/SuiteScripts/clgx/fam/lib/CLGX2_FAQ_Lib"],
function(record, search, format, moment, lib) {
    function beforeSubmit(scriptContext) {
    	try {    		
    		var nr        = scriptContext.newRecord;
    		var assetType = nr.getValue("custrecord_clgx_faq_asset_type");
    		
    		if(assetType == 5) { //Leasehold
    			var depreciationStartDate = nr.getValue("custrecord_clgx_faq_depreciation_start");
    			var assetFacilityID       = nr.getValue("custrecord_clgx_faq_asset_facility");
    			var facilityEndDate       = nr.getValue("custrecord_clgx_faq_lease_end");
    			
    			if(!assetFacilityID) {
    				var locationID = parseLocationFacility(nr.getValue("custrecord_clgx_faq_tran_facility"));
    				var facilityID = lib.getFacilityByLocation(locationID);
    				
    				nr.setValue({ fieldId: "custrecord_clgx_faq_asset_facility", value: facilityID });
    			}
    			 
    			if(facilityEndDate) {
    				nr.setValue({ fieldId: "custrecord_clgx_faq_asset_lifetime", value: calculateAssetLifetime(depreciationStartDate, facilityEndDate) });
    			}
    		}
    		
    		nr.setValue({ fieldId: "custrecord_clgx_faq_department", value: lib.departmentMap(nr.getValue("custrecord_clgx_faq_department")) });
    	} catch(ex) {
    		log.error({ title: "beforeSubmit - Error", details: ex });
    	}
    }
    
    
    /**
     * Returns the asset lifetime in months from the depreciation start date to the facility lease end date.
     * 
     * @access private
     * @function calculateAssetLifetime
     * 
     * @param {string} startDate - Depreciation start date.
     * @param {string} endDate - Facility lease end date.
     */
    function calculateAssetLifetime(startDate, endDate) {
    	if(startDate && endDate) {
    		return Math.round(moment(endDate).diff(startDate, "months", true));
    	}
    }
    
    
    /**
     * Calculates the depreciation start date from the transaction date.
     * 
     * @access private
     * @function calculateDepreciationStartDate
     * 
     * @param {string} transactionDate
     * @return {string}
     */
    function calculateDepreciationStartDate(transactionDate) {
    	if(transactionDate) {
    		return moment(transactionDate).startOf("month").add(1, "month").format("M/D/YYYY");
    	}
    }
    
    
    /**
     * Returns the facility lease end date.
     * 
     * @access private
     * @function getFacilityEndDate
     * 
     * @param {number} facilityID
     * @return {string}
     */
    function getFacilityEndDate(facilityID) {
    	if(facilityID) {
    		var recordObject = record.load({ type: "customrecord_cologix_facility", id: facilityID });
    		return recordObject.getValue("custrecord_clgx_lease_end");
    	}
    }
    
    
    /**
     * Returns the asset type from the CIP item type.
     * 
     * @access private
     * @function getAssetType
     * 
     * @param {number} cipItemID
     * @return {number}
     */
    function getAssetType(cipItemID) {
    	if(cipItemID) {
    		var recordObject = record.load({ type: "customrecord_clgx_cip_item_type", id: cipItemID });
    		return parseInt(recordObject.getValue("custrecord_clgx_cip_asset_type"));
    	}
    }
    
    /**
     * Returns the facility name from the transaction location text.
     * 
     * @access public
     * @function parseLocationFacility
     * 
     * @param {string} locationName
     * @return {string}
     */
    function parseLocationFacility(locationName) {
    	if(locationName) {
    		var locationArray = locationName.split(":");
    		return (locationArray[locationArray.length - 1].trim());
    	}
    }
    
    
    return {
        beforeSubmit: beforeSubmit
    };
    
});
