/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/search", "N/record"],
function(search, record) {
    function execute(scriptContext) {
    	try {
    		removeDepreciationHistory();
    		removeFamValues();
    		clearQueueFamField();
    		removeFixedAsset();
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }

    
    function removeDepreciationHistory() {
    	var searchObject = search.load({ id: "customsearch_clgx_dep_hist_clean_up" });
    	searchObject.run().each(function(result) {
    		try {
    			record.delete({ type: "customrecord_ncfar_deprhistory", id: result.getValue("internalid") });
    		} catch(ex) {
    	   		log.error({ title: "removeDepreciationHistory - Error", details: ex });
    	   	}
    		
    		return true;
    	});
    }
    
    
    function removeFamValues() {
    	var searchObject = search.load({ id: "customsearch_clgx_asset_values_clean_up" });
    	searchObject.run().each(function(result) {
    		try {
    			record.delete({ type: "customrecord_fam_assetvalues", id: result.getValue("internalid") });
    		} catch(ex) {
    	   		log.error({ title: "removeFamValues - Error", details: ex });
    	   	}
    		
    		return true;
    	});
    }
    
    
    function clearQueueFamField() {
    	var searchObject = search.load({ id: "customsearch_clgx_assets_created_today" });
    	searchObject.run().each(function(result) {
    		try {
    			record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: result.getValue("internalid"), values: { custrecord_clgx_faq_fam_asset: "" } });
    		} catch(ex) {
    	   		log.error({ title: "clearQueueFamField - Error", details: ex });
    	   	}
    		
    		return true;
    	});
    }
    
    
    function removeFixedAsset() {
    	var searchObject = search.load({ id: "customsearch_clgx_assets_created_today_2" });
    	searchObject.run().each(function(result) {
    		try {
    			if(result.getValue("internalid")) {
    				record.delete({ type: "customrecord_ncfar_asset", id: result.getValue("internalid") });
    			}
    		} catch(ex) {
    	   		log.error({ title: "removeFixedAsset - Error", details: ex });
    	   	}
    		
    		return true;
    	});
    }
    
    return {
        execute: execute
    };
    
});
