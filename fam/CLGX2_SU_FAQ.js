/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/record", "/SuiteScripts/clgx/fam/lib/CLGX2_FAQ_Lib"],
function(record, lib) {
    function afterSubmit(scriptContext) {
    	var nr = scriptContext.newRecord;
    	
    	if(scriptContext.type === "create" || scriptContext.type === "edit") {
    		var errorMessage = "";
    		var hasError     = false;
    		
    		if(nr.getValue("custrecord_clgx_faq_cip_type") === null || nr.getValue("custrecord_clgx_faq_cip_type") === "") {
    			errorMessage += "Missing CIP Item Type\n";
    		} 
    		
    		if(nr.getValue("custrecord_clgx_faq_cip_type")) {
    			var recordObject = record.load({ type: "customrecord_clgx_cip_item_type", id: nr.getValue("custrecord_clgx_faq_cip_type") });
    			if(recordObject.getValue("isinactive")) {
    				errorMessage += "Selected CIP Item Type is Inactive\n";
    			}
    		}  
    		
    		if(nr.getValue("custrecord_clgx_faq_job") === null || nr.getValue("custrecord_clgx_faq_job") === "") {
    			errorMessage += "Missing Project\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_asset_type") === null || nr.getValue("custrecord_clgx_faq_asset_type") === "") {
    			errorMessage += "Missing Asset Type\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_oac") === null || nr.getValue("custrecord_clgx_faq_oac") === "") {
    			errorMessage += "Missing Original Asset Cost\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_accounting_method") === null || nr.getValue("custrecord_clgx_faq_accounting_method") === "") {
    			errorMessage += "Missing Accounting Method\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_asset_lifetime") === null || nr.getValue("custrecord_clgx_faq_asset_lifetime") === "") {
    			errorMessage += "Missing Asset Lifetime\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_cust_loc") === null || nr.getValue("custrecord_clgx_faq_cust_loc") === "") {
    			errorMessage += "Missing Customer Location\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_tran_location") === null || nr.getValue("custrecord_clgx_faq_tran_location") === "") {
    			errorMessage += "Missing Transaction Location\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_asset_facility") === null || nr.getValue("custrecord_clgx_faq_asset_facility") === "") {
    			errorMessage += "Missing Asset Factility\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_tran_subsidiary") === null || nr.getValue("custrecord_clgx_faq_tran_subsidiary") === "") {
    			errorMessage += "Missing Transaction Subsidiary\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_pur_date") === null || nr.getValue("custrecord_clgx_faq_pur_date") === "") {
    			errorMessage += "Missing Purchase Date\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_depreciation_start") === null || nr.getValue("custrecord_clgx_faq_depreciation_start") === "") {
    			errorMessage += "Missing Depreciation Start Date\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_dep_rules") === null || nr.getValue("custrecord_clgx_faq_dep_rules") === "") {
    			errorMessage += "Missing Depreciation Rules\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_tax_cat") === null || nr.getValue("custrecord_clgx_faq_tax_cat") === "") {
    			errorMessage += "Missing Tax Category\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_tax_cat")) {
    			var listObject = lib.getListItem({ type: "customlist_famassettaxcategory", id: nr.getValue("custrecord_clgx_faq_tax_cat") });
    			if(listObject.inactive !== undefined && listObject.inactive == true) {
    				errorMessage += "Selected Tax Cateogry is Inactive\n";
    			}
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_asset_account") === null || nr.getValue("custrecord_clgx_faq_asset_account") === "") {
    			errorMessage += "Missing Asset Account\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_tran_parent") === null || nr.getValue("custrecord_clgx_faq_tran_parent") === "") {
    			errorMessage += "Missing Parent Transaction\n";
    		}
    		
    		if(nr.getValue("custrecord_clgx_faq_tran_line") === null || nr.getValue("custrecord_clgx_faq_tran_line") === "") {
    			errorMessage += "Missing Originating Transaction Line\n";
    		}
    		
    		if(errorMessage && errorMessage.length > 0) {
    			hasError = true;
    		}
    		
    		record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: nr.id, values: { custrecord_clgx_faq_haserror: hasError, custrecord_clgx_faq_error_message: errorMessage } });
    		
    		//nr.setValue({ fieldId: "custrecord_clgx_faq_haserror",      value: ((errorMessage === "") ? false : true) });
        	//nr.setValue({ fieldId: "custrecord_clgx_faq_error_message", value: errorMessage });
    	}
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
