/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 3/7/2019
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/fam/lib/CLGX2_FAQ_Lib"],
function(record, search, lib) {
    function onRequest(context) {
    	try {
    		var ro = {};
        	ro.response   = context.response;
    		ro.request    = context.request;
    		ro.eventType  = ro.request.parameters.event    || null;
    		ro.actionType = ro.request.parameters.action   || null;
    		ro.data       = ro.request.parameters.data     || null;
    		ro.date       = ro.request.parameters.date     || null;
    		ro.tranid     = ro.request.parameters.tranid   || null;
    		ro.parentid   = ro.request.parameters.parentid || null;
    		ro.mergeids   = ro.request.parameters.mergeids || null;
    		
    		
    		if(ro.eventType == "get") {
    			if(ro.actionType == "unprocessedqueue") {
    				var recordArray = lib.getBufferedQueueRecords("customsearch_clgx_faq_unprocessed");
    				ro.response.addHeader({ name: "Content-Type", value: "application/json" });
    				ro.response.write(JSON.stringify(recordArray));
    			}
    			
    			if(ro.actionType == "readytoprocessqueue") {
    				var recordArray = lib.getBufferedQueueRecords("customsearch_clgx_faq_r2p");
    				ro.response.addHeader({ name: "Content-Type", value: "application/json" });
    				ro.response.write(JSON.stringify(recordArray));
    			}
    			
    			if(ro.actionType == "merge") {
    				var recordArray = lib.getBufferedQueueRecords("customsearch_clgx_faq_r2p", [
    					{ "name": "custrecord_clgx_faq_tran_parent", "operator": "anyof", "value": parseInt(ro.tranid) },
    					{ "name": "internalid", "operator": "noneof", "value": parseInt(ro.parentid) }
    				]);
    				
    				var recordObject = lib.getQueueRecord(ro.parentid);
    				recordObject     = lib.updateObjectProperty({ records: recordObject, property: "isparent", value: 1 }); //Set the parent record
    				recordArray      = lib.updateObjectProperty({ records: recordArray, property: "parentid", value: ro.parentid });
    				
    				recordArray.unshift(recordObject); //Insert static record at the beginning of the json array.
    				
    				ro.response.addHeader({ name: "Content-Type", value: "application/json" });
    				ro.response.write(JSON.stringify(recordArray));
    			}
    			
    			if(ro.actionType == "scriptsrunning") {
    				var populationRunning = lib.isScriptInstanceRunning("customsearch_clgx2_faq_script_running", [
    					{ "name": "name", "join": "script", "operator": "contains", "value": "CLGX2_SS_PopulateFamQueue" }
    				]);
    				var processingRunning = lib.isScriptInstanceRunning("customsearch_clgx2_faq_script_running", [
    					{ "name": "name", "join": "script", "operator": "contains", "value": "CLGX2_SS_ProcessFamQueue" }
        			]);
    				
    				ro.response.addHeader({ name: "Content-Type", value: "application/json" });
    				ro.response.write(JSON.stringify({ "population": populationRunning, "processing": processingRunning }));
    			}
    		}
    		
    		if(ro.eventType == "schedule") {
    			if(ro.actionType == "clear") {
    				lib.scheduleScript({ scriptId: "1839", params: { custscript_clgx2_1774_st: "delete", custscript_clgx2_1774_at: "delete" } });
    				ro.response.write("The queue has been scheduled to clear.");
    			}
    			
    			if(ro.actionType == "populate") {
    				lib.scheduleScript({ scriptId: "1839", params: { custscript_clgx2_1774_st: "reset", custscript_clgx2_1774_at: "delete" } });
    				ro.response.write("The queue has been scheduled to reset.");
    			}
    			
    			
    			if(ro.actionType == "generate") {
    				if(!ro.date || ro.date == 0 || ro.date == "Invalid date") {
    					ro.response.write("<b style=\"color: #FF0000;\">Missing Journal Entry date. Please enter a date and reschedule.</b>");
    				} else if(!ro.data || ro.data.length <= 0) {
    					ro.response.write("<b style=\"color: #FF0000;\">You must select at least one record in order to generate assets.</b>");
    				} else {
    					lib.scheduleScript({ scriptId: "1840", params: { custscript_clgx_1778_at: "fam", custscript_clgx_1778_jed: ro.date, custscript_clgx_1778_qids: ro.data } });
        				ro.response.write("Records have been scheduled to be generated.");
    				}
    			}
    			
    			if(ro.actionType == "merge") {
    				lib.scheduleScript({ scriptId: "1892", params: { custscript_clgx_1918_at: "merge", custscript_clgx_1918_pid: ro.parentid, custscript_clgx_1918_qids: ro.mergeids } });
    				ro.response.write("Records have been scheduled to be merged.");
    			}
    			
    			if(ro.actionType == "save") {
    				lib.scheduleScript({ scriptId: "1892", params: { custscript_clgx_1918_at: "save", custscript_clgx_1918_qids: ro.data } });
    				ro.response.write("Records have been scheduled to be saved.");
    			}
    			
    			if(ro.actionType == "remove") {
    				lib.scheduleScript({ scriptId: "1892", params: { custscript_clgx_1918_at: "remove", custscript_clgx_1918_qids: ro.data } });
    				ro.response.write("Records have been scheduled to be removed from the processing queue.");
    			}
    		}
    		
    		if(ro.eventType == "filter") {
    			if(ro.actionType == "project") {
    				var recordArray = lib.getBufferedQueueRecords("customsearch_clgx_faq_unprocessed");    				
    				var filteredProjects = lib.buildListFilter({ records: recordArray, dataIndex: "projectname" });
    				
    				ro.response.addHeader({ name: "Content-Type", value: "application/json" });
    				ro.response.write(JSON.stringify(filteredProjects));
    			}
    		}
    	} catch(ex) {
    		log.error({ title: "onRequest - Error", details: ex });
    	}
    }
    
    return {
        onRequest: onRequest
    };
    
});