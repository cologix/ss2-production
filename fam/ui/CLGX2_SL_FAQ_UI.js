/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 3/7/2019
 */
define(["N/file", "/SuiteScripts/clgx/fam/lib/CLGX2_FAQ_Lib"],
function(file, lib) {
    function onRequest(context) {
    	try {
    		var fileObject = file.load({ id: 19814242 });
			var fileHtml   = fileObject.getContents();
			
			var recordArray = lib.getBufferedQueueRecords("customsearch_clgx_faq_unprocessed");
			
			fileHtml = fileHtml.replace(new RegExp("{queuedata}", "g"), recordArray);
			fileHtml = fileHtml.replace(new RegExp("{statusdata}", "g"), lib.getBufferedQueueRecords("customsearch_clgx_faq_r2p"));
			fileHtml = fileHtml.replace(new RegExp("{isScriptRunning}", "g"), lib.isScriptInstanceRunning("customsearch_clgx2_faq_script_running"));
			
			context.response.write(fileHtml);
    	} catch(ex) {
    		log.error({ title: "onRequest - Error", detials: ex });
    	}
    }
    
    return {
        onRequest: onRequest
    };
    
});