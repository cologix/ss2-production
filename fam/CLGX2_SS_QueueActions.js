/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define([
	"N/search", 
	"N/record", 
	"N/runtime", 
	"/SuiteScripts/clgx/fam/lib/CLGX2_FAQ_Lib"
],
function(search, record, runtime, lib) {   
    function execute(scriptContext) {
    	try {
    		var rt = runtime.getCurrentScript();
        	
        	var paramObject           = new Object();
        	paramObject.savedSearchID = "";
        	paramObject.actionType    = rt.getParameter("custscript_clgx_1918_at");
        	paramObject.parentID      = rt.getParameter("custscript_clgx_1918_pid") || null;
        	paramObject.queueIDs      = rt.getParameter("custscript_clgx_1918_qids") || null;
        	
        	if(paramObject.actionType == "merge") {
        		mergeQueueRecords(paramObject);
        	}
        	
        	if(paramObject.actionType == "save") {
        		markQueueRecords(paramObject);
        	}
        	
        	if(paramObject.actionType == "remove") {
        		unmarkQueueRecords(paramObject);
        	}
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }

    
    /**
     * Merges queue records into one new record marked as ready for processing.
     * 
     * @access private
     * @function mergeQueueRecords
     * 
     * @param {Object} paramObject
     * @return undefined
     */
    function mergeQueueRecords(paramObject) {
    	if(paramObject) {
    		var parentRecord = paramObject.parentID;
    		var queueRecords = JSON.parse(paramObject.queueIDs);
    		
    		var additionalAssetCost = calculateAdditionalCost(parentRecord, queueRecords);
    		
    		var recordObject        = record.copy({ type: "customrecord_clgx_fam_ast_queue", id: parentRecord });
    		var currentAssetCost    = parseFloat(recordObject.getValue("custrecord_clgx_faq_oac"));
    		
    		recordObject.setValue({ fieldId: "custrecord_clgx_faq_oac", value: (currentAssetCost + additionalAssetCost) });
    		recordObject.setValue({ fieldId: "custrecord_clgx_faq_ready_to_process", value: true });
    		
    		var recordID = recordObject.save();
    		
    		updateMergedRecords({ newRecord: recordID, parent: parentRecord, children: queueRecords });
    	}
    } 
    
    
    /**
     * Returns the asset costs from the records being merged into the parent record.
     * 
     * @access private
     * @function calculateAdditionalCost
     * 
     * @param {Array} queueRecords
     * @return {Decimal}
     */
    function calculateAdditionalCost(parentID, queueRecords) {
    	if(queueRecords) {
    		var additionalAssetCost = 0;
    		var recordCount = queueRecords.length;
    		
    		for(var r = 0; r < recordCount; r++) {
    			var tmpField = search.lookupFields({ type: "customrecord_clgx_fam_ast_queue", id: queueRecords[r], columns: ["custrecord_clgx_faq_oac"] });
    			additionalAssetCost += parseFloat(tmpField.custrecord_clgx_faq_oac);
    		}
    		
    		return additionalAssetCost;
    	}
    }
    
    
    
    /**
     * Updates the merged queue records with the new record id.
     * 
     * @access private
     * @function updateMergedRecords
     * 
     * @param {Object} paramObject
     * @param {Number} paramObject.newRecord
     * @param {Number} paramObject.parent
     * @param {Array}  paramObject.children
     * 
     * @return undefined
     */
    function updateMergedRecords(paramObject) {
    	if(paramObject) {
    		var recordCount = paramObject.children.length;
    		
    		for(var r = 0; r < recordCount; r++) {
    			record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: paramObject.children[r], values: { custrecord_clgx_faq_ismerged: true, custrecord_clgx_faq_merged_into: paramObject.newRecord } });
    		}
    		
    		record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: paramObject.parent, values: { custrecord_clgx_faq_ismerged: true, custrecord_clgx_faq_merged_into: paramObject.newRecord } });
    	}
    }
    
    
    /**
     * Marks the FAM queue records as ready for processing.
     * 
     * @access private
     * @function markQueueRecords
     * 
     * @param {Object} paramObject
     * @return null
     */
    function markQueueRecords(paramObject) {
    	if(paramObject && paramObject.queueIDs !== undefined) {
    		var queueRecords = JSON.parse(paramObject.queueIDs);
    		var queueCount   = queueRecords.length;
    		
    		for(var q = 0; q < queueCount; q++) {
    			record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: queueRecords[q], values: { custrecord_clgx_faq_ready_to_process: true } });
    		}
    	}
    }
    
    
    /**
     * Un-marks the FAM queue records as ready for processing.
     * 
     * @access private
     * @function markQueueRecords
     * 
     * @param {Object} paramObject
     * @return null
     */
    function unmarkQueueRecords(paramObject) {
    	if(paramObject && paramObject.queueIDs !== undefined) {
    		var queueRecords = JSON.parse(paramObject.queueIDs);
    		var queueCount   = queueRecords.length;
    		
    		for(var q = 0; q < queueCount; q++) {
    			record.submitFields({ type: "customrecord_clgx_fam_ast_queue", id: queueRecords[q], values: { custrecord_clgx_faq_ready_to_process: false } });
    		}
    	}
    }
    
    return {
        execute: execute
    };
    
});
