/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   8/2/2017
 * @description Used to hide the "Summary" section of a transaction record. Universal to any transaction record.
 * 
 * Script Name  : CLGX2_SU_HideSummary
 * Script File  : CLGX2_SU_HideSummary.js
 * Script ID    : customscript_clgx2_su_hidesummary
 * Deployment ID: customdeploy_clgx2_su_hidesummary
 * Deployed To  : Proposal
 * Event Type   : Edit, View
 * 
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/ui/serverWidget"],
function(widget) {
	
	/**
	 * Before Load - Entry Point
	 *  
	 * @param {object} context
	 * @return null
	 */
    function beforeLoad(context) {
    	if(context.type == context.UserEventType.EDIT || context.type == context.UserEventType.VIEW) {
    		//FieldDisplayType: https://system.na2.netsuite.com/app/help/helpcenter.nl?fid=section_4332670964.html
    		
    		if(context.form.getField({id: "subtotal"}) != "" && context.form.getField({id: "subtotal"}) != null) {
    			context.form.getField({id: "subtotal"}).updateDisplayType({displayType: widget.FieldDisplayType.HIDDEN});
    		}
    		
    		if(context.form.getField({id: "discounttotal"}) != "" && context.form.getField({id: "discounttotal"}) != null) {
    			context.form.getField({id: "discounttotal"}).updateDisplayType({displayType: widget.FieldDisplayType.HIDDEN});
    		}
    		
    		if(context.form.getField({id: "taxtotal"}) != "" && context.form.getField({id: "taxtotal"}) != null) {
    			context.form.getField({id: "taxtotal"}).updateDisplayType({displayType: widget.FieldDisplayType.HIDDEN});
    		}
    		
    		if(context.form.getField({id: "tax2total"}) != "" && context.form.getField({id: "tax2total"}) != null) {
    			context.form.getField({id: "tax2total"}).updateDisplayType({displayType: widget.FieldDisplayType.HIDDEN});
    		}
    		
    		if(context.form.getField({id: "altshippingcost"}) != "" && context.form.getField({id: "altshippingcost"}) != null) {
    			context.form.getField({id: "altshippingcost"}).updateDisplayType({displayType: widget.FieldDisplayType.HIDDEN});
    		}
    		
    		if(context.form.getField({id: "total"}) != "" && context.form.getField({id: "total"}) != null) {
    			context.form.getField({id: "total"}).updateDisplayType({displayType: widget.FieldDisplayType.HIDDEN});
    		}
    		
    		if(context.form.getField({id: "altsalestotal"}) != "" && context.form.getField({id: "altsalestotal"}) != null) {
    			context.form.getField({id: "altsalestotal"}).updateDisplayType({displayType: widget.FieldDisplayType.HIDDEN});
    		}
    		
    		if(context.form.getField({id: "custbody_clgx_so_pm_flag"}) != "" && context.form.getField({id: "custbody_clgx_so_pm_flag"}) != null) {
    			context.form.getField({id: "custbody_clgx_so_pm_flag"}).updateDisplayType({displayType: widget.FieldDisplayType.INLINE});
    		}
    	}
    }

    return {
        beforeLoad: beforeLoad
    };
    
});
