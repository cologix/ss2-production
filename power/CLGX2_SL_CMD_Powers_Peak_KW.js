/**
 *
 * @author Catalina Taran - catalina.taran@cologix.com
 * @date   9/1/2017
 * @description
 *
 * Script Name  : CLGX2_SL_CMD_Powers_Peak_KW
 * Script File  : CLGX2_SL_CMD_Powers_Peak_KW.js
 * Script ID    : customscript_clgx2_sl_cmd_powers_peak_kw
 * Deployment ID: customdeploy_clgx2_sl_cmd_powers_peak_kw
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1451&deploy=1
 *
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
        "N/file",
        "N/task",
        "N/https",
        "N/record",
        "N/runtime",
        "N/search",
        "/SuiteScripts/clgx/libraries/lodash.min"
    ],
    function(file, task, https, record, runtime, search, _) {

        function onRequest(context) {

            var response = context.response;
            var request = context.request;
            log.debug({
                "title": "request",
                "details": request
            });
            var customerid = request.parameters.customerid;
            var facilityid = request.parameters.facilityid;
            var famid = request.parameters.famid;
            var allpwrs = request.parameters.allpwrs;
            var html = '';


            var arrPowers =  new Array();
            var arrServicesIDs = new Array();

          /*  if(customerid > 0){
                var results = search.load({ type: 'customrecord_clgx_power_circuit', id: "customsearch_clgx_dcim_pwrs_kw_se" });
                results.filters.push(search.createFilter({ name: "parent", join:"custrecord_cologix_power_service",operator: "ANYOF", values:customerid }));

                results.run().each(function(result) {
                    var serviceid = parseInt(result.getValue(result.columns[0]));
                    arrServicesIDs.push(serviceid);
                    _.uniq(arrServicesIDs);

                    return true;
                });
            }
*/
            if(customerid > 0 || famid > 0){

                var arrColumns = new Array();
                var arrFilters = new Array();
                if(allpwrs > 0){
                    var results = search.load({ type: 'customrecord_clgx_power_circuit', id: "customsearch_clgx_dcim_pwrs_kw_cust_all" });
                }
                else{
                    var results = search.load({ type: 'customrecord_clgx_power_circuit', id: "customsearch_clgx_dcim_pwrs_kw_cust_pnl" });
                }
                if(customerid > 0){
                    results.filters.push(search.createFilter({ name: "parent", join:"custrecord_cologix_power_service",operator: "ANYOF", values:customerid }));
             }
                if(famid > 0){

                    results.filters.push(search.createFilter({ name: "custrecord_clgx_power_panel_pdpm", operator: "ANYOF", values:famid }));
                }





                var arrResults=new Array();
                var resultSet = results.run();

                // now take the first portion of data.
                var currentRange = resultSet.getRange({
                    start : 0,
                    end : 1000
                });

                var i = 0;  // iterator for all search results
                var j = 0;  // iterator for current result range 0..999

                while ( j < currentRange.length ) {
                    var objPower = new Object();
                    // take the result row
                    var result = currentRange[j];
                    //var columns = result.getAllColumns();
                    objPower["facilityid"] = parseInt(result.getValue(result.columns[0]));
                   

                    arrPowers.push(objPower);

                    // finally:
                    i++; j++;
                    if( j==1000 ) {   // check if it reaches 1000
                        j=0;          // reset j an reload the next portion
                        currentRange = resultSet.getRange({
                            start : i,
                            end : i+1000
                        });
                    }
                }
            }

            var fileObj = file.load({
                id: 4495118
            });
            html = fileObj.getContents();
            html = html.replace(new RegExp('{powersJSON}','g'), JSON.stringify(arrPowers));
            response.write( html );

        }
        return {
            onRequest: onRequest
        };

        function round(value) {
            return Number(Math.round(value+'e'+2)+'e-'+2);
        }

    });
