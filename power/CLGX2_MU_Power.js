/**
 * @NApiVersion 2.x
 * @NScriptType MassUpdateScript
 * @NModuleScope SameAccount
 */
define(["N/record"],
function(record) {
    function each(params) {
    	try {
    		var rec = record.load({ type: params.type, id: params.id });
    		
    		var voltageString = rec.getText("custrecord_cologix_power_volts");
    		var voltageNumber = parseInt(voltageString.substr(0, 3).replace("V", ""));
			var amps          = parseInt(rec.getText("custrecord_cologix_power_amps").replace("A", ""));
			var kva           = 0.00;
			   
			switch(voltageString) {
			case "120V Single Phase":
			   kva = parseFloat((amps * 0.12).toFixed(2));
			   break;
			case "208V Single Phase":
			   kva = parseFloat((amps * 0.208).toFixed(2));
		       break;
		    case "208V Three Phase":
		       kva = parseFloat((amps * Math.sqrt(3) * 0.208).toFixed(2));
		       break;
		    case "240V Single Phase":
		       kva = parseFloat((amps * 0.24).toFixed(2));
		       break;
		    case "240V Three Phase":
		       kva = parseFloat((amps * Math.sqrt(3) * 0.415).toFixed(2));
		       break;
		    case "600V Single Phase":
		     	kva = parseFloat((amps * 0.347).tofixed(2));
		        break;
		    case "600V Three Phase":
		      	kva = parseFloat((amps * 0.347).toFixed(2));
		        break;
		    default:
		       	kva = parseFloat(((voltageNumber * amps) / 1000).toFixed(2));
			}
			
			var deviceId = rec.getValue("custrecord_clgx_dcim_device");
			
			if(deviceId) {
				var device = record.load({ type: "customrecord_clgx_dcim_devices", id: rec.getValue("custrecord_clgx_dcim_device") });
				var chains = device.getValue("custrecord_clgx_dcim_device_pwr_chains");
				if(chains) { rec.setValue({ fieldId: "custrecord_clgx_power_chain", value: chains }); }
			}
			
			rec.setValue({ fieldId: "custrecord_clgx_dcim_inst_kva_a", value: parseFloat((kva * 0.8).toFixed(2)) });
    		rec.save({ ignoreMandatoryFields: true, enableSourcing: false });
    	} catch(ex) {
    		log.error({ title: "Error on record: " + params.id, details: ex });
    	}
    }

    return {
        each: each
    };
    
});
