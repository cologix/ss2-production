/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType ClientScript
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/6/2018
 */
define(["/SuiteScripts/clgx/libraries/lodash.min.js"],
function(_) {
	function onSave(context) {
		var cr = context.currentRecord;
		
		var panel = cr.getValue("custrecord_clgx_power_panel_pdpm");
		if(panel == null || panel == "") { panel = -1; }

		var ups   = cr.getValue("custrecord_cologix_power_ups_rect");
		if(ups == null || ups == "") { ups = -2; }
		
		var gen1  = cr.getValue("custrecord_clgx_power_generator");
		if(gen1 == null || gen1 == "") { gen1 = -3; }
		
		var gen2  = cr.getValue("custrecord_clgx_custombody_gen2");
		if(gen2 == null || gen2 == "") { gen2 = -4; }
		
		var values = [panel, ups, gen1, gen2];
		 
		if(hasDuplicates(values)) {
			alert("Panel/UPS/Generator/Generator 2 contains duplicate FAM records.");
			return false;
		} else {
			return true;
		}
	}
	
	
	/**
	 * Returns whether or not an array has duplicates.
	 * 
	 * @access private
	 * @function hasDuplicates
	 * 
	 * @param {array} array
	 * @returns {boolean}
	 */
	function hasDuplicates(array) {
		return _.uniq(array).length !== array.length;
	}
	
    return {
    	saveRecord: onSave
    };
    
});
