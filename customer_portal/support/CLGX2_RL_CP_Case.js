/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 *
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   12/18/2017
 */

define(["N/record", "N/search", "N/format", "N/runtime", "N/https", "N/file", "N/task",
        "/SuiteScripts/clgx/customer_portal/CLGX2_LIB_CP_Session", "/SuiteScripts/clgx/libraries/CLGX2_LIB_Execute",
        "/SuiteScripts/clgx/libraries/moment.min", "/SuiteScripts/clgx/customer_portal/lib/CLGX2_LIB_CP_Core"],
    function(record, search, format, runtime, https, file, task, session, execute, moment, core) {
        function getContactInformation(contactID) {
            if(contactID != null) {
                return search.lookupFields({ type: search.Type.CONTACT, id: contactID, columns: ["entityid", "email"] });
            }

            return null;
        }


        /**
         * Triggered when the case is updated.
         *
         * @param {Integer} caseID
         * @param {Integer} contactID
         * @param {String} authorEmail
         * @param {Integer} companyID
         * @param {String} companyEmail
         * @param {String} subject
         *
         * @return {Void}
         */
        function processCaseMessageUpdate(caseID, fileID, contactID, authorEmail, companyID, companyEmail, subject, message) {
            if(caseID != null) {
                var caseObj     = new Object();
                caseObj.id      = caseID;
                caseObj.contact = contactID;
                caseObj.email   = authorEmail;
                caseObj.company = companyID;
                caseObj.email   = companyEmail;
                caseObj.emailed = false;
                caseObj.subject = subject;
                caseObj.message = message;

                core.createCaseMessage(caseObj, fileID);
            }
        }


        /**
         * This method is used when cases are updated.
         */
        var onPost = session.authenticate(function onPost(data, returnObj, srights, companyID, contactID, modifierID, radix, sid) {
            //log.debug({ title: "data", details: data });
            //log.debug({ title: "returnObj",  details: returnObj });
            //log.debug({ title: "srights",    details: srights });
            //log.debug({ title: "companyID",  details: companyID });
            //log.debug({ title: "contactID",  details: contactID });
            //log.debug({ title: "modifierID", details: modifierID });
            //log.debug({ title: "radix",      details: radix });
            //log.debug({ title: "sid",        details: sid });

            var obj = new Object();

            if(srights.cases > 0 || srights.casesfin > 1) {
                var rid            = data.rid            || "0";
                var message        = data.message        || "";
                var close          = data.close          || false;
                var type           = data.type           || "";
                var caseid         = parseInt(rid, radix);
                var hasattachment  = data.hasattachment  || "0";
                var file           = data.file           || "";
                var filename       = data.filename       || "";
                var contact        = getContactInformation(contactID);

                if(message || close == "on") {
                    var caseObj = record.load({ type: "supportcase", id: caseid });

                    if(close == "on") {
                        caseObj.setValue({ fieldId: "status", value: "5" });
                    }

                    if(message != "") {
                        try {
                            var fileID         = null;
                            obj.fileDownloaded = 0;

                            if(hasattachment == "1" && file != null) {
                                fileID = core.downloadCaseAttachment(filename, file, sid, contactID, companyID);

                                if(fileID != null) {
                                    obj.fileDownloaded = 1;
                                }
                            }
                            var subject=message.substring(0,20);
                            message += '\n\nUpdated By: ' + contact.entityid;
                            log.debug({ title: "message_update", details: message });
                            processCaseMessageUpdate(caseid, fileID, contactID, contact.email, companyID, contact.email, subject, message);
                        } catch(ex) {
                            log.error({ title: "Case Message", details: ex });
                        }

                        //caseObj.setValue({ fieldId: "messagenew", value: true });
                        //caseObj.setValue({ fieldId: "incomingmessage", value: message + "\n\nUpdated By: " + contact.entityid });
                    }

                    caseObj.save({ enableSourcing: true, ignoreMandatoryFields: true });
                }

                var caseObj   = record.load({ type: "supportcase", id: caseid });
                obj.id        = caseid;
                obj.rid       = rid;
                obj.type      = type;
                obj.number    = caseObj.getValue("casenumber");
                obj.closed    = caseObj.getValue("enddate");
                obj.created   = caseObj.getValue("createddate");
                
                var scheduledDate = caseObj.getValue("custevent_cologix_case_sched_followup");
                if(!scheduledDate) { scheduledDate = ""; }
                
                var scheduledTime = caseObj.getValue("custevent_cologix_sched_start_time");
                if(!scheduledTime) { scheduledTime = ""; }
                
                obj.scheduled = scheduledDate + " " + scheduledTime;

                var createdby = caseObj.getText("contact").split(":") || "";
                obj.createdby = createdby[createdby.length - 1];

                obj.category  = caseObj.getText("category");
                obj.subject   = caseObj.getValue("title");
                obj.facility  = caseObj.getText("custevent_cologix_facility");
                obj.priority  = caseObj.getText("priority");

                var comments = [];
                var results  = search.load({ type: "message", id: "customsearch_clgx_cp_case_messages" });
                results.filters.push(search.createFilter({ name: "internalid", join: "case", operator: "anyof", values: caseid }));

                results.run().each(function(result) {
                    var comment       = {};
                    var id            = parseInt(result.getValue("internalid"));
                    comment.id        = id;
                    comment.ris       = id.toString(radix);
                    comment.created   = result.getValue("messagedate");
                    comment.author    = result.getValue("author");
                    comment.recipient = result.getValue("recipient");
                    comment.subject   = result.getValue("subject");
                    comments.push(comment);

                    return true;
                });

                obj.comments = comments;

                var now = moment().format("M/D/YYYY h:mm:ss a");
                record.submitFields({ type: "customrecord_clgx_api_cust_portal_logins", id: sid, values: { custrecord_clgx_api_cust_portal_last: now } });

                obj.error = "F";
                obj.code  = "SUCCESS";
                obj.msg   = "You are managing cases.";
            } else {
                obj.error = "T";
                obj.code  = "NO_RIGHTS_CASES";
                obj.msg   = "No rights for cases.";
            }
log.debug({ title: "Return OBJ", details: JSON.stringify(obj) });
            return obj;
        });


        /**
         * This method is used when cases are created.
         */
        var onPut = session.authenticate(function onPut(data, returnObj, srights, companyID, contactID, modifierID, radix, sid) {
            var obj = new Object();

            log.debug({ title: "pup", details: data });
            log.debug({ title: "rcases", details: srights.cases });
            log.debug({ title: "rcasesfin", details: srights.casefin  });

            if(srights.cases > 0 || srights.casesfin > 0 || srights.security > 0) {
                var subject        = data.subject        || "";
                var priority       = data.priority       || "3";
                var type           = data.type           || "1";
                var request_type   = data.request_type   || "1";
                var requesteddate  = data.requested      || "";
                var requestedtime  = data.requestedtime  || "";
                var requesteddate1 = data.requested1     || "";
                var requestedtime1 = data.requestedtime1 || "";
                var facility       = data.facility       || "";
                var message        = data.message        || "";
                var rectype        = data.rectype        || "0";
                var recid          = data.recid          || "0";
                var recname        = data.recname        || "";
                var so             = data.so             || "";
                var userid         = data.userid         || "";
                var sr             = data.sr             || "0";
                var hasattachment  = data.hasattachment  || "0";
                var file           = data.file           || "";
                var filename       = data.filename       || "";
                var lang           = data.lanportal      || "en";
                var contact        = getContactInformation(contactID);

                var redirect        = "";
                var date            = new Date();
                var finalJsonObject = new Array();

                var queue = record.create({ type: "customrecord_clgx_global_queue" });
                queue.setValue({ fieldId: "custrecord_clgx_gq_uid", value: runtime.getCurrentUser().id });
                queue.setValue({ fieldId: "custrecord_clgx_gq_rt",  value: "supportcase" });
                queue.setValue({ fieldId: "custrecord_clgx_gq_cid",  value: companyID });

                if(sr != 0) {
                    var messageComplete = "Requester - " + contact.entityid + "\n";
                    messageComplete += message;

                    redirect = "admin/users/";
                    if(srights.cases > 0 || srights.casesfin > 0) {
                        redirect = "support/cases/?r=1";
                    }

                    finalJsonObject.push({
                        "title"                                : subject,
                        "company"                              : companyID,
                        "contact"                              : contactID,
                        "email"                                : contact.email,
                        "custevent_cologix_case_sched_followup": requesteddate,
                        "custevent_cologix_sched_start_time"   : requestedtime,
                        "custevent_clgx_case_end_date"         : requesteddate1,
                        "custevent_clgx_case_end_time"         : requestedtime1,
                        "incomingmessage"                      : messageComplete,
                        "custevent_cologix_facility"           : "",
                        "category"                             : "1",
                        "custevent_cologix_sub_case_type"      : "",
                        "assigned"                             : "946879",
                        "priority"                             : priority,
                        "origin"                               : "Web",
                        "lang"                                 : lang,
                        "custevent_clgx_case_request_type"     : request_type,
                        "hasattachment"                        : hasattachment,
                        //"file"                                 : file,
                        "filename"                             : filename
                    });

                } else {
                    var recordid = parseInt(recid, radix);
                    var soid     = -1;

                    var category = "";
                    var casetype = "";
                    var assigned = "";

                    if (so != "") {
                        if (rectype != 3) { soid = parseInt(so, radix); } else { soid = so; }
                    } else {
                        soid = "";
                    }

                    if (type == 1) {
                        category = 1;
                        casetype = 5;
                        assigned = 379486;
                        redirect = "cases";
                    } else if (type == 2) {
                        category = 1;
                        casetype = 6;
                        assigned = 379486;
                        redirect = "cases";
                    } else if (type == 3) {
                        category = 11;
                        assigned = 12399;
                        redirect = "casesfin";
                    } else if (type == 4) {
                        category = 3;
                        casetype = 86;
                        assigned = 379486;
                        redirect = "cases";
                    } else if(soid != "") {
                        casetype = 50;
                    }

                    finalJsonObject.push({
                        "title"                                : subject,
                        "company"                              : companyID,
                        "contact"                              : contactID,
                        "email"                                : contact.email,
                        "custevent2"                           : soid,
                        "custevent_cologix_case_sched_followup": requesteddate,
                        "custevent_cologix_sched_start_time"   : requestedtime,
                        "incomingmessage"                      : message,
                        "custevent_clgx_case_cons_inv"         : (rectype == 1) ? recordid : "",
                        "custevent2"                           : (rectype == 2) ? recordid : "",
                        "custevent_power_circuit"              : (rectype == 3) ? recordid : "",
                        "custevent_cologix_space"              : (rectype == 4) ? recordid : "",
                        "custevent_cologix_cross_connect"      : (rectype == 5) ? recordid : "",
                        "custevent_cologix_facility"           : facility,
                        "category"                             : category,
                        "custevent_cologix_sub_case_type"      : casetype,
                        "assigned"                             : assigned,
                        "priority"                             : priority,
                        "origin"                               : "Web",
                        "lang"                                 : lang,
                        "company"                              : companyID,
                        "hasattachment"                        : hasattachment,
                        //"file"                                 : file,
                        "filename"                             : filename,
                        "startdate"                            : format.format({ value: date, type: format.Type.DATE }),
                        "starttime"                            : format.format({ value: date, type: format.Type.TIMEOFDAY })
                    });
                }

                queue.setValue({ fieldId: "custrecord_clgx_gq_json", value: JSON.stringify(finalJsonObject) });

                try {
                    recordid = queue.save();
                    obj.fileDownloaded = 0;

                    //If the case has an attachment, download it and attach it to the record.
                    if(hasattachment == "1" && file != "") {
                        var attachment = core.downloadCaseAttachment(filename, file, recordid, contactID, companyID);
                        record.submitFields({ type: "customrecord_clgx_global_queue", id: recordid, values: {custrecord_clgx_gq_fileid: attachment} });
                        obj.fileDownloaded = 1;
                    }

                    var sc          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                    sc.scriptId     = 1422;
                    sc.params       = { custscript_clgx2_dw_cqid: recordid };
                    sc.submit();
                } catch(ex) {
                    obj.error = "T";
                    obj.code  = "ERROR";
                    obj.msg   = "Case record was not created. Reason: " + ex.message;

                    log.error({ title: "Case Create", details: ex });
                }

                //Respond to the portal server.
                obj.error    = "F";
                obj.code     = "SUCCESS";
                obj.msg      = "A new case has been added to the queue.";
                obj.id       = recordid;
                obj.rid      = recordid;
                obj.redirect = redirect;
            } else {
                obj.error = "T";
                obj.code  = "NO_RIGHTS_CASES";
                obj.msg   = "No rights for cases.";
            }
log.debug({ title: "Return OBJ", details: JSON.stringify(obj) });
            return obj;
        });

        return {
            post: onPost,
            put: onPut
        };
    });