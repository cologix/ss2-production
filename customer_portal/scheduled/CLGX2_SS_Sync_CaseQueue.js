/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 *
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   12/18/2017
 */
define(["N/record","N/email", "N/search", "N/runtime", "N/format", "/SuiteScripts/clgx/customer_portal/lib/CLGX2_LIB_CP_Core"],
    function(record ,email, search, runtime, format, core) {
        /**
         * Updates a queue record once it has been processed.
         *
         * @param {Integer} recordID
         * @return {Void}
         */
        function updateRecordFlag(recordID, caseID) {
            if(recordID != null) {
                log.debug({ title: "updateRecordFlag - recordID", details: recordID });

                record.submitFields({ type: "customrecord_clgx_global_queue", id: recordID, values: {custrecord_clgx_gq_processed: true, custrecord_clgx_gq_caseid: caseID} });
            }
        }


        /**
         * Creates a support case.
         *
         * @param {Object} rt - Runtime object.
         * @param {Object} json - JSON object from the queue record.
         *
         * @return {Integer}
         */
        function createSupportCase(rt, json) {
            if(rt != null && json != null) {
                var supportcase = record.create({ type: "supportcase" });
                supportcase.setValue({ fieldId: "title",                                 value: json[0].title });
                supportcase.setValue({ fieldId: "company",                               value: json[0].company });
                supportcase.setValue({ fieldId: "contact",                               value: json[0].contact });
                supportcase.setValue({ fieldId: "email",                                 value: json[0].email });
                supportcase.setValue({ fieldId: "custevent2",                            value: json[0].custevent2 });

                if(json[0].custevent_cologix_case_sched_followup != null && json[0].custevent_cologix_case_sched_followup != "") {
                    supportcase.setValue({ fieldId: "custevent_cologix_case_sched_followup", value: format.parse({ value: json[0].custevent_cologix_case_sched_followup, type: format.Type.DATE }) });
                }

                if(json[0].custevent_cologix_sched_start_time != null && json[0].custevent_cologix_sched_start_time != "") {
                    supportcase.setValue({ fieldId: "custevent_cologix_sched_start_time",    value: format.parse({ value: json[0].custevent_cologix_sched_start_time, type: format.Type.TIMEOFDAY }) });
                }

                supportcase.setValue({ fieldId: "custevent_clgx_case_cons_inv",          value: json[0].custevent_clgx_case_cons_inv });
                supportcase.setValue({ fieldId: "custevent_power_circuit",               value: json[0].custevent_power_circuit });
                supportcase.setValue({ fieldId: "custevent_cologix_space",               value: json[0].custevent_cologix_space });
                supportcase.setValue({ fieldId: "custevent_cologix_cross_connect",       value: json[0].custevent_cologix_cross_connect });
                supportcase.setValue({ fieldId: "custevent_cologix_facility",            value: json[0].custevent_cologix_facility });
                supportcase.setValue({ fieldId: "category",                              value: json[0].category });
                supportcase.setValue({ fieldId: "custevent_cologix_sub_case_type",       value: json[0].custevent_cologix_sub_case_type });
                supportcase.setValue({ fieldId: "assigned",                              value: json[0].assigned });
                supportcase.setValue({ fieldId: "priority",                              value: json[0].priority });
                supportcase.setValue({ fieldId: "origin",                                value: "-5" });

                var caseID = supportcase.save({ enableSourcing: true, ignoreMandatoryFields: true });
                
                //record.submitFields({ type: "customrecord_clgx_package", id: json[0].package, values: { custrecord_clgx_package_case: caseID } });
                
                var objRecord = record.load({
                    type: "supportcase",
                    id: caseID
                });
                var casenumber = objRecord.getValue({
                    fieldId: 'casenumber'
                });
                var lang=json[0].lang;
                var emailSubject='Case #'+casenumber+' : '+json[0].title;
                var portalLink='<a href="https://my.cologix.com">my.cologix.com</a>';
                var emailBody='Case #'+casenumber+' has been created per your request.  A member of the Cologix team will be reaching out soon to follow up on your request.\n' +
                    '\n' +
                    'This email is not monitored. If you need to check the status or add an update, please log into our Customer Portal, my.cologix.com or call our Live Support Team at +1.855.449.4357.\n' +
                    '\n' +
                    'Thank you for contacting Cologix, Inc.';
                if(lang!='en'){
                    var emailSubject='Cas #'+casenumber+' : '+json[0].title;
                    var emailBody='Cas #'+casenumber+' a &#233;t&#233; cr&#233;&#233; selon votre demande. Un membre de l&#39;&#233;quipe Cologix vous contactera bient&#244;t pour discuter votre demande.\n'+
                        '\n' +
                        'Ce courriel n&#39;est pas surveill&#233;. Si vous voulez v&#233;rifier le statut ou ajouter une mise &#224; jour, veuillez vous connectez &#224; notre portail client(my.cologix.com) ou appelez notre &#233;quipe Live Support au +1(855) 449-4357 .'+
                        '\n' +
                        'Merci d&#39;avoir contact&#233; Cologix, Inc.';
                }
                var senderId = 432742;
                email.send({
                    author: senderId,
                    recipients: json[0].contact,
                    subject: emailSubject,
                    body: emailBody,
                    relatedRecords: {
                        entityId: json[0].contact

                    }
                });
                return {
                    "id": caseID,
                    "company": json[0].company,
                    "contact": json[0].contact,
                    "email"  : json[0].email,
                    "subject": json[0].title,
                    "message": json[0].incomingmessage
                };
            }
        }


        /**
         * Definition of the Scheduled script trigger point.
         *
         * @param {Object} scriptContext
         * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
         * @Since 2015.2
         */
        function execute(scriptContext) {
        	try {
                var srt     = runtime.getCurrentScript();
                var queueID = srt.getParameter("custscript_clgx2_dw_cqid");
                
                var results = search.load({ type: "customrecord_clgx_global_queue", id: "customsearch_clgx_global_queue" });
                
                if(queueID != null && queueID != "") {
                    results.filters.push(search.createFilter({ name: "internalid",                   operator: "is", values: queueID }))
                } 
                
                results.filters.push(search.createFilter({ name: "custrecord_clgx_gq_rt",        operator: "is", values: "supportcase" }));
                results.filters.push(search.createFilter({ name: "custrecord_clgx_gq_processed", operator: "is", values: false }));
                results.filters.push(search.createFilter({ name: "custrecord_clgx_gq_pid",       operator: "isempty", values: [] }));
                
                results.run().each(function(result) {
                	var qid  = result.getValue("id");
                    var fid  = result.getValue("custrecord_clgx_gq_fileid");
                    var json = JSON.parse(result.getValue("custrecord_clgx_gq_json"));
                    
                    var caseObj = createSupportCase(srt, json);
                    var msgID   = core.createCaseMessage(caseObj, fid);

                    log.debug({ title: "execute - caseObj", details: "Case: " + JSON.stringify(caseObj) + " | Msg: " +  msgID});

                    updateRecordFlag(qid, caseObj.id);
               	
               	    return true;
               });
            } catch(ex) {
                log.error({ title: "CLGX2_SS_Sync_CaseQueue Error", details: ex });
            }
        }

        return {
            execute: execute
        };

    });