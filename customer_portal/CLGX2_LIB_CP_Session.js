/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   12/14/2017
 */

define(["N/search", "/SuiteScripts/clgx/libraries/lodash.min"], 
function(search, _) {
	function authenticate(func) {
		return function (data) {
			var returnObj    = new Object();
			returnObj.error  = "F";
			returnObj.code   = "";
			returnObj.msg    = "";
			returnObj.me     = {};
			returnObj.rights = 0;
			
			try {
				var nsid  = data.nsid;
				var cfid  = data.cfid;
				var ip    = data.ip;
				var radix = data.radix;
				var mrid  = data.mrid;
				var crid  = data.crid;
				var erid  = data.erid;

				if(nsid && cfid && ip && radix && mrid && crid && erid) {
					var modifierID = parseInt(mrid, radix);
					var contactID  = parseInt(crid, radix);
					var companyID  = parseInt(erid, radix);
					
					var columns = new Array();
					var filters = new Array();
					
					var sessionSearch = search.load({ id: "customsearch_clgx_cp_sesssion_check" });
					
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_modifier", operator: "anyof", values: modifierID}));
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_contact", operator: "anyof",  values: contactID}));
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_entity", operator: "anyof",   values: companyID}));
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_uuid", operator: "is",        values: nsid}));
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_cfid", operator: "is",        values: cfid}));
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_ip", operator: "is",          values: ip}));
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_radix", operator: "equalto",  values: radix}));
					sessionSearch.filters.push(search.createFilter({ name: "custrecord_clgx_api_cust_portal_status", operator: "anyof",   values: 1}));

					sessionSearch.run().each(function(result) {
						if(result) {
							var sid        = parseInt(result.getValue("internalid"));
							var sadmin     = parseInt(result.getValue("custrecord_clgx_api_cust_portal_sadmin"));
							var scompanies = JSON.parse(result.getValue("custrecord_clgx_api_cust_portal_entities"));
							var scompany   = _.find(scompanies, function(arr) { return (arr.id == companyID); });
							var srights    = JSON.parse(result.getValue("custrecord_clgx_api_cust_portal_rights"));
							
							if(scompany || sadmin) {
								returnObj = func(data, returnObj, srights, companyID, contactID, modifierID, radix, sid);
							} else {
								returnObj.error = "T";
								returnObj.code  = "NO_RIGHTS_COMPANY";
								returnObj.msg   = "No rights for this company.";
							}
						} else {
							returnObj.error = "T";
							returnObj.code  = "NO_SESSION";
							returnObj.msg   = "Session request not valid.";
						}
					});
				} else {
					returnObj.error = "T";
					returnObj.code  = "MISS_ARGS";
					returnObj.msg   = "Missing arguments.";
				}
			} catch(ex) {
				returnObj.error = "T";
				returnObj.code  = ex.name;
				returnObj.msg   = ex.message;
				throw ex;
			}
			
			return returnObj;
		};
	}
	
	return {
		authenticate: authenticate
	};
});