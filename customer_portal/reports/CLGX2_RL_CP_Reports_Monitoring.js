/**
 * 
 * @author Liz Soucy- liz.soucy@cologix.com Script Name :
 *         CLGX2_RL_CP_Reports_Monitoring Script ID
 *         :customscript_clgx2_rl_cp_reports_monitoring Deployment
 *         ID:customdeploy_clgx2_rl_cp_reports_monitor Internal URL
 *         :/app/site/hosting/restlet.nl?script=1856&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

define(
		[ "N/file", "N/https", "N/record", "N/task", "N/runtime", "N/search",
				"N/task", "/SuiteScripts/clgx/libraries/lodash.min",
				"/SuiteScripts/clgx/libraries/moment.min",
				"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX" ],
		function(file, https, record, task, runtime, search, task, _, moment,
				xcex) {

			function doPost(object) {

				var chart = object.chart;
				var reportid = object.id;
				var reportType = object.type;
				var powers = [];
				var ids = [];
				var monitorData = search.load({
					type : 'customrecord_clgx_monitoring_device',
					id : "customsearch_clgx2_monitoring_devices"
				});

				monitorData.filters.push(search.createFilter({
					name : "custrecord_clgx_md_report",
					operator : search.Operator.ANYOF,
					values : reportid
				}));

				monitorData.run().each(
						function(result) {
							ids.push({
								"device_id" : parseInt(result
										.getValue(result.columns[4]))
							});
							return true;
						});

				ids = _.uniq(_.map(ids, 'device_id'));

				if (reportType == "Power") {
					var pwr_results = search
							.create({
								type : "customrecord_clgx_power_circuit",
								filters : [
										[ "custrecord_clgx_dcim_device",
												"anyof", ids ], "and",
										[ "isinactive", "is", "F" ] ],
								columns : [ {
									"name" : "internalid",
									"summary" : "GROUP"
								}, {
									"name" : "name",
									"summary" : "GROUP"
								}, {
									"name" : "custrecord_clgx_dcim_device",
									"summary" : "GROUP"
								} ]
							});
					pwr_results.run().each(function(result) {
						powers.push({
							"power_id" : parseInt(result.getValue({
								name : "internalid",
								summary : "GROUP"
							})),
							"power" : result.getValue({
								name : "name",
								summary : "GROUP"
							}),
							"device_id" : parseInt(result.getValue({
								name : "custrecord_clgx_dcim_device",
								summary : "GROUP"
							})),
							"device" : result.getText({
								name : "custrecord_clgx_dcim_device",
								summary : "GROUP"
							})
						});
						return true;
					});
				}
				var modius_alarms_points = get_all_modius_alarms_points();

				var modius = [];
				var points = [];
				monitorData.run().each(
						function(result) {
							var point_id = parseInt(result
									.getValue(result.columns[0]));
							var modius_id = parseInt(result
									.getValue(result.columns[1]));
							var alarm = 0;
							if (_.indexOf(modius_alarms_points, modius_id) > -1) 
								alarm = 1;
							if (result.getText(result.columns[1])) {
								modius.push(modius_id);
							}
							points.push({
								"point_id" : point_id,
								"modius_id" : modius_id,
								"point" : result.getValue(result.columns[2]),
								"alarm" : alarm,
								"value" : 0,
								"time" : "",
								"device_id" : parseInt(result
										.getValue(result.columns[4])),
								"units" : result.getText(result.columns[3])
							});
							return true;
						});

				modius = modius.filter(function(modiusPoint) {
					return modiusPoint != null;
				});
	
				//COMMENT OUT WHILE TESTING MODIUS SQL LATCHES
				var modius_alarm_point_kpis = https
				.get({
					url : 'https://lucee-nnj3.dev.nac.net/odins/devices/get_customer_report_points_kpi.cfm?ids='
							+ modius.join()
				});
				var modiusPointDataKPIs = JSON.parse(modius_alarm_point_kpis.body);
				
	 		// set alarms based on child KPI alarm points
				for (var i = 0; i < points.length; i++) {
					var obj = _.find(modiusPointDataKPIs, function(pointData) {
						return (pointData.ID == points[i].modius_id);
					});
					if (obj) {
						if (_.indexOf(modius_alarms_points, obj.KPI) > -1) {
							points[i].alarm = 1;
						}
					}
				}			

				var req = https
						.get({
							url : 'https://lucee-nnj3.dev.nac.net/odins/devices/get_customer_report_points.cfm?ids='
									+ modius.join()
						});

				var modiusPointData = JSON.parse(req.body);

				// set current values to display in report for MODIUS POINTS
				for (var i = 0; i < points.length; i++) {
					var obj = _.find(modiusPointData, function(pointData) {
						return (pointData.ID == points[i].modius_id);
					});
					if (obj) {
						points[i].value = obj.VAL;
						points[i].time = obj.DTS;
					}
				}

				var devices = [];
				monitorData.run().each(
						function(result) {
							var device_id = parseInt(result
									.getValue(result.columns[5]));
							var arr = _.filter(powers, function(o) {
								return o.device_id == device_id;
							});
							var pwrs = _.map(arr, function(obj) {
								return _.omit(obj, [ 'device_id', 'device' ]);
							});
							var arr = _.filter(points, function(o) {
								return o.device_id == device_id;
							});
							var pnts = _.map(arr, function(obj) {
								return _.omit(obj, [ 'device_id' ]);
							});
							pnts = _.orderBy(pnts, [ 'point' ], [ 'asc' ]);
							devices.push({
								"device_id" : device_id,
								"device" : result.getValue(result.columns[10]),
								"modius_id" : parseInt(result
										.getValue(result.columns[9])),
								"parent_id" : parseInt(result
										.getValue(result.columns[6])),
								"parent" : result.getText(result.columns[6]),
								"category" : result.getText(result.columns[7]),
								"capacity" : parseInt(result
										.getValue(result.columns[8])),
								"alarm" : _.sumBy(pnts, function(o) {
									return o.alarm;
								}),
								"points" : pnts,
								"powers" : pwrs
							});
							return true;
						});

				// FORMAT DATA TO DISPLAY AS GOOGLE CHARTS: ORG CHART
				if (chart > 0) {

					var arr = [];
					for (var i = 0; i < devices.length; i++) {
						var obj_html = get_node_body(devices[i]);
						arr.push([ {
							v : devices[i].device,
							f : obj_html
						}, devices[i].parent, devices[i].category ]);
					}

					// 21009160 IS FILE: CLGX_HTML_GoogleOrgChart.html
					var fileObj = file.load({
						id : 21009160
					});
					var html = fileObj.getContents();
					html = html.replace(new RegExp('{dataChart}', 'g'), JSON
							.stringify(arr));
					return html;
				} else {
					return JSON.stringify(devices, null, 4);
				}
			}

			function get_all_modius_alarms_points() {
				var req = https
						.get({
							url : 'https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms.cfm'
						});
				var od = JSON.parse(req.body);
				var od_points = _.uniq(_.map(od, 'POINT_ID'));
				return od_points;
			}

			function get_node_body(objDevice) {
				var html = '';
				if (objDevice.alarm > 0) {
					html += '<table align="center" width="100%" class="alarm">';
				} else {
					html += '<table align="center" width="100%" class="myFormat">';
				}
				html += '<tr class="bb2 ac hd0">';
				html += '<td colspan="2" nowrap>' + objDevice.device + '</td>';

				html += '</tr><tr class="bb">';
				html += '<td class="ar" colspan="2" nowrap>'
						+ objDevice.category + '</td>';
				html += '</tr>';
				if (objDevice.capacity > 0) {
					html += '<tr class="bb hd3"></tr>';
					html += '<tr class="bb hd0">';
					html += '<td nowrap class="al" align="left">Real Power Derated Capacity</td>';
					html += '<td nowrap class="al" align="right">'
							+ objDevice.capacity + '</td>';
					html += '</tr>';
				}

				if (objDevice.points.length > 0
						&& objDevice.points[0].point.length > 0) {
					html += '<tr class="bb hd3"></tr>';
					html += '<tr class="bb hd0">';
					html += '<td nowrap class="al" align="left">Points ('
							+ objDevice.points.length + ')</td>';
					html += '<td nowrap class="al" align="right">Value</td>';
					html += '</tr>';
					for (var i = 0; objDevice.points != null
							&& i < objDevice.points.length; i++) {
						html += '<tr>';
						if (objDevice.points[i].alarm == 0) {
							html += '<td nowrap class="al" align="left"><span class="hd2">'
									+ objDevice.points[i].point
									+ '</span></td>';
						} else {
							html += '<td nowrap class="al" align="left"><span class="hd3">'
									+ objDevice.points[i].point
									+ '</span></td>';
						}
						html += '<td nowrap class="al" align="right">'
								+ objDevice.points[i].value + ' '
								+ objDevice.points[i].units + '</td>';
						html += '</tr>';
					}
				}

				if (objDevice.powers.length > 0) {
					html += '<tr class="bb hd3"></tr>';
					html += '<tr class="bb hd0">';
					html += '<td nowrap class="al" align="left">Powers ('
							+ objDevice.powers.length + ')</td>';
					html += '</tr>';
					for (var i = 0; objDevice.powers != null
							&& i < objDevice.powers.length; i++) {
						html += '<tr>';
						html += '<td nowrap class="al" align="left"><span class="hd1">'
								+ objDevice.powers[i].power + '</span></td>';
						html += '<td nowrap class="al" align="right"></td>';
						html += '</tr>';
					}
				}

				html += '</table>';

				return html;
			}

			return {
				'post' : doPost
			};
		});