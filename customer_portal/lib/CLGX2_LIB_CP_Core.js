/**
 * @NApiVersion 2.x
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   12/18/2017
 */
define(["N/file", "N/record"],
function(file, record) {
	/**
	 * Returns the file extension for the file being downloaded to the file cabinet.
	 * 
	 * @param {String} fileName
	 */
	function getFileExtension(fileName) {
		if(fileName != null) {
			var nameObj = fileName.split(".");
			var objLength = nameObj.length;
			return ("." + nameObj[objLength - 1]);
		}
	}
	
	/**
	 * Returns the NetSuite file type enum based on a file extension.
	 * 
	 * @param {string} extension - The file extension. Must be in the format of ".[extension]"
	 */
	function getFileTypeFromExtension(extension) {
		if(extension != null && extension != "") {
			var lowerCaseExtension = extension.toLowerCase();
			
			if(lowerCaseExtension == ".zip") {
				return file.Type.ZIP;
			} else if(lowerCaseExtension == ".pdf") {
				return file.Type.PDF;
			} else if(lowerCaseExtension == ".png") {
				return file.Type.PNGIMAGE;
			} else if(lowerCaseExtension == ".jpg" || lowerCaseExtension == ".jpeg") {
				return file.Type.JPGIMAGE;
			} else if(lowerCaseExtension == ".doc" || lowerCaseExtension == ".docx") {
				return file.Type.WORD;
			} else if(lowerCaseExtension == ".xls" || lowerCaseExtension == ".xlsx") {
				return file.Type.EXCEL;
			} else if(lowerCaseExtension == ".csv") {
				return file.Type.CSV;
			}
		}
	}
	
	
	/**
	 * Downloads a file from the customer portal.
	 * 
	 * @param {String} fileUrl
	 * @return {Boolean}
	 */
	function downloadCaseAttachment(fileName, fileContents, queueID, contactID, companyInternalID) {
		if(fileContents != null && fileContents != "") {
			try {
				var caseFileExtension = getFileExtension(fileName);
				var caseFileType      = getFileTypeFromExtension(caseFileExtension);
				
				var caseFile = file.create({ 
					name: ("case-" + queueID + "-" + contactID + "-" + companyInternalID + caseFileExtension), 
					fileType: caseFileType, 
					contents: fileContents.replace("{", "").replace("}", ""), 
					folder: 6343559 
				});
				
				var fileID = caseFile.save();
				return fileID;
			} catch(ex) {
				log.error({ title: "downloadCaseAttachment - Error", details: ex });
			}
		}
		
		return null;
	}
	
	
	/**
	 * Creates a message and attaches it to a case.
	 * 
	 * @param {Integer} caseID - The case internal ID.
	 * @param {Integer} companyID - The company's internal id from the case.
	 * 
	 *  @return {Integer}
	 */
	function createCaseMessage(caseObj, fileID) {
		if(caseObj != null) {
			try {
				log.debug({ title: "createCaseMessage - caseObj", details: caseObj });
				log.debug({ title: "createCaseMessage - fileID", details: fileID });
				
				var msg = record.create({ type: "message", isDynamic: true });
				msg.setValue({ fieldId: "activity",       value: caseObj.id });
				msg.setValue({ fieldId: "author",         value: caseObj.contact });
				msg.setValue({ fieldId: "authoremail",    value: caseObj.email });
				msg.setValue({ fieldId: "recipient",      value: caseObj.company });
				msg.setValue({ fieldId: "recipientemail", value: caseObj.email });
				msg.setValue({ fieldId: "emailed",        value: false });
				msg.setValue({ fieldId: "subject",        value: caseObj.subject });
				msg.setValue({ fieldId: "message",        value: caseObj.message });
				
				if(fileID != null && fileID != "") {
					msg.selectLine({ sublistId: "mediaitem", line: 0 });
					msg.setCurrentSublistValue({ sublistId: "mediaitem", fieldId: "internalid", value: fileID });
					msg.commitLine({ sublistId: "mediaitem" });
				}
				
				log.debug({ title: "createCaseMessage - msg", details: msg });
				
				return msg.save({ ignoreMandatoryFields: true, enableSourcing: false });
			} catch(ex) {
				log.error({ title: "createCaseMessage - Error", details: ex });
			}
		}
	}
	
	
    return {
    	downloadCaseAttachment: downloadCaseAttachment,
    	createCaseMessage: createCaseMessage
    };
});
