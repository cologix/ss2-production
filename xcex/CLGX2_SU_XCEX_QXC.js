/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/**
 @author:         Catalina Taran - catalina.taran@cologix.com
 @date:           10/09/2019
 @description:    Create a Case when MDSO status is failed
 Script Name:     CLGX2_SU_XCEX_QXC
 Script ID:       customscript_CLGX2_SU_XCEX_QXC
 */

define(["N/runtime",
        "N/record",
        "N/search",
        "/SuiteScripts/clgx/libraries/lodash.min",
        'N/email'],
    function(runtime,record, search,_, email) {


        function afterSubmit(context) {

            if( (context.type == "create" || context.type == "edit")) {
                var new_rec         = context.newRecord;

                if(context.type == "edit") {
                    var oldRec = context.oldRecord;
                    var mdso_statusO = oldRec.getValue("custrecord_clgx_xcex_queue_bp_status") || "";
                }
                else{
                    var mdso_statusO=0;
                }
                var mdso_status= new_rec.getValue("custrecord_clgx_xcex_queue_bp_status") || "";

                if((mdso_status == 9 && mdso_statusO!=9) || (mdso_status == 9 && mdso_statusO==0)){
                    log.debug({ title: "mdso_status,", details: mdso_status});
                    var currentRecord = new_rec ;
                    var objCase = new Object();
                    var results = search.load({
                        type: 'customrecord_clgx_xcex_queue_v_xcs',
                        id: "customsearch_ss_clgx_xcex_queue_xcs_f"
                    });
                    log.debug({ title: "currentRecord.id,", details: currentRecord.id});
                    results.filters.push(search.createFilter({
                        name: "internalid",
                        operator: "ANYOF",
                        values: currentRecord.id
                    }));
                    results.run().each(function (result) {
                        var customer = parseInt(result.getValue(result.columns[0]));
                        log.debug({ title: "customer", details: customer});
                        var cRecord = search.lookupFields({
                            type: 'customer',
                            id: customer,
                            columns: ['companyname']
                        });
                        var companyname = cRecord.companyname;
                        objCase.cName = companyname;
                        var recType = result.getValue(result.columns[1]);
                        if (recType == "Virtual Cross Connect") {
                            var rec = result.getText(result.columns[3]);
                            var fac = result.getValue(result.columns[5]);
                            var label = result.getValue(result.columns[9]);
                        } else {
                            var rec = result.getText(result.columns[2]);
                            var fac = result.getValue(result.columns[4]);
                            var label = result.getValue(result.columns[8]);
                        }
                        objCase.rec = rec;
                        objCase.fac = fac;
                        var email = result.getValue(result.columns[6]);
                        log.debug({ title: "email", details: email});

                        objCase.email = email;
                        objCase.label = label;
                        var contact = result.getValue(result.columns[7]);
                        var contact_id=result.getValue(result.columns[10]);
                        objCase.contact = contact;
                        objCase.contact_id = contact_id;
                        objCase.customer = customer;

                        return true;
                    });
                    if(!(_.isEmpty(objCase))) {
                        var message = "The following XC/VXC has failed and needs to be review and fixed in order for the cloud order to be completed successfully." + objCase.rec
                        var rec = record.create({type: "supportcase"});
                        rec.setValue({fieldId: "company", value: objCase.customer});
                        rec.setValue({fieldId: "title", value: objCase.rec + " has failed to provisioned in MDSO"});
                        rec.setValue({fieldId: "custevent_cologix_facility", value: objCase.fac});
                        rec.setValue({fieldId: "profile", value: 9});
                        rec.setValue({fieldId: "assigned", value: 482198});
                        rec.setValue({fieldId: "email", value: objCase.email});
                        rec.setValue({fieldId: "origin", value: 3});
                        rec.setValue({fieldId: "category", value: 2});
                        rec.setValue({fieldId: "custevent_cologix_sub_case_type", value: 7});
                        rec.setValue({fieldId: "internalonly", value: true});
                        rec.setValue({fieldId: "emailform", value: true});
                        rec.setValue({fieldId: "htmlmessage", value: true});
                        rec.setValue({fieldId: "outgoingmessage", value: message});

                        var case_id = rec.save({
                            enableSourcing: true,
                            ignoreMandatoryFields: true
                        });
                        var caseRecord = search.lookupFields({
                            type: 'supportcase',
                            id: case_id,
                            columns: ['casenumber']
                        });
                        var cNumber = caseRecord.casenumber;
                        objCase.cNumber = cNumber;
                        send_customer_email(objCase);
                    }

                }
            }

        }

        function send_customer_email(record) {
            var subject = record.label + " failure - Network Engineering team has been engaged";
            var html = "Hello " + record.contact + " (" + record.cName + "),\n\n" + record.label + " failed to provisioned properly through automated process.  Our Network Engineering team has been made aware of this failure and will be working on correcting it.\n\nCase#" + record.cNumber + " has been opened on your behalf in order to track the progress of this failure.\n\nA member of our team will be providing updates through the case listed above.\n\nSincerely,\nCologix Access Market Place";

            log.debug({ title: "record.contact_id,", details: record.contact_id});
            email.send({
                author: 432742,
                recipients: record.contact_id,
                subject: subject,
                body: html,
                relatedRecords: {
                    entityId: record.contact_id
                }
            });

            return true;
        }



        return {
            afterSubmit   : afterSubmit
        };


    });