/**
 *
 * @author Catalina Taran - catalina.taran@cologix.com
 * @date   10/23/2019
 * @description Serve XC JSON - MDSO LIve and History
 *
 * Script Name  : CLGX2_SL_XCEX_Equipment
 * Script ID    : customscript_clgx2_sl_xcex_equipment
 * Deployment ID: customdeploy_clgx2_sl_xcex_equipment
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1757&deploy=1
 *
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
        "N/file",
        "N/task",
        "N/https",
        "N/record",
        "N/runtime",
        "N/search",
        "/SuiteScripts/clgx/libraries/lodash.min",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
    ],
    function(file, task, https, record, runtime, search, _, moment, xcex) {

        function onRequest(context) {

            var response = context.response;
            var request  = context.request;

            var ns_quid = request.parameters.quid || 0;
            if(ns_quid>0) {
                /*var fileObj = file.load({id: file_id});
                var history = JSON.parse(fileObj.getContents());
                var file_name = fileObj.name;

                if (fileObj.getContents() != "") {

                    // provisioning
                    for (var i = 0; i < history.provisioning.posting.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "action": "provisioning",
                            "status": "posting",
                            "posted": history.provisioning.posting.posted[i],
                            "source": history.provisioning.posting.source[i]
                        });
                    }
                    for (var i = 0; i < history.provisioning.provisioning.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "updated": history.provisioning.provisioning.bpupdt[i],
                            "action": "provisioning",
                            "status": "provisioning",
                            "posted": history.provisioning.provisioning.posted[i],
                            "source": history.provisioning.provisioning.source[i]
                        });
                    }
                    for (var i = 0; i < history.provisioning.provisioned.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "updated": history.provisioning.provisioned.bpupdt[i],
                            "action": "provisioning",
                            "status": "provisioned",
                            "posted": history.provisioning.provisioned.posted[i],
                            "source": history.provisioning.provisioned.source[i]
                        });
                    }
                    for (var i = 0; i < history.provisioning.failed.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "updated": history.provisioning.failed.bpupdt[i],
                            "action": "provisioning",
                            "status": "failed",
                            "posted": history.provisioning.failed.posted[i],
                            "source": history.provisioning.failed.source[i],
                            "errors": history.provisioning.failed.errors
                        });
                    }

                    // inactivating
                    for (var i = 0; i < history.inactivating.posting.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "action": "inactivating",
                            "status": "posting",
                            "posted": history.inactivating.posting.posted[i],
                            "source": history.inactivating.posting.source[i]
                        });
                    }
                    for (var i = 0; i < history.inactivating.inactivating.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "updated": history.inactivating.inactivating.bpupdt[i],
                            "action": "inactivating",
                            "status": "inactivating",
                            "posted": history.inactivating.inactivating.posted[i],
                            "source": history.inactivating.inactivating.source[i]
                        });
                    }
                    for (var i = 0; i < history.inactivating.inactivated.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "updated": history.inactivating.inactivated.bpupdt[i],
                            "action": "inactivating",
                            "status": "inactivated",
                            "posted": history.inactivating.inactivated.posted[i],
                            "source": history.inactivating.inactivated.source[i]
                        });
                    }
                    for (var i = 0; i < history.inactivating.failed.posted.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "updated": history.inactivating.failed.bpupdt[i],
                            "action": "inactivating",
                            "status": "failed",
                            "posted": history.inactivating.failed.posted[i],
                            "source": history.inactivating.failed.source[i],
                            "errors": history.inactivating.failed.errors
                        });
                    }

                    // notifications
                    for (var i = 0; i < history.notifying.notifs.length; i++) {
                        json.push({
                            "queueid": history.ns_qu_id,
                            "mdsoid": history.bp_vxc_id,
                            "action": "notifying",
                            "status": "posting",
                            "posted": history.notifying.posted[i],
                            "source": history.notifying.source[i],
                            "notifs": history.notifying.notifs[i]
                        });
                    }

                }
*/

                var fileObj = file.load({id: 22837467});
                var html = fileObj.getContents();
              //  html = html.replace(new RegExp('{json}', 'g'), JSON.stringify(json));
              //  html = html.replace(new RegExp('{ns_name}', 'g'), ns_name);
                response.write(html);
            }


        }
        return {
            onRequest: onRequest
        };

    });