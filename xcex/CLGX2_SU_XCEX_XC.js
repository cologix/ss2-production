/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    Trigger MDSO Southbounds actions
 Script Name:     CLGX2_SU_XCEX_XC
 Script ID:       customscript_clgx2_su_xcex_xc
 Link:            /app/common/scripting/script.nl?id=1737
 */

define([
        "N/file",
        "N/record",
        "N/task",
        "N/runtime",
        "N/search",
        "N/task",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX",
        "/SuiteScripts/clgx/libraries/lodash.min"],
    function(file,record, task, runtime, search, task, xcex,_) {

        function beforeLoad(context) {

            if(runtime.executionContext == "USERINTERFACE" && (context.type == "view" || context.type == "edit")) {

                var rec    = context.newRecord;
                var xcid   = rec.id;
                var quid   = parseInt(rec.getValue("custrecord_clgx_xcex_queue_id")) || 0;
                var type   = parseInt(rec.getValue("custrecord_cologix_xc_type") || 0);

                if(quid > 0 && type == 32){ // only for Cloud Exchange and if queue record exist

                    var queue   = search.lookupFields({
                        type    : 'customrecord_clgx_xcex_queue_v_xcs',
                        id      : quid,
                        columns : ['custrecord_clgx_xcex_queue_bp_id', 'custrecord_clgx_xcex_queue_bp_status', 'custrecord_clgx_xcex_queue_file_id']
                    });
                    if(queue.custrecord_clgx_xcex_queue_bp_status && queue.custrecord_clgx_xcex_queue_bp_status[0]){
                        rec.setValue("custrecord_clgx_xcex_bp_status", queue.custrecord_clgx_xcex_queue_bp_status[0].text || "");
                    }
                    if(queue.custrecord_clgx_xcex_queue_bp_id){
                        rec.setValue("custrecord_clgx_xcex_bp_xc_id", queue.custrecord_clgx_xcex_queue_bp_id || "");
                    }
                    if(queue.custrecord_clgx_xcex_queue_file_id){
                        var file_id = parseInt(queue.custrecord_clgx_xcex_queue_file_id) || 0;
                    }
                    log.debug({ title: "XCEX", details: ' | file_id:'+file_id});
                    if(file_id > 0){
                        rec.setValue("custrecord_clgx_xcex_bp_history", '<iframe name="_xcex_hist" id="_xcex_hist" src="/app/site/hosting/scriptlet.nl?script=1734&deploy=1&live=0&xcid=' + xcid + '&quid=' + quid + '" height="500px" width="1000" frameborder="0" scrolling="yes"></iframe>');
                    }
                    rec.setValue("custrecord_clgx_xcex_bp_live", '<iframe name="_xcex_live" id="_xcex_live" src="/app/site/hosting/scriptlet.nl?script=1734&deploy=1&live=1&xcid=' + xcid + '&quid=' + quid + '" height="300px" width="450" frameborder="0" scrolling="yes"></iframe>');
                }
            }
        }

        function beforeSubmit(context) {

            if(runtime.executionContext == "USERINTERFACE") {
                if(context.type == "edit" || context.type == "create") {
                    var new_rec         = context.newRecord;
                    var new_type        = parseInt(new_rec.getValue("custrecord_cologix_xc_type") || 0);

                    if(new_type == 32){ // only for Cloud Exchange

                        var user_id         = parseInt(runtime.getCurrentUser().id);
                        var ns_xc_id        = parseInt(new_rec.id);
                        var ns_qu_id        = parseInt(new_rec.getValue("custrecord_clgx_xcex_queue_id") || 0);
                        var provd_id        = parseInt(new_rec.getValue("custrecord_clgx_xcex_bp_provider") || 0);
                        var new_port        = parseInt(new_rec.getValue("custrecord_clgx_a_end_port") || 0);
                        var new_suspend     = new_rec.getValue("custrecord_clgx_xcex_suspend");
                        var new_inactive    = new_rec.getValue("isinactive") || false;

                        if(context.type == "create"){
                            new_rec.setValue("custrecord_clgx_a_end_port", null); // if user input, remove it  - this must be configured after the manual creation of the xc, on the second step
                            new_rec.setValue("custrecord_clgx_xcex_cloud_prov_sold_bw", 0);


                        }

                        if(context.type == "edit" && ns_qu_id > 0) {
                            var old_rec         = context.oldRecord;
                            var old_port        = parseInt(old_rec.getValue("custrecord_clgx_a_end_port") || 0);
                            var old_suspend     = old_rec.getValue("custrecord_clgx_xcex_suspend");
                            var old_inactive    = old_rec.getValue("isinactive");

                            log.debug({ title: "XCEX", details: ' | port_old/new : ' + old_port + "/" + new_port + ' | suspend_old/new: ' + old_suspend + '/' + new_suspend + ' | isinactive_old/new : ' + old_inactive + '/' + new_inactive + " |"});

                            // Manage changes
                            // ============================================== new port or port change ==============================================
                            if (old_port == 0 && new_port > 0) {
                                // trigger CLGX2_SS_XCEX_XC script
                                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                                scrpt.scriptId     = "customscript_clgx2_ss_xcex_xc";
                                scrpt.params       = { custscript_clgx_xcex_queue_xc: parseInt(ns_qu_id) };
                                scrpt.submit();
                            }
                            // ====================================== suspending or re provisioning requested  ======================================
                            if (old_suspend == false && new_suspend == true) { // suspending requested
                                xcex.update_queue (ns_qu_id, 4); // Change queue record - status to 'suspending'
                            }
                            if (old_suspend == true && new_suspend == false) { // reprovisioning requested
                                xcex.update_queue (ns_qu_id, 6); // Change queue record - status to 'reprovisioning'
                            }
                            // =============================================== inactivation requested ==============================================
                            if (old_inactive == false && new_inactive == true) {
                                xcex.update_queue (ns_qu_id, 7); // Change queue record - status to 'inactivating'
                                /*
                                // create inactivation case
                                var so_id = parseInt(new_rec.custrecord_xconnect_service_order);
                                var fields = search.lookupFields({
                                    type    : 'salesorder',
                                    id      : so_id,
                                    columns : ['entity']
                                });
                                var customer_id  = fields.entity;
                                var req = {
                                        "customer_id": customer_id,
                                        "contact_id": user_id,
                                        "id": ns_xc_id,
                                        "type": "xc"
                                };
                                // trigger CLGX2_SS_XCEX_Inactivate_Case script
                                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                                scrpt.scriptId     = "customscript_clgx2_ss_xcex_inactive_case";
                                scrpt.params       = { custscript_clgx_xcex_inactive_case_req: JSON.stringify(req) };
                                scrpt.submit();
                                */
                            }

                            /*
                            // reposting is desired because of a port change and que status failed
                            var ns_xc = search.lookupFields({
                                type    : 'customrecord_clgx_xcex_queue_v_xcs',
                                id      : ns_qu_id,
                                columns : ['custrecord_clgx_xcex_queue_bp_status']
                            });
                            var bp_st_id   = parseInt(ns_xc.custrecord_clgx_xcex_queue_bp_status) || null;
                            if (bp_st_id == 9 && ((old_port > 0 && new_port > 0 && old_port != new_port) || (old_port == 0 && new_port > 0))) {
                                xcex.update_queue (ns_qu_id, 9); // Change old queue record - status to 'failed'
                                // create new queue record
                                var new_qu_id = xcex.create_queue (27, ns_xc_id, null, null, customer_id, null, user_id, rec);
                                new_rec.setValue("custrecord_clgx_xcex_queue_id", new_qu_id); // write new queue id on xc

                                // trigger CLGX2_SS_XCEX_XC script
                                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                                scrpt.scriptId     = "customscript_clgx2_ss_xcex_xcu";
                                scrpt.params       = { custscript_clgx_xcex_queue_xc: parseInt(new_qu_id) };
                                scrpt.submit();
                            }
                            */
                        }
                    }
                }
            }
        }
        function afterSubmit(context) {

            if(runtime.executionContext == "USERINTERFACE") {
                var new_rec         = context.newRecord;
                var new_type        = parseInt(new_rec.getValue("custrecord_cologix_xc_type") || 0);

                if(context.type == "create" && new_type == 32){ // only for Cloud Exchange
                    var ns_xc_id        = parseInt(new_rec.id);
                    var provd_id        = parseInt(new_rec.getValue("custrecord_clgx_xcex_bp_provider") || 0);
                    var so_id           = parseInt(new_rec.getValue("custrecord_xconnect_service_order") || 0);
                    var service_id      = parseInt(new_rec.getValue("custrecord_cologix_xc_service") || 0);
                    var lag=new_rec.getValue('custrecord_clgx_xc_lag');
                    log.debug({ title: "lag", details: ' | lag:'+lag});
                    if(lag==true){
                        var lag1=1;
                    }else{
                        var lag1=0;
                    }
                    var lagname=new_rec.getValue('custrecord_clgx_xc_lag_id');
                    var lagports=new_rec.getValue('custrecord_clgx_xc_lag_ports');
                    var speed=new_rec.getValue('custrecord_clgx_xcex_xc_speed');
                    var provider_port=new_rec.getValue('custrecord_clgx_xc_cloud_port_label');
                    if(provider_port==null){
                        provider_port='';
                    }
                    var sw=[];
                    for(var i=0;i<lagports.length;i++){
                        var rec=record.load({type: 'customrecord_clgx_active_port', id: lagports[i]});
                        var namearray  = (rec.getValue('name')).split("|");
                        var objSw={
                            "speed": parseInt(speed),
                            "switch_id": namearray[0],
                            "port":  namearray[1]
                        };
                        sw.push(objSw);
                    }
                    if(so_id > 0 && service_id > 0){
                        var so = search.lookupFields({
                            type    : 'salesorder',
                            id      : so_id,
                            columns : ['entity']
                        });
                        var customer_id     = parseInt(so.entity[0].value);
                        var user_id = parseInt(runtime.getCurrentUser().id);
                        var obj={ "provider_id": provd_id,"lag_name":lagname,"sw":sw,"lag":lag1,"provider_port":provider_port};
                        var new_qu_id = xcex.create_queue (27, ns_xc_id, null, 1, customer_id, null, user_id,obj);
                        record.submitFields({
                            type   : 'customrecord_cologix_crossconnect',
                            id     : ns_xc_id,
                            values: { custrecord_clgx_xcex_queue_id : new_qu_id },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                        record.submitFields({
                            type  : 'customrecord_clgx_xcex_queue_v_xcs',
                            id    : new_qu_id,
                            values: {
                                custrecord_clgx_xcex_queue_so : so_id,
                                custrecord_clgx_xcex_queue_service : service_id},
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                }
                if(context.type == "edit" && new_type == 32) {
                    var lag = new_rec.getValue('custrecord_clgx_xc_lag');
                    var inactive=new_rec.getValue('isinactive');
                    log.debug({ title: "lag", details: ' | lag:'+lag});
                    if (lag == true && inactive==false) {
                        var old_rec         = context.oldRecord;
                        var lagname = new_rec.getValue('custrecord_clgx_xc_lag_id');
                        var lagports = new_rec.getValue('custrecord_clgx_xc_lag_ports');
                        var lagports_0     = old_rec.getValue("custrecord_clgx_xc_lag_ports");
                        var migrated=new_rec.getValue('custrecord_clgx_xcex_xc_migrate');
                        var difflag=_.xor(lagports_0,lagports);
                        var diff=_.difference(lagports_0,lagports);
                        log.debug({ title: "LAG_1", details: ' | difflag.length:'+difflag.length+';'+lagports.length});
                        if((lagports!=null && lagports!='')&& difflag.length>0) {
                            var speed = new_rec.getValue('custrecord_clgx_xcex_xc_speed');
                            var quID = new_rec.getValue('custrecord_clgx_xcex_queue_id');
                            var sw = [];
                            for (var i = 0; i < lagports.length; i++) {
                                var rec = record.load({type: 'customrecord_clgx_active_port', id: lagports[i]});
                                var namearray = (rec.getValue('name')).split("|");
                                var objSw = {
                                    "speed": parseInt(speed),
                                    "switch_id": namearray[0],
                                    "port": namearray[1]
                                };
                                sw.push(objSw);
                            }
                            var recQ = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: quID});
                            var file_id = parseInt(recQ.getValue('custrecord_clgx_xcex_queue_file_id'));

                            var fileObj = file.load({id: file_id});
                            var fileBody = JSON.parse(fileObj.getContents());
                            var file_name = fileObj.name;
                            fileBody.provisioning.posting.ns_post.lag_name=lagname;
                            fileBody.provisioning.posting.ns_post.migrated=migrated;
                            fileBody.provisioning.posting.ns_post.switches = sw;
                            log.debug({ title: "XCEX", details: ' | fileBody:'+JSON.stringify(fileBody)});

                            // save Order JSON on /XCEX/orders/
                            var fileObj = file.create({
                                name: file_name,
                                fileType: file.Type.PLAINTEXT,
                                contents: JSON.stringify(fileBody),
                                encoding: file.Encoding.UTF8,
                                folder: 10639242
                            });
                            recQ.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 16});
                            recQ.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent', value: true});

                            log.debug({ title: "XCEX", details: ' | LAG_file1:'+file_id});

                            var file_id = fileObj.save();
                            log.debug({ title: "XCEX", details: ' | LAG_file2:'+file_id});
                            recQ.setValue({fieldId: 'custrecord_clgx_xcex_queue_file_id', value: file_id});
                            var recid = recQ.save({enableSourcing: false, ignoreMandatoryFields: true});
                            for (var i = 0; diff != null && i < diff.length; i++) {
                                var rec = record.load({type: 'customrecord_clgx_active_port', id: diff[i]});
                                rec.setValue({fieldId: 'custrecord_clgx_active_port_status', value:2});
                                rec.setValue({fieldId: 'custrecord_clgx_active_port_xc', value:''});
                                rec.setValue({fieldId: 'custrecord_clgx_active_port_sequence', value:''});
                                rec.setValue({fieldId: 'custrecord_clgx_active_port_service', value:''});
                                rec.setValue({fieldId: 'custrecord_clgx_active_port_so', value:''});
                                rec.setValue({fieldId: 'custrecord_clgx_active_port_customer', value:''});
                                var recid = rec.save({enableSourcing: false, ignoreMandatoryFields: true});

                            }
                        }
                    }
                }
            }
        }

        return {
            beforeLoad    : beforeLoad,
            beforeSubmit  : beforeSubmit,
            afterSubmit   : afterSubmit
        };


    });