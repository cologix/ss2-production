/**
 * @NApiVersion 2.x
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    Library for XCEC
 Script Name:     CLGX2_LB_XCEX
 */

define([
        "N/file",
        "N/https",
        "N/record",
        "N/search",
        "N/runtime",
        "/SuiteScripts/clgx/libraries/moment.min"
    ],
    function(file, https, record, search, runtime, moment) {

        function set_enviro(){
            var enviro = "preprod";
            if(runtime.envType == "PRODUCTION"){
                enviro = "prod";
            }
            return enviro;
        }

        function get_enviros(){

            var results = search.create({
                type: 'customrecord_clgx_xcex_mdso_enviro',
                columns: [
                    {"name":"internalid"},
                    {"name":"name"},
                    {"name":"custrecord_clgx_xcex_mdso_enviro_burl"},
                    {"name":"custrecord_clgx_xcex_mdso_enviro_user"},
                    {"name":"custrecord_clgx_xcex_mdso_enviro_pass"},
                ]
            });
            var enviros = [];
            results.run().each(function(result) {
                var base_url = result.getValue({ name: "custrecord_clgx_xcex_mdso_enviro_burl"});
                enviros.push({
                    "enviro_id"   : parseInt(result.getValue({ name: "internalid"})),
                    "enviro"      : result.getValue({ name: "name"}),
                    "base_url"    : base_url,
                    "token_url"   : base_url + "/tron/api/v1/tokens",
                    "ping_url"    : base_url + "/bpcologixservicegwy/api/v1/ping",
                    "xc_ord_url"  : base_url + "/bpcologixservicegwy/api/v1/order/xc",
                    "xc_inv_url"  : base_url + "/bpcologixservicegwy/api/v1/inventory/xc",
                    "vxc_ord_url" : base_url + "/bpcologixservicegwy/api/v1/order/vxc",
                    "vxc_inv_url" : base_url + "/bpcologixservicegwy/api/v1/inventory/vxc",
                    'username'    : result.getValue({ name: "custrecord_clgx_xcex_mdso_enviro_user"}),
                    'password'    : result.getValue({ name: "custrecord_clgx_xcex_mdso_enviro_pass"}),
                });
                return true;
            });
            return enviros;
        }

        function get_headers (){
            var headers = {
                'Authorization'  : 'Bearer ' + get_token (),
                //'Token'          : get_token (enviro),
                'Content-Type'   : 'application/json',
                'Accept'         : 'application/json'
            };
            return headers;
        }

        function get_token (){
            var enviro = set_enviro ();
            if(enviro == "dev"){
                var file_id = 15766995;
            }
            if(enviro == "preprod"){
                var file_id = 15766996;
            }
            if(enviro == "prod"){
                var file_id = 15766997;
            }
            var fileObj = file.load({id: file_id});
            var contents = JSON.parse(fileObj.getContents());
            return contents.token;
        }

        function get_base_url (){
            var enviro = set_enviro ();
            if(enviro == "dev"){
                var base_url = "https://jaxbplab.cologix.net/";
            }
            if(enviro == "preprod"){
                var base_url = "https://bp-preprod.corp.cologix.net/";
            }
            if(enviro == "prod"){
                var base_url = "https://bp-prod.corp.cologix.net/";
            }
            return base_url;
        }

        function ping_mdso (){
            var success = null;
            var req = https.get({
                url : get_base_url() + "/bpcologixservicegwy/api/v1/ping",
                headers : get_headers()
            });
            var ping = JSON.parse(req.body);
            if(ping && ping.status && ping.status == "active"){
                success = true;
            } else {
                get_new_token();
                var req = https.get({
                    url : get_base_url() + "/bpcologixservicegwy/api/v1/ping",
                    headers : get_headers()
                });
                var ping = JSON.parse(req.body);
                if(ping && ping.status && ping.status == "active"){
                    success = true;
                } else{
                    success = false;
                }
            }
            return success;
        }

        function get_new_token (){
            var enviro = set_enviro ();
            if(enviro == "dev"){
                var obj = {
                    "url"    : "https://jaxbplab.cologix.net/tron/api/v1/tokens",
                    'username': 'admin',
                    'password': 'adminpw'
                };
            }
            if(enviro == "preprod"){
                var obj = {
                    "url"    : "https://bp-preprod.corp.cologix.net/tron/api/v1/tokens",
                    'username': 'netsuite',
                    'password': 'IYhWBVqjRtVNNC2NV6Ft'
                };
            }
            if(enviro == "prod"){
                var obj = {
                    "url"    : "https://bp-prod.corp.cologix.net/tron/api/v1/tokens",
                    'username': 'Netsuite',
                    'password': '3LgcDvuQtTzqyiTwGGrf'
                };
            }
            var headers = {
                'Content-Type' : 'application/json',
                'Accept'       : 'application/json'
            }

            var body = {
                'username': obj.username,
                'password': obj.password,
                'domain': {
                    'name': 'master'
                }
            };
            var req = https.post({
                url       : obj.url,
                body      : JSON.stringify(body),
                headers   : headers
            });
            var resp = JSON.parse(req.body);

            var fileObj = file.create({
                name: 'token_' + enviro + '.json',
                fileType: file.Type.PLAINTEXT,
                contents: JSON.stringify(resp),
                description: 'Last MDSO Token for ' + enviro,
                encoding: file.Encoding.UTF8,
                folder: 10639240,
                isOnline: false
            });
            var fileId = fileObj.save();

            return true;
        }

        function get_url_order_xc (){
            var url =  get_base_url () + '/bpcologixservicegwy/api/v1/order/xc';
            return url;
        }
        function get_url_invent_xc (){
            var url =  get_base_url () + '/bpcologixservicegwy/api/v1/inventory/xc';
            return url;
        }
        function get_url_order_vxc (){
            var url =  get_base_url () + '/bpcologixservicegwy/api/v1/order/vxc';
            return url;
        }
        function get_url_invent_vxc (){
            var url =  get_base_url () + '/bpcologixservicegwy/api/v1/inventory/vxc';
            return url;
        }
        function get_url_device_act(){
            var url =  get_base_url () + '/bpcologixactivationgwy/api/v1/device-activation';
            return url;
        }

        function update_queue (queue_id, status_id){
            var que_req_id = record.submitFields({
                type: 'customrecord_clgx_xcex_queue_v_xcs',
                id: queue_id,
                values: {custrecord_clgx_xcex_queue_bp_status  : status_id},
                options: {enableSourcing: false, ignoreMandatoryFields : true}
            });
            return true;
        }

        function update_bandwidth (ns_vxc_id, type) {
            log.debug({title: "XCEX", details: ' | ns_vxc_id : ' + ns_vxc_id + " | type: " + type + " |"});
            // bandwidth filed on vxc
            if (type == "decrease") {
                var bandwidth = search.lookupFields({
                    type: 'customrecord_cologix_vxc',
                    id: ns_vxc_id,
                    columns: ['custrecord_clgx_vxc_bandwidth', 'custrecord_clgx_cloud_connect_port']
                });
                //log.debug({ title: "debug", details: ' | bandwidth : ' + JSON.stringify(bandwidth) + " |"});
                var vxc_speed_id = parseInt(bandwidth.custrecord_clgx_vxc_bandwidth[0].value) || 0;
                var a_ns_xc_id = parseInt(bandwidth.custrecord_clgx_cloud_connect_port[0].value) || 0;
                //log.debug({ title: "debug", details: ' | vxc_speed_id : ' + vxc_speed_id + " |"});
                //log.debug({ title: "debug", details: ' | a_ns_xc_id : ' + a_ns_xc_id + " |"});
                if (vxc_speed_id > 0) {
                    // speed in mb on bandwidth record
                    var speed = search.lookupFields({
                        type: 'customrecord_clgx_xcex_xc_bandwidthspeed',
                        id: vxc_speed_id,
                        columns: ['custrecord_clgx_xcex_xc_speed_mb']
                    });
                    //log.debug({ title: "debug", details: ' | speed : ' + JSON.stringify(speed) + " |"});
                    var vxc_sold = parseInt(speed.custrecord_clgx_xcex_xc_speed_mb) || 0;
                    //log.debug({ title: "debug", details: ' | vxc_sold : ' + vxc_sold + " |"});
                    // total bandwidth sold on xc
                    var xc = search.lookupFields({
                        type: 'customrecord_cologix_crossconnect',
                        id: a_ns_xc_id,
                        columns: ['custrecord_clgx_xcex_cloud_prov_sold_bw']
                    });
                    //log.debug({ title: "debug", details: ' | xc : ' + JSON.stringify(xc) + " |"});
                    var a_ns_xc_sold = parseInt(xc.custrecord_clgx_xcex_cloud_prov_sold_bw) || 0;
                    //log.debug({ title: "debug", details: ' | a_ns_xc_sold : ' + a_ns_xc_sold + " |"});
                    // increase or decrease bandwidth on A side xc of the vxc


                    var sumBandwidth= a_ns_xc_sold - vxc_sold;

                    //log.debug({ title: "debug", details: ' | total : ' + total + " |"});
                    var xc_sold = record.submitFields({
                        type: 'customrecord_cologix_crossconnect',
                        id: a_ns_xc_id,
                        values: {custrecord_clgx_xcex_cloud_prov_sold_bw: sumBandwidth},
                        options: {enableSourcing: false, ignoreMandatoryFields: true}
                    });
                }
            }else {
                var results = search.load({type: 'customrecord_cologix_crossconnect', id: "customsearch7420"});
                results.filters.push(search.createFilter({name: "internalid", operator: "ANYOF", values: ns_vxc_id}));
                var sumBandwidth = 0;
                results.run().each(function (result) {
                    sumBandwidth += parseInt(result.getText(result.columns[0]));
                    log.debug({
                        title: "debug",
                        details: ' | speed : ' + parseInt(result.getText(result.columns[0])) + " |"
                    });
                    return true;
                });
                log.debug({title: "debug", details: ' | sumBandwidth : ' + sumBandwidth + " |"});
                var xc_sold = record.submitFields({
                    type: 'customrecord_cologix_crossconnect',
                    id: ns_vxc_id,
                    values: {custrecord_clgx_xcex_cloud_prov_sold_bw: parseInt(sumBandwidth)},
                    options: {enableSourcing: false, ignoreMandatoryFields: true}
                });
            }
            return sumBandwidth;
        }

        function create_queue (rec_type_id, ns_xc_id, ns_vxc_id, status_id, customer_id, contact_id, user_id, rec){

            var queue = record.create({ type: "customrecord_clgx_xcex_queue_v_xcs" });
            queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_record_type",     value: rec_type_id         }); // 27 for XC, 54 for VXC
            queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_xc",              value: ns_xc_id            }); // This XC ID
            queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_vxc",             value: ns_vxc_id           }); // This VXC ID
            if(rec.provider_id > 0){
                queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_provider",    value: rec.provider_id     }); // Cloud Provider - only for Providers
            }
            queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_bp_status",       value: status_id           }); // 'nonexisting' from portal, 'provisioning' from netsuite
            queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_contact",         value: contact_id          }); // contact (created from portal)
            queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_employee",        value: user_id             }); // employee (created manually)
            //queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_file_id",         value: parseInt(file_id)   });
            var ns_qu_id = queue.save();

            if(rec_type_id == 27){ // xc
                var folder_id = 10639242;
                var prefix    = "xc_";
                var rec_id    = ns_xc_id;
                var history = get_empty_history_xc (ns_xc_id, ns_qu_id, customer_id, contact_id, user_id, rec);
            }
            if(rec_type_id == 54){ // vxc
                var folder_id = 10639243;
                var prefix    = "vxc_";
                var rec_id    = ns_vxc_id;
                var history = get_empty_history_vxc (ns_vxc_id, ns_qu_id, customer_id, contact_id, user_id, rec);
            }

            var today = moment().format("MM/DD/YYYY_HH/mm/ss");
            var fileObj = file.create({
                name: prefix + rec_id + '_' + customer_id + '_' + today + '.json',
                fileType: file.Type.PLAINTEXT,
                contents: JSON.stringify(history),
                description: prefix + rec_id + '_' + customer_id + '_' + today,
                encoding: file.Encoding.UTF8,
                folder: folder_id,
                isOnline: false
            });
            var file_id = fileObj.save();

            var que_req_id = record.submitFields({
                type: 'customrecord_clgx_xcex_queue_v_xcs',
                id: ns_qu_id,
                values: {custrecord_clgx_xcex_queue_file_id : file_id},
                options: {enableSourcing: false,ignoreMandatoryFields : true}
            });
            return ns_qu_id;
        }

        function get_empty_history_xc (ns_xc_id, ns_qu_id, customer_id, contact_id, user_id, rec){
            if(!contact_id){
                contact_id = user_id;
            }
            // get XC name
            var xc = search.lookupFields({
                type    : 'customrecord_cologix_crossconnect',
                id      : ns_xc_id,
                columns : ['name', 'custrecord_clgx_port_id','custrecord_clgx_xcex_xc_migrate','custrecord_clgx_xcex_tagged']
            });

            var tagged=xc.custrecord_clgx_xcex_tagged;
            if(tagged==false){
                var port_mode = "access";
            }else{
                var port_mode = "trunk";
            }
            //var name = xc.name;
            //var name = xc.custrecord_clgx_port_id;
            if (rec.lag&& rec.lag==1){
                var ns_post = {
                    "ns_xc_id": ns_xc_id,
                    "ns_qu_id": ns_qu_id,
                    "ns_customer_id": customer_id,
                    "ns_contact_id": contact_id,
                    "status": "provisioning",
                    "inconsistency": false,
                    "lag_name": rec.lag_name,
                    "description": xc.name,
                    "switches": rec.sw,
                    "provider_id": rec.provider_id,
                    "provider_port": rec.provider_port,
                    "migrated": xc.custrecord_clgx_xcex_xc_migrate,
                    "port_mode":port_mode
                };
                log.debug({title: "NSPOST", details:  JSON.stringify(ns_post)});

            }else {
                if (rec.provider_id == 0) { // this is a XC for a customer
                    var ns_post = {
                        "ns_xc_id": ns_xc_id,
                        "ns_qu_id": ns_qu_id,
                        "ns_customer_id": customer_id,
                        "ns_contact_id": contact_id,
                        "status": "provisioning",
                        "inconsistency": false,
                        "lag_id": "",
                        "description": xc.name,
                        "switches": [],
                        "provider_id": 0,
                        "migrated": xc.custrecord_clgx_xcex_xc_migrate,
                        "port_mode":port_mode
                    };
                } else { // this is a XC for a provider
                    var ns_post = {
                        "ns_xc_id": ns_xc_id,
                        "ns_qu_id": ns_qu_id,
                        "ns_customer_id": customer_id,
                        "ns_contact_id": contact_id,
                        "inconsistency": false,
                        "status": "provisioning",
                        "lag_id": "",
                        "description": xc.name,
                        "switches": [],
                        "provider_id": rec.provider_id,
                        "provider_port": "",
                        "migrated": xc.custrecord_clgx_xcex_xc_migrate,
                        "port_mode":port_mode
                    };
                }
            }

            history = {
                "ns_xc_id": ns_xc_id,
                "ns_qu_id": ns_qu_id,
                "bp_xc_id": "",
                "provisioning": {
                    "posting": {
                        "ns_post": ns_post,
                        "posted": [],
                        "source": []
                    },
                    "provisioning": {
                        "bpupdt": [],
                        "posted": [],
                        "source": []
                    },
                    "provisioned": {
                        "bpupdt": [],
                        "posted": [],
                        "source": []
                    },
                    "failed": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                        "errors": ""
                    }
                },
                "suspending":{
                    "posting": {
                        "ns_post":{
                            "ns_xc_id": ns_xc_id,
                            "status": "suspending"
                        },
                        "posted": [],
                        "source": []
                    },
                    "suspending": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "suspended": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "failed": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                        "errors": ""
                    }
                },
                "reprovisioning":{
                    "posting": {
                        "ns_post":{
                            "ns_xc_id": ns_xc_id,
                            "status": "reprovisioning"
                        },
                        "posted": [],
                        "source": []
                    },
                    "reprovisioning": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "provisioned": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "failed": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                        "errors": ""
                    }
                },
                "inactivating": {
                    "posting": {
                        "ns_post": {
                            "ns_xc_id": ns_xc_id,
                            "status": "inactivating"
                        },
                        "posted": [],
                        "source": []
                    },
                    "inactivating": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "inactivated": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "failed": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                        "errors": ""
                    }
                },
                "notifying": {
                    "notifs": [],
                    "posted": [],
                    "source": []
                }
            };
            return history;
        }

        function get_empty_history_vxc (ns_vxc_id, ns_qu_id, customer_id, contact_id, user_id, rec ){
            if(!contact_id){
                contact_id = user_id;
            }
            // get VXC name
            var vxc = search.lookupFields({
                type    : 'customrecord_cologix_vxc',
                id      : ns_vxc_id,
                columns : ['name', 'custrecord_clgx_vxc_user_defined_name']
            });
            //var name = vxc.name;
            //var name = vxc.custrecord_clgx_vxc_user_defined_name;

            history = {
                "ns_vxc_id": ns_vxc_id,
                "ns_qu_id": ns_qu_id,
                "bp_vxc_id": "",
                "provisioning": {
                    "posting": {
                        "ns_post":{
                            "ns_vxc_id": ns_vxc_id,
                            "ns_qu_id": ns_qu_id,
                            "ns_customer_id": customer_id,
                            "ns_contact_id": contact_id,
                            "bp_vxc_id": null,
                            "status": "provisioning",
                            "inconsistency": false,
                            "xcs": rec.xcs,
                            "provider_id": rec.provider_id,
                            "description": vxc.name,
                            "vxlan": ns_vxc_id,
                            "bandwidth": rec.speed,
                            "cloud_reference_id" : rec.cloud_reference_id,
                            "cloud_connection_name" :  vxc.custrecord_clgx_vxc_user_defined_name
                        },
                        "posted": [],
                        "source": []
                    },
                    "provisioning": {
                        "bpupdt": [],
                        "posted": [],
                        "source": []
                    },
                    "provisioned": {
                        "bpupdt": [],
                        "posted": [],
                        "source": []
                    },
                    "failed": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                        "errors": ""
                    }
                },
                "updating":{
                    "posting": {
                        "ns_post":{
                            "ns_vxc_id": ns_vxc_id,
                            "status": "updating"
                        },
                        "posted": [],
                        "source": []
                    },
                    "updating": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "updated": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "failed": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                        "errors": ""
                    }
                },
                "inactivating": {
                    "posting": {
                        "ns_post": {
                            "ns_vxc_id": ns_vxc_id,
                            "status": "inactivating"
                        },
                        "posted": [],
                        "source": []
                    },
                    "inactivating": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "inactivated": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                    },
                    "failed": {
                        "bpupdt": [],
                        "posted": [],
                        "source": [],
                        "errors": ""
                    }
                },
                "notifying": {
                    "notifs": [],
                    "posted": [],
                    "source": []
                }

            };
            return history;
        }

        /* function get_aws_facilities (faciliy_id){
             var aws_facilities = [];
             switch(faciliy_id) {
                 case 2: case 5: case 4: case 9: case 6: case 7: case 19: case 45: case 51: case 52: case 53: // MTL
                     aws_facilities = [4]; // MTL3
                     break;
                 case 8: case 15: case 43: // TOR
                     aws_facilities = [8]; // TOR1
                     break;
                 case 14: case 20: case 42: // VAN
                     aws_facilities = [20]; // VAN2
                     break;
                 case 24: case 29: // COL
                     aws_facilities = [24]; // COL1&2
                     break;
                 case 34: case 33: case 35: case 36: // NNJ
                     aws_facilities = [50]; // ASH1
                     break;
                 case 3: case 18: case 44: // DAL
                     aws_facilities = [3,18]; // DAL1, DAL2
                     break;
                 case 21: case 27: // JAX
                     aws_facilities = [50]; // ASH1
                     break;
                 case 28: // LAK
                     aws_facilities = [50]; // ASH1
                     break;
                 case 17: case 25: // MIN
                     aws_facilities = [25]; // MIN3
                     break;
                 default:
                     aws_facilities = [];
             }
             return aws_facilities;
         }*/

        function get_nni_facilities (provider_id, faciltiy_id,market){

            var facilities = [];
            var columns = [];
            log.debug({ title: "debug LIB Market", details: ' | market : ' + market + provider_id+" |"});
            columns.push(search.createColumn({ name: "internalid" }));
            if(provider_id==3) {
                columns.push(search.createColumn({ name: "custrecord_clgx_xcex_nnis_so_facility" }));
            }else{
                columns.push(search.createColumn({ name: "custrecord_clgx_xcex_nnis_xc_facility" }))
            }
            columns.push(search.createColumn({ name: "custrecord_clgx_xcex_nnis_xc_facility" }));
            var filters = [];
            filters.push(search.createFilter({ name: "isinactive", operator: "is", values: false }));
            filters.push(search.createFilter({ name: "custrecord_clgx_xcex_nnis_provider", operator: "anyof", values: provider_id }));
            if(provider_id==3){
                filters.push(search.createFilter({
                    name: "custrecord_clgx_xcex_nnis_market",
                    operator: "is",
                    values: market
                }));

            }else {
                filters.push(search.createFilter({
                    name: "custrecord_clgx_xcex_nnis_so_facility",
                    operator: "anyof",
                    values: faciltiy_id
                }));
            }
            var results = search.create({ type: "customrecord_clgx_xcex_nnis", filters: filters, columns: columns });
            if(provider_id==3) {
                results.run().each(function (result) {

                    facilities.push(parseInt(result.getValue("custrecord_clgx_xcex_nnis_so_facility")))

                    return true;
                });
            }
            else{
                results.run().each(function (result) {
                    facilities.push(parseInt(result.getValue("custrecord_clgx_xcex_nnis_xc_facility")))
                    return true;

            });
            }

            return facilities;

            /*
            var aws_facilities = [];
            switch(faciliy_id) {
            case 2: case 5: case 4: case 9: case 6: case 7: case 19: case 45: case 51: case 52: case 53: // MTL
                aws_facilities = [4]; // MTL3
                break;
            case 8: case 15: case 43: // TOR
                aws_facilities = [8]; // TOR1
                  break;
            case 14: case 20: case 42: // VAN
                aws_facilities = [20]; // VAN2
                break;
            case 24: case 29: // COL
                aws_facilities = [24]; // COL1&2
                  break;
            case 34: case 33: case 35: case 36: // NNJ
                aws_facilities = [50]; // ASH1
                  break;
            case 3: case 18: case 44: // DAL
                aws_facilities = [3,18]; // DAL1, DAL2
                  break;
            case 21: case 27: // JAX
                aws_facilities = [50]; // ASH1
                break;
            case 28: // LAK
                aws_facilities = [50]; // ASH1
                  break;
            case 17: case 25: // MIN
                aws_facilities = [25]; // MIN3
                  break;
              default:
                aws_facilities = [];
            }
            return aws_facilities;
            */
        }
        function get_nni_facilities_google (region){


            var google_facilities = [];
            switch(region) {
                case "cologix-mtl3-b": // MTL
                    google_facilities = [4]; // MTL3
                    break;
                case "cologix-ashb": // ASH
                    google_facilities = [50,24]; // ASH1
                    break;
                default:
                    google_facilities = [];
            }
            return google_facilities;

        }

        function sleep (wait) {
            var start = new Date().getTime();
            while (new Date().getTime() < start + wait);
        }

        return {
            ping_mdso             : ping_mdso,
            get_token             : get_token,
            get_new_token         : get_new_token,
            get_enviros           : get_enviros,
            get_headers           : get_headers,
            get_base_url          : get_base_url,
            get_url_order_xc      : get_url_order_xc,
            get_url_invent_xc     : get_url_invent_xc,
            get_url_order_vxc     : get_url_order_vxc,
            get_url_invent_vxc    : get_url_invent_vxc,
            get_url_device_act    :get_url_device_act,
            update_queue          : update_queue,
            update_bandwidth      : update_bandwidth,
            create_queue          : create_queue,
            get_nni_facilities    : get_nni_facilities,
            get_nni_facilities_google    : get_nni_facilities_google,
            sleep                 : sleep
        };
    });