/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           10/15/2018
  @description:    MDSO Northbound POSTs for XCs
  Script Name:     CLGX2_RL_XCEX_Equipment
  Script ID:       customscript_clgx2_rl_xcex_equipment
  Link:            /app/site/hosting/restlet.nl?script=1754&deploy=1
*/


define([
	"N/file",
	"N/https",
	"N/record",
	"N/task", 
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
],

function(file, https, record, task, moment, xcex) {
   
    function doPost(mdso) {
    	
		try {
			log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
			return mdso;
		}
		catch (error) {
			var err = "";
	        if (error.getDetails != undefined){
	            err = error.getCode();
	        } else {
	            err = error.toString();
	        }
			var resp = {
                "status"   : "failed",
                "error"    : err
            };
			log.debug({ title: "debug", details: ' | error : ' + JSON.stringify(error) + " |"});
			return resp;
		}
    }
    return {
        post: doPost
    };

});
