/**
 * 
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   9/1/2017
 * @description Serve XC JSON - MDSO LIve and History
 * 
 * Script Name  : CLGX2_SL_XCEX_XC
 * Script File  : CLGX2_SL_XCEX_XC
 * Script ID    : customscript_clgx2_sl_xcex_xc
 * Deployment ID: customdeploy_clgx2_sl_xcex_xc
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1715&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
	],
function(file, task, https, record, runtime, search, _, moment, xcex) {

    function onRequest(context) {
		
	    var response = context.response;
	    var request  = context.request;
	    
	    var ns_xc_id = request.parameters.xcid || 0;
	    var live     = request.parameters.live || 0;
	    var json     = {};
	    
	    if(live > 0){
	    	
			var ping = xcex.ping_mdso(); 
		    if(ping){

		    	var req = https.get({
			        url       : xcex.get_url_invent_xc() + '/' + ns_xc_id,
			        headers   : xcex.get_headers()
			    });
				var resp = JSON.parse(req.body); 
			    json = resp;
		    }
		    response.write(JSON.stringify(json, null, 4));
	    }
	    else {
	    	
	    	// find all queue records for this XC - loop
	    	var json = [];
	    	var xc = search.lookupFields({
			    type    : 'customrecord_cologix_crossconnect',
			    id      : ns_xc_id,
			    columns : ['name']
			});
			var ns_name  = xc.name
	    	
	    	var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_xc_history" });
			results.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_queue_xc", operator: "IS", values: ns_xc_id }));
			results.run().each(function(result) {
				
				var ns_qu_id  = parseInt(result.getValue(result.columns[0]));
				var file_id   = parseInt(result.getValue(result.columns[1]));
				//var histstr   = result.getValue(result.columns[2]);
				var fileObj     = file.load({id: file_id});
				var history     = JSON.parse(fileObj.getContents());
				var file_name   = fileObj.name;
				
				if(fileObj.getContents() != ""){
					
					// provisioning
					for ( var i = 0; i < history.provisioning.posting.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"action"    : "provisioning",
							"status"    : "posting",
							"posted"    : history.provisioning.posting.posted[i],
							"source"    : history.provisioning.posting.source[i]
						});
					}
					for ( var i = 0; i < history.provisioning.provisioning.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.provisioning.provisioning.bpupdt[i],
							"action"    : "provisioning",
							"status"    : "provisioning",
							"posted"    : history.provisioning.provisioning.posted[i],
							"source"    : history.provisioning.provisioning.source[i]
						});
					}
					for ( var i = 0; i < history.provisioning.provisioned.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.provisioning.provisioned.bpupdt[i],
							"action"    : "provisioning",
							"status"    : "provisioned",
							"posted"    : history.provisioning.provisioned.posted[i],
							"source"    : history.provisioning.provisioned.source[i]
						});
					}
					for ( var i = 0; i < history.provisioning.failed.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.provisioning.failed.bpupdt[i],
							"action"    : "provisioning",
							"status"    : "failed",
							"posted"    : history.provisioning.failed.posted[i],
							"source"    : history.provisioning.failed.source[i],
							"errors"    : history.provisioning.failed.errors
						});
					}
					
					// suspending
					for ( var i = 0; i < history.suspending.posting.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"action"    : "suspending",
							"status"    : "posting",
							"posted"    : history.suspending.posting.posted[i],
							"source"    : history.suspending.posting.source[i]
						});
					}
					for ( var i = 0; i < history.suspending.suspending.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.suspending.suspending.bpupdt[i],
							"action"    : "suspending",
							"status"    : "suspending",
							"posted"    : history.suspending.suspending.posted[i],
							"source"    : history.suspending.suspending.source[i]
						});
					}
					for ( var i = 0; i < history.suspending.suspended.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.suspending.suspended.bpupdt[i],
							"action"    : "suspending",
							"status"    : "suspended",
							"posted"    : history.suspending.suspended.posted[i],
							"source"    : history.suspending.suspended.source[i]
						});
					}
					for ( var i = 0; i < history.suspending.failed.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.suspending.failed.bpupdt[i],
							"action"    : "suspending",
							"status"    : "failed",
							"posted"    : history.suspending.failed.posted[i],
							"source"    : history.suspending.failed.source[i],
							"errors"    : history.suspending.failed.errors
						});
					}
					
					// reprovisioning
					for ( var i = 0; i < history.reprovisioning.posting.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"action"    : "reprovisioning",
							"status"    : "posting",
							"posted"    : history.reprovisioning.posting.posted[i],
							"source"    : history.reprovisioning.posting.source[i]
						});
					}
					for ( var i = 0; i < history.reprovisioning.reprovisioning.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.reprovisioning.reprovisioning.bpupdt[i],
							"action"    : "reprovisioning",
							"status"    : "reprovisioning",
							"posted"    : history.reprovisioning.reprovisioning.posted[i],
							"source"    : history.reprovisioning.reprovisioning.source[i]
						});
					}
					for ( var i = 0; i < history.reprovisioning.provisioned.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.reprovisioning.provisioned.bpupdt[i],
							"action"    : "reprovisioning",
							"status"    : "provisioned",
							"posted"    : history.reprovisioning.provisioned.posted[i],
							"source"    : history.reprovisioning.provisioned.source[i]
						});
					}
					for ( var i = 0; i < history.reprovisioning.failed.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.reprovisioning.failed.bpupdt[i],
							"action"    : "reprovisioning",
							"status"    : "failed",
							"posted"    : history.reprovisioning.failed.posted[i],
							"source"    : history.reprovisioning.failed.source[i],
							"errors"    : history.reprovisioning.failed.errors
						});
					}
					
					// inactivating
					for ( var i = 0; i < history.inactivating.posting.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"action"    : "inactivating",
							"status"    : "posting",
							"posted"    : history.inactivating.posting.posted[i],
							"source"    : history.inactivating.posting.source[i]
						});
					}
					for ( var i = 0; i < history.inactivating.inactivating.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.inactivating.inactivating.bpupdt[i],
							"action"    : "inactivating",
							"status"    : "inactivating",
							"posted"    : history.inactivating.inactivating.posted[i],
							"source"    : history.inactivating.inactivating.source[i]
						});
					}
					for ( var i = 0; i < history.inactivating.inactivated.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.inactivating.inactivated.bpupdt[i],
							"action"    : "inactivating",
							"status"    : "inactivated",
							"posted"    : history.inactivating.inactivated.posted[i],
							"source"    : history.inactivating.inactivated.source[i]
						});
					}
					for ( var i = 0; i < history.inactivating.failed.posted.length; i++ ) {
						json.push({
							"queueid"   : history.ns_qu_id,
							"mdsoid"    : history.bp_xc_id,
							"updated"   : history.inactivating.failed.bpupdt[i],
							"action"    : "inactivating",
							"status"    : "failed",
							"posted"    : history.inactivating.failed.posted[i],
							"source"    : history.inactivating.failed.source[i],
							"errors"    : history.inactivating.failed.errors
						});
					}
					
				}
		        return true;
			});
			
			var fileObj = file.load({id: 15488004});
			var html = fileObj.getContents();
			html = html.replace(new RegExp('{json}','g'), JSON.stringify(json));
			html = html.replace(new RegExp('{ns_name}','g'), ns_name);
			response.write(html);
		}
	    
    }
    return {
        onRequest: onRequest
    };
	
});
