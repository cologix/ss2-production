/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Southbounds for XCs
 Script Name:     CLGX2_SS_XCEX_XC_Inactivate_Case
 Script ID:       customscript_clgx2_ss_xcex_inactive_case
 Link:            /app/common/scripting/script.nl?id=1741
 */

define([
        "N/file",
        "N/https",
        "N/record",
        "N/task",
        "N/runtime",
        "N/search",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
    ],
    function(file, https, record, task, runtime, search, moment, xcex) {

        function execute(context) {

            var current  = runtime.getCurrentScript();
            var req = JSON.parse(current.getParameter("custscript_clgx_xcex_inactive_case_req"));

            log.debug({ title: "debug", details: ' | req : ' + JSON.stringify(req) + " |"});

            var today    = moment().format("MM/DD/YYYY HH:mm:ss");
            var day      = moment().format("M/D/YYYY");
            var time     = moment().format("HH:mm");

            var fields = search.lookupFields({
                type    : 'customer',
                id      : req.customer_id,
                columns : ['entityid']
            });
            var custarr      = (fields.entityid).split(":");
            var customer     = custarr[(custarr.length-1)].trim();

            var fields = search.lookupFields({
                type    : 'contact',
                id      : req.contact_id,
                columns : ['entityid', 'email', 'phone']
            });
            var contact  = fields.entityid;
            var email    = fields.email || "";
            var phone    = fields.phone || "";

            var items = [];
            if(req.type == "vxc"){
                var fields = search.lookupFields({
                    type    : 'customrecord_cologix_vxc',
                    id      : req.id,
                    columns : ['name', 'custrecord_cologix_service_order', 'custrecord_cologix_vxc_service','custrecord_cologix_vxc_facility']
                });
                items.push({
                    "name"      : fields.name,
                    "so"        : fields.custrecord_cologix_service_order[0].text,
                    "service"   : fields.custrecord_cologix_vxc_service[0].text
                });
                var facility=fields.custrecord_cologix_vxc_facility[0].value;
            }
            if(req.type == "xc"){
                var fields = search.lookupFields({
                    type    : 'customrecord_cologix_crossconnect',
                    id      : req.id,
                    columns : ['name', 'custrecord_xconnect_service_order', 'custrecord_cologix_xc_service']
                });
                items.push({
                    "name"      : fields.name,
                    "so"        : fields.custrecord_xconnect_service_order[0].text,
                    "service"   : fields.custrecord_cologix_xc_service[0].text
                });
                // search all VXCs on this XC
                var results = search.load({ type: 'customrecord_cologix_vxc', id: "customsearch_clgx_xcex_xc_has_vxcs" });
                results.filters.push(search.createFilter({ name: "custrecord_clgx_cloud_connect_port_z", operator: "ANYOF", values: req.id }));
                results.run().each(function(result) {
                    var vxc_id = parseInt(result.getValue(result.columns[0]));
                    var fields = search.lookupFields({
                        type    : 'customrecord_cologix_vxc',
                        id      : vxc_id,
                        columns : ['name', 'custrecord_cologix_service_order', 'custrecord_cologix_vxc_service']
                    });
                    items.push({
                        "name"      : fields.name,
                        "so"        : fields.custrecord_cologix_service_order[0].text,
                        "service"   : fields.custrecord_cologix_vxc_service[0].text
                    });
                    return true;
                });
            }
            if(req.origin=="bandwidth"){
                var title = "Portal Bandwidth Upgrade " + req.customer;



            var message = "<br/>The following Virtual Connection services have been requested to be upgraded on "+req.timestamp+" from the Azure Portal.<br/>"+req.vxc+ " - "+req.so+" / "+req.service+"<br/>\n" +
                "The Account Receivable Rep is responsible to update the Service Order accordingly and update the pricing to reflect the new rate of $"+req.price+"<br/><br/>"

            var rec = record.create({type: "supportcase"});
            rec.setValue({fieldId: "company", value: req.customer_id});
            rec.setValue({fieldId: "title", value: title});
            rec.setValue({fieldId: "origin", value: -5});
            //rec.setValue({ fieldId: "inboundemail",                            value: email                   });
            rec.setValue({fieldId: "assigned", value: 12399});
           // rec.setValue({fieldId: "contact", value: req.contact_id});
            //rec.setValue({ fieldId: "email",                                   value: email                   });
           // rec.setValue({fieldId: "phone", value: phone});
            //rec.setValue({ fieldId: "custevent_cologix_case_sched_followup",   value: day                     });
            rec.setValue({ fieldId: "custevent_cologix_facility",      value: facility                   });
            rec.setValue({fieldId: "category", value: 11});
            rec.setValue({fieldId: "custevent_cologix_sub_case_type", value: 130});
            rec.setValue({fieldId: "internalonly", value: true});
            rec.setValue({fieldId: "emailform", value: true});
            rec.setValue({fieldId: "htmlmessage", value: true});
            rec.setValue({fieldId: "outgoingmessage", value: message});

            var case_id = rec.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });

                log.debug({ title: "debug", details: ' | case_id : ' + JSON.stringify(case_id) + " |"});
            }
            else if(req.origin=="cloud"){
                //  3	AWS - Direct Connect
                //  2	Azure - Express Route
                //  4	Google - Cloud Interconnect
                //	6	None
                //   5	Oracle - FastConnect
                //	1	SoftLayer - Direct Link Cloud Exchange
                var message = "";
                if(req.provider==4) {
                    var title = "GCP Cloud Connection Disco Request " + customer;
                    message += "The following services have been requested to be disconnected by " + contact + " on " + today + " from the Google Cloud Platform.<br/><br/>"

                }
                else if(req.provider==3) {
                    var title = "AWS - Direct Connect " + customer;
                    message += "The following services have been requested to be disconnected by " + contact + " on " + today + " from the AWS Cloud Platform.<br/><br/>"

                }
                else if(req.provider==2) {
                    var title = "Azure - Express Route " + customer;
                    message += "The following services have been requested to be disconnected by " + contact + " on " + today + " from the Azure Cloud Platform.<br/><br/>"

                }
                else if(req.provider==5) {
                    var title = "Oracle - FastConnect " + customer;
                    message += "The following services have been requested to be disconnected by " + contact + " on " + today + " from the Oracle Cloud Platform.<br/><br/>"

                }
                else{
                    var title = "Disco Request " + customer;
                }

                for (var i = 0; items != null && i < items.length; i++) {
                    message += items[i].name + ' - ' + items[i].so + ' / ' + items[i].service + '<br/>'
                }
                message += "<br/>The Account Receivable Rep is responsible to update the Service Order accordingly and stop the billing for the services listed above.<br/><br/>"

                var rec = record.create({type: "supportcase"});
                rec.setValue({fieldId: "company", value: req.customer_id});
                rec.setValue({fieldId: "title", value: title});
                rec.setValue({fieldId: "origin", value: -5});
                //rec.setValue({ fieldId: "inboundemail",                            value: email                   });
                rec.setValue({fieldId: "assigned", value: 12399});
                rec.setValue({fieldId: "contact", value: req.contact_id});
                //rec.setValue({ fieldId: "email",                                   value: email                   });
                rec.setValue({fieldId: "phone", value: phone});
                //rec.setValue({ fieldId: "custevent_cologix_case_sched_followup",   value: day                     });
                //rec.setValue({ fieldId: "custevent_cologix_sched_start_time",      value: time                    });
                rec.setValue({fieldId: "category", value: 11});
                rec.setValue({fieldId: "custevent_cologix_sub_case_type", value: 50});
                rec.setValue({fieldId: "internalonly", value: true});
                rec.setValue({fieldId: "emailform", value: true});
                rec.setValue({fieldId: "htmlmessage", value: true});
                rec.setValue({fieldId: "outgoingmessage", value: message});

                var case_id = rec.save({
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                });
            }else {

                var title = "Portal Disco Request " + customer;
                var message = "";
                message += "The following services have been requested to be disconnected by " + contact + " on " + today + " from the Cologix Customer Portal.<br/><br/>"
                for (var i = 0; items != null && i < items.length; i++) {
                    message += items[i].name + ' - ' + items[i].so + ' / ' + items[i].service + '<br/>'
                }
                message += "<br/>The Account Receivable Rep is responsible to update the Service Order accordingly and stop the billing for the services listed above.<br/><br/>"

                var rec = record.create({type: "supportcase"});
                rec.setValue({fieldId: "company", value: req.customer_id});
                rec.setValue({fieldId: "title", value: title});
                rec.setValue({fieldId: "origin", value: -5});
                //rec.setValue({ fieldId: "inboundemail",                            value: email                   });
                rec.setValue({fieldId: "assigned", value: 12399});
                rec.setValue({fieldId: "contact", value: req.contact_id});
                //rec.setValue({ fieldId: "email",                                   value: email                   });
                rec.setValue({fieldId: "phone", value: phone});
                //rec.setValue({ fieldId: "custevent_cologix_case_sched_followup",   value: day                     });
                //rec.setValue({ fieldId: "custevent_cologix_sched_start_time",      value: time                    });
                rec.setValue({fieldId: "category", value: 11});
                rec.setValue({fieldId: "custevent_cologix_sub_case_type", value: 50});
                rec.setValue({fieldId: "internalonly", value: true});
                rec.setValue({fieldId: "emailform", value: true});
                rec.setValue({fieldId: "htmlmessage", value: true});
                rec.setValue({fieldId: "outgoingmessage", value: message});

                var case_id = rec.save({
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                });
            }



            return true;
        }

        return {
            execute: execute
        };

    });