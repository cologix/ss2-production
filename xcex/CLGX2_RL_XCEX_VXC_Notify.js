/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Northbound POSTs for XCs Changes
 Script Name:     CLGX2_RL_XCEX_VXC_Notify
 Script ID:       customscript_clgx2_rl_xcex_vxc_notify
 Link:            /app/common/scripting/script.nl?id=1708
 */

define([
        "N/file",
        "N/https",
        "N/record",
        "N/search",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
    ],

    function(file, https, record,search, moment, xcex) {

        function doPost(mdso) {

            try {
                log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
                var cloud_connection_status = mdso.cloud_connection_status;

                var today     = moment().format("MM/DD/YYYY,HH:mm:ss");
                var queue     = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: mdso.ns_qu_id});
                var que = {
                    "ns_vxc_id"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_vxc'}) || 0),
                    "file_id"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                    "status"      : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
                };
                var fileObj     = file.load({id: que.file_id});
                var history     = JSON.parse(fileObj.getContents());
                var file_name   = fileObj.name;

                //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent',value: true});
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                if(mdso.inconsistency==true){
                    history.provisioning.posting.ns_post.inconsistency=false;
                    queue.setValue({fieldId: "custrecord_clgx_xcex_queue_inconsistent", value: true});
                }
                history.notifying.notifs.push(mdso.cloud_connection_status);
                history.notifying.posted.push(today);
                history.notifying.source.push("BP_Post");

                try {
                    if (cloud_connection_status == 'DEFUNCT' || cloud_connection_status == 'deleted' || cloud_connection_status == 'rejected') {
                        var isinactive = 7;
                    } else {
                        var isinactive = 0;
                        var vxc_fields = search.lookupFields({
                            type    : 'customrecord_cologix_vxc',
                            id      :  mdso.ns_vxc_id,
                            columns : ['custrecord_clgx_vxc_bandwidth']
                        });
                        var band=vxc_fields.custrecord_clgx_vxc_bandwidth;
                        var bandwidth_text=band[0].text;
                        if(bandwidth_text!=mdso.bandwidth){
                            var speed= ((mdso.bandwidth).match(/\d+/g)).toString();
                            log.debug({ title: "debug", details: ' | speed : ' +speed + " |"});
                            var vxc     = record.load({type: 'customrecord_cologix_vxc', id: mdso.ns_vxc_id});
                            vxc.setText({ fieldId: "custrecord_clgx_vxc_bandwidth", text:  mdso.bandwidth  });
                            vxc.setText({ fieldId: "custrecord_clgx_vxc_speed", text: speed.toString()});
                            var recid = vxc.save({enableSourcing: false, ignoreMandatoryFields: true});

                            history.provisioning.posting.ns_post.inconsistency=false;
                            history.provisioning.posting.ns_post.bandwidth=mdso.bandwidth;
                            queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 10 });

                        }
                    }
                    // update history and save queue record
                    //
                    if (isinactive > 0) {
                        var vxc_req_id = record.submitFields({
                            type: 'customrecord_cologix_vxc',
                            id: que.ns_vxc_id,
                            values: {
                                custrecord_clgx_vxc_cloud_conn_status: cloud_connection_status,
                                custrecord_clgx_vxcex_bp_status: isinactive

                            },
                            options: {enableSourcing: false, ignoreMandatoryFields: true}
                        });
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: isinactive });
                    } else {
                        var vxc_req_id = record.submitFields({
                            type: 'customrecord_cologix_vxc',
                            id: que.ns_vxc_id,
                            values: {
                                custrecord_clgx_vxc_cloud_conn_status: cloud_connection_status

                            },
                            options: {enableSourcing: false, ignoreMandatoryFields: true}
                        });
                    }

                    var resp = {
                        "ns_xc_id": mdso.ns_xc_id,
                        "ns_qu_id": mdso.ns_qu_id,
                        "bp_xc_id": mdso.bp_xc_id,
                        "status": mdso.status
                    };

                    var recid = queue.save({enableSourcing: false, ignoreMandatoryFields: true});

                    var fileObj = file.create({
                        name: file_name,
                        fileType: file.Type.PLAINTEXT,
                        contents: JSON.stringify(history),
                        encoding: file.Encoding.UTF8,
                        folder: 10639243
                    });
                    var file_id = fileObj.save();


                }
                catch (error) {
                    var err = "";
                    if (error.getDetails != undefined){
                        err = error.getCode();
                    } else {
                        err = error.toString();
                    }
                    var resp = {
                        "ns_xc_id" : mdso.ns_xc_id,
                        "status"   : "failed",
                        "error"    : err
                    };
                    log.debug({ title: "debug", details: ' | ns_xc_id : ' + mdso.ns_xc_id + ' | error : ' + JSON.stringify(error) + " |"});
                }
                return resp;
            }
            catch (error) {
                var err = "";
                if (error.getDetails != undefined){
                    err = error.getCode();
                } else {
                    err = error.toString();
                }
                var resp = {
                    "ns_xc_id" : mdso.ns_xc_id,
                    "status"   : "failed",
                    "error"    : err
                };
                log.debug({ title: "debug", details: ' | ns_xc_id : ' + mdso.ns_xc_id + ' | error : ' + JSON.stringify(error) + " |"});
                return resp;
            }
        }
        return {
            post: doPost
        };
    });