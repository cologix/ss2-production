/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           12/15/2018
  @description:    Check SO inventory - if all are provisioned, change status to Pending Billing
  Script Name:     CLGX2_SS_XCEX_Order_Pending
  Script ID:       customscript_clgx2_ss_xcex_order_pending
  Link:            /app/site/hosting/restlet.nl?script=1735&deploy=1
*/

define([
		"N/file",
		"N/record", 
		"N/task", 
		"N/runtime", 
		"N/search",
		'N/email',
		"/SuiteScripts/clgx/libraries/lodash.min",
		"/SuiteScripts/clgx/libraries/moment.min"
	],
function(file, record, task, runtime, search, email, _, moment) {
	
    function execute(context) {
    	
    	log.debug({ title: "XCEX", details: "===================================== START ============================================"});
    	
	    var response = context.response;
	    var request = context.request;
	    var today = moment().format("MM/DD/YYYY");
	    var start = moment().add(1, 'months').startOf('month').format("MM/DD/YYYY");
	    
	    var pending = [];
	    var results = search.load({ type: 'transaction', id: "customsearch_clgx_xcex_pending_sos" });
		results.run().each(function(result) {
			pending.push(parseInt(result.getValue(result.columns[0])));
			return true;
		});
		
		var queues = [];
	    var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_pending_sos_inv" });
	    results.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_queue_so", operator: "ANYOF", values: pending }));
		results.run().each(function(result) {
			queues.push({
				"so_id" : parseInt(result.getValue(result.columns[0])),
				"type" : parseInt(result.getValue(result.columns[1])),
				"status" : parseInt(result.getValue(result.columns[2]))
			});
			return true;
		});
		
		var so_ids = _.uniq(_.map(queues, 'so_id'));
		for ( var i = 0; i < so_ids.length; i++ ) {
			var all   = _.filter(queues, function(o) { return o.so_id == so_ids[i]; });
			var ready = _.filter(queues, function(o) { return (o.so_id == so_ids[i] && o.status  == 3); });
			var vxcs  = _.filter(queues, function(o) { return (o.so_id == so_ids[i] && o.type  == 54); });
			
			if(all.length == ready.length || vxcs.length == 0){
				// update SO status
				record.submitFields({ 
					type    : "salesorder", 
					id      : so_ids[i], 
					values  : { 
						orderstatus: "B",
						custbody_cologix_service_actl_instl_dt : today,
    	    	    	startdate : start
					} 
				});
				send_customer_email (so_ids[i], today);
				log.debug({ title: "XCEX", details: "| so_id: " + so_ids[i] + " |" });
			}
		}
		
		log.debug({ title: "XCEX", details: "===================================== END ============================================"});
		
    	return true;
    }
	
    function send_customer_email (so_id, today){
    	// second email
    	
    	var file_id = 0;
	    var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_so_billing_email" });
	    results.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_queue_so", operator: "ANYOF", values: so_id }));
		results.run().each(function(result) {
			file_id = parseInt(result.getValue(result.columns[1]));
			return true;
		});
	    
		if(file_id > 0){
			
			var fileObj   = file.load({id: file_id});
			var order     = JSON.parse(fileObj.getContents());
			
			var cust_fields = search.lookupFields({
			    type    : 'customer',
			    id      : order.customer_id,
			    columns : ['custentity_clgx_cust_ccm']
			});
			var ccmrep = "";
			if(cust_fields && cust_fields.custentity_clgx_cust_ccm && cust_fields.custentity_clgx_cust_ccm.length > 0 && cust_fields.custentity_clgx_cust_ccm[0] != null){
				ccmrep  = cust_fields.custentity_clgx_cust_ccm[0].text;
			}
			
			var cont_fields = search.lookupFields({
			    type    : 'contact',
			    id      : order.contact_id,
			    columns : ['entityid', 'custentity_clgx_modifier']
			});
			var contact = cont_fields.entityid;
			var user = "";
			if(cont_fields && cont_fields.custentity_clgx_modifier && cont_fields.custentity_clgx_modifier.length > 0 && cont_fields.custentity_clgx_modifier[0] != null){
				user  = cont_fields.custentity_clgx_modifier[0].text;
			}
	
			var so_fields = search.lookupFields({
			    type    : 'salesorder',
			    id      : order.so_id,
			    columns : ['tranid', 'entity', 'salesrep', "custbody_cologix_service_actl_instl_dt"]
			});
			var so           = so_fields.tranid;
			var custarr      = (so_fields.entity[0].text).split(":");
			var customer     = custarr[(custarr.length-1)].trim();
			var salesrep     = so_fields.salesrep[0].text;
			var installed    = so_fields.installed || "";
	
			var subject = "Service Commencement Notice Service Order " + so;
			var fileObj  = file.load({id: 16047018});
			var html     = fileObj.getContents();
			html = html.replace(new RegExp('{so}','g'), so);
			html = html.replace(new RegExp('{contact}','g'), contact);
			html = html.replace(new RegExp('{customer}','g'), customer);
			html = html.replace(new RegExp('{installed}','g'), today);
			html = html.replace(new RegExp('{ccmrep}','g'), ccmrep);
	
			email.send({
	            author: 432742,
	            recipients: order.contact_id,
	            subject: subject,
	            body: html,
	            relatedRecords: {
	                entityId: order.contact_id
	            }
	        });
		}
    	return true;
    }
    
    return {
        execute: execute
    };

});
