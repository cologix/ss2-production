/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Northbound POSTs for XCs Changes
 Script Name:     CLGX2_RL_XCEX_XC_Notify
 Script ID:       customscript_clgx2_rl_xcex_xc_notify
 Link:            /app/site/hosting/restlet.nl?script=1706&deploy=1
 */

define([
        "N/file",
        "N/record",
        "/SuiteScripts/clgx/libraries/moment.min"
    ],

    function(file, record, moment) {

        function doPost(mdso) {

            try {

                log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
                return mdso;


                /*
                var today        = moment().format("MM/DD/YYYY HH:mm:ss");
                var queue        = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: mdso.ns_qu_id});
                var qu_bp_status = parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}));
                var history      = JSON.parse(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_history'}));
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                
                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent',value: true});
                history.notifying.bpupdt.push(mdso);
                history.notifying.posted.push(today);
                history.notifying.source.push("BP_Post");
                
                // update history and save queue record
                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});
                
                var resp = {
                        "ns_xc_id" : mdso.ns_xc_id,
                        "ns_qu_id" : mdso.ns_qu_id,
                        "bp_xc_id" : mdso.bp_xc_id,
                        "status"   : "received",
                        "error"    : ""
                    };
                
                */



            }
            catch (error) {
                var err = "";
                if (error.getDetails != undefined){
                    err = error.getCode();
                } else {
                    err = error.toString();
                }

                var resp = {
                    "ns_xc_id" : mdso.ns_xc_id,
                    "status"   : "failed",
                    "error"    : err
                };
                log.debug({ title: "debug", details: ' | ns_xc_id : ' + mdso.ns_xc_id + ' | error : ' + JSON.stringify(error) + " |"});
                return resp;
            }

        }
        return {
            post: doPost
        };

    });
