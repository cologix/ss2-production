/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Northbound POSTs for XCs
 Script Name:     CLGX2_RL_XCEX_Order
 Script ID:       customscript_clgx2_rl_xcex_order
 Link:            /app/site/hosting/restlet.nl?script=1724&deploy=1
 */


define([
        "N/file",
        "N/record",
        "N/task",
        "N/search",
        "/SuiteScripts/clgx/libraries/lodash.min",
        "/SuiteScripts/clgx/libraries/moment.min"
    ],
    function (file, record, task, search, _, moment) {

        function doPost(orders) {
            log.debug({ title: "debug", details: ' | order : ' + JSON.stringify(orders) + " |"});

            try {

                var today        = moment().format("MM/DD/YYYY_HH:mm:ss");
                var location_ids = _.uniq(_.map(orders.items, 'location_id'));
                log.debug({ title: "debug", details: ' | location_ids : ' + JSON.stringify(location_ids) + " |"});

                for ( var i = 0; location_ids != null && i < location_ids.length; i++ ) {

                    var facility_id  = get_facility_id (location_ids[i])
                    var items = _.filter(orders.items, function(arr){
                        return (arr.location_id == location_ids[i]);
                    });
                    for ( var j = 0; items != null && j < items.length; j++ ) {
                        items[j].facility_id = facility_id;
                    }

                    var order = {
                        "customer_id"       : orders.customer_id,
                        "contact_id"        : orders.contact_id,
                        "title"             : orders.title,
                        "location_id"       : location_ids[i],
                        "facility_id"       : facility_id,
                        "oppty_id"          : orders.oppty_id,
                        "estimate_id"       : orders.estimate_id,
                        "so_id"             : orders.so_id,
                        "po"                : orders.po,
                        "billing"           : orders.billing,
                        "server"            : orders.server,
                        "items"             : items
                    };
                    log.debug({ title: "debug", details: ' | order_' + location_ids[i] + ' : ' + JSON.stringify(order) + " |"});

                    var fileObj = file.create({
                        name: 'order_' + location_ids[i] + '_' + order.customer_id + '_' + today + '.json',
                        fileType: file.Type.PLAINTEXT,
                        contents: JSON.stringify(order),
                        description: order + '_' + order.customer_id + '_' + today,
                        encoding: file.Encoding.UTF8,
                        folder: 10639241,
                        isOnline: false
                    });
                    var file_id = fileObj.save();

                    var queue = record.create({ type: "customrecord_clgx_xcex_queue_v_xcs" });
                    queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_record_type",     value: -30                    }); // Transaction
                    queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_bp_status",       value: 2                      }); // provisioning
                    queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_contact",         value: order.contact_id       }); // contact - from portal
                    queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_file_id",         value: parseInt(file_id)      });
                    //queue.setValue({ fieldId: "custrecord_clgx_xcex_queue_history",         value: JSON.stringify(order)  }); // JSON Order
                    var ns_qu_id = queue.save();

                    log.debug({ title: "debug", details: ' | ns_qu_id' + ns_qu_id});

                    // trigger CLGX2_SS_XCEX_Order
                    var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                    scrpt.scriptId     = "customscript_clgx2_ss_xcex_order";
                    scrpt.params       = {
                        custscript_clgx_xcex_queue_order: parseInt(ns_qu_id) ,
                        custscript_clgx_xcex_queue_file: parseInt(file_id)
                    };
                    scrpt.submit();

                }

                return location_ids;

            }
            catch (error) {
                var err = "";
                if (error.getDetails != undefined){
                    err = error.getCode();
                } else {
                    err = error.toString();
                }
                log.debug({ title: "debug", details: ' | error : ' + JSON.stringify(err) + " |"});
                return JSON.stringify(err) ;
            }

        }

        function get_facility_id (location_id){
            var facilityid = 0;
            var mySearch = search.load({ type: 'customrecord_cologix_facility', id: 'customsearch_clgx_xcex_facility'});
            mySearch.filters.push(search.createFilter({ name: "custrecord_clgx_facility_location", operator: "ANYOF", values: location_id }));
            var results = mySearch.run().getRange({start: 0,end: 1});
            if(results && results.length == 1){
                facility_id = parseInt(results[0].getValue(results[0].columns[0]));
            };
            return facility_id;
        }

        return {
            post: doPost
        };

    });