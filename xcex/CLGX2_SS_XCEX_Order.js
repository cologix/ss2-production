/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Southbounds for XCs
 Script Name:     CLGX2_SS_XCEX_Order
 Script ID:       customscript_clgx2_ss_xcex_order
 Link:            /app/common/scripting/script.nl?id=1738
 */

define([
        "N/file",
        "N/record",
        "N/task",
        "N/runtime",
        "N/search",
        'N/email',
        "/SuiteScripts/clgx/libraries/lodash.min",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
    ],
    function(file, record, task, runtime, search, email, _, moment, xcex) {

        function execute(context) {

            var current  = runtime.getCurrentScript();
            var ns_qu_id = current.getParameter("custscript_clgx_xcex_queue_order");
            var file_id  = current.getParameter("custscript_clgx_xcex_queue_file");

            var fileObj     = file.load({id: file_id});
            var order       = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;
            var queue       = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});

            log.debug({ title: "debug", details: ' | ns_qu_id : ' + ns_qu_id + " |"});

            order = create_inventory (order);
            order = create_transactions (order);
            order = update_queues (order);
            send_customer_email (order);
            calculate_transactions_totals (order.oppty_id, order.estimate_id, order.so_id, order.customer_id);
            create_pdf_contract (order.estimate_id, order.contact_id);

            log.debug({ title: "XCEX", details: ' | customer_id : ' + order.customer_id +  ' | contact_id : ' + order.contact_id +  ' | location_id : ' + order.location_id +  ' | oppty_id : ' + order.oppty_id +  " | estimate_id: " + order.estimate_id  + " | so_id : " + order.so_id  + " |"});

            // update and save queue record
            queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_so',           value: order.so_id             });
            queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',    value: 3                       });
            //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',      value: JSON.stringify(order)   });
            var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

            // save Order JSON on /XCEX/orders/
            var fileObj = file.create({
                name: file_name,
                fileType: file.Type.PLAINTEXT,
                contents: JSON.stringify(order),
                encoding: file.Encoding.UTF8,
                folder: 10639241
            });
            var file_id = fileObj.save();

            return true;
        }

        function create_inventory (order){
            // create XCs
            for ( var i = 0; order.items != null && i < order.items.length; i++ ) {
                if(order.items[i].type == "xc"){
                    // create primary XC
                    var ns_xc_id = create_xc (order.customer_id, order.items[i], false);
                    order.items[i].prm_ns_xc_id = ns_xc_id;
                    // create queue record
                    var ns_qu_id = xcex.create_queue (27, ns_xc_id, null, null, order.customer_id, order.contact_id, null, order.items[i]);
                    order.items[i].prm_ns_qu_id = ns_qu_id;
                    //log.debug({ title: "debug", details: ' | xc_pri : ' + JSON.stringify(order) + " |"});
                    // write new queue id on xc
                    var xc_req_id = record.submitFields({
                        type: 'customrecord_cologix_crossconnect',
                        id: ns_xc_id,
                        values: {custrecord_clgx_xcex_queue_id : ns_qu_id},
                        options: {enableSourcing: false,ignoreMandatoryFields : true}
                    });
                    // create secondary XC, if any
                    if(order.items[i].redundant){
                        var ns_xc_id = create_xc (order.customer_id, order.items[i], true);
                        order.items[i].sec_ns_xc_id = ns_xc_id;
                        // create queue record
                        var ns_qu_id = xcex.create_queue (27, ns_xc_id, null, null, order.customer_id, order.contact_id, null, order.items[i]);
                        order.items[i].sec_ns_qu_id = ns_qu_id;
                        //log.debug({ title: "debug", details: ' | xc_sec : ' + JSON.stringify(order) + " |"});
                        // write new queue id on xc
                        var xc_req_id = record.submitFields({
                            type: 'customrecord_cologix_crossconnect',
                            id: ns_xc_id,
                            values: {custrecord_clgx_xcex_queue_id : ns_qu_id},
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                }
            }
            // create VXCs
            for ( var i = 0; order.items != null && i < order.items.length; i++ ) {
                if(order.items[i].type == "vxc"){
                    // if vxc is on a new xc, find the id of the above created xc (primary or secondary)
                    var xc_exist = true;
                    if(order.items[i].ns_xc_id == 0){
                        xc_exist = false;
                        var prm_xc = _.find(order.items, function(o) { return (o.type == "xc" && o.prm_new_xc_id == order.items[i].new_xc_id); });
                        if(prm_xc){ // this vxc is on the primary xc
                            //log.debug({ title: "debug", details: ' | prm_xc : ' + JSON.stringify(prm_xc) + " |"});
                            order.items[i].ns_xc_id = prm_xc.prm_ns_xc_id;
                            //log.debug({ title: "debug", details: ' | vxc_pri : ' + JSON.stringify(order) + " |"});
                        }
                        var sec_xc = _.find(order.items, function(o) { return (o.type == "xc" && o.sec_new_xc_id == order.items[i].new_xc_id); });
                        if(sec_xc){ // this vxc is on the primary xc
                            //log.debug({ title: "debug", details: ' | sec_xc : ' + JSON.stringify(sec_xc) + " |"});
                            order.items[i].ns_xc_id = sec_xc.sec_ns_xc_id;
                            //log.debug({ title: "debug", details: ' | vxc_pri : ' + JSON.stringify(order) + " |"});
                        }
                    }
                    // find the A End XC for provider and facility
                    var a_ns_xc_id = null;
                    if(order.items[i].provider_id == 3){ // AWS
                        a_ns_xc_id = get_a_ns_xc_id (order.items[i].provider_id, order.items[i].facility_id, order.items[i].azure_secondary_port);
                        order.items[i].xcs.push({
                            "end" :"A",
                            "ns_xc_id": a_ns_xc_id,
                            "vlan_tagging":'Single Tagged'
                            //"egress_vlan": 2905, // this is provided by mdso
                            //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                        });
                        if(order.items[i].z_egress_vlan > 0){ // if 0, mdso will choose it
                            order.items[i].xcs.push({
                                "end" :"Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "egress_vlan": parseInt(order.items[i].z_egress_vlan),
                                "vlan_tagging":order.items[i].tagged
                                //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                            });
                        } else {
                            order.items[i].xcs.push({
                                "end" :"Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "vlan_tagging": order.items[i].vlan_tagging
                                //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                            });
                        }
                    }
                    if(order.items[i].provider_id == 4){ // Google
                        a_ns_xc_id = get_a_ns_xc_id (order.items[i].provider_id, order.items[i].facility_id,'',order.items[i].region,order.items[i].ns_xc_id);
                        order.items[i].xcs.push({
                            "end" :"A",
                            "ns_xc_id": a_ns_xc_id,
                            "vlan_tagging":'Single Tagged'
                            //"egress_vlan": 2905, // this is provided by mdso
                            //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                        });
                        if(order.items[i].z_egress_vlan > 0){ // if 0, mdso will choose it
                            order.items[i].xcs.push({
                                "end" :"Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "egress_vlan": parseInt(order.items[i].z_egress_vlan),
                                "vlan_tagging":order.items[i].tagged
                                //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                            });
                        } else {
                            order.items[i].xcs.push({
                                "end" :"Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "vlan_tagging":order.items[i].tagged,

                                //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                            });
                        }
                    }
                    if(order.items[i].provider_id == 2||order.items[i].provider_id == 5){ // Azure
                        var port_label = "";
                        if(order.items[i].azure_primary_port != ""){ // primary
                            //log.debug({ title: "XCEX", details: ' | provider_id : ' + order.items[i].provider_id + " | facility_id: " + order.items[i].facility_id  +  " | nni: " + order.items[i].azure_primary_port  + " |"});
                            a_ns_xc_id = get_a_ns_xc_id (order.items[i].provider_id, order.items[i].facility_id, order.items[i].azure_primary_port);
                            //log.debug({ title: "debug", details: ' | a_ns_xc_id : ' + a_ns_xc_id + " |"});
                        }
                        if(order.items[i].azure_secondary_port != ""){ // secondary
                            //log.debug({ title: "XCEX", details: ' | provider_id : ' + order.items[i].provider_id + " | facility_id: " + order.items[i].facility_id  +  " | nni: " + order.items[i].azure_secondary_port  + " |"});
                            a_ns_xc_id = get_a_ns_xc_id (order.items[i].provider_id, order.items[i].facility_id, order.items[i].azure_secondary_port);
                            //log.debug({ title: "debug", details: ' | a_ns_xc_id : ' + a_ns_xc_id + " |"});
                        }
                        if (order.items[i].provider_id == 2) {
                            order.items[i].xcs.push({
                                "end": "A",
                                "ns_xc_id": a_ns_xc_id,
                                "egress_vlan": parseInt(order.items[i].a_egress_vlan), // returned by Azure
                                "vlan_tagging":'Double Tagged'
                                //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                            });
                        }else{
                            order.items[i].xcs.push({
                                "end": "A",
                                "ns_xc_id": a_ns_xc_id,
                                "vlan_tagging":'Single Tagged'
                            });
                        }
                        if(order.items[i].z_egress_vlan > 0){
                            order.items[i].xcs.push({
                                "end" :"Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "egress_vlan": parseInt(order.items[i].z_egress_vlan),
                                "vlan_tagging":order.items[i].tagged
                                //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                            });
                        } else {
                            order.items[i].xcs.push({
                                "end" :"Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "vlan_tagging":order.items[i].tagged
                                //"egress_type": order.items[i].egress_type // mdso hard coded in phase 1
                            });
                        }
                    }

                    if(order.items[i].tport>0){
                        if(order.items[i].taggedt=='') {


                            order.items[i].taggedt=order.items[i].tagged;
                            order.items[i].vlant=order.items[i].z_egress_vlan;

                        }
                        if(order.items[i].vlant!='') {
                            order.items[i].xcs.push({
                                "end": "A",
                                "ns_xc_id": parseInt(order.items[i].tport),
                                "egress_vlan": parseInt(order.items[i].vlant),
                                "vlan_tagging": order.items[i].taggedt

                            });
                        }
                        else{
                            order.items[i].xcs.push({
                                "end": "A",
                                "ns_xc_id": parseInt(order.items[i].tport),
                                "vlan_tagging": order.items[i].taggedt

                            });
                        }
                        if(order.items[i].z_egress_vlan!='') {
                            order.items[i].xcs.push({
                                "end": "Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "egress_vlan": parseInt(order.items[i].z_egress_vlan),
                                "vlan_tagging": order.items[i].tagged
                            });
                        }else{
                            order.items[i].xcs.push({
                                "end": "Z",
                                "ns_xc_id": order.items[i].ns_xc_id,
                                "vlan_tagging": order.items[i].tagged
                            });
                        }
                        order.items[i].provider_id=6;
                    }

                    // create vxc
                    log.debug({ title: "debug", details: ' | order0 : ' + JSON.stringify(order) + " |"});
                    var ns_vxc_id = create_vxc (order.customer_id, order.items[i]);
                    order.items[i].ns_vxc_id = ns_vxc_id;
                    order.items[i].vxlan = ns_vxc_id;
                    order.items[i].xcs[0].vlan_tagging=( order.items[i].xcs[0].vlan_tagging ).toLowerCase().replace(' ', "-");
                    order.items[i].xcs[1].vlan_tagging=((order.items[i].xcs[1].vlan_tagging).toLowerCase()).replace(' ', "-");
                    if(order.items[i].tport>0){
                        order.items[i].provider_id=0;
                    }
                    if(order.items[i].xcs[0].vlan_tagging==''){
                        order.items[i].xcs[0].vlan_tagging='single-tagged';
                    }
                    log.debug({ title: "debug", details: ' | order1 : ' + JSON.stringify(order) + " |"});
                    // create queue
                    var ns_qu_id = xcex.create_queue (54, order.items[i].ns_xc_id, ns_vxc_id, 1, order.customer_id, order.contact_id, null, order.items[i]);
                    order.items[i].ns_qu_id = ns_qu_id;
                    // write new queue id on xc
                    var vxc_req_id = record.submitFields({
                        type: 'customrecord_cologix_vxc',
                        id: ns_vxc_id,
                        values: {custrecord_clgx_vxcex_queue_id : ns_qu_id},
                        options: {enableSourcing: false,ignoreMandatoryFields : true}
                    });

                    // verify if the corresponding XC is provisioned and post only if it is  ==============================================================================
                    //log.debug({ title: "debug", details: ' | item : ' + JSON.stringify(order.items[i]) + " |"});
                    var vxc_xc = search.lookupFields({
                        type    : 'customrecord_cologix_crossconnect',
                        id      : order.items[i].ns_xc_id,
                        columns : ['custrecord_clgx_xcex_queue_id']
                    });
                    //log.debug({ title: "debug", details: ' | vxc_xc : ' + JSON.stringify(vxc_xc) + " |"});
                    var vxc_xc_qu_id = 0;
                    if(vxc_xc && vxc_xc.custrecord_clgx_xcex_queue_id && vxc_xc.custrecord_clgx_xcex_queue_id.length > 0 && vxc_xc.custrecord_clgx_xcex_queue_id[0] != null){
                        vxc_xc_qu_id  = parseInt(vxc_xc.custrecord_clgx_xcex_queue_id[0].value);
                    }
                    var vxc_xc_qu = search.lookupFields({
                        type    : 'customrecord_clgx_xcex_queue_v_xcs',
                        id      : vxc_xc_qu_id,
                        columns : ['custrecord_clgx_xcex_queue_bp_status']
                    });
                    //log.debug({ title: "debug", details: ' | vxc_xc_qu : ' + JSON.stringify(vxc_xc_qu) + " |"});
                    var vxc_xc_qu_stat_id = 0;
                    if(vxc_xc_qu && vxc_xc_qu.custrecord_clgx_xcex_queue_bp_status && vxc_xc_qu.custrecord_clgx_xcex_queue_bp_status.length > 0 && vxc_xc_qu.custrecord_clgx_xcex_queue_bp_status[0] != null){
                        vxc_xc_qu_stat_id  = parseInt(vxc_xc_qu.custrecord_clgx_xcex_queue_bp_status[0].value);
                    }
                    // verify if the corresponding XC is provisioned and post only if it is  ==============================================================================
                    if(xc_exist && vxc_xc_qu_stat_id == 3){
                        // trigger CLGX2_SS_XCEX_VXC script
                        var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                        scrpt.scriptId     = "customscript_clgx2_ss_xcex_vxc";
                        scrpt.params       = { custscript_clgx_xcex_queue_vxc: parseInt(ns_qu_id) };
                        scrpt.submit();
                    }
                }
            }
            return order;
        }

        function create_xc (customer_id, xc, sec){
            var title = xc.prm_name;
            if(sec){
                title = xc.sec_name;
            }
            //log.debug({ title: "debug", details: ' | title : ' + JSON.stringify(title) + " |"});
            var rec = record.create({ type: "customrecord_cologix_crossconnect"});
            rec.setValue({ fieldId: "custrecord_clgx_xc_facility",                value: xc.facility_id         });
            if(xc.provider_id > 0){
                rec.setValue({ fieldId: "custrecord_clgx_xcex_bp_provider",       value: xc.provider_id         });; // Cloud Provider - only for Providers
            }
            rec.setValue({ fieldId: "custrecord_clgx_xcex_xc_speed",              value: xc.speed_id            });
            rec.setValue({ fieldId: "custrecord_cologix_xc_circuit_type",         value: xc.media_id            });
            rec.setValue({ fieldId: "custrecord_clgx_port_id",                    value: title                  });
            rec.setValue({ fieldId: "custrecord_cologix_xc_type",                 value: 32                     });
            rec.setValue({ fieldId: "custrecord_cologix_xc_status",               value: 1                      });
            rec.setValue({ fieldId: "custrecord_cologix_carrier_name",            value: 2763                   });
            rec.setValue({ fieldId: "custrecord_clgx_xcex_tagged",                value: true                   });
            rec.setValue({ fieldId: "custrecord_clgx_xcex_customer_id",           value: customer_id            });
            rec.setValue({ fieldId: "custrecord_clgx_xcex_cloud_prov_sold_bw",    value: 0                      });

            if(xc.redundant) {
                rec.setValue({ fieldId: "custrecord_clgx_xc_type",                value: 2                   });
            } else {
                rec.setValue({ fieldId: "custrecord_clgx_xc_type",                value: 1                   });
            }

            var ns_xc_id = rec.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });
            return ns_xc_id;
        }

        function get_a_ns_xc_id (provider_id, facility_id, port_label,region,ns_xc){
            //log.debug({ title: "debug", details: ' | provider_id : ' + provider_id + " | facility_id: " + facility_id  + " | port_label : " + port_label  + " |"});

            var a_ns_xc_id = null;
            var mySearch = search.load({ type: 'customrecord_cologix_crossconnect', id: 'customsearch_clgx_xcex_find_a_end_xc'});
            mySearch.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_bp_provider", operator: "ANYOF", values: provider_id }));
            // mySearch.filters.push(search.createFilter({ name: "internalid", operator: "NONEOF", values: ns_xc }));
            if(provider_id == 2||provider_id == 5){ //Azure|| Oracle
                mySearch.filters.push(search.createFilter({ name: "custrecord_clgx_xc_cloud_port_label", operator: "IS", values: port_label }));
            }else if(provider_id == 4) {
                mySearch.filters.push(search.createFilter({ name: "custrecord_clgx_xc_cloud_port_label", operator: "IS", values: region }));
            }else if(provider_id == 3) {
                var nni_facilities = xcex.get_nni_facilities(provider_id, facility_id,port_label);
                mySearch.filters.push(search.createFilter({ name: "custrecord_clgx_xc_facility", operator: "ANYOF", values: nni_facilities }));

            }
            else {
                var nni_facilities = xcex.get_nni_facilities(provider_id, facility_id);

                mySearch.filters.push(search.createFilter({ name: "custrecord_clgx_xc_facility", operator: "ANYOF", values: nni_facilities }));
                //mySearch.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_cloud_prov_sold_bw", operator: "GREATERTHAN", values: -1 }));
            }
            log.debug({ title: "debug", details: ' | provider_id : ' + provider_id + " | facility_id: " + facility_id  + " | port_label : " + port_label  + " |"});

            var results = mySearch.run().getRange({start: 0,end: 1});
            if(results && results.length == 1){
                a_ns_xc_id = parseInt(results[0].getValue(results[0].columns[0]));
                //log.debug({ title: "debug", details: ' | a_ns_xc_id : ' + a_ns_xc_id  + " |"});
            };
            return a_ns_xc_id;
        }

        function create_vxc (customer_id, vxc) {

            //log.debug({ title: "debug", details: ' | vxc : ' + JSON.stringify(vxc) + " |"});

            var rec = record.create({ type: "customrecord_cologix_vxc"});
            rec.setValue({ fieldId: "custrecord_cologix_vxc_facility",        value: vxc.facility_id           });
            rec.setValue({ fieldId: "custrecord_clgx_cloud_provider",         value: vxc.provider_id           });
            rec.setValue({ fieldId: "custrecord_clgx_vxc_speed",              value: vxc.speed_id              });
            rec.setValue({ fieldId: "custrecord_clgx_vxc_bandwidth",          value: vxc.speed_id              });
            rec.setValue({ fieldId: "custrecord_cologix_vxc_type",            value: 32                        });
            rec.setValue({ fieldId: "custrecord_cologix_vxc_status",          value: 1                         });
            rec.setValue({ fieldId: "custrecord_cologix_vxc_carrier_name",    value: 2790                      });
            rec.setValue({ fieldId: "custrecord_clgx_vxc_user_defined_name",  value: vxc.name                  });
            //rec.setValue({ fieldId: "custrecord_clgx_vxcex_tagged",           value: vxc.tagged                });
            rec.setValue({ fieldId: "custrecord_clgx_vxcex_customer_id",      value: customer_id               });
            if(vxc.azure_primary_port != ""){
                rec.setValue({ fieldId: "custrecord_clgx_vxc_cc_port_label",  value: vxc.azure_primary_port});
            }
            if(vxc.azure_secondary_port != ""){
                rec.setValue({ fieldId: "custrecord_clgx_vxc_cc_port_label",  value: vxc.azure_secondary_port});
            }
            rec.setValue({ fieldId: "custrecord_clgx_vxc_skey",               value: vxc.cloud_reference_id    });
            //rec.setValue({ fieldId: "custrecord_clgx_vxlan_id",               value: vxc.vxlan                 }); // to get latter form mdso inventory
            // A
            rec.setValue({ fieldId: "custrecord_clgx_cloud_connect_port",     value: vxc.xcs[0].ns_xc_id       });
            rec.setText({ fieldId: "custrecord_clgx_z_vlan_tag",              text:  vxc.xcs[1].vlan_tagging               });
            rec.setText({ fieldId: "custrecord_clgx_a_vlan_tag",              text:  vxc.xcs[0].vlan_tagging               });


            rec.setValue({ fieldId: "custrecord_clgx_vxc_a_egress_type",      value: 1                         }); // hard coded to 1 for now
            //rec.setValue({ fieldId: "custrecord_clgx_vxc_msft_stag",          value: vxc.xcs[0].egress_vlan    }); // to get latter form mdso inventory
            // Z
            rec.setValue({ fieldId: "custrecord_clgx_cloud_connect_port_z",   value: vxc.xcs[1].ns_xc_id       });
            rec.setValue({ fieldId: "custrecord_clgx_vxc_z_egress_type",      value: 1                         }); // hard coded to 1 for now
            //rec.setValue({ fieldId: "custrecord_clgx_vxc_z_egress_vlan",      value: vxc.xcs[0].egress_vlan    }); // to get latter form mdso inventory

            var ns_vxc_id = rec.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });
            if(vxc.tagged  == false){ // update tagged flag on XC to false
                var req_id = record.submitFields({
                    type: 'customrecord_cologix_crossconnect',
                    id: vxc.xcs[1].ns_xc_id,
                    values: { custrecord_clgx_xcex_tagged : false },
                    options: {enableSourcing: false,ignoreMandatoryFields : true}
                });
            }
            // increase sold bandwidth on A Side XC
            xcex.update_bandwidth (vxc.xcs[1].ns_xc_id);

            return ns_vxc_id;
        }

        function create_transactions (order){

            // create opportunity ============================================================================================================================================
            var oppty = record.create({ type: "opportunity"});
            oppty.setValue({ fieldId: "title",                                            value: order.title                         });
            oppty.setValue({ fieldId: "entity",                                           value: order.customer_id                   });
            oppty.setValue({ fieldId: "custbody_clgx_so_portal_req",                      value: order.contact_id                    });
            oppty.setValue({ fieldId: "location",                                         value: order.location_id                   });
            oppty.setValue({ fieldId: "forecasttype",                                     value: 3                                   });
            oppty.setValue({ fieldId: "custbody_cologix_opp_sale_type",                   value: 2                                   });
            oppty.setValue({ fieldId: "leadsource",                                       value: 73127                               });
            var line_ndx = 0;

            for ( var i = 0; order.items != null && i < order.items.length; i++ ) {

                if(order.items[i].type == "xc"){
                    var description = order.items[i].prm_name;
                    if(order.items[i].redundant){
                        var description = order.items[i].prm_name + " \n " + order.items[i].sec_name;
                    }
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'item',                line: line_ndx, value: order.items[i].item_id       });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'quantity',            line: line_ndx, value: 1                            });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'rate',                line: line_ndx, value: order.items[i].mrc           });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'amount',              line: line_ndx, value: order.items[i].mrc           });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'location',            line: line_ndx, value: order.items[i].location_id   });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'description',         line: line_ndx, value: description                  });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'price',               line: line_ndx, value: -1                           }); // not sure what 'price' is
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'pricelevels',         line: line_ndx, value: -1                           }); // hard coded for JAX for now
                    //oppty.setSublistValue({sublistId: 'item', fieldId: 'taxcode',             line: line_ndx, value: 498                          }); // hard coded for JAX for now
                    order.items[i].line_id = line_ndx;
                    line_ndx = line_ndx + 1;

                    //if(order.items[i].nrc > 0){
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'item',            line: line_ndx, value: 244                          });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'quantity',        line: line_ndx, value: 1                            });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'rate',            line: line_ndx, value: order.items[i].nrc           });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'amount',          line: line_ndx, value: order.items[i].nrc           });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'location',        line: line_ndx, value: order.items[i].location_id   });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'description',     line: line_ndx, value: description                  });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'price',           line: line_ndx, value: -1                           }); // not sure what 'price' is
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'pricelevels',     line: line_ndx, value: -1                           }); // hard coded for JAX for now
                    //oppty.setSublistValue({sublistId: 'item', fieldId: 'taxcode',         line: line_ndx, value: 498                          }); // hard coded for JAX for now
                    line_ndx = line_ndx + 1;
                    //}
                }
                if(order.items[i].type == "vxc"){
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'item',                line: line_ndx, value: order.items[i].item_id       });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'quantity',            line: line_ndx, value: 1                            });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'rate',                line: line_ndx, value: order.items[i].mrc           });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'amount',              line: line_ndx, value: order.items[i].mrc           });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'location',            line: line_ndx, value: order.items[i].location_id   });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'description',         line: line_ndx, value: order.items[i].name          });
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'price',               line: line_ndx, value: -1                           }); // not sure what 'price' is
                    oppty.setSublistValue({sublistId: 'item', fieldId: 'pricelevels',         line: line_ndx, value: -1                           }); // hard coded for JAX for now
                    //oppty.setSublistValue({sublistId: 'item', fieldId: 'taxcode',             line: line_ndx, value: 498                          }); // hard coded for JAX for now
                    order.items[i].line_id = line_ndx;
                    line_ndx = line_ndx + 1;
                }
            }

            var oppty_id = oppty.save();
            order.oppty_id = oppty_id;

            // create proposal ============================================================================================================================================
            var terms = search.lookupFields({
                type    : 'customrecord_clgx_contract_terms',
                id      : 2,
                columns : ['custrecord_clgx_contract_terms_body']
            });
            termsBody = terms.custrecord_clgx_contract_terms_body || "";
            var estimate = record.transform({
                fromType: record.Type.OPPORTUNITY,
                fromId: oppty_id,
                toType: record.Type.ESTIMATE
            });
            estimate.setValue({ fieldId: "custbody_clgx_contract_terms_contact",          value: order.contact_id                    });
            estimate.setValue({ fieldId: "custbody_clgx_order_notes",                     value: order.title                         });
            estimate.setValue({ fieldId: "opportunity",                                   value: order.oppty_id                      });
            estimate.setValue({ fieldId: "custbodyclgx_aa_type",                          value: 3                                   });
            estimate.setValue({ fieldId: "custbody_cologix_annual_accelerator",           value: 3                                   });
            estimate.setValue({ fieldId: "entitystatus",                                  value: 11                                  });
            estimate.setValue({ fieldId: "custbody_clgx_contract_terms_title",            value: 2                                   });
            estimate.setValue({ fieldId: "otherrefnum",                                   value: order.po                            });
            estimate.setValue({ fieldId: "custbody_clgx_contract_terms_body",             value: termsBody                           });
            if(order.billing != ""){
                estimate.setValue({ fieldId: "billaddress",                               value: order.billing                       });
            }
            var estimate_id = estimate.save();
            order.estimate_id = estimate_id;

            // create sales order ============================================================================================================================================
            var proposal = search.lookupFields({
                type    : 'estimate',
                id      : order.estimate_id,
                columns : ['tranid']
            });
            var pname = proposal.tranid;
            var sname = 'SO' + pname + '-1';

            var clocation_id = get_consolidate_location (order.location_id);

            var so = record.transform({
                fromType: record.Type.ESTIMATE,
                fromId: estimate_id,
                toType: record.Type.SALES_ORDER
            });
            so.setValue({ fieldId: "tranid",                                   value: sname                 });
            so.setValue({ fieldId: "custbody_clgx_consolidate_locations",      value: clocation_id          });
            so.setValue({ fieldId: "custbody_clgx_ready_to_uplift",            value: false                 });
            var so_id = so.save();
            order.so_id = so_id;

            var so = record.load({type: 'salesorder', id: so_id});

            // put quantity 1 on all NRC lines
            var numLines = so.getLineCount({
                sublistId: 'item'
            });
            for ( var i = 0; i < numLines; i++ ) {
                var item_id = so.getSublistValue({
                    sublistId: 'item',
                    fieldId: 'item',
                    line: i
                });
                if(item_id == 244){ // NRC Install
                    so.setSublistValue({sublistId: 'item', fieldId: 'custcol_clgx_qty2print',    line: i, value: 1 });
                    so.setSublistValue({sublistId: 'item', fieldId: 'billingschedule',           line: i, value: 17 });
                }
            }

            for ( var i = 0; order.items != null && i < order.items.length; i++ ) {

                // create service
                var service_id = create_service (order.customer_id, so_id, order.facility_id);
                order.items[i].service_id = service_id;

                if(order.items[i].type == "xc"){
                    // update service and SO on XCs
                    var xc_req_id = record.submitFields({
                        type: 'customrecord_cologix_crossconnect',
                        id: order.items[i].prm_ns_xc_id,
                        values: {
                            custrecord_xconnect_service_order : so_id,
                            custrecord_cologix_xc_service     : service_id
                        },
                        options: {enableSourcing: false,ignoreMandatoryFields : true}
                    });
                    if(order.items[i].redundant){
                        var xc_req_id = record.submitFields({
                            type: 'customrecord_cologix_crossconnect',
                            id: order.items[i].sec_ns_xc_id,
                            values: {
                                custrecord_xconnect_service_order : so_id,
                                custrecord_cologix_xc_service     : service_id
                            },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                }
                if(order.items[i].type == "vxc"){
                    var xc_req_id = record.submitFields({
                        type: 'customrecord_cologix_vxc',
                        id: order.items[i].ns_vxc_id,
                        values: {
                            custrecord_cologix_service_order   : so_id,
                            custrecord_cologix_vxc_service     : service_id
                        },
                        options: {enableSourcing: false,ignoreMandatoryFields : true}
                    });
                }

                // update & change item line fields
                so.setSublistValue({sublistId: 'item', fieldId: 'custcol_clgx_so_col_service_id',    line: order.items[i].line_id, value: service_id                 });
                so.setSublistValue({sublistId: 'item', fieldId: 'quantity',                          line: order.items[i].line_id, value: 120                        });
                so.setSublistValue({sublistId: 'item', fieldId: 'custcol_clgx_qty2print',            line: order.items[i].line_id, value: 1                          });
                so.setSublistValue({sublistId: 'item', fieldId: 'billingschedule',                   line: order.items[i].line_id, value: 3                          });
            }
            var so_id = so.save();

            return order;
        }

        function get_consolidate_location (location_id){
            var clocation_id = null;
            var filters = [];
            filters.push(search.createFilter({ name: "isinactive", operator: "is", values: false }));
            filters.push(search.createFilter({ name: "custrecord_clgx_consolidate_location_ids", operator: "anyof", values: location_id }));
            var columns = [];
            columns.push(search.createColumn({ name: "internalid" }));
            var results = search.create({ type: "customrecord_clgx_consolidate_locations", filters: filters, columns: columns });
            results.run().each(function(result) {
                if(clocation_id == null){
                    clocation_id = result.getValue("internalid");
                }
            });
            return clocation_id;
        }

        function create_service (customer_id, so_id, facility_id){
            var rec_id = record.load({type: 'customrecord_clgx_auto_generated_numbers', id: 1});
            var int_id = parseInt(rec_id.getValue({fieldId: 'custrecord_clgx_auto_generated_number'})) + 1;
            rec_id.setValue({fieldId: 'custrecord_clgx_auto_generated_number',value: int_id });
            rec_id.save({enableSourcing: false,ignoreMandatoryFields: true});
            var str_id = new String(int_id);
            var str_prefix = 'S00000000';
            var new_job_id = str_prefix.substring(0,9-parseInt(str_id.length)) + str_id;
            /*
            var fac = search.lookupFields({
                type    : 'customrecord_cologix_facility',
                id      : facility_id,
                columns : ['custrecord_cologix_location_subsidiary']
            });
            //log.debug({ title: "debug", details: ' | fac : ' + JSON.stringify(fac) + " |"});
            var subsidiary_id = 0;
            if(fac && fac.custrecord_cologix_location_subsidiary && fac.custrecord_cologix_location_subsidiary.length > 0 && fac.custrecord_cologix_location_subsidiary[0] != null){
                subsidiary_id  = parseInt(fac.custrecord_cologix_location_subsidiary[0].value);
            }
            */
            var job = record.create({ type: "job"});
            job.setValue({ fieldId: "entityid",                            value: new_job_id                         });
            job.setValue({ fieldId: "custentity_cologix_service_order",    value: so_id                              });
            job.setValue({ fieldId: "parent",                              value: customer_id                        });
            //job.setValue({ fieldId: "subsidiary",                          value: subsidiary_id                      });
            job.setValue({ fieldId: "custentity_cologix_facility",         value: facility_id                        });
            job.setValue({ fieldId: "custentity_cologix_int_prj",          value: 2                                  });
            var job_id = job.save();

            return job_id;
        }

        function update_queues (order){
            // add sales order on each queue xc & vxc
            for ( var i = 0; order.items != null && i < order.items.length; i++ ) {
                var qu_id = 0;
                if (order.items[i].type == "xc"){
                    if (order.items[i].prm_ns_qu_id > 0){
                        var qu_req = record.submitFields({
                            type: 'customrecord_clgx_xcex_queue_v_xcs',
                            id: order.items[i].prm_ns_qu_id,
                            values: {
                                custrecord_clgx_xcex_queue_so : order.so_id,
                                custrecord_clgx_xcex_queue_service : order.items[i].service_id
                            },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                    if (order.items[i].sec_ns_qu_id > 0){
                        var qu_req = record.submitFields({
                            type: 'customrecord_clgx_xcex_queue_v_xcs',
                            id: order.items[i].sec_ns_qu_id,
                            values: {
                                custrecord_clgx_xcex_queue_so : order.so_id,
                                custrecord_clgx_xcex_queue_service : order.items[i].service_id
                            },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                }
                if (order.items[i].type == "vxc"){
                    if (order.items[i].ns_qu_id > 0){
                        var qu_req = record.submitFields({
                            type: 'customrecord_clgx_xcex_queue_v_xcs',
                            id: order.items[i].ns_qu_id,
                            values: {
                                custrecord_clgx_xcex_queue_so : order.so_id,
                                custrecord_clgx_xcex_queue_service : order.items[i].service_id
                            },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                }
            }
            return order;
        }

        function calculate_transactions_totals (oppty_id, estimate_id, so_id, customer_id){
            // trigger opportunity totals
            var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
            scrpt.scriptId     = "customscript_clgx_ss_transact_totals";
            scrpt.params       = {
                custscript_transact_totals_customer   : parseInt(customer_id),
                custscript_transact_totals_rec_type   : "oppty" ,
                custscript_transact_totals_rec_id     : parseInt(oppty_id)
            };
            scrpt.submit();
            // trigger proposal totals
            var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
            scrpt.scriptId     = "customscript_clgx_ss_transact_totals";
            scrpt.params       = {
                custscript_transact_totals_customer   : parseInt(customer_id),
                custscript_transact_totals_rec_type   : "proposal" ,
                custscript_transact_totals_rec_id     : parseInt(estimate_id)
            };
            scrpt.submit();
            // trigger so totals
            var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
            scrpt.scriptId     = "customscript_clgx_ss_transact_totals";
            scrpt.params       = {
                custscript_transact_totals_customer   : parseInt(customer_id),
                custscript_transact_totals_rec_type   : "salesorder" ,
                custscript_transact_totals_rec_id     : parseInt(so_id)
            };
            scrpt.submit();
            return true;
        }

        function send_customer_email (order){
            // first email
            var today = moment().format("MM/DD/YYYY HH:mm:ss");
            var cust_fields = search.lookupFields({
                type    : 'customer',
                id      : order.customer_id,
                columns : ['custentity_clgx_cust_ccm']
            });
            var ccmrep = "";
            if(cust_fields && cust_fields.custentity_clgx_cust_ccm && cust_fields.custentity_clgx_cust_ccm.length > 0 && cust_fields.custentity_clgx_cust_ccm[0] != null){
                ccmrep  = cust_fields.custentity_clgx_cust_ccm[0].text;
            }

            var cont_fields = search.lookupFields({
                type    : 'contact',
                id      : order.contact_id,
                columns : ['entityid', 'custentity_clgx_modifier']
            });
            var contact = cont_fields.entityid;
            var user = "";
            if(cont_fields && cont_fields.custentity_clgx_modifier && cont_fields.custentity_clgx_modifier.length > 0 && cont_fields.custentity_clgx_modifier[0] != null){
                user  = cont_fields.custentity_clgx_modifier[0].text;
            }

            var so_fields = search.lookupFields({
                type    : 'salesorder',
                id      : order.so_id,
                columns : ['tranid', 'entity', 'salesrep']
            });
            var so           = so_fields.tranid;
            var custarr      = (so_fields.entity[0].text).split(":");
            var customer     = custarr[(custarr.length-1)].trim();
            var salesrep     = so_fields.salesrep[0].text;
            var installed    = so_fields.installed;

            var subject = "New Order " + so + " - " + customer;
            var fileObj  = file.load({id: 16047019});
            var html     = fileObj.getContents();
            var server    = order.server || "";
            html = html.replace(new RegExp('{contact}','g'), contact);
            html = html.replace(new RegExp('{customer}','g'), customer);
            html = html.replace(new RegExp('{so}','g'), so);
            html = html.replace(new RegExp('{title}','g'), order.title);
            html = html.replace(new RegExp('{user}','g'), user);
            html = html.replace(new RegExp('{today}','g'), today);
            html = html.replace(new RegExp('{server}','g'), server);
            html = html.replace(new RegExp('{salesrep}','g'), salesrep);

            email.send({
                author: 432742,
                recipients: order.contact_id,
                subject: subject,
                body: html,
                relatedRecords: {
                    entityId: order.contact_id
                }
            });

            return true;
        }

        function create_pdf_contract (estimate_id, contact_id){
            // trigger proposal pdf contract
            var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
            scrpt.scriptId     = "customscript_clgx_ss_proposal_pdf";
            scrpt.params       = {
                custscript_proposal_pdf_proposalid   : estimate_id,
                custscript_proposal_pdf_userid       : contact_id,
                custscript_proposal_pdf_quote        : false,
                custscript_proposal_pdf_contract     : true,
                custscript_proposal_pdf_no_email     : 1
            };
            scrpt.submit();
            return true;
        }

        return {
            execute: execute
        };

    });