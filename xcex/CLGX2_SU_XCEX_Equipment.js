/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           10/15/2018
  @description:    Trigger MDSO Southbounds actions
  Script Name:     CLGX2_SU_XCEX_Equipment
  Script ID:       customscript_clgx2_su_xcex_equipment
  Link:            /app/common/scripting/script.nl?id=1756
*/

define([
	"N/https",
	"N/record", 
	"N/runtime",
	"N/search",
	"N/task",
	"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"],
function(https, record, runtime, search, task, xcex) {
	
	function beforeLoad(context) {
		if(runtime.executionContext == "USERINTERFACE" && (context.type == "view" || context.type == "edit")) {
			
		}
	}

	function beforeSubmit(context) {
		if(runtime.executionContext == "USERINTERFACE") {
			if(context.type == "create" || context.type == "edit") {
				
			}
		}
	}

    return {
    	beforeLoad: beforeLoad,
    	beforeSubmit: beforeSubmit
    };
	
    
});
