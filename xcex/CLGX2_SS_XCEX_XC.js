/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           10/15/2018
  @description:    MDSO Southbounds for XCs
  Script Name:     CLGX2_SS_XCEX_XC
  Script ID:       customscript_clgx2_ss_xcex_xc
  Link:            /app/common/scripting/script.nl?id=1741
*/

define([
	"N/file",
	"N/https",
	"N/record", 
	"N/task", 
	"N/runtime", 
	"N/search",
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
	],
function(file, https, record, task, runtime, search, moment, xcex) {
	
    function execute(context) {
    	
		var current  = runtime.getCurrentScript();
    	var ns_qu_id = current.getParameter("custscript_clgx_xcex_queue_xc");
    	var today = moment().format("MM/DD/YYYY,HH:mm:ss");
    	var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
    	var que = {
    			"ns_xc_id"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_xc'}) || 0),
    			"ns_qu_id"   : ns_qu_id,
    			"file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
    			"bp_xc_id"   : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
    			"provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
    			"status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
    	};
    	log.debug({ title: "debug", details: ' | que : ' + JSON.stringify(que) + " |"});
		var fileObj     = file.load({id: que.file_id});
		var history     = JSON.parse(fileObj.getContents());
		var file_name   = fileObj.name;
		
		var xc_fields = search.lookupFields({
		    type    : 'customrecord_cologix_crossconnect',
		    id      : que.ns_xc_id,
		    columns : ['name', 'custrecord_clgx_a_end_port', 'custrecord_clgx_xcex_xc_speed', 'custrecord_clgx_xc_cloud_port_label', 'custrecord_clgx_xcex_xc_migrate']
		});
		log.debug({ title: "debug", details: ' | xc_fields : ' + JSON.stringify(xc_fields) + " |"});
		var portid  = parseInt(xc_fields.custrecord_clgx_a_end_port[0].value);
		var portstr = xc_fields.custrecord_clgx_a_end_port[0].text;
		var portarr  = portstr.split("|");
		
		var speed   = parseInt(xc_fields.custrecord_clgx_xcex_xc_speed[0].value);
		var migrated   = xc_fields.custrecord_clgx_xcex_xc_migrate;
		history.provisioning.posting.ns_post.migrated  = migrated;
		
		var provider_port = xc_fields.custrecord_clgx_xc_cloud_port_label || "";
		history.provisioning.posting.ns_post.provider_port  = provider_port;
		
		var equip_fields = search.lookupFields({
		    type    : 'customrecord_clgx_active_port',
		    id      : portid,
		    columns : ['custrecord_clgx_active_port_equipment']
		});
		var equipname   = equip_fields.custrecord_clgx_active_port_equipment[0].text;
		var equiparray  = equipname.split("-");

		// populate to history initial request data to be posted
		if(history.provisioning.posting.ns_post.switches.length == 0){ // add switch only on first post
			history.provisioning.posting.ns_post.switches.push({
				"switch_id"  : equiparray[0],
				"port"       : portarr[1],
				"speed"      : speed
			});
		}
		
	    var xcex_head  = xcex.get_headers();
	    var xcex_url   = xcex.get_url_order_xc();
	    //var vxcex_url  = xcex.get_url_order_vxc();
	    
	    var ping = null;
	    try {
			var ping = xcex.ping_mdso(); 
		    if(ping){
		    	// check if this XC has at least one VXC - if not do not post yet
				var arr_vxcs = [];
			    var results = search.load({ type: 'customrecord_cologix_vxc', id: "customsearch_clgx_xcex_xc_has_vxcs" });
			    results.filters.push(search.createFilter({ name: "custrecord_clgx_cloud_connect_port_z", operator: "ANYOF", values: que.ns_xc_id }));
				results.run().each(function(result) {
					arr_vxcs.push(parseInt(result.getValue(result.columns[0])));
					return true;
				});
				
				if (arr_vxcs.length > 0) { // customer XC must have VXCs
					// try the initial post
					var req = https.post({
				        url       : xcex_url,
				        headers   : xcex_head,
				        body      : JSON.stringify(history.provisioning.posting.ns_post)
				    });
					var resp = JSON.parse(req.body);
					log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
					history.provisioning.posting.posted.push(today);
					history.provisioning.posting.source.push("NS");
					
					if (resp && resp.status){
						resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
						if (resp.status == 'provisioning' || resp.status == 'migrate'){
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 2}); // Queue BP provisioning
							history.bp_xc_id = resp.bp_id || "";
							history.provisioning.posting.ns_post.bp_xc_id = resp.bp_id || "";
							history.provisioning.provisioning.bpupdt.push(resp.bp_timestamp);
							history.provisioning.provisioning.posted.push(today);
							history.provisioning.provisioning.source.push("BP_Resp");
						}
						else if (resp.status == 'failed'){
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
							history.bp_xc_id = resp.bp_id || "";
							history.provisioning.posting.ns_post.bp_xc_id = resp.bp_id || ""; // exist if failed ??
							history.provisioning.failed.bpupdt.push(resp.bp_timestamp); // exist if failed ??
							history.provisioning.failed.posted.push(today);
							history.provisioning.failed.source.push("BP_Resp");
							history.provisioning.failed.errors = resp.error;
						}
						else { // no other response possible
						}
					}
					else { // mdso did not respond to post, repeat on next script run
						queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 1}); // Queue BP nonexisting
					}
				} else {
					queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 1}); // Queue BP nonexisting
				}

			} else {  // mdso not available, repeat on next script run
				queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 1}); // Queue BP nonexisting
		    }
	    }
		catch (error) {
			var err = "";
			if (error.getDetails != undefined){
			    err = error.getDetails();
			}
			else{
			    err = error.toString();
			}
			queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 1}); // Queue BP nonexisting
			
			log.debug({ title: "XCEX", details: " | error : " + err +  " |"});
			log.debug({ title: "XCEX", details: "===================================== ERROR ==================================="});
		}
	    
		// update history on queue record and save queue record
		//queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
		var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

		var fileObj = file.create({
  			name: file_name,
  		    fileType: file.Type.PLAINTEXT,
  		    contents: JSON.stringify(history),
  		    encoding: file.Encoding.UTF8,
  		    folder: 10639242
  		});
  		var file_id = fileObj.save();
  		
    	return true;
    }

    return {
        execute: execute
    };

});
