/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           10/15/2018
  @description:    MDSO Northbound POSTs for XCs
  Script Name:     CLGX2_RL_XCEX_Inactivate
  Script ID:       customscript_clgx2_rl_xcex_inactivate
  Link:            /app/site/hosting/restlet.nl?script=1728&deploy=1
*/


define([
		"N/record",
		"N/search",
		"N/task"
	],
function (record, search, task) {
   
    function doPost(req) {
    	
    	log.debug({ title: "debug", details: ' | req : ' + JSON.stringify(req) + " |"});
    	
    	//try {
			if (req.type == "xc"){
				
				var xc = search.lookupFields({
				    type    : 'customrecord_cologix_crossconnect',
				    id      : req.id,
				    columns : ['custrecord_clgx_xcex_queue_id', 'custrecord_clgx_a_end_port']
				});
				
				var ns_qu_id  = parseInt(xc.custrecord_clgx_xcex_queue_id[0].value) || 0;
				var port_id = 0;
				if(xc && xc.custrecord_clgx_a_end_port && xc.custrecord_clgx_a_end_port.length > 0 && xc.custrecord_clgx_a_end_port[0] != null){
					port_id  = parseInt(xc.custrecord_clgx_a_end_port[0].value);
				}
				
				var qu = search.lookupFields({
				    type    : 'customrecord_clgx_xcex_queue_v_xcs',
				    id      : ns_qu_id,
				    columns : ['custrecord_clgx_xcex_queue_bp_status']
				});
				var qu_stat_id = 0;
				if(qu && qu.custrecord_clgx_xcex_queue_bp_status && qu.custrecord_clgx_xcex_queue_bp_status.length > 0 && qu.custrecord_clgx_xcex_queue_bp_status[0] != null){
					qu_stat_id  = parseInt(qu.custrecord_clgx_xcex_queue_bp_status[0].value);
				}
				
				if(qu_stat_id > 2){ // anything after provisioning
					var qu_req_id = record.submitFields({
	    	    	    type     : 'customrecord_clgx_xcex_queue_v_xcs',
	    	    	    id       : ns_qu_id,
	    	    	    values   : {custrecord_clgx_xcex_queue_bp_status : 7}, // inactivating
	    	    	    options  : {enableSourcing: false,ignoreMandatoryFields : true}
	    	    	});
				}
				else {
					var qu = record.submitFields({
	    	    	    type    : 'customrecord_clgx_xcex_queue_v_xcs',
	    	    	    id       : ns_qu_id,
	    	    	    values   : {
	    	    	    		isinactive : true,
	    	    	    		custrecord_clgx_xcex_queue_bp_status : 8
	    	    	    },
	    	    	    options  : {enableSourcing: false,ignoreMandatoryFields : true}
	    	    	});
					var xc = record.submitFields({
	    	    	    type     : 'customrecord_cologix_crossconnect',
	    	    	    id       : req.id,
	    	    	    values   : {isinactive : true, custrecord_clgx_a_end_port: null},
	    	    	    options  : {enableSourcing: false,ignoreMandatoryFields : true}
	    	    	});
					if(port_id > 0){
						var port = record.submitFields({
		    	    	    type: 'customrecord_clgx_active_port',
		    	    	    id: port_id,
		    	    	    values: {
		    	    	    	custrecord_clgx_active_port_status     : 2,
		    	    	    	custrecord_clgx_active_port_sequence   : null,
		    	    	    	custrecord_clgx_active_port_xc         : null,
		    	    	    	custrecord_clgx_active_port_service    : null,
		    	    	    	custrecord_clgx_active_port_so         : null,
		    	    	    	custrecord_clgx_active_port_customer   : null
		    	    	    },
		    	    	    options: {enableSourcing: false,ignoreMandatoryFields : true}
		    	    	});
					}
					// inactivate all VXCs of this XC
					var results = search.load({ type: 'customrecord_cologix_vxc', id: "customsearch_clgx_xcex_xc_has_vxcs" });
				    results.filters.push(search.createFilter({ name: "custrecord_clgx_cloud_connect_port_z", operator: "ANYOF", values: req.id }));
					results.run().each(function(result) {
						var vxc_id = parseInt(result.getValue(result.columns[0]));
						var qu_id = parseInt(result.getValue(result.columns[1])) || 0;
						if(qu_id > 0){
							var qu = record.submitFields({
			    	    	    type     : 'customrecord_clgx_xcex_queue_v_xcs',
			    	    	    id       : qu_id,
			    	    	    values   : {
		    	    	    		isinactive : true,
		    	    	    		custrecord_clgx_xcex_queue_bp_status : 8
			    	    	    },
			    	    	    options  : {enableSourcing: false,ignoreMandatoryFields : true}
			    	    	});
						}
						var qu = record.submitFields({
		    	    	    type: 'customrecord_cologix_vxc',
		    	    	    id: vxc_id,
		    	    	    values: {
		    	    	    	isinactive : true,
		    	    	    	custrecord_clgx_vxc_provider_switch_1 : null,
		    	    	    	custrecord_clgx_vxc_access_switch_1 : null,
		    	    	    	custrecord_clgx_vxc_access_switch_2 : null,
		    	    	    	custrecord_clgx_vxc_access_switch_3 : null,
		    	    	    	custrecord_clgx_vxc_speed : null,
		    	    	    	custrecord_clgx_vxc_bandwidth : null,
		    	    	    	custrecord_clgx_cloud_connect_port : null,
		    	    	    	custrecord_clgx_cloud_connect_port_z : null,
		    	    	    	custrecord_clgx_vxc_stag : null

		    	    	    },
		    	    	    options: {enableSourcing: false,ignoreMandatoryFields : true}
		    	    	});
						return true;
					});
				}
			}
			if (req.type == "vxc"){
				
				var vxc = search.lookupFields({
				    type    : 'customrecord_cologix_vxc',
				    id      : req.id,
				    columns : ['custrecord_clgx_vxcex_queue_id']
				});
				var ns_qu_id  = parseInt(vxc.custrecord_clgx_vxcex_queue_id[0].value);

				var qu = search.lookupFields({
				    type    : 'customrecord_clgx_xcex_queue_v_xcs',
				    id      : ns_qu_id,
				    columns : ['custrecord_clgx_xcex_queue_bp_status']
				});
				
				var qu_stat_id = 0;
				if(qu && qu.custrecord_clgx_xcex_queue_bp_status && qu.custrecord_clgx_xcex_queue_bp_status.length > 0 && qu.custrecord_clgx_xcex_queue_bp_status[0] != null){
					qu_stat_id  = parseInt(qu.custrecord_clgx_xcex_queue_bp_status[0].value);
				}
				
				if(qu_stat_id > 2){ // anything after provisioning
					var qu_req_id = record.submitFields({
	    	    	    type     : 'customrecord_clgx_xcex_queue_v_xcs',
	    	    	    id       : ns_qu_id,
	    	    	    values   : {custrecord_clgx_xcex_queue_bp_status : 7}, // inactivating
	    	    	    options  : {enableSourcing: false,ignoreMandatoryFields : true}
	    	    	});
				} 
				else {
					var qu = record.submitFields({
	    	    	    type     : 'customrecord_clgx_xcex_queue_v_xcs',
	    	    	    id       : ns_qu_id,
	    	    	    values   : {
    	    	    		isinactive : true,
    	    	    		custrecord_clgx_xcex_queue_bp_status : 8
	    	    	    },
	    	    	    options  : {enableSourcing: false,ignoreMandatoryFields : true}
	    	    	});
					var qu = record.submitFields({
	    	    	    type: 'customrecord_cologix_vxc',
	    	    	    id: req.id,
	    	    	    values: {
	    	    	    	isinactive : true,
	    	    	    	custrecord_clgx_vxc_provider_switch_1 : null,
	    	    	    	custrecord_clgx_vxc_access_switch_1 : null,
	    	    	    	custrecord_clgx_vxc_access_switch_2 : null,
	    	    	    	custrecord_clgx_vxc_access_switch_3 : null,
	    	    	    	custrecord_clgx_vxc_speed : null,
	    	    	    	custrecord_clgx_vxc_bandwidth : null,
	    	    	    	custrecord_clgx_cloud_connect_port : null,
	    	    	    	custrecord_clgx_cloud_connect_port_z : null,
	    	    	    	custrecord_clgx_vxc_stag : null
	    	    	    },
	    	    	    options: {enableSourcing: false,ignoreMandatoryFields : true}
	    	    	});
				}
			}

			// trigger CLGX2_SS_XCEX_Inactivate_Case script
	        var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
	        scrpt.scriptId     = "customscript_clgx2_ss_xcex_inactive_case";
	        scrpt.params       = { custscript_clgx_xcex_inactive_case_req: JSON.stringify(req) };
	        scrpt.submit();
		/*}
		catch (error) {
			var err = "";
	        if (error.getDetails != undefined){
	            err = error.getCode();
	        } else {
	            err = error.toString();
	        }
			log.debug({ title: "debug", details: ' | error : ' + JSON.stringify(err) + " |"});
			return JSON.stringify(err) ;
		}*/

        
		return req;
    }
    return {
        post: doPost
    };

});
