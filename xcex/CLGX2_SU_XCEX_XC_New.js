/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           10/15/2018
  @description:    Trigger MDSO Southbounds actions
  Script Name:     CLGX2_SU_XCEX_XC
  Script ID:       customscript_clgx2_su_xcex_xc
  Link:            /app/common/scripting/script.nl?id=1737
*/

define([
	"N/record", 
	"N/task", 
	"N/runtime",
	"N/search",
	"N/task",
	"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"],
function(record, task, runtime, search, task, xcex) {
	
	function beforeLoad(context) {
		
		if(runtime.executionContext == "USERINTERFACE" && (context.type == "view" || context.type == "edit")) {
			
    		var rec    = context.newRecord;
    		var xcid   = rec.id;
    		var quid   = parseInt(rec.getValue("custrecord_clgx_xcex_queue_id")) || 0;
    		var type   = parseInt(rec.getValue("custrecord_cologix_xc_type") || 0);
    		
    		if(quid > 0 && type == 32){ // only for Cloud Exchange and if queue record exist
    			
    			var queue   = search.lookupFields({
    			    type    : 'customrecord_clgx_xcex_queue_v_xcs',
    			    id      : quid,
    			    columns : ['custrecord_clgx_xcex_queue_bp_id', 'custrecord_clgx_xcex_queue_bp_status', 'custrecord_clgx_xcex_queue_file_id']
    			});
    			if(queue.custrecord_clgx_xcex_queue_bp_status && queue.custrecord_clgx_xcex_queue_bp_status[0]){
    				rec.setValue("custrecord_clgx_xcex_bp_status", queue.custrecord_clgx_xcex_queue_bp_status[0].text || "");
    			}
    			if(queue.custrecord_clgx_xcex_queue_bp_id){
    				rec.setValue("custrecord_clgx_xcex_bp_xc_id", queue.custrecord_clgx_xcex_queue_bp_id || "");
    			}
    			if(queue.custrecord_clgx_xcex_queue_file_id){
    				var file_id = parseInt(queue.custrecord_clgx_xcex_queue_bp_id) || 0;
    			}
    			if(file_id > 0){
    				rec.setValue("custrecord_clgx_xcex_bp_history", '<iframe name="_xcex_hist" id="_xcex_hist" src="/app/site/hosting/scriptlet.nl?script=1734&deploy=1&live=0&xcid=' + xcid + '&quid=' + quid + '" height="500px" width="1000" frameborder="0" scrolling="yes"></iframe>');
    			}
        		rec.setValue("custrecord_clgx_xcex_bp_live", '<iframe name="_xcex_live" id="_xcex_live" src="/app/site/hosting/scriptlet.nl?script=1734&deploy=1&live=1&xcid=' + xcid + '&quid=' + quid + '" height="300px" width="450" frameborder="0" scrolling="yes"></iframe>');
        	}
		}
	}

	function beforeSubmit(context) {
		
		if(runtime.executionContext == "USERINTERFACE") {
			if(context.type == "edit" || context.type == "create") {
				
				var new_rec         = context.newRecord;
				var new_type        = parseInt(new_rec.getValue("custrecord_cologix_xc_type") || 0);
				if(new_type == 32 && context.type == "create"){ // only for Cloud Exchange
					new_rec.setValue("custrecord_clgx_a_end_port", null); // if user input, remove it  - this must be configured after the manual creation of the xc, on the second step
				}
			}
		}
	}
	function afterSubmit(context) {
		
		if(runtime.executionContext == "USERINTERFACE") {
			var new_rec         = context.newRecord;
			var new_type        = parseInt(new_rec.getValue("custrecord_cologix_xc_type") || 0);
			
			if(new_type == 32){ // only for Cloud Exchange
				
				var user_id         = parseInt(runtime.getCurrentUser().id);
				var ns_xc_id        = parseInt(new_rec.id);
				var ns_qu_id        = parseInt(new_rec.getValue("custrecord_clgx_xcex_queue_id") || 0);
				var provd_id        = parseInt(new_rec.getValue("custrecord_clgx_xcex_bp_provider") || 0);
				var new_port        = parseInt(new_rec.getValue("custrecord_clgx_a_end_port") || 0);
				var new_suspend     = new_rec.getValue("custrecord_clgx_xcex_suspend");
				var new_inactive    = new_rec.getValue("isinactive") || false;
				
				if(context.type == "create"){ // only for Cloud Exchange
					var so_id           = parseInt(new_rec.getValue("custrecord_xconnect_service_order") || 0);
					var service_id      = parseInt(new_rec.getValue("custrecord_cologix_xc_service") || 0);
					if(so_id > 0 && service_id > 0){
						var so = search.lookupFields({
						    type    : 'salesorder',
						    id      : so_id,
						    columns : ['entity']
						});
						var customer_id     = parseInt(so.entity[0].value);
						var user_id = parseInt(runtime.getCurrentUser().id);
						var new_qu_id = xcex.create_queue (27, ns_xc_id, null, null, customer_id, null, user_id, { "provider_id": provd_id });
				    	record.submitFields({
		    	    	    type   : 'customrecord_cologix_crossconnect',
		    	    	    id     : ns_xc_id,
		    	    	    values: { custrecord_clgx_xcex_queue_id : new_qu_id },
		    	    	    options: {enableSourcing: false,ignoreMandatoryFields : true}
		    	    	});
				    	record.submitFields({
		    	    	    type  : 'customrecord_clgx_xcex_queue_v_xcs',
		    	    	    id    : new_qu_id,
		    	    	    values: { 
		    	    	    	custrecord_clgx_xcex_queue_so : so_id, 
		    	    	    	custrecord_clgx_xcex_queue_service : service_id},
		    	    	    options: {enableSourcing: false,ignoreMandatoryFields : true}
		    	    	});
					}
				}
				
				if(context.type == "edit" && ns_qu_id > 0) {
					var old_rec         = context.oldRecord;
					var old_port        = parseInt(old_rec.getValue("custrecord_clgx_a_end_port") || 0);
					var old_suspend     = old_rec.getValue("custrecord_clgx_xcex_suspend");
					var old_inactive    = old_rec.getValue("isinactive");
					
					log.debug({ title: "XCEX", details: ' | port_old/new : ' + old_port + "/" + new_port + ' | suspend_old/new: ' + old_suspend + '/' + new_suspend + ' | isinactive_old/new : ' + old_inactive + '/' + new_inactive + " |"});
					
					// Manage changes
					// ============================================== new port or port change ============================================== 
					if (old_port == 0 && new_port > 0) {
						// trigger CLGX2_SS_XCEX_XC script
	                    var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
	                    scrpt.scriptId     = "customscript_clgx2_ss_xcex_xc";
	                    scrpt.params       = { custscript_clgx_xcex_queue_xc: parseInt(ns_qu_id) };
	                    scrpt.submit();
					}
					// ====================================== suspending or re provisioning requested  ====================================== 
					if (old_suspend == false && new_suspend == true) { // suspending requested
						xcex.update_queue (ns_qu_id, 4); // Change queue record - status to 'suspending'
					}
					if (old_suspend == true && new_suspend == false) { // reprovisioning requested
						xcex.update_queue (ns_qu_id, 6); // Change queue record - status to 'reprovisioning'
					}
					// =============================================== inactivation requested ============================================== 
					if (old_inactive == false && new_inactive == true) {
						xcex.update_queue (ns_qu_id, 7); // Change queue record - status to 'inactivating'
						/*
						// create inactivation case
						var so_id = parseInt(new_rec.custrecord_xconnect_service_order);
						var fields = search.lookupFields({
						    type    : 'salesorder',
						    id      : so_id,
						    columns : ['entity']
						});
						var customer_id  = fields.entity;
						var req = {
						    	"customer_id": customer_id,
					    		"contact_id": user_id,
					    		"id": ns_xc_id,
					    		"type": "xc"
					    };
						// trigger CLGX2_SS_XCEX_Inactivate_Case script
			            var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
			            scrpt.scriptId     = "customscript_clgx2_ss_xcex_inactive_case";
			            scrpt.params       = { custscript_clgx_xcex_inactive_case_req: JSON.stringify(req) };
			            scrpt.submit();
						*/
					}
					
					/*
					// reposting is desired because of a port change and que status failed
					var ns_xc = search.lookupFields({
					    type    : 'customrecord_clgx_xcex_queue_v_xcs',
					    id      : ns_qu_id,
					    columns : ['custrecord_clgx_xcex_queue_bp_status']
					});
					var bp_st_id   = parseInt(ns_xc.custrecord_clgx_xcex_queue_bp_status) || null;
					if (bp_st_id == 9 && ((old_port > 0 && new_port > 0 && old_port != new_port) || (old_port == 0 && new_port > 0))) {
						xcex.update_queue (ns_qu_id, 9); // Change old queue record - status to 'failed'
						// create new queue record
						var new_qu_id = xcex.create_queue (27, ns_xc_id, null, null, customer_id, null, user_id, rec);
						new_rec.setValue("custrecord_clgx_xcex_queue_id", new_qu_id); // write new queue id on xc
						
						// trigger CLGX2_SS_XCEX_XC script
	                    var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
	                    scrpt.scriptId     = "customscript_clgx2_ss_xcex_xcu";
	                    scrpt.params       = { custscript_clgx_xcex_queue_xc: parseInt(new_qu_id) };
	                    scrpt.submit();
					}
					*/
				}
			}

		}
	}
	
    return {
    	beforeLoad    : beforeLoad,
    	beforeSubmit  : beforeSubmit,
    	afterSubmit   : afterSubmit
    };
	
    
});
