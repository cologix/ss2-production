/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Northbound POSTs for XCs
 Script Name:     CLGX2_RL_XCEX_XC
 Script ID:       customscript_clgx2_rl_xcex_xc
 Link:            /app/common/scripting/script.nl?id=1706
 */


define([
        "N/file",
        "N/record",
        "N/task",
        "N/search",
        "/SuiteScripts/clgx/libraries/moment.min"
    ],

    function(file, record, task, search, moment) {

        function doPost(mdso) {

            try {

                log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");

                var today        = moment().format("MM/DD/YYYY HH:mm:ss");
                var queue        = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: mdso.ns_qu_id});
                var ns_xc_id     = parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_xc'}));
                var file_id      = parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}));
                var qu_bp_status = parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}));
                //var history      = JSON.parse(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_history'}));
                var fileObj     = file.load({id: file_id});
                var history     = JSON.parse(fileObj.getContents());
                var file_name   = fileObj.name;
                if(mdso.inconsistency==true){
                    history.provisioning.posting.ns_post.inconsistency=false;
                    queue.setValue({fieldId: "custrecord_clgx_xcex_queue_inconsistent", value: true});
                }
                if (qu_bp_status == 1 || qu_bp_status == 2|| qu_bp_status == 10|| qu_bp_status == 9) { // queue mdso status waiting for provisioning confirmation
                    if (mdso.status == 'provisioned') { // provisioned confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 3}); // Queue BP provisioned
                        history.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.provisioned.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.provisioned.posted.push(today);
                        history.provisioning.provisioned.source.push("BP_Post");
                    }
                    if (mdso.status == 'failed') { // provisioning failed confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.failed.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.failed.posted.push(today);
                        history.provisioning.failed.source.push("BP_Post");
                        history.provisioning.failed.errors = mdso.error;
                        // check all children VXCs from Queue and change their BP Status to failed ?
                    }
                }

                if (qu_bp_status == 4) { // queue mdso status waiting suspending
                    if (mdso.status == 'suspended') { // suspending confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 5}); // Queue BP suspended
                        history.suspending.suspended.bpupdt.push(mdso.bp_timestamp);
                        history.suspending.suspended.posted.push(today);
                        history.suspending.suspended.source.push("BP_Post");
                    }
                    if (mdso.status == 'failed') { // suspending failure confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.suspending.failed.bpupdt.push(mdso.bp_timestamp);
                        history.suspending.failed.posted.push(today);
                        history.suspending.failed.source.push("BP_Post");
                        history.suspending.failed.errors = mdso.error;
                    }
                }

                if (qu_bp_status == 6) { // queue mdso status waiting re-provisioning
                    if (mdso.status == 'provisioned') { // provisioned confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 3}); // Queue BP provisioned
                        history.reprovisioning.provisioned.bpupdt.push(mdso.bp_timestamp);
                        history.reprovisioning.provisioned.posted.push(today);
                        history.reprovisioning.provisioned.source.push("BP_Post");
                    }
                    if (mdso.status == 'failed') { // reactivating failure confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.reprovisioning.failed.bpupdt.push(mdso.bp_timestamp);
                        history.reprovisioning.failed.posted.push(today);
                        history.reprovisioning.failed.source.push("BP_Post");
                        history.reprovisioning.failed.errors = mdso.error;
                    }
                }

                if (qu_bp_status == 7) { // queue mdso status waiting inactivating
                    if (mdso.status == 'inactivated') { // inactive confirmation
                        // mdso status inactivated
                        queue.setValue({fieldId: 'isinactive',value: true}); // Queue Record inactive
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 8}); // Queue BP inactivated
                        history.inactivating.inactivated.bpupdt.push(mdso.bp_timestamp);
                        history.inactivating.inactivated.posted.push(today);
                        history.inactivating.inactivated.source.push("BP_Post");
                    }
                    if (mdso.status == 'failed') { // inactive failure confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.inactivating.failed.bpupdt.push(mdso.bp_timestamp);
                        history.inactivating.failed.posted.push(today);
                        history.inactivating.failed.source.push("BP_Post");
                        history.inactivating.failed.errors = mdso.error;
                    }
                }

                // update history on queue record and save queue record
                try {
                    // try first to update queue record - to stop if error RCRD_HAS_BEEN_CHANGED
                    //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                    var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});
                    // if queue record updated successfully, continue with changes

                    var fileObj = file.create({
                        name: file_name,
                        fileType: file.Type.PLAINTEXT,
                        contents: JSON.stringify(history),
                        encoding: file.Encoding.UTF8,
                        folder: 10639242
                    });
                    var file_id = fileObj.save();

                    if (mdso.status == 'provisioned') {
                        // look for all VXCs on this XC and change status from null to non existing to start processing them
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_vxc_queues" });
                        results.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_queue_xc", operator: "ANYOF", values: mdso.ns_xc_id }));
                        results.run().each(function(result) {
                            var que_id     = parseInt(result.getValue(result.columns[0]));
                            var status_id  = parseInt(result.getValue(result.columns[2]) || 0);
                            if(status_id == 0){
                                // trigger CLGX2_SS_XCEX_VXC script
                                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                                scrpt.scriptId     = "customscript_clgx2_ss_xcex_vxc";
                                scrpt.params       = { custscript_clgx_xcex_queue_vxc: parseInt(que_id) };
                                scrpt.submit();
                            }
                            return true;
                        });
                    }

                    if (mdso.status == 'inactivated') { // inactive confirmation
                        var xc = search.lookupFields({
                            type: 'customrecord_cologix_crossconnect',
                            id: ns_xc_id,
                            columns: ['custrecord_clgx_a_end_port', 'custrecord_clgx_xc_lag_ports']
                        });
                        var lagPorts = xc.custrecord_clgx_xc_lag_ports;
                        if (lagPorts.length > 0) {
                            for (var i = 0; lagPorts != null && i < lagPorts.length; i++) {
                                
                                var port_req_id = record.submitFields({
                                    type: 'customrecord_clgx_active_port',
                                    id: lagPorts[i].value,
                                    values: {
                                        custrecord_clgx_active_port_status: 2,
                                        custrecord_clgx_active_port_sequence: null,
                                        custrecord_clgx_active_port_xc: null,
                                        custrecord_clgx_active_port_service: null,
                                        custrecord_clgx_active_port_so: null,
                                        custrecord_clgx_active_port_customer: null
                                    },
                                    options: {enableSourcing: false, ignoreMandatoryFields: true}
                                });

                            }
                            // make XC inactive, nullify port
                            var vxc_req_id = record.submitFields({
                                type: 'customrecord_cologix_crossconnect',
                                id: ns_xc_id,
                                values: {
                                    isinactive: true,
                                    custrecord_clgx_xc_lag_ports: null
                                },
                                options: {enableSourcing: false, ignoreMandatoryFields: true}
                            });

                        } else {
                            var port_id = parseInt(xc.custrecord_clgx_a_end_port[0].value);
                            // make XC inactive, nullify port
                            var vxc_req_id = record.submitFields({
                                type: 'customrecord_cologix_crossconnect',
                                id: ns_xc_id,
                                values: {
                                    isinactive: true,
                                    custrecord_clgx_a_end_port: null
                                },
                                options: {enableSourcing: false, ignoreMandatoryFields: true}
                            });
                            // make port record available
                            var port_req_id = record.submitFields({
                                type: 'customrecord_clgx_active_port',
                                id: port_id,
                                values: {
                                    custrecord_clgx_active_port_status: 2,
                                    custrecord_clgx_active_port_sequence: null,
                                    custrecord_clgx_active_port_xc: null,
                                    custrecord_clgx_active_port_service: null,
                                    custrecord_clgx_active_port_so: null,
                                    custrecord_clgx_active_port_customer: null
                                },
                                options: {enableSourcing: false, ignoreMandatoryFields: true}
                            });
                        }
                    }
                    var resp = {
                        "ns_xc_id" : mdso.ns_xc_id,
                        "ns_qu_id" : mdso.ns_qu_id,
                        "bp_xc_id" : mdso.bp_xc_id,
                        "status"   : mdso.status
                    };
                }
                catch (error) {
                    var err = "";
                    if (error.getDetails != undefined){
                        err = error.getCode();
                    } else {
                        err = error.toString();
                    }
                    var resp = {
                        "ns_xc_id" : mdso.ns_xc_id,
                        "ns_qu_id" : mdso.ns_qu_id,
                        "bp_xc_id" : mdso.bp_xc_id,
                        "status"   : "failed",
                        "error"    : err
                    };
                    log.debug({ title: "debug", details: ' | ns_xc_id : ' + mdso.ns_xc_id + ' | error : ' + JSON.stringify(error) + " |"});
                }
                return resp;
            }
            catch (error) {
                var err = "";
                if (error.getDetails != undefined){
                    err = error.getCode();
                } else {
                    err = error.toString();
                }
                var resp = {
                    "ns_xc_id" : mdso.ns_xc_id,
                    "ns_qu_id" : mdso.ns_qu_id,
                    "bp_xc_id" : mdso.bp_xc_id,
                    "status"   : "failed",
                    "error"    : err
                };
                log.debug({ title: "debug", details: ' | ns_xc_id : ' + mdso.ns_xc_id + ' | error : ' + JSON.stringify(error) + " |"});
                return resp;
            }

        }
        return {
            post: doPost
        };

    });