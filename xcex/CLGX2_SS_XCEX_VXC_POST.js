/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           10/15/2018
  @description:    MDSO Southbounds for VXCs
  Script Name:     CLGX2_SS_XCEX_VXC_POST
  Script ID:       customscript_clgx2_ss_xcex_vxc_post
  Link:            /app/common/scripting/script.nl?id=1742
*/

define([
	"N/file",
	"N/https",
	"N/record", 
	"N/task", 
	"N/runtime", 
	"N/search",
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
	],
function(file, https, record, task, runtime, search, moment, xcex) {
	
    function execute(context) {
    	
		var current  = runtime.getCurrentScript();
    	var ns_qu_id = current.getParameter("custscript_clgx_xcex_queue_vxc");
    	var today = moment().format("MM/DD/YYYY,HH:mm:ss");
    	var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
    	var que = {
    			"ns_vxc_id"  : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_vxc'}) || 0),
    			"ns_qu_id"   : ns_qu_id,
    			"file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
    			"bp_vxc_id"  : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
    			"provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
    			"status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
    	};
    	log.debug({ title: "debug", details: ' | que : ' + JSON.stringify(que) + " |"});
    	
    	if(que.provider == 2 || que.provider == 3){ // only for Azure or AWS
    		
	    	var fileObj     = file.load({id: que.file_id});
			var history     = JSON.parse(fileObj.getContents());
			var file_name   = fileObj.name;
			
		    var xcex_head  = xcex.get_headers();
		    //var xcex_url   = xcex.get_url_order_xc();
		    var vxcex_url  = xcex.get_url_order_vxc();
		    
		    var ping = null;
		    try {
		    	var ping = xcex.ping_mdso(); 
				if(ping){
					
					// try the initial post
					var req = https.post({
				        url       : vxcex_url,
				        headers   : xcex_head,
				        body      : JSON.stringify(history.provisioning.posting.ns_post)
				    });
					var resp = JSON.parse(req.body);
					
					history.provisioning.posting.posted.push(today);
					history.provisioning.posting.source.push("NS");
					
					if (resp && resp.status){
						resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
						if (resp.status == 'provisioning'){
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 2}); // Queue BP provisioning
							history.bp_vxc_id = resp.bp_id || "";
							history.provisioning.posting.ns_post.bp_vxc_id = resp.bp_id || "";
							history.provisioning.provisioning.bpupdt.push(resp.bp_timestamp);
							history.provisioning.provisioning.posted.push(today);
							history.provisioning.provisioning.source.push("BP_Resp");
						}
						else if (resp.status == 'failed'){
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
							queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
							history.bp_vxc_id = resp.bp_id || "";
							history.provisioning.posting.ns_post.bp_vxc_id = resp.bp_id || ""; // exist if failed ??
							history.provisioning.failed.bpupdt.push(resp.bp_timestamp); // exist if failed ??
							history.provisioning.failed.posted.push(today);
							history.provisioning.failed.source.push("BP_Resp");
							history.provisioning.failed.errors = resp.error;
						}
						else { // no other response possible
						}
					}
					else { // mdso did not respond to post, repeat on next script run
						queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 1}); // Queue BP nonexisting
					}
				} else {  // mdso not available, repeat on next script run
					queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 1}); // Queue BP nonexisting
			    }
		    }
			catch (error) {
				var err = "";
				if (error.getDetails != undefined){
				    err = error.getDetails();
				}
				else{
				    err = error.toString();
				}
				queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 1}); // Queue BP nonexisting
				
				log.debug({ title: "XCEX", details: " | error : " + err +  " |"});
				log.debug({ title: "XCEX", details: "===================================== ERROR ==================================="});
			}
		    
			// update history on queue record and save queue record
			//queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
			var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true})
			
			var fileObj = file.create({
	  			name: file_name,
	  		    fileType: file.Type.PLAINTEXT,
	  		    contents: JSON.stringify(history),
	  		    encoding: file.Encoding.UTF8,
	  		    folder: 10639243
	  		});
	  		var file_id = fileObj.save();
    	}
    	return true;
    }

    return {
        execute: execute
    };

});
