/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    Trigger MDSO Southbounds actions
 Script Name:     CLGX2_SU_XCEX_VXC
 Script ID:       customscript_clgx2_su_xcex_vxc
 Link:            /app/common/scripting/script.nl?id=1736
 */

define([
        "N/https",
        "N/record",
        "N/runtime",
        "N/search",
        "N/task",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"],
    function(https, record, runtime, search, task, xcex) {

        function beforeLoad(context) {
            if(runtime.executionContext == "USERINTERFACE" && (context.type == "view" || context.type == "edit")) {

                var rec             = context.newRecord;
                var vxcid           = rec.id;
                var ns_qu_id            = parseInt(rec.getValue("custrecord_clgx_vxcex_queue_id")) || 0;
                var provider_id     = parseInt(rec.getValue("custrecord_clgx_cloud_provider")) || 0;

                if(ns_qu_id > 0 && (provider_id == 2 || provider_id == 3|| provider_id == 4|| provider_id == 5|| provider_id == 6)){
                    var queue   = search.lookupFields({
                        type    : 'customrecord_clgx_xcex_queue_v_xcs',
                        id      : ns_qu_id,
                        columns : ['custrecord_clgx_xcex_queue_bp_id', 'custrecord_clgx_xcex_queue_bp_status', 'custrecord_clgx_xcex_queue_file_id']
                    });
                    if(queue.custrecord_clgx_xcex_queue_bp_status && queue.custrecord_clgx_xcex_queue_bp_status[0]){
                        rec.setValue("custrecord_clgx_vxcex_bp_status", queue.custrecord_clgx_xcex_queue_bp_status[0].text || "");
                    }
                    if(queue.custrecord_clgx_xcex_queue_bp_id){
                        rec.setValue("custrecord_clgx_vxcex_bp_vxc_id", queue.custrecord_clgx_xcex_queue_bp_id || "");
                    }
                    if(queue.custrecord_clgx_xcex_queue_file_id){
                        var file_id = parseInt(queue.custrecord_clgx_xcex_queue_file_id) || 0;
                    }
                    if(file_id > 0){
                        rec.setValue("custrecord_clgx_vxcex_bp_history", '<iframe name="_xcex_hist" id="_xcex_hist" src="/app/site/hosting/scriptlet.nl?script=1735&deploy=1&live=0&vxcid=' + vxcid + '&quid=' + ns_qu_id + '" height="500px" width="1000" frameborder="0" scrolling="yes"></iframe>');
                    }
                    rec.setValue("custrecord_clgx_vxcex_bp_live", '<iframe name="_xcex_live" id="_xcex_live" src="/app/site/hosting/scriptlet.nl?script=1735&deploy=1&live=1&vxcid=' + vxcid + '&quid=' + ns_qu_id + '" height="500px" width="450" frameborder="0" scrolling="yes"></iframe>');
                }
            }
        }

        function beforeSubmit(context) {
            if(runtime.executionContext == "USERINTERFACE") {
                if(context.type == "create" || context.type == "edit") {

                    var user_id         = parseInt(runtime.getCurrentUser().id);
                    var new_rec         = context.newRecord;
                    var ns_vxc_id       = parseInt(new_rec.id);
                    var ns_qu_id            = parseInt(new_rec.getValue("custrecord_clgx_vxcex_queue_id")) || 0;
                    var provider_id     = parseInt(new_rec.getValue("custrecord_clgx_cloud_provider") || 0);
                    var new_inactive    = new_rec.getValue("isinactive");

                    if(ns_qu_id > 0 && (provider_id == 2 || provider_id == 3|| provider_id == 4)) {
                        if(context.type == "create") {

                        }
                        if(context.type == "edit") {
                            var old_rec         = context.oldRecord;
                            var old_inactive    = old_rec.getValue("isinactive");

                            log.debug({ title: "XCEX", details: ' | isinactive_old/new : ' + old_inactive + '/' + new_inactive + " |"});

                            // Manage changes
                            // =============================================== inactivation requested ============================================== 
                            if (old_inactive == false && new_inactive == true) {

                                // decrease sold bandwidth on A Side XC
                                xcex.update_bandwidth (ns_vxc_id, "decrease");

                                xcex.update_queue (ns_qu_id, 7); // Change queue record - status to 'inactivating'
                                /*
                                // create inactivation case
                                var so_id = parseInt(new_rec.custrecord_cologix_service_order);
                                var fields = search.lookupFields({
                                    type    : 'salesorder',
                                    id      : so_id,
                                    columns : ['entity']
                                });
                                var customer_id  = fields.entity;
                                var req = {
                                        "customer_id": customer_id,
                                        "contact_id": user_id,
                                        "id": ns_vxc_id,
                                        "type": "vxc"
                                };
                                // trigger CLGX2_SS_XCEX_Inactivate_Case script
                                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                                scrpt.scriptId     = "customscript_clgx2_ss_xcex_inactive_case";
                                scrpt.params       = { custscript_clgx_xcex_inactive_case_req: JSON.stringify(req) };
                                scrpt.submit();
                                */
                            }
                        }
                    }
                }
            }
        }

        return {
            beforeLoad: beforeLoad,
            beforeSubmit: beforeSubmit
        };


    });