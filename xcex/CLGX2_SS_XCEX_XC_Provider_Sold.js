/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           12/15/2018
  @description:    Check SO inventory - if all are provisioned, change status to Pending Billing
  Script Name:     CLGX2_SS_XCEX_XC_Provider_Sold
  Script ID:       customscript_clgx2_ss_xcex_xc_prov_sold
  Link:            /app/site/hosting/restlet.nl?script=1748&deploy=1
*/

define([
		"N/file",
		"N/record", 
		"N/task", 
		"N/runtime", 
		"N/search",
		"/SuiteScripts/clgx/libraries/lodash.min",
		"/SuiteScripts/clgx/libraries/moment.min"
	],
function(file, record, task, runtime, search, _, moment) {
	
    function execute(context) {
    	
    	log.debug({ title: "XCEX", details: "===================================== START ============================================"});
    	var scriptObj = runtime.getCurrentScript();
    	
	    var aws_xcs = [];
	    var results = search.load({ type: 'customrecord_cologix_crossconnect', id: "customsearch_clgx_xcex_xc_provider_ports" });
		results.run().each(function(result) {
			aws_xcs.push(parseInt(result.getValue(result.columns[0])));
			return true;
		});
		
		var aws_vxcs = [];
	    var results = search.load({ type: 'customrecord_cologix_vxc', id: "customsearch_clgx_xcex_xc_provider_total" });
	    results.filters.push(search.createFilter({ name: "custrecord_clgx_cloud_connect_port", operator: "ANYOF", values: aws_xcs }));
		results.run().each(function(result) {
			aws_vxcs.push({
				"xc_id" : parseInt(result.getValue(result.columns[0])),
				"total" : parseInt(result.getValue(result.columns[1]))
			});
			return true;
		});
		
		for ( var i = 0; i < aws_vxcs.length; i++ ) {
			// update XC sold bandwidth
			record.submitFields({ 
				type    : "customrecord_cologix_crossconnect", 
				id      : aws_vxcs[i].xc_id, 
				values  : { custrecord_clgx_xcex_cloud_prov_sold_bw : aws_vxcs[i].total} 
			});

	    	var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
			log.debug({ title: "XCEX", details: "| xc_id: " + aws_vxcs[i].xc_id + "| total: " + aws_vxcs[i].total + " | usage: " + usage  + " |" });
		}
		
		log.debug({ title: "XCEX", details: "===================================== END ============================================"});
		
    	return true;
    }
	
    return {
        execute: execute
    };

});
