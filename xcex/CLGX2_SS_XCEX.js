/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Southbounds for XCs
 Script Name:     CLGX2_SS_XCEX
 Script ID:       customscript_clgx2_ss_xcex
 Link:            /app/common/scripting/script.nl?id=1740
 */

define([
        "N/file",
        "N/https",
        "N/record",
        "N/task",
        "N/runtime",
        "N/search",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
    ],
    function(file, https, record, task, runtime, search, moment, xcex) {

        function execute(context) {

            log.debug({ title: "XCEX", details: "===================================== START ============================================"});

            var script     = runtime.getCurrentScript();
            var deployment = script.deploymentId;

            var mstart     = moment();
            var start      = moment().format("MM/DD/YYYY HH:mm:ss");
            var today      = moment().format("M/D/YYYY");

            try {
                var ping = xcex.ping_mdso();
                if(ping){

                    var xcex_head  = xcex.get_headers();
                    var xcex_url   = xcex.get_url_order_xc();
                    var vxcex_url  = xcex.get_url_order_vxc();

                    var token = xcex.get_token();
                    var enviro_url = xcex.get_base_url();
                    log.debug({ title: "XCEX", details: ' | url : ' + enviro_url + " | token: " + token  + " |"});

                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do XCs POSTs ======================================="});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_xc_post" });
                        results.run().each(function(result) {
                            do_xc_post (parseInt(result.getValue(result.columns[0])), xcex_url, xcex_head);
                            return true;
                        });
                    }
                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do XCs PUTs ========================================"});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_xc_put" });
                        results.run().each(function(result) {
                            do_xc_put (parseInt(result.getValue(result.columns[0])), xcex_url, xcex_head);
                            return true;
                        });
                    }
                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do XCs LAG PUTs ========================================"});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_xc_put_lag" });
                        results.run().each(function(result) {
                            do_xc_put_lag (parseInt(result.getValue(result.columns[0])), xcex_url, xcex_head);
                            return true;
                        });
                    }
                    /*if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do XCs LAG GETs ========================================"});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_xc_get_lag" });
                        results.run().each(function(result) {
                            do_xc_get_lag (parseInt(result.getValue(result.columns[0])), xcex_url, xcex_head);
                            return true;
                        });
                    }*/

                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do XCs DELETEs ======================================"});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_xc_delete" });
                        results.run().each(function(result) {
                            do_xc_delete (parseInt(result.getValue(result.columns[0])), xcex_url, xcex_head);
                            return true;
                        });
                    }

                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do VXCs POSTs ======================================"});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_vxc_post" });
                        results.run().each(function(result) {
                            do_vxc_post (parseInt(result.getValue(result.columns[0])), vxcex_url, xcex_head);
                            return true;
                        });
                    }
                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do VXCs PUTs ======================================="});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_vxc_put" });
                        results.run().each(function(result) {
                            do_vxc_put (parseInt(result.getValue(result.columns[0])), vxcex_url, xcex_head);
                            return true;
                        });
                    }
                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do VXCs DELETEs ====================================="});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_vxc_delete" });
                        results.run().each(function(result) {
                            do_vxc_delete (parseInt(result.getValue(result.columns[0])), vxcex_url, xcex_head);
                            return true;
                        });
                    }

                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do VXCs AWS Status ====================================="});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_vxc_aws_status" });
                        results.run().each(function(result) {
                            do_vxc_aws_status (parseInt(result.getValue(result.columns[0])), xcex_head);
                            return true;
                        });
                    }

                    if(deployment == 'customdeploy_clgx2_ss_xcex_all'){
                        log.debug({ title: "XCEX", details: "===================================== Do Contracts to Box ====================================="});
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_contract_2box" });
                        results.run().each(function(result) {
                            do_contract_to_box (parseInt(result.getValue(result.columns[0])), xcex_head);
                            return true;
                        });
                    }
                }
                else{
                    log.debug({ title: "XCEX", details: "===================================== MDSO is not available ==================================="});
                    log.debug({ title: "XCEX", details: JSON.stringify(ping)});
                    // notify ?
                }
            }
            catch (error) {
                var err = "";
                if (error.getDetails != undefined){
                    err = error.getDetails();
                }
                else{
                    err = error.toString();
                }
                log.debug({ title: "XCEX", details: " | error : " + err +  " |"});
                log.debug({ title: "XCEX", details: "===================================== ERROR ==================================="});
            }

            var end = moment().format("MM/DD/YYYY,HH:mm:ss");
            var duration = parseInt(moment().diff(mstart));
            var wait = 60000 - duration;
            if(wait > 0){
                xcex.sleep(wait);
            }

            log.debug({ title: "XCEX", details: "===================================== Timing ==================================="});
            var scriptObj = runtime.getCurrentScript();
            var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
            log.debug({ title: "XCEX", details: ' | start : ' + start + " | end: " + end  +  " | duration: " + (duration/1000)  +  " sec | wait: " + (wait/1000)  + " sec | usage: " + usage  + " |"});

            var scriptTask           = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
            scriptTask.scriptId      = "customscript_clgx2_ss_xcex";
            scriptTask.deploymentId  = deployment;
            var scriptTaskId         = scriptTask.submit();

            log.debug({ title: "XCEX", details: "===================================== END ============================================="});
        }

        return {
            execute: execute
        };

        function do_xc_post (ns_qu_id, xcex_url, xcex_head){

            var scriptObj     = runtime.getCurrentScript();
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_xc_id"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_xc'}) || 0),
                "ns_qu_id"   : ns_qu_id,
                "file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_xc_id"   : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
            };
            var fileObj     = file.load({id: que.file_id});
            var history     = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;

            // check if this XC has at least one VXC - if not do not post yet
            var arr_vxcs = [];
            var results = search.load({ type: 'customrecord_cologix_vxc', id: "customsearch_clgx_xcex_xc_has_vxcs" });
            results.filters.push(search.createFilter({ name: "custrecord_clgx_cloud_connect_port_z", operator: "ANYOF", values: que.ns_xc_id }));
            results.run().each(function(result) {
                arr_vxcs.push(parseInt(result.getValue(result.columns[0])));
                return true;
            });
            //log.debug({ title: "debug", details: ' | arr_vxcs.length : ' + arr_vxcs.length + " |"});
            if (arr_vxcs.length > 0 || que.provider > 0) { // customer XC must have VXCs
                log.debug({ title: "XCEX", details: ' | ns_xc_id : ' + que.ns_xc_id +  " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});

                // get MDSO XC status
                var req = https.get({
                    url       : xcex_url + '/' + que.ns_xc_id,
                    headers   : xcex_head
                });
                var mdso = JSON.parse(req.body);

                if (mdso && mdso.status){
                    log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
                    // if status mdso status non-existing, post to mdso (first xc post, or post from another queue record, or missed)
                    if (mdso.status == 'non-existing' || que.status == 1) {

                        var xc_fields = search.lookupFields({
                            type    : 'customrecord_cologix_crossconnect',
                            id      : que.ns_xc_id,
                            columns : ['name','custrecord_clgx_xc_lag', 'custrecord_clgx_a_end_port', 'custrecord_clgx_xcex_xc_speed', 'custrecord_clgx_xc_cloud_port_label', 'custrecord_clgx_xcex_xc_migrate','custrecord_clgx_xcex_tagged']
                        });
                        var tagged=xc_fields.custrecord_clgx_xcex_tagged;
                        if(tagged==false){
                            history.provisioning.posting.ns_post.port_mode = "access";
                        }
                        var migrated = xc_fields.custrecord_clgx_xcex_xc_migrate;
                        history.provisioning.posting.ns_post.migrated = migrated;
                        var lag=xc_fields.custrecord_clgx_xc_lag;
                        log.debug({ title: "debug", details: ' | xc_fields : ' + JSON.stringify(xc_fields) + " |"});
                        if(lag==false) {
                            var portid = parseInt(xc_fields.custrecord_clgx_a_end_port[0].value);
                            var portstr = xc_fields.custrecord_clgx_a_end_port[0].text;
                            var portarr = portstr.split("|");

                            var speed = parseInt(xc_fields.custrecord_clgx_xcex_xc_speed[0].value);



                            var equip_fields = search.lookupFields({
                                type: 'customrecord_clgx_active_port',
                                id: portid,
                                columns: ['custrecord_clgx_active_port_equipment']
                            });
                            var equipname = equip_fields.custrecord_clgx_active_port_equipment[0].text;
                            var equiparray = equipname.split("-");

                            // populate to history initial request data to be posted
                            if (history.provisioning.posting.ns_post.switches.length == 0) { // add switch only on first post
                                history.provisioning.posting.ns_post.switches.push({
                                    "switch_id": equiparray[0],
                                    "port": portarr[1],
                                    "speed": speed
                                });
                            }
                        }
                        var provider_port = xc_fields.custrecord_clgx_xc_cloud_port_label || "";
                        history.provisioning.posting.ns_post.provider_port = provider_port;


                        var req = https.post({
                            url       : xcex_url,
                            headers   : xcex_head,
                            body      : JSON.stringify(history.provisioning.posting.ns_post)
                        });
                        var resp = JSON.parse(req.body);
                        log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                        history.provisioning.posting.posted.push(today);
                        history.provisioning.posting.source.push("NS");
                        if (resp && resp.status){
                            //resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                            if (resp.status == 'provisioning' || mdso.status == 'migrate'){
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 2}); // Queue BP provisioning
                                history.bp_xc_id = resp.bp_id || "";
                                history.provisioning.posting.ns_post.bp_xc_id = resp.bp_id || "";
                                history.provisioning.provisioning.bpupdt.push(resp.bp_timestamp);
                                history.provisioning.provisioning.posted.push(today);
                                history.provisioning.provisioning.source.push("BP_Resp");
                            }
                            else if (resp.status == 'failed'){
                                if(resp.bp_id!=null){
                                    var  bp_id=resp.bp_id;
                                }else{
                                    var  bp_id='';
                                }
                                if(bp_id!='') {
                                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id', value: bp_id}); // write bp_id on queue

                                }
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                                history.bp_xc_id = resp.bp_id || "";
                                history.provisioning.posting.ns_post.bp_xc_id = resp.bp_id || ""; // exist if failed ??
                                history.provisioning.failed.bpupdt.push(resp.bp_timestamp); // exist if failed ??
                                history.provisioning.failed.posted.push(today);
                                history.provisioning.failed.source.push("BP_Resp");
                                history.provisioning.failed.errors = resp.error;
                            }
                            else { // no other response possible
                            }
                        }
                        else { // mdso did not respond, repeat on next script run
                        }
                    }
                    else if (mdso.status == 'provisioning' || mdso.status == 'migrate') {
                        mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 2}); // Queue BP provisioning
                        history.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.provisioning.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.provisioning.posted.push(today);
                        history.provisioning.provisioning.source.push("NS");
                    }
                    else if (mdso.status == 'provisioned') {
                        mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 3}); // Queue BP provisioned
                        history.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.provisioned.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.provisioned.posted.push(today);
                        history.provisioning.provisioned.source.push("NS");

                        // look for all VXCs on this XC and change status from null to non existing to start processing them
                        var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_vxc_queues" });
                        results.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_queue_xc", operator: "ANYOF", values: que.ns_xc_id }));
                        results.run().each(function(result) {
                            var que_id     = parseInt(result.getValue(result.columns[0]));
                            var status_id  = parseInt(result.getValue(result.columns[2]) || 0);
                            if(status_id == 0){
                                // trigger CLGX2_SS_XCEX_VXC script
                                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                                scrpt.scriptId     = "customscript_clgx2_ss_xcex_vxc";
                                scrpt.params       = { custscript_clgx_xcex_queue_vxc: parseInt(que_id) };
                                scrpt.submit();
                            }
                            return true;
                        });

                    }
                    else if (mdso.status == 'failed') {
                        mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_xc_id = mdso.bp_id || "";
                        history.provisioning.failed.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.failed.posted.push(today);
                        history.provisioning.failed.source.push("NS");
                        history.provisioning.failed.errors = mdso.error;
                        // check all children VXCs from Queue and change their BP Status to failed ?
                    }
                    else { // no other response possible
                    }

                    // update history on queue record and save queue record
                    //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                    var x=0;
                    var fileObj = file.create({
                        name: file_name,
                        fileType: file.Type.PLAINTEXT,
                        contents: JSON.stringify(history),
                        encoding: file.Encoding.UTF8,
                        folder: 10639242
                    });
                    var file_id = fileObj.save();
                    var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

                }
                else { // mdso did not respond, repeat on next script run
                }
            } else {
                log.debug({ title: "XCEX", details: ' | ns_xc_id : ' + que.ns_xc_id +  " has no VXCs yet | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});
            }
            return true;
        }
        function do_xc_get_lag (ns_qu_id, xcex_url, xcex_head) {

            var scriptObj = runtime.getCurrentScript();
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_xc_id": parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_xc'}) || 0),
                "ns_qu_id": ns_qu_id,
                "file_id": parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_xc_id": queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider": parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status": parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
            };
            var fileObj = file.load({id: que.file_id});
            var history = JSON.parse(fileObj.getContents());
            var file_name = fileObj.name;
            var saveQ = 0;

            log.debug({
                title: "debugLAGPUT",
                details: ' | ns_xc_id : ' + que.ns_xc_id + " | ns_qu_id: " + ns_qu_id + " | que.status : " + que.status + ' | usage : ' + (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"
            });

            // get MDSO XC status =======================================================================================
            var req = https.get({
                url: xcex_url + '/' + que.ns_xc_id,
                headers: xcex_head
            });
            var resp = JSON.parse(req.body);

            if (resp && resp.status){
                //resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                if (resp.status == 'provisioning' ){
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 2}); // Queue BP provisioning
                    history.bp_xc_id = resp.bp_id || "";
                    history.provisioning.posting.ns_post.bp_xc_id = resp.bp_id || "";
                    history.provisioning.provisioning.bpupdt.push(resp.bp_timestamp);
                    history.provisioning.provisioning.posted.push(today);
                    history.provisioning.provisioning.source.push("BP_Resp");
                }
                else if (resp.status == 'failed'){
                    if(resp.bp_id!=null){
                        var  bp_id=resp.bp_id;
                    }else{
                        var  bp_id='';
                    }
                    if(bp_id!='') {
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id', value: bp_id}); // write bp_id on queue

                    }
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent', value: false});
                    history.bp_xc_id = resp.bp_id || "";
                    history.provisioning.posting.ns_post.bp_xc_id = resp.bp_id || ""; // exist if failed ??
                    history.provisioning.failed.bpupdt.push(resp.bp_timestamp); // exist if failed ??
                    history.provisioning.failed.posted.push(today);
                    history.provisioning.failed.source.push("BP_Resp");
                    history.provisioning.failed.errors = resp.error;
                }
                else if (resp.status == 'provisioned') {
                    resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 3}); // Queue BP provisioned
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent', value: false});
                    history.bp_xc_id = resp.bp_id || "";
                    history.provisioning.posting.ns_post.bp_xc_id = resp.bp_id || "";
                    history.provisioning.provisioned.bpupdt.push(resp.bp_timestamp);
                    history.provisioning.provisioned.posted.push(today);
                    history.provisioning.provisioned.source.push("NS");

                }else if (resp.status == 'updating_ports'|| resp.status == 'update_ports'|| resp.status == 'updating') {
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 10});
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent', value: false});
                    history.provisioning.reprovisioning.bpupdt.push(resp.bp_timestamp);
                    history.reprovisioning.reprovisioning.posted.push(today);
                    history.reprovisioning.reprovisioning.source.push("BP_Resp");
                }
                else{

                }



            }
            else { // mdso did not respond, repeat on next script run
            }
            var fileObj = file.create({
                name: file_name,
                fileType: file.Type.PLAINTEXT,
                contents: JSON.stringify(history),
                encoding: file.Encoding.UTF8,
                folder: 10639242
            });
            var file_id = fileObj.save();
            var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});
            return true;
        }

        function do_xc_put_lag (ns_qu_id, xcex_url, xcex_head){

            var scriptObj     = runtime.getCurrentScript();
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_xc_id"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_xc'}) || 0),
                "ns_qu_id"   : ns_qu_id,
                "file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_xc_id"   : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
            };
            var fileObj     = file.load({id: que.file_id});
            var history     = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;
            var saveQ=0;

            log.debug({ title: "debugLAGPUT", details: ' | ns_xc_id : ' + que.ns_xc_id + " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});

            // get MDSO XC status =======================================================================================
            var req = https.get({
                url       : xcex_url + '/' + que.ns_xc_id,
                headers   : xcex_head
            });
            var mdso = JSON.parse(req.body);
            log.debug({ title: "debugLAGPUT mdso.status", details: mdso.status});


            if (mdso && mdso.status){
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                if (que.status == 16) { // updating the lag

                    if (mdso.status == "non-existing") {
                        //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 1});
                        var xcpost=do_xc_post (ns_qu_id, xcex_url, xcex_head);
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent', value: false});
                        saveQ=1;
                    }else{
                        var putBody = {
                            "status": "updating_ports",
                            "switches": history.provisioning.posting.ns_post.switches
                        }
                        var req = https.put({
                            url: xcex_url + '/' + que.ns_xc_id,
                            headers: xcex_head,
                            body: JSON.stringify(putBody)
                        });
                        var resp = JSON.parse(req.body);
                        log.debug({title: "XCEX", details: ' | resp:' + JSON.stringify(resp)});
                        history.reprovisioning.posting.posted.push(today);
                        history.reprovisioning.posting.source.push("NS");

                        if (resp && resp.status) {
                            queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent', value: false});
                            resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                            //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                            if (resp.status == 'updating_ports'|| resp.status == 'update_ports'|| resp.status == 'updating') {
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 10});
                                history.reprovisioning.reprovisioning.bpupdt.push(resp.bp_timestamp);
                                history.reprovisioning.reprovisioning.posted.push(today);
                                history.reprovisioning.reprovisioning.source.push("BP_Resp");
                                saveQ=1;
                            } else if (resp.status == 'failed') {
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 9}); // Queue BP Status failed
                                history.reprovisioning.failed.bpupdt.push(resp.bp_timestamp);
                                history.reprovisioning.failed.posted.push(today);
                                history.reprovisioning.failed.source.push("BP_Resp");
                                history.reprovisioning.failed.errors = resp.error;
                                saveQ=1;
                            } else { // no other response possible
                            }
                        } else { // mdso did not respond, repeat on next script run
                        }


                    }
                }
                var fileObj = file.create({
                    name: file_name,
                    fileType: file.Type.PLAINTEXT,
                    contents: JSON.stringify(history),
                    encoding: file.Encoding.UTF8,
                    folder: 10639242
                });
                var file_id = fileObj.save();
                if(saveQ==1) {
                    var recid = queue.save({enableSourcing: false, ignoreMandatoryFields: true});
                }



            }
            else { // mdso did not respond, repeat on next script run
            }
            return true;
        }

        function do_xc_put (ns_qu_id, xcex_url, xcex_head){

            var scriptObj     = runtime.getCurrentScript();
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_xc_id"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_xc'}) || 0),
                "ns_qu_id"   : ns_qu_id,
                "file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_xc_id"   : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
            };
            var fileObj     = file.load({id: que.file_id});
            var history     = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;

            log.debug({ title: "debug", details: ' | ns_xc_id : ' + que.ns_xc_id + " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});

            // get MDSO XC status =======================================================================================
            var req = https.get({
                url       : xcex_url + '/' + que.ns_xc_id,
                headers   : xcex_head
            });
            var mdso = JSON.parse(req.body);

            if (mdso && mdso.status){
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                //log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
                if (que.status == 4) { // suspending from provisioned was requested

                    if (mdso.status == "provisioned") { // mdso provisioned XC - post the suspending

                        var req = https.put({
                            url       : xcex_url + '/' + que.ns_xc_id,
                            headers   : xcex_head,
                            body      : JSON.stringify(history.suspending.posting.ns_post)
                        });
                        var resp = JSON.parse(req.body);
                        history.suspending.posting.posted.push(today);
                        history.suspending.posting.source.push("NS");

                        if (resp && resp.status){
                            resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                            //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                            if (resp.status == 'suspending'){
                                history.suspending.suspending.bpupdt.push(resp.bp_timestamp);
                                history.suspending.suspending.posted.push(today);
                                history.suspending.suspending.source.push("BP_Resp");
                            }
                            else if (resp.status == 'failed'){
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                                history.suspending.failed.bpupdt.push(resp.bp_timestamp);
                                history.suspending.failed.posted.push(today);
                                history.suspending.failed.source.push("BP_Resp");
                                history.suspending.failed.errors = resp.error;
                            }
                            else { // no other response possible
                            }
                        }
                        else { // mdso did not respond, repeat on next script run
                        }
                    }
                    else if (mdso.status == "suspending") { // mdso status suspending of a suspending request
                        history.suspending.suspending.bpupdt.push(mdso.bp_timestamp);
                        history.suspending.suspending.posted.push(today);
                        history.suspending.suspending.source.push("NS");
                    }
                    else if (mdso.status == "suspended") { // mdso status suspended of a suspending request
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 5}); // Queue BP suspended
                        history.suspending.suspended.bpupdt.push(mdso.bp_timestamp);
                        history.suspending.suspended.posted.push(today);
                        history.suspending.suspended.source.push("NS");
                    }
                    else if (mdso.status == "failed") {
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.suspending.failed.bpupdt.push(mdso.bp_timestamp);
                        history.suspending.failed.posted.push(today);
                        history.suspending.failed.source.push("NS");
                        history.suspending.failed.errors = mdso.error;
                    }
                    else{ // no other response possible
                    }
                }


                if (que.status == 6) { // re-provisioning from suspended was requested

                    if (mdso.status == "suspended") { // mdso suspended XC - post the re-provisioning

                        var req = https.put({
                            url       : xcex_url + '/' + que.ns_xc_id,
                            headers   : xcex_head,
                            body      : JSON.stringify(history.reprovisioning.posting.ns_post)
                        });
                        var resp = JSON.parse(req.body);
                        history.reprovisioning.posting.posted.push(today);
                        history.reprovisioning.posting.source.push("NS");

                        //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                        if (resp && resp.status){
                            resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                            //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                            if (resp.status == 'reprovisioning'){
                                history.reprovisioning.reprovisioning.bpupdt.push(resp.bp_timestamp);
                                history.reprovisioning.reprovisioning.posted.push(today);
                                history.reprovisioning.reprovisioning.source.push("BP_Resp");
                            }
                            else if (resp.status == 'failed'){
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                                history.reprovisioning.failed.bpupdt.push(resp.bp_timestamp);
                                history.reprovisioning.failed.posted.push(today);
                                history.reprovisioning.failed.source.push("BP_Resp");
                                history.reprovisioning.failed.errors = resp.error;
                            }
                            else { // no other response possible
                            }
                        }
                        else { // mdso did not respond, repeat on next script run
                        }
                    }
                    else if (mdso.status == "reprovisioning") { // mdso status suspending of a suspending request
                        history.reprovisioning.reprovisioning.bpupdt.push(mdso.bp_timestamp);
                        history.reprovisioning.reprovisioning.posted.push(today);
                        history.reprovisioning.reprovisioning.source.push("NS");
                    }
                    else if (mdso.status == "provisioned") { // mdso status provisioned of a suspending request
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 3}); // Queue BP provisioned
                        history.reprovisioning.provisioned.bpupdt.push(mdso.bp_timestamp);
                        history.reprovisioning.provisioned.posted.push(today);
                        history.reprovisioning.provisioned.source.push("NS");
                    }
                    else if (mdso.status == "failed") {
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.reprovisioning.failed.bpupdt.push(mdso.bp_timestamp);
                        history.reprovisioning.failed.posted.push(today);
                        history.reprovisioning.failed.source.push("NS");
                        history.reprovisioning.failed.errors = mdso.error;
                    }
                    else{ // no other response possible
                    }
                }

                // update history on queue record and save queue record
                //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

                var fileObj = file.create({
                    name: file_name,
                    fileType: file.Type.PLAINTEXT,
                    contents: JSON.stringify(history),
                    encoding: file.Encoding.UTF8,
                    folder: 10639242
                });
                var file_id = fileObj.save();

            }
            else { // mdso did not respond, repeat on next script run
            }
            return true;
        }

        function do_xc_delete (ns_qu_id, xcex_url, xcex_head){

            var scriptObj     = runtime.getCurrentScript();
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_xc_id"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_xc'}) || 0),
                "ns_qu_id"   : ns_qu_id,
                "file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_xc_id"   : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
            };
            var fileObj     = file.load({id: que.file_id});
            var history     = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;

            // look for all VXCs on this XC and change status from whatever to suspending
            var has_vxcs = false;
            var results = search.load({ type: 'customrecord_clgx_xcex_queue_v_xcs', id: "customsearch_clgx_xcex_vxc_queues" });
            results.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_queue_xc", operator: "ANYOF", values: que.ns_xc_id }));
            results.filters.push(search.createFilter({ name: "custrecord_clgx_xcex_queue_bp_status", operator: "NONEOF", values: 8 })); // all that are not inactivated
            results.run().each(function(result) {
                var que_id     = parseInt(result.getValue(result.columns[0]));
                var status_id  = parseInt(result.getValue(result.columns[1]) || 0);
                if(status_id != 7){
                    var qu_req_id = record.submitFields({
                        type: 'customrecord_clgx_xcex_queue_v_xcs',
                        id: que_id,
                        values: {custrecord_clgx_xcex_queue_bp_status : 7}, // start XVCs inactivation process
                        options: {enableSourcing: false,ignoreMandatoryFields : true}
                    });
                }
                has_vxcs = true;
                return true;
            });
            log.debug({ title: "debug", details: ' | ns_xc_id : ' + que.ns_xc_id + " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});

            // get MDSO XC status =======================================================================================
            var req = https.get({
                url       : xcex_url + '/' + que.ns_xc_id,
                headers   : xcex_head
            });
            var mdso = JSON.parse(req.body);
            log.debug({ title: "debug", details: ' | has_vxcs : ' +has_vxcs+" |"});


            if (mdso && mdso.status && has_vxcs == false){
                log.debug({ title: "debug", details: ' | ns_xc_id : ' + que.ns_xc_id + " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | delete HAS VXC : '+ has_vxcs + " |"});

                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                //log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});

                if (mdso.status == "non-existing") { // inactivation of a not posted XC was requested
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 8}); // inactivated
                    queue.setValue({fieldId: 'isinactive',value: true}); // Inactivate queue record

                    var xc = search.lookupFields({
                        type    : 'customrecord_cologix_crossconnect',
                        id      : que.ns_xc_id,
                        columns : ['custrecord_clgx_a_end_port']
                    });
                    var port_id = 0;
                    if(xc.length > 0 && xc.custrecord_clgx_a_end_port[0] != null){
                        port_id  = parseInt(xc.custrecord_clgx_a_end_port[0].value);
                    }

                    var xc = record.submitFields({
                        type     : 'customrecord_cologix_crossconnect',
                        id       : que.ns_xc_id,
                        values   : {isinactive : true, custrecord_clgx_a_end_port: null},
                        options  : {enableSourcing: false,ignoreMandatoryFields : true}
                    });
                    if(port_id > 0){
                        var port = record.submitFields({
                            type: 'customrecord_clgx_active_port',
                            id: port_id,
                            values: {
                                custrecord_clgx_active_port_status     : 2,
                                custrecord_clgx_active_port_sequence   : null,
                                custrecord_clgx_active_port_xc         : null,
                                custrecord_clgx_active_port_service    : null,
                                custrecord_clgx_active_port_so         : null,
                                custrecord_clgx_active_port_customer   : null
                            },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                    // inactivate all VXCs of this XC
                    var results = search.load({ type: 'customrecord_cologix_vxc', id: "customsearch_clgx_xcex_xc_has_vxcs" });
                    results.filters.push(search.createFilter({ name: "custrecord_clgx_cloud_connect_port_z", operator: "ANYOF", values: que.ns_xc_id }));
                    results.run().each(function(result) {
                        var vxc_id = parseInt(result.getValue(result.columns[0]));
                        var qu_id = parseInt(result.getValue(result.columns[1])) || 0;
                        if(qu_id > 0){
                            var qu = record.submitFields({
                                type     : 'customrecord_clgx_xcex_queue_v_xcs',
                                id       : qu_id,
                                values   : {
                                    isinactive : true,
                                    custrecord_clgx_xcex_queue_bp_status : 8
                                }, // inactivated
                                options  : {enableSourcing: false,ignoreMandatoryFields : true}
                            });
                        }
                        // decrease sold bandwidth on A Side XC
                        xcex.update_bandwidth (vxc_id, "decrease");

                        var qu = record.submitFields({
                            type: 'customrecord_cologix_vxc',
                            id: vxc_id,
                            values: {
                                isinactive : true,
                                custrecord_clgx_vxc_provider_switch_1 : null,
                                custrecord_clgx_vxc_access_switch_1 : null,
                                custrecord_clgx_vxc_access_switch_2 : null,
                                custrecord_clgx_vxc_access_switch_3 : null,
                                custrecord_clgx_vxc_speed : null,
                                custrecord_clgx_vxc_bandwidth : null,
                                custrecord_clgx_cloud_connect_port : null,
                                custrecord_clgx_cloud_connect_port_z : null,
                                custrecord_clgx_vxc_stag : null
                            },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                        return true;
                    });
                }

                if (mdso.status == "provisioned" || mdso.status == "suspended" || mdso.status == "failed") { // mdso provisioned XC - post the inactivating

                    var req = https.delete({
                        url       : xcex_url + '/' + que.ns_xc_id,
                        headers   : xcex_head,
                        body      : JSON.stringify(history.inactivating.posting.ns_post)
                    });
                    var resp = JSON.parse(req.body);
                    history.inactivating.posting.posted.push(today);
                    history.inactivating.posting.source.push("NS");

                    if (resp && resp.status){
                        resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                        if (resp.status == 'inactivating'){
                            history.inactivating.inactivating.bpupdt.push(resp.bp_timestamp);
                            history.inactivating.inactivating.posted.push(today);
                            history.inactivating.inactivating.source.push("BP_Resp");
                        }
                        else if (resp.status == 'failed'){
                            queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                            history.inactivating.failed.bpupdt.push(resp.bp_timestamp);
                            history.inactivating.failed.posted.push(today);
                            history.inactivating.failed.source.push("BP_Resp");
                            history.inactivating.failed.errors = resp.error;
                        }
                        else { // no other response possible
                        }
                    }
                    else { // mdso did not respond, repeat on next script run
                    }
                }
                else if (mdso.status == "inactivating") { // mdso status inactivating
                    history.inactivating.inactivating.bpupdt.push(mdso.bp_timestamp);
                    history.inactivating.inactivating.posted.push(today);
                    history.inactivating.inactivating.source.push("NS");
                }
                else if (mdso.status == "inactivated") { // mdso status inactivated
                    queue.setValue({fieldId: 'isinactive',value: true}); // Queue Record inactive
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 8}); // Queue BP inactivated
                    history.inactivating.inactivated.bpupdt.push(mdso.bp_timestamp);
                    history.inactivating.inactivated.posted.push(today);
                    history.inactivating.inactivated.source.push("NS");

                    var xc = search.lookupFields({
                        type    : 'customrecord_cologix_crossconnect',
                        id      : que.ns_xc_id,
                        columns : ['custrecord_clgx_a_end_port']
                    });
                    var port_id = 0;
                    if(xc.custrecord_clgx_a_end_port && xc.custrecord_clgx_a_end_port[0]){
                        port_id  = parseInt(xc.custrecord_clgx_a_end_port[0].value) || 0;
                    }

                    // make XC inactive, nullify port
                    var vxc_req_id = record.submitFields({
                        type: 'customrecord_cologix_crossconnect',
                        id: que.ns_xc_id,
                        values: {
                            isinactive                  : true,
                            custrecord_clgx_a_end_port  : null
                        },
                        options: {enableSourcing: false, ignoreMandatoryFields : true}
                    });
                    if(port_id > 0){
                        // make port record available
                        var port_req_id = record.submitFields({
                            type: 'customrecord_clgx_active_port',
                            id: port_id,
                            values: {
                                custrecord_clgx_active_port_status     : 2,
                                custrecord_clgx_active_port_sequence   : null,
                                custrecord_clgx_active_port_xc         : null,
                                custrecord_clgx_active_port_service    : null,
                                custrecord_clgx_active_port_so         : null,
                                custrecord_clgx_active_port_customer   : null
                            },
                            options: {enableSourcing: false,ignoreMandatoryFields : true}
                        });
                    }
                }
                else if (mdso.status == "failed") {
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                    history.inactivating.failed.bpupdt.push(mdso.bp_timestamp);
                    history.inactivating.failed.posted.push(today);
                    history.inactivating.failed.source.push("NS");
                    history.inactivating.failed.errors = mdso.error;
                }
                else{ // no other response possible
                }

                // update history on queue record and save queue record
                //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

                var fileObj = file.create({
                    name: file_name,
                    fileType: file.Type.PLAINTEXT,
                    contents: JSON.stringify(history),
                    encoding: file.Encoding.UTF8,
                    folder: 10639242
                });
                var file_id = fileObj.save();

            }
            else { // mdso did not respond, or VXCs neeed to be inactivated first -  repeat on next script run
            }
            return true;
        }

        function do_vxc_post (ns_qu_id, vxcex_url, xcex_head){

            var scriptObj     = runtime.getCurrentScript();
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_vxc_id"  : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_vxc'}) || 0),
                "ns_qu_id"   : ns_qu_id,
                "file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_vxc_id"  : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)

            };
            var fileObj     = file.load({id: que.file_id});
            var history     = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;

            // check if the Z End XC is in provisioned state (3) - if not, do not post yet
            var vxc_z_xc_id = history.provisioning.posting.ns_post.xcs[1].ns_xc_id;
            var xc_fields = search.lookupFields({
                type    : 'customrecord_cologix_crossconnect',
                id      : vxc_z_xc_id,
                columns : ['custrecord_clgx_xcex_queue_id']
            });
            var qu_id  = parseInt(xc_fields.custrecord_clgx_xcex_queue_id[0].value) || 0;
            var qu_fields = search.lookupFields({
                type    : 'customrecord_clgx_xcex_queue_v_xcs',
                id      : qu_id,
                columns : ['custrecord_clgx_xcex_queue_bp_status']
            });
            var vxc_z_xc_qu_stat_id  = parseInt(qu_fields.custrecord_clgx_xcex_queue_bp_status[0].value) || 0;

            if (vxc_z_xc_qu_stat_id == 3) {
                log.debug({ title: "XCEX", details: ' | ns_vxc_id : ' + que.ns_vxc_id +  " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});

                // get MDSO VXC status
                var req = https.get({
                    url       : vxcex_url + '/' + que.ns_vxc_id,
                    headers   : xcex_head
                });
                var mdso = JSON.parse(req.body);

                if (mdso && mdso.status){
                    //log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
                    // if status mdso status non-existing, post to mdso (first vxc post, or post from another queue record, or missed)
                    if (mdso.status == 'non-existing' || que.status == 1) {
                        //log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});

                        var req = https.post({
                            url       : vxcex_url,
                            headers   : xcex_head,
                            body      : JSON.stringify(history.provisioning.posting.ns_post)
                        });
                        var resp = JSON.parse(req.body);

                        history.provisioning.posting.posted.push(today);
                        history.provisioning.posting.source.push("NS");
                        if (resp && resp.status){
                            resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                            if (resp.status == 'provisioning'){
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 2}); // Queue BP provisioning
                                history.bp_vxc_id = resp.bp_id || "";
                                history.provisioning.posting.ns_post.bp_vxc_id = resp.bp_id || "";
                                history.provisioning.provisioning.bpupdt.push(resp.bp_timestamp);
                                history.provisioning.provisioning.posted.push(today);
                                history.provisioning.provisioning.source.push("BP_Resp");
                            }
                            else if (resp.status == 'failed'){
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: resp.bp_id}); // write bp_id on queue
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                                history.bp_vxc_id = resp.bp_id || "";
                                history.provisioning.posting.ns_post.bp_vxc_id = resp.bp_id || ""; // exist if failed ??
                                history.provisioning.failed.bpupdt.push(resp.bp_timestamp); // exist if failed ??
                                history.provisioning.failed.posted.push(today);
                                history.provisioning.failed.source.push("BP_Resp");
                                history.provisioning.failed.errors = resp.error;
                            }
                            else { // no other response possible
                            }
                        }
                        else { // mdso did not respond, repeat on next script run
                        }
                    }
                    else if (mdso.status == 'provisioning') {
                        mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 2}); // Queue BP provisioning
                        history.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.provisioning.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.provisioning.posted.push(today);
                        history.provisioning.provisioning.source.push("NS");
                    }
                    else if (mdso.status == 'provisioned') {
                        mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 3}); // Queue BP provisioned
                        history.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.provisioned.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.provisioned.posted.push(today);
                        history.provisioning.provisioned.source.push("NS");

                        // update VXC from MDSO inventory
                        var req = https.get({
                            url       : xcex.get_url_invent_vxc() + '/' + que.ns_vxc_id,
                            headers   : xcex.get_headers()
                        });
                        var resp = JSON.parse(req.body);
                        if(resp){
                            var vxc_req_id = record.submitFields({
                                type: 'customrecord_cologix_vxc',
                                id: que.ns_vxc_id,
                                values: {
                                    custrecord_clgx_vxc_vxlan_id          : resp.vxlan.toString() ,
                                    custrecord_clgx_vxc_multicast_address : resp.multicast_address,
                                    custrecord_clgx_vxc_cloud_conn_id     : resp.cloud_connection_id ,
                                    custrecord_clgx_vxc_cloud_conn_status : resp.cloud_connection_status,
                                    // A
                                    custrecord_clgx_vxc_msft_stag         : resp.xcs[0].egress_vlan.toString(),
                                    custrecord_clgx_vxc_stag              : resp.xcs[0].switch_vlan.toString(),
                                    // Z
                                    custrecord_clgx_vxc_z_egress_vlan     : resp.xcs[1].egress_vlan.toString(),
                                    custrecord_clgx_vxc_z_switch_vlan     : resp.xcs[1].switch_vlan.toString()
                                },
                                options: {enableSourcing: false, ignoreMandatoryFields : true}
                            });
                        }
                        // increase sold bandwidth on A Side XC
                        // xcex.update_bandwidth (que.ns_vxc_id, "increase");
                    }
                    else if (mdso.status == 'failed') {
                        mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.failed.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.failed.posted.push(today);
                        history.provisioning.failed.source.push("NS");
                        history.provisioning.failed.errors = mdso.error;
                    }
                    else { // no other response possible
                    }

                    // update history on queue record and save queue record
                    //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                    var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

                    var fileObj = file.create({
                        name: file_name,
                        fileType: file.Type.PLAINTEXT,
                        contents: JSON.stringify(history),
                        encoding: file.Encoding.UTF8,
                        folder: 10639243
                    });
                    var file_id = fileObj.save();

                }
                else { // mdso did not respond, repeat on next script run
                }
            } else {
                log.debug({ title: "XCEX", details: ' | Z End XC was not provisioned yet for - ns_vxc_id : ' + que.ns_vxc_id +  " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});
            }
            return true;
        }


        function do_vxc_put (ns_qu_id, vxcex_url, xcex_head){
            log.debug({ title: "XCEX do_vxc_put", details: ns_qu_id});
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_vxc_id"  : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_vxc'}) || 0),
                "ns_qu_id"   : ns_qu_id,
                "file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_vxc_id"  : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0),
                "contact_id" : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_contact'}) || 0),
                "so_id"      : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_so'}) || 0),
                "so"         : queue.getText({fieldId: 'custrecord_clgx_xcex_queue_so'}) || "",
                "ser"        : queue.getText({fieldId: 'custrecord_clgx_xcex_queue_service'}) || "",
                "vxc"        : queue.getText({fieldId: 'custrecord_clgx_xcex_queue_vxc'}) || "",

            };
            var so_fields = search.lookupFields({
                type    : 'salesorder',
                id      : que.so_id,
                columns : [ 'entity']
            });
            var customer=parseInt(so_fields.entity[0].value);
            var customerName=so_fields.entity[0].text;
            var fileObj     = file.load({id: que.file_id});
            var history     = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;
            var saveQ=0;
            var extended=0;
            var mySearch = search.load({ type: 'customrecord_cologix_vxc', id: 'customsearch_clgx_ss_vxc_b_upgrade'});
            mySearch.filters.push(search.createFilter({ name: "internalid", operator: "ANYOF", values: que.ns_vxc_id }));
            var results = mySearch.run().getRange({start: 0,end: 1});
            if(results && results.length == 1) {
                var fac1= parseInt(results[0].getValue(results[0].columns[0]));
                var fac2= parseInt(results[0].getValue(results[0].columns[1]));
                if(fac1!=fac2){
                    extended=1;
                }
            }



            var rate=get_rate(history.provisioning.posting.ns_post.bandwidth,extended);

            // get MDSO XC status =======================================================================================
            var req = https.get({
                url       : vxcex_url + '/' + que.ns_vxc_id,
                headers   : xcex_head
            });
            var mdso = JSON.parse(req.body);

            if (mdso && mdso.status){
                log.debug({ title: "debug", details: ' | mdso vxc put : ' + JSON.stringify(mdso) + " |"});
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                if (que.status == 10) { // updating the lag

                    if (mdso.status == "non-existing") {
                        //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 1});
                        var xcpost=do_vxc_post (ns_qu_id, vxcex_url, xcex_head);

                    }else{
                        var putBody = {

                            "status": "updating",
                            "bandwidth": history.provisioning.posting.ns_post.bandwidth,
                            "inconsistency":false
                        }
                        var req = https.put({
                            url: vxcex_url + '/' + que.ns_vxc_id,
                            headers: xcex_head,
                            body: JSON.stringify(putBody)
                        });
                        var resp = JSON.parse(req.body);
                        log.debug({title: "XCEX", details: ' | resp vxc put:' + JSON.stringify(resp)});
                        history.updating.posting.posted.push(today);
                        history.updating.posting.source.push("NS");

                        if (resp && resp.status) {
                            queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_inconsistent', value: false});
                            resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                            //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                            if ( resp.status == 'updating') {
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 3});
                                history.updating.updating.bpupdt.push(resp.bp_timestamp);
                                history.updating.updating.posted.push(today);
                                history.updating.updating.source.push("BP_Resp");
                                saveQ=1;
                                var obj={"origin":"bandwidth","provider":que.provider,"contact_id":que.contact_id,"id":que.ns_vxc_id,"type":"vxc","customer_id":customer,"customer":customerName,"so":que.so,"service":que.ser,"vxc":que.vxc,"price":rate,"timestamp":today};
                                log.debug({ title: "debug", details: ' | obj vxc put : ' + JSON.stringify(obj) + " |"});
                                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                                scrpt.scriptId     = "customscript_clgx2_ss_xcex_inactive_case";
                                scrpt.params       = { custscript_clgx_xcex_inactive_case_req: JSON.stringify(obj) };
                                scrpt.submit();
                            } else if (resp.status == 'failed') {
                                queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status', value: 9}); // Queue BP Status failed
                                history.updating.failed.bpupdt.push(resp.bp_timestamp);
                                history.updating.failed.posted.push(today);
                                history.updating.failed.source.push("BP_Resp");
                                history.updating.failed.errors = resp.error;
                                saveQ=1;
                            } else { // no other response possible
                            }
                        } else { // mdso did not respond, repeat on next script run
                        }


                    }
                }
                var fileObj = file.create({
                    name: file_name,
                    fileType: file.Type.PLAINTEXT,
                    contents: JSON.stringify(history),
                    encoding: file.Encoding.UTF8,
                    folder: 10639243
                });
                var file_id = fileObj.save();
                if(saveQ==1) {
                    var recid = queue.save({enableSourcing: false, ignoreMandatoryFields: true});
                }



            }
            else { // mdso did not respond, repeat on next script run
            }


            return true;
        }

        function do_vxc_delete (ns_qu_id, vxcex_url, xcex_head){
            var scriptObj     = runtime.getCurrentScript();
            var today = moment().format("MM/DD/YYYY,HH:mm:ss");
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_vxc_id"  : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_vxc'}) || 0),
                "ns_qu_id"   : ns_qu_id,
                "file_id"    : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}) || 0),
                "bp_vxc_id"  : queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id'}) || "",
                "provider"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_provider'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0),
                "contact_id" : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_contact'}) || 0),
                "so_id"      : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_so'}) || 0)

            };
            var so_fields = search.lookupFields({
                type    : 'salesorder',
                id      : que.so_id,
                columns : [ 'entity']
            });
            var customer=parseInt(so_fields.entity[0].value);
            var fileObj     = file.load({id: que.file_id});
            var history     = JSON.parse(fileObj.getContents());
            var file_name   = fileObj.name;

            log.debug({ title: "debug", details: ' | ns_vxc_id : ' + que.ns_vxc_id + " | ns_qu_id: " + ns_qu_id  + " | que.status : " + que.status  + ' | usage : '+ (10000 - parseInt(scriptObj.getRemainingUsage())) + " |"});

            // get MDSO XC status =======================================================================================
            var req = https.get({
                url       : vxcex_url + '/' + que.ns_vxc_id,
                headers   : xcex_head
            });
            var mdso = JSON.parse(req.body);
            //log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
            if (mdso && mdso.status){
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                //log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});

                if (mdso.status == "non-existing") { // inactivation of a not posted VXC was requested
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 8}); // inactivated
                    queue.setValue({fieldId: 'isinactive',value: true}); // Inactivate queue record

                    var qu = record.submitFields({
                        type: 'customrecord_cologix_vxc',
                        id: que.ns_vxc_id,
                        values: {
                            isinactive : true,
                            custrecord_clgx_vxc_provider_switch_1 : null,
                            custrecord_clgx_vxc_access_switch_1 : null,
                            custrecord_clgx_vxc_access_switch_2 : null,
                            custrecord_clgx_vxc_access_switch_3 : null,
                            custrecord_clgx_vxc_speed : null,
                            //custrecord_clgx_vxc_bandwidth : null,
                            custrecord_clgx_cloud_connect_port : null,
                            custrecord_clgx_cloud_connect_port_z : null,
                            custrecord_clgx_vxc_stag : null
                        },
                        options: {enableSourcing: false,ignoreMandatoryFields : true}
                    });
                }
                else if (mdso.status == "provisioned" || mdso.status == "failed") { // mdso provisioned VXC - post the inactivating
                    //log.debug({ title: "debug", details: ' | post : ' + JSON.stringify(history.inactivating.posting.ns_post) + " |"});
                    var req = https.delete({
                        url       : vxcex_url + '/' + que.ns_vxc_id,
                        headers   : xcex_head,
                        body      : JSON.stringify(history.inactivating.posting.ns_post)
                    });
                    var resp = JSON.parse(req.body);
                    //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                    history.inactivating.posting.posted.push(today);
                    history.inactivating.posting.source.push("NS");

                    if (resp && resp.status){
                        resp.bp_timestamp = moment(resp.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                        //log.debug({ title: "debug", details: ' | resp : ' + JSON.stringify(resp) + " |"});
                        if (resp.status == 'inactivating'){
                            history.inactivating.inactivating.bpupdt.push(resp.bp_timestamp);
                            history.inactivating.inactivating.posted.push(today);
                            history.inactivating.inactivating.source.push("BP_Resp");
                        }
                        else if (resp.status == 'failed'){
                            queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                            history.inactivating.failed.bpupdt.push(resp.bp_timestamp);
                            history.inactivating.failed.posted.push(today);
                            history.inactivating.failed.source.push("BP_Resp");
                            history.inactivating.failed.errors = resp.error;
                        }
                        else { // no other response possible
                        }
                    }
                    else { // mdso did not respond, repeat on next script run
                    }
                }
                else if (mdso.status == "inactivating") { // mdso status inactivating
                    history.inactivating.inactivating.bpupdt.push(mdso.bp_timestamp);
                    history.inactivating.inactivating.posted.push(today);
                    history.inactivating.inactivating.source.push("NS");
                }
                else if (mdso.status == "inactivated") { // mdso status inactivated
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 8}); // Queue BP inactivated
                    queue.setValue({fieldId: 'isinactive',value: true}); // Inactivate queue record
                    history.inactivating.inactivated.bpupdt.push(mdso.bp_timestamp);
                    history.inactivating.inactivated.posted.push(today);
                    history.inactivating.inactivated.source.push("NS");

                    // get cloud connection status on VXC from MDSO inventory
                    var req = https.get({
                        url       : xcex.get_url_invent_vxc() + '/' + que.ns_vxc_id,
                        headers   : xcex.get_headers()
                    });
                    var resp = JSON.parse(req.body);
                    var cloud_connection_status = "";
                    if(resp){
                        cloud_connection_status = resp.cloud_connection_status;
                    }

                    // make VXC record inactive, nullify fields, update cloud connection status
                    var vxc_req_id = record.submitFields({
                        type: 'customrecord_cologix_vxc',
                        id: que.ns_vxc_id,
                        values: {
                            isinactive                            : true,
                            custrecord_clgx_vxc_provider_switch_1 : null,
                            custrecord_clgx_vxc_access_switch_1   : null,
                            custrecord_clgx_vxc_access_switch_2   : null,
                            custrecord_clgx_vxc_access_switch_3   : null,
                            custrecord_clgx_vxc_speed             : null,
                            custrecord_clgx_vxc_bandwidth         : null,
                            custrecord_clgx_cloud_connect_port    : null,
                            custrecord_clgx_cloud_connect_port_z  : null,
                            //custrecord_clgx_vxc_stag              : null,
                            custrecord_clgx_vxc_cloud_conn_status : cloud_connection_status
                        },
                        options: {enableSourcing: false, ignoreMandatoryFields : true}
                    });
                }
                /*
                else if (mdso.status == "failed") {
                    queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                    history.inactivating.failed.bpupdt.push(mdso.bp_timestamp);
                    history.inactivating.failed.posted.push(today);
                    history.inactivating.failed.source.push("NS");
                    history.inactivating.failed.errors = mdso.error;
                }
                */
                else{ // no other response possible
                }

                // update history on queue record and save queue record
                //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

                var fileObj = file.create({
                    name: file_name,
                    fileType: file.Type.PLAINTEXT,
                    contents: JSON.stringify(history),
                    encoding: file.Encoding.UTF8,
                    folder: 10639243
                });
                var file_id = fileObj.save();
                var obj={"origin":"cloud","provider":que.provider,"contact_id":que.contact_id,"id":que.ns_vxc_id,"type":"vxc","customer_id":customer};
                var scrpt          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
                scrpt.scriptId     = "customscript_clgx2_ss_xcex_inactive_case";
                scrpt.params       = { custscript_clgx_xcex_inactive_case_req: JSON.stringify(obj) };
                scrpt.submit();

            }
            else { // mdso did not respond, repeat on next script run
            }
            return true;
        }

        function do_vxc_aws_status (ns_qu_id, xcex_head){
            var queue = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: ns_qu_id});
            var que = {
                "ns_vxc_id"   : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_vxc'}) || 0),
                "status"     : parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}) || 0)
            };
            // update VXC from MDSO inventory
            var req = https.get({
                url       : xcex.get_url_invent_vxc() + '/' + que.ns_vxc_id,
                headers   : xcex.get_headers()
            });
            var mdso = JSON.parse(req.body);
            //log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
            if (mdso){
                var cloud_connection_status = mdso.cloud_connection_status;
                log.debug({ title: "debug", details: ' | ns_vxc_id : ' + que.ns_vxc_id +' | cloud_connection_status : ' + cloud_connection_status + " |"});
                var vxc_req_id = record.submitFields({
                    type: 'customrecord_cologix_vxc',
                    id: que.ns_vxc_id,
                    values: {
                        custrecord_clgx_vxc_cloud_conn_status : cloud_connection_status
                    },
                    options: {enableSourcing: false, ignoreMandatoryFields : true}
                });
            }
        }

        function do_contract_to_box (ns_qu_id, xcex_head){

            var qu_fields = search.lookupFields({
                type    : 'customrecord_clgx_xcex_queue_v_xcs',
                id      : ns_qu_id,
                columns : ['custrecord_clgx_xcex_queue_so']
            });
            var so_id = parseInt(qu_fields.custrecord_clgx_xcex_queue_so[0].value);

            var so_fields = search.lookupFields({
                type    : 'salesorder',
                id      : so_id,
                columns : ['createdfrom', 'entity']
            });
            var estimate_id = parseInt(so_fields.createdfrom[0].value);
            var customer_id = parseInt(so_fields.entity[0].value);
            var full_name = so_fields.entity[0].text;
            var arr_name = full_name.split(":")
            var customer = (arr_name[(arr_name.length-1)]).trim();

            var estimate_fields = search.lookupFields({
                type    : 'estimate',
                id      : estimate_id,
                columns : ['custbody_clgx_prop_contract_file_id', 'tranid']
            });
            var file_id = parseInt(estimate_fields.custbody_clgx_prop_contract_file_id) || 0;
            var tran_id = estimate_fields.tranid || "";

            if(runtime.envType == "PRODUCTION"){
                //var base_url = "https://system.na2.netsuite.com";
                var base_url = "https://1337135.app.netsuite.com";
            } else {
                //var base_url = "https://system.netsuite.com";
                var base_url = "https://1337135-sb1.app.netsuite.com";
            }

            if(file_id > 0){
                var file_obj     = file.load({id: file_id});
                var post_body = {
                    "customer_id"    : customer_id,
                    "customer"       : customer,
                    "so_id"          : so_id,
                    "ns_qu_id"       : ns_qu_id,
                    "estimate_id"    : estimate_id,
                    "tran_id"        : tran_id,
                    "file_id"        : file_id,
                    "folder_id"      : file_obj.folder,
                    "file_path"      : file_obj.path,
                    "cont"           : file_obj.getContents(),
                    "file_url"       : base_url + file_obj.url
                }
                log.debug({ title: "debug", details: ' | post_body : ' + JSON.stringify(post_body) + " |"});
                var post_url = "https://my.cologix.com/portal/echosign/uploadB/index.cfm";
                var req = https.post({
                    url       : post_url,
                    headers   : xcex_head,
                    body      : JSON.stringify(post_body)
                });
                var resp = JSON.parse(req.body);

                if(resp.success || resp.status == "existing"){
                    record.submitFields({
                        type: 'customrecord_clgx_xcex_queue_v_xcs',
                        id: ns_qu_id,
                        values: {custrecord_clgx_xcex_queue_contract2box : true},
                        options: {enableSourcing: false,ignoreMandatoryFields : true}
                    });
                }
            }

            log.debug({ title: "debug", details: ' | ns_qu_id : ' + ns_qu_id + ' | file_id : ' + file_id + ' | resp : ' + JSON.stringify(resp) + " |"});
            return true;
        }
        function get_rate(bandwidth,extended){
            if(extended==0){
                switch(bandwidth) {
                    case '50Mbps':
                        var rate=50.00;
                        break;
                    case '100Mbps':
                        var rate=75.00;
                        break;
                    case '200Mbps':
                        var rate=100.00;
                        break;
                    case '300Mbps':
                        var rate=125.00;
                        break;
                    case '400Mbps':
                        var rate=150.00;
                        break;
                    case '500Mbps':
                        var rate=175.00;
                        break;
                    case '1Gbps':
                        var rate=200.00;
                        break;
                    case '2Gbps':
                        var rate=300.00;
                        break;
                    case '5Gbps':
                        var rate=500.00;
                        break;
                    case '10Gbps':
                        var rate=750.00;
                        break;

                    default:
                        var rate=0;
                }
            }else{
                switch(bandwidth) {
                    case '50Mbps':
                        var rate=200.00;
                        break;
                    case '100Mbps':
                        var rate=250.00;
                        break;
                    case '200Mbps':
                        var rate=350.00;
                        break;
                    case '300Mbps':
                        var rate=400.00;
                        break;
                    case '400Mbps':
                        var rate=450.00;
                        break;
                    case '500Mbps':
                        var rate=500.00;
                        break;
                    case '1Gbps':
                        var rate=900.00;
                        break;
                    case '2Gbps':
                        var rate=1200.00;
                        break;
                    case '5Gbps':
                        var rate=1750.00;
                        break;
                    case '10Gbps':
                        var rate=2500.00;
                        break;

                    default:
                        var rate=0;
                }
            }

            return rate;
        }


    });