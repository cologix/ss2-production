/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           10/15/2018
  @description:    MDSO Southbounds for XCs
  Script Name:     CLGX2_SS_XCEX_Equipment
  Script ID:       customscript_clgx2_ss_xcex_equipment
  Link:            /app/common/scripting/script.nl?id=1754
*/

define([
	"N/file",
	"N/https",
	"N/record", 
	"N/task", 
	"N/runtime", 
	"N/search",
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
	],
function(file, https, record, task, runtime, search, moment, xcex) {
	
    function execute(context) {
    	return true;
    }

    return {
        execute: execute
    };

});
