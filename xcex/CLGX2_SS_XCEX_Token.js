/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

/**
  @author:         Dan Tansanu - dan.tansanu@cologix.com
  @date:           11/2/2018
  @description:    Get a new MDSO valid token every 12 hours and writes it into a file
  Script Name:     CLGX2_SS_XCEX_Token
  Script ID:       customscript_clgx2_ss_xcex_token
  Link:            /app/common/scripting/script.nl?id=1717
*/

define(["N/file", 
		"N/https", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min",
		"/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
],
function(file, https, search, moment, xcex) {
	
    function execute() {
    	
    	log.debug({ title: "XCEX", details: "===================================== START ============================================="});
    	
		var headers = {
				'Content-Type' : 'application/json',
				'Accept'       : 'application/json'
		};
		var enviros = xcex.get_enviros();
		for ( var i = 0; i < enviros.length; i++ ) {
        	var body = {
    				'username': enviros[i].username,
    				'password': enviros[i].password,
    				'domain': {
    					'name': 'master'
    				}
    		};
    		var req = https.post({
    	        url       : enviros[i].token_url,
    	        body      : JSON.stringify(body),
    	        headers   : headers
    	    });
     	    var resp = JSON.parse(req.body); 
    	    
     	   var fileObj = file.create({
    			name: 'token_' + enviros[i].enviro + '.json',
    		    fileType: file.Type.PLAINTEXT,
    		    contents: JSON.stringify(resp),
    		    description: 'Last MDSO Token for ' + enviros[i].enviro,
    		    encoding: file.Encoding.UTF8,
    		    folder: 10639240,
    		    isOnline: false
    		});
    		var fileId = fileObj.save();
    		
    		log.debug({ title: "token_" + enviros[i].enviro, details: ' | time : ' + moment().format("MM/DD/YYYY HH:mm:ss")  + ' | token ' + enviros[i].enviro + ' : '+ resp.token + " |" + ' fileId : '+ fileId  + ' | url : ' + enviros[i].url + " |"});
    	}
    	log.debug({ title: "XCEX", details: "===================================== END ============================================="});
    }

    return {
        execute: execute
    };

});
