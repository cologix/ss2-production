/**
 *
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * Script Name  : CLGX2_SL_Dan_Test_2
 * Script ID    : customscript_clgx2_sl_dan_test_2
 * Deployment ID: customdeploy_clgx2_sl_dan_test_2
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1715&deploy=1
 *
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
        "N/file",
        "N/task",
        "N/https",
        "N/record",
        "N/runtime",
        "N/search",
        "/SuiteScripts/clgx/libraries/lodash.min",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
    ],
    function(file, task, https, record, runtime, search, _, moment, xcex) {

        function onRequest(context) {

            var response = context.response;
            var request = context.request;

var ns_xc_id     = 23533;
	    var provider_id  = 3;
	    var so_id        = null;
	    var service_id   = 13212;
	    var customer_id  = 2790;
            var user_id      = parseInt(runtime.getCurrentUser().id);

            var new_qu_id = xcex.create_queue (27, ns_xc_id, null, null, customer_id, null, user_id, { "provider_id": provider_id });
            record.submitFields({
                type   : 'customrecord_cologix_crossconnect',
                id     : ns_xc_id,
                values: { custrecord_clgx_xcex_queue_id : new_qu_id },
                options: {enableSourcing: false,ignoreMandatoryFields : true}
            });
            record.submitFields({
                type  : 'customrecord_clgx_xcex_queue_v_xcs',
                id    : new_qu_id,
                values: {
                    custrecord_clgx_xcex_queue_so : so_id,
                    custrecord_clgx_xcex_queue_service : service_id},
                options: {enableSourcing: false,ignoreMandatoryFields : true}
            });

            response.write(JSON.stringify(new_qu_id));

        }
        return {
            onRequest: onRequest
        };


    });

