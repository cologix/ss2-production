/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

/**
 @author:         Dan Tansanu - dan.tansanu@cologix.com
 @date:           10/15/2018
 @description:    MDSO Northbound POSTs for XCs
 Script Name:     CLGX2_RL_XCEX_VXC
 Script ID:       customscript_clgx2_rl_xcex_vxc
 Link:            /app/common/scripting/script.nl?id=1707
 */


define([
        "N/file",
        "N/https",
        "N/record",
        "N/task",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/xcex/CLGX2_LB_XCEX"
    ],

    function(file, https, record, task, moment, xcex) {

        function doPost(mdso) {

            try {

                log.debug({ title: "debug", details: ' | mdso : ' + JSON.stringify(mdso) + " |"});
                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");

                var today        = moment().format("MM/DD/YYYY HH:mm:ss");
                var queue        = record.load({type: 'customrecord_clgx_xcex_queue_v_xcs', id: mdso.ns_qu_id});
                var ns_vxc_id    = parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_vxc'}));
                var file_id      = parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_file_id'}));
                var qu_bp_status = parseInt(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status'}));
                //var history      = JSON.parse(queue.getValue({fieldId: 'custrecord_clgx_xcex_queue_history'}));
                var fileObj     = file.load({id: file_id});
                var history     = JSON.parse(fileObj.getContents());
                var file_name   = fileObj.name;

                mdso.bp_timestamp = moment(mdso.bp_timestamp).format("MM/DD/YYYY HH:mm:ss");
                if(mdso.inconsistency==true){
                    history.provisioning.posting.ns_post.inconsistency=false;
                    queue.setValue({fieldId: "custrecord_clgx_xcex_queue_inconsistent", value: true});
                }
                if (qu_bp_status == 1 || qu_bp_status == 2) { // queue mdso status waiting provisioning
                    if (mdso.status == 'provisioned') { // provisioned confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 3}); // Queue BP provisioned
                        history.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.provisioned.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.provisioned.posted.push(today);
                        history.provisioning.provisioned.source.push("BP_Post");
                    }
                    if (mdso.status == 'failed') { // failed confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_id',value: mdso.bp_id}); // write bp_id on queue
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.posting.ns_post.bp_vxc_id = mdso.bp_id || "";
                        history.provisioning.failed.bpupdt.push(mdso.bp_timestamp);
                        history.provisioning.failed.posted.push(today);
                        history.provisioning.failed.source.push("BP_Post");
                        history.provisioning.failed.errors = mdso.error;
                    }
                }

                if (qu_bp_status == 7) { // queue mdso status waiting inactivating
                    if (mdso.status == 'inactivated') { // inactive confirmation
                        queue.setValue({fieldId: 'isinactive',value: true}); // Queue Record inactive
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 8}); // Queue BP inactivated
                        history.inactivating.inactivated.bpupdt.push(mdso.bp_timestamp);
                        history.inactivating.inactivated.posted.push(today);
                        history.inactivating.inactivated.source.push("BP_Post");
                    }
                    if (mdso.status == 'failed') { // inactive failure confirmation
                        queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_bp_status',value: 9}); // Queue BP Status failed
                        history.inactivating.failed.bpupdt.push(mdso.bp_timestamp);
                        history.inactivating.failed.posted.push(today);
                        history.inactivating.failed.source.push("BP_Post");
                        history.inactivating.failed.errors = mdso.error;
                    }
                }

                try {
                    // update history on queue record and save queue record
                    //queue.setValue({fieldId: 'custrecord_clgx_xcex_queue_history',value: JSON.stringify(history) });
                    var recid = queue.save({enableSourcing: false,ignoreMandatoryFields: true});

                    var fileObj = file.create({
                        name: file_name,
                        fileType: file.Type.PLAINTEXT,
                        contents: JSON.stringify(history),
                        encoding: file.Encoding.UTF8,
                        folder: 10639243
                    });
                    var file_id = fileObj.save();

                    if (mdso.status == 'provisioned') {
                        // update VXC from MDSO inventory
                        var req = https.get({
                            url       : xcex.get_url_invent_vxc() + '/' + mdso.ns_vxc_id,
                            headers   : xcex.get_headers()
                        });
                        var resp = JSON.parse(req.body);
                        if(resp){
                            var vxc_req_id = record.submitFields({
                                type: 'customrecord_cologix_vxc',
                                id: ns_vxc_id,
                                values: {
                                    custrecord_clgx_vxc_vxlan_id          : resp.vxlan.toString() ,
                                    custrecord_clgx_vxc_multicast_address : resp.multicast_address,
                                    custrecord_clgx_vxc_cloud_conn_id     : resp.cloud_connection_id ,
                                    custrecord_clgx_vxc_cloud_conn_status : resp.cloud_connection_status,
                                    // A
                                    custrecord_clgx_vxc_msft_stag         : resp.xcs[0].egress_vlan.toString(),
                                    custrecord_clgx_vxc_stag              : resp.xcs[0].switch_vlan.toString(),
                                    // Z
                                    custrecord_clgx_vxc_z_egress_vlan     : resp.xcs[1].egress_vlan.toString(),
                                    custrecord_clgx_vxc_z_switch_vlan     : resp.xcs[1].switch_vlan.toString()
                                },
                                options: {enableSourcing: false, ignoreMandatoryFields : true}
                            });
                            // increase sold bandwidth on A Side XC
                            //    xcex.update_bandwidth (ns_vxc_id, "increase");
                        }
                    }

                    if (mdso.status == 'inactivated') {

                        // decrease sold bandwidth on A Side XC
                        xcex.update_bandwidth (ns_vxc_id, "decrease");

                        // make VXC record inactive, nullify fields
                        var vxc_req_id = record.submitFields({
                            type: 'customrecord_cologix_vxc',
                            id: ns_vxc_id,
                            values: {
                                isinactive                            : true,
                                custrecord_clgx_vxc_provider_switch_1 : null,
                                custrecord_clgx_vxc_access_switch_1   : null,
                                custrecord_clgx_vxc_access_switch_2   : null,
                                custrecord_clgx_vxc_access_switch_3   : null,
                                custrecord_clgx_vxc_speed             : null,
                                custrecord_clgx_vxc_bandwidth         : null,
                                custrecord_clgx_cloud_connect_port    : null,
                                custrecord_clgx_cloud_connect_port_z  : null,
                                custrecord_clgx_vxc_stag              : null
                            },
                            options: {enableSourcing: false, ignoreMandatoryFields : true}
                        });
                    }
                    var resp = {
                        "ns_xc_id" : mdso.ns_xc_id,
                        "ns_qu_id" : mdso.ns_qu_id,
                        "bp_xc_id" : mdso.bp_xc_id,
                        "status"   : mdso.status
                    };
                }
                catch (error) {
                    var err = "";
                    if (error.getDetails != undefined){
                        err = error.getCode();
                    } else {
                        err = error.toString();
                    }
                    var resp = {
                        "ns_xc_id" : mdso.ns_xc_id,
                        "ns_qu_id" : mdso.ns_qu_id,
                        "bp_xc_id" : mdso.bp_xc_id,
                        "status"   : "failed",
                        "error"    : err
                    };
                    log.debug({ title: "debug", details: ' | ns_xc_id : ' + mdso.ns_xc_id + ' | error : ' + JSON.stringify(error) + " |"});
                }
                return resp;
            }
            catch (error) {
                var err = "";
                if (error.getDetails != undefined){
                    err = error.getCode();
                } else {
                    err = error.toString();
                }
                var resp = {
                    "ns_xc_id" : mdso.ns_xc_id,
                    "ns_qu_id" : mdso.ns_qu_id,
                    "bp_xc_id" : mdso.bp_xc_id,
                    "status"   : "failed",
                    "error"    : err
                };
                log.debug({ title: "debug", details: ' | ns_xc_id : ' + mdso.ns_xc_id + ' | error : ' + JSON.stringify(error) + " |"});
                return resp;
            }
        }
        return {
            post: doPost
        };

    });