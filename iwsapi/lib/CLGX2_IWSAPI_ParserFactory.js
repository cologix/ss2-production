/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/CLGX2_IWSAPI_ParserFactory
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/18/2018
 */
define(["/SuiteScripts/clgx/iwsapi/lib/factories/xml/CLGX2_IWSAPI_XML_GetList", 
	    "/SuiteScripts/clgx/iwsapi/lib/factories/xml/CLGX2_IWSAPI_XML_Search"],
function(gl, s) {
	/**
	 * @typedef {Object} ParameterObject
	 * @property {string} method - The NetSuite request method.
	 * @property {string} body - The body XML string.
	 */
	
	/**
	 * Directs the SuiteTalk response to the proper XML parser.
	 * 
	 * @access public
	 * @function parserFactory
	 * 
	 * @param {ParameterObject} paramObject
	 * @returns {(string | null)}
	 */
    function parserFactory(paramObject) {
    	if((paramObject != null && typeof paramObject.method !== undefined) && (paramObject != null && typeof paramObject.body !== undefined)) {
    		var response = null;
    		
    		if(paramObject.method == "getList") {
    			response = gl.parseRecords(paramObject.body);
    		} else if(paramObject.method == "search") {
    			response = s.parseRecords(paramObject.body);
    		} else {
    			response = "Invalid method: " + paramObject.method;
    		}
    		
    		return response;
    	}
    }
	
    return {
        parserFactory: parserFactory
    };
    
});
