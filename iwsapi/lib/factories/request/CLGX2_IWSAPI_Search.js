/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/Factories/Request/CLGX2_IWSAPI_Search
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/16/2017
 */
define(["/SuiteScripts/clgx/iwsapi/lib/core/CLGX2_IWSAPI_Template"], 
function(tm) {
	/**
	 * @typedef {Object} AuthenticationObject
	 * @property {string} authenticationName - The name of the authentication record.
	 * @property {string} applicationID - The application ID specified in the Integration record.
	 * @property {number} accountID - The NetSuite account ID.
	 * @property {string} email - The user's login email.
	 * @property {string} password - The user's login password.
	 * @property {number} role - The internal id ofd the users role.
	 */
	/**
	 * @typedef {Object} ConfigurationObject
	 * @property {number} version - The record version.
	 * @property {string} method - The SuiteTalk request method.
	 * @property {string} url - The data center URL.
	 * @property {number} envelopeTemplateID - The internal id of the envelope template.
	 * @property {number} bodyTemplateID - The internal id of the request body template.
	 */
	/**
	 * @typedef {Object} FilterObject
	 * @property {string} name - The field name.
	 * @property {string} operator - The filter operation.
	 * @property {string} value - The value we are filtering on.
	 */
	/**
	 * @typedef {Object} BodyObject
	 * @property {string} record - The NetSuite record type.
	 * @property {FilterObject[]} filters - The array of columns we are filtering on.
	 */
	/**
	 * @typedef {Object} ParameterObject
	 * @property {AuthenticationObject} authentication - The authentgication object.
	 * @property {ConfigurationObject} config - The configuration object.
	 * @property {BodyObject} body - The body object.
	 */
	
	
	/**
	 * Assembles the final template that will be returned.
	 * 
	 * @access public
	 * @function assembleTemplate
	 * 
	 * @param {ParameterObject} paramObject
	 * 
	 * @returns {(string | null)}
	 */
	function assembleTemplate(paramObject) {
		if((paramObject != null && paramObject.body != undefined) && (paramObject != null && paramObject.authentication != undefined)) {
			var body     = assembleBody({ config: paramObject.config, body: paramObject.body });
			var envelope = assembleEnvelope({ authentication: paramObject.authentication, config: paramObject.config, body: body });
			
			return envelope;
		}
	}
	
	
	/**
	 * Assembles the envelope header objects.
	 * 
	 * @access private
	 * @function assembleEnvelope
	 * 
	 * @param {ParameterObject} paramObject
	 * 
	 * @returns {(string | null)}
	 */
	function assembleEnvelope(paramObject) {
		if((paramObject != null && typeof paramObject.authentication !== undefined) 
				&& (paramObject != null && typeof paramObject.config !== undefined) 
				&& (paramObject != null && typeof paramObject.body !== undefined)) {
			var tmpEnv = tm.loadTemplate({ fileId: paramObject.config.envelopeTemplateID });
			
			tmpEnv = tm.replaceElement({ find: "{applicationID}", replace: paramObject.authentication.applicationID, content: tmpEnv });
			tmpEnv = tm.replaceElement({ find: "{accountID}",     replace: paramObject.authentication.accountID,     content: tmpEnv });
			tmpEnv = tm.replaceElement({ find: "{email}",         replace: paramObject.authentication.email,         content: tmpEnv });
			tmpEnv = tm.replaceElement({ find: "{password}",      replace: paramObject.authentication.password,      content: tmpEnv });
			tmpEnv = tm.replaceElement({ find: "{roleID}",        replace: paramObject.authentication.role,          content: tmpEnv });
			tmpEnv = tm.replaceElement({ find: "{soapBody}",      replace: paramObject.body,                         content: tmpEnv });
			
			return tmpEnv;
		}
	}
	
	
	/**
	 * Assembles thew body portion of the envelope.
	 * 
	 * @access private
	 * @function assembleBody
	 * 
	 * @param {ParameterObject} paramObject
	 * 
	 * @returns {(string | null)}
	 */
	function assembleBody(paramObject) {
		if((paramObject != null && typeof paramObject.body !== undefined) && (paramObject != null && typeof paramObject.config !== undefined)) {
			var tmpLetter = tm.loadTemplate({ fileId: paramObject.config.bodyTemplateID });
			tmpLetter = tm.replaceElement({ find: "{bodyFilters}", replace: assembleBodyElements(paramObject.body), content: tmpLetter });
			
			return tmpLetter;
		}
	}
	
	
	/**
	 * Builds the elements for the body of the envelope.
	 * 
	 * @access private
	 * @function assembleBodyElements
	 * 
	 * @param {ParameterObject} paramObject
	 * 
	 * @returns {(string | null)}
	 */
	function assembleBodyElements(paramObject) {
		if((paramObject != null && typeof paramObject.filters !== undefined)) {
			if(typeof paramObject.filters === "object") {
				var finalString = "";
				var filterLength = paramObject.filters.length;

				finalString += "<platformMsgs:searchRecord xsi:type=\"platformCommon:" + paramObject.recordMapped + "\">";
				
				for(var f = 0; f < filterLength; f++) {
					finalString += "<platformCommon:" + paramObject.filters[f].name + " operator=\"" + paramObject.filters[f].operator + "\">\n"
					
					if(paramObject.filters[f].operator == "anyOf") {
						finalString += "<platformCore:searchValue internalId=\"" + paramObject.filters[f].value + "\" xsi:type=\"platformMsgs:RecordRef\" />";
					} else if(paramObject.filters[f].operator == "within") {
						finalString += "<platformCore:searchValue>" + paramObject.filters[f].values[0] + "</platformCore:searchValue>";
						finalString += "<platformCore:searchValue2>" + paramObject.filters[f].values[1] + "</platformCore:searchValue2>";
					} else {
						finalString += "<platformCore:searchValue>" + paramObject.filters[f].value + "</platformCore:searchValue>";
					}
					
					finalString += "</platformCommon:" + paramObject.filters[f].name + ">\n";
				}
				
				finalString += "</platformMsgs:searchRecord>";
				
				return finalString;
			}
		}
	}
	
	return {
		assembleTemplate: assembleTemplate
	};
});