/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/Factories/XML/CLGX2_IWSAPI_XML_Search
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/17/2018
 */
define(["N/xml", "N/error"],
function(xml, error) {
	/**
	 * @typedef {Object} NSXMLDocument
	 * @property {object} document.doctype - Returns a node of type DOCUMENT_TYPE_NODE that represents the doctype of the XML document.
	 * @property {xml.Element} document.documentElement - Root node of the XML document.
	 * @property {string} document.documentURI - Location of the document or null if undefined.
	 * @property {string} document.inputEncoding - Encoding used for an XML document at the time the document was parsed.
	 * @property {string} document.xmlEncoding - Part of the XML declaration, the XML encoding of the XML document.
	 * @property {boolean} document.xmlStandalone - Part of the XML declaration, returns true if the current XML document is standalone or returns false if it is not.
	 * @property {string} document.xmlVersion - Part of the XML declaration, the version number of the XML document.
	 */
	
	/**
	 * Kicks off the XML document parsing.
	 * 
	 * @access public
	 * @function parseRecords
	 * 
	 * @param {string} xmlString - The raw XML response returned from SuiteTalk.
	 * @throws {(SSS_XML_DOM_EXCEPTION | IWSAPI_ERROR)} The input XML string is malformed.
	 * @returns {(string | null)}
	 */
	function parseRecords(xmlString) {
		try {
			var document = xml.Parser.fromString({ text: xmlString });
			var header   = getSearchResultInformation(document);
			var body     = getRecordElements(document);
			return JSON.stringify({ header: header, body: body });
		} catch(ex) {
			throw error.create({ name: "IWSAPI_ERROR", message: ex, notifyOff: true });
		}
	}
	
	
	/**
	 * @typedef {Object} HeaderObject
	 * @property {number} totalRecords - The total number of records returned from the query.
	 * @property {number} pageSize - The number of records per page.
	 * @property {number} totalPages - The total number of pages.
	 * @property {number} pageIndex - The current page. 
	 */
	/**
	 * Returns the header information from the response.
	 * 
	 * @function getSearchResultInformation
	 * @param {NSXMLDocument} document - The xml.Document object returned from xml.Parser
	 * @returns {HeaderObject}
	 */
	function getSearchResultInformation(document) {
		var finalObject          = new Object();
		var nodes                = xml.XPath.select({ node: document, xpath: "//platformCore:searchResult" });
		
		finalObject.totalRecords = nodes[0].childNodes[1].textContent;
		finalObject.pageSize     = nodes[0].childNodes[2].textContent;
		finalObject.totalPages   = nodes[0].childNodes[3].textContent;
		finalObject.pageIndex    = nodes[0].childNodes[4].textContent;
		
		return finalObject;
	}
	
	
	/**
	 * @typedef {Object} BodyObject
	 * @property {Object} header
	 * @property {Object} body
	 */
	/**
	 * Returns the record information from the response body.
	 * 
	 * @function getRecordElements
	 * @param {NSXMLDocument} document - The xml.Document object returned from xml.Parser
	 * @returns {BodyObject}
	 */
	function getRecordElements(document) {
		var finalObject     = new Array();
		var nodes           = xml.XPath.select({ node: document, xpath: "//*[local-name()='record']" });
		var nodeLength      = nodes.length;
		var childNodeLength = 0;
		var tmpNode         = null;
		
		//Records
		for(var pn = 0; pn < nodeLength; pn++) {
			childNodeLength = nodes[pn].childNodes.length;
			tmpNode         = new Object();
			tmpNode.id      = nodes[pn].attributes.internalId.value;
			
			for(var cn = 0; cn < childNodeLength; cn++) {
				tmpNode[nodes[pn].childNodes[cn].localName]      = new Object();
				tmpNode[nodes[pn].childNodes[cn].localName].text = nodes[pn].childNodes[cn].textContent;
				
				if(Object.size(nodes[pn].childNodes[cn].attributes) > 0) {
					tmpNode[nodes[pn].childNodes[cn].localName].value = nodes[pn].childNodes[cn].attributes.internalId.value;
				}
			}
			
			finalObject.push(tmpNode);
		}
		
		return finalObject;
	}
	
	Object.size = function(obj) {
		var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	}
	
    return {
        parseRecords: parseRecords
    };
});
