/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/CLGX2_IWSAPI_MethodFactory
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/16/2018
 */
define(["N/runtime", "N/error",
	    "/SuiteScripts/clgx/iwsapi/lib/factories/request/CLGX2_IWSAPI_GetList",
	    "/SuiteScripts/clgx/iwsapi/lib/factories/request/CLGX2_IWSAPI_Search", 
	    "/SuiteScripts/clgx/iwsapi/lib/core/CLGX2_IWSAPI_LIB_Core", 
	    "/SuiteScripts/clgx/iwsapi/lib/core/CLGX2_IWSAPI_Request",
	    "/SuiteScripts/clgx/iwsapi/lib/core/CLGX2_IWSAPI_Configuration",
	    "/SuiteScripts/clgx/iwsapi/lib/core/CLGX2_IWSAPI_SearchMap",
	    "/SuiteScripts/clgx/iwsapi/lib/CLGX2_IWSAPI_ParserFactory",
	    "/SuiteScripts/clgx/iwsapi/lib/core/CLGX2_IWSAPI_Authentication"],
function(runtime, error, get, search, core, rm, config, sm, pf, auth) {
	/**
	 * @typedef {Object} FilterObject
	 * @property {string} name - The field name.
	 * @property {string} operator - The filter operation.
	 * @property {string} value - The value we are filtering on.
	 */
	/**
	 * @typedef {Object} SearchBodyObject
	 * @property {string} record - The NetSuite record type.
	 * @property {FilterObject[]} filters - The array of columns we are filtering on.
	 */
	/**
	 * @typedef {Object} RecordObject
	 * @property {string} type - The NetSuite record type.
	 * @property {string} id - The NetSuite internal id of the record.
	 */
	/**
	 * @typedef {Object} GetListBodyObject
	 * @property {string} record - The NetSuite record type.
	 * @property {RecordObject[]} records - The array of records with are returning.
	 */
	/**
	 * @typedef {Object} ParameterObject
	 * @property {string} method - The SuiteTalk request method.
	 * @property {number} authenticationID - The internal id of the authentication record.
	 * @property {AuthenticationObject} authentication - The authentication object
	 * @property {(SearchBodyObject | GetListBodyObject)} body - The body object.
	 */
	
	
	/**
	 * Controller method.
	 * 
	 * @access public
	 * @function requestFactory
	 * 
	 * @param {ParameterObject} paramObject
	 * @returns {(object | string | null)}
	 */
    function requestFactory(paramObject) {
    	if(paramObject != null) {
    		if(((typeof paramObject.method !== undefined) && (paramObject.method != null))  
    				&& ((typeof paramObject.authenticationID !== undefined) && (paramObject.authenticationID != null))
    				&& ((typeof paramObject.body !== undefined) && (paramObject.body != null))) {
    			var sc           = runtime.getCurrentScript();
    	    	var authObject   = auth.authenticate(paramObject.authenticationID);
    			var configObject = config.getConfig({ version: 1, method: paramObject.method });
    			var response     = null;
    			
    			if(paramObject.method == "getList") {
    				response = rm.sendRequest({
            			config: configObject,
            			body: get.assembleTemplate({ 
            				authentication: authObject, 
            				body: paramObject.body, 
            				config: configObject
            			})
            		});
    			} else if(paramObject.method == "search") {
    				paramObject.body.recordMapped = sm.getMappedValue(paramObject.body.record);
    				
    				response = rm.sendRequest({
            			config: configObject,
            			body: search.assembleTemplate({ 
            				authentication: authObject, 
            				body: paramObject.body, 
            				config: configObject
            			})
            		});
    			} else {
    				response = "Invalid method: " + paramObject.method;
    			}
    			
    			if(response.code == 200) {
    				return pf.parserFactory({ method: paramObject.method, body: response.body.toString() });
    			} else {
    				return response;
    			}
        	}
    	}
    }
	 
    return {
        requestFactory: requestFactory
    };
    
});
