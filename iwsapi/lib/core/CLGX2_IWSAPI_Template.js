/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/Core/CLGX2_IWSAPI_Template
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/25/2018
 */
define(["N/file"],
function(file) {
	/**
	 * @typedef {Object} TemplateObject
	 * @property {number} fileId - The internal of a file inr the file cabinet.
	 */
	/**
	 * @typedef {Object} ElementObject
	 * @property {string} find - The string to find.
	 * @property {string} replace The string to replace the string we're looking for.
	 * @property {string} content - The content we're replacing the string in.
	 */
	
	/**
	 * Checks if a template exists.
	 * 
	 * @access private
	 * @function templateExists
	 * 
	 * @param {integer} templateID - The template's internal id.
	 * @returns {boolean}
	 */
	function templateExists(templateID) {
		if(templateID != null && templateID != "") {
			try {
				var templateRecord = null;
				
				if(typeof templateID === "string") {
					templateRecord = file.load({ id: templateID.trim() });
				} else if(typeof templateID === "number") {
					templateRecord = file.load({ id: templateID });
				}
				
				//Checks whether or not the status is CUSTOMER-Closed Won: 13
				var templateContents = templateRecord.getContents();
				if(templateContents != null && templateContents != "") {
					return true;
				}
			} catch(ex) {
				return false;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Loads a template into memory.
	 * 
	 * @access public
	 * @function loadTemplate
	 * 
	 * @param {TemplateObject} obj
	 * @returns {string}
	 */
	function loadTemplate(obj) {
		try {
			if(obj != null && obj.fileId != undefined) {
				var fileObj = null;
				
				if(typeof obj.fileId === "number") {
					fileObj = file.load({ id: obj.fileId });
				} else if(typeof obj.fileId === "string") {
					fileObj = file.load({ id: obj.fileId.trim() });
				}
				
				if(fileObj != null && fileObj != "") {
					return fileObj.getContents();
				} else {
					throw "IWSAPI_GENERAL_ERROR: File contents are empty.";
				}
			} else {
				throw "IWASPI_MISSING_INPUT: Missing property \"fileId\".";
			}		
		} catch(ex) {
			log.error({ title: "IWSAPI_GENERAL_ERROR: loadTemplate", details: ex });
		}
	}
	
	
	/**
	 * Replaces an object located within the template
	 * 
	 * @access public
	 * @function replaceElement
	 * 
	 * @param {ElementObject} obj
	 * @returns {string}
	 */
	function replaceElement(obj) {
		try {
			if(obj.content != null) {
				if((obj != null && typeof obj.find !== undefined) && (obj != null && obj.replace !== undefined) && (obj != null && obj.content !== undefined)) {
					return obj.content.replace(obj.find, obj.replace);
				} else {
					return "IWASPI_MISSING_INPUT: Find/Replace cannot be null.";
				}
			} else {
				return "IWASPI_GENERAL_ERROR: Template must be loaded before using this method.";
			}
		} catch(ex) {
			log.error({ title: "IWSAPI_GENERAL_ERROR: replaceObject", details: ex });
		}
	}
	
	return {
		loadTemplate: loadTemplate,
		replaceElement: replaceElement,
	};
});