/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/Core/CLGX2_IWSAPI_Configuration
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/5/2018
 */
define(["N/record", "N/search", "N/cache"],
function(record, search, cache) {
	/**
	 * @typedef {Object} ParameterObject
	 * @property {number} version - The record version.
	 * @property {string} method - The SuiteTalk request method.
	 */
	/**
	 * @typedef {Object} ConfigurationObject
	 * @property {number} version - The record version.
	 * @property {string} method - The SuiteTalk request method.
	 * @property {string} url - The data center URL.
	 * @property {number} envelopeTemplateID - The internal id of the envelope template.
	 * @property {number} bodyTemplateID - The internal id of the request body template.
	 */
	
	
	/**
	 * Returns a configuration record based on the version and method fields.
	 * 
	 * @access private
	 * @function findConfigRecord
	 * 
	 * @param {ParameterObject} paramObject
	 * @returns {ConfigurationObject}
	 */
	function findConfigRecord(paramObject) {
		var configObject = new Object();
    	
    	if(paramObject != null && (typeof paramObject.version !== undefined) && (typeof paramObject.method !== undefined)) {
    		var filters = new Array();
    		filters.push(search.createFilter({ name: "custrecord_clgx_iwsapi_config_version", operator: "is", values: paramObject.version }));
    		filters.push(search.createFilter({ name: "custrecord_clgx_iwsapi_config_method",  operator: "is", values: paramObject.method  }));
    			
    		var columns = new Array();
    		columns.push(search.createColumn({ name: "custrecord_clgx_iwsapi_config_version"  }));
    		columns.push(search.createColumn({ name: "custrecord_clgx_iwsapi_config_method"   }));
    		columns.push(search.createColumn({ name: "custrecord_clgx_iwsapi_config_url"      }));
    		columns.push(search.createColumn({ name: "custrecord_clgx_iwsapi_config_envelope" }));
    		columns.push(search.createColumn({ name: "custrecord_clgx_iwsapi_config_body"     }));
    			
    		var searchObject = search.create({ type: "customrecord_clgx_iwsapi_configuration", filters: filters, columns: columns });
    		
    		searchObject.run().each(function(result) {
    			configObject.version            = result.getValue("custrecord_clgx_iwsapi_config_version");
    			configObject.method             = result.getValue("custrecord_clgx_iwsapi_config_method");
    			configObject.url                = result.getValue("custrecord_clgx_iwsapi_config_url");
    			configObject.envelopeTemplateID = result.getValue("custrecord_clgx_iwsapi_config_envelope");
    			configObject.bodyTemplateID     = result.getValue("custrecord_clgx_iwsapi_config_body");
    		});
    	}
    	
    	return configObject;
	}
	
	
	/**
	 * Wrapper function for the findConfigRecord method
	 * 
	 * @see findConfigRecord
	 * @access public
	 * @function getConfig
	 * 
	 * @param {ParameterObject} paramObject
	 * @returns {ConfigurationObject}
	 */
    function getConfig(paramObject) {
    	var configObject = new Object();
    	
    	if(paramObject != null && (typeof paramObject.version !== undefined) && (typeof paramObject.method !== undefined)) {
    		configObject = findConfigRecord({ version: paramObject.version, method: paramObject.method });
    	}
    	
    	return configObject;
    }
    
	
    return {
        getConfig: getConfig
    };
    
});
