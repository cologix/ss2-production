/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/Core/CLGX2_IWSAPI_SearchMap
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/16/2018
 */
define(["N/search"],
function(search) {
	/**
	 * Returns the mapped value from a custom record.
	 * 
	 * @access public
	 * @function getMappedValue
	 * 
	 * @param {string} fromValue - The SuiteTalk request method.
	 * @returns {string}
	 */
	function getMappedValue(fromValue) {
		if(fromValue != null && fromValue != "") {
			var finalValue = "";
			var columns    = new Array();
			var filters    = new Array();
			columns.push(search.createColumn({ name: "custrecord_clgx_iwsapi_map_from" }));
			columns.push(search.createColumn({ name: "custrecord_clgx_iwsapi_map_to" }));
			filters.push(search.createFilter({ name: "custrecord_clgx_iwsapi_map_from", operator: "contains", values: fromValue }));
			
			var searchObject = search.create({ type: "customrecord_clgx_iwsapi_searchmap", columns: columns, filters: filters });
			
			searchObject.run().each(function(result) {
				finalValue = result.getValue("custrecord_clgx_iwsapi_map_to");
				
				return true;
			});
			
			return finalValue;
		}
	}
	
    return {
        getMappedValue: getMappedValue
    };
    
});
