/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/Core/CLGX2_IWSAPI_Authentication
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/3/2018
 */
define(["N/record", "N/runtime", "N/cache"],
function(record, runtime, cache) {
	/**
	 * @typedef {Object} NSRecord
	 * @property {number} id - The internal id of the record.
	 * @property {boolean} isDynamic - Indicates whether the record is in dynamic or standard mode.
	 * @property {string} type - The record type.
	 */
	/**
	 * @typedef {Object} AuthObject
	 * @property {string} authenticationName - The name of the authentication record.
	 * @property {string} applicationID - The application ID specified in the Integration record.
	 * @property {number} accountID - The NetSuite account ID.
	 * @property {string} email - The user's login email.
	 * @property {string} password - The user's login password.
	 * @property {number} role - The internal id ofd the users role.
	 */
	
	
	/**
	 * Returns and authentication record as an object.
	 * 
	 * @access private
	 * @function getAuthRecord
	 * 
	 * @param {number} authRecordID - The authentication record's internal id.
	 * @returns {NSRecord}
	 */
	function getAuthRecord(authRecordID) {
		var authRecord = new Object();
		
		if(authRecordID != null && authRecordID != "") {
			try {
				authRecord = record.load({ type: "customrecord_clgx_iwsapi_auth", id: authRecordID });
			} catch(ex) {}
		}
		
		return authRecord;
	}
	
	
	/**
	 * Returns the authentication record as an object.
	 * 
	 * @access public
	 * @function authenticate
	 * 
	 * @param {number} authRecordID - The authentication record's internal id.
	 * @returns {AuthObject}
	 */
	function authenticate(authRecordID) {
		var authRecord    = new Object();
		var tmpAuthRecord = null;
		
		if((typeof authRecordID === "number") || (typeof authRecordID === "string")) {
			tmpAuthRecord = getAuthRecord(authRecordID);
		} else {
			tmpAuthRecord = getAuthRecord(authRecordID.key);
		}
		
		if(tmpAuthRecord != null && tmpAuthRecord != "") {
			authRecord.authenticationName = tmpAuthRecord.getValue("name");
			authRecord.applicationID      = tmpAuthRecord.getValue("custrecord_clgx_iwsapi_app_id");
			authRecord.accountID          = tmpAuthRecord.getValue("custrecord_clgx_iwsapi_account_id");
			authRecord.email              = tmpAuthRecord.getValue("custrecord_clgx_iwsapi_user_email");
			authRecord.password           = tmpAuthRecord.getValue("custrecord_clgx_iwsapi_user_password");
			authRecord.role               = tmpAuthRecord.getValue("custrecord_clgx_iwsapi_user_role");
		}
		
		return authRecord;
	}
	
	
    return {
        authenticate: authenticate
    };
    
});
