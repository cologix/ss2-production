/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module IWSAPI/Core/CLGX2_IWSAPI_Request
 * 
 * @author Alex Guzenski - Alex Guzenski alex.guzenski@cologix.com
 * @date 1/25/2018
 */
define(["N/https"],
function(https) {
	/**
	 * @typedef {Object} ConfigurationObject
	 * @property {number} version - The record version.
	 * @property {string} method - The SuiteTalk request method.
	 * @property {string} url - The data center URL.
	 * @property {number} envelopeTemplateID - The internal id of the envelope template.
	 * @property {number} bodyTemplateID - The internal id of the request body template.
	 */
	/**
	 * @typedef {Object} RequestObject
	 * @property {ConfigurationObject} config - Configuration object
	 * @property {string} body - The request body in XML.
	 */
	/**
	 * @typedef {Object} NSClientResponse
	 * @property {string} body - The HTTP response body.
	 * @property {number} code - The HTTP response code.
	 * @property {object} headers - The HTTP response body.
	 */
	
	
	/**
	 * Sends a SuiteTalk request to a specified URL.
	 * 
	 * @access public
	 * @function sendRequest
	 * 
	 * @param {RequestObject} requestObject - The request information object.
	 * 
	 * @throws {SSS_MISSING_REQD_ARGUMENT}
	 * @returns {NSClientResponse}
	 */
	function sendRequest(requestObject) {
		if(requestObject != null) {
			if((typeof requestObject.config !== undefined) && (typeof requestObject.body !== undefined)) {
				log.debug({ title: "", details: requestObject });
				return https.request({ method: "POST", url: requestObject.config.url, body: requestObject.body, headers: {"SOAPAction" : requestObject.config.method } });
			}
		}
	}
	
    return {
        sendRequest: sendRequest
    };
});
