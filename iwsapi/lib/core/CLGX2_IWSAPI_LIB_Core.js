/**
 * @NApiVersion 2.x
 */
define(["N/encode", "/SuiteScripts/clgx/libraries/cryptojs/crypto-js"],
function(encode, cryptojs) {
	
	/**
	 * Generates a request signature.
	 * 
	 * @param {Object} envelopeObj
	 * @return {String}
	 */
	function generateSignature(envelopeObj) {
		if(envelopeObj != null) {
			var baseString = (envelopeObj.accountID + "&" + envelopeObj.cKey + "&" + envelopeObj.token + "&" + envelopeObj.nonce + "&" + Date.now());
			var baseKey    = (envelopeObj.cSecret + "&" + envelopeObj.tokenSecret);
			var baseHash  = cryptojs.HmacSHA256(baseString, baseKey);
			var finalHmac = cryptojs.enc.Base64.stringify(baseHash);
			
			return finalHmac;
		}
	}
	
	
	/**
	 * Generates a random one-time token.
	 * 
	 * @param {Integer} tokenLength
	 * @return {String}
	 */
	function generateToken() {
		return cryptojs.enc.Hex.stringify(cryptojs.lib.WordArray.random(128/8));
	}
	
	return {
		generateSignature: generateSignature,
		generateToken: generateToken
	};
});
