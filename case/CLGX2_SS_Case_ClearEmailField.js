/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   2/22/2018
 */
define(["N/task", "N/record", "N/runtime"],
function(task, record, runtime) {
    function execute(scriptContext) {
    	var currentScript  = runtime.getCurrentScript();
    	var customerID     = parseInt(currentScript.getParameter("custscript_clgx2_ss_cef_cid"));
    	var executionCount = parseInt(currentScript.getParameter("custscript_clgx2_ss_cef_ec"));
    	
    	log.debug({ title: "CLGX2_SS_Case_ClearEmailField - currentScript", details: currentScript });
    	
    	try {
    		log.debug({ title: "CLGX2_SS_Case_ClearEmailField - customerID", details: customerID });
        	record.submitFields({ type: "customer", id: customerID, values: { email: "" } });
    	} catch(ex) {
    		log.error({ title: "CLGX2_SS_Case_ClearEmailField - Error", details: ex });
    	}
    }

    return {
        execute: execute
    };
    
});
