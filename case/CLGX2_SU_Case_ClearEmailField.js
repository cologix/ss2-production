/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType UserEventScript
 * 
 * @module Cologix/UserEvent/Case/CLGX2_SU_Case_ClearEmailField
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/29/2018
 * @description Clears the email from the email field on the customer record.
 */
define(["N/record", "N/runtime", "N/task"],
function(record, runtime, task) {
	/**
	 * @typedef {Object} NSRecord
	 * @property {number} id - The internal ID of a specific record.
	 * @property {boolean} isDynamic - Indicates whether the record is in dynamic or standard mode.
	 * @property {string} type - The record type.
	 */
	/**
	 * @typedef {Object} UserEventContext
	 * @property {NSRecord} newRecord - The new record.
	 * @property {NSRecord} oldRecord - The old record.
	 * @property {string} type - The trigger type. Use the context.UserEventType enum to set this value.
	 */
	
	/**
	 * Returns whether or not a company is a customer.
	 * 
	 * @access private
	 * @function isCustomer
	 * 
	 * @param {number} recordID - The internal id of the NetSuite record.
	 * @returns {boolean}
	 */
	function isCustomer(recordID) {
		if(recordID != null && recordID != "") {
			try {
				var customerRecord = null;
				
				if(typeof recordID === "string") {
					customerRecord = record.load({ type: "customer", id: recordID.trim() });
				} else if(typeof recordID === "number") {
					customerRecord = record.load({ type: "customer", id: recordID });
				}
				
				//Checks whether or not the status is CUSTOMER-Closed Won: 13
				var statusID = customerRecord.getValue("stage");
				if(statusID === "CUSTOMER") {
					return true;
				}
			} catch(ex) {
				return false;
			}
		}
		
		return false;
	}
	
	
	/**
	 * The afterSubmit entry point.
	 * 
	 * @access public
	 * @function afterSubmit
	 * 
	 * @param {UserEventContext} context
	 * @returns {null}
	 */
	function afterSubmit(context) {
		var cu = runtime.getCurrentUser();
		
		if(cu.id != 19434 && cu.id != "19434") {
			if(context.type == "create" || context.type == "edit") {
				var nr         = context.newRecord;
				var customerID = nr.getValue("company");
		
				if(customerID != 19434 && customerID != "19434") {
					if(isCustomer(customerID) == true) {
						log.debug({ title: "CLGX2_SU_Case_ClearEmailField - customerID", details: customerID });
						record.submitFields({ type: "customer", id: customerID, values: { email: "" } });
						/*var t          = task.create({taskType: "SCHEDULED_SCRIPT"});
			    		t.scriptId     = 1486;
						t.params       = { custscript_clgx2_ss_cef_cid: customerID, custscript_clgx2_ss_cef_ec: 0 };
						t.submit();*/
					} else {
						log.error({ title: "customerID is not a customer", details: context.type + " - " + customerID });
					}
				}
			}
		}
	}
	
    return {
        afterSubmit: afterSubmit
    };
    
});
