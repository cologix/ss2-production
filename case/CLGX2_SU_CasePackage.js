/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/search", "N/redirect"],
function(search, redirect) {
    function afterSubmit(scriptContext) {
    	try {
    		var newRecord  = scriptContext.newRecord;
    		var caseID     = newRecord.id;
    		var customerID = newRecord.getValue("company");
    		var caseStatus = newRecord.getValue("status"); 
    		
    		if(caseHasPackages(caseID) && caseStatus == 5) {
    			redirect.toSuitelet({ 
					scriptId: "1882", 
					deploymentId: "1", 
					parameters: { ci: caseID, cu: customerID } 
				});
    		}
    	} catch(ex) {
    		log.error({ title: "afterSubmit - Error", details: ex });
    	}
    }

    
    /**
     * Returns whether or not the current case has any packages tied to it.
     * 
     * @access private
     * @function caseHasPackages
     * 
     * @param {Number} caseID - The case internal id.
     * 
     * @return {Boolean}
     */
    function caseHasPackages(caseID) {
    	if(caseID) {
    		var columns = new Array();
    		columns.push(search.createColumn("internalid"));
    		
    		var filters = new Array();
    		filters.push(search.createFilter({ name: "custrecord_clgx_package_case", operator: "is", values: [caseID] }));
    		filters.push(search.createFilter({ name: "custrecord_clgx_pkg_delivery_status", operator: "is", values: [2] })); //Reveived/Stored
    		
    		var searchObject = search.create({ type: "customrecord_clgx_package", columns: columns, filters: filters }).run();
    		var searchPage   = searchObject.getRange({ start: 0, end: 1000 });
    		
    		if(searchPage.length > 0) {
    			return true;
    		}
    		
    		return false;
    	}
    }
    
    return {
        afterSubmit: afterSubmit
    };
    
});
