/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   3/20/2018
 */
define(["N/ui/serverWidget"],
function(widget) {
    function beforeLoad(context) {
    	var type  = context.type;
    	var field = context.form.addField({ id: "custpage_clgx_timebill_html", label: "Test", type: "inlinehtml" });
    	
    	var defaultValue = "";
    	defaultValue     = "<script type=\"text/javascript\">";
    	defaultValue    += "function onLoad() {";
    	
    	if(type == "view") {
    		defaultValue += "NS.jQuery(\"#timeitem_splits\").bind(\"DOMSubtreeModified\", function() { var header = document.querySelector(\"#timeitemheader td[data-label='Project ID']\"); var rows   = document.querySelectorAll(\"tr.uir-list-row-tr > td:nth-child(5)\"); if(rows && rows.length > 0) { for(var r = 0; r < rows.length; r++) { rows[r].style.display = 'none'; } } if(header) { header.style.display = 'none'; }});";
    	} else if(type == "edit" || type == "create") {
    		defaultValue += "NS.jQuery(\"#timeitem_splits\").bind(\"DOMSubtreeModified\", function() { var header = document.querySelector(\"#timeitem_headerrow td[data-label='Project ID']\"); var rows   = document.querySelectorAll(\"tr.uir-machine-row > td:nth-child(5)\"); if(rows && rows.length > 0) { for(var r = 0; r < rows.length; r++) { rows[r].style.display = 'none'; } } if(header) { header.style.display = 'none'; } });";
    	}
    	
    	defaultValue    += "}";
    	defaultValue    += "var owol = window.onload; window.onload = (function() { owol && owol(); onLoad(); });";
    	defaultValue    += "</script>";
    	
    	field.defaultValue = defaultValue;
    }

    return {
        beforeLoad: beforeLoad
    };
    
});
