/**
 * @NApiVersion 2.x
 */
define(['N/runtime'],
function(runtime) {
	
	/**
	 * Executes a block of code only for a user by ID or by name.
	 * 
	 * @return {Boolean}
	 */
	function forUser(object) {
		if(object != null) {
			var rtu = runtime.getCurrentUser();
			
			if(object.userId !== undefined) {
				if(rtu.id == object.userId) { return true; }
			} else if(object.userEmail !== undefined) {
				if(rtu.email == object.userEmail) { return true; }
			} else if(object.roleName !== undefined) {
				if(rtu.role == object.roleName) { return true; }
			} else if(object.roleId !== undefined) {
				if(rtu.roleId == object.roleId) { return true; }
			}
		}
		
		return false;
	}

    return { forUser: forUser };
});
