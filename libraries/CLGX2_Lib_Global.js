/**
 * @NApiVersion 2.x
 * @NModuleScope public
 */
define(["N/search"],
function(search) {
	function getMarketName(marketID) {
		var market = "";
		
		if(marketID > 0) {
			market = search.lookupFields({ type: search.Type.LOCATION, id: marketID, columns: ["name"] });
			if(market == null || market == "") { market = "All"; }
		} else { market = "All"; }
		
		return market;
	}
	
	
    return {
        getMarketName: getMarketName
    };
    
});
