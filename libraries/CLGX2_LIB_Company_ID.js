/**
 * @NApiVersion 2.x
 * @NModuleScope public
 */
define(["N/runtime"],
function(runtime) {
	function clgx_get_compid() {
		var compid = '1337135'
		if(runtime.envType === runtime.EnvType.SANDBOX) {
			compid += '_sb';
		}
		else if(runtime.envType === runtime.EnvType.BETA) {
			compid += '_rp';
		}
		else {}
		return compid;
	}
    return {
    		clgx_get_compid: clgx_get_compid
    };
});
