/**
 * @author Liz Soucy - Liz.Soucy@cologix.com
 * @date  8/13/2019
 * 
 * Script Name  : CLGX2_SU_Device_Monitor
 * Script File  : CLGX2_SU_Device_Monitor.js
 * Script ID    : customscript_clgx2_su_device_monitor
 * Deployment ID: customdeploy_clgx2_su_device_monitor
 * Deployed To  : CLGX_DCIM_Device
 * Event Type   : Edit
 * 
 * 
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define([ 'N/runtime', "N/search", "N/record", "N/email", "/SuiteScripts/clgx/libraries/lodash.min" ], function(runtime, search,
		record, email, _) {

	/**
	 * After Submit Entry Point
	 * 
	 * scriptContext
	 * 
	 * @return null
	 * @param context
	 */
	function afterSubmit(context) {
		try {

			if (runtime.executionContext == "USERINTERFACE" && context.type == context.UserEventType.EDIT) {
			
				var clgx_dcim_device = context.newRecord;
			    var parentDevice = clgx_dcim_device.getText('custrecord_clgx_dcim_device_parent');
			    if (parentDevice.length == 0)
			    	return;
				var parentid = clgx_dcim_device.getValue('custrecord_clgx_dcim_device_parent');
			    var parentDevice = clgx_dcim_device.getText('custrecord_clgx_dcim_device_parent');
			    var deviceid = context.newRecord.id;
				var device = clgx_dcim_device.getText('name');
				var deviceReports = search.load({
					type : 'customrecord_clgx_monitoring_device',
					id : "customsearch_clgx2_all_monitored_devices"
				});
				deviceReports.filters.push(search.createFilter({
					name : "custrecord_clgx_md_device",
					operator : search.Operator.ANYOF,
					values : deviceid
				}));
				var reportIDs = [];
				deviceReports.run().each(
						function(result) {
							reportIDs.push({
								"report_id" : parseInt(result
										.getValue(result.columns[2]))
							});
							return true;
						});
				reportIDs = _.uniq(_.map(reportIDs, 'report_id'));
				
				var parentDeviceReports = search.load({
					type : 'customrecord_clgx_monitoring_device',
					id : "customsearch_clgx2_all_monitored_devices"
				});
				
				 parentDeviceReports.filters.push(search.createFilter({
				 	name : "custrecord_clgx_md_device",
				 	operator : search.Operator.ANYOF,
				 	values : parentid
				 }));
				
				 if (reportIDs.length > 0){
				 	parentDeviceReports.filters.push(search.createFilter({
				 		name : "custrecord_clgx_md_report",
				 		operator : search.Operator.NONEOF,
				 		values : reportIDs
				 	}));
				 }

				var reports = [];
				
				parentDeviceReports.run().each(
						function(result) {
							reports.push({
								"report" : result
										.getText(result.columns[2])
							});
							return true;
						});
				
				
				var reportNames = "";
				reports.forEach(function (item) {
					reportNames += item.report + "<br/>";
				});

				if (reportNames.length > 0){
					
					var emails = [];
					var results = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx2_monitor_rpt_email" });
					results.run().each(function(result) {
						emails.push(parseInt(result.getValue(result.columns[0])));
						return true;
					});
					
					sendEmail({
						recipients : emails,
	        			subject: "Please review - " + device + " may need to be added to Monitoring reports",
	        			body: device + " is now linked to a parent device that is included on a Power or Cooling Monitoring report. <br/>" +
	        			"Please consider adding "	+ device + "  to the same monitoring report if applicable. <br/><br/>" + 
	        			"Parent Device: " + parentDevice + " is being monitored on Power or Cooling Monitoring Reports:<br/>" + 
	        			reportNames });
				}
			}
		} catch (e) {
			log.debug({
				title : "After Submit Error",
				details : e
			});
		}
	}
	
	
	 /**
     * Sends emails to list paramObject.recipients & currentUser
     * 
     * @access private
     * @function sendEmail
     * 
     * @param {Object} paramObject
     * @return null
     */
    function sendEmail(paramObject) {
    	if(paramObject) {
    		
    		var currentUser = runtime.getCurrentUser();
    		paramObject.recipients.push(currentUser.id);
    		paramObject.recipients = _.uniq(paramObject.recipients);
			
    		email.send({
    			author    : currentUser.id,
    			recipients: paramObject.recipients,
    			subject   : paramObject.subject,
    			body      : paramObject.body
    		});
    	}
    }

	return {
		afterSubmit : afterSubmit
	};

});
