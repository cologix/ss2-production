/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 8/10/2020
 */
define(["N/search", "N/record", "N/runtime", "N/file", "N/task", "/SuiteScripts/clgx/libraries/lodash.min", "/SuiteScripts/clgx/command/lib/CLGX2_LIB_Powers"],
function(search, record, runtime, file, task, _, lib) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	try {
    		var currentScript = runtime.getCurrentScript();
    		
    		var powersArray       = lib.getAllPowers();
    		currentScript.percentComplete = 9;
    		
    		var servicesArray     = lib.getAllServices();
    		currentScript.percentComplete = 18;
    		
    		var orphansArray      = lib.getAllOrphans();
    		currentScript.percentComplete = 27;
    		
    		var mergedPowersArray = lib.mergeServicesAndPowers({ powers: powersArray, services: servicesArray });
    		currentScript.percentComplete = 36;
    		
    		buildPowerCounts(powersArray);
    		currentScript.percentComplete = 45;
    		
    		buildServicePowers(servicesArray);
    		currentScript.percentComplete = 54;
    		
    		buildAllPowers(mergedPowersArray);
    		currentScript.percentComplete = 63;
    		
    		buildTopology(mergedPowersArray);
    		currentScript.percentComplete = 72;
    		
    		buildProvisionedPowers(mergedPowersArray);
    		currentScript.percentComplete = 81;
    		
    		buildMisconfigPowers(mergedPowersArray);
    		currentScript.percentComplete = 90;
    		
    		buildOrphanPowers(orphansArray);
    		
    		log.debug({ title: "", details: runtime.getCurrentScript().getRemainingUsage() });
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }
    
    /**
     * Creates the CLGX_JSON_CMD_Powers_Counts.json file.
     * 
     * @access private
     * @function buildPowerCounts
     * 
     * @param {Array} powersArray
     */
    function buildPowerCounts(powersArray) {
    	try {
    		var finalPowerArray = [];
	    	var ids = _.uniq(_.map(powersArray, "serviceid"));
	    	for (var i = 0; i < ids.length; i++) {
	    		var selection = _.filter(powersArray, function(finalPowerArray){
					return (finalPowerArray.serviceid == ids[i]);
				});
	    		
	    		finalPowerArray.push({
					"customerid": selection[0].customerid,
					"locationid": selection[0].locationid,
					"location"  : selection[0].location,
					"soid"      : selection[0].soid,
					"serviceid" : selection[0].serviceid,
					"count"     : selection.length
	    		});
	    	}
    		
	    	lib.createBufferedFile({ name: "CLGX_JSON_CMD_Powers_Counts.json", folder: 3824960, data: JSON.stringify(finalPowerArray) });
    	} catch(ex) {
    		log.error({ title: "buildPowerCounts - Error", details: ex });
    	}
    }
    
    function buildServicePowers(serviceArray) {
    	try {
    		lib.createBufferedFile({ name: "CLGX_JSON_CMD_Powers_Services.json", folder: 3824960, data: JSON.stringify(serviceArray) });
    	} catch(ex) {
    		log.error({ title: "buildServicePowers - Error", details: ex });
    	}
    }
    
    function buildAllPowers(powersArray) {
    	try {    		
    		lib.createBufferedFile({ name: "CLGX_JSON_CMD_Powers_All.json", folder: 3824960, data: JSON.stringify(powersArray) });
    	} catch(ex) {
    		log.error({ title: "buildAllPowers - Error", details: ex });
    	}
    }
    
    function buildTopology(powerArray) {
    	try {
    		var topology = JSON.stringify(lib.buildPowerTopology(powerArray));
    		lib.createBufferedFile({ name: "CLGX_JSON_CMD_Powers_Topology.json", folder: 3824960, data: topology });
    	} catch(ex) {
    		log.error({ title: "buildPowerTopology - Error", details: ex });
    	}
    }
    
    function buildProvisionedPowers(powerArray) {
    	try {
    		var provisioned = _.filter(powerArray, function(arr) {
	    		return (arr.categoryid > 0);
	    	});
    		
	    	var tree = JSON.stringify(lib.buildGridTree({ inventory: "Powers", type: "Provisioned", grid: provisioned }));
	    	lib.createBufferedFile({ name: "CLGX_JSON_CMD_Powers_Provisioned.json", folder: 3824960, data: tree });
    	} catch(ex) {
    		log.error({ title: "buildProvisionedPowers - Error", details: ex });
    	}
    }
    
    function buildMisconfigPowers(powerArray) {
    	try {
    		var misconfigs = _.filter(powerArray, function(arr){
	    		return (arr.categoryid == 0);
	    	});
    		
	    	var tree = JSON.stringify(lib.buildGridTree({ inventory: "Powers", type: "Misconfigs", grid: misconfigs }));
	    	lib.createBufferedFile({ name: "CLGX_JSON_CMD_Powers_Misconfigs.json", folder: 3824960, data: tree });
    	} catch(ex) {
    		log.error({ title: "buildMisconfigPowers - Error", details: ex });
    	}
    }
    
    function buildOrphanPowers(orphanArray) {
    	try {
    		var ids = _.uniq(_.map(orphanArray, "facilityid"));
			var facilities = [];
			for ( var i = 0; i < ids.length; i++ ) {
				var children = _.filter(orphanArray, function(arr) {
					return (arr.facilityid == ids[i]);
				});
				
				if(children) {
					facilities.push({
						"nodeid"  : children[0].facilityid,
						"node"    : children[0].facility,
						"type"    : "facility",
						"faicon"  : "building",
						"expanded": false,
						"leaf"    : false,
						"count"   : children.length,
						"children": children
					});
				}
			}
			var tree = {
				"text"     : ".",
				"inventory": "Powers",
				"type"     : "Orphans",
				"count"    : orphanArray.length,
				"children" : facilities
			};
			
			lib.createBufferedFile({ name: "CLGX_JSON_CMD_Powers_Orphans.json", folder: 3824960, data: JSON.stringify(tree) });
    	} catch(ex) {
    		log.error({ title: "buildOrphanPowers - Error", details: ex });
    	}
    }
    
    return {
        execute: execute
    };
    
});
