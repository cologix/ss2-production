/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/file", "N/https", "/SuiteScripts/clgx/libraries/lodash.min", "N/encode", "N/runtime", "N/task"],
    function(file, https, _, encode, runtime, task) {
        function execute(context) {
            try {
                var startTime = new Date().getTime();

                while((runtime.getCurrentScript().getRemainingUsage() > 200) && (((new Date().getTime() - startTime) / 60000) < 30)) {
                    getNACAlarms();
                    getODAlarms();
                    sleep(5);
                }

                scheduleScript({
                    scriptID: 1714,
                    deploymentID: "customdeploy_clgx2_ss_update_alarms"
                });

             try {
                    var startTime = new Date().getTime();
                    getNACAlarms();
                    getODAlarms();
                    var endTime = new Date().getTime();

                    var executionTime = Math.round(((endTime - startTime) / 1000));
                    var sleepTime     = Math.floor((300 - executionTime) / 60);

                    sleep(sleepTime);

                    scheduleScript({
                        scriptID: 1714,
                        deploymentID: "customdeploy_clgx2_ss_update_alarms"
                    });
                } catch(ex) {
                    log.error({ title: "ERROR", details: ex });
                }
            } catch(ex) {
                log.error({ title: "ERROR", details: ex });
            }
        }


        /**
         * Gets the nacas alarms and saves them to a json file in the file cabinet.
         *
         * @access private
         * @function getNACAlarms
         *
         * @libraries N/https, N/file, lodash.min
         *
         * @returns null
         */
        function getNACAlarms() {
            try {
                var deviceRequest = https.get({
                    headers: {
                        "Content-Type" : "application/json",
                        "Authorization": "Basic " + encode.convert({
                            string: "4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ",
                            inputEncoding: encode.Encoding.UTF_8,
                            outputEncoding: encode.Encoding.BASE_64
                        })
                    },
                    url: "https://nsapi1.dev.nac.net/v1.0/monitoring/alarms/?per_page=0&status=1&alarm_status_not=Info"
                });

                var nacasBody = JSON.parse(deviceRequest.body);
                var flexArray = nacasBody.data;
                var ids       = _.uniq(_.map(flexArray, "id"));

                var nacasRequest = https.get({ url: "https://lucee-nnj3.dev.nac.net/nacas/alarms/get_alarms.cfm?ids=" + ids.join() });
                var nacasArray   = JSON.parse(nacasRequest.body);

                var flexArrayLength = flexArray.length;
                for(var i = 0; flexArray != null && i < flexArray.length; i++) {
                    flexArray[i].comment = flexArray[i].cleared_by;
                    var tmpObject = _.find(nacasArray, function(array) { return (array.ID == flexArray[i].id); });

                    if(tmpObject && tmpObject.COMMENT != "") {
                        flexArray[i].comment = tmpObject.COMMENT;
                    }
                }

                createJSONFile({
                    name: "clgx-cmd-nacas-alarms.json",
                    folder: 3824960,
                    content: JSON.stringify(flexArray)
                });
            } catch(ex) {
                log.error({ title: "Error - getNACAlarms", details: ex });
            }
        }


        /**
         * Gets the odins alarms and saves them to a json file in the file cabinet.
         *
         * @access private
         * @function getNACAlarms
         *
         * @libraries N/https, N/file
         *
         * @returns null
         */
        function getODAlarms() {
            try {
                var odinsRequest = https.get({ url: "https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms.cfm" });
                var odinsArray   = JSON.parse(odinsRequest.body);
                var odinsTree    = getAlarmsTreeJSON(odinsArray);

                createJSONFile({
                    name: "clgx-cmd-odins-alarms.json",
                    folder: 3824960,
                    content: JSON.stringify(odinsTree)
                });
            } catch(ex) {
                log.error({ title: "Error - getODAlarms", details: ex });
            }
        }


        function getAlarmsTreeJSON(arrAlarms){

            var userid = runtime.getCurrentUser().id;
            var arrAlarmsFAMs = new Array();
            for ( var i = 0; arrAlarms != null && i < arrAlarms.length; i++ ) {

                var objAlarmsFAM = new Object();

                objAlarmsFAM["deviceid"] = parseInt(arrAlarms[i].DEVICE_ID);
                objAlarmsFAM["devicename"] = arrAlarms[i].DEVICE_NAME;
                objAlarmsFAM["devicecode"] = arrAlarms[i].DEVICE_CODE;

                objAlarmsFAM["pointid"] = parseInt(arrAlarms[i].POINT_ID);
                objAlarmsFAM["pointname"] = arrAlarms[i].POINT_NAME;
                objAlarmsFAM["pointcode"] = arrAlarms[i].POINT_CODE;

                objAlarmsFAM["eventid"] = parseInt(arrAlarms[i].EVENT_ID);
                objAlarmsFAM["eventname"] = arrAlarms[i].EVENT_NAME;
                objAlarmsFAM["eventcode"] = arrAlarms[i].EVENT_CODE;
                objAlarmsFAM["eventresp"] = arrAlarms[i].RESPONSE;

                objAlarmsFAM["date"] = arrAlarms[i].DATE;
                objAlarmsFAM["severity"] = parseInt(arrAlarms[i].SEVERITY);

                objAlarmsFAM["famid"] = Number(arrAlarms[i].EXTERNAL_ID.substring(3));
                objAlarmsFAM["fam"] = arrAlarms[i].EXTERNAL_ID;
                objAlarmsFAM["userid"]=userid;

                arrAlarmsFAMs.push(objAlarmsFAM);
            }

            return arrAlarmsFAMs;
        }


        /**
         * @typedef CreateJSONParamObject
         * @property {String} name - The name of the file in the file cabinet.
         * @property {Number} folder - The internal id of the destination folder in the file cabinet.
         * @property {String} content - The contents of the file.
         */
        /**
         * Creates a JSON file.
         *
         * @access private
         * @function createJSONFile
         *
         * @libraries N/file
         *
         * @param {CreateJSONParamObject} paramObject
         * @returns {Number}
         */
        function createJSONFile(paramObject) {
            var fileObject = file.create({
                name: paramObject.name,
                fileType: file.Type.JSON,
                folder: paramObject.folder,
                contents: paramObject.content
            });

            return fileObject.save();
        }


        /**
         * Delays script execution for the specified number of minutes.
         *
         * @access private
         * @function sleep
         *
         * @libraries none
         *
         * @param {Number} minutes [required] - The number of minutes expressed as a whole integer from 1 to 14.
         * @returns null
         */
        function sleep (minutes) {
            var start = new Date().getTime();
            while (new Date().getTime() < start + minutes * 60000);
        }


        /**
         * Schedules a script.
         *
         * @access private
         * @function scheduleScript
         *
         * @libraries N/task
         *
         * @param {Object} paramObject
         * @returns void
         */
        function scheduleScript(paramObject) {
            var scriptTask          = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});

            scriptTask.scriptId     = paramObject.scriptID;
            scriptTask.deploymentId = paramObject.deploymentID;

            if(paramObject.params !== undefined) {
                scriptTask.params = paramObject.params;
            }

            var scriptTaskId = scriptTask.submit();
        }


        return {
            execute: execute
        };
    });