/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/sftp", "N/file", "N/runtime"],
function(sftp, file, runtime) {
    function execute(scriptContext) {
    	try {
    		var sc              = runtime.getCurrentScript();
        	var sourceName      = sc.getParameter("custscript_clgx2_sftp_sn");
        	var sourceDirectory = sc.getParameter("custscript_clgx2_sftp_sd");
        	
        	var destName      = sc.getParameter("custscript_clgx2_sftp_dn");
        	var destDirectory = parseInt(sc.getParameter("custscript_clgx2_sftp_ddid"));
        	
        	var connectionObject = sftp.createConnection({
    			url: "13.58.59.85",
    			username: "netsuite",
    			passwordGuid: "5a12896458c441bdb6983d1264be904c",
    			hostKey: "AAAAB3NzaC1yc2EAAAADAQABAAABAQDIkeUi0S+EK5I0HHifzPcAuwE2EYl+Lu09oIoeMw6xF7yLsYnHJOZFvU8fN1lgeniQ14lJJHQyCKhmJC66n1K9TSrow3nJRQeo3107qSWcljBEjIT8BfITe0lzX/HOGqQqC6/VrCeUe86kmWaMEag37ImajBUEaE6nD+YoOXOsNj4qkjkpzKPh9lDTL0UcSIyqxwpr+YAYG+DqAYqh2jh/5lq4aSxKxmvvh+Za7l2GgnSQeHMzFQBRRn545sr3KBWYilD2qWvMktB1tp6173pLOSnHr2wM/lWCRiWcr+wEZ4z3laYajfO/qZm7rH3hRBJpzBOSLInfEhhTRolepcKX"
    		});
        	
        	var fileObject = connectionObject.download({
        		filename: sourceName,
        		directory: sourceDirectory
        	});
        	
        	fileObject.name   = destName;
        	fileObject.folder = destDirectory;
        	fileObject.save();
    	} catch(ex) {
    		log.debug({ title: "Error", details: ex });
    	} 
    }

    return {
        execute: execute
    };
    
});
