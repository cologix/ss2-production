/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   5/31/2018
 */
define(["N/https", "N/crypto"],
function(https, crypto) {
	function onExecute(context) {
		var response = https.post({ 
			url: "https://nsapi1.dev.nac.net/v1.0/monitoring/alarms/?per_page=0&status=1&alarm_status_not=Info", 
			headers: {"Content-Type": "application/json"}, 
			body: {"Authorization" : "Basic " + btoa("4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ")} });
		
		log.debug({ title: "", details: response.body });
	}
	
    return {
        onExecute: onExecute
    };
    
});
