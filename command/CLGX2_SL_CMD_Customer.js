/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/search", "N/file", "/SuiteScripts/clgx/libraries/lodash.min", "/SuiteScripts/clgx/command/lib/CLGX2_LIB_CMD", "N/runtime"],
function(search, file, _, lib, runtime) {
	
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function onRequest(scriptContext) {
    	try {
    		var customerID = scriptContext.request.parameters.customerid || 0;
    		var locationID = scriptContext.request.parameters.locationid || 0;
    		
    		var customerNameRaw = search.lookupFields({ type: "customer", id: customerID, columns: "entityid" }).entityid;
    		var customerName    = lib.formatFieldName({ text: customerNameRaw, delimiter: ":" });
    		
    		var rows   = lib.getRows({ customerID: customerID, locationID: locationID });
    		var rowIDs = _.uniq(_.map(rows, "serviceid"));
    		
    		var spaces  = lib.getInventory({ customerID: parseInt(customerID), locationID: parseInt(locationID), fileID: 4830208 });
    		var powers  = lib.getInventory({ customerID: parseInt(customerID), locationID: parseInt(locationID), fileID: 4833030 });
    		var net     = lib.getInventory({ customerID: parseInt(customerID), locationID: parseInt(locationID), fileID: 4875312 });
    		var xcs     = lib.getInventory({ customerID: parseInt(customerID), locationID: parseInt(locationID), fileID: 4834334 });
    		var vxcs    = lib.getInventory({ customerID: parseInt(customerID), locationID: parseInt(locationID), fileID: 4662283 });
    		var managed = lib.getInventory({ customerID: parseInt(customerID), locationID: parseInt(locationID), fileID: 11063851 });
    		
    		var inventoryTreeParameter = {
    			"customerID": customerID,
    			"locationID": locationID,
    			"customerName": customerName,
    			"spaces": spaces,
    			"powers": powers,
    			"net": net,
    			"xcs": xcs,
    			"vxcs": vxcs,
    			"managed": managed,
    			"rows": rows
    		};
    		
    		var inventoryTree    = lib.buildInventoryTree(inventoryTreeParameter);
    		var serviceOrderTree = lib.buildServiceOrderTree(inventoryTreeParameter);
    		
    		var fileObject = file.load({ id: 4577619 });
    		var html = fileObject.getContents();
    		html = html.replace(new RegExp('{treeSOs}','g'), JSON.stringify(serviceOrderTree));
    		html = html.replace(new RegExp('{treeInv}','g'), JSON.stringify(inventoryTree));
    		
    		scriptContext.response.write(html);
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }
    
    return {
    	onRequest: onRequest
    };
});
