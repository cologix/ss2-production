/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/search", "N/record", "/SuiteScripts/clgx/libraries/lodash.min", "N/file"], 
function(search, record, _, file) {
	function onRequest(context) {
		try {
			var request = context.request;
			var response = context.response;
			
			var customerID = request.parameters.customerid || 0;
			var facilityID = request.parameters.facilityid || 0;
			var famID      = request.parameters.famid || 0;
			var allPowers  = request.parameters.allpwrs || 0;
			
			if(customerID > 0 || famID > 0) {
				var finalPowers  = new Array();
				
				var services = getCustomerServices(customerID);
				
				var searchObject = search.load({ id: "customsearch_clgx2_cmd_powers_all" });
				
				if(customerID > 0) {
					searchObject.filters.push(search.createFilter({ name: "custrecord_cologix_power_service", operator: "anyof", values: services }));
				}
				
				if(famID > 0) {
					
				}
				
				if(allPowers > 0) {
					
				}
				
				searchObject.filters.push(search.createFilter({ name: "isinactive", operator: "is", values: ["F"] }));
				searchObject.run().each(function(result) {
					var columns = result.columns;
					var tmpPower = new Object();
					
					var contractAmps = parseFloat(result.getText(columns[9]).replace("A", ""));
					
					tmpPower["facilityid"] = parseInt(result.getValue(columns[0]));
					tmpPower["facility"]   = result.getText(columns[0]);
					tmpPower["panelid"]    = parseInt(result.getValue(columns[1]));
					tmpPower["panel"]      = result.getText(columns[1]);
					tmpPower["powerid"]    = parseInt(result.getValue(columns[2]));
					tmpPower["power"]      = result.getValue(columns[3]);
					tmpPower["pairid"]     = parseInt(result.getValue(columns[4]));
					tmpPower["pair"]       = result.getText(columns[4]);
					tmpPower["spaceid"]    = parseInt(result.getValue(columns[5]));
					tmpPower["space"]      = result.getText(columns[5]);
			    	tmpPower["serviceid"]  = parseInt(result.getValue(columns[6]));
			    	tmpPower["service"]    = _.last((result.getText(columns[6])).split(" : "));
					tmpPower["soid"]       = parseInt(result.getValue(columns[7]));
			    	tmpPower["so"]         = (result.getText(columns[7])).replace("Service Order #", "");
			    	tmpPower["volts"]      = result.getText(columns[8]);
			    	tmpPower["contract"]   = result.getText(columns[9]);
					tmpPower["module"]     = parseInt(result.getValue(columns[10]));
					tmpPower["breaker"]    = parseInt(result.getValue(columns[11]));
					var kwavg              = parseFloat(result.getValue(columns[12]));
					
					if(kwavg > 0) { tmpPower["kwavg"] = kwavg; } else { tmpPower["kwavg"] = 0; }
					
					
					tmpPower["amps"]     = parseFloat(result.getValue(columns[15]));
					tmpPower["ampspair"] = parseFloat(result.getValue(columns[17]));
					tmpPower["absum"]    = parseFloat(result.getValue(columns[16]));
					tmpPower["usage"]    = parseFloat(result.getValue(columns[14]));
					
					finalPowers.push(tmpPower);
					
					return true;
				});
				
				var fileObject = file.load({ id: 4495118 });
				var html = fileObject.getContents();
				html = html.replace(new RegExp('{powersJSON}','g'), JSON.stringify(finalPowers));
				
				response.write(html);
			}
		} catch(ex) {
			log.error({ title: "onRequest - Error", details: ex });
		}
	}
	
	
	function getCustomerServices(customerID) {
		if(customerID) {
			var services = new Array();
			
			var columns = new Array();
			columns.push(search.createColumn({ name: "custrecord_cologix_power_service", summary: "GROUP" }));
			
			var filters = new Array();
			filters.push(search.createFilter({ join: "custrecord_cologix_power_service", name: "parent", operator: "anyof", values: [customerID] }));
			filters.push(search.createFilter({ name: "isinactive", operator: "is", values: ["F"] }));
			
			var searchObject = search.create({ type: "customrecord_clgx_power_circuit", columns: columns, filters: filters });
			searchObject.run().each(function(result) {
				services.push(parseInt(result.getValue({ name: "custrecord_cologix_power_service", summary: "GROUP" })));
				return true;
			});
			
			return _.uniq(services);
		}
	}
	
	return {
		onRequest: onRequest
	}
});