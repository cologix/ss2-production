/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/file"],
function(file) {
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	try {
    		var requestType = context.request.parameters.type;
    		var filesObject = new Object();
    		filesObject["complete"]  = 0;
    		filesObject["misconfig"] = 0;
    		filesObject["orphans"]   = 0;
    		
    		if(requestType == "spaces") {
    			filesObject["complete"]  = 4830307;
        		filesObject["misconfig"] = 4830308;
        		filesObject["orphans"]   = 4830309;
    		} else if(requestType == "powers") {
    			filesObject["complete"]  = 4833031;
        		filesObject["misconfig"] = 4833032;
        		filesObject["orphans"]   = 4833033;
    		} else if(requestType == "xcs") {
    			filesObject["complete"]  = 4834634;
        		filesObject["misconfig"] = 4834636;
        		filesObject["orphans"]   = 4834637;
    		} else if(requestType == "network") {
    			filesObject["complete"]  = 4834635;
        		filesObject["misconfig"] = 4834636;
        		filesObject["orphans"]   = 4834637;
    		} else if(requestType == "vxcs") {
    			filesObject["complete"]  = 4835934;
        		filesObject["misconfig"] = 4835434;
        		filesObject["orphans"]   = 4662285;
    		} else if(requestType == "managed") {
    			filesObject["complete"]  = 11065154;
        		filesObject["misconfig"] = 11065255;
        		filesObject["orphans"]   = 11065656;
    		}
    		
    		var fileObject = file.load({ id: 33737659 });
    		var html       = fileObject.getContents();
    		
    		var complete  = JSON.parse(bufferedFileLoad(filesObject.complete));
    		var misconfig = JSON.parse(bufferedFileLoad(filesObject.misconfig));
    		var orphans   = JSON.parse(bufferedFileLoad(filesObject.orphans));
    		
    		html = html.replace(new RegExp('{complete}','g'), JSON.stringify(complete));
    		html = html.replace(new RegExp('{misconfig}','g'), JSON.stringify(misconfig));
    		html = html.replace(new RegExp('{orphans}','g'), JSON.stringify(orphans));
    		
    		context.response.write(html);
    	} catch(ex) {
    		log.error({ title: "onRequest - Error", details: ex });
    	}
    }

    return {
        onRequest: onRequest
    };
    
    
    /**
     * Returns a file loaded into memory one line at a time.
     * 
     * @access private
     * @function bufferedFileLoad
     * 
     * @param {Number} fileID
     * @return {String}
     */
    function bufferedFileLoad(fileID) {
    	var fileContents = new String(); 
    	var fileObject   = file.load({ id: parseInt(fileID) });
    	
    	var iteratorObject = fileObject.lines.iterator();
    	iteratorObject.each(function(line) {
    		fileContents += line.value;
    		return true;
    	});
    	
    	return fileContents;
    }
});
