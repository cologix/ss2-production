define(["N/file", "N/search"],
function(file, search) {
	/**
     * 
     * @access private
     * @function formatFieldName
     * 
     * @param {Object} paramObject
     * @param {String} paramObject.text
     * @param {String} paramObject.delimiter
     * 
     * @returns {String}
     */
    function formatFieldName(paramObject) {
    	if(paramObject) {
    		var tmpRawString = paramObject.text;
    		
    		if(!tmpRawString) { return ""; }
    		
    		tmpRawString = tmpRawString.split(paramObject.delimiter);
    		tmpRawString = tmpRawString[tmpRawString.length - 1].trim();
    		
    		return tmpRawString; 
    	}
    }
	
    
    /**
     * Returns a file loaded into memory one line at a time.
     * 
     * @access private
     * @function bufferedFileLoad
     * 
     * @param {Number} fileID
     * @return {String}
     */
    function bufferedFileLoad(fileID) {
    	var fileContents = new String(); 
    	var fileObject   = file.load({ id: parseInt(fileID) });
    	
    	var iteratorObject = fileObject.lines.iterator();
    	iteratorObject.each(function(line) {
    		fileContents += line.value;
    		return true;
    	});
    	
    	return fileContents;
    }
    
    
    /**
     * Returns an ExtJS tree array to be displayed.
     * 
     * @access public
     * @function buildInventoryTree
     * 
     * @param {Object} paramObject
     * @param {Object} paramObject.spaces
     * @param {Object} paramObject.powers
     * @param {Object} paramObject.net
     * @param {Object} paramObject.xcs
     * @param {Object} paramObject.vxcs
     * @param {Object} paramObject.managed
     * @returns {Array}
     */
    function buildInventoryTree(paramObject) {
    	if(paramObject) {
    		var inventoryArray = new Array();
    		
    		var spaces  = paramObject.spaces;
    		var powers  = paramObject.powers;
    		var net     = paramObject.net;
    		var xcs     = paramObject.xcs;
    		var vxcs    = paramObject.vxcs;
    		var managed = paramObject.managed;
    		
    		if(spaces.length > 0) {
    			inventoryArray.push({
    				"node":'Spaces',
    				"type":'spaces',
    				"faicon": "map-o",
    				"expanded": false,
    				"leaf": false,
    				"children": spaces
    			});
    		}
    		
    		if(powers.length > 0) {
    			inventoryArray.push({
    				"node":'Powers',
    				"type":'powers',
    				"faicon": "plug",
    				"expanded": false,
    				"leaf": false,
    				"children": powers
    			});	
    		}
    		
    		if(net.length > 0) {
    			inventoryArray.push({
    				"node":'Network',
    				"type":'nets',
    				"faicon": "share-alt-square",
    				"expanded": false,
    				"leaf": false,
    				"children": net
    			});
    		}
    		
    		if(xcs.length > 0) {
    			inventoryArray.push({
    				"node":'XCs',
    				"type":'xcs',
    				"faicon": "share-alt-square",
    				"expanded": false,
    				"leaf": false,
    				"children": xcs
    			});
    		}
    		
    		if(vxcs.length > 0) {
    			inventoryArray.push({
    				"node":'VXCs',
    				"type":'vxcs',
    				"faicon": "share-alt",
    				"expanded": false,
    				"leaf": false,
    				"children": vxcs
    			});
    		}
    		
    		if(managed.length > 0) {
    			inventoryArray.push({
    				"node":'Managed',
    				"type":'managed',
    				"faicon": "gear",
    				"expanded": false,
    				"leaf": false,
    				"children": managed
    			});
    		}
    		
    		var treeInv = {
    			"text": ".",
    			"children": inventoryArray
    		};
    		
    		return treeInv;
    	}
    }
    
    
    /**
     * Returns an ExtJS tree array to be displayed.
     * 
     * @access public
     * @function buildServiceOrderTree
     * 
     * @param {Object} paramObject
     * @returns {Array}
     */
    function buildServiceOrderTree(paramObject) {
    	if(paramObject) {
    		var customerID   = paramObject.customerID;
    		var customerName = paramObject.customerName;
    		
    		var rows       = paramObject.rows;
    		var spaces     = paramObject.spaces;
    		var powers     = paramObject.powers;
    		var net        = paramObject.net;
    		var xcs        = paramObject.xcs;
    		var vxcs       = paramObject.vxcs;
    		var managed    = paramObject.managed;
    		
    		for (var row = 0; row < rows.length; row++) {
    			var arr = _.filter(spaces, function(arr) {
    				return (arr.soid == rows[row].soid && arr.serviceid == rows[row].serviceid);
    			});
    			
    			if(arr.length > 0){
    				if(rows[row].cages > 0){
    					rows[row].inv_space = arr.length;
    					rows[row].sos_space = arr.length
    				} else {
    					rows[row].inv_space = arr.length;
    				}
    				rows[row].children.push({
    					"node":'Spaces',
    					"type":'spaces',
    					"faicon": "map-o",
    					"expanded": false,
    					"leaf": false,
    					"children": arr
    				});
    			}
    			
    			var arr = _.filter(powers, function(arr){
    				return (arr.soid == rows[row].soid && arr.serviceid == rows[row].serviceid);
    			});
    			if(arr.length > 0){
    				rows[row].inv_power = arr.length;
    				rows[row].children.push({
    					"node":'Powers',
    					"type":'powers',
    					"faicon": "plug",
    					"expanded": false,
    					"leaf": false,
    					"children": arr
    				});
    			}
    			
    			var arr = _.filter(net, function(arr){
    				return (arr.soid == rows[row].soid && arr.serviceid == rows[row].serviceid);
    			});
    			if(arr.length > 0){
    				rows[row].inv_net = arr.length;
    				rows[row].children.push({
    					"node":'Network',
    					"type":'nets',
    					"faicon": "share-alt-square",
    					"expanded": false,
    					"leaf": false,
    					"children": arr
    				});
    			}
    			
    			var arr = _.filter(xcs, function(arr){
    				return (arr.soid == rows[row].soid && arr.serviceid == rows[row].serviceid);
    			});
    			if(arr.length > 0){
    				rows[row].inv_xcs = arr.length;
    				rows[row].children.push({
    					"node":'XCs',
    					"type":'xcs',
    					"faicon": "share-alt-square",
    					"expanded": false,
    					"leaf": false,
    					"children": arr
    				});
    			}
    			
    			var arr = _.filter(vxcs, function(arr){
    				return (arr.soid == rows[row].soid && arr.serviceid == rows[row].serviceid);
    			});
    			if(arr.length > 0){
    				rows[row].inv_vxcs = arr.length;
    				rows[row].children.push({
    					"node":'VXCs',
    					"type":'vxcs',
    					"faicon": "share-alt",
    					"expanded": false,
    					"leaf": false,
    					"children": arr
    				});
    			}
    			
    			var arr = _.filter(managed, function(arr){
    				return (arr.soid == rows[row].soid && arr.serviceid == rows[row].serviceid);
    			});
    			if(arr.length > 0){
    				rows[row].inv_managed = arr.length;
    				rows[row].children.push({
    					"node":'Managed',
    					"type":'managed',
    					"faicon": "gear",
    					"expanded": false,
    					"leaf": false,
    					"children": arr
    				});
    			}
    		}
    		
    		var sos = [];
    		var sosids = _.uniq(_.map(rows, "soid"));
    		
    		for ( var row = 0; row < sosids.length; row++ ) {
    			
    			var arrSO = _.filter(rows, function(arr){
    				return (arr.soid == sosids[row]);
    			});
    			
    			var sos_count = 1;
    			var arr = _.map(arrSO, 'serv_count');
    			var serv_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			
    			var arr = _.map(arrSO, 'sos_space');
    			var sos_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'sos_power');
    			var sos_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'sos_net');
    			var sos_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'sos_xcs');
    			var sos_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'sos_vxcs');
    			var sos_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'sos_managed');
    			var sos_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);

    			var arr = _.map(arrSO, 'inv_space');
    			var inv_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'inv_power');
    			var inv_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'inv_net');
    			var inv_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'inv_xcs');
    			var inv_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'inv_vxcs');
    			var inv_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			var arr = _.map(arrSO, 'inv_managed');
    			var inv_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			
    			sos.push({
    				"nodeid": arrSO[0].soid,
    				"node": arrSO[0].so,
    				"type": "so",
    				"locationid": arrSO[0].locationid,
    				"location": arrSO[0].location,
    				"sos_count": sos_count,
    				"serv_count": serv_count,
    				"sos_space": sos_space,
    				"inv_space": inv_space,
    				"sos_power": sos_power,
    				"inv_power": inv_power,
    				"sos_net": sos_net,
    				"inv_net": inv_net,
    				"sos_xcs": sos_xcs,
    				"inv_xcs": inv_xcs,
    				"sos_vxcs": sos_vxcs,
    				"inv_vxcs": inv_vxcs,
    				"sos_managed": sos_managed,
    				"inv_managed": inv_managed,
    				"faicon": "th-list",
    				"expanded": false,
    				"leaf": false,
    				"children": arrSO
    			});
    		}

    		// sums at customer level
    		var arr = _.map(sos, 'sos_count');
    		var sos_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		var arr = _.map(sos, 'serv_count');
    		var serv_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		
    		var arr = _.map(sos, 'sos_space');
    		var sos_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		var arr = _.map(sos, 'sos_power');
    		var sos_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		var arr = _.map(sos, 'sos_net');
    		var sos_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		var arr = _.map(sos, 'sos_xcs');
    		var sos_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		var arr = _.map(sos, 'sos_vxcs');
    		var sos_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		var arr = _.map(sos, 'sos_managed');
    		var sos_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    		
    		var treeSOs = {
    			"text": ".",
    			"children": [{
    				"nodeid": customerID,
    				"node": customerName,
    				"type": "customer",
    				"sos_count": sos_count,
    				"serv_count": serv_count,
    				"sos_power": sos_power,
    				"inv_power": powers.length,
    				"sos_space": sos_space,
    				"inv_space": spaces.length,
    				"sos_net": sos_net,
    				"inv_net": net.length,
    				"sos_xcs": sos_xcs,
    				"inv_xcs": xcs.length,
    				"sos_vxcs": sos_vxcs,
    				"inv_vxcs": vxcs.length,
    				"sos_managed": sos_managed,
    				"inv_managed": managed.length,
    				"faicon": "user-secret",
    				"expanded": false,
    				"leaf": false,
    				"children": sos
    			}]
    		};
    		
    		return treeSOs;
    	}
    }
    
    
    /**
     * Returns an inventory JSON file.
     * 
     * @access private
     * @function getInventory
     * 
     * @param {Object} paramObject
     * @param {Number} paramObject.fileID
     * @param {Number} paramObject.customerID
     * @param {Number} paramObject.locationID
     * 
     * @return {Array}
     */
    function getInventory(paramObject) {
    	if(paramObject) {
    		var fileContents = JSON.parse(bufferedFileLoad(paramObject.fileID));
        	var finalArray   = new Array();
        	
        	if(paramObject.locationID) {
        		finalArray = _.filter(fileContents, function(result) {
        			return (result.customerid == paramObject.customerID && result.locationid == paramObject.locationID);
        		});
        	} else {
        		finalArray = _.filter(fileContents, function(result) {
        			return (result.customerid == paramObject.customerID);
        		});
        	}
        	
        	return finalArray;
    	}
    }
    
    /**
     * Returns transaction data by customer.
     * 
     * @access private
     * @function getRows
     * 
     * @param {Object} paramObject
     * @param {Number} paramObject.customerID
     * @param {Number} paramObject.locationID
     * 
     * @returns {Array}
     */
    function getRows(paramObject) {
    	if(paramObject) {
    		var rowArray = new Array();
    		var filters  = new Array();
    		
    		filters.push(search.createFilter({ name: "mainname", operator: "anyof", values: paramObject.customerID }));
    		
    		if(paramObject.locationID > 0) {
    			filters.push(search.createFilter({ name: "location", operator: "anyof", values: paramObject.locationID }));
    		}
    		
    		var searchObject          = search.load({ type: "transaction", id: "customsearch_clgx_cmd_customer" });
    		searchObject.filters      = filters;
    		
    		var searchResultObject    = searchObject.runPaged({ pageSize: 1000 });
    		var searchResultPageCount = searchResultObject.pageRanges.length;
    		
    		for(var pageIndex = 0; pageIndex < searchResultPageCount; pageIndex++) {
    			var pageDataObject = searchResultObject.fetch({ index: pageIndex }).data;
    			var pageSize       = pageDataObject.length;
    			
    			for(var resultIndex = 0; resultIndex < pageSize; resultIndex++) {
    				var currentResultObject  = pageDataObject[resultIndex];
    				var currentResultColumns = currentResultObject.columns;
    				
    				var locationName     = formatFieldName({ text: currentResultObject.getText({ name: "location", summary: "group" }), delimiter: ":" });
    				var serviceName      = formatFieldName({ text: currentResultObject.getText({ name: "custcol_clgx_so_col_service_id", summary: "group" }), delimiter: ":" });
    				var serviceOrderName = formatFieldName({ text: currentResultObject.getValue({ name: "transactionname", summary: "group" }), delimiter: "#" });
    				
    				
    				rowArray.push({
    					"nodeid"     : parseInt(currentResultObject.getValue({ name: "custcol_clgx_so_col_service_id", summary: "group"})) || 0,
    					"node"       : serviceName,
    					"type"       : "service",
    					"locationid" : parseInt(currentResultObject.getValue({ name: "location", summary: "group" })) || 0,
    					"location"   : locationName,
    					"soid"       : parseInt(currentResultObject.getValue({ name: "internalid", summary: "group" })),
    					"so"         : serviceOrderName,
    					"serviceid"  : parseInt(currentResultObject.getValue({ name: "custcol_clgx_so_col_service_id", summary: "group" })) || 0,
    					"sos_count"  : 0,
    					"serv_count" : 1,
    					"sos_space"  : parseInt(currentResultObject.getValue(currentResultColumns[4])) || 0,
    					"inv_space"  : 0,
    					"sos_power"  : parseInt(currentResultObject.getValue(currentResultColumns[5])) || 0,
    					"inv_power"  : 0,
    					"sos_net"    : parseInt(currentResultObject.getValue(currentResultColumns[6])) || 0,
    					"inv_net"    : 0,
    					"sos_xcs"    : parseInt(currentResultObject.getValue(currentResultColumns[7])) || 0,
    					"inv_xcs"    : 0,
    					"sos_vxcs"   : parseInt(currentResultObject.getValue(currentResultColumns[8])) || 0,
    					"inv_vxcs"   : 0,
    					"sos_managed": parseInt(currentResultObject.getValue(currentResultColumns[9])) || 0,
    					"inv_managed": 0,
    					"cages"      : parseInt(currentResultObject.getValue(currentResultColumns[10])) || 0,
    					"faicon"     : "gears",
    					"leaf"       : false,
    					"children"   : []
    				});
    			}
    		}
    		
    		return rowArray;
    	}
    }
    
    return {
    	getRows: getRows,
    	getInventory: getInventory,
    	buildServiceOrderTree: buildServiceOrderTree,
    	buildInventoryTree: buildInventoryTree,
    	formatFieldName : formatFieldName,
    	bufferedFileLoad: bufferedFileLoad
    };
    
});
