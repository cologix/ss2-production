define(["N/file", "N/search", "/SuiteScripts/clgx/libraries/lodash.min"],
function(file, search, _) {
	/**
	 * Creates a file that bypasses the 10MB file limit.
	 * 
	 * @access public
	 * @function createBufferedFile
	 * 
	 * @param {Object}  paramObject
	 * @param {String}  paramObject.name
	 * @param {Integer} paramObject.folder
	 * @param {Array}   paramObject.data
	 * 
	 * @returns {Integer}
	 */
	function createBufferedFile(paramObject) {
		try {
			if(paramObject) {
				var fileObject = file.create({ name: paramObject.name, fileType: file.Type.JSON, folder: parseInt(paramObject.folder) });
				
				var stringArray = paramObject.data;
				
				if(paramObject.data.length > 7000) { 
					stringArray = createStringArrayFromObject(paramObject.data);
					
					var dataCount = stringArray.length;
			    	for(var dataIndex = 0; dataIndex < dataCount; dataIndex++) {
			    		fileObject.appendLine({ value: stringArray[dataIndex] });
			    	}
				} else {
					fileObject.appendLine({ value: stringArray[dataIndex] });
				}
		    	
		    	var fileID = fileObject.save();
		    	return fileID;
			}			
		} catch(ex) {
			log.error({ title: "createBufferedFile", details: ex });
		}
	}
	
	/**
	 * Creates a file that bypasses the 10MB file limit.
	 * 
	 * @access public
	 * @function createBufferedFile
	 * 
	 * @param {Object}  paramObject
	 * @param {String}  paramObject.name
	 * @param {Integer} paramObject.folder
	 * @param {Array}   paramObject.data
	 * 
	 * @returns {Integer}
	 */
	/*function createBufferedFile(paramObject) {
		try {
			if(paramObject) {
				var fileObject = file.create({ name: paramObject.name, fileType: file.Type.JSON, folder: parseInt(paramObject.folder) });
		    	
		    	var linePrefix = "";
		    	var lineSuffix = "";
		    	var dataCount  = paramObject.data.length;
		    	
		    	for(var dataIndex = 0; dataIndex < dataCount; dataIndex++) {
		    		if(dataIndex == 0) {
		    			linePrefix = "[";
		    			lineSuffix = ",";
		    		} else if(dataIndex == (dataCount - 1)) {
		    			lineSuffix = "]";
		    		} else {
		    			lineSuffix = ",";
		    		}
		    		
		    		fileObject.appendLine({ value: linePrefix + JSON.stringify(paramObject.data[dataIndex]) + lineSuffix });
		    		linePrefix = "";
			    	lineSuffix = "";
		    	}
		    	
		    	var fileID = fileObject.save();
		    	return fileID;
			}			
		} catch(ex) {
			log.error({ title: "createBufferedFile", details: ex });
		}
	}*/
	
	/**
	 * Returns an array of powers.
	 * 
	 *  @access public
	 *  @function getAllPowers
	 *  
	 *  @returns {Array}
	 */
	function getAllPowers() {
		try {
			var powersArray           = new Array();
    		var searchObject          = search.load({ id: "customsearch_clgx_cmd_powers" });
    		var searchResultObject    = searchObject.runPaged({ pageSize: 1000 });
    		var searchResultPageCount = searchResultObject.pageRanges.length;
    		
    		for(var pageIndex = 0; pageIndex < searchResultPageCount; pageIndex++) {
    			var pageDataObject = searchResultObject.fetch({ index: pageIndex }).data;
    			var pageSize       = pageDataObject.length;
    			
    			for(var resultIndex = 0; resultIndex < pageSize; resultIndex++) {
    				var currentResultObject = pageDataObject[resultIndex];
    				
    				var recordID            = parseInt(currentResultObject.getValue("internalid"));
	    			var locationName        = formatFieldName({ text: currentResultObject.getText({ name: "location", join: "CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER" }), delimiter: ":" });
	        		var serviceOrderNumber  = formatFieldName({ text: currentResultObject.getText("custrecord_power_circuit_service_order"), delimiter: "#" });
	        		var serviceName         = formatFieldName({ text: currentResultObject.getText("custrecord_cologix_power_service"), delimiter: ":" });
	
	        		powersArray.push({
	    				"nodeid"      : parseInt(currentResultObject.getValue("internalid")) || 0,
	    				"node"        : currentResultObject.getValue("name") || "",
	    				"type"        : "power",
	    				"categoryid"  : 0,
	    				"customerid"  : parseInt(currentResultObject.getValue({ name: "mainname", join: "CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER" })) || 0,
	    				"customer"    : currentResultObject.getText({ name: "mainname", join: "CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER" }) || "",
	    				"locationid"  : parseInt(currentResultObject.getValue({ name: "location", join: "CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER" })) || 0,
	    				"location"    : locationName,
	    				"facilityid"  : parseInt(currentResultObject.getValue("custrecord_cologix_power_facility")) || 0,
	    				"facility"    : currentResultObject.getText("custrecord_cologix_power_facility") || "",
	    				"spaceid"     : parseInt(currentResultObject.getValue("custrecord_cologix_power_space")) || 0,
	    				"space"       : currentResultObject.getText("custrecord_cologix_power_space") || "",
	    				"soid"        : parseInt(currentResultObject.getValue("custrecord_power_circuit_service_order")) || 0,
	        			"so"          : serviceOrderNumber,
	        			"serviceid"   : parseInt(currentResultObject.getValue("custrecord_cologix_power_service")) || 0,
	        			"service"     : serviceName,
	        			"genid"       : parseInt(currentResultObject.getValue("custrecord_clgx_power_generator")) || 0,
	    				"gen"         : currentResultObject.getText("custrecord_clgx_power_generator") || "",
	    				"gen_name"    : currentResultObject.getValue({ name: "altname", join: "custrecord_clgx_power_generator" }) || "",
	    				"gen_dev_id"  : parseInt(currentResultObject.getValue({ name: "custrecord_clgx_od_fam_device", join: "custrecord_clgx_power_generator" })) || 0,
	    				"gen_dev"     : currentResultObject.getText({ name: "custrecord_clgx_od_fam_device", join: "custrecord_clgx_power_generator" }) || "",
	    				"upsid"       : parseInt(currentResultObject.getValue("custrecord_cologix_power_ups_rect")) || 0,
	    				"ups"         : currentResultObject.getText("custrecord_cologix_power_ups_rect") || "",
	    				"ups_name"    : currentResultObject.getValue({ name: "altname", join: "custrecord_cologix_power_ups_rect" }) || "",
	    				"ups_dev_id"  : parseInt(currentResultObject.getValue({ name: "custrecord_clgx_od_fam_device", join: "custrecord_cologix_power_ups_rect" })) || 0,
	    				"ups_dev"     : currentResultObject.getText({ name: "custrecord_clgx_od_fam_device", join: "custrecord_cologix_power_ups_rect" }) || "",
	    				"panelid"     : parseInt(currentResultObject.getValue("custrecord_clgx_power_panel_pdpm")) || 0,
	    				"panel"       : currentResultObject.getText("custrecord_clgx_power_panel_pdpm") || "",
	    				"panel_name"  : currentResultObject.getValue({ name: "altname", join: "custrecord_clgx_power_panel_pdpm" }) || "",
	    				"panel_dev_id": parseInt(currentResultObject.getValue("custrecord_clgx_dcim_device")) || 0,
	    				"panel_dev"   : currentResultObject.getText("custrecord_clgx_dcim_device") || "",
	    				"itemid"      : 0,
	    				"item"        : "",
	        			"faicon"      : "plug",
	        			"leaf"        : true
	        		});
    			}
    		}
    		
    		return powersArray;
		} catch(ex) {
			log.error({ title: "getAllPowers - Error", details: ex });
		}
	}
	
	/**
	 * Returns an array of services.
	 * 
	 * @access public
	 * @function getAllServices
	 * 
	 * @returns {Array}
	 */
	function getAllServices() {
		try {
			var serviceArray          = new Array();
    		var searchObject          = search.load({ id: "customsearch_clgx_cmd_powers_jobs" });
    		var searchResultObject    = searchObject.runPaged({ pageSize: 1000 });
    		var searchResultPageCount = searchResultObject.pageRanges.length;
    		
    		for(var pageIndex = 0; pageIndex < searchResultPageCount; pageIndex++) {
    			var pageDataObject = searchResultObject.fetch({ index: pageIndex }).data;
    			var pageSize       = pageDataObject.length;
    			
    			for(var resultIndex = 0; resultIndex < pageSize; resultIndex++) {
    				var currentResultObject = pageDataObject[resultIndex];
    				
    				var serviceID           = parseInt(currentResultObject.getValue("custcol_clgx_so_col_service_id"));
	    			var locationName        = formatFieldName({ text: currentResultObject.getText({ name: "location", join: "CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER" }), delimiter: ":" });
	        		var serviceOrderNumber  = formatFieldName({ text: currentResultObject.getText("custrecord_power_circuit_service_order"), delimiter: "#" });
	        		var serviceName         = formatFieldName({ text: currentResultObject.getText("custrecord_cologix_power_service"), delimiter: ":" });
	        		
	        		serviceArray.push({
	    				"serviceid" : parseInt(currentResultObject.getValue({ name: "custcol_clgx_so_col_service_id", summary: "GROUP" })) || 0,
	    				"soid"      : parseInt(currentResultObject.getValue({ name: "internalid", summary: "GROUP" })) || 0,
	    				"categoryid": parseInt(currentResultObject.getValue({ name: "custcol_cologix_invoice_item_category", summary: "GROUP" })) || 0,
	    				"itemid"    : parseInt(currentResultObject.getValue({ name: "item", summary: "GROUP" })) || 0,
	    				"item"      : currentResultObject.getText({ name: "item", summary: "GROUP" }) || ""
	        		});
    			}
    		}
    		
    		return serviceArray;
		} catch(ex) {
			log.error({ title: "getAllServices - Error", details: ex });
		}
	}
	
	function getAllOrphans() {
		try {
			var orphanArray           = new Array();
			var searchObject          = search.load({ id: "customsearch_clgx_cmd_powers_orphans" });
    		var searchResultObject    = searchObject.runPaged({ pageSize: 1000 });
    		var searchResultPageCount = searchResultObject.pageRanges.length;
    		
    		for(var pageIndex = 0; pageIndex < searchResultPageCount; pageIndex++) {
    			var pageDataObject = searchResultObject.fetch({ index: pageIndex }).data;
    			var pageSize       = pageDataObject.length;
    			
    			for(var resultIndex = 0; resultIndex < pageSize; resultIndex++) {
    				var currentResultObject = pageDataObject[resultIndex];
	        		var serviceOrderNumber  = formatFieldName({ text: currentResultObject.getText("custrecord_power_circuit_service_order"), delimiter: "#" });
	        		var serviceName         = formatFieldName({ text: currentResultObject.getText("custrecord_cologix_power_service"), delimiter: ":" });

		    		orphanArray.push({
		    			"nodeid"    : parseInt(currentResultObject.getValue("internalid")) || 0,
		    			"node"      : currentResultObject.getValue("name") || '',
		    			"type"      : "power",
		    			"facilityid": parseInt(currentResultObject.getValue("custrecord_cologix_power_facility")) || 0,
		    			"facility"  : currentResultObject.getText("custrecord_cologix_power_facility") || '',
		    			"soid"      : parseInt(currentResultObject.getValue("custrecord_power_circuit_service_order")) || 0,
		    			"so"        : serviceOrderNumber,
		    			"serviceid" : parseInt(currentResultObject.getValue("custrecord_cologix_power_service")) || 0,
		    			"service"   : serviceName,
		    			"faicon"    : "plug",
		    			"leaf"      : true
		    		});
    			}
    		}
	    	
	    	return orphanArray;
		} catch(ex) {
			log.error({ title: "getAllOrphans - Error", details: ex });
		}
	}
	
	
	/**
	 * Merges services and powers.
	 * 
	 * @access public
	 * @function mergeServicesAndPowers
	 * 
	 * @param {Object} paramObject
	 * @param {Array} paramObject.powers
	 * @param {Array} paramObject.services
	 * 
	 * @return {Object}
	 */
	function mergeServicesAndPowers(paramObject) {
		if(paramObject) {
			var powers   = paramObject.powers;
    		var services = paramObject.services;
    		
    		for ( var i = 0; i < powers.length; i++ ) {
	    		var service = _.find(services, function(arr){ return (arr.soid == powers[i].soid && arr.serviceid == powers[i].serviceid) ; });
	    		if(service){
	    			powers[i].categoryid = service.categoryid;
	    			powers[i].itemid = service.itemid;
	    			powers[i].item = service.item;
	    		}
	    	}
    		
    		return powers;
		}
	}
	
	/**
     * 
     * @access private
     * @function formatFieldName
     * 
     * @param {Object} paramObject
     * @param {String} paramObject.text
     * @param {String} paramObject.delimiter
     * 
     * @returns {String}
     */
    function formatFieldName(paramObject) {
    	if(paramObject) {
    		var tmpRawString = paramObject.text;
    		
    		if(!tmpRawString) { return ""; }
    		
    		tmpRawString = tmpRawString.split(paramObject.delimiter);
    		tmpRawString = tmpRawString[tmpRawString.length - 1].trim();
    		
    		return tmpRawString; 
    	}
    }
    
    function buildPowerTopology(powers) {
    	try {
    		var facids = _.uniq(_.map(powers, 'facilityid'));
        	var Facilities = [];
        	for ( var i = 0; i < facids.length; i++ ) {
        		
        		var generators = _.filter(powers, function(arr){
        			return (arr.facilityid == facids[i]);
        		});
        		var genids = _.uniq(_.map(generators, 'genid'));
        		var Generators = [];
        		for ( var j = 0; j < genids.length; j++ ) {
        			
            		var ups = _.filter(generators, function(arr){
        				return (arr.genid == genids[j]);
        			});
        			var upsids = _.uniq(_.map(ups, 'upsid'));
            		var Ups = [];
            		for ( var k = 0; k < upsids.length; k++ ) {
            			
                		var panels = _.filter(ups, function(arr){
                			return (arr.upsid == upsids[k]);
                		});
                		var pnlids = _.uniq(_.map(panels, 'panelid'));
                		var Panels = [];
                    	for ( var l = 0; l < pnlids.length; l++ ) {
                			
                    		var Powers = _.filter(panels, function(arr){
                    			return (arr.panelid == pnlids[l]);
                    		});
                			Panels.push({
                    			"node": Powers[0].panel + ' ' + Powers[0].panel_name,
                    			"nodeid": Powers[0].panelid,
                    			"type": 'panel',
                    			"deviceid": Powers[0].panel_dev_id,
                    			"device": Powers[0].panel_dev,
                    			"checked": false,
                    			"faicon": "bolt",
                    			"expanded": false,
                    			"leaf": false,
                    			"children": Powers
                    		});
                			Panels = _.sortBy(Panels, function(obj){ return obj.node;});
                		}
                    	Ups.push({
                			"node": panels[0].ups + ' ' + panels[0].ups_name,
                			"nodeid": panels[0].upsid,
                			"type": 'ups',
                			"deviceid": panels[0].ups_dev_id,
                			"device": panels[0].ups_dev,
                			"checked": false,
                			"faicon": "bolt",
                			"expanded": false,
                			"leaf": false,
                			"children": Panels
                		});
                    	Ups = _.sortBy(Ups, function(obj){ return obj.node;});
            		}
            		Generators.push({
            			"node": ups[0].gen + ' ' + ups[0].gen_name,
            			"nodeid": ups[0].genid,
            			"type": 'generator',
            			"deviceid": ups[0].gen_dev_id,
            			"device": ups[0].gen_dev,
            			"checked": false,
            			"faicon": "bolt",
            			"expanded": true,
            			"leaf": false,
            			"children": Ups
            		});
            		Generators = _.sortBy(Generators, function(obj){ return obj.node;});
            	}
            	Facilities.push({
        			"node": generators[0].facility,
        			"nodeid": generators[0].facilityid,
        			"type": 'facility',
        			"checked": false,
        			"faicon": "building",
        			"expanded": true,
        			"leaf": false,
        			"children": Generators
        		});
        	}

        	var tree = {
        			"text": ".",
        			"inventory": 'Powers Notifications',
        			"type": 'notifications',
        			"children": Facilities
        	};
        	
        	return tree;
    	} catch(ex) {
    		log.error({ title: "buildPowerTopology - Error", details: ex });
    	}
    }
    
    function buildGridTree(paramObject) {
    	var inventory = paramObject.inventory;
    	var type      = paramObject.type;
    	var grid      = paramObject.grid;
    	
    	if(inventory == 'Managed'){
    		
    		var ids = _.uniq(_.map(grid, 'customerid'));
    		var customers = [];
    		for ( var i = 0; i < ids.length; i++ ) {
    			var children = _.filter(grid, function(arr){
    				return (arr.customerid == ids[i]);
    			});
    			if(children){
    				
    				var ids2 = _.uniq(_.map(children, 'typeid'));
    				var types = [];
    				for ( var j = 0; j < ids2.length; j++ ) {
    					var children2 = _.filter(children, function(arr){
    						return (arr.typeid == ids2[j]);
    					});
    					types.push({
    						"nodeid": children2[0].typeid,
    						"node": children2[0].type,
    						"customerid": children[0].customerid,
    						"customer": children[0].customer,
    						"type": "type",
    						"count": children2.length,
    						"faicon": "gears",
    						"expanded": false,
    						"leaf": false,
    						"children": children2
    					});
    				}
    				customers.push({
    					"nodeid": children[0].customerid,
    					"node": children[0].customer,
    					"customerid": children[0].customerid,
    					"customer": children[0].customer,
    					"type": "customer",
    					"count": children.length,
    					"faicon": "user",
    					"expanded": false,
    					"leaf": false,
    					"children": types
    				});
    			}
    		}
    		
    	} else {
    		
    		var ids = _.uniq(_.map(grid, 'customerid'));
    		var customers = [];
    		for ( var i = 0; i < ids.length; i++ ) {
    			var children = _.filter(grid, function(arr){
    				return (arr.customerid == ids[i]);
    			});
    			if(children){
    				customers.push({
    					"nodeid": children[0].customerid,
    					"node": children[0].customer,
    					"customerid": children[0].customerid,
    					"customer": children[0].customer,
    					"type": "customer",
    					"count": children.length,
    					"faicon": "user",
    					"expanded": false,
    					"leaf": false,
    					"children": children
    				});
    			}
    		}
    	}

    	customers = _.sortBy(customers, function(obj){ return obj.node;});

    	var tree = {
    			"text": ".",
    			"inventory": inventory,
    			"type": type,
    			"count": grid.length,
    			"children": customers
    	};
    	return tree;
    }
    
    /**
	 * Creates an array of strings to be saved in a file.
	 * 
	 * @access private
	 * @function createJSONStringFromObject
	 * 
	 * @param {Object} paramObject
	 * @param {String} paramObject.dataString
	 * @param {Number} paramObject.chunkSize
	 * @returns {Array}
	 */
	function createStringArrayFromObject(dataString) {
		//if(dataString) { return dataString.match(/G.{1,7000}/g); }
		if(dataString) { return dataString.split(/(?=},{)/g); }
	}
    
    return {
    	buildPowerTopology         : buildPowerTopology,
    	buildGridTree              : buildGridTree,
    	getAllPowers               : getAllPowers,
    	getAllServices             : getAllServices,
    	getAllOrphans              : getAllOrphans,
    	mergeServicesAndPowers     : mergeServicesAndPowers,
    	createBufferedFile         : createBufferedFile
    };
    
});
