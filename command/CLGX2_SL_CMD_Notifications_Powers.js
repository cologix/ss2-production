/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/search", "/SuiteScripts/clgx/libraries/lodash.min", "/SuiteScripts/clgx/command/lib/CLGX2_LIB_CMD", "N/file"],
function(search, _, lib, file) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	try {
    		var facilityID = context.request.parameters.facilityid || 0;
    		var customerID = search.lookupFields({ type: "customrecord_cologix_facility", id: facilityID, columns: ["custrecord_cologix_location_subsidiary"] }).custrecord_cologix_location_subsidiary[0].value || 0;
    		
    		var fileContents = lib.bufferedFileLoad(5094050);
    		var tree         = JSON.parse(fileContents);
    		
    		var facility = _.find(tree.children, function(arr){ return (arr.nodeid == facilityID); });
    		
    		if(facility){
            	var tree = {
            			"text": '.',
            			"facility": facility.node,
            			"facilityid": facility.nodeid,
            			"customerid": parseInt(customerID),
            			"children": facility.children
            	};
        	} else {
            	var tree = {
            			"text": '.',
            			"facility": '',
            			"facilityid": 0,
            			"customerid": parseInt(customerID),
            			"children": []
            	};
        	}
    		
    		var fileObject = file.load({ id: 5093537 });
    		var html = fileObject.getContents();
    		html     = html.replace(new RegExp('{tree}','g'), JSON.stringify(tree));
    		context.response.write(html);
    	} catch(ex) {
    		log.error({ title: "onRequest - Error", details: ex });
    	}
    }

    return {
        onRequest: onRequest
    };
    
});
