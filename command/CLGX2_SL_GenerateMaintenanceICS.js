/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["/SuiteScripts/clgx/libraries/cryptojs/crypto-js", "N/file", "N/runtime", "/SuiteScripts/clgx/libraries/moment.min"],
function(cryptojs, file, runtime, moment) {
    function onRequest(context) {
    	var stack = {};
    	
    	try {
    		if(context.request.method == "GET") {
    			stack.htmlFileID    = parseInt(runtime.getCurrentScript().getParameter("custscript_clgx2_gmics_html_file")) || 0;
    			stack.encryptionKey = runtime.getCurrentScript().getParameter("custscript_clgx2_gmics_encryption_key").toString() || "";
    			stack.encryptedData = decodeURIComponent(context.request.parameters.event) || "";
    			
    			if(stack.encryptedData && stack.encryptionKey) {
    				stack.decryptedData = cryptojs.AES.decrypt(stack.encryptedData, stack.encryptionKey).toString(cryptojs.enc.Utf8);
    				
    				if(isJSON(stack.decryptedData)) {
    					stack.decryptedDataObject = JSON.parse(stack.decryptedData);
    					
    					stack.fileObject   = file.load({ id: stack.htmlFileID });
    					stack.fileContents = stack.fileObject.getContents();
    					
    					stack.fileContents = stack.fileContents.replace(new RegExp("{eventTitle}", "g"), stack.decryptedDataObject.title);
    					stack.fileContents = stack.fileContents.replace(new RegExp("{eventDetails}", "g"), buildEventScript(stack.decryptedDataObject));
    					context.response.write(stack.fileContents);
    				}
    			}
    		}
    	} catch(ex) {
    		log.error({ title: "onRequest - Error", details: ex });
    	}
    }
    
    
    /**
     * Returns the code to download the calendar file.
     * 
     * @access private
     * @function buildEventScript
     * 
     * @param {Object} eventObject
     * @returns {String}
     */
    function buildEventScript(eventObject) {
    	if(eventObject) {
    		var eventScriptString = "";
    		
    		
    		var fileName = (eventObject.title.toLowerCase().replace(new RegExp(" ", "g"), "-") + "-" + eventObject.location.toLowerCase() + "-" + eventObject.begin);
    		
    		eventScriptString += "var event = ics();";
    		eventScriptString += "event.addEvent(\"" + eventObject.title + "\", \"" + eventObject.details + "\", \"" + eventObject.location + "\", \"" + new moment(eventObject.begin).format("M/D/YYYY HH:mm a") + "\", \"" + new moment(eventObject.end).format("M/D/YYYY HH:mm a") + "\");";
    		eventScriptString += "window.onready = event.download(\"" + fileName + "\");";
    		
    		return eventScriptString;
    	}
    }
    
    
    /**
     * Checks whether or not the string is JSON.
     * 
     * @access private
     * @function isJSON
     * 
     * @param {String} string
     * @returns {Boolean}
     */
    function isJSON(string) {
    	try {
    		JSON.parse(string);
    	} catch(ex) {
    		return false;
    	}
    	
    	return true;
    }

    return {
        onRequest: onRequest
    };
    
});
