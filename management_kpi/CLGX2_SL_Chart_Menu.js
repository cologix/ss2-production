/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
require.config({
	paths: { 
		"underscore" : "/SuiteScripts/clgx/libraries/underscore-min",
		"cologix"    : "/SuiteScripts/clgx/libraries/CLGX2_Lib_Global"
	}
});

define(["N/runtime", "N/search", "N/file", "underscore", "cologix"],
function(runtime, search, file, _, clgx) {
	/**
	 * 
	 */
	function createActivitiesMenu(admin, type, locationID, userID) {
		var exclude      = [294, 279321, 480345, 71418, 115655, 2347, 4274, 6857, 213255, 323586, 339721, 501436, 387984, 394031, 390070, 3118, 530873, 1127523, 600791, 1251296];
		var locationName = clgx.getMarketName(locationID);
		var stMenu       = "var dataActivities = {\"text\":\".\",\"children\": [\n";
		
		
		if(type === "all") {
			stMenu += "{report:\"All Markets\", market:\"All\", marketid:0, userid:0, script: 208, period:\"month\", periodid:0, expanded:true, iconCls:\"calendar\", leaf:false, children:[\n" +
            	"{report:\"This week\", market:\"All\", marketid:0, userid:0, script: 208, period:\"week\", periodid:0, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"Last Week\", market:\"All\", marketid:0, userid:0, script: 208, period:\"week\", periodid:1, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"2 Weeks Ago\", market:\"All\", marketid:0, userid:0, script: 208, period:\"week\", periodid:2, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"3 Weeks Ago\", market:\"All\", marketid:0, userid:0, script: 208, period:\"week\", periodid:3, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"4 Weeks Ago\", market:\"All\", marketid:0, userid:0, script: 208, period:\"week\", periodid:4, iconCls:\"chart_bar\", leaf:true}\n" +
            	"]},\n";
			
			var marketSearchObj = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx_chrt_act_sale_reps" });
			
			var filters = new Array();
			if(admin === "yes") {
				filters.push(search.createFilter({ name: "location", operator: search.Operator.NONEOF, values: [1] }));
			}
			
			filters.push(search.createFilter({ name: "location", operator: search.Operator.NONEOF, values: [1, 14] }));
			filters.push(search.createFilter({ name: "internalid", operator: search.Operator.NONEOF, values: exclude }));
			
			marketSearchObj.filters = filters;
			marketSearchObj.columns = [
				search.createColumn({ name: "location", summary: search.Summary.GROUP })
			];
			
			marketSearchObj.run().each(function(result) {
				var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					
					stMenu += "\n{report:'" + marketName + "', market:'" + marketName + "', marketid:'" + marketID + "', userid:0, script: 208, period:\"month\", periodid:-1, expanded:false, iconCls:\"group\", leaf:false, children:[\n" +
                    	"{report:\"Current Month\", market:'" + marketName + "', marketid:'" + marketID + "', userid:0, script: 208, period:\"month\", periodid:0, expanded:false, iconCls:\"calendar\", leaf:false, children:[\n" +
                    	"{report:\"This week\", market:'" + marketName + "', marketid:'" + marketID + "', userid:0, script: 208, period:\"week\", periodid:0, iconCls:\"chart_bar\", leaf:true},\n" +
                    	"{report:\"Last Week\", market:'" + marketName + "', marketid:'" + marketID + "', userid:0, script: 208, period:\"week\", periodid:1, iconCls:\"chart_bar\", leaf:true},\n" +
                    	"{report:\"2 Weeks Ago\", market:'" + marketName + "', marketid:'" + marketID + "', userid:0, script: 208, period:\"week\", periodid:2, iconCls:\"chart_bar\", leaf:true},\n" +
                    	"{report:\"3 Weeks Ago\", market:'" + marketName + "', marketid:'" + marketID + "', userid:0, script: 208, period:\"week\", periodid:3, iconCls:\"chart_bar\", leaf:true},\n" +
                    	"{report:\"4 Weeks Ago\", market:'" + marketName + "', marketid:'" + marketID + "', userid:0, script: 208, period:\"week\", periodid:4, iconCls:\"chart_bar\", leaf:true}\n" +
                    	"]},\n";
					
					var salesRepSearchObj = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx_chrt_act_sale_reps" });

					
					salesRepSearchObj.filters = [
						search.createFilter({ name: "location", operator: search.Operator.NONEOF, values: [1] }),
						search.createFilter({ name: "location", operator: search.Operator.NONEOF, values: [1, 14] }),
						search.createFilter({ name: "internalid", operator: search.Operator.NONEOF, values: exclude })
					];
					
					salesRepSearchObj.columns = [
						search.createColumn({ name: "location" }),
						search.createColumn({ name: "internalid" }),
						search.createColumn({ name: "firstname" }),
						search.createColumn({ name: "lastname" })
					];
					
					salesRepSearchObj.run().each(function(result) {
						var userID        = result.getValue("internalid");
		            	var userFirstName = result.getValue("firstname");
		            	var userLastName  = result.getValue("lastname");
		            	
		            	stMenu += "\n{report:'" + userFirstName + " " + userLastName + "', market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"month\", periodid:0, expanded:false, iconCls:\"user\", leaf:false, children:[\n" +
		            		"{report:\"This week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:0, iconCls:\"chart_bar\", leaf:true},\n" +
		            		"{report:\"Last Week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:1, iconCls:\"chart_bar\", leaf:true},\n" +
		            		"{report:\"2 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:2, iconCls:\"chart_bar\", leaf:true},\n" +
		            		"{report:\"3 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:3, iconCls:\"chart_bar\", leaf:true},\n" +
		            		"{report:\"4 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:4, iconCls:\"chart_bar\", leaf:true}\n" +
		            		"]},";
		            	
		            	return true;
					});
					
					var strLen = stMenu.length;
	                stMenu = stMenu.slice(0, strLen - 1);
	                stMenu += "]},";
				}
				
				var strLen = stMenu.length;
                stMenu = stMenu.slice(0, strLen - 1);
                stMenu += "]},";
                
                return true;
			});
			
			var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]};\n";
            
		} else if(type === "team") {
			stMenu += "\n{report:\"Current Month\", market:'" + locationName + "', marketid:'" + locationID + "', userid:0, script: 208, period:\"month\", periodid:0, expanded:true, iconCls:\"calendar\", leaf:false, children:[\n" +
            	"{report:\"This week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:0, script: 208, period:\"week\", periodid:0, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"Last Week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:0, script: 208, period:\"week\", periodid:1, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"2 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:0, script: 208, period:\"week\", periodid:2, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"3 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:0, script: 208, period:\"week\", periodid:3, iconCls:\"chart_bar\", leaf:true},\n" +
            	"{report:\"4 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:0, script: 208, period:\"week\", periodid:4, iconCls:\"chart_bar\", leaf:true}\n" +
            	"]},\n";
			
			var salesRepSearchObj     = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx_chrt_act_sale_reps" });
			salesRepSearchObj.columns = [
            	search.createColumn({ name: "internalid" }),
            	search.createColumn({ name: "firstname" }),
            	search.createColumn({ name: "lastname" })
            ];
            
			salesRepSearchObj.filters = [
            	search.createFilter({ name: "location", operator: search.Operator.ANYOF, values: [parseInt(locationID)] }),
            	search.createFilter({ name: "isinactive", operator: search.Operator.IS, values: ["F"] }),
            	search.createFilter({ name: "internalid", operator: search.Operator.NONEOF, values: [exclude] })
            ];
            
			salesRepSearchObj.run().each(function(result) {
            	var userID        = result.getValue("internalid");
            	var userFirstName = result.getValue("firstname");
            	var userLastName  = result.getValue("lastname");
            	
            	stMenu += "\n{report:'" + userFirstName + " " + userLastName + "', market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"month\", periodid:0, expanded:false, iconCls:\"user\", leaf:false, children:[\n" +
            		"{report:\"This week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:0, iconCls:\"chart_bar\", leaf:true},\n" +
            		"{report:\"Last Week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:1, iconCls:\"chart_bar\", leaf:true},\n" +
            		"{report:\"2 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:2, iconCls:\"chart_bar\", leaf:true},\n" +
            		"{report:\"3 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:3, iconCls:\"chart_bar\", leaf:true},\n" +
            		"{report:\"4 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:4, iconCls:\"chart_bar\", leaf:true}\n" +
            		"]},";
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += ']};\n';
		} else {
            stMenu = "var dataActivities =  {\"text\":\".\",\"children\": [";
            stMenu += "\n{report:\"Activity (Current Month)\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"month\", periodid:0, expanded:true, iconCls:\"calendar\", leaf:false, children:[\n" +
                "{report:\"This week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:0, iconCls:\"chart_bar\", leaf:true},\n" +
                "{report:\"Last Week\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:1, iconCls:\"chart_bar\", leaf:true},\n" +
                "{report:\"2 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:2, iconCls:\"chart_bar\", leaf:true},\n" +
                "{report:\"3 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:3, iconCls:\"chart_bar\", leaf:true},\n" +
                "{report:\"4 Weeks Ago\", market:'" + locationName + "', marketid:'" + locationID + "', userid:'" + userID + "', script: 209, period:\"week\", periodid:4, iconCls:\"chart_bar\", leaf:true}\n" +
                "]}\n" +
                "]};\n";
		}
		
		return stMenu;
	}
	
	
	/**
	 * 
	 */
	function createSalesMenu(type, departmentID, locationID, userID) {
		var stMenu = "";
		
		if(type === "all") {
			stMenu = "var dataSales =  {\"text\":\".\",\"children\": [";
			
			var marketSearchObj = search.load({ 
				type: search.Type.EMPLOYEE, 
				id: "customsearch_clgx_chrt_act_sale_reps"
			});
			
			marketSearchObj.columns = [search.createColumn({ name: "location", summary: search.Summary.GROUP })];
			marketSearchObj.filters = [search.createFilter({ name: "location", operator: search.Operator.NONEOF, values: [1] })];
			
			var marketSearchResult = marketSearchObj.run();
			
			//Gross Sales Tree Menu
			stMenu += "\n{\"report\":\"Gross Sales\", \"departmentid\":0, \"market\": \"All\", \"marketid\":0, \"script\": 716, \"iconCls\":\"chart_bar\", \"expanded\":true, \"leaf\":false, \"children\":[\n";
			marketSearchResult.each(function(result) {
				var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					stMenu += "{\"report\":'" + marketName + "', \"departmentid\":0, \"market\":'" + marketName + "', \"marketid\":'" + marketID + "', \"script\": 716, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
				}
				
				return true;
			});
			
			var strLen = stMenu.length;
            stMenu     = stMenu.slice(0, strLen - 1);
            stMenu    += "]},\n";
            
            
            //Unadjusted Gross Sales Tree Menu
            stMenu += "\n{\"report\":\"Net Sales\", \"departmentid\":0, \"market\": \"All\", \"marketid\":0, \"script\": 211, \"iconCls\":\"chart_bar\", \"expanded\":false, \"leaf\":false, \"children\":[\n";
            marketSearchResult.each(function(result) {
            	var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					stMenu += "{\"report\":'" + marketName + "', \"departmentid\":0, \"market\":'" + marketName + "', \"marketid\":'" + marketID + "', \"script\": 211, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
				}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu     = stMenu.slice(0, strLen - 1);
            stMenu    += "]},\n";
            
            
            //Sales Funnel Quarterly Tree Menu
            stMenu += "\n{\"report\":\"Sales Funnel Quarterly\", \"region\":\"\", \"market\": \"All\", \"marketid\":0, \"script\": 215, \"iconCls\":\"chart_bar\", \"expanded\":false, \"leaf\":false, \"children\":[\n";
            
            marketSearchResult.each(function(result) {
            	var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					stMenu += "{\"report\":'" + marketName + "', \"departmentid\":0, \"market\":'" + marketName + "', \"marketid\":'" + marketID + "', \"script\": 215, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
				}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu     = stMenu.slice(0, strLen - 1);
            stMenu    += "]},\n";
            
            
            //Departments Gross Sales Tree Menu
            var objReport = new Object();
            objReport["report"]       = "Departments Gross Sales";
            objReport["region"]       = "Departments Gross Sales";
            objReport["market"]       = "All";
            objReport["marketid"]     = 0;
            objReport["department"]   = "Departments";
            objReport["departmentid"] = 9;
            objReport["script"]       = 211;
            objReport["iconCls"]      = "chart_bar";
            objReport["expanded"]     = false;
            objReport["leaf"]         = false;
            objReport["children"]     = getMenuDepartments(type, 211); //Build tree menu function - script 211
            
            stMenu += JSON.stringify(objReport);
            stMenu += ",";

            //Departments Funnel Quarterly Tree Menu
            var objReport = new Object();
            objReport["report"]       = "Departments Funnel Quarterly";
            objReport["region"]       = "Departments Funnel Quarterly";
            objReport["market"]       = "All";
            objReport["marketid"]     = 0;
            objReport["department"]   = "Departments";
            objReport["departmentid"] = 9;
            objReport["script"]       = 215;
            objReport["iconCls"]      = "chart_bar";
            objReport["expanded"]     = false;
            objReport["leaf"]         = false;
            objReport["children"]     = getMenuDepartments(type, 215); // build tree menu function - script 211
            
            stMenu += JSON.stringify(objReport);
            
            
			stMenu += "]};\n";
		} else if(type === "team") {
			stMenu = "var dataSales =  {\"text\":\".\",\"children\": [";
			
			if(userID == 206211 || userID == 2406 || userID == 211645 || userID == -5 || userID == 63) {
				stMenu += "{\"report\":\"Gross Sales\", \"departmentid\":0, \"market\":'" + clgx.getMarketName(locationID) + "', \"marketid\":\'" + locationID + "', \"script\": 716, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
			}
			
			stMenu += "{\"report\":\"Net Sales\", \"departmentid\":0, \"market\":'" + clgx.getMarketName(locationID) + "', \"marketid\":'" + locationID + "', \"script\": 211, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
            stMenu += "{\"report\":\"Sales Funnel Quarterly\", \"departmentid\":0, \"market\":'" + clgx.getMarketName(locationID) + "', \"marketid\":'" + locationID + "', \"script\": 215, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
            stMenu += "{\"report\":\"Department Gross Sales\", \"departmentid\":'" + departmentID + "', \"market\":\"\", \"marketid\":0, \"script\": 211, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
            stMenu += "{\"report\":\"Department Funnel Quarterly\", \"departmentid\":'" + departmentID + "', \"market\":\"\", \"marketid\":0, \"script\": 215, \"iconCls\":\"chart_bar\", \"leaf\":true},\n";
			
			stMenu += "]};\n";
		} else {
			stMenu = "var dataSales =  {\"text\":\".\",\"children\": []};";
		}
		
		return stMenu;
	}
	
	
	/**
	 * 
	 */
	function getMenuDepartments(type, script, userID) {
		var employees         = new Array();
		var employeeSearchObj = search.load({ 
			type: search.Type.EMPLOYEE,
			id: "customsearch_clgx_chrt_act_sale_reps"
		});
		
		employeeSearchObj.columns = [search.createColumn({ name: "internalid" })];
		
		employeeSearchObj.run().each(function(result) {
			employees.push(result.getValue("internalid"));
			
			return true;
		});
		
		
		var departments = new Array();
		var departmentSearchObj = search.load({
			type: search.Type.EMPLOYEE,
			id: "customsearch_clgx_kpi_sales_reps_departm"
		});
		
		departmentSearchObj.filters = [search.createFilter({ name: "internalid", operator: search.Operator.ANYOF, values: [employees] })]
		
		departmentSearchObj.run().each(function(result) {
			var columns                      = departmentSearchObj.columns;
			var tmpDepartmentObj             = new Object();
			tmpDepartmentObj["region"]       = result.getValue(columns[0]);
			tmpDepartmentObj["department"]   = result.getValue({ name: "departmentnohierarchy", summary: search.Summary.GROUP });
			tmpDepartmentObj["departmentid"] = parseInt(result.getValue({ name: "department", summary: search.Summary.GROUP }));
			departments.push(tmpDepartmentObj);
			
			return true;
		});
		
		
		var regions      = [{"region":"Canada","regionid": 23},{"region":"Cross Market","regionid": 24},{"region":"US North","regionid": 25},{"region":"US South","regionid": 26}];
		var regionLength = regions.length;
		var finalRegions = new Array();
		
		for(var r = 0; regions != null && r < regionLength; r++) {
			var tmpRegion = new Object();
			
			tmpRegion["report"]       = regions[r].region;
			tmpRegion["region"]       = regions[r].region;
			tmpRegion["regionid"]     = regions[r].regionid;
			tmpRegion["market"]       = "";
			tmpRegion["marketid"]     = 0;
			tmpRegion["department"]   = regions[r].region;
			tmpRegion["departmentid"] = regions[r].regionid;
			tmpRegion["script"]       = script;
			tmpRegion["iconCls"]      = "chart_bar";
			tmpRegion["expanded"]     = false;
			tmpRegion["leaf"]         = false;
			
			var regionDepartments = _.filter(departments, function(array) {
				return (array.region == regions[r].region);
			});
			
			var regionDepartmentLength = regionDepartments.length;
			var finalRegionDepartments = new Array();
			for(var rd = 0; regionDepartments != null && rd < regionDepartmentLength; rd++) {
				var tmpRegionDepartment             = new Object();
				tmpRegionDepartment["report"]       = regionDepartments[rd].department;
				tmpRegionDepartment["region"]       = regionDepartments[rd].region;
				tmpRegionDepartment["market"]       = '';
				tmpRegionDepartment["marketid"]     = 0;
				tmpRegionDepartment["department"]   = regionDepartments[rd].department;
				tmpRegionDepartment["departmentid"] = regionDepartments[rd].departmentid;
				tmpRegionDepartment["script"]       = script;
				tmpRegionDepartment["iconCls"]      = 'chart_bar';
				tmpRegionDepartment["leaf"]         = true;
				
				finalRegionDepartments.push(tmpRegionDepartment);
			}
			
			tmpRegion["children"] = finalRegionDepartments;
			finalRegions.push(tmpRegion);
		}
		
		return finalRegions;
	}
	
	
	/**
	 * 
	 */
	function createChurnsMenu(admin, type, locationID, userID) {
		var stMenu = "";
		
		if(type === "all") {
			stMenu = "var dataChurns =  {\"text\":\".\",\"children\": [";
			
			var marketSearchObj = search.load({ id: "customsearch_clgx_chrt_act_sale_reps" });
			marketSearchObj.filters.push(search.createFilter({ name: "location", operator: search.Operator.NONEOF, values: 1 }));
			marketSearchObj.columns.push(search.createColumn({ name: "location", summary: search.Summary.GROUP }));
			
			var marketSearchResult = marketSearchObj.run();
			
			stMenu += "\n{entity:\"Churn Funnel Quarterly\", market: \"All\", marketid:0, script: 319, iconCls:\"chart_bar\", expanded:true, leaf:false, children:[\n";
			marketSearchResult.each(function(result) {
				var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:319, iconCls:\"chart_bar\", leaf:true},\n";
				}
				
				return true;
			});
			
			var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]},\n";
			
            
            stMenu += "\n{type: \"market\", script:318,entity:\"Markets\", marketid:0, repid: 0, iconCls:\"facility\", expanded:false, leaf:false, children:[\n";
            marketSearchResult.each(function(result) {
            	var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
            	
            	if(marketID != null && marketID != "") {
            		var marketName = result.getValue({ name: "location", summary: search.Summary.GROUP });
            		stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:318, iconCls:\"facility\", leaf:true},\n";
            	}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]},\n";
            
            var salesRepSearchObj = search.load({ id: "customsearch_clgx_churn_salesrep" });
            stMenu += "\n{type: \"reps\", entity:\"Sales Reps\", marketid:0, repid:0, iconCls:\"group\", expanded:false,script:318, leaf:false, children:[\n";
            
            salesRepSearchObj.run().each(function(result) {
            	var salesRepID   = result.getValue("custrecord_clgx_churn_salesrep");
				var salesRepName = result.getText("custrecord_clgx_churn_salesrep");
				
				if(salesRepID != null && salesRepID != "") {
					stMenu += "{type: \"reps\", entity:'" + salesRepName + "', marketid:0, repid:'" + salesRepID + "', script:318, iconCls:\"user_suit\", leaf:true},\n";
				}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]},\n";
            
            
            stMenu += "\n{type: \"market\", script:320,entity:\"Churn Report - Lost\", marketid:0, repid: 0, iconCls:\"chart_bar\", expanded:false, leaf:false, children:[\n";
            
            marketSearchResult.each(function(result) {
            	var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
            	
            	if(marketID != null && marketID != "") {
            		var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
            		stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:320, iconCls:\"chart_bar\", leaf:true},\n";
            	}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]}\n";
            
            stMenu += "]};\n";
		} else if(type === "team") {
			stMenu = "var dataChurns =  {\"text\":\".\",\"children\": [";
			
			var marketSearchObj = search.load({ id: "customsearch_clgx_chrt_act_sale_reps" });
			
			marketSearchObj.filters.push(search.createFilter({ name: "location", operator: search.Operator.ANYOF, values: locationID }));
			marketSearchObj.filters.push(search.createFilter({ name: "internalid", operator: search.Operator.NONEOF, values: 71418 }));
			marketSearchObj.columns.push(search.createColumn({ name: "location", summary: search.Summary.GROUP }));
			
			stMenu += "\n{type: \"market\", entity:\"Churn Funnel Quarterly\", marketid:0, repid: 0, iconCls:\"chart_bar\", expanded:true, leaf:false, children:[\n";
			
			var marketSearchResult = marketSearchObj.run();
			
			marketSearchResult.each(function(result) {
				var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:319, iconCls:\"chart_bar\", leaf:true},\n";
				}
			});
			
			var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
			stMenu += "]},\n";
			
			
			
			stMenu += "\n{type: \"market\", entity:\"Markets\", marketid:0, repid: 0, iconCls:\"facility\", expanded:false, leaf:false, children:[\n";
			
			marketSearchResult.each(function(result) {
				var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:319, iconCls:\"facility\", leaf:true},\n";
				}
			});
			
			var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]},\n";
			
            
            
            var salesRepSearchObj = search.load({ id: "customsearch_clgx_churn_salesrep" });
            
            stMenu += "\n{type: \"reps\", entity:\"Sales Reps\", marketid:0, repid:0, iconCls:\"group\", expanded:false, leaf:false, children:[\n";
            salesRepSearchObj.run().each(function(result) {
            	var salesRepID = result.getValue({ name: "custrecord_clgx_churn_salesrep", summary: search.Summary.GROUP });
            	var salesRepName = result.getText({ name: "custrecord_clgx_churn_salesrep", summary: search.Summary.GROUP });
            	
            	if(salesRepID != null && salesRepID != "") {
            		if(userID == salesRepID) {
            			stMenu += "{type: \"reps\", entity:'" + salesRepName + "', marketid:0, repid:'" + salesRepID + "', script:318, iconCls:\"user_suit\", leaf:true},\n";
            		}
            	}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]},\n";
            
            
            stMenu += "\n{type: \"market\", script:320,entity:\"Churn Report - Lost\", marketid:0, repid: 0, iconCls:\"lost_bar\", expanded:false, leaf:false, children:[\n";
            
            marketSearchResult.each(function(result) {
            	var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
            	
            	if(marketID != null && marketID != "") {
            		var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
            		stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:319, iconCls:\"chart_bar\", leaf:true},\n";
            	}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]}\n";
            
            stMenu += "]};\n";
		} else if(type === "rep") {
			stMenu = "var dataChurns  =  {\"text\":\".\",\"children\": [";
			
			var salesRepSearchObj = search.load({ id: "customsearch_clgx_churn_salesrep" });
			
			salesRepSearchObj.filters.push(search.createFilter({ name: "custrecord_clgx_churn_salesrep", operator: search.Operator.ANYOF, values: userID }));
			
			stMenu += "\n{type: \"reps\", entity:\"Sales Reps\", script:318,marketid:0, repid:'" + userID + "', iconCls:\"group\", expanded:true, leaf:false, children:[\n";
			salesRepSearchObj.run().each(function(result) {
				var salesRepID   = result.getValue("custrecord_clgx_churn_salesrep");
				var salesRepName = result.getText("custrecord_clgx_churn_salesrep");
				
				if(salesRepID != null && salesRepID != "") {
					stMenu += "{type: \"reps\", entity:'" + salesRepName + "', marketid:0, repid:'" + salesRepID + "', script:318, iconCls:\"user_suit\", leaf:true},\n";
				}
				
				return true;
			});
			
			stMenu += "]};\n";
		} else {
			stMenu = "var dataChurns =  {\"text\":\".\",\"children\": []};";
		}
		
		return stMenu;
	}
	
	
	/**
	 * 
	 */
	function createNotChurnsMenu(admin, type, locationID, userID) {
		var stMenu = "";
		
		if(type === "all") {
			stMenu = "var dataNotChurns =  {\"text\":\".\",\"children\": [";
			
			var marketSearchObj = search.load({ id: "customsearch_clgx_chrt_act_sale_reps" });
			
			marketSearchObj.columns.push(search.createColumn({ name: "location", summary: search.Summary.GROUP }));
			marketSearchObj.filters.push(search.createFilter({ name: "location", operator: search.Operator.NONEOF, values: 1 }));
			
			var marketSearchResult = marketSearchObj.run();
			
			stMenu += "\n{entity:\"Net Change in MRR\", market: \"All\", marketid:0, script: 444, iconCls:\"chart_bar\", expanded:true, leaf:false, children:[\n";
			marketSearchResult.each(function(result) {
				var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
				
				if(marketID != null && marketID != "") {
					var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
					stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:444, iconCls:\"chart_bar\", leaf:true},\n";
				}
				
				return true;
			});
			
			var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += ']},\n';
			
            
            stMenu += "\n{entity:\"MRR by Category\", market: \"All\", marketid:0, script: 446, iconCls:\"chart_bar\", expanded:false, leaf:false, children:[\n";
            
            marketSearchResult.each(function(result) {
            	var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
            	
            	if(marketID != null && marketID != "") {
            		var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
            		stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:446, iconCls:\"chart_bar\", leaf:true},\n";
            	}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += "]},\n";


            stMenu += "]};\n";
		} else if(type === "team") {
			stMenu = "var dataNotChurns =  {\"text\":\".\",\"children\": [";
			
			var marketSearchObj = search.load({ id: "customsearch_clgx_chrt_act_sale_reps" });
			marketSearchObj.columns.push(search.createColumn({ name: "location", summary: search.Summary.GROUP }));
			marketSearchObj.filters.push(search.createFilter({ name: "location", operator: search.Operator.ANYOF, values: locationID }));
			marketSearchObj.filters.push(search.createFilter({ name: "internalid", operator: search.Operator.NONEOF, values: 71418 }));
			
			var marketSearchResult = marketSearchObj.run();
			
			stMenu += "\n{entity:\"Net Change in MRR\", market: \"All\", marketid:0, script: 444, iconCls:\"chart_bar\", expanded:true, leaf:false, children:[\n";
			marketSearchResult.each(function(result) {
				var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
            	
            	if(marketID != null && marketID != "") {
            		var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
            		stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:444, iconCls:\"chart_bar\", leaf:true},\n";
            	}
				
				return true;
			});
			
			var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]},\n";
            
            stMenu += "\n{entity:\"Revenue per Category\", market: \"All\", marketid:0, script: 446, iconCls:\"chart_bar\", expanded:false, leaf:false, children:[\n";
            
            marketSearchResult.each(function(result) {
            	var marketID = result.getValue({ name: "location", summary: search.Summary.GROUP });
            	
            	if(marketID != null && marketID != "") {
            		var marketName = result.getText({ name: "location", summary: search.Summary.GROUP });
            		stMenu += "{type: \"market\", entity:'" + marketName + "', marketid:'" + marketID + "', repid:0, script:446, iconCls:\"chart_bar\", leaf:true},\n";
            	}
            	
            	return true;
            });
            
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0, strLen - 1);
            stMenu += "]},\n";

            stMenu += "]};\n";
		} else {
			stMenu = "var dataNotChurns =  {\"text\":\".\",\"children\": []};";
		}
		
		return stMenu;
	}
	
	
	function createRenewalsMenu(admin, type, locationID, userID) {}
	
	
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	try {
    		var fileID = runtime.getCurrentScript().getParameter({ name: "custscript_clgx2_sl_chrt_mnu_file" });
    		
    		var userObj      = runtime.getCurrentUser();
    		var userID       = userObj.id;
    		var userRole     = userObj.role;
    		
    		var columns      = search.lookupFields({type: search.Type.EMPLOYEE, id: userID, columns: ["location", "department"]});
    		var locationID   = columns["location"][0].value;
    		var departmentID = columns["department"][0].value;
    		
    		var fileObj      = file.load({ id: fileID });
    		var html         = fileObj.getContents();
    		
    		if (userRole == -5 || userRole == 3 || userRole == 18 ) {
    			html = html.replace(new RegExp('{dataActivities}','g'), createActivitiesMenu("yes", "all", locationID, userID));
                //html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=716&deploy=1&departmentid=0&marketid=0&userid=0&period=month&periodid=0');
                html = html.replace(new RegExp('{dataSales}','g'), createSalesMenu("all", locationID, userID));
                html = html.replace(new RegExp('{dataChurns}','g'), createChurnsMenu('yes', 'all', locationID, userID));
                html = html.replace(new RegExp('{dataNotChurns}','g'), createNotChurnsMenu('yes', 'all', locationID, userID));
                ////html = html.replace(new RegExp('{dataMaintenance}','g'), menuMaintenance('yes', 'all', locationid, userid));
                html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
                //html = html.replace(new RegExp('{dataRenewals}','g'), menuRenewals('yes', 'all', locationid, userid));
                html = html.replace(new RegExp('{menuItems}','g'), 'treeSales,treeRenewals,treeChurns,treeNotChurns,treeActivities');
    		}
    		
    		//log.debug({ title: "locationID", details: locationID });
    		//log.debug({ title: "departmentID", details: departmentID });
    		
    		//log.debug({ title: "departmentID", details: createMaintenanceMenu("yes", "all", locationID, userID) });
    		
    		context.response.write(html);
    	} catch(ex) {
    		log.debug({ title: "Error", details: "Error: " + ex });
    	}
    }

    return {
        onRequest: onRequest
    };
    
});
