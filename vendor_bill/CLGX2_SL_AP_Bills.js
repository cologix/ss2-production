/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 8/9/2018
 */
define(["N/search", "N/file"],
function(search, file) {
    function onRequest(context) {
    	try {
    		var fileObject = file.load({ id: 12777164 });
    		var contents   = fileObject.getContents();
    		var bills      = getVendorBillList();
    		var billCount  = bills.length;
    		
    		contents       = contents.replace(new RegExp("{bills}","g"), JSON.stringify(bills));
    		contents       = contents.replace(new RegExp("{recordCount}","g"), "\"" + (billCount + terms(billCount)) + "\"");
    		contents       = contents.replace(new RegExp("{isDisabled}","g"), billCount <= 0 ? true : false);
    		
    		context.response.write(contents);
    	} catch(ex) {
    		context.response.write(JSON.stringify(ex));
    	}
    }

    function getVendorBillList() {
    	var bills = new Array();
    	var searchObject = search.load({ type: "vendorbill", id: "customsearch_clgx_change_account_period" });
    	searchObject.run().each(function(result) {
    		bills.push({ internalid: parseInt(result.getValue("internalid")), refnumber: result.getValue(result.columns[0]), datecreated: result.getValue("datecreated"), 
    			vendor: result.getValue({ join: "vendor", name: "entityid" }), amount: result.getValue("amount"), location: result.getText("locationnohierarchy"), 
    			project: result.getText("custbody_cologix_project_name"), status: result.getText("statusref") });    		
    		return true;
    	});
    	
    	return bills;
    }
    
    function terms(value) {
    	if(value == 1) {
    		return " record"
    	} else {
    		return " records"
    	}
    }
    
    function booleanConversion(value) {
    	if(value == true) {
    		return "Yes";
    	} else {
    		return "No";
    	}
    }
    
    return {
        onRequest: onRequest
    };
    
});
