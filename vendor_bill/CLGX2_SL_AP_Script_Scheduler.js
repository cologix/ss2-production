/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 8/9/2018
 */
define(["N/task"],
function(task) {
	function onRequest(context) {
		var taskObject          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
		taskObject.scriptId     = "1579";
		taskObject.deploymentId = "customdeploy_clgx2_ss_chng_acct_period";
		taskObject.submit();
		
		context.response.write("The script has been scheduled and will begin processing shortly.");
	}
	
    return {
        onRequest: onRequest
    };
    
});
