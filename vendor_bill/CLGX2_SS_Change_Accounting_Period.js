/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   5/16/2018
 */
define(["N/search", "N/record", "N/format", "/SuiteScripts/clgx/libraries/moment.min"],
function(search, record, format, moment) {
    function execute(scriptContext) {
    	var monthStart = moment().format("M/1/YYYY");
    	
    	var columns = new Array();
		columns.push(search.createColumn({ name: "internalid" }));
    	var searchObject = search.load({ type: "transaction", id: "customsearch_clgx_change_account_period" });
    	
    	searchObject.run().each(function(result) {
    		try {
    			var recordObject     = record.load({ type: "vendorbill", id: result.getValue("internalid") });
    			
        		var oldBillDate      = recordObject.getValue("duedate");
        		var oldPostingPeriod = recordObject.getValue("postingperiod");
        		var oldDate          = recordObject.getValue("trandate");
        		
        		recordObject.setValue({ fieldId: "trandate", value: format.parse({ value: monthStart, type: format.Type.DATE }) });
        		recordObject.setValue({ fieldId: "duedate",  value: oldBillDate });
        		
        		recordObject.save();
        		
        		log.audit({ title: "CLGX2_SS_Change_Accounting_Period - Record Updated", details: "Vendor Bill: " + result.getValue("internalid") });
    		} catch(ex) {
    			log.error({ title: "CLGX2_SS_Change_Accounting_Period - Error", details: "Vendor Bill " + result.getValue("internalid") + " was not updated. Error: " + ex });
    		}
    		
    		return true;
    	});
    }

    return {
        execute: execute
    };
    
});
