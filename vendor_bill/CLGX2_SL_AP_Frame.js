/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 8/7/2018
 */
define(["N/ui/serverWidget"],
function(ui) {
    function onRequest(context) {
    	var form = ui.createForm({ title: "Accounting Period Update" });
    	var html = form.addField({
    		id: "custpage_sl_ap_frame_html",
    		label: "HTML",
    		type: ui.FieldType.INLINEHTML
    	});
    	
    	html.defaultValue = "<iframe name=\"period\" id=\"period\" src=\"/app/site/hosting/scriptlet.nl?script=1591&deploy=1\" width=\"1260px\" height=\"565px\" frameborder=\"0\" scrolling=\"no\"></iframe>";
    	context.response.writePage(form);
    }

    return {
        onRequest: onRequest
    };
    
});
