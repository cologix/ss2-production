/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/file", "N/runtime", "N/record"], function(file, runtime, record) {
	/**
	 * @typedef ReportObject
	 * @property {String} name - The name of the report's custom record.
	 * @property {String} code - The embed code of the report.
	 */
	
    function onRequest(context) {
    	var sc       = runtime.getCurrentScript();
    	var reportID = sc.getParameter("custscript_clgx_sl_tr_rid");
    	
    	var reportObject = getReport(reportID);
    	var body         = buildBody(reportObject);
    	
    	context.response.write(body);
    }
    
    
    /**
     * Returns a report's custom record information.
     * 
     * @access private
     * @function getReport
     * 
     * @param {Number} reportID - The custom record's internal id.
     * @returns {ReportObject}
     */
    function getReport(reportID) {
    	var recordObject = record.load({ type: "customrecord_clgx_tableau_report", id: reportID });
    	
    	return {
    		name: recordObject.getValue("name"),
    		code: recordObject.getValue("custrecord_clgx_tr_embed_code")
    	};
    }
    
    
    /**
     * Builds the final body of the template.
     * 
     * @access private
     * @function buildBody
     * 
     * @param {ReportObject} reportObject
     * @returns {String}
     */
    function buildBody(reportObject) {
    	var fileObject = file.load({ id: 14391340 });
    	var content    = fileObject.getContents();
    	
    	content = content.replace(new RegExp("{title}", "g"), reportObject.name);
    	content = content.replace(new RegExp("{content}", "g"), reportObject.code);
    	
    	return content;
    }

    return {
        onRequest: onRequest
    };
    
});
