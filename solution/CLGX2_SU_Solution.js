/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 10/8/2018
 */
define(["N/record"],
function(record) {    
    function afterSubmit(scriptContext) {
    	var currentRecord = scriptContext.newRecord;
    	var recordObject  = record.load({ type: "solution", id: currentRecord.id });
    	var description   = recordObject.getValue("longdescription");
    	var createDate    = recordObject.getValue("createddate");
    	var modifiedDate  = recordObject.getValue("lastmodifieddate");
    	description       = description.replace(/<dates>[\s\S]*?<\/dates>/g, "<dates><i>Created: <b>" + createDate + "</b></i> - <i>Last Updated: <b>" + modifiedDate + "</b></i></dates>");
    	
    	recordObject.setValue({ fieldId: "longdescription", value: description });
    	recordObject.save();
    }
    
    return {
        afterSubmit: afterSubmit
    };
    
});
