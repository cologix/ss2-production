/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   2/20/2018
 */
define(["N/ui/serverWidget", "N/runtime"],
function(widget, runtime) {

	/**
	 * Called when the record is being created.
	 * 
	 * @param {Object} context
	 * @returns null
	 */
	function onCreate(form, parameters) {
		if(form.title == "Time Tracking") {
			//form.getField("customer").updateDisplayType({ displayType: widget.FieldDisplayType.DISABLED });
		} else if(form.title == "Weekly Time Tracking") {
			//form.getSublist("timeitem").getField("customer").updateDisplayType({ displayType: widget.FieldDisplayType.DISABLED });
		}
	}
	
	
	/**
	 * Called when the record is being edited.
	 * 
	 * @param {Object} context
	 * @returns null
	 */
	function onEdit(form, parameters) {
		//form.getField("customer").updateDisplayType({ displayType: widget.FieldDisplayType.DISABLED });
	}
	
	
	/**
	 * Called when the record is being viewed.
	 * 
	 * @param {Object} context
	 * @returns null
	 */
	function onView(form, parameters) {
		//form.getField("customer").updateDisplayType({ displayType: widget.FieldDisplayType.DISABLED });
	}
	
	
    function beforeLoad(context) {
    	try {		
    		var form            = context.form;
    		var parameters      = context.request.parameters || null;
    		
    		var execution       = new Array();
        	execution["create"] = onCreate;
        	execution["edit"]   = onEdit;
        	execution["view"]   = onView;
        	
        	execution[context.type](form, parameters);
    	} catch(ex) {
    		log.debug({ title: "CLGX2_SU_TimeBill - Error", details: ex });
    	}
    }

    return {
        beforeLoad: beforeLoad
    };
    
});