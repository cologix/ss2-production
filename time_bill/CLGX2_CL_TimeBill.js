/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   3/20/2018
 */
define([],
function() {
    function fieldChanged(context) {
    	if(context.sublistId == "timeitem" && context.fieldId == "custcol_clgx_tb_customer_service") {
    		context.currentRecord.setCurrentSublistValue({ sublistId: "timeitem", fieldId: "customer", value: context.currentRecord.getCurrentSublistValue({ sublistId: "timeitem", fieldId: "custcol_clgx_tb_customer_service" }) });
    	}
    	
    	
    	if((context.sublistId == null || context.sublistId == "") && context.fieldId == "custcol_clgx_tb_customer_service") {
    		var native_project = parseInt(context.currentRecord.getValue("customer"));
    		var custom_project = parseInt(context.currentRecord.getValue("custcol_clgx_tb_customer_service"));
    		
    		if(custom_project && (native_project != custom_project)) {
    			context.currentRecord.setValue({ fieldId: "customer", value: context.currentRecord.getValue("custcol_clgx_tb_customer_service") });
    		}
    	}
    	
    	
    	if((context.sublistId == null || context.sublistId == "") && context.fieldId == "customer") {
    		var native_project = parseInt(context.currentRecord.getValue("customer"));
    		var custom_project = parseInt(context.currentRecord.getValue("custcol_clgx_tb_customer_service"));
    		
    		if(native_project) {
    			context.currentRecord.setValue({ fieldId: "custcol_clgx_tb_customer_service", value: context.currentRecord.getValue("customer") });
    		}
    	}
    }

    return {
        fieldChanged: fieldChanged
    };
    
});
