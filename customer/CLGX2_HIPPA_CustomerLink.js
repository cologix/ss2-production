/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/record", "N/search"],
function(record, search) {
	/**
	 * Returns an array of opportunity internal ids.
	 * 
	 *  @param {number} customerId - The customer's internal id.
	 *  @return array
	 */
	function getOpportunitiesByCustomerID(customerId) {
		var finalArray = [];
		var s = search.create({ 
			type: search.Type.OPPORTUNITY,
			columns: ["internalid"],
			filters: [['entity', 'is', parseInt(customerId)], 'and', 
			          ['entitystatus', 'noneof', '13'], 'and', 
			          ['entitystatus', 'noneof', '14'], 'and',
			          ['custbodyclgx_hipaa_baa_proposal', 'is', 'F']]
		});
		
		s.run().each(function(result) {
			finalArray.push(result.getValue("internalid"));
		});
		
		return finalArray;
	}
	
	/**
	 * Returns an array of proposal internal ids.
	 * 
	 *  @param {number} customerId - The customer's internal id.
	 *  @return array
	 */
	function getProposalsByCustomerID(customerId) {
		var finalArray = [];
		var s = search.create({ 
			type: search.Type.ESTIMATE,
			columns: ["internalid"],
			filters: [['entity', 'is', parseInt(customerId)], 'and', 
			          ['entitystatus', 'noneof', '13'], 'and', 
			          ['entitystatus', 'noneof', '14'], 'and',
			          ['custbodyclgx_hipaa_baa_proposal', 'is', 'F']]
		});
		
		s.run().each(function(result) {
			finalArray.push(result.getValue("internalid"));
		});
		
		return finalArray;
	}
	
	
	/**
	 * Processes the opportunity records.
	 * 
	 * @param {array} opportunityArray - The array of opportunity internal ids.
	 * @return null
	 */
	function processOpportunityRecords(recordType, recordId, opportunityArray) {
		if(opportunityArray != null && opportunityArray != "") {
			var count = opportunityArray.length;
			
			var opp = null;
			for(var o = 0; count < o; o++) {
				opp = record.load({ type: recordType, id: recordId });
				opp.setValue({ fieldId: "custbodyclgx_hipaa_baa_proposal", value: true });
				opp.save();
			}
		} else {
			
		}
	}
	
	/**
	 * Processes the proposal records.
	 * 
	 * @param {array} proposalArray - The array of proposal internal ids.
	 * @return null
	 */
	function processProposalRecords(recordType, recordId, proposalArray) {
		if(proposalArray != null && proposalArray != "") {
			var count = proposalArray.length;
			
			var prop = null;
			for(var p = 0; count < p; p++) {
				prop = record.load({ type: recordType, id: recordId });
				prop.setValue({ fieldId: "custbodyclgx_hipaa_baa_proposal", value: true });
				prop.save();
			}
		} else {
			
		}
	}
	
	
    /**
     * After Submit - Entry Point.
     *
     * @param {object} context - Script execution context.
     * @Since 2015.2
     */
    function afterSubmit(context) {
    	var logging = runtime.getCurrentScript().getParameter({ name: "custscript_clgx2_hippa_link_log" });
    	
    	try {
    		
    	} catch(e) {
    		
    	}
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
