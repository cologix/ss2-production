/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   8/4/2017
 * 
 * Script Name:	CLGX2_SU_HIPAA_CustomerLink
 * Script File:	CLGX2_SU_HIPAA_CustomerLink.js
 * Script ID:   customscript_clgx2_su_hipaa_customerlink
 * Script Type:	User Event
 * Script Event: Edit
 * Script Record: Customer
 * Purpose: This script is used to update the HIPPA check box on all proposals and opportunities linked to a customer record. This script 
 *          is designed to be executed on "Edit" against a customer record.
 * 
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/record", "N/search", "N/runtime"],
function(record, search, runtime) {
	/**
	 * Returns an array of opportunity internal ids.
	 * 
	 *  @param {number} customerId - The customer's internal id.
	 *  @param {boolean} logging - The logging flag.
	 *  @return array
	 */
	function getOpportunitiesByCustomerID(customerId, logging) {
		var finalArray = [];
		var s = search.create({ 
			type: search.Type.OPPORTUNITY,
			columns: ["internalid"],
			filters: [['entity', 'is', customerId], 'and', 
			          ['entitystatus', 'noneof', ['13', '14']], 'and',
			          ['custbody_clgx_hipaa_baa_proposal', 'is', 'F']]
		});
		
		s.run().each(function(result) {
			finalArray.push(result.getValue("internalid"));
			
			return true;
		});
		
		return finalArray;
	}
	
	
	/**
	 * Returns an array of proposal internal ids.
	 * 
	 *  @param {number} customerId - The customer's internal id.
	 *  @param {boolean} logging - The logging flag.
	 *  @return array
	 */
	function getProposalsByCustomerID(customerId, logging) {
		var finalArray = [];
		var s = search.create({ 
			type: search.Type.ESTIMATE,
			columns: ["internalid"],
			filters: [['entity', 'is', customerId], 'and', 
			          ['entitystatus', 'noneof', ['13', '14']], 'and', 
			          ['custbody_clgx_hipaa_baa_proposal', 'is', 'F'], 'and',
			          ['mainline', 'is', 'T']]
		});
		
		s.run().each(function(result) {
			finalArray.push(result.getValue("internalid"));
			
			return true;
		});
		
		return finalArray;
	}
	
	
	/**
	 * Processes the opportunity records.
	 * 
	 * @param {number} customerId - The internal id of the customer being saved.
	 * @param {boolean} logging - The logging flag.
	 * @return null
	 */
	function processOpportunityRecords(customerId, logging) {
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "==============================" });
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "Begin opportunity processing" });
		
		var opportunityArray = getOpportunitiesByCustomerID(customerId, logging);
		
		if(opportunityArray != null && opportunityArray != "") {
			var count = opportunityArray.length;
			
			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "Opportunity Array: " + opportunityArray });
			
			var opp = null;
			for(var o = 0; o < count; o++) {
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "Opening opportunity: " + opportunityArray[o] });
				
				opp = record.load({ type: record.Type.OPPORTUNITY, id: opportunityArray[o] });
				
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Checkbox value before save: " + opp.getValue({ fieldId: "custbody_clgx_hipaa_baa_proposal" }) });
				
				opp.setValue({ fieldId: "custbody_clgx_hipaa_baa_proposal", value: true });
				opp.save({ ignoreMandatoryFields: true });
				
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Checkbox value after save: " + opp.getValue({ fieldId: "custbody_clgx_hipaa_baa_proposal" }) });
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "Saved opportunity: " + opportunityArray[o] });
			}
		} else {
			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "Opportunity array is null. Skipping." });
		}
		
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "End opportunity processing" });
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processOpportunityRecords", details: "==============================" });
	}
	
	
	/**
	 * Processes the proposal records.
	 * 
	 * @param {number} customerId - The internal id of the customer being saved.
	 * @param {boolean} logging - The logging flag.
	 * @return null
	 */
	function processProposalRecords(customerId, logging) {
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "==============================" });
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Begin proposal processing" });
		
		var proposalArray = getProposalsByCustomerID(customerId, logging);
		
		if(proposalArray != null && proposalArray != "") {
			var count = proposalArray.length;
			
			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Proposal Array: " + proposalArray });
			
			var prop = null;
			for(var p = 0; p < count; p++) {
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Opening proposal: " + proposalArray[p] });
				
				prop = record.load({ type: record.Type.ESTIMATE, id: proposalArray[p] });
				
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Checkbox value before save: " + prop.getValue({ fieldId: "custbody_clgx_hipaa_baa_proposal" }) });
				
				prop.setValue({ fieldId: "custbody_clgx_hipaa_baa_proposal", value: true });
				prop.save({ ignoreMandatoryFields: true });
				
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Checkbox value after save: " + prop.getValue({ fieldId: "custbody_clgx_hipaa_baa_proposal" }) });
				log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Saved proposal: " + proposalArray[p] });
			}
		} else {
			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "Proposal array is null. Skipping." });
		}
		
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "End proposal processing" });
		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - processProposalRecords", details: "==============================" });
	}
	
	
    /**
     * After Submit - Entry Point.
     *
     * @param {object} context - Script execution context.
     * @Since 2015.2
     */
    function afterSubmit(context) {
    	var logging = runtime.getCurrentScript().getParameter({ name: "custscript_clgx2_hipaa_link_log" });
    	
    	try {
    		if(context.newRecord.getValue({ fieldId: "custentity_clgx_hipaa_baa" }) == true) {
    			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - afterSubmit", details: "==============================" });
    			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - afterSubmit", details: "Customer ID: " + context.newRecord.id});
    			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - afterSubmit", details: "Event Type: " + context.type});
    			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - afterSubmit", details: "Value of HIPAA BAA: " + context.newRecord.getValue({ fieldId: "custentity_clgx_hipaa_baa" })});
        		log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - afterSubmit", details: "==============================" });
        		
    			processOpportunityRecords(context.newRecord.id, logging);
        		processProposalRecords(context.newRecord.id, logging);
    		} else {
    			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - afterSubmit", details: "custentity_clgx_hipaa_baa is set to false. Skipping." });
    		}
    	} catch(e) {
			log.debug({ title: "CLGX2_SU_HIPAA_CustomerLink - afterSubmit", details: "Error: " + e });
    	}
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
