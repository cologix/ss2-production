/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   8/4/2017
 * 
 * Script Name:	CLGX2_SU_HIPPA_CustomerLink
 * Script File:	CLGX2_SU_HIPPA_CustomerLink.js
 * Script ID:   customscript_clgx2_su_hippa_customerlink
 * Script Type:	User Event
 * Script Event: Edit
 * Script Record: Customer
 * 
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/record", "N/search", "N/runtime"],
function(record, search, runtime) {
	/**
	 * Returns an array of opportunity internal ids.
	 * 
	 *  @param {number} customerId - The customer's internal id.
	 *  @param {boolean} logging - The logging flag.
	 *  @return array
	 */
	function getOpportunitiesByCustomerID(customerId, logging) {
		var finalArray = [];
		var s = search.create({ 
			type: search.Type.OPPORTUNITY,
			columns: ["internalid"],
			filters: [['entity', 'is', parseInt(customerId)], 'and', 
			          ['entitystatus', 'noneof', '13'], 'and', 
			          ['entitystatus', 'noneof', '14'], 'and',
			          ['custbodyclgx_hipaa_baa_proposal', 'is', 'F']]
		});
		
		s.run().each(function(result) {
			finalArray.push(result.getValue("internalid"));
		});
		
		return finalArray;
	}
	
	
	/**
	 * Returns an array of proposal internal ids.
	 * 
	 *  @param {number} customerId - The customer's internal id.
	 *  @param {boolean} logging - The logging flag.
	 *  @return array
	 */
	function getProposalsByCustomerID(customerId, logging) {
		var finalArray = [];
		var s = search.create({ 
			type: search.Type.ESTIMATE,
			columns: ["internalid"],
			filters: [['entity', 'is', parseInt(customerId)], 'and', 
			          ['entitystatus', 'noneof', '13'], 'and', 
			          ['entitystatus', 'noneof', '14'], 'and',
			          ['custbodyclgx_hipaa_baa_proposal', 'is', 'F']]
		});
		
		s.run().each(function(result) {
			finalArray.push(result.getValue("internalid"));
		});
		
		return finalArray;
	}
	
	
	/**
	 * Processes the opportunity records.
	 * 
	 * @param {number} customerId - The internal id of the customer being saved.
	 * @param {boolean} logging - The logging flag.
	 * @return null
	 */
	function processOpportunityRecords(customerId, logging) {
		if(logging != null && logging == true) {
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processOpportunityRecords", details: "==============================" });
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processOpportunityRecords", details: "Begin opportunity processing" });
		}
		
		var opportunityArray = getOpportunitiesByCustomerID(customerId, logging);
		
		if(opportunityArray != null && opportunityArray != "") {
			var count = opportunityArray.length;
			
			var opp = null;
			for(var o = 0; o < count; o++) {
				opp = record.load({ type: record.Type.OPPORTUNITY, id: opportunityArray[o] });
				opp.setValue({ fieldId: "custbodyclgx_hipaa_baa_proposal", value: true });
				opp.save({ ignoreMandatoryFields: true });
				
				if(logging != null && logging == true) {
					log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processOpportunityRecords", details: "Saved opportunity: " + opportunityArray[o] });
				}
			}
		} else {
			if(logging != null && logging == true) {
				log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processOpportunityRecords", details: "Opportunity array is null. Skipping." });
			}
		}
		
		if(logging != null && logging == true) {
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processOpportunityRecords", details: "End opportunity processing" });
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processOpportunityRecords", details: "==============================" });
		}
	}
	
	
	/**
	 * Processes the proposal records.
	 * 
	 * @param {number} customerId - The internal id of the customer being saved.
	 * @param {boolean} logging - The logging flag.
	 * @return null
	 */
	function processProposalRecords(customerId, logging) {
		if(logging != null && logging == true) {
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processProposalRecords", details: "==============================" });
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processProposalRecords", details: "Begin proposal processing" });
		}
		
		var proposalArray = getProposalsByCustomerID(customerId, logging);
		
		if(proposalArray != null && proposalArray != "") {
			var count = proposalArray.length;
			
			var prop = null;
			for(var p = 0; p < count; p++) {
				prop = record.load({ type: record.Type.ESTIMATE, id: proposalArray[p] });
				prop.setValue({ fieldId: "custbodyclgx_hipaa_baa_proposal", value: true });
				prop.save({ ignoreMandatoryFields: true });
				
				if(logging != null && logging == true) {
					log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processProposalRecords", details: "Saved proposal: " + proposalArray[p] });
				}
			}
		} else {
			if(logging != null && logging == true) {
				log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processProposalRecords", details: "Proposal array is null. Skipping." });
			}
		}
		
		if(logging != null && logging == true) {
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processProposalRecords", details: "End proposal processing" });
			log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - processProposalRecords", details: "==============================" });
		}
	}
	
	
    /**
     * After Submit - Entry Point.
     *
     * @param {object} context - Script execution context.
     * @Since 2015.2
     */
    function afterSubmit(context) {
    	var logging = runtime.getCurrentScript().getParameter({ name: "custscript_clgx2_hippa_link_log" });
    	
    	try {
    		if(context.newRecord.getValue({ fieldId: "custentityclgx_hipaa_baa" }) == true) {
    			processOpportunityRecords(context.newRecord.id, logging);
        		processProposalRecords(context.newRecord.id, logging);
    		} else {
    			if(logging != null && logging == true) {
    				log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - afterSubmit", details: "custentityclgx_hipaa_baa is set to false. Skipping." });
    			}
    		}
    	} catch(e) {
    		if(logging != null && logging == true) {
				log.debug({ title: "CLGX2_SU_HIPPA_CustomerLink - afterSubmit", details: "Error: " + e });
			}
    	}
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
