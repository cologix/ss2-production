/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * 
 * @module Customer/CLGX2_SU_LSA
 * 
 * @date 1/29/2019
 * @description Used to move a locked custom field created by another script.
 */
define(["N/runtime"], function(runtime) {
	function beforeLoad(context) {
		try {
			if(runtime.executionContext === runtime.ContextType.USER_INTERFACE) {
				var fieldObject = context.form.getField("custpage_lsa_vis");
				context.form.insertField({ field: fieldObject, nextfield: "custentity_clgx_date_salesrep_assigned" });
			}
		} catch(ex) {
			log.error({ title: "ERROR - Message", details: ex });
			log.error({ title: "ERROR - Request", details: context.request });
		}
	}
	
    return {
    	beforeLoad: beforeLoad
    };
});
