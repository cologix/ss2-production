/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 1/15/2018
 */
define([],
function() {
    function afterSubmit(context) {
    	log.debug({ title: "context", details: context.newRecord.getValue("email") });
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
