define(["N/search", "N/record"],
function(search, record) {	
	/**
	 * Returns a configuration variable from a custom record based off the category and/or name. If no
	 * properties are specified, then all configuration objects are returned.
	 * 
	 * @access public
	 * @function getConfig
	 * 
	 * @param {Object} paramObject
	 * @param {String} [paramObject.category] - The category of the setting. Case insensitive.
	 * @param {String} [paramObject.name] - The name of the setting. Case insensitive.
	 * @return {Array}
	 */
	function getConfig(paramObject) {
		try {
			var returnArray = new Array();
			
			var columns = new Array();
			columns.push(search.createColumn("internalid"));
			columns.push(search.createColumn("custrecord_clgx_mus_category"));
			columns.push(search.createColumn("custrecord_clgx_mus_name"));
			columns.push(search.createColumn("custrecord_clgx_mus_value"));
			 
			var filters = new Array();
			
			if(paramObject.category !== undefined) {
				filters.push(search.createFilter({ name: "custrecord_clgx_mus_category", operator: "contains", values: [paramObject.category] }));
			}
			
			if(paramObject.name !== undefined) {
				filters.push(search.createFilter({ name: "custrecord_clgx_mus_name", operator: "contains", values: [paramObject.name] }));
			}
			
			var searchObject = search.create({ type: "customrecord_clgx_dw_mu_settings", columns: columns, filters: filters });
			searchObject.run().each(function(result) {
				returnArray.push({
					id      : parseInt(result.getValue("internalid")),
					category: result.getValue("custrecord_clgx_mus_category"),
					name    : result.getValue("custrecord_clgx_mus_name"),
					value   : result.getValue("custrecord_clgx_mus_value")
				});
				return true;
			});
			
			return returnArray;
		} catch(ex) {
			log.error({ title: "getConfig - Error", details: ex });
		}
	}
	
	
	/**
	 * Updates a config object based on the key.
	 * 
	 * @access public
	 * @function updateConfig
	 * 
	 * @param {Object} paramObject
	 * 
	 * @param {Object} paramObject.old - The old values of the configuration variable that's being updated.
	 * @param {String} paramObject.old.category - The current category of the configuration variable.
	 * @param {String} paramObject.old.name - The current name of the configuration variable.
	 * 
	 * @param {Object} paramObject.new - The new values (if any) of the configuration variable being updated.
	 * @param {String} paramObject.new.category - The new category name of the configuration, if any.
	 * @param {String} paramObject.new.name - The new name of the configuration object, if any.
	 * @param {String} paramObject.new.value - The new value of the configuration variable.
	 * 
	 * @return {Boolean}
	 */
	function updateConfig(paramObject) {
		try {
			if(paramObject) {
				
				if(paramObject.old && paramObject.new) {
					var columns = new Array();
					columns.push(search.createColumn("internalid"));
					
					var filters = new Array();
					filters.push(search.createFilter({ name: "custrecord_clgx_mus_category", operator: "is", values: [paramObject.old.category] }));
					filters.push(search.createFilter({ name: "custrecord_clgx_mus_name", operator: "is", values: [paramObject.old.name] }));
					
					var searchObject = search.create({ type: "customrecord_clgx_dw_mu_settings", columns: columns, filters: filters });
					ssearchObject.run().each(function(result) {
						var recordObject = record.load({ type: "customrecord_clgx_dw_mu_settings", id: result.getValue("internalid") });
						recordObject.setValue({ fieldId: "custrecord_clgx_mus_category", value: paramObject.new.category });
						recordObject.setValue({ fieldId: "custrecord_clgx_mus_name",     value: paramObject.new.name });
						recordObject.setValue({ fieldId: "custrecord_clgx_mus_value",    value: paramObject.new.value });
						recordObject.save();
					});
				}
				
				return true;
			}
		} catch(ex) {
			log.error({ title: "updateConfigByKey - Error", details: ex });
		}
		
		return false;
	}
	
	return {
		getConfig: getConfig,
		updateConfig: updateConfig
	};
});
