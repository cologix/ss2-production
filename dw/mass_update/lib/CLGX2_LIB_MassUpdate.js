/**
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 */
define(["/SuiteScripts/clgx/dw/mass_update/lib/CLGX2_LIB_Config", "N/search", "N/task"],
function(config, search, task) {
	
	
	/**
	 * Returns a whitelist array of records who have a last modified date field.
	 * 
	 * @access public
	 * @function getFieldWhitelist
	 * 
	 * @return {Array}
	 */
	function getFieldWhitelist() {
		try {
			var finalArray = new Array();
			
			var columns = new Array();
			columns.push(search.createColumn("custrecord_clgx_mur_primary_name"));
			
			var filters = new Array();
			filters.push(search.createFilter({ name: "custrecord_clgx_dwmu_lmid", operator: "isnotempty", values: "" }));
			filters.push(search.createFilter({ name: "isinactive", operator: "is", values: "F" }));
			
			var searchObject = search.create({ type: "customrecord_clgx_dw_mu_records", columns: columns, filters: filters });
			searchObject.run().each(function(result) {
				finalArray.push(result.getValue("custrecord_clgx_mur_primary_name"));
				return true;
			});
			
			return finalArray;
		} catch(ex) {
			log.error({ title: "getFieldWhitelist - Error", details: ex });
		}
	}
	
	
	/**
	 * Returns a list of field internal ids for a specific record type.
	 * 
	 * @access public
	 * @function getFieldTypes
	 * 
	 * @param {Object} paramObject
	 * @param {String} paramObject.recordType
	 * 
	 * @return {Array}
	 */
	function getFieldTypes(paramObject) {
		try {
			if(paramObject) {
				var finalArray = new Array();
				
				var columns = new Array();
				columns.push(search.createColumn("custrecord_clgx_dwmu_lmid"));
				columns.push(search.createColumn("custrecord_clgx_dwmu_cdid"))
				
				var filters = new Array();
				filters.push(search.createFilter({ name: "custrecord_clgx_mur_primary_name", operator: "contains", values: [paramObject.recordType] }));
				filters.push(search.createFilter({ name: "custrecord_clgx_dwmu_lmid", operator: "isnotempty" }));
				filters.push(search.createFilter({ name: "custrecord_clgx_dwmu_cdid", operator: "isnotempty" }));
				
				var searchObject = search.create({ type: "customrecord_clgx_dw_mu_records", columns: columns, filters: filters });
				searchObject.run().each(function(result) {
					finalArray = [
						{ id: result.getValue("custrecord_clgx_dwmu_lmid"), text: "Last Modified Date" }, 
						{ id: result.getValue("custrecord_clgx_dwmu_cdid"), text: "Date Created" }
					];
				});
				
				return finalArray;
			}
		} catch(ex) {
			log.error({ title: "getFieldTypes - Error", details: ex });
		}
	}
	
	
	/**
	 * Returns a list of record types from a custom record.
	 * 
	 * @access public
	 * @function getRecordTypes
	 * 
	 * @returns Array
	 */
	function getRecordTypes() {
		try {
			var finalArray = new Array();
			
			var columns = new Array();
			columns.push(search.createColumn("internalid"));
			columns.push(search.createColumn({ name: "name", sort: search.Sort.ASC }));
			columns.push(search.createColumn("custrecord_clgx_mur_primary_name"));
			columns.push(search.createColumn("custrecord_clgx_dwmu_lmid"));
			columns.push(search.createColumn("custrecord_clgx_dwmu_cdid"));
			
			var filters = new Array();
			filters.push({ name: "isinactive", operator: "is", values: [false] });
			
			var searchObject = search.create({ type: "customrecord_clgx_dw_mu_records", columns: columns });
			searchObject.run().each(function(result) {
				finalArray.push({
					id: result.getValue("custrecord_clgx_mur_primary_name"),
					displayname: result.getValue("name")
				});
				
				return true;
			});
			
			return finalArray;
		} catch(ex) {
			log.error({ title: "getRecordTypes - Error", details: ex });
		}
	}
	
	
	/**
	 * Returns a list of record destinations from the configuration record.
	 * 
	 * @access public
	 * @function getRecordDestinations
	 * 
	 * @return {Array}
	 */
	function getRecordDestinations() {
		try {
			var configObject = config.getConfig({ category: "Environment", name: "Production" });
			return configObject;
		} catch(ex) {
			log.error({ title: "getRecordDestinations - Error", details: ex });
		}
	}
	
	
	/**
	 * Schedules a script to be run.
	 * 
     * @access public
     * @function scheduleScript
     * 
     * @param {Object} paramObject
     * @param {Number} paramObject.scriptId - The internal id of the script.
     * @param {Number} paramObject.deploymentId - The internal id of the script's deployment record.
     * @param {Object} paramObject.params - The object used to pass script parameters as a key/value pair.
     * 
     * @return null
     */
    function scheduleScript(paramObject) {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = paramObject.scriptId;
    		
    		if(paramObject.deploymentId !== undefined) {
    			t.deploymentId = paramObject.deploymentId;
    		}
    		
    		if(paramObject.params !== undefined) {
    			t.params = paramObject.params;
    		}
    		
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }
	
    
    /**
     * Returns whether or not a script is running.
     * 
     * @access public
     * @function isScriptInstanceRunning
     * 
     * @param {string} searchName
     * @return {boolean}
     */
    function isScriptInstanceRunning(searchName, filterArray) {
    	if(searchName) {
    		try {
    			var recordCount  = 0;
        		var searchObject = search.load({ id: searchName });
        		
        		if(filterArray) {
        			_.forEach(filterArray, function(object, key) {
        				if(object.join) {
        					searchObject.filters.push(search.createFilter({ name: object.name, join: object.join, operator: object.operator, values: [object.value] }));
        				} else {
        					searchObject.filters.push(search.createFilter({ name: object.name, operator: object.operator, values: [object.value] }));
        				}
        			});
        		}
        		
        		searchObject.run().each(function(result) {
        			recordCount = recordCount + 1;
        			return true;
        		});
        		
        		if(recordCount > 0) {
        			return true;
        		} else {
        			return false;
        		}
    		} catch(ex) {
    			log.error({ title: "isScriptInstanceRunning - Error", details: ex });
    		}
    	}
    }
    
    return {
    	getFieldWhitelist: getFieldWhitelist,
    	getFieldTypes: getFieldTypes,
    	getRecordTypes: getRecordTypes,
    	getRecordDestinations: getRecordDestinations,
    	scheduleScript: scheduleScript,
    	isScriptInstanceRunning: isScriptInstanceRunning
    };
    
});
