/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(["N/search"],
function(search) {
	var recordWhitelist = [];
	var currentRecord = {};
	
	function pageInit(scriptContext) {
		currentRecord = scriptContext.currentRecord;
		recordWhitelist = getFieldWhitelist();
		
		//Hide fields by default
		currentRecord.getField("custscript_clgx2_record_destination").isDisplay = false;
		currentRecord.getField("custscript_clgx2_date_field").isDisplay         = false;
		currentRecord.getField("custscript_clgx2_start_date").isDisplay         = false;
		currentRecord.getField("custscript_clgx2_end_date").isDisplay           = false;
	}
	
    function fieldChanged(scriptContext) {
    	if(scriptContext.fieldId == "custscript_clgx2_reset_queue") {
    		var currentCheckboxValue  = currentRecord.getValue("custscript_clgx2_reset_queue");
    		
    		//Disable all fields if the Reset Queue checkbox is checked
    		currentRecord.getField("custscript_clgx2_record_destination").isDisplay = !currentCheckboxValue;
    		currentRecord.getField("custscript_clgx2_record_type").isDisplay        = !currentCheckboxValue;
    		currentRecord.getField("custscript_clgx2_record_type").isMandatory      = !currentCheckboxValue;
    		currentRecord.getField("custscript_clgx2_date_field").isDisplay         = !currentCheckboxValue;
    		currentRecord.getField("custscript_clgx2_start_date").isDisplay         = !currentCheckboxValue;
    		currentRecord.getField("custscript_clgx2_end_date").isDisplay           = !currentCheckboxValue;
    		currentRecord.getField("custscript_clgx2_sync_on_complete").isDisplay   = !currentCheckboxValue;
    	}
    	
    	if(scriptContext.fieldId == "custscript_clgx2_record_type") {
    		var recordID = currentRecord.getValue("custscript_clgx2_record_type");
    		
    		if(recordWhitelist.indexOf(recordID) > -1) {
    			currentRecord.getField("custscript_clgx2_date_field").isDisplay         = true;
        		currentRecord.getField("custscript_clgx2_start_date").isDisplay         = true;
        		currentRecord.getField("custscript_clgx2_end_date").isDisplay           = true;
    		} else {
    			currentRecord.getField("custscript_clgx2_date_field").isDisplay         = false;
        		currentRecord.getField("custscript_clgx2_start_date").isDisplay         = false;
        		currentRecord.getField("custscript_clgx2_end_date").isDisplay           = false;
    		}
    	}
    }

    
    /**
	 * Returns a whitelist array of records who have a last modified date field.
	 * 
	 * @access public
	 * @function getFieldWhitelist
	 * 
	 * @return {Array}
	 */
	function getFieldWhitelist() {
		try {
			var finalArray = new Array();
			
			var columns = new Array();
			columns.push(search.createColumn("custrecord_clgx_mur_primary_name"));
			
			var filters = new Array();
			filters.push(search.createFilter({ name: "custrecord_clgx_dwmu_lmid", operator: "isnotempty", values: "" }));
			filters.push(search.createFilter({ name: "isinactive", operator: "is", values: "F" }));
			
			var searchObject = search.create({ type: "customrecord_clgx_dw_mu_records", columns: columns, filters: filters });
			searchObject.run().each(function(result) {
				finalArray.push(result.getValue("custrecord_clgx_mur_primary_name"));
				return true;
			});
			
			return finalArray;
		} catch(ex) {
			log.error({ title: "getFieldWhitelist - Error", details: ex });
		}
	}
    
    return {
    	pageInit: pageInit,
    	fieldChanged: fieldChanged
    };
    
});
