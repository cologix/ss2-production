/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/search", "N/record", "N/file", "N/runtime", "N/task", "/SuiteScripts/clgx/dw/mass_update/lib/CLGX2_LIB_MassUpdate", "N/format", "/SuiteScripts/clgx/libraries/moment.min"],
function(search, record, file, runtime, task, lib, format, moment) {
    function execute(scriptContext) {
    	try {
    		var rt = runtime.getCurrentScript();
        	
        	var paramObject                = new Object();
        	paramObject.savedSearchID      = "";
        	paramObject.step               = rt.getParameter("custscript_clgx2_1957_st");
        	paramObject.action             = rt.getParameter("custscript_clgx2_1957_at");
        	paramObject.fileID             = parseInt(rt.getParameter("custscript_clgx2_1957_fid"));
        	paramObject.initialIndex       = parseInt(rt.getParameter("custscript_clgx2_1957_idx"));
        	paramObject.currentIndex       = 0;
        	paramObject.pageSize           = parseInt(rt.getParameter("custscript_clgx2_1957_ps"));
        	paramObject.environment        = rt.getParameter("custscript_clgx2_1957_environment");
        	paramObject.recordType         = rt.getParameter("custscript_clgx2_1957_recordtype");
        	paramObject.dateField          = rt.getParameter("custscript_clgx2_1957_datefield");
        	paramObject.startDate          = rt.getParameter("custscript_clgx2_1957_startdate");
        	paramObject.endDate            = rt.getParameter("custscript_clgx2_1957_enddate");
        	paramObject.employeeID         = parseInt(rt.getParameter("custscript_clgx2_1957_eid"));
        	paramObject.continueProcessing = rt.getParameter("custscript_clgx2_1957_continue") == true ? "T" : "F";
        	
        	if(paramObject.step == "delete" && paramObject.action == "delete") {
        		paramObject.savedSearchID = "customsearch_clgx_dwmu_queue_reset";
    			var reschedule            = deleteQueueRecords(paramObject);
    			
    			if(reschedule) {
    				lib.scheduleScript({
    					scriptId: runtime.getCurrentScript().id,
    					params: {
    		    			custscript_clgx2_1957_st         : paramObject.step,
    		    			custscript_clgx2_1957_at         : paramObject.action,
    		    			custscript_clgx2_1957_fid        : paramObject.fileID,
    		    			custscript_clgx2_1957_idx        : paramObject.currentIndex,
    		    			custscript_clgx2_1957_ps         : paramObject.pageSize,
    		    			custscript_clgx2_1957_environment: paramObject.environment,
    		    			custscript_clgx2_1957_recordtype : paramObject.recordType,
    		    			custscript_clgx2_1957_datefield  : paramObject.dateField,
    		    			custscript_clgx2_1957_startdate  : paramObject.startDate,
    		    			custscript_clgx2_1957_enddate    : paramObject.endDate,
    		    			custscript_clgx2_1957_continue   : paramObject.continueProcessing,
    						custscript_clgx2_1957_eid        : paramObject.employeeID
    		    		}
    				});
    			}
        	}
        	
        	if(paramObject.step == "populate") {
        		if(paramObject.action == "file") {
        			var reschedule = createIDFile(paramObject);
        			
        			log.debug({ title: "paramObject", details: paramObject });
        			
        			if(!reschedule) { 
        				paramObject.currentIndex = 0;
        				paramObject.initialIndex = 0;
        				paramObject.action   = "create";
        			}
        			
        			lib.scheduleScript({
        				scriptId: runtime.getCurrentScript().id,
        				params: {
    		    			custscript_clgx2_1957_st         : paramObject.step,
    		    			custscript_clgx2_1957_at         : paramObject.action,
    		    			custscript_clgx2_1957_fid        : paramObject.fileID,
    		    			custscript_clgx2_1957_idx        : paramObject.currentIndex,
    		    			custscript_clgx2_1957_ps         : paramObject.pageSize,
    		    			custscript_clgx2_1957_environment: paramObject.environment,
    		    			custscript_clgx2_1957_recordtype : paramObject.recordType,
    		    			custscript_clgx2_1957_datefield  : paramObject.dateField,
    		    			custscript_clgx2_1957_startdate  : paramObject.startDate,
    		    			custscript_clgx2_1957_enddate    : paramObject.endDate,
    		    			custscript_clgx2_1957_continue   : paramObject.continueProcessing,
    						custscript_clgx2_1957_eid        : paramObject.employeeID
    		    		}
        			});
        		} else if(paramObject.action == "create") {
        			var reschedule            = createQueueRecords(paramObject);
        			
        			if(!reschedule) {
        				if(paramObject.continueProcessing) {
        					lib.scheduleScript({
        						scriptId: "1309",
        						deploymentId: "customdeploy_clgx2_ss_dw_mass_update"
        					}); //Process record queue.
        				}
        			} else {
        				lib.scheduleScript({
        					scriptId: runtime.getCurrentScript().id,
        					params: {
        		    			custscript_clgx2_1957_st         : paramObject.step,
        		    			custscript_clgx2_1957_at         : paramObject.action,
        		    			custscript_clgx2_1957_fid        : paramObject.fileID,
        		    			custscript_clgx2_1957_idx        : paramObject.currentIndex,
        		    			custscript_clgx2_1957_ps         : paramObject.pageSize,
        		    			custscript_clgx2_1957_environment: paramObject.environment,
        		    			custscript_clgx2_1957_recordtype : paramObject.recordType,
        		    			custscript_clgx2_1957_datefield  : paramObject.dateField,
        		    			custscript_clgx2_1957_startdate  : paramObject.startDate,
        		    			custscript_clgx2_1957_enddate    : paramObject.endDate,
        		    			custscript_clgx2_1957_continue   : paramObject.continueProcessing,
        						custscript_clgx2_1957_eid        : paramObject.employeeID
        		    		}
        				});
        			}
        		}
        	}
        	
        	/*if(paramObject.step == "reset") {
        		if(paramObject.action == "delete") {
        			var reschedule            = deleteQueueRecords(paramObject);
        			
        			if(!reschedule) {
        				paramObject.currentIndex = 0;
        				paramObject.initialIndex = 0;
        				paramObject.action   = "file"; 
        			}
        			
        			scheduleScript(paramObject);
        		} else if(paramObject.action == "file") {
        			var reschedule            = createIDFile(paramObject);
        			
        			if(!reschedule) {
        				paramObject.currentIndex = 0;
        				paramObject.initialIndex = 0;
        				paramObject.action   = "create";
        			}
        			
        			scheduleScript(paramObject);
        		} else if(paramObject.action == "create") {
        			var reschedule = createQueueRecords(paramObject);
        			if(reschedule) {
        				scheduleScript(paramObject);
        			}
        		}
        	}*/
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }

    
    /**
     * Handles creating a json file which acts as an adhoc stack.
     * 
     * @access private
     * @function createIDFile
     * 
     * @param {Object} paramObject
     * 
     * @returns {Boolean}
     */
    function createIDFile(paramObject) {
    	try {
    		var recordIDArray   = new Array();
        	
        	var processResults  = null;
        	var existingContent = "";
        	var globalIndex     = 0;
        	
        	var filters         = new Array();
        	var columns         = new Array();
        	var ids             = new Array();
        	
        	columns.push(search.createColumn("internalid"));
        	
        	if(paramObject.dateField && paramObject.startDate) {
        		var operator = "";
        		var value    = "";
        		var name     = "";
        		
        		var startDate = moment(paramObject.startDate).format("M/d/YY h:mm a");
        		var endDate   = moment(paramObject.endDate).format("M/d/YY h:mm a");
        		
        		log.debug({ title: "startDate", details: startDate });
        		log.debug({ title: "endDate", details: endDate });
        		
        		if(paramObject.endDate) {
        			operator = "between";
        			value    = [moment(paramObject.startDate).format("M/d/YY h:mm a"), moment(paramObject.endDate).format("M/d/YY h:mm a")];
        		} else {
        			operator = "onorafter";
        			value    = [format.format({ value: paramObject.startDate, type: format.Type.DATETIME })];
        		}
        		
        		var fieldTypes = lib.getFieldTypes({ recordType: paramObject.recordType });
        		
        		if(fieldTypes.length > 0) {
        			if(paramObject.dateField == 1) {
            			name = fieldTypes[0].id; //Last Modified Date
            		} else if(paramObject.dateField == 2) {
            			name = fieldTypes[1].id; //Date Created
            		}
            		
            		filters.push(search.createFilter({ name: name, operator: operator, values: [value] }));
        		}
        		
        		if(paramObject.recordType == "customer") {
        			filters.push(search.createFilter({ name: "isjob", operator: "is", values: ["F"] }));
        		} else if(paramObject.recordType == "service") {
        			filters.push(search.createFilter({ name: "isjob", operator: "is", values: ["T"] }));
        		}
        	}
        	
        	var searchObject  = search.create({ type: paramObject.recordType, columns: columns, filters: filters }).run();
        	var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
        	
        	processResults = processResultSet(paramObject, searchResults);
        	
        	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
        		recordIDArray = processResults.ids;
        	} else {
        		var existingFile = file.load({ id: paramObject.fileID });
        		existingContent  = JSON.parse(existingFile.getContents());
        		recordIDArray    = recordIDArray.concat(existingContent).concat(processResults.ids);
        	}
        	
        	while (processResults.nextPage == true) {
        		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
        		processResults = processResultSet(paramObject, searchResults);
        		recordIDArray  = recordIDArray.concat(processResults.ids);
        	}
        	
        	log.debug({ title: "recordIDArray", details: recordIDArray.length });
        	
        	var fileObject = file.create({ 
    			name: "records.json", 
    			fileType: file.Type.PLAINTEXT,
    			folder: 18140592,
    			contents: JSON.stringify(recordIDArray)
    		});
        	
        	paramObject.fileID = fileObject.save();
        	
        	return processResults.reschedule;
    	} catch(ex) {
    		log.debug({ title: "createIDFile - Error", details: ex });
    	}
    }
    
    
    /**
     * Processes each page of results.
     */
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var ids          = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		ids.push({ id: parseInt(searchResults[r].getValue(searchResults[r].columns[0])), type: paramObject.recordType });
    		
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }
    
    
    /**
     * Loads a JSON file from the file cabinet and adds the records to the record processing queue.
     * 
     * @access private
     * @function createQueueRecords
     * 
     * @param {Object} paramObject
     * 
     * @returns {Boolean}
     */
    function createQueueRecords(paramObject) {
    	try {
    		var usage    = runtime.getCurrentScript().getRemainingUsage();
    		
    		var idFile   = file.load({ id: paramObject.fileID });
    		var ids      = JSON.parse(idFile.getContents());
    		var idLength = ids.length;
    		
    		paramObject.currentIndex = paramObject.initialIndex;
    		
    		for(var i = parseInt(paramObject.initialIndex); i < idLength; i++) { 
    			var q = record.create({ type: "customrecord_clgx_dw_mu_queue" });
        		q.setValue({ fieldId: "custrecord_clgx_muq_rid",         value: parseInt(ids[i].id)  });
        		q.setValue({ fieldId: "custrecord_clgx_muq_record_type", value: ids[i].type });
        		q.setValue({ fieldId: "custrecord_clgx_muq_processed",   value: false });
        		q.save({ ignoreMandatoryFields: false, enableSourcing: true });
        		
        		paramObject.currentIndex = paramObject.currentIndex + 1;
        		
        		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
        			break;
        		}
    		}
    		
    		if(usage <= 200) {
    			return true; //Reschedule
    		}
    	} catch(ex) {
    		log.debug({ title: "createQueueRecords - Error", details: ex });
    	}
    	
    	return false;
    }
    
    
    /**
     * Loads a saved search and deletes the specified records from the data warehouse record queue.
     * 
     * @access private
     * @function deleteQueueRecords
     * 
     * @param {ParamObejct} paramObject
     * 
     * @return {Boolean}
     */
    function deleteQueueRecords(paramObject) {
    	try {
    		var usage        = runtime.getCurrentScript().getRemainingUsage();
    		
    		var searchObject = search.load({ id: paramObject.savedSearchID });
    		searchObject.run().each(function(result) {    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
        			return false;
        		}

    			record.delete({ type: "customrecord_clgx_dw_mu_queue", id: result.getValue(result.columns[0]) }); //Update the cancelled checkbox with submitFields.
    			
    			return true;
    		});
    		
    		if(usage <= 200) {
    			return true; //Reschedule
    		}
    	} catch(ex) {
    		log.debug({ title: "deleteQueueRecords - Error", details: ex });
    	}
    	
    	return false;
    }
    
    return {
        execute: execute
    };
    
});
