/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/ui/serverWidget", "/SuiteScripts/clgx/dw/mass_update/lib/CLGX2_LIB_MassUpdate", 
	"/SuiteScripts/clgx/libraries/lodash.min", "N/task", "N/runtime"],
function(userInterface, lib, _, task, runtime) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	try {
    		if(context.request.method == "GET") {
    			var returnValue = null;
    			var formObject  = userInterface.createForm({ title: "Data Warehouse Mass Update" });
    			formObject.clientScriptModulePath = "/SuiteScripts/clgx/dw/mass_update/CLGX2_CL_UI_MassUpdate.js";
    			
    			var scriptRunning = lib.isScriptInstanceRunning("customsearch_clgx2_faq_script_running", [
					{ "name": "name", "join": "script", "operator": "contains", "value": "CLGX2_SS_MassUpdateProcessing" }
				]);
    			
    			if(scriptRunning) {
    				returnValue = "<html><head><title>Mass Update Already Running</title></head><body><div align='center' valign='center'><b>A mass update is already running.</b></div></body></html>";
    			} else {
    				var resetQueueCheckboxField = formObject.addField({ id: "custscript_clgx2_reset_queue", type: "checkbox", label: "Reset Queue" });
    				resetQueueCheckboxField.setHelpText({ help: "Clears the entire mass update processing queue.", showInlineForAssistant: true });
    				
    				var syncQueueCheckboxField = formObject.addField({ id: "custscript_clgx2_sync_on_complete", type: "checkbox", label: "Sync on Completion" });
    				syncQueueCheckboxField.setHelpText({ help: "Immediately begins sending records to the data warehouse when all queue records have been created.", showInlineForAssistant: true });
    				
    				var recordDestinationObject      = lib.getRecordDestinations();
        			var recordDestinationFieldObject = formObject.addField({ id: "custscript_clgx2_record_destination", type: "select", label: "Destination" });
        			
        			_.forEach(recordDestinationObject, function(destination) {
    					recordDestinationFieldObject.addSelectOption({ value: destination.id, text: destination.name });
    				});
        			
        			var recordTypesObject     = lib.getRecordTypes();
    				var recordTypeFieldObject = formObject.addField({ id: "custscript_clgx2_record_type", type: "select", label: "Record Type" });
    				
    				recordTypeFieldObject.addSelectOption({ value: "", text: "" });
    				
    				_.forEach(recordTypesObject, function(recordType) {
    					recordTypeFieldObject.addSelectOption({ value: recordType.id, text: recordType.displayname });
    				});
    				
    				
    				var dateFieldObject = formObject.addField({ id: "custscript_clgx2_date_field", type: "select", label: "Date Field" });
    				dateFieldObject.setHelpText({ help: "You may select one of the two options to filter records by date.", showInlineForAssistant: true });
    				
    				dateFieldObject.addSelectOption({ value: 0, text: "" });
    				dateFieldObject.addSelectOption({ value: 1, text: "Last Modified Date" });
    				dateFieldObject.addSelectOption({ value: 2, text: "Date Created" });
    				
    				
    				var dateRangeStartFieldObject = formObject.addField({ id: "custscript_clgx2_start_date", type: "date", label: "Start Date" });
    				var dateRangeEndFieldObject   = formObject.addField({ id: "custscript_clgx2_end_date", type: "date", label: "End Date" });
        			
    				formObject.addSubmitButton({ label: "Submit" });
    				
    				returnValue = formObject;
    			}
    			
    			if(typeof returnValue === "object") {
    				context.response.writePage(returnValue);
    			} else {
    				context.response.write(returnValue);
    			}
    		} else if(context.request.method == "POST") {
    			var parameters = context.request.parameters;
    			
    			var resetQueue  = parameters.custscript_clgx2_reset_queue;
    			if(resetQueue == "T") {
    				resetMassUpdateQueue();
    			} else {
    				processMassUpdate(parameters);
    			}
    			
    			context.response.write("<html><head><title>Mass Update Started</title></head><body><div align='center'><b>Mass Update Started</b></div></body></html>");
    		}
    	} catch(ex) {
    		log.error({ title: "onRequest - Error", details: ex });
    	}
    }
    
    
    /**
     * Schedules a script to begin processing the mass update.
     * 
     * @access private
     * @function processMassUpdate
     * 
     * @params {Object} parameters
     * @return null
     */
    function processMassUpdate(parameters) {
    	var destination    = parameters.custscript_clgx2_record_destination;
		var recordType     = parameters.custscript_clgx2_record_type;
		var dateField      = parameters.custscript_clgx2_date_field || 0;
		var startDate      = parameters.custscript_clgx2_start_date;
		var endDate        = parameters.custscript_clgx2_end_date;
		var syncOnComplete = parameters.custscript_clgx2_sync_on_complete;
		
		lib.scheduleScript({
			scriptId: "2036",
			params: {
				custscript_clgx2_1957_environment: destination,
				custscript_clgx2_1957_recordtype: recordType,
				custscript_clgx2_1957_datefield: dateField,
				custscript_clgx2_1957_startdate: new Date(startDate),
				custscript_clgx2_1957_enddate: new Date(endDate),
				custscript_clgx2_1957_st: "populate",
				custscript_clgx2_1957_at: "file",
				custscript_clgx2_1957_fid: 0,
				custscript_clgx2_1957_continue: syncOnComplete,
				custscript_clgx2_1957_eid: runtime.getCurrentUser().id
			}
		});
    }
    
    /**
     * Schedules a script to clear all records from the mass update queue.
     * 
     * @access private
     * @function processMassUpdate
     * 
     * @return null
     */
    function resetMassUpdateQueue() {
    	try {
    		lib.scheduleScript({
				scriptId: "1957",
				params: {
					custscript_clgx2_1957_st: "delete",
					custscript_clgx2_1957_at: "delete"
				}
			});
    	} catch(ex) {
    		log.error({ title: "resetMassUpdateQueue - Error", details: ex });
    	}
    }

    
    return {
        onRequest: onRequest
    };
    
});
