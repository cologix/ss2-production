/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/20/2018
 * Script File:	CLGX2_SU_DW_RecordChange.js
 * Script Name:	CLGX2_SU_DW_RecordChange
 * Script ID:   customscript_clgx2_su_dw_rc
 * Script Type:	User Event
 * 
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

define(["N/record", "N/runtime", "N/search", "N/cache"],
function(record, runtime, search, cache) {
	/**
	 * Checks if the record being saved is currently sitting unprocessed in the queue.
	 * 
	 * @access private
	 * @function recordExistsInQueue
	 * 
	 * @param {number} recordId   - The Internal ID of the current record.
	 * @param {string} recordType - The record type of the current record.
	 * @return {boolean}
	 */
	function recordExistsInQueue(recordId, recordType, eventType) {
		try {
			var s = search.create({ 
				type: "customrecord_clgx_dw_record_queue",
				columns: ["internalid"],
				filters: [["custrecord_clgx_dw_rq_hist", "isnot", "T"], "and",
				["custrecord_clgx_dw_rq_rt", "contains", recordType], "and",
				["custrecord_clgx_dw_rq_et", "contains", eventType], "and",
				["custrecord_clgx_dw_rq_rid", "equalto", recordId]]
			});
			var count = 0;
			s.run().each(function(result) { count = count + 1; });
			if(count > 0) {
				log.debug({ title: "recordExistsInQueue", details: "Skipping " + recordType + " (" + recordId + ")" + ". Record exists and has not been processed." });
				return true; 
			}
		} catch(ex) {
			log.error({ title: "recordExistsInQueue Error", details: ex });
		}
		
		return false;
	}
	
	
	/**
	 * Adds the current record to the queue.
	 * 
	 * @access private
	 * @function addRecordToQueue
	 * 
	 * @param {number} recordID - The record internalid
	 * @param {string} recordType - The internal name of the record in netsuite.
	 * @param {string} eventType - The action being performed on the record.
	 * @return undefined
	 */
	function addRecordToQueue(recordID, recordType, eventType) {
		try {
			var queue = record.create({ type: "customrecord_clgx_dw_record_queue" });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: recordID                              });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: parseInt(runtime.getCurrentUser().id) });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: recordType                            });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: eventType                             });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: false                                 });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_error", 	 value: 0                                     });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_hist", 	 value: false                                 });
			var queueID = queue.save();
			
			
			log.debug({ title: "addRecordToQueue", details: "Added " + recordType + " (" + recordID + ")" + " to queue." });
		} catch(ex) {
			log.error({ title: "addRecordToQueue Error", details: ex });
		}
	}
	
	
	/**
	 * Before Submit - Entry Point
	 * 
	 * @access public
	 * @function beforeSubmit
	 * 
	 * @param {object} context
	 * @return undefined
	 */
	function beforeSubmit(context) {
		if(runtime.envType === runtime.EnvType.PRODUCTION) {
			var cacheObject = cache.getCache({ name: "CLGX_1279", scope: cache.Scope.PRIVATE });
			
			var recordID   = context.newRecord.id;
			var recordType = context.newRecord.type;
			var eventType  = context.type;
			
			switch(eventType) {
			case context.UserEventType.DELETE:
				if(recordExistsInQueue(context.newRecord.id, context.newRecord.type, context.type) == false) {
            		addRecordToQueue(context.newRecord.id, context.newRecord.type, context.type);
            	}
				
				cacheObject.put({ key: "CLGX_1279_PROCESS", value: "false" });
				break;
			default:
				cacheObject.put({ key: "CLGX_1279_PROCESS", value: "true" });
			}
		}
	}
	
	
	/**
	 * After Submit - Entry Point
	 * 
	 * @access public
	 * @function afterSubmit
	 * 
	 * @param {object} context
	 * @return undefined
	 */
    function afterSubmit(context) {
    	log.debug({ title: "Context Test", details: context });
    	
    	if(runtime.envType === runtime.EnvType.PRODUCTION) {
    		var cacheObject = cache.getCache({ name: "CLGX_1279", scope: cache.Scope.PRIVATE });
    		var process     = cacheObject.get({ key: "CLGX_1279_PROCESS"});
    		
    		if(process == "true") {
    			var recordID   = context.newRecord.id;
    			
    			var recordType = context.newRecord.type;
    			var eventType  = context.type;
    			
    			switch(eventType) {
    			case context.UserEventType.CREATE:
    				switch(recordType) {
    				case "estimate":
    					if(recordExistsInQueue(context.newRecord.getValue("opportunity"), "opportunity", eventType) == false) {
                    		addRecordToQueue(context.newRecord.getValue("opportunity"), "opportunity", eventType);
                    	}
    					
    					if(recordExistsInQueue(recordID, recordType, eventType) == false) {
    						addRecordToQueue(recordID, recordType, eventType);
    					}
    					break;
    				default:
    					if(recordExistsInQueue(recordID, recordType, eventType) == false) { 
    						addRecordToQueue(recordID, recordType, eventType);
    					}
    				}
    				break;
    			case context.UserEventType.EDIT:
    				if(recordExistsInQueue(recordID, recordType, eventType) == false) {
    					addRecordToQueue(recordID, recordType, eventType);
    				}
    				break;
    			case context.UserEventType.XEDIT:
    				if(recordExistsInQueue(recordID, recordType, eventType) == false) {
    					addRecordToQueue(recordID, recordType, eventType);
    				}
    				break;
    			}
    		}
    		
    		cacheObject.remove({ key: "CLGX_1279_PROCESS" });
    	}
    }

    return {
    	beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
