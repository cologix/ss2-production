/**
 * @NApiVersion 2.x
 * @NScriptType workflowactionscript
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 8/29/2018
 */
define(["N/record"],
function(record) {
    function onAction(scriptContext) {
    	var id = scriptContext.newRecord.id;
    	var type = scriptContext.newRecord.type;
    	
    	log.debug({ title: "record", details: '| id: ' + id + ' | type: ' + type + ' |'});

		try {
	    	var rec = record.create({ type: "customrecord_clgx_dw_record_queue" });
	    	rec.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: parseInt(id) });
	    	rec.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: type });
	    	rec.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: 'edit' });
	    	rec.save({ ignoreMandatoryFields: false, enableSourcing: false });
		}
		catch (error) {
			log.debug({ title: "error", details: error });
		}
    }

    return {
        onAction : onAction
    };
    
});
