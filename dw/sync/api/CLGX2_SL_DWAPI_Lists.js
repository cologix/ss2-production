/**
 * 
 * @author      Dan Tansanu - dan.tansanu@cologix.com
 * @date        12/15/2017
 * @description A REST API implementation for CRUD operations within the data warehouse. 
 * 
 * Script Name  : CLGX2_SL_DWAPI_Lists
 * Script File  : CLGX2_SL_DWAPI_Lists.js
 * Script ID    : customscript_clgx2_sl_dwapi_lists
 * Deployment ID: customscript_clgx2_sl_dwapi_lists
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1443&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
	"N/record", 
	"N/search",  
	"N/runtime",
	"N/https",
	"N/file",
	"N/task",
	"/SuiteScripts/clgx/libraries/moment.min"
],
function(record, search, runtime, https, file, task, moment) {
	/**
	 * Entry point for the "get" logic.
	 * 
	 * @access private
	 * @function onGet
	 * 
	 * @param {string} listName - The name of the NetSuite list.
	 * @returns {object}
	 */
	function onGet(listName) {
		return getList(listName)
	}
	
	
	/**
	 * Entry point for the "create" logic.
	 * 
	 * @access private
	 * @function onCreate
	 * 
	 * @param {string} listName - The name of the NetSuite list.
	 * @returns {object}
	 */
	function onCreate(listName) {
		scheduleListTask(listName);
		return "List has been scheduled to update.";
	}
	
	
	/**
	 * Returns a JSON list based on the list type.
	 * 
	 * @access private
	 * @function getList
	 * 
	 * @param {string} listName - The name of the NetSuite list.
	 * @returns {string}
	 */
	function getList(listName) {
		var fileid = 7109881;
		
		switch(listName) {
	    case 'geography':
	    	fileid = 7109881;
	        break;
	    case 'entities':
	    	fileid = 7109981;
	        break;
	    case 'items':
	    	fileid = 7310244;
	        break;
	    case 'account':
	    	fileid = 7309488;
	        break;
	    case 'accounting':
	    	fileid = 7109783;
	        break;
	    case 'transactions':
	    	fileid = 7109982;
	        break;
	    case 'xcs':
	    	fileid = 8377523;
	        break;
	    case 'powers':
	    	fileid = 8357729;
	        break;
	    case 'spaces':
	    	fileid = 8340509;
	        break;
	    case 'equipments':
	    	fileid = 8332982;
	        break;
	    case 'budgets':
	    	switch(budgetYear) {
	    		case "2010":
	    			fileid = 9052771;
	    			break;
	    		case "2011":
	    			fileid = 9054649;
	    			break;
	    		case "2012":
	    			fileid = 9055050;
	    			break;
	    		case "2013":
	    			fileid = 9054849;
	    			break;
	    		case "2014":
	    			fileid = 9054949;
	    			break;
	    		case "2015":
	    			fileid = 9054650;
	    			break;
	    		case "2016":
	    			fileid = 9053455;
	    			break;
	    		case "2017":
	    			fileid = 9054449;
	    			break;
	    		case "2018":
	    			fileid = 9051548;
	    			break;
	    		default:
	    			fileid = 9051548;
	    	}
	    	
	    	break;
	    case 'crm':
	    	fileid = 9339090;
	    	break;
	    case 'inventory':
	    	fileid = 9388801;
	    	break;
	    case 'churn':
	    	fileid = 9693262;
	    	break;
	    case 'fam':
	    	fileid = 10227448;
	    	break;
	    case 'rate':
	    	fileid = 13388338;
	    	break;
	    case 'crate':
	    	fileid = 13461020;
	    	break;
	    default:
	    	fileid = 7109881;
		}
		
		var fileObj = file.load({
		    id: fileid
		});
		
		return fileObj.getContents();
	}
	
	
	/**
	 * Schedules a script to process a list.
	 * 
	 * @access private 
	 * @function scheduleListTask
	 * 
	 * @param {string} listName - The name of the NetSuite list.
	 * @returns {number}
	 */
	function scheduleListTask(listName) {
		var script = getListDeployment(listName);
		
		try {
			var scriptTask          = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
		    scriptTask.scriptId     = script.script_id;
		    scriptTask.deploymentId = script.deployment_id;
		    var scriptTaskId        = scriptTask.submit();
		} catch(ex) {
			log.debug({ title: "CLGX2_SL_DW_Lists - Error", details: ex });
		}
	}
	
	
	/**
	 * Returns the script deployment id based on the list.
	 * 
	 * @access private
	 * @function getListDeployment
	 * 
	 * @param {string} listName - The name of the NetSuite list.
	 * @returns {string}
	 */
	function getListDeployment(listName) {
		var returnObject = {};
		
		if(listName == "geography") {
			returnObject = {
				script_id: 1307,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_geography"
			};
		} else if(listName == "entities") {
			returnObject = {
				script_id: 1308,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_entities"
			};
		} else if(listName == "items") {
			returnObject = {
				script_id: 1338,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_items"
			};
		} else if(listName == "account") {
			returnObject = {
				script_id: 1337,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_account"
			};
		} else if(listName == "accounting") {
			returnObject = {
				script_id: 1310,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_accounting"
			};
		} else if(listName == "transactions") {
			returnObject = {
				script_id: 1311,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_transact"
			};
		} else if(listName == "xcs") {
			returnObject = {
				script_id: 1442,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_xcs"
			};
		} else if(listName == "powers") {
			returnObject = {
				script_id: 1440,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_powers"
			};
		} else if(listName == "spaces") {
			returnObject = {
				script_id: 1439,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_spaces"
			};
		} else if(listName == "equipments") {
			returnObject = {
				script_id: 1437,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_equipments"
			};
		} else if(listName == "budgets") {
			returnObject = {
				script_id: 1454,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_budget2018"
			};
		} else if(listName == "rate") {
			returnObject = {
				script_id: 1637,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_exr"
			};
		} else if(listName == "crate") {
			returnObject = {
					script_id: 1648,
					deployment_id: "customdeploy_clgx2_ss_dw_sync_cfxr"
				};
			}
		else if(listName == "crm") {
			returnObject = {
				script_id: 1477,
				deployment_id: "customdeploy_clgx_ss_dw_sync_crm"
			};
		} else if(listName == "inventory") {
			returnObject = {
				script_id: 1478,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_inventory"
			};
		} else if(listName == "churn") {
			returnObject = {
				script_id: 1487,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_churn"
			};
		} else if(listName == "fam") {
			returnObject = {
				script_id: 1488,
				deployment_id: "customdeploy_clgx2_ss_dw_sync_fam"
			};
		}
		
		return returnObject;
	}
	
	
    function onRequest(context) {
    	var functionArray = [];
    	var response      = context.response;
		var request       = context.request;
		
		var lists         = request.parameters.lists || "geography";
		var budgetYear    = request.parameters.year  || "2018";
		var listEvent     = request.parameters.event || "get";
		
    	functionArray["get"]    = onGet;
    	functionArray["create"] = onCreate;
    	
		response.write(functionArray[listEvent](lists));
    }
    
    return {
        onRequest: onRequest
    };
});
