/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 10/25/2017
 * 
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */

define(["N/record", "N/search", "N/runtime",
	    "/SuiteScripts/clgx/libraries/moment.min", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", 
	    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_CreditMemo", 
	    "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Contact", 
	    "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Customer", 
	    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Invoice", 
	    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Opportunity", 
	    "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Partner", 
	    "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_PortalUser", 
	    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Proposal", 
	    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_SalesOrder", 
	    "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Vendor",
	    "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Service",
	    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_JournalEntry"],
function(record, search, runtime, moment, dwg, dwcm, dwco, dwcu, dwi, dwo, dwpa, dwpr, dwpu, dwso, dwv, dwje) {
	function getRecordJSON(recordType, recordID, recordEvent, userID, date, environment) {
		var json = {
		   "response"  : "",
		   "proc"      : "CLGX_DW_SYNC_" + recordType.toUpperCase(),
		   "enviro"    : environment,
		   "user_id"   : userID,
		   "source_id" : 1,
		   "datetime"  : moment().format('YYYY-MM-DD H:mm:ss'),
		   "event"     : recordEvent,
		   "rec_type"  : recordType,
		   "record_id" : recordID,
		   "usage"     : 0,
		   "record"    : []
		};
		
		if(recordEvent != "delete") {
			if(recordType == "vendor") {
				json.record = dwv.get_vendor(recordID);
			} 
			
			if(recordType == "partner") {
				json.record = dwpa.get_partner(recordID);
			}
				
			if(recordType == "customer") {
				json.record = dwcu.get_customer(recordID);
			}
				
			if(recordType == "contact") {
				json.record = dwco.get_contact(recordID);
			}
				
			if(recordType == "portal_user") {
				json.record = dwpu.get_portal_user(recordID);
			}
				
			if(recordType == "service") {
				json.record = dwse.get_service(recordID);
			}
				
			if(recordType == "opportunity") {
				json.record = dwo.get_opportunity(recordID);
			}
				
			if(recordType == "estimate") {
				json.record = dwpr.get_proposal(recordID);
			}
				
			if(recordType == "salesorder") {
				json.record = dwso.get_salesorder(recordID);
			}
				
			if(recordType == "invoice") {
				json.record = dwi.get_invoice(recordID);
			}
				
			if(recordType == "cinvoice") {
				json.record = dwi.get_cinvoice(recordID);
			}
				
			if(recordType == "creditmemo") {
				json.record = dwcm.get_creditmemo(recordID);
			}
		}
		
		return json;
	}
	
	
	/**
	 * Posts a JSON object to the data warehouse.
	 * 
	 * @param {string} dwUrl
	 * @param {boolean} post_flags
	 * @param {object} json
	 * @return {object}
	 */
	function postToDW(dwUrl, post_flags, json) {
		if(dwUrl != null) {
			var processed = "T";
			var error = 0;
			
			try {
	        	var requestURL = https.post({
	        	    url: dwUrl,
	        	    body: JSON.stringify(json)
	        	});
	        	
		    	json.response = JSON.parse(requestURL.body);
		    	error = parseInt(json.response.ERROR);
	    	} catch (error) {
	    		log.error({ title: "Sync " + json.rec_type, details: " | recordID - " + json.record_id + " | Post Error |" });
	    		processed = "F";
	    		error = -2;
	    	}
	    	
	    	if(post_flags == true) {
	    		try {
		    		dwg.set_queue_flags(rec_type, record_id, processed, error);
		    	} catch (error) {
		    		log.error({ title: "Sync " + json.rec_type, details: " | recordID - " + json.record_id + " | Record Error |" });
		    		error = -3;
		    	}
	    	}

			var scriptObj = runtime.getCurrentScript();
			var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
			
			json.usage = usage;
			
			return json;
		}
	}
	
	
    function getInputData(context) {
    	var ro = runtime.getCurrentScript();
    	
    	if(context.isRestarted == true) {
    		log.audit({ title: "CLGX2_MR_DW_Sync_Queue - getInputData", details: "Input Data Restarting" });
    	}
    	
    	return search.load({type: ro.getParameter({ name: "custscript_clgx2_dws_srt" }), id: ro.getParameter({ name: "custscript_clgx2_dws_ss" }) });
    }
    
    function map(context) {
    	if(context.isRestarted == true) {
    		log.audit({ title: "CLGX2_MR_DW_Sync_Queue - Map", details: "Map Restarting" });
    	}
    	
    	var cc                 = JSON.parse(context.value);
    	var finalObject        = new Object();
    	finalObject.id         = cc.id;
    	finalObject.recordType = cc.values.custrecord_clgx_dw_rq_rt;
    	finalObject.recordID   = cc.values.custrecord_clgx_dw_rq_rid;
    	finalObject.eventType  = cc.values.custrecord_clgx_dw_rq_et;
    	finalObject.userID     = cc.values.custrecord_clgx_dw_rq_uid;
    	finalObject.recordDate = cc.values.custrecord_clgx_dw_rq_date;
    	
    	context.write(context.key, finalObject);
    }

    function reduce(context) {
    	if(context.isRestarted == true) {
    		log.audit({ title: "CLGX2_MR_DW_Sync_Queue - Reduce", details: "Reduce Restarting" });
    	}
    	
    	var ro = runtime.getCurrentScript();
    	var cr = JSON.parse(context.values[0]);
    	//log.debug({ title: "reduce",        details: cr            });
    	//log.debug({ title: "cr.recordType", details: cr.recordType });
    	//log.debug({ title: "cr.recordID",   details: cr.recordID   });
    	//log.debug({ title: "cr.eventType",  details: cr.eventType  });
    	//log.debug({ title: "cr.userID",     details: cr.userID     });
    	//log.debug({ title: "cr.recordDate", details: cr.recordDate });

    	var json = getRecordJSON(cr.recordType, cr.recordID, cr.eventType, cr.userID, cr.recordDate, "dev");
    	postToDW("https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm", ro.getParameter({ name: "custscript_clgx2_dws_pf" }), json);
    }
    
    function summarize(context) {
    	if(context.isRestarted == true) {
    		log.audit({ title: "CLGX2_MR_DW_Sync_Queue - Summarize", details: "Summarize Restarting" });
    	}
    	
    	var totalRecords = 0;
    	context.output.iterator().each(function(key, value) {
    		totalRecords = totalRecords + parseInt(value);
    		return true;
    	});
    	
    	log.audit({ title: "CLGX2_MR_DW_Sync_Queue - Summarize", details: "Total Records Updated: " + totalRecords });
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
