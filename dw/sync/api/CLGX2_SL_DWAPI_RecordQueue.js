/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   3/6/2018
 */
define(["N/ui/serverWidget", "N/task", "N/runtime", "N/http"],
function(ui, task, runtime, http) {
    function onRequest(context) {
    	var execution = new Array();
    	execution[http.Method.GET] = onGet;
    	execution[http.Method.POST] = onPost;
    	
    	context.response.writePage(execution[context.request.method]());
    }

    
    function onGet() {
    	var form = ui.createForm({ title: "Cologix DWAPI - Queue Population" });
    	var recordType = form.addField({ id: "recordtype", type: ui.FieldType.SELECT, label: "Record Type" });
    	recordType.addSelectOption({ value: "-1",            text: "Please select a record type" });
    	recordType.addSelectOption({ value: "case",          text: "Case" });
    	recordType.addSelectOption({ value: "contact",       text: "Contact" });
    	recordType.addSelectOption({ value: "cinvoice",      text: "CInvoice" });
    	recordType.addSelectOption({ value: "creditmemo",    text: "Credit Memo" });
    	recordType.addSelectOption({ value: "customer",      text: "Customer" });
    	recordType.addSelectOption({ value: "device",        text: "Device" });
    	recordType.addSelectOption({ value: "equipment",     text: "Equipment" });
    	recordType.addSelectOption({ value: "fam",           text: "FAM" });
    	recordType.addSelectOption({ value: "forecast",      text: "Forecast" });
    	recordType.addSelectOption({ value: "invoice",       text: "Invoice" });
    	recordType.addSelectOption({ value: "journalentry",  text: "Journal Entry" });
    	recordType.addSelectOption({ value: "opportunity",   text: "Opportunity" });
    	recordType.addSelectOption({ value: "partner",       text: "Partner" });
    	recordType.addSelectOption({ value: "payment",       text: "Payment" });
    	recordType.addSelectOption({ value: "portal_user",   text: "Portal User" });
    	recordType.addSelectOption({ value: "power",         text: "Power" });
    	recordType.addSelectOption({ value: "project",       text: "Project" });
    	recordType.addSelectOption({ value: "proposal",      text: "Proposal" });
    	recordType.addSelectOption({ value: "purchaseorder", text: "Purchase Order" });
    	recordType.addSelectOption({ value: "salesorder",    text: "Sales Order" });
    	recordType.addSelectOption({ value: "service",       text: "Service" });
    	recordType.addSelectOption({ value: "space",         text: "Space" });
    	recordType.addSelectOption({ value: "vendor",        text: "Vendor" });
    	recordType.addSelectOption({ value: "vendorbill",    text: "Vendor Bill" });
    	recordType.addSelectOption({ value: "vendorcredit",  text: "Vendor Credit" });
    	recordType.addSelectOption({ value: "vendorpayment", text: "Vendor Payment" });
    	recordType.addSelectOption({ value: "xc",            text: "Cross Connect" });
    	recordType.addSelectOption({ value: "vxc",           text: "Virtual Cross Connect" });
    	
    	form.addField({ id: "createfile", type: ui.FieldType.CHECKBOX, label: "Create File" });
    	
    	return form;
    }
    
    function onPost() {
    	
    }
    
    function getRecordInfo(record_type) {
    	
    }
    
    return {
        onRequest: onRequest
    };
    
});
