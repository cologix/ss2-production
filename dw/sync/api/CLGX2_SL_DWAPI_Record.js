/**
 *
 * @author      Alex Guzenski - alex.guzenski@cologix.com
 * @date        10/15/2017
 * @description A REST API implementation for CRUD operations within the data warehouse.
 *
 * Script Name  : CLGX2_SL_DWAPI_Record
 * Script File  : CLGX2_SL_DWAPI_Record.js
 * Script ID    : customscript_clgx2_sl_dwapi_record
 * Deployment ID: customscript_clgx2_sl_dwapi_record
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1344&deploy=1
 *
 * Parameters:
 * event (string): The type of operation to be performed on the database. This can be get, create, or delete.
 * type (string) : The name of the record being accessed.
 * id (integer)  : The Internal ID of the record being accessed.
 *
 *
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define(["N/https", "N/record", "N/runtime", "N/search",
        "/SuiteScripts/clgx/libraries/underscore-min",
        "/SuiteScripts/clgx/libraries/moment.min",
        "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global",

        "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Vendor",
        "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Partner",
        "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Customer",
        "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Contact",

        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Opportunity",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Proposal",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_SalesOrder",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Invoice",
        "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Service",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_CreditMemo",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_JournalEntry",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_PurchaseOrder",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Payment",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_CustomerRefund",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_VendorBill",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_VendorPayment",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_VendorCredit",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Forecast",

        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Equipment",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Space",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Power",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_FAM",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Device",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_XC",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_VXC",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_OutOfBand",

        "/SuiteScripts/clgx/dw/sync/lib/crm/CLGX2_LIB_DW_Case",
        "/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Group",
        "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Churn",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_ManagedService",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_CustomerKWPeak",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Device_Capacity"
    ],
    function(https, record, runtime, search, _, moment, dwg, dwv, dwpa, dwcu, dwco, dwop, dwpr, dwso, dwin, dwse, dwcm, dwje, dwpo, dwcp, dwcr, dwvb, dwvp, dwvc, dwfc, dweq, dwsp, dwpw, dwfm, dwdv, dwxc, dwvxc, dwob, dwca, dweg, dwch, dwms, dwckwp, dwdc) {

        function get_record(recordType, recordID, eventType, userID, date, environment) {

            var procedureName = recordType;

            if(recordType == 'estimate'){
                recordType = 'proposal';
            }
            if(recordType == 'job'){
                recordType = 'service';
                try{
                    var rec = record.load({ type: 'JOB', id: recordID });
                    var isproj = parseInt(rec.getValue({ fieldId: "custentity_cologix_int_prj" }));
                    if(isproj == 1){
                        recordType = 'project';
                    }
                } catch (error) {

                }
            }

            if(recordType == 'customrecord_clgx_consolidated_invoices'){
                recordType = 'cinvoice';
            }
            if(recordType == 'customrecord_clgx_cap_bud_forecast'){
                recordType = 'forecast';
            }
            if(recordType == 'customrecord_clgx_modifier'){
                recordType = 'portal_user';
            }
            if(recordType == 'customrecord_cologix_equipment'){
                recordType = 'equipment';
            }
            if(recordType == "customrecord_clgx_active_port") {
                recordType    = "port";
                procedureName = "equipment";
            }
            if(recordType == "customrecord_clgx_cpu") {
                recordType    = "cpu";
                procedureName = "equipment";
            }
            if(recordType == "customrecord_clgx_disks") {
                recordType    = "disk";
                procedureName = "equipment";
            }
            if(recordType == "customrecord_clgx_memory") {
                recordType    = "memory";
                procedureName = "equipment";
            }
            if(recordType == 'customrecord_cologix_space'){
                recordType = 'space';
            }
            if(recordType == 'customrecord_clgx_power_circuit'){
                recordType = 'power';
            }
            if(recordType == 'customrecord_clgx_oob_facility'){
                recordType = 'oob';
            }
            if(recordType == 'customrecord_clgx_managed_services') {
                recordType = 'managed_service';
            }
            if(recordType == "fam_asset_type") {
                procedureName = "fam";
            }
            if(recordType == "fam_deprication") {
                procedureName = "fam";
            }
            if(recordType == "fam_repair_category") {
                procedureName = "fam";
            }
            if(recordType == "customrecord_clgx_dcim_customer_peak") {
                recordType = "customer_peak";
            }
            if(recordType == "customrecord_cologix_xcpath") {
                recordType = "xc_path";
            }

            var json = {
                "response"  : "",
                "proc"      : "CLGX_DW_SYNC_" + procedureName.toUpperCase(),
                "enviro"    : environment,
                "user_id"   : userID,
                "source_id" : 1,
                "datetime"  : moment().format('YYYY-MM-DD H:mm:ss'),
                "event"     : eventType,
                "rec_type"  : recordType,
                "record_id" : recordID,
                "usage"     : 0,
                "record"    : []
            };

            try {
                if(recordType == "vendor") {
                    json.record = dwv.get_vendor(recordID);
                }

                if(recordType == "partner") {
                    json.record = dwpa.get_partner(recordID);
                }

                if(recordType == "customer") {
                    json.record = dwcu.get_customer(recordID);
                }

                if(recordType == "contact") {
                    json.record = dwco.get_contact(recordID);
                }

                if(recordType == "portal_user") {
                    json.record = dwco.get_portal_user(recordID);
                }

                if(recordType == "service") {
                    json.record = dwse.get_service(recordID);
                }

                if(recordType == "project") {
                    json.record = dwse.get_project(recordID);
                }

                if(recordType == "opportunity") {
                    json.record = dwop.get_opportunity(recordID);
                }

                if(recordType == "proposal") {
                    json.record = dwpr.get_proposal(recordID);
                }

                if(recordType == "salesorder") {
                    json.record = dwso.get_salesorder(recordID);
                }

                if(recordType == "invoice") {
                    json.record = dwin.get_invoice(recordID);
                }

                if(recordType == "cinvoice") {
                    json.record = dwin.get_cinvoice(recordID);
                }

                if(recordType == "creditmemo") {
                    json.record = dwcm.get_creditmemo(recordID);
                }

                if(recordType == "journalentry") {
                    json.record = dwje.get_journalentry(recordID);
                }

                if(recordType == "job") {
                    json.record = dwse.get_project(recordID);
                }

                if(recordType == "purchaseorder") {
                    json.record = dwpo.get_purchaseorder(recordID);
                }

                if(recordType == "customerpayment") {
                    json.record = dwcp.get_payment(recordID);
                }

                if(recordType == "customerrefund") {
                    json.record = dwcr.get_customerrefund(recordID);
                }

                if(recordType == "vendorbill") {
                    json.record = dwvb.get_vendorbill(recordID);
                }
                if(recordType == 'vendorpayment'){
                    json.record = dwvp.get_vendorpayment(recordID);
                }
                if(recordType == 'vendorcredit'){
                    json.record = dwvc.get_vendorcredit(recordID);
                }
                if(recordType == 'forecast'){
                    json.record = dwfc.get_forecast(recordID);
                }

                if(recordType == "equipment") {
                    json.record = dweq.get_equipment(recordID);
                }
                if(recordType == "space") {
                    json.record = dwsp.get_space(recordID);
                }
                if(recordType == "power") {
                    json.record = dwpw.get_power(recordID);
                }
                if(recordType == "port") {
                    json.record = dweq.get_port(recordID);
                }
                if(recordType == "cpu") {
                    json.record = dweq.get_cpu(recordID);
                }
                if(recordType == "disk") {
                    json.record = dweq.get_disk(recordID);
                }
                if(recordType == "memory") {
                    json.record = dweq.get_memory_record(recordID);
                }

                if(recordType == "fam") {
                    json.record = dwfm.get_fam(recordID);
                }
                if(recordType == "fam_asset_type") {
                    json.record = dwfm.get_fam_asset_type(recordID);
                }
                if(recordType == "fam_deprication") {
                    json.record = dwfm.get_fam_depreciation_method(recordID);
                }
                if(recordType == "fam_repair_category") {
                    json.record = dwfm.get_fam_repair_maintenance_category(recordID);
                }
                if(recordType == "device") {
                    json.record = dwdv.get_device(recordID);
                }
                if(recordType == "device_capacity") {
                    json.record = dwdc.get_device_capacity(recordID);
                }
                if(recordType == "xc") {
                    json.record = dwxc.get_xc(recordID);
                }
                if(recordType == "xc_path") {
                    json.record = dwxc.get_xc_path(recordID);
                }
                if(recordType == "vxc") {
                    json.record = dwvxc.get_vxc(recordID);
                }
                if(recordType == "case") {
                    json.record = dwca.get_case(recordID);
                }
                if(recordType == "oob") {
                    json.record = dwob.get_oob(recordID);
                }
                if(recordType == "group") {
                    json.record = dweg.get_group(recordID);
                }
                if(recordType == "churn") {
                    json.record = dwch.get_churn(recordID);
                }
                if(recordType == "managed_service") {
                    json.record = dwms.get_managed_service(recordID);
                }
                if(recordType == "customer_peak") {
                    json.record = dwckwp.get_kw_peak(recordID);
                }
            } catch(ex) {
                log.debug({ title: "get_record - Error", details: ex });
            }

            return json;
        }

        function post_record(record_id, rec_type, json) {
            var error     = 0;
            var processed = true;
            var dwUrl = runtime.getCurrentScript().getParameter({ name: "custscript_clgx2_dwapi_dwurl" });

            try {
                var requestURL = https.post({
                    url: dwUrl,
                    body: JSON.stringify(json)
                });

                json.response = JSON.parse(requestURL.body);
                error = parseInt(json.response.ERROR);
            } catch (error) {
                log.debug({ title: "Sync " + rec_type, details: ' | record_id - '+ record_id + ' | post error  |' });
                log.debug({ title: "Sync " + rec_type, details: error });
                processed = false;
                error     = -2;
            }
        }

        function executeOnRequest(paramObject) {
            if(paramObject.eventType != null) {
                var json = get_record(paramObject.recordType, paramObject.recordID, paramObject.eventType, paramObject.userID, paramObject.date, paramObject.environment);

                if(paramObject.eventType == "get") {
                    json.usage = (1000 - parseInt(runtime.getCurrentScript().getRemainingUsage()))
                    paramObject.response.write(JSON.stringify(json));
                } else if(paramObject.eventType == "create") {
                    post_record(paramObject.recordID, paramObject.recordType, json);
                    json.usage = (1000 - parseInt(runtime.getCurrentScript().getRemainingUsage()))
                    paramObject.response.write(JSON.stringify(json));
                } else if(paramObject.eventType == "delete") {
                    post_record(paramObject.recordID, paramObject.recordType, json);
                    json.usage = (1000 - parseInt(runtime.getCurrentScript().getRemainingUsage()))
                    paramObject.response.write(JSON.stringify(json));
                }
            }
        }


        function onRequest(context) {
            var ro = {};
            ro.response = context.response;
            ro.request = context.request;

            ro.recordType  = ro.request.parameters.type || null;
            ro.recordID    = parseInt(ro.request.parameters.id) || -1;
            ro.eventType   = ro.request.parameters.event || null;
            ro.userID      = runtime.getCurrentUser().id;
            ro.date        = moment().format('YYYY-MM-DD H:mm:ss');
            ro.environment = ro.request.parameters.enviro || "prod";
            ro.process     = parseInt(ro.request.parameters.process) || 0;
            ro.access      = ro.request.parameters.access || "";

            if((runtime.getCurrentScript().deploymentId == "customdeploy_clgx2_sl_dwapi_record_ol") && (typeof ro.access !== "undefined") && (ro.access === "ADFE145A7A48A90B5DD6D8548B5EB67F8D757458")) {
                executeOnRequest(ro);
            }

            if(runtime.getCurrentScript().deploymentId == "customdeploy_clgx2_sl_dwapi_record") {
                executeOnRequest(ro);
            }
        }

        return {
            onRequest: onRequest
        };
    });

