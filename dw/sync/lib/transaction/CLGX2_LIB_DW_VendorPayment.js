/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_vendorpayment(record_id) {
		
		var vendorpayment     = {};
		try {
			
			var rec  = record.load({ type: record.Type.VENDOR_PAYMENT, id: record_id });
			
			var entity_id = parseInt(rec.getValue({ fieldId: "entity" }));
			var external_type = get_entity_type(entity_id);
			
			var status_id = null;
			var results = search.load({ type: "transaction", id: "customsearch_clgx_dw_vendorpayment_ids" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
			results.run().each(function(result) {
				status_id = result.getValue(result.columns[1]);
		        return true;
			});
			
			var apply = [];
			var lines = rec.getLineCount({sublistId: 'apply'});
			for ( var i = 0; i < lines; i++ ) {
				var applied = dwg.get_bit(rec.getSublistValue({sublistId: 'apply',fieldId: 'apply',line: i}));
				if(applied){
					apply.push({
						"vendor_payment_id" : record_id,
						"line_id"           : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'line',line: i})),
						"tran_id"           : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'internalid',line: i})),
						"tran_type"         : rec.getSublistValue({sublistId: 'apply',fieldId: 'trantype',line: i}),
						"amount"            : parseFloat(rec.getSublistValue({sublistId: 'apply',fieldId: 'amount',line: i}))
					});
				}
			}
			
			var vendorpayment = {
				"vendor_payment_id"    : record_id,
				"external_id"          : rec.getValue({ fieldId: "externalid" }),
				"account_id"           : parseInt(rec.getValue({ fieldId: "account" })),
				"entity_id"            : parseInt(rec.getValue({ fieldId: "entity" })),
				"entity_type"          : external_type,
				"subsidiary_id"        : parseInt(rec.getValue({ fieldId: "subsidiary" })),
				"location_id"          : parseInt(rec.getValue({ fieldId: "location" })),
				"department_id"        : parseInt(rec.getValue({ fieldId: "department" })),
				"status_id"            : dwg.get_null(status_id),
				"class_id"             : parseInt(rec.getValue({ fieldId: "class" })),
				"accounting_period_id" : parseInt(rec.getValue({ fieldId: "postingperiod" })),
				"currency_id"          : parseInt(rec.getValue({ fieldId: "currency" })),
				"exchangerate"         : parseFloat(rec.getValue({ fieldId: "exchangerate" })),
				"balance"              : parseFloat(rec.getValue({ fieldId: "balance" })),
				"total"                : parseFloat(rec.getValue({ fieldId: "total" })),
				"date"                 : dwg.sqlDate(rec.getValue({ fieldId: "trandate" })),
				"check"                : rec.getValue({ fieldId: "tranid" }),
				"memo"                 : rec.getValue({ fieldId: "memo" }),
				"vendor_payment_apply" : apply,
			};
		}
		catch (error) {

		}
		return [vendorpayment];
	}

	function get_entity_type(entity_id) {

		var entityType = '';
		if(entity_id){
			var results     = search.load({ type: 'entity', id: "customsearch_clgx_dw_entity" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: entity_id }));
			results.run().each(function(result) {
				entityType = result.getValue(result.columns[1]);
		        return true;
			});
		}
		return entityType;
	}
	
	return {
		get_vendorpayment: get_vendorpayment
	};
});