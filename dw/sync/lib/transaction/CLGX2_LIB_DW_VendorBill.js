/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_vendorbill(record_id) {
		
		var vendorbill     = {};
		try {
			
			var rec  = record.load({ type: record.Type.VENDOR_BILL, id: record_id });

			var items = [];
			var lines = rec.getLineCount({sublistId: 'item'});
			for ( var i = 0; i < lines; i++ ) {
				var entity_id = parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'customer',line: i}));
				var entity_type = get_entity_type(entity_id);
				
				items.push({
					"vendor_bill_id"     : record_id,
					"line_id"           : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'line',line: i})),
					"item_id"           : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'item',line: i})),
					"class_id"          : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'class',line: i})),
					"department_id"     : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'department',line: i})),
					"location_id"       : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'location',line: i})),
					"entity_id"         : entity_id,
					"entity_type"       : entity_type,
					"project_id"        : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'custcol_clgx_journal_project_id',line: i})),
					"billable"          : dwg.get_bit(rec.getSublistValue({sublistId: 'item',fieldId: 'isbillable',line: i})),
					"no_fam_impact"     : dwg.get_bit(rec.getValue({ fieldId: "custbody_clgx_inservice_entry" })),
					"quantity"          : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'quantity',line: i})),
					"rate"              : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'rate',line: i})),
					"amount"            : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'amount',line: i})),
					"taxcode"           : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'taxcode',line: i})),
					"taxrate1"          : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'taxrate1',line: i})),
					"taxrate2"          : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'taxrate2',line: i})),
					"memo"              : rec.getSublistValue({sublistId: 'item',fieldId: 'description',line: i}),

				});
			}
			
			var expenses = [];
			var lines = rec.getLineCount({sublistId: 'expense'});
			for ( var i = 0; i < lines; i++ ) {
				var entity_id = parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'customer',line: i}));
				var entity_type = get_entity_type(entity_id);
				
				expenses.push({
					"vendor_bill_id"     : record_id,
					"line_id"           : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'line',line: i})),
					"account_id"        : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'account',line: i})),
					"class_id"          : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'class',line: i})),
					"department_id"     : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'department',line: i})),
					"location_id"       : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'location',line: i})),
					"entity_id"         : entity_id,
					"entity_type"       : entity_type,
					"project_id"        : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'custcol_clgx_journal_project_id',line: i})),
					"amount"            : parseFloat(rec.getSublistValue({sublistId: 'expense',fieldId: 'amount',line: i})),
					"memo"              : rec.getSublistValue({sublistId: 'expense',fieldId: 'memo',line: i}),

				});
			}
			
			var pos = [];
			var lines = rec.getLineCount({sublistId: 'purchaseorders'});
			for ( var i = 0; i < lines; i++ ) {
				pos.push({
					"vendor_bill_id"     : record_id,
					"purchase_order_id" : parseInt(rec.getSublistValue({sublistId: 'purchaseorders',fieldId: 'id',line: i})),
					"date"              : dwg.sqlDate(rec.getSublistValue({sublistId: 'purchaseorders',fieldId: 'podate',line: i})),
					"amount"            : parseFloat(rec.getSublistValue({sublistId: 'purchaseorders',fieldId: 'poamount',line: i}))
				});
			}
			
			var reimbursements = [];
			var lines = rec.getLineCount({sublistId: 'reimbursements'});
			for ( var i = 0; i < lines; i++ ) {
				pos.push({
					"vendor_bill_id"     : record_id,
					//"reimbursement_id"  : parseInt(rec.getSublistValue({sublistId: 'reimbursements',fieldId: 'id',line: i})),
					//"date"              : dwg.sqlDate(rec.getSublistValue({sublistId: 'reimbursements',fieldId: 'podate',line: i})),
					//"amount"            : parseFloat(rec.getSublistValue({sublistId: 'reimbursements',fieldId: 'poamount',line: i}))
				});
			}
			
			/*
			var vendorpayments = [];
			var lines = rec.getLineCount({sublistId: 'links'});
			for ( var i = 0; i < lines; i++ ) {
				vendorpayments.push({
					"vendor_bill_id"     : record_id,
					"vendor_payment_id" : parseInt(rec.getSublistValue({sublistId: 'links',fieldId: 'id',line: i})),
					"date"              : dwg.sqlDate(rec.getSublistValue({sublistId: 'links',fieldId: 'trandate',line: i})),
					"amount"            : parseFloat(rec.getSublistValue({sublistId: 'links',fieldId: 'total',line: i})),
					"status"            : rec.getSublistValue({sublistId: 'links',fieldId: 'status',line: i}),
					"type"              : rec.getSublistValue({sublistId: 'links',fieldId: 'type',line: i})
				});
			}
			*/
			
			var status_id = null;
			var results = search.load({ type: "transaction", id: "customsearch_clgx_dw_vendorbill_ids" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
			results.run().each(function(result) {
				status_id = result.getValue(result.columns[1]);
		        return true;
			});
			
			vendorbill = {
				"vendor_bill_id"       : record_id,
				"vendor_bill"          : rec.getValue({ fieldId: "tranid" }),
				"external_id"          : rec.getValue({ fieldId: "externalid" }),
				"vendor_id"            : parseInt(rec.getValue({ fieldId: "entity" })),
				"subsidiary_id"        : parseInt(rec.getValue({ fieldId: "subsidiary" })),
				"location_id"          : parseInt(rec.getValue({ fieldId: "location" })),
				"status_id"            : dwg.get_null(status_id),
				"project_id"           : parseInt(rec.getValue({ fieldId: "custbody_cologix_project_name" })),
				"date"                 : dwg.sqlDate(rec.getValue({ fieldId: "trandate" })),
				"duedate"              : dwg.sqlDate(rec.getValue({ fieldId: "duedate" })),
				"exchangerate"         : parseFloat(rec.getValue({ fieldId: "exchangerate" })),
				"amount"               : parseFloat(rec.getValue({ fieldId: "usertotal" })),
				"discountamount"       : parseFloat(rec.getValue({ fieldId: "discountamount" })),
				"discountdate"         : dwg.sqlDate(rec.getValue({ fieldId: "discountdate" })),
				"no_fam_impact"        : dwg.get_bit(rec.getValue({ fieldId: "custbody_clgx_inservice_entry" })),
				"currency_id"          : parseInt(rec.getValue({ fieldId: "currency" })),
				"payment_terms_id"     : parseInt(rec.getValue({ fieldId: "terms" })),
				"accounting_period_id" : parseInt(rec.getValue({ fieldId: "postingperiod" })),
				"memo"                 : rec.getValue({ fieldId: "memo" }),
				"vendor_bill_items"    : items,
				"vendor_bill_expenses" : expenses,
				//"vendor_bill_reimbursements"       : reimbursements,
				//"vendor_bill_purchase_orders"      : pos,
				//"vendor_payments"      : vendorpayments,
			};
			
		}
		catch (error) {

		}
		
		return [vendorbill];
		
	}

	function get_entity_type(entity_id) {
		var entityType = '';
		if(entity_id){
			var results     = search.load({ type: 'entity', id: "customsearch_clgx_dw_entity" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: entity_id }));
			results.run().each(function(result) {
				entityType = result.getValue(result.columns[1]);
		        return true;
			});
			if(!entityType){
				var results     = search.load({ type: 'job', id: "customsearch_clgx_dw_service" });
				results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: entity_id }));
				results.run().each(function(result) {
					entityType = 'Service';
			        return true;
				});
			}
		}
		return entityType;
	}
	
	return {
		get_vendorbill: get_vendorbill
	};
});