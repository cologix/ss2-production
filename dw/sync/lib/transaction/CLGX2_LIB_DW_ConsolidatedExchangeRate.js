/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com	
 * @date 9/11/2018
 */
define(["N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", "/SuiteScripts/clgx/libraries/lodash.min"],
function(search, dwg, _) {
	function get_consolidated_rates() {
		var period_list     = get_accounting_period_list();
		var currency_list   = get_currency_list();
		var subsidiary_list = get_subsidiary_list();
		
		var rates = new Array();
		var searchObject = search.load({ id: "customsearch_clgx_dw_sync_fx_rates" });
		
		searchObject.run().each(function(result) {
			rates.push({
				"consolidated_rate_id": parseInt(result.getValue(result.columns[0])),
				"accounting_period_id": parseInt(get_accounting_period(period_list, result.getValue(result.columns[1]))),
				"period_start_date"   : result.getValue(result.columns[2]),
				"period_end_date"     : result.getValue(result.columns[3]),
				"period_closed"       : result.getValue(result.columns[4]),
				"from_subsidiary_id"  : parseInt(get_subsidiary(subsidiary_list, result.getValue(result.columns[5]))),
				"from_currency_id"    : parseInt(get_currency(currency_list, result.getValue(result.columns[6]))),
				"to_subsidiary_id"    : parseInt(get_subsidiary(subsidiary_list, result.getValue(result.columns[7]))),
				"to_currency_id"      : parseInt(get_currency(currency_list, result.getValue(result.columns[8]))),
				"current_rate"        : parseFloat(result.getValue(result.columns[9])),
				"average_rate"        : parseFloat(result.getValue(result.columns[10])),
				"historical_rate"     : parseFloat(result.getValue(result.columns[11]))
			});
			
			return true;
		});
		
		return rates;
	}
	
	
	function get_subsidiary_list() {
		var subsidiary = new Array();
		var results = search.load({ type: "subsidiary", id: "customsearch_clgx_dw_subsidiary" });
		results.run().each(function(result) {
			subsidiary.push({
				"subsidiary_id":	parseInt(result.getValue(result.columns[0])),
				"subsidiary":		result.getValue(result.columns[1])
			});
            return true;
		});
		
		return subsidiary;
	}
	
	function get_accounting_period_list() {
		var ap = new Array();
		
		var results = search.load({ type: "accountingperiod", id: "customsearch_clgx_dw_accounting_period" });
		results.run().each(function(result) {
			ap.push({
				"period_id": parseInt(result.getValue(result.columns[0])),
				"period"   : result.getValue(result.columns[1])
			});
			
	        return true;
		});
		
		return ap;
	}
	
	
	function get_currency_list() {
		var currency = new Array();
		var results  = search.create({type: "currency", filters: [], columns: [{"name":"internalid"}, {"name":"name"}] });
		results.run().each(function(result) {
			currency.push({
				"currency_id": parseInt(result.getValue({name: "internalid"})),
				"currency"   : result.getValue({name: "name"})
			});
			
            return true;
		});
		
		return currency;
	}
	
	function get_subsidiary(subsidiary_list, subsidiary_name) {
		if(subsidiary_list) {
			return _.find(subsidiary_list, function(item) {
				return item.subsidiary == subsidiary_name;
			}).subsidiary_id;
		}
	}
	
	function get_currency(currency_list, currency_name) {
		if(currency_list) {
			return _.find(currency_list, function(item) {
				return item.currency == currency_name;
			}).currency_id;
		}
	}
	
	function get_accounting_period(period_list, period_name) {
		if(period_list) {
			return _.find(period_list, function(item) {
				return item.period == period_name;
			}).period_id;
		}
	}
	
	
    return {
    	get_consolidated_rates: get_consolidated_rates
    };
    
});
