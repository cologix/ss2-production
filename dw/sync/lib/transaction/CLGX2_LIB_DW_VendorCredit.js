/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_vendorcredit(record_id) {
		
		var vendorcredit     = {};
		try {

			var rec  = record.load({ type: record.Type.VENDOR_CREDIT, id: record_id });
			
			var items = [];
			var lines = rec.getLineCount({sublistId: 'item'});
			for ( var i = 0; i < lines; i++ ) {
				var entity_id = parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'customer',line: i}));
				var entity_type = get_entity_type(entity_id);
				
				items.push({
					"vendor_credit_id"     : record_id,
					"line_id"           : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'line',line: i})),
					"item_id"           : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'item',line: i})),
					"class_id"          : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'class',line: i})),
					"department_id"     : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'department',line: i})),
					"location_id"       : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'location',line: i})),
					"entity_id"         : entity_id,
					"entity_type"       : entity_type,
					"project_id"        : parseInt(rec.getSublistValue({sublistId: 'item',fieldId: 'custcol_clgx_journal_project_id',line: i})),
					"billable"          : dwg.get_bit(rec.getSublistValue({sublistId: 'item',fieldId: 'isbillable',line: i})),
					"no_fam_impact"     : dwg.get_bit(rec.getValue({ fieldId: "custbody_clgx_inservice_entry" })),
					"quantity"          : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'quantity',line: i})),
					"rate"              : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'rate',line: i})),
					"amount"            : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'amount',line: i})),
					"taxcode"           : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'taxcode',line: i})),
					"taxrate1"          : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'taxrate1',line: i})),
					"taxrate2"          : parseFloat(rec.getSublistValue({sublistId: 'item',fieldId: 'taxrate2',line: i})),
					"memo"              : rec.getSublistValue({sublistId: 'item',fieldId: 'description',line: i}),

				});
			}
			
			var expenses = [];
			var lines = rec.getLineCount({sublistId: 'expense'});
			for ( var i = 0; i < lines; i++ ) {
				var entity_id = parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'customer',line: i}));
				var entity_type = get_entity_type(entity_id);
				
				expenses.push({
					"vendor_credit_id"     : record_id,
					"line_id"           : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'line',line: i})),
					"account_id"        : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'account',line: i})),
					"class_id"          : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'class',line: i})),
					"department_id"     : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'department',line: i})),
					"location_id"       : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'location',line: i})),
					"entity_id"         : entity_id,
					"entity_type"       : entity_type,
					"project_id"        : parseInt(rec.getSublistValue({sublistId: 'expense',fieldId: 'custcol_clgx_journal_project_id',line: i})),
					"amount"            : parseFloat(rec.getSublistValue({sublistId: 'expense',fieldId: 'amount',line: i})),
					"memo"              : rec.getSublistValue({sublistId: 'expense',fieldId: 'memo',line: i}),

				});
			}

			var apply = [];
			var lines = rec.getLineCount({sublistId: 'apply'});
			for ( var i = 0; i < lines; i++ ) {
				var applied = dwg.get_bit(rec.getSublistValue({sublistId: 'apply',fieldId: 'apply',line: i}));
				if(applied){
					apply.push({
						"vendor_credit_id"  : record_id,
						"line_id"           : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'line',line: i})),
						"tran_id"           : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'internalid',line: i})),
						"tran_type"         : rec.getSublistValue({sublistId: 'apply',fieldId: 'trantype',line: i}),
						"amount"            : parseFloat(rec.getSublistValue({sublistId: 'apply',fieldId: 'amount',line: i}))
					});
				}
			}

			vendorcredit = {
				"vendor_credit_id"     : record_id,
				"vendor_credit"        : rec.getValue({ fieldId: "tranid" }),
				"external_id"          : rec.getValue({ fieldId: "externalid" }),
				"vendorbill_id"        : parseInt(rec.getValue({ fieldId: "createdfrom" })),
				"vendor_id"            : parseInt(rec.getValue({ fieldId: "entity" })),
				"subsidiary_id"        : parseInt(rec.getValue({ fieldId: "subsidiary" })),
				"location_id"          : parseInt(rec.getValue({ fieldId: "location" })),
				"currency_id"          : parseInt(rec.getValue({ fieldId: "currency" })),
				"project_id"           : parseInt(rec.getValue({ fieldId: "custbody_cologix_project_name" })),
				"accounting_period_id" : parseInt(rec.getValue({ fieldId: "postingperiod" })),
				"amount"               : parseFloat(rec.getValue({ fieldId: "usertotal" })),
				"exchangerate"         : parseFloat(rec.getValue({ fieldId: "exchangerate" })),
				"applied"              : parseFloat(rec.getValue({ fieldId: "applied" })),
				"unapplied"            : parseFloat(rec.getValue({ fieldId: "unapplied" })),
				"no_fam_impact"        : dwg.get_bit(rec.getValue({ fieldId: "custbody_clgx_inservice_entry" })),
				"memo"                 : rec.getValue({ fieldId: "memo" }),
				"vendor_credit_items"                : items,
				"vendor_credit_expenses"             : expenses,
				"vendor_credit_apply"                : apply,
			};

			
		}
		catch (error) {

		}
		
		return [vendorcredit];
		
	}

	function get_entity_type(entity_id) {
		var entityType = '';
		if(entity_id){
			var results     = search.load({ type: 'entity', id: "customsearch_clgx_dw_entity" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: entity_id }));
			results.run().each(function(result) {
				entityType = result.getValue(result.columns[1]);
		        return true;
			});
			if(!entityType){
				var results     = search.load({ type: 'job', id: "customsearch_clgx_dw_service" });
				results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: entity_id }));
				results.run().each(function(result) {
					entityType = 'Service';
			        return true;
				});
			}
		}
		return entityType;
	}
	
	
	return {
		get_vendorcredit: get_vendorcredit
	};
});