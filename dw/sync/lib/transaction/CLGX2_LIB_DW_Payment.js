/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_payment(record_id) {
		var payment     = {};
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_payment" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			var rec  = record.load({ type: record.Type.CUSTOMER_PAYMENT, id: record_id });
			
			var apply_items = [];
			var lines = rec.getLineCount({sublistId: 'apply'});
			for ( var i = 0; i < lines; i++ ) {
				var amount = parseFloat(rec.getSublistValue({sublistId: 'apply',fieldId: 'amount',line: i}));
				var apply = dwg.get_bit(rec.getSublistValue({sublistId: 'apply',fieldId: 'apply',line: i}));
				if(apply){
					apply_items.push({
						"payment_id"        : record_id,
						"line_id"           : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'line',line: i})),
						"applied_to_id"     : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'internalid',line: i})),
						"applied_to_refnum" : dwg.get_null(rec.getSublistValue({sublistId: 'apply',fieldId: 'refnum',line: i})),
						"applied_to_type"   : dwg.get_null(rec.getSublistValue({sublistId: 'apply',fieldId: 'trantype',line: i})),
						"applied_to_date"   : dwg.sqlDate(rec.getSublistValue({sublistId: 'apply',fieldId: 'applydate',line: i})),
						//"apply"             : apply,
						"amount"            : amount
					});
				}
			}
			payment = {
				"payment_id"           : record_id,
				"payment"              : result.getValue(result.columns[1]),
				"external_id"          : result.getValue(result.columns[2]),
				"customer_id"          : parseInt(rec.getValue({ fieldId: "customer" })),
				"subsidiary_id"        : result.getValue(result.columns[3]),
				"currency_id"          : parseInt(result.getValue(result.columns[4])),
				"status_id"            : parseInt(result.getValue(result.columns[5])),
				"balance_paid"         : parseFloat(rec.getValue({ fieldId: "balance" })),
				"balance_pending"      : parseFloat(rec.getValue({ fieldId: "pending" })),
				"payment_amount"       : parseFloat(rec.getValue({ fieldId: "payment" })),
				"date"                 : dwg.sqlDate(result.getValue(result.columns[6])),
				"accounting_period_id" : parseInt(result.getValue(result.columns[7])),
				"account_id"           : parseInt(result.getValue(result.columns[8])),
				"ar_account_id"        : parseInt(rec.getValue({ fieldId: "aracct" })),
				"location_id"          : parseInt(result.getValue(result.columns[9])),
				"memo"                 : parseInt(result.getValue(result.columns[10])),
				"payment_apply"        : apply_items
			};
			
	        return true;
		});

		return [payment];
	}
	
	
	function get_payment_applied(record_id) {
		
		var rec = record.load({ type: 'customerpayment', id: record_id });
		var items = [];
		var lines = rec.getLineCount({sublistId: 'apply'});
		for ( var i = 0; i < lines; i++ ) {
			var amount = parseFloat(rec.getSublistValue({sublistId: 'apply',fieldId: 'amount',line: i}));
			if(amount){
				items.push({
					"payment_id"        : record_id,
					"applied_to_date"   : dwg.sqlDate(rec.getSublistValue({sublistId: 'apply',fieldId: 'applydate',line: i})),
					"applied_to_type"   : dwg.get_null(rec.getSublistValue({sublistId: 'apply',fieldId: 'trantype',line: i})),
					"applied_to_id"     : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'internalid',line: i})),
					"applied_to_refnum" : dwg.get_null(rec.getSublistValue({sublistId: 'apply',fieldId: 'refnum',line: i})),
					"amount"            : amount
				});
			}
		}
		
		/*
		var items = [];
		var applied_to = [];
		var results = search.load({ type: "transaction", id: "customsearch_clgx_dw_payment_apply" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			var applied_to_id = parseInt(result.getValue(result.columns[3]));
			if(applied_to_id){
				if(applied_to.indexOf(applied_to_id) == -1){ // remove unavoidable (?) search dups
					applied_to.push(applied_to_id);
					items.push({
						"payment_id"        : parseInt(result.getValue(result.columns[0])),
						"applied_to_date"   : dwg.sqlDate(result.getValue(result.columns[1])),
						"applied_to_type"   : dwg.get_null(result.getValue(result.columns[2])),
						"applied_to_id"     : applied_to_id,
						"applied_to_refnum" : dwg.get_null(result.getValue(result.columns[4])),
						"amount"            : parseFloat(result.getValue(result.columns[5]))
					});
				}
			}
	        return true;
		});
		*/

		return items;
	}
	
	return {
		get_payment: get_payment
	};
});