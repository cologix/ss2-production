/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/26/2018
 */
define(["/SuiteScripts/clgx/iwsapi/lib/CLGX2_IWSAPI_MethodFactory", "N/http", "N/error", "N/runtime", "N/search","/SuiteScripts/clgx/libraries/moment.min", ],
function(iwsapi, http, error, runtime, search, moment) {
	/**
	 * Returns an accounting period internal id by the fiscal year.
	 * 
	 * @param {number} year - The fiscal year.
	 * @return {(number|null)}
	 */
	function getAccountingPeriodIDByYear(year) {
		var finalId = -1;
		
		var filters = new Array();
		filters.push(search.createFilter({ name: "periodname", operator: "contains", values: ("FY " + year) }));
		var searchObject = search.create({ type: "accountingperiod", id: "customsearch_clgx_dw_accounting_period", filters: filters });
		searchObject.run().each(function(result) {
			finalId = result.id;
		});
		
		return finalId;
	}
	
	
	/**
	 * Returns whether or not a variable has a value.
	 * 
	 * @param {(string | number)}
	 * @returns {boolean}
	 */
	function isNull(variable) {
		if(typeof variable === "undefined" || variable == null || variable == "") {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Queries SuiteTalk and builds a JSON of the results.
	 * 
	 * @returns {Object}
	 */
	function get_budgets() {
		
		var requestBody     = new Object();
    	requestBody.record  = "budget";
    	
    	requestBody.filters = new Array();
    	requestBody.filters.push({ name: "year", operator: "anyOf", value: getAccountingPeriodIDByYear(runtime.getCurrentScript().getParameter("custscript_clgx2_dw_budget_budgetyear")) });
    	//requestBody.filters.push({ name: "year", operator: "anyOf", value: moment().format('YYYY') });
    	var response    = iwsapi.requestFactory({ method: "search", authenticationID: 1, body: requestBody });
    	log.debug({ title: "response", details: response });
    	
    	var records     = JSON.parse(response);
    	var recordCount = records.header.totalRecords;
    	var budgets     = new Array();
    	var tmpRecord   = null;
    	
    	for(var r = 0; r < recordCount; r++) {
    		if(records.body[r] != null) {
    			budgets.push({
        			"budget_id"           : isNull(records.body[r].id) == true ? -1 : parseInt(records.body[r].id),
        			"accounting_period_id": isNull(records.body[r].year) == true ? -1 : parseInt(records.body[r].year.value),
        			"class_id"            : isNull(records.body[r].class) == true ? -1 : parseInt(records.body[r].class.value),
        			"location_id"         : isNull(records.body[r].location) == true ? -1 : parseInt(records.body[r].location.value),
        			"account_id"          : isNull(records.body[r].account) == true ? -1 : parseInt(records.body[r].account.value),
        			"subsidiary_id"       : isNull(records.body[r].subsidiary) == true ? -1 : parseInt(records.body[r].subsidiary.value),
        			"category_id"         : isNull(records.body[r].category) == true ? -1 : parseInt(records.body[r].category.value),
        			"budget_type"         : isNull(records.body[r].budgetType) == true ? "" : records.body[r].budgetType.text,
        			"currency_id"         : isNull(records.body[r].currency) == true ? -1 : parseInt(records.body[r].currency.value),
        			"january_amount"      : isNull(records.body[r].periodAmount1) == true ? 0.00 : parseFloat(records.body[r].periodAmount1.text),
        			"february_amount"     : isNull(records.body[r].periodAmount2) == true ? 0.00 : parseFloat(records.body[r].periodAmount2.text),
        			"march_amount"        : isNull(records.body[r].periodAmount3) == true ? 0.00 : parseFloat(records.body[r].periodAmount3.text),
        			"april_amount"        : isNull(records.body[r].periodAmount4) == true ? 0.00 : parseFloat(records.body[r].periodAmount4.text),
        			"may_amount"          : isNull(records.body[r].periodAmount5) == true ? 0.00 : parseFloat(records.body[r].periodAmount5.text),
        			"june_amount"         : isNull(records.body[r].periodAmount6) == true ? 0.00 : parseFloat(records.body[r].periodAmount6.text),
        			"july_amount"         : isNull(records.body[r].periodAmount7) == true ? 0.00 : parseFloat(records.body[r].periodAmount7.text),
        			"august_amount"       : isNull(records.body[r].periodAmount8) == true ? 0.00 : parseFloat(records.body[r].periodAmount8.text),
        			"september_amount"    : isNull(records.body[r].periodAmount9) == true ? 0.00 : parseFloat(records.body[r].periodAmount9.text),
        			"october_amount"      : isNull(records.body[r].periodAmount10) == true ? 0.00 : parseFloat(records.body[r].periodAmount10.text),
        			"november_amount"     : isNull(records.body[r].periodAmount11) == true ? 0.00 : parseFloat(records.body[r].periodAmount11.text),
        			"december_amount"     : isNull(records.body[r].periodAmount12) == true ? 0.00 : parseFloat(records.body[r].periodAmount12.text)
        		});
    		}
    	}
    	
    	return budgets;
	}
	
	
    return {
        get_budgets: get_budgets
    };
    
});
