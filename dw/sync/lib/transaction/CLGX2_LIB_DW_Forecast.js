/**
 * @NApiVersion 2.x
 */

define(["N/record",  "N/search",  "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	function get_forecast(record_id) {
		var forecast = {};
		try {
			var rec  = record.load({ type: 'customrecord_clgx_cap_bud_forecast', id: record_id });
			forecast = {
					"forecast_id"          : parseInt(rec.getValue({ fieldId:     "id" })),
					"project_id"           : parseInt(rec.getValue({ fieldId:     "custrecord_clgx_cap_bud_fore_project_id" })),
					"accounting_period_id" : parseInt(rec.getValue({ fieldId:     "custrecord_clgx_cap_bud_fore_period" })),
					"key"                  : rec.getValue({ fieldId:              "custrecord_clgx_cap_bud_fore_key" }),
					"amount"               : parseFloat(rec.getValue({ fieldId:   "custrecord_clgx_cap_bud_fore_amount" }))
				};
		}
		catch (error) {}
		return [forecast];
	}
	return {
		get_forecast: get_forecast
	};
});