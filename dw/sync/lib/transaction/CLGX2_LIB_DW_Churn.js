/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   2/23/2018
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"],
function(record, search, dwg) {
	function get_churn(record_id) {
		var churns       = new Array();
		var searchObject = search.load({ type: "customrecord_cologix_churn", id: "customsearch_clgx_dw_churn" });
		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", values: [record_id] }));
		
		searchObject.run().each(function(result) {
			var columns = result.columns;
			
			churns.push({
				"churn_id"                    : parseInt(result.getValue(columns[0])),
				"customer_id"                 : parseInt(result.getValue(columns[1])),
				"location_id"                 : parseInt(result.getValue(columns[2])),
				"reason_id"                   : parseInt(result.getValue(columns[3])),
				"status_id"                   : parseInt(result.getValue(columns[4])),
				"accounting_period_id"        : parseInt(result.getValue(columns[5])),
				"employee_id"                 : parseInt(result.getValue(columns[6])),
				"churn_type_id"               : parseInt(result.getValue(columns[7])),
				"case_id"                     : parseInt(result.getValue(columns[8])),
				"task"                        : result.getValue(columns[9]),
				"currency_id"                 : parseInt(result.getValue(columns[10])),
				"expected_power_mrr"          : result.getValue(columns[11]),
				"expected_space_mrr"          : result.getValue(columns[12]),
				"expected_interconnection_mrr": result.getValue(columns[13]),
				"expected_network_mrr"        : result.getValue(columns[14]),
				"expected_other_mrr"          : result.getValue(columns[15]),
				"current_power_mrr"           : result.getValue(columns[16]),
				"current_space_mrr"           : result.getValue(columns[17]),
				"current_interconnection_mrr" : result.getValue(columns[18]),
				"current_network_mrr"         : result.getValue(columns[19]),
				"current_other_mrr"           : result.getValue(columns[20]),
				"comments"                    : result.getValue(columns[21]),
				"actual_power_mrr"            : result.getValue(columns[22]),
				"actual_space_mrr"            : result.getValue(columns[23]),
				"actual_other_mrr"            : result.getValue(columns[24]),
				"actual_interconnection_mrr"  : result.getValue(columns[25]),
				"actual_network_mrr"          : result.getValue(columns[26]),
				"date_closed"                 : dwg.sqlDate(result.getValue(columns[27]))
			});
			
			return true;
		});
		
		return churns;
	}
	
    return {
        get_churn: get_churn
    };
    
});
