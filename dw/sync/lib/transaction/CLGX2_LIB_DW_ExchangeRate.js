/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 9/6/2018
 */
define(["/SuiteScripts/clgx/iwsapi/lib/CLGX2_IWSAPI_MethodFactory", "/SuiteScripts/clgx/libraries/moment.min"],
function(iwsapi, moment) {
	function get_exchange_rate() {
		var requestBody     = new Object();
		requestBody.record  = "rate";
		requestBody.filters = new Array(); 	
		requestBody.filters.push({ name: "baseCurrency", operator: "anyOf", value: "1" }); //USD
		requestBody.filters.push({ name: "transactionCurrency", operator: "anyOf", value: "3" }); //CAD
		requestBody.filters.push({ name: "effectiveDate", operator: "after", value: moment().subtract(30, "days").format() });
		var records     = JSON.parse(iwsapi.requestFactory({ method: "search", authenticationID: 1, body: requestBody }));
		
		var exchange    = new Array();
		var recordCount = records.body.length;
		for(var er = 0; er < recordCount; er++) {
		   exchange.push({ 
			   "rate_id"                : parseInt(records.body[er].id),
			   "base_currency_id"       : parseInt(records.body[er].baseCurrency.value),
			   "transaction_currency_id": parseInt(records.body[er].transactionCurrency.value),
			   "effective_date"         : moment(records.body[er].effectiveDate.text).format("MM/DD/YYYY"),
			   "exchange_rate"          : parseFloat(records.body[er].exchangeRate.text)
		   });
		}
		   
		return exchange;   
	}
	
    return {
    	get_exchange_rate: get_exchange_rate
    };
    
});
