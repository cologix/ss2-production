/**
 * @NApiVersion 2.x
 */


define(["N/record", "N/search", "/SuiteScripts/clgx/libraries/moment.min", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, moment, dwg) {
	
	function get_invoice(record_id) {
		var invoice = {};
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {

			var invoice_date  = moment(result.getValue(result.columns[12])).format('MM/DD/YYYY')
		    
			var sales_order_id = parseInt(result.getValue(result.columns[6]));
			var type_id = '';
			if(sales_order_id){
				var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_transaction_type" });
				results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: sales_order_id })];
				results.run().each(function(result) {
					type_id = result.getValue(result.columns[1]);
			        return true;
				});
				if(type_id != 'SalesOrd') { // nullify CreatedFrom if is not a SalesOrder
					sales_order_id = null;
				}
			}
			
			var items = get_invoice_items(record_id);
			//var totals = dwg.get_totals(items);
			
			invoice = {
				"invoice_id"                 : parseInt(result.getValue(result.columns[0])),
				"invoice"                    : result.getValue(result.columns[1]),
				"external_id"                : result.getValue(result.columns[2]),
				"customer_id"                : parseInt(result.getValue(result.columns[3])),
				"location_id"                : parseInt(result.getValue(result.columns[4])),
				"clocation_id"               : parseInt(result.getValue(result.columns[5])),
				"sales_order_id"             : sales_order_id,
				"currency_id"                : parseInt(result.getValue(result.columns[7])),
				"status_id"                  : result.getValue(result.columns[8]),
				"cinvoice_id"                : parseInt(result.getValue(result.columns[9])),
				"accounting_period_id"       : parseInt(result.getValue(result.columns[10])),
				"due_date"                   : dwg.sqlDate(result.getValue(result.columns[11])),
				"date"                       : dwg.sqlDate(invoice_date),
				"po_nbr"                     : result.getValue(result.columns[13]),
				"fx_rate"                    : parseFloat(result.getValue(result.columns[15])),
				"date_closed"                : dwg.sqlDate(result.getValue(result.columns[16])),
				"items"                      : items
			};
	        return true;
		});
		return [invoice];
	}
	
	function get_invoice_items(record_id,prev_start,prev_end) {
		
		var items = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice_item" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(cresult) {
			
			
			var line_id = parseInt(cresult.getValue(cresult.columns[1]));
			var item_id = parseInt(cresult.getValue(cresult.columns[3]));
			var service_id = parseInt(cresult.getValue(cresult.columns[4]));
			
			var tax_id = parseInt(cresult.getValue(cresult.columns[11]));
			var tax_line = cresult.getValue(cresult.columns[12]);
			var class_id = parseInt(cresult.getValue(cresult.columns[14]));
			var is_mrc = dwg.is_mrc(class_id);
			var memo = cresult.getValue(cresult.columns[15]).replace(/'/g, "’");
			
			var main_line = false;
			var main = cresult.getValue(cresult.columns[13]);
			if(main){
				main_line = true;
			}
			if (tax_line){
				item_id = null;
				tax_id = null;
			}
			if(item_id != -6){
				
				var quantity = parseFloat(cresult.getValue(cresult.columns[6])) || 0;
				var rate = parseFloat(cresult.getValue(cresult.columns[7])) || 0;
				var fxrate = parseFloat(cresult.getValue(cresult.columns[8])) || 0;
				var amount = parseFloat(cresult.getValue(cresult.columns[9])) || 0;
				var fxamount = parseFloat(cresult.getValue(cresult.columns[10])) || 0;
				
				items.push({
					"invoice_id"        : parseInt(cresult.getValue(cresult.columns[0])),
					"line_id"           : line_id,
					"line_key"          : parseInt(cresult.getValue(cresult.columns[2])),
					"item_id"           : item_id,
					"service_id"        : service_id,
					"location_id"       : parseInt(cresult.getValue(cresult.columns[5])),
					//"schedule_id"       : parseInt(cresult.getValue(cresult.columns[6])),
					"quantity"          : dwg.round(quantity),
					"rate"              : dwg.round(rate),
					"fxrate"            : dwg.round(fxrate),
					"amount"            : dwg.round(amount),
					"fxamount"          : dwg.round(fxamount),
					"tax_id"            : tax_id,
					"discount_line"     : dwg.is_discount(item_id),
					"tax_line"          : dwg.get_bit(tax_line),
					"main_line"         : dwg.get_bit(main_line),
					"memo"              : memo,
					"is_mrc"            : is_mrc
				});
			}

	        return true;
		});
		return items;
	}
	
	// =================================================================================================================================== //

	function get_cinvoice(record_id) {
		
		var cinvoice = {};
		var results = search.load({ type: 'customrecord_clgx_consolidated_invoices', id: "customsearch_clgx_dw_cinvoice" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			
			var cinvoice_id = parseInt(result.getValue(result.columns[0]));
			
			var ids = (result.getValue(result.columns[25])).split(",");
			var invoices = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				var invoice_id = parseInt(ids[i]);
				if(invoice_id){
					invoices.push({
						"cinvoice_id": 			cinvoice_id,
						"invoice_id": 			invoice_id
					})
				}
			}
			
			var ids = (result.getValue(result.columns[26])).split(",");
			var contacts = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				var contact_id = parseInt(ids[i]);
				if(contact_id){
					contacts.push({
						"cinvoice_id": 			cinvoice_id,
						"contact_id": 			contact_id
					})
				}
			}
			
			cinvoice = {
				"cinvoice_id"              : cinvoice_id,
				"cinvoice"                 : result.getValue(result.columns[1]),
				"external_id"              : result.getValue(result.columns[2]),
				"customer_id"              : parseInt(result.getValue(result.columns[3])),
				"clocation_id"             : parseInt(result.getValue(result.columns[4])),
				"output_id"                : parseInt(result.getValue(result.columns[5])),
				"date"                     : dwg.sqlDate(result.getValue(result.columns[6])),
				"pdf_file"                 : parseInt(result.getValue(result.columns[7])),
				"json_file"                : parseInt(result.getValue(result.columns[8])),
				"month_id"                 : result.getValue(result.columns[9]),
				"year"                     : parseInt(result.getValue(result.columns[10])),
				"month_displayed_id"       : parseInt(result.getValue(result.columns[11])),
				"year_displayed"           : parseInt(result.getValue(result.columns[12])),
				"subtotal"                 : parseFloat(result.getValue(result.columns[13])),
				"tax1"                     : parseFloat(result.getValue(result.columns[14])),
				"tax2"                     : parseFloat(result.getValue(result.columns[15])),
				"total"                    : parseFloat(result.getValue(result.columns[16])),
				"balance"                  : parseFloat(result.getValue(result.columns[17])),
				"currency"                 : result.getValue(result.columns[18]),
				"credit_card"              : result.getValue(result.columns[19]),
				"credit_card_transaction"  : result.getValue(result.columns[20]),
				"churn"                    : result.getValue(result.columns[21]),
				"cons_inv_churn"           : result.getValue(result.columns[22]),
				"ar_reminder"              : result.getValue(result.columns[23]),
				"processed"                : result.getValue(result.columns[24]),
				"invoices"                 : invoices,
				"contacts"                 : contacts
			};
	        return true;
		});
		return [cinvoice];
	}

	
	return {
		get_invoice: get_invoice,
		get_cinvoice: get_cinvoice
	};
});