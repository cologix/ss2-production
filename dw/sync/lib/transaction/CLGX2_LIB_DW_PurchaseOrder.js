/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	function get_purchaseorder(record_id) {
		var purchase_order = {};
		var results        = search.load({ type: search.Type.PURCHASE_ORDER, id: "customsearch_clgx_dw_purchaseorder" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		
		results.run().each(function(result) {
			var customerObj = get_customer_type(parseInt(result.getValue(result.columns[3])));
			
			var items = get_purchaseorder_items(record_id);
			var totals = dwg.get_totals(items);
			
			purchase_order = {
				"purchaseorder_id" : parseInt(result.getValue(result.columns[0])),
				"purchaseorder"    : result.getValue(result.columns[1]),
				"external_id"      : parseInt(result.getValue(result.columns[2])),
				"customer_id"      : customerObj.customer,
				"vendor_id"        : customerObj.vendor,
				"subsidiary_id"    : parseInt(result.getValue(result.columns[4])),
				"location_id"      : parseInt(result.getValue(result.columns[5])),
				"currency_id"      : parseInt(result.getValue(result.columns[6])),
				"employee_id"      : parseInt(result.getValue(result.columns[7])),
				"date"             : dwg.sqlDate(result.getValue(result.columns[8])),
				"receive"          : dwg.sqlDate(result.getValue(result.columns[9])),
				"status_id"        : result.getValue(result.columns[10]),
				"project_id"       : parseInt(result.getValue(result.columns[11])),
				"proposal_id"      : parseInt(result.getValue(result.columns[12])),
				"salesorder_id"    : parseInt(result.getValue(result.columns[13])),
				"accounting_period_id"    : parseInt(result.getValue(result.columns[14])),
				"memo"             : result.getValue(result.columns[15]),
				"sub_total"        : totals.sub_total,
				"tax_total"        : totals.tax_total,
				"total"            : totals.total,
				"items"            : items
			};
			
			return true;
		});
		
		return [purchase_order];
	}
	
	function get_purchaseorder_items(record_id) {
		var items       = [];
		
		try {
			var results     = search.load({ type: search.Type.PURCHASE_ORDER, id: "customsearch_clgx_dw_purchaseorder_item" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
			results.run().each(function(result) {
				
				var main_line = false;
				var main = dwg.get_null(result.getValue(result.columns[14]));
				if(main){
					main_line = true;
				}
				
				var item_id   = parseInt(result.getValue(result.columns[2]));
				var tax_line  = result.getValue(result.columns[15]);
				var tax_id    = parseInt(result.getValue(result.columns[16]));
				
				if (tax_line){
					item_id = null;
					tax_id = null;
				}
				
				items.push({
					"purchaseorder_id"      : parseInt(result.getValue(result.columns[0])),
					"line_id"               : parseInt(result.getValue(result.columns[1])),
					"item_id"               : item_id, //parseInt(result.getValue(result.columns[2]))
					"location_id"           : parseInt(result.getValue(result.columns[3])),
					"vendor_id"             : parseInt(result.getValue(result.columns[4])),
					"department_id"         : parseInt(result.getValue(result.columns[5])),
					"expected_receipt_date" : dwg.sqlDate(result.getValue(result.columns[6])),
					"quantity"              : parseInt(result.getValue(result.columns[7])),
					"quantity_billed"       : parseInt(result.getValue(result.columns[8])),
					"quantity_fulfilled"    : parseInt(result.getValue(result.columns[9])),
					"billable"              : dwg.get_bit(result.getValue(result.columns[10])),
					"closed"                : dwg.get_bit(result.getValue(result.columns[11])),
					"rate"                  : parseFloat(result.getValue(result.columns[12])),
					"amount"                : parseFloat(result.getValue(result.columns[13])),
					"tax_id"                : tax_id, //result.getValue(result.columns[14])
					"tax_line"              : dwg.get_bit(tax_line), //result.getValue(result.columns[15])
					"main_line"             : dwg.get_bit(main_line),
					"tax_item"              : result.getValue(result.columns[17]),
					"tax_rate"              : parseFloat(result.getValue(result.columns[18])),
					"memo"                  : result.getValue(result.columns[19]).replace(/'/g, "’")
				});
				
		        return true;
			});
		} catch(ex) {
			log.error({ title: "Error - get_purchaseorder_items", details: ex });
		}
		
		return items;
	}
	
	
	function get_customer_type(record_id) {
		var entityObject      = new Object();
		entityObject.customer = null;
		entityObject.vendor   = null;
		
		var entityType = '';
		if(record_id){
			var results     = search.load({ type: 'entity', id: "customsearch_clgx_dw_entity" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
			results.run().each(function(result) {
				entityType = result.getValue(result.columns[1]);
		        return true;
			});
		}

		if(entityType == 'CustJob'){
			entityObject.customer = record_id;
		}
		if(entityType == 'Vendor'){
			entityObject.vendor = record_id;
		}
		/*
		try { 
			record.load({ type: record.Type.CUSTOMER, id: record_id });
			entityObject.customer = record_id;
		} catch(ex) {}
		
		try { 
			record.load({ type: record.Type.VENDOR, id: record_id });
			entityObject.vendor = record_id;
		} catch(ex) {}
		*/
		return entityObject;
	}
	
	
	return {
		get_purchaseorder: get_purchaseorder
	};
});