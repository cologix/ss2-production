/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_journalentry(record_id) {
		var je = get_journal_record(record_id);
		var journal = {};
		if(je){  // avoid 'Attempting to access memorized transaction as a non-memorized transaction'
			
			var results = search.load({ type: "transaction", id: "customsearch_clgx_dw_journalentry" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
			results.run().each(function(result) {
				var journal_items = get_journalentry_items(record_id);
				
				journal = {
					"journalentry_id"      : parseInt(result.getValue(result.columns[0])),
					"journalentry"         : result.getValue(result.columns[1]),
					"external_id"          : result.getValue(result.columns[2]),
					"status_id"            : result.getValue(result.columns[3]),
					"subsidiary_id"        : parseInt(je.getValue("subsidiary")),
					"void_of"              : parseInt(je.getValue("createdfrom")),
					"date"                 : dwg.sqlDate(result.getValue(result.columns[4])),
					"accounting_period_id" : parseInt(result.getValue(result.columns[5])),
					"currency_id"          : parseInt(result.getValue(result.columns[6])),
					"reversal_nbr"         : journal_items.reversal.number,
					"reversal_date"        : journal_items.reversal.date,
					"fx_rate"              : parseFloat(result.getValue(result.columns[8])),
					"memo"                 : result.getValue(result.columns[7]).replace("- None -", ""),
					"items"                : journal_items.items
				};
				
		        return true;
			});
		}
		return [journal];
	}
	
	
	function get_journalentry_items(record_id) {
		var items           = [];
		var reversal_number = null;
		var reversal_date   = null;
		var entityID        = null;
		var prevEntityID    = null;
		var entityObject    = null;
		
		var results     = search.load({ type: 'transaction', id: "customsearch_clgx_dw_journalentry_item" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			entityID = parseInt(result.getValue(result.columns[8]));
			
			if((entityID != null && entityID != "") && (prevEntityID != entityID)) {
				entityObject = get_entity_type(parseInt(result.getValue(result.columns[8])));
			}
			
			reversal_number = parseInt(result.getValue(result.columns[15]));
			reversal_date   = dwg.sqlDate(result.getValue(result.columns[16]));
				
			items.push({
				"journalentry_id"  : parseInt(result.getValue(result.columns[0])),
				"line_id"          : parseInt(result.getValue(result.columns[1])),
				"account_id"       : parseInt(result.getValue(result.columns[2])),
				"debit"            : parseFloat(result.getValue(result.columns[3])),
				"credit"           : parseFloat(result.getValue(result.columns[4])),
				"amount"           : parseFloat(result.getValue(result.columns[5])),
				"fxamount"         : parseFloat(result.getValue(result.columns[6])),
				"class_id"         : parseInt(result.getValue(result.columns[7])),
				"customer_id"      : entityObject.customer,
				"vendor_id"        : entityObject.vendor,
				"employee_id"      : entityObject.employee,
				"subsidiary_id"    : parseInt(result.getValue(result.columns[9])),
				"to_subsidiary_id" : parseInt(result.getValue(result.columns[10])),
				"location_id"      : parseInt(result.getValue(result.columns[11])),
				"to_location_id"   : parseInt(result.getValue(result.columns[12])),
				"department_id"    : parseInt(result.getValue(result.columns[13])),
				"project_id"       : parseInt(result.getValue(result.columns[14])),
				"memo"             : result.getValue(result.columns[17]).replace(/'/g, "’").replace("- None -", "")
			});
			
			prevEntityID = entityID;
			
	        return true;
		});
		
		return {"items" : items, "reversal" : {"number" : reversal_number, "date" : reversal_date}};
	}
	
	
	function get_journal_record(record_id) {
		var recordObject = null;
		
		if(recordObject == null) {
			try {
				recordObject = record.load({ type: "journalentry", id: record_id });
			} catch(ex) { recordObject = null; }
		}
		
		if(recordObject == null) {
			try {
				recordObject = record.load({ type: "advintercompanyjournalentry", id: record_id });
			} catch(ex) { recordObject = null; }
		}
		
		return recordObject;
	}
	
	
	function get_entity_type(record_id) {
		
		var entityObject      = new Object();
		entityObject.customer = null;
		entityObject.vendor   = null;
		entityObject.employee = null;
		
		var entityType = '';
		if(record_id){
			var results     = search.load({ type: 'entity', id: "customsearch_clgx_dw_entity" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
			results.run().each(function(result) {
				entityType = result.getValue(result.columns[1]);
		        return true;
			});
		}
		if(entityType == 'CustJob'){
			entityObject.customer = record_id;
		}
		if(entityType == 'Vendor'){
			entityObject.vendor = record_id;
		}
		if(entityType == 'Employee'){
			entityObject.employee = record_id;
		}
		return entityObject;
	}
	
	
	return {
		get_journalentry: get_journalentry
	};
});