/**
 * @NApiVersion 2.x
 */

define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_opportunity(record_id) {
		var opportunity = {};
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_opportunity" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			
			var opportunity_id = parseInt(result.getValue(result.columns[0]));
			var columns    = search.lookupFields({type: search.Type.OPPORTUNITY, id: opportunity_id, columns: ["winlossreason"]});
			var loss_reason_id = null;
			if(columns["winlossreason"][0]){
				var loss_reason_id   = parseInt(columns["winlossreason"][0].value);
			}
			
			var items = get_opportunity_items(record_id);
			var totals = dwg.get_totals(items);

			opportunity = {
				"opportunity_id":			opportunity_id,
				"opportunity":				result.getValue(result.columns[1]),
				"external_id":				result.getValue(result.columns[2]),
				"title":				  		result.getValue(result.columns[3]),
				"customer_id":			  	parseInt(result.getValue(result.columns[4])),
				"subsidiary_id":		  		parseInt(result.getValue(result.columns[5])),
				"currency_id":			  	parseInt(result.getValue(result.columns[6])),
				"sales_rep_id":			  	parseInt(result.getValue(result.columns[7])),
				"sales_engineer_id":        parseInt(result.getValue(result.columns[21])),
				"status_id":			  		result.getValue(result.columns[8]),
				"entity_status_id":         parseInt(result.getValue(result.columns[18])),
				"partner_id":			  	parseInt(result.getValue(result.columns[9])),
				"date":					  	dwg.sqlDate(result.getValue(result.columns[10])),
				"requested_install_date": 	dwg.sqlDate(result.getValue(result.columns[11])),
				"close_date":			  	dwg.sqlDate(result.getValue(result.columns[12])),
				"sale_type_id":			  	parseInt(result.getValue(result.columns[13])),
				"lead_source_id":		  	parseInt(result.getValue(result.columns[14])),
				"forecast_type_id":		  	parseInt(result.getValue(result.columns[15])),
				"incremental_mrc":		  	parseFloat(result.getValue(result.columns[16])),
				"location_id":              parseInt(result.getValue(result.columns[19])),
				"location_name":            result.getText(result.columns[20]),
				"channel_rep_id":           parseInt(result.getValue(result.columns[22])),
				"channel_rep_name":         result.getText(result.columns[22]),
				"loss_reason_id":		  	loss_reason_id,
				"memo":					  	result.getValue(result.columns[17]).replace(/'/g, "’"),
				"nrc_total":					totals.nrc_total,
				"mrc_total":					totals.mrc_total,
				"sub_total":					totals.sub_total,
				"discount":					totals.discount,
				"tax_total":					totals.tax_total,
				"total":						totals.total,
				"items":				  		items
			};
	        return true;
		});
		return [opportunity];
	}
	
	function get_opportunity_items(record_id) {
		var items = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_opportunity_item" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			
			var item_id = parseInt(result.getValue(result.columns[3]));
			var tax_id = parseInt(result.getValue(result.columns[9]));
			var tax_line = result.getValue(result.columns[10]);
			var class_id = parseInt(result.getValue(result.columns[4]));
			
			var main_line = false;
			var main = parseInt(result.getValue(result.columns[11]));
			if(main){
				main_line = true;
			}
			if (tax_line){
				item_id = null;
				tax_id = null;
			}
			items.push({
				"opportunity_id":	parseInt(result.getValue(result.columns[0])),
				"line_id":			parseInt(result.getValue(result.columns[1])),
				"line_key":			parseInt(result.getValue(result.columns[2])),
				"item_id":			item_id,
				"location_id":		parseInt(result.getValue(result.columns[5])),
				"quantity":			parseFloat(result.getValue(result.columns[6])),
				"rate":				parseFloat(result.getValue(result.columns[7])),
				"amount":			parseFloat(result.getValue(result.columns[8])),
				"tax_id":			tax_id,
				"discount_line":		dwg.is_discount(item_id),
				"tax_line":			dwg.get_bit(tax_line),
				"main_line":			dwg.get_bit(main_line),
				"memo":				result.getValue(result.columns[12]).replace(/'/g, "’"),
				"is_mrc":			dwg.is_mrc(class_id)
			});
	        return true;
		});
		return items;
	}
	
	return {
		get_opportunity: get_opportunity
	};
});