/**
 * @NApiVersion 2.x
 */

define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", 
	    "/SuiteScripts/clgx/libraries/moment.min"], 
function(record, search, dwg, moment) {
	function get_credit(record_id) {
		var cm = record.load({ type: record.Type.CREDIT_MEMO, id: record_id });
		
		var credit = {};
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_creditmemo" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		
		results.run().each(function(result) {
			credit = {
				"creditmemo_id"        : parseInt(result.getValue(result.columns[0])),
				"creditmemo"           : result.getValue(result.columns[1]),
				"external_id"          : result.getValue(result.columns[2]),
				"status_id"            : result.getValue(result.columns[3]), 
				"customer_id"          : parseInt(result.getValue(result.columns[4])),
				"subsidiary_id"        : parseInt(result.getValue(result.columns[5])),
				"location_id"          : parseInt(result.getValue(result.columns[6])),
				"account_id"           : parseInt(result.getValue(result.columns[7])),
				"currency_id"          : parseInt(result.getValue(result.columns[8])),
				"date"                 : dwg.sqlDate(result.getValue(result.columns[9])),
				"accounting_period_id" : parseInt(result.getValue(result.columns[10])),
				"credit_reason_id"     : parseInt(result.getValue(result.columns[11])),
				"memo"                 : result.getValue(result.columns[12]),
				"unapplied"            : parseFloat(cm.getValue({ fieldId: "unapplied" })),
				"items"                : get_credit_items(record_id, cm),
				"applied_to"           : get_credit_applied(record_id)
			};
			
	        return true;
		});
		
		return [credit];
	}
	
	
	function get_credit_items(record_id, cm) {
		var items       = [];
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_creditmemo_item" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			var item_id   = parseInt(result.getValue(result.columns[2]));
			
			var tax_id    = parseInt(result.getValue(result.columns[6]));
			var tax_line  = dwg.get_bit(result.getValue(result.columns[7]));
			
			var main_line = false;
			var main      = result.getValue(result.columns[8]);
			
			if(tax_line) {
				item_id = null;
				tax_id = null;
			}
			
			if(main) {
				main_line = true;
			}
			
			items.push({
				"creditmemo_id"  : parseInt(result.getValue(result.columns[0])),
				"line_id"        : parseInt(result.getValue(result.columns[1])),
				"item_id"        : item_id, //parseInt(result.getValue(result.columns[2]))
				"quantity"       : parseInt(result.getValue(result.columns[3])),
				"rate"           : parseFloat(result.getValue(result.columns[4])),
				"amount"         : parseFloat(result.getValue(result.columns[5])),
				"tax_id"         : tax_id, //parseInt(result.getValue(result.columns[6]));
				"tax_line"       : tax_line, //result.getValue(result.columns[7]);
				"main_line"      : dwg.get_bit(result.getValue(result.columns[8])),
				"description"    : result.getValue(result.columns[9]).replace(/'/g, "’")
			});
			
	        return true;
		});
		
		return items;
	}
	
	
	function get_credit_applied(record_id) {
		var applied_to  = new Array();
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_creditmemo_apply" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		
		results.run().each(function(result) {
			applied_to.push({
				"creditmemo_id"     : parseInt(result.getValue(result.columns[0])),
				"applied_to_date"   : dwg.sqlDate(result.getValue(result.columns[1])),
				"applied_to_type"   : result.getValue(result.columns[2]),
				"applied_to_id"     : parseInt(result.getValue(result.columns[3])),
				"applied_to_refnum" : parseInt(result.getValue(result.columns[4])),
				"amount"            : parseFloat(result.getValue(result.columns[5]))
			});
			
			return true;
	    });
		
		return applied_to;
	}
	
	
	return {
		get_credit: get_credit
	};
});