/**
 * @NApiVersion 2.x
 */


define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {

	function get_salesorder(record_id) {
		var sales_order_object = record.load({ type: "salesorder", id: record_id });
		var commercial_terms = sales_order_object.getValue("custbody_clgx_cust_commerical_terms");
		
		var sales_order = {};
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_salesorder" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			
			var opportunity_id = parseInt(result.getValue(result.columns[4]));
			var proposal_id = parseInt(result.getValue(result.columns[5]));
			
			if(opportunity_id == proposal_id){
				proposal_id = null;
			}
			var services = [];
			var itemsObj = get_salesorder_items(record_id);
			var items = itemsObj.items;
			var ids = itemsObj.services;
			if(ids && ids.length > 0){
				services = get_salesorder_services(ids)
			}
			
			var waiting_hours_start = result.getValue(result.columns[55]);
			if(waiting_hours_start == '' || waiting_hours_start == '- None -'){
				waiting_hours_start = null;
			}
			var waiting_hours_finish = result.getValue(result.columns[56]);
			if(waiting_hours_finish == '' || waiting_hours_finish == '- None -'){
				waiting_hours_finish = null;
			}
			
			
			var totals = dwg.get_totals(items);
			
			sales_order = {
				"sales_order_id":			parseInt(result.getValue(result.columns[0])),
				"sales_order":				result.getValue(result.columns[1]),
				"external_id":				result.getValue(result.columns[2]),
				"customer_id":				parseInt(result.getValue(result.columns[3])),
				"opportunity_id":			opportunity_id, //column[4]
				"proposal_id":				proposal_id, //column[5]
				"partner_id":               parseInt(result.getValue(result.columns[57])),
				"location_id":				parseInt(result.getValue(result.columns[6])),
				"clocation_id":				parseInt(result.getValue(result.columns[7])),
				"sales_rep_id":				parseInt(result.getValue(result.columns[8])),
				"engineer_id":				parseInt(result.getValue(result.columns[9])),
				"technician_id":				parseInt(result.getValue(result.columns[10])),
				"currency_id":				parseInt(result.getValue(result.columns[11])),
				"status_id":					result.getValue(result.columns[12]) || null,
				"so_order_date":				dwg.sqlDate(result.getValue(result.columns[13])),
				"invoice_start_date":		dwg.sqlDate(result.getValue(result.columns[14])),
				"sales_effective_date":		dwg.sqlDate(result.getValue(result.columns[15])),
				"renew_start_date":			dwg.sqlDate(result.getValue(result.columns[16])),
				"current_end_date":			dwg.sqlDate(result.getValue(result.columns[17])),
				"target_install_date":		dwg.sqlDate(result.getValue(result.columns[18])),
				"actual_install_date":		dwg.sqlDate(result.getValue(result.columns[19])),
				"contract_start_date":		dwg.sqlDate(result.getValue(result.columns[20])),
				"contract_end_date":			dwg.sqlDate(result.getValue(result.columns[21])),
				"accelerator":				result.getValue(result.columns[22]),
				"accelerator_date":			dwg.sqlDate(result.getValue(result.columns[23])),
				"accelerator_type_id":		parseInt(result.getValue(result.columns[24])),
				"ip_burst":					dwg.get_bit(result.getValue(result.columns[25])),
				"ip_burst_rate":				parseFloat(result.getValue(result.columns[26])),
				"ip_gb_rate":				parseFloat(result.getValue(result.columns[27])),
				"metered_power":				dwg.get_bit(result.getValue(result.columns[28])),
				"metered_power_type_id":		parseInt(result.getValue(result.columns[29])),
				"metered_power_utilization":	parseFloat(result.getValue(result.columns[30])),
				"metered_power_commit":		parseFloat(result.getValue(result.columns[31])),
				"metered_power_overage_rate":parseFloat(result.getValue(result.columns[32])),
				"hd_colo":					dwg.get_bit(result.getValue(result.columns[33])),
				"hd_colo_high_utilization":	parseFloat(result.getValue(result.columns[34])),
				"hd_colo_high_rate":			parseFloat(result.getValue(result.columns[35])),
				"hd_colo_ultra_high_utilization":parseFloat(result.getValue(result.columns[36])),
				"hd_colo_ultra_high_rate":	parseFloat(result.getValue(result.columns[37])),
				"legacy_utilization":		parseFloat(result.getValue(result.columns[38])),
				"legacy_rate":				parseFloat(result.getValue(result.columns[39])),
				"incremental_mrc":			parseFloat(result.getValue(result.columns[40])),
				"netex_vendor":				dwg.get_null(result.getValue(result.columns[41])),
				"netex_account":				dwg.get_null(result.getValue(result.columns[42])),
				"audited_prod_management":	dwg.get_bit(result.getValue(result.columns[43])),
				"box_signed_so":				dwg.get_bit(result.getValue(result.columns[44])),
				"no_partner_commissions":	dwg.get_bit(result.getValue(result.columns[45])),
				"contract_terms_id":			parseInt(result.getValue(result.columns[46])),
				"contract_cycle":			result.getValue(result.columns[47]),
				"legacy_so":					dwg.get_null(result.getValue(result.columns[48])),
				"ropportunity_id":			parseInt(result.getValue(result.columns[49])),
				"rproposal_id":				parseInt(result.getValue(result.columns[50])),
				"rsales_order_id":			parseInt(result.getValue(result.columns[51])),
				"renewed_from_sos":         result.getValue(result.columns[52]),
				"special_project":			dwg.get_bit(result.getValue(result.columns[53])),
				"waiting_hours":			parseInt(result.getValue(result.columns[54])),
				"waiting_hours_start":		dwg.sqlDate(waiting_hours_start,1),
				"waiting_hours_finish":		dwg.sqlDate(waiting_hours_finish,1),
				"service_terms":			dwg.get_null(result.getValue(result.columns[58])),
				"channel_rep_id":           parseInt(result.getValue(result.columns[59])),
				"channel_rep_name":         result.getText(result.columns[59]),
				"order_notes":              sales_order_object.getValue("custbody_clgx_order_notes"),
				"commercial_terms":         commercial_terms,
				"nrc_total":					totals.nrc_total,
				"mrc_total":					totals.mrc_total,
				"sub_total":					totals.sub_total,
				"discount":					totals.discount,
				"tax_total":					totals.tax_total,
				"total":						totals.total,
				"services":					services,
				"items":						items
			};
	        return true;
		});
		return [sales_order];
	}
	
	function get_salesorder_services(ids) {
		var services = [];
		var results = search.load({ type: 'job', id: "customsearch_clgx_dw_service" });
		//results.filters = [search.createFilter({ name: "custentity_cologix_service_order", operator: "ANYOF", values: record_id })];
		results.filters.push(search.createFilter({ name: "internalid", operator: "ANYOF", values: ids }));
		results.run().each(function(result) {
			var names = result.getValue(result.columns[1]).split(":");
			var name = (names[names.length-1]).trim();
			services.push({
				"service_id":		parseInt(result.getValue(result.columns[0])),
				"service":			name,
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"customer_id":		parseInt(result.getValue(result.columns[4])),
				"sales_order_id":	parseInt(result.getValue(result.columns[5])),
				"subsidiary_id":	parseInt(result.getValue(result.columns[6])),
				"facility_id":		parseInt(result.getValue(result.columns[7]))
			});
	        return true;
		});
		return services;
	}
	
	function get_salesorder_items(record_id) {
		var items = [];
		var services = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_salesorder_item" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			
			var item_id = parseInt(result.getValue(result.columns[3]));
			var tax_id = parseInt(result.getValue(result.columns[10]));
			var tax_line = result.getValue(result.columns[11]);
			var class_id = parseInt(result.getValue(result.columns[18]));
			
			var amount = parseFloat(result.getValue(result.columns[9]));
			var sys_amount = parseFloat(result.getValue(result.columns[14]));
			
			var main_line = false;
			var main = result.getValue(result.columns[12]);
			if(main){
				main_line = true;
				amount = dwg.round(sys_amount / 120);
			}
			if (tax_line){
				item_id = null;
				tax_id = null;
			}
			
			var service_id = parseInt(result.getValue(result.columns[4]));
			if(service_id){
				services.push(service_id);
			}
			
			items.push({
				"sales_order_id":	parseInt(result.getValue(result.columns[0])),
				"line_id":			parseInt(result.getValue(result.columns[1])),
				"line_key":			parseInt(result.getValue(result.columns[2])),
				"item_id":			item_id,
				"service_id":		service_id,
				"location_id":		parseInt(result.getValue(result.columns[5])),
				"schedule_id":		parseInt(result.getValue(result.columns[6])),
				"quantity":			parseFloat(result.getValue(result.columns[7])),
				"rate":				parseFloat(result.getValue(result.columns[8])),
				"amount":			amount,
				"tax_id":			tax_id,
				"discount_line":		dwg.is_discount(item_id),
				"tax_line":			dwg.get_bit(tax_line),
				"main_line":			dwg.get_bit(main_line),
				"sys_quantity":		parseFloat(result.getValue(result.columns[13])),
				"sys_amount":		sys_amount,
				"closed":			dwg.sqlDate(result.getValue(result.columns[15])),
				"old_rate":			parseFloat(result.getValue(result.columns[16])),
				"memo":				result.getValue(result.columns[17]).replace(/'/g, "’"),
				"is_mrc":			dwg.is_mrc(class_id)
			});
	        return true;
		});
		return {"items":items, "services":services};
	}
	
	return {
		get_salesorder: get_salesorder
	};
});