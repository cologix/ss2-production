/**
 * @NApiVersion 2.x
 */

define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", 
	    "/SuiteScripts/clgx/libraries/moment.min"], 
function(record, search, dwg, moment) {
	function get_creditmemo(record_id) {
		var rec = record.load({ type: record.Type.CREDIT_MEMO, id: record_id });
		
		var credit_memo = {};
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_creditmemo" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		
		results.run().each(function(result) {
			
			var apply_items = [];
			var lines = rec.getLineCount({sublistId: 'apply'});
			for ( var i = 0; i < lines; i++ ) {
				var amount = parseFloat(rec.getSublistValue({sublistId: 'apply',fieldId: 'amount',line: i}));
				var apply = dwg.get_bit(rec.getSublistValue({sublistId: 'apply',fieldId: 'apply',line: i}));
				if(apply){
					apply_items.push({
						"creditmemo_id"     : record_id,
						"line_id"           : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'line',line: i})),
						"applied_to_id"     : parseInt(rec.getSublistValue({sublistId: 'apply',fieldId: 'internalid',line: i})),
						"applied_to_refnum" : dwg.get_null(rec.getSublistValue({sublistId: 'apply',fieldId: 'refnum',line: i})),
						"applied_to_type"   : dwg.get_null(rec.getSublistValue({sublistId: 'apply',fieldId: 'trantype',line: i})),
						"applied_to_date"   : dwg.sqlDate(rec.getSublistValue({sublistId: 'apply',fieldId: 'applydate',line: i})),
						//"apply"             : apply,
						"amount"            : amount
					});
				}
			}
			
			var credit_reason_id = parseInt(result.getValue(result.columns[11]));
			if(credit_reason_id < 1){
				credit_reason_id = null;
			}
			
			credit_memo = {
				"creditmemo_id"        : parseInt(result.getValue(result.columns[0])),
				"creditmemo"           : result.getValue(result.columns[1]),
				"external_id"          : result.getValue(result.columns[2]),
				"status_id"            : result.getValue(result.columns[3]), 
				"customer_id"          : parseInt(result.getValue(result.columns[4])),
				"subsidiary_id"        : parseInt(result.getValue(result.columns[5])),
				"location_id"          : parseInt(result.getValue(result.columns[6])),
				"account_id"           : parseInt(result.getValue(result.columns[7])),
				"currency_id"          : parseInt(result.getValue(result.columns[8])),
				"date"                 : dwg.sqlDate(result.getValue(result.columns[9])),
				"accounting_period_id" : parseInt(result.getValue(result.columns[10])),
				"credit_reason_id"     : credit_reason_id,
				"memo"                 : result.getValue(result.columns[12]),
				"unapplied"            : parseFloat(rec.getValue({ fieldId: "unapplied" })),
				"fx_rate"              : parseFloat(result.getValue(result.columns[13])),
				"items"                : get_creditmemo_items(record_id),
				//"applied_to"         : get_creditmemo_applied(record_id),
				"applied_to"           : apply_items
			};
			
	        return true;
		});
		
		return [credit_memo];
	}
	
	
	function get_creditmemo_items(record_id) {
		var items       = [];
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_creditmemo_item" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			
			var item_id   = parseInt(result.getValue(result.columns[2]));
			
			var tax_id    = parseInt(result.getValue(result.columns[7]));
			var tax_line  = dwg.get_bit(result.getValue(result.columns[8]));
			
			var main_line = false;
			var main      = result.getValue(result.columns[9]);
			
			if(tax_line) {
				item_id = null;
				tax_id = null;
			}
			
			if(main) {
				main_line = true;
			}
			
			if(item_id != -6){
				items.push({
					"creditmemo_id"  : parseInt(result.getValue(result.columns[0])),
					"line_id"        : parseInt(result.getValue(result.columns[1])),
					"item_id"        : item_id, 
					"quantity"       : parseInt(result.getValue(result.columns[3])),
					"rate"           : parseFloat(result.getValue(result.columns[4])),
					"amount"         : parseFloat(result.getValue(result.columns[5])),
					"fxamount"       : parseFloat(result.getValue(result.columns[6])),
					"tax_id"         : tax_id, //parseInt(result.getValue(result.columns[6]));
					"tax_line"       : tax_line, //result.getValue(result.columns[7]);
					"main_line"      : dwg.get_bit(result.getValue(result.columns[9])),
					"description"    : result.getValue(result.columns[10]).replace(/'/g, "’")
				});
			}
	        return true;
		});
		
		return items;
	}
	
	
	function get_creditmemo_applied(record_id) {
		var applied_to  = [];
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_creditmemo_apply" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			var applied_to_id = parseInt(result.getValue(result.columns[3]));
			var amount = parseFloat(result.getValue(result.columns[5]));
			if(applied_to_id && amount){
				applied_to.push({
					"creditmemo_id"     : parseInt(result.getValue(result.columns[0])),
					"applied_to_date"   : dwg.sqlDate(result.getValue(result.columns[1])),
					"applied_to_type"   : result.getValue(result.columns[2]),
					"applied_to_id"     : applied_to_id,
					"applied_to_refnum" : parseInt(result.getValue(result.columns[4])),
					"amount"            : amount
				});
			}
			return true;
	    });
		
		return applied_to;
	}
	
	
	return {
		get_creditmemo: get_creditmemo
	};
});