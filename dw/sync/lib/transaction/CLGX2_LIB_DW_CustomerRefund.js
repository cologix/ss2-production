/**
 * @NApiVersion 2.x
 */

define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", 
	    "/SuiteScripts/clgx/libraries/moment.min"], 
function(record, search, dwg, moment) {
	function get_customerrefund(record_id) {
		
		var rec = record.load({ type: 'customerrefund', id: record_id });
		var ar_account = parseInt(rec.getValue({ fieldId: "aracct" }));
		
		var customer_refund = {};
		var results     = search.load({ type: "transaction", id: "customsearch_clgx_dw_customerrefund" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		
		results.run().each(function(result) {
			customer_refund = {
				"customerrefund_id"        : parseInt(result.getValue(result.columns[0])),
				"customerrefund"           : dwg.get_null(result.getValue(result.columns[1])),
				"transaction_nbr"          : parseInt(result.getValue(result.columns[2])),
				"external_id"              : result.getValue(result.columns[3]),
				"customer_id"              : parseInt(result.getValue(result.columns[4])),
				"subsidiary_id"            : parseInt(result.getValue(result.columns[5])),
				"location_id"              : parseInt(result.getValue(result.columns[6])),
				"department_id"            : parseInt(result.getValue(result.columns[7])),
				"class_id"                 : parseInt(result.getValue(result.columns[8])),
				"accounting_period_id"     : parseInt(result.getValue(result.columns[9])),
				"account_id"               : parseInt(result.getValue(result.columns[10])),
				"ar_account_id"            : ar_account,
				"currency_id"              : parseInt(result.getValue(result.columns[11])),
				"date"                     : dwg.sqlDate(result.getValue(result.columns[12])),
				"amount"                   : parseFloat(result.getValue(result.columns[13])),
				"check_nbr"                : dwg.get_null(result.getValue(result.columns[14])),
				"memo"                     : result.getValue(result.columns[15])
			};
			
	        return true;
		});
		
		return [customer_refund];
	}

	return {
		get_customerrefund : get_customerrefund
	};
});