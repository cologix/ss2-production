/**
 * @NApiVersion 2.x
 */


define(["N/record", "N/search", "/SuiteScripts/clgx/libraries/moment.min", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, moment, dwg) {
	
	function get_invoice(record_id) {
		var invoice = {};
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		
		results.run().each(function(result) {

			//var prev_sales_order_id = null;
			var prev_invoice_id = null;
			var prev_invoice = null;
			
			var inv_date = result.getValue(result.columns[12]);
			var invoice_date  = moment(inv_date).format('MM/DD/YYYY')
			var prev_invoice_date = moment(inv_date).subtract(1, 'months').format('MM/DD/YYYY');
			var prev_start = moment(inv_date).subtract('months',1).subtract('days',1).format('M/D/YYYY');
		    var prev_end = moment(inv_date).subtract('months',1).endOf('month').format('M/D/YYYY');
		    
			var sales_order_id = parseInt(result.getValue(result.columns[6]));
			var type_id = '';
			if(sales_order_id){
				var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_transaction_type" });
				results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: sales_order_id })];
				results.run().each(function(result) {
					type_id = result.getValue(result.columns[1]);
			        return true;
				});
				if(type_id != 'SalesOrd') { // nullify CreatedFrom if is not a SalesOrder
					sales_order_id = null;
				} else {
					// attempt to find the previous month invoice
					//prev_invoice_id = get_prev_invoice_id(sales_order_id,prev_invoice_date);
					//if(prev_invoice_id){
					//	prev_invoice = get_prev_invoice(prev_invoice_id);
					//}
				}
			}
			
			var items = get_invoice_items(record_id,prev_start,prev_end);
			var totals = dwg.get_totals(items);
			
			var prev_status_id = null;
			var prev_cinvoice_id = null;
			var prev_accounting_period_id = null;
			var prev_date = null;
			
			var prev_nrc_total = 0;
			var prev_mrc_total = 0;
			var prev_sub_total = 0;
			var prev_tax_total = 0;
			var prev_total = 0;
			
			var diff_nrc_total = 0;
			var diff_mrc_total = 0;
			var diff_sub_total = 0;
			var diff_tax_total = 0;
			var diff_total = 0;
			var change_type_id = null;
			
			/*
			if(prev_invoice){
				prev_status_id = prev_invoice.status_id;
				prev_cinvoice_id = prev_invoice.cinvoice_id;
				prev_accounting_period_id = prev_invoice.accounting_period_id;
				prev_date = prev_invoice.date;
				prev_nrc_total = prev_invoice.nrc_total;
				prev_mrc_total = prev_invoice.mrc_total;
				prev_sub_total = prev_invoice.sub_total;
				prev_tax_total = prev_invoice.tax_total;
				prev_total = prev_invoice.total;
			}
			
			var diff_nrc_total = dwg.round(totals.nrc_total - prev_nrc_total);
			var diff_mrc_total = dwg.round(totals.mrc_total - prev_mrc_total);
			var diff_sub_total = dwg.round(totals.sub_total - prev_sub_total);
			var diff_tax_total = dwg.round(totals.tax_total - prev_tax_total);
			var diff_total = dwg.round(totals.total - prev_total);
			
			var change_type_id = 1; // No Change
			if (!prev_invoice_id){
				change_type_id = 2; // New logo
			}
			else if(diff_sub_total != 0){
				if(diff_sub_total > 0){
					change_type_id = 6; // Price Increase
				}
				if(diff_sub_total < 0){
					change_type_id = 7; // Price Decrease
				}
			}
			*/
			
			invoice = {
				"invoice_id"                 : parseInt(result.getValue(result.columns[0])),
				"invoice"                    : result.getValue(result.columns[1]),
				"external_id"                : result.getValue(result.columns[2]),
				"customer_id"                : parseInt(result.getValue(result.columns[3])),
				"location_id"                : parseInt(result.getValue(result.columns[4])),
				"clocation_id"               : parseInt(result.getValue(result.columns[5])),
				"sales_order_id"             : sales_order_id,
				"currency_id"                : parseInt(result.getValue(result.columns[7])),
				"status_id"                  : result.getValue(result.columns[8]),
				"cinvoice_id"                : parseInt(result.getValue(result.columns[9])),
				"accounting_period_id"       : parseInt(result.getValue(result.columns[10])),
				"due_date"                   : dwg.sqlDate(result.getValue(result.columns[11])),
				"date"                       : dwg.sqlDate(invoice_date),
				"po_nbr"                     : result.getValue(result.columns[13]),
				"nrc_total"                  : totals.nrc_total,
				"mrc_total"                  : totals.mrc_total,
				"sub_total"                  : totals.sub_total,
				"discount"                   : totals.discount,
				"tax_total"                  : totals.tax_total,
				"total"                      : totals.total,
				"fx_rate"                    : parseFloat(result.getValue(result.columns[15])),
				
				"prev_invoice_id"            : prev_invoice_id,
				//"prev_location_id"           : prev_location_id,
				//"prev_sales_order_id"        : prev_sales_order_id,
				"prev_date"                  : prev_date,
				"prev_status_id"             : prev_status_id,
				"prev_cinvoice_id"           : prev_cinvoice_id,
				"prev_accounting_period_id"  : prev_accounting_period_id,
				"prev_nrc_total"             : prev_nrc_total,
				"prev_mrc_total"             : prev_mrc_total,
				"prev_sub_total"             : prev_sub_total,
				"prev_tax_total"             : prev_tax_total,
				"prev_total"                 : prev_total,
				"diff_nrc_total"             : diff_nrc_total,
				"diff_mrc_total"             : diff_mrc_total,
				"diff_sub_total"             : diff_sub_total,
				"diff_tax_total"             : diff_tax_total,
				"diff_total"                 : diff_total,
				"change_type_id"             : change_type_id,
				
				"items"                      : items
			};
	        return true;
		});
		return [invoice];
	}
	
	function get_invoice_items(record_id,prev_start,prev_end) {
		
		var items = [];
		var cinvoice = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice_item" });
		cinvoice.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		cinvoice.run().each(function(cresult) {
			
			var service_id = parseInt(cresult.getValue(cresult.columns[4]));
			var line_id = parseInt(cresult.getValue(cresult.columns[1]));
			var item_id = parseInt(cresult.getValue(cresult.columns[3]));
			var tax_id = parseInt(cresult.getValue(cresult.columns[10]));
			var tax_line = cresult.getValue(cresult.columns[11]);
			var class_id = parseInt(cresult.getValue(cresult.columns[14]));
			var is_mrc = dwg.is_mrc(class_id);
			var memo = cresult.getValue(cresult.columns[13]).replace(/'/g, "’");
			
			var main_line = false;
			var main = cresult.getValue(cresult.columns[12]);
			if(main){
				main_line = true;
			}
			if (tax_line){
				item_id = null;
				tax_id = null;
			}
			if(item_id != -6){
				
				var prev_invoice_id = null;
				var prev_line_id = null;
				var prev_line_key = null;
				var prev_quantity = 0;
				var prev_rate = 0;
				var prev_amount = 0;
				var prev_count = 0;
				
				if(service_id && item_id){
					var pinvoice = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice_item" });
					pinvoice.filters.push(search.createFilter({ name: "custcol_clgx_so_col_service_id", operator: "is", values: service_id }));
					pinvoice.filters.push(search.createFilter({ name: "item", operator: "is", values: item_id }));
					pinvoice.filters.push(search.createFilter({ name: "trandate", operator: "after", values: prev_start }));
					pinvoice.filters.push(search.createFilter({ name: "trandate", operator: "before", values: prev_end }));
					pinvoice.run().each(function(presult) {
						prev_invoice_id = parseInt(presult.getValue(presult.columns[0]));
						prev_line_id = parseInt(presult.getValue(presult.columns[1]));
						prev_line_key = parseInt(presult.getValue(presult.columns[2]));
						prev_quantity = parseFloat(presult.getValue(presult.columns[7]));
						prev_rate = parseFloat(presult.getValue(presult.columns[8]));
						prev_amount = parseFloat(presult.getValue(presult.columns[9]));
						prev_count += 1;
				        return true;
					});
				}
				
				var quantity = parseFloat(cresult.getValue(cresult.columns[7]));
				var rate = parseFloat(cresult.getValue(cresult.columns[8]));
				var amount = parseFloat(cresult.getValue(cresult.columns[9]));
				
				var diff_quantity = 0;
				var diff_rate = 0;
				var diff_amount = 0;
				if(!main_line && !tax_line && is_mrc){
					diff_quantity = dwg.round(quantity - prev_quantity);
					diff_rate = dwg.round(rate - prev_rate);
					diff_amount = dwg.round(amount - prev_amount);
				}

				var change_type_id = null;
				if (main_line || tax_line || !is_mrc){ // if main line, tax line not mrc, then no change
					change_type_id = null;
				}
				else if (memo.indexOf("Prior Month") != -1 || memo.indexOf("précédent pour") != -1){
					change_type_id = 7; // Pro Rate
				}
				else if (item_id == 210 || item_id == 574 || item_id == 612 || item_id == 758 || item_id == 762){
					change_type_id = 9; // Usage
				}
				else {
					if (prev_invoice_id){ // if  previous invoice found, then not new
						
						if(diff_quantity == 0 && diff_amount == 0){
							change_type_id = 1; // No Change
						}
						else if(diff_quantity > 0 && diff_amount > 0){
							change_type_id = 2; // Upgrade
						}
						else if(diff_quantity < 0 && diff_amount < 0){
							change_type_id = 3; // Down grade
						}
						else if(diff_quantity == 0 && diff_amount > 0){
							change_type_id = 4; // Price Increase
						}
						else if(diff_quantity == 0 && diff_amount < 0){
							change_type_id = 5; // Price Decrease
						}
						else{
							change_type_id = 0; // Unknown
						}
					} 
					else {
						change_type_id = 6; // New Logo
					}
				}
				
				items.push({
					"invoice_id"        : parseInt(cresult.getValue(cresult.columns[0])),
					"line_id"           : line_id,
					"line_key"          : parseInt(cresult.getValue(cresult.columns[2])),
					"item_id"           : item_id,
					"service_id"        : service_id,
					"location_id"       : parseInt(cresult.getValue(cresult.columns[5])),
					"schedule_id"       : parseInt(cresult.getValue(cresult.columns[6])),
					"quantity"          : quantity,
					"rate"              : rate,
					"amount"            : amount,
					"tax_id"            : tax_id,
					"discount_line"     : dwg.is_discount(item_id),
					"tax_line"          : dwg.get_bit(tax_line),
					"main_line"         : dwg.get_bit(main_line),
					"memo"              : memo,
					"is_mrc"            : is_mrc,
					
					"prev_invoice_id"   : prev_invoice_id,
					"prev_line_id"      : prev_line_id,
					"prev_line_key"     : prev_line_key,
					"prev_quantity"     : prev_quantity,
					"prev_rate"         : prev_rate,
					"prev_amount"       : prev_amount,
					"prev_count"        : prev_count,
					"prev_start"        : prev_start,
					"prev_end"          : prev_end,
					
					"diff_quantity"     : diff_quantity,
					"diff_rate"         : diff_rate,
					"diff_amount"       : diff_amount,
					"change_type_id"    : change_type_id,
				});
			}

	        return true;
		});
		return items;
	}
	
	
	// =================================================================================================================================== //
	
	function get_prev_invoice_id(sales_order_id, prev_invoice_date) {
		var prev_invoice_id = null;
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice_ids" });
		results.filters = [
			search.createFilter({ name: "createdfrom", operator: "ANYOF", values: sales_order_id }),
			search.createFilter({ name: "trandate", operator: "ON", values: prev_invoice_date })
		];
		results.run().each(function(result) {
			prev_invoice_id = parseInt(result.getValue(result.columns[0]));
			return true;
		});
		return prev_invoice_id;
	}
	
	function get_prev_invoice(record_id) {
		
		var prev_invoice = {};
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			
			var items = get_invoice_prev_items(record_id);
			var totals = dwg.get_totals(items);
			
			var invoice_date  = moment(result.getValue(result.columns[12])).format('MM/DD/YYYY')
			var sales_order_id = parseInt(result.getValue(result.columns[6]));
			var type_id = '';
			if(sales_order_id){
				var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_transaction_type" });
				results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: sales_order_id })];
				results.run().each(function(result) {
					type_id = result.getValue(result.columns[1]);
			        return true;
				});
				if(type_id != 'SalesOrd') {
					sales_order_id =null;
				} 
			}
			prev_invoice = {
				"invoice_id"            : parseInt(result.getValue(result.columns[0])),
				"invoice"               : result.getValue(result.columns[1]),
				"external_id"           : result.getValue(result.columns[2]),
				"customer_id"           : parseInt(result.getValue(result.columns[3])),
				"location_id"           : parseInt(result.getValue(result.columns[4])),
				"clocation_id"          : parseInt(result.getValue(result.columns[5])),
				"sales_order_id"        : sales_order_id,
				"currency_id"           : parseInt(result.getValue(result.columns[7])),
				"status_id"             : result.getValue(result.columns[8]),
				"cinvoice_id"           : parseInt(result.getValue(result.columns[9])),
				"accounting_period_id"  : parseInt(result.getValue(result.columns[10])),
				"due_date"              : dwg.sqlDate(result.getValue(result.columns[11])),
				"date"                  : dwg.sqlDate(invoice_date),
				"po_nbr"                : result.getValue(result.columns[13]),
				"nrc_total"             : totals.nrc_total,
				"mrc_total"             : totals.mrc_total,
				"sub_total"             : totals.sub_total,
				"discount"              : totals.discount,
				"tax_total"             : totals.tax_total,
				"total"                 : totals.total,
				"items"                 : items
			};
	        return true;
		});
		return prev_invoice;
	}
	
	function get_invoice_prev_items(record_id) {
		
		var items = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_invoice_item" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			
			var item_id = parseInt(result.getValue(result.columns[3]));
			var tax_id = parseInt(result.getValue(result.columns[10]));
			var tax_line = result.getValue(result.columns[11]);
			var class_id = parseInt(result.getValue(result.columns[14]));
			
			var main_line = false;
			var main = result.getValue(result.columns[12]);
			if(main){
				main_line = true;
			}
			if (tax_line){
				item_id = null;
				tax_id = null;
			}
			if(item_id != -6){
				items.push({
					"invoice_id"       : parseInt(result.getValue(result.columns[0])),
					"line_id"          : parseInt(result.getValue(result.columns[1])),
					"line_key"         : parseInt(result.getValue(result.columns[2])),
					"item_id"          : item_id,
					"service_id"       : parseInt(result.getValue(result.columns[4])),
					"location_id"      : parseInt(result.getValue(result.columns[5])),
					"schedule_id"      : parseInt(result.getValue(result.columns[6])),
					"quantity"         : parseFloat(result.getValue(result.columns[7])),
					"rate"             : parseFloat(result.getValue(result.columns[8])),
					"amount"           : parseFloat(result.getValue(result.columns[9])),
					"tax_id"           : tax_id,
					"discount_line"    : dwg.is_discount(item_id),
					"tax_line"         : dwg.get_bit(tax_line),
					"main_line"        : dwg.get_bit(main_line),
					"memo"             : result.getValue(result.columns[13]).replace(/'/g, "’"),
					"is_mrc"           : dwg.is_mrc(class_id)
				});
			}

	        return true;
		});
		return items;
	}
	

	
	// =================================================================================================================================== //
	
	function get_cinvoice(record_id) {
		var cinvoice = {};
		var results = search.load({ type: 'customrecord_clgx_consolidated_invoices', id: "customsearch_clgx_dw_cinvoice" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			
			var cinvoice_id = parseInt(result.getValue(result.columns[0]));
			
			var ids = (result.getValue(result.columns[25])).split(",");
			var invoices = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				var invoice_id = parseInt(ids[i]);
				if(invoice_id){
					invoices.push({
						"cinvoice_id": 			cinvoice_id,
						"invoice_id": 			invoice_id
					})
				}
			}
			
			var ids = (result.getValue(result.columns[26])).split(",");
			var contacts = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				var contact_id = parseInt(ids[i]);
				if(contact_id){
					contacts.push({
						"cinvoice_id": 			cinvoice_id,
						"contact_id": 			contact_id
					})
				}
			}
			
			cinvoice = {
				"cinvoice_id"              : cinvoice_id,
				"cinvoice"                 : result.getValue(result.columns[1]),
				"external_id"              : result.getValue(result.columns[2]),
				"customer_id"              : parseInt(result.getValue(result.columns[3])),
				"clocation_id"             : parseInt(result.getValue(result.columns[4])),
				"output_id"                : parseInt(result.getValue(result.columns[5])),
				"date"                     : dwg.sqlDate(result.getValue(result.columns[6])),
				"pdf_file"                 : parseInt(result.getValue(result.columns[7])),
				"json_file"                : parseInt(result.getValue(result.columns[8])),
				"month_id"                 : result.getValue(result.columns[9]),
				"year"                     : parseInt(result.getValue(result.columns[10])),
				"month_displayed_id"       : parseInt(result.getValue(result.columns[11])),
				"year_displayed"           : parseInt(result.getValue(result.columns[12])),
				"subtotal"                 : parseFloat(result.getValue(result.columns[13])),
				"tax1"                     : parseFloat(result.getValue(result.columns[14])),
				"tax2"                     : parseFloat(result.getValue(result.columns[15])),
				"total"                    : parseFloat(result.getValue(result.columns[16])),
				"balance"                  : parseFloat(result.getValue(result.columns[17])),
				"currency"                 : result.getValue(result.columns[18]),
				"credit_card"              : result.getValue(result.columns[19]),
				"credit_card_transaction"  : result.getValue(result.columns[20]),
				"churn"                    : result.getValue(result.columns[21]),
				"cons_inv_churn"           : result.getValue(result.columns[22]),
				"ar_reminder"              : result.getValue(result.columns[23]),
				"processed"                : result.getValue(result.columns[24]),
				"invoices"                 : invoices,
				"contacts"                 : contacts
			};
	        return true;
		});
		return [cinvoice];
	}

	
	return {
		get_invoice: get_invoice,
		get_cinvoice: get_cinvoice
	};
});