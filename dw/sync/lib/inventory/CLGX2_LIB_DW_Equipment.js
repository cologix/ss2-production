/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_equipment(record_id) {
		var equipment     = {};
		var results     = search.load({ type: "customrecord_cologix_equipment", id: "customsearch_clgx_dw_equipment" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			equipment = {
				"equipment_id"                  : parseInt(result.getValue(result.columns[0])),
				"equipment"                     : result.getValue(result.columns[1]),
				/*"equipment_name_id"             : parseInt(result.getValue(result.columns[24])),*/
				"inactive"                      : dwg.get_bit(result.getValue(result.columns[2])),
				"external_id"                   : result.getValue(result.columns[3]),
				"parent_id"                     : parseInt(result.getValue(result.columns[4])),
				"function_id"                   : parseInt(result.getValue(result.columns[5])),
				"model_id"                      : parseInt(result.getValue(result.columns[6])),
				"type_id"                       : parseInt(result.getValue(result.columns[7])),
				"status_id"                     : parseInt(result.getValue(result.columns[8])),
				"facility_id"                   : parseInt(result.getValue(result.columns[9])),
				"space_id"                      : parseInt(result.getValue(result.columns[10])),
				"rack_unit_id"                  : parseInt(result.getValue(result.columns[11])),
				"ip_addr_id"                    : parseInt(result.getValue(result.columns[12])),
				"make_id"                       : parseInt(result.getValue(result.columns[13])),
				"xc_sequence"                   : parseInt(result.getValue(result.columns[14])),
				"serial_number"                 : result.getValue(result.columns[15]),
				"port_count_0_id"               : parseInt(result.getValue(result.columns[16])),
				"port_count_1_id"               : parseInt(result.getValue(result.columns[17])),
				"port_count_2_id"               : parseInt(result.getValue(result.columns[18])),
				"port_count_3_id"               : parseInt(result.getValue(result.columns[19])),
				"port_count_4_id"               : parseInt(result.getValue(result.columns[20])),
				"router_port_id"                : parseInt(result.getValue(result.columns[21])),
				"backup_config"                 : dwg.get_bit(result.getValue(result.columns[22])),
				"date_created"                  : dwg.sqlDate(result.getValue(result.columns[23])), 
				"equipment_ports"               : get_ports(record_id),
				"equipment_cpus"                : get_cpus(record_id),
				"equipment_disks"               : get_disks(record_id),
				"equipment_memory"              : get_memory(record_id)
			};
	        return true;
		});
		return [equipment];
	}
	
	function get_ports(record_id) {
		var ports = [];
		var results = search.load({ type: "customrecord_clgx_active_port", id: "customsearch_clgx_dw_active_port" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_active_port_equipment", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			ports.push({
						"active_port_id"        : parseInt(result.getValue(result.columns[0])),
						"active_port"           : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"active_port_nbr"       : parseInt(result.getValue(result.columns[4])),
						"equipment_id"          : parseInt(result.getValue(result.columns[5])),
						"inventory_status_id"   : parseInt(result.getValue(result.columns[6])),
						"xc_id"                 : parseInt(result.getValue(result.columns[7])),
						"sequence_id"           : parseInt(result.getValue(result.columns[8])),
						"customer_id"           : parseInt(result.getValue(result.columns[9])),
						"sales_order_id"        : parseInt(result.getValue(result.columns[10])),
						"service_id"            : parseInt(result.getValue(result.columns[11])),
						"prtg_id"               : parseInt(result.getValue(result.columns[12])),
						"prtg_description"      : result.getValue(result.columns[13]),
						"cloud_connect_label"   : result.getValue(result.columns[14]),
						"cross_connect_id"      : parseInt(result.getValue(result.columns[15])),
						"equipment_type_id"     : parseInt(result.getValue(result.columns[16])),
						"equipment_make_id"     : parseInt(result.getValue(result.columns[17])),
						"date_created"          : dwg.sqlDate(result.getValue(result.columns[18]))
			});
	        return true;
		});
		return ports;
	}
	
	function get_port(record_id) {
		var port = {};
		var results = search.load({ type: "customrecord_clgx_active_port", id: "customsearch_clgx_dw_active_port" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			port = {
						"active_port_id"        : parseInt(result.getValue(result.columns[0])),
						"active_port"           : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"active_port_nbr"       : parseInt(result.getValue(result.columns[4])),
						"equipment_id"          : parseInt(result.getValue(result.columns[5])),
						"inventory_status_id"   : parseInt(result.getValue(result.columns[6])),
						"xc_id"                 : parseInt(result.getValue(result.columns[7])),
						"sequence_id"           : parseInt(result.getValue(result.columns[8])),
						"customer_id"           : parseInt(result.getValue(result.columns[9])),
						"sales_order_id"        : parseInt(result.getValue(result.columns[10])),
						"service_id"            : parseInt(result.getValue(result.columns[11])),
						"prtg_id"               : parseInt(result.getValue(result.columns[12])),
						"prtg_description"      : result.getValue(result.columns[13]),
						"cloud_connect_label"   : result.getValue(result.columns[14]),
						"cross_connect_id"      : parseInt(result.getValue(result.columns[15])),
						"equipment_type_id"     : parseInt(result.getValue(result.columns[16])),
						"equipment_make_id"     : parseInt(result.getValue(result.columns[17])),
						"date_created"          : dwg.sqlDate(result.getValue(result.columns[18]))
			};
			
	        return true;
		});
		
		return [port];
	}

	function get_cpus(record_id) {
		var cpus = [];
		var results = search.load({ type: "customrecord_clgx_cpu", id: "customsearch_clgx_dw_cpu" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_cpu_equipment", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			cpus.push({
						"cpu_id"                : parseInt(result.getValue(result.columns[0])),
						"cpu"                   : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"equipment_id"          : parseInt(result.getValue(result.columns[4])),
						"model_id"              : parseInt(result.getValue(result.columns[5])),
						"make_id"               : parseInt(result.getValue(result.columns[6])),
						"clock_speed"           : parseInt(result.getValue(result.columns[7])),
						"cores"                 : parseInt(result.getValue(result.columns[8])),
						"serial_number"         : result.getValue(result.columns[9]),
						"id"                    : result.getValue(result.columns[10])
			});
	        return true;
		});
		return cpus;
	}
	
	function get_cpu(record_id) {
		var cpu = {};
		var results = search.load({ type: "customrecord_clgx_cpu", id: "customsearch_clgx_dw_cpu" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			cpu = {
						"cpu_id"                : parseInt(result.getValue(result.columns[0])),
						"cpu"                   : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"equipment_id"          : parseInt(result.getValue(result.columns[4])),
						"model_id"              : parseInt(result.getValue(result.columns[5])),
						"make_id"               : parseInt(result.getValue(result.columns[6])),
						"clock_speed"           : parseInt(result.getValue(result.columns[7])),
						"cores"                 : parseInt(result.getValue(result.columns[8])),
						"serial_number"         : result.getValue(result.columns[9]),
						"id"                    : result.getValue(result.columns[10])
			};
			
	        return true;
		});
		
		return [cpu];
	}
	
	function get_disks(record_id) {
		var disks = [];
		var results = search.load({ type: "customrecord_clgx_disks", id: "customsearch_clgx_dw_disk" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_disks_equipment", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			disks.push({
						"disk_id"               : parseInt(result.getValue(result.columns[0])),
						"disk"                  : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"equipment_id"          : parseInt(result.getValue(result.columns[4])),
						"make_id"               : parseInt(result.getValue(result.columns[5])),
						"model_id"              : parseInt(result.getValue(result.columns[6])),
						"capacity_id"           : parseInt(result.getValue(result.columns[7])),
						"interface"             : result.getValue(result.columns[8]),
						"raid"                  : result.getValue(result.columns[9]),
						"rpm"                   : result.getValue(result.columns[10]),
						"size"                  : result.getValue(result.columns[11]),
						"serial_number"         : result.getValue(result.columns[12])
			});
	        return true;
		});
		return disks;
	}
	
	function get_disk(record_id) {
		var disk = {};
		var results = search.load({ type: "customrecord_clgx_disks", id: "customsearch_clgx_dw_disk" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			disk = {
						"disk_id"               : parseInt(result.getValue(result.columns[0])),
						"disk"                  : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"equipment_id"          : parseInt(result.getValue(result.columns[4])),
						"make_id"               : parseInt(result.getValue(result.columns[5])),
						"model_id"              : parseInt(result.getValue(result.columns[6])),
						"capacity_id"           : parseInt(result.getValue(result.columns[7])),
						"interface"             : result.getValue(result.columns[8]),
						"raid"                  : result.getValue(result.columns[9]),
						"rpm"                   : result.getValue(result.columns[10]),
						"size"                  : result.getValue(result.columns[11]),
						"serial_number"         : result.getValue(result.columns[12])
			};
			
	        return true;
		});
		
		return [disk];
	}
	
	function get_memory(record_id) {
		var memory = [];
		var results = search.load({ type: "customrecord_clgx_memory", id: "customsearch_clgx_dw_memory_2" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_memory_equipment", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			memory.push({
						"memory_id"             : parseInt(result.getValue(result.columns[0])),
						"memory"                : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"equipment_id"          : parseInt(result.getValue(result.columns[4])),
						"model_id"              : parseInt(result.getValue(result.columns[5])),
						"make_id"               : parseInt(result.getValue(result.columns[6])),
						"capacity"              : parseInt(result.getValue(result.columns[7])),
						"serial_number"         : result.getValue(result.columns[8])
			});
	        return true;
		});
		return memory;
	}
	
	function get_memory_record(record_id) {
		var memory = {};
		var results = search.load({ type: "customrecord_clgx_memory", id: "customsearch_clgx_dw_memory_2" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			memory = {
						"memory_id"             : parseInt(result.getValue(result.columns[0])),
						"memory"                : result.getValue(result.columns[1]),
						"inactive"              : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"           : result.getValue(result.columns[3]),
						"equipment_id"          : parseInt(result.getValue(result.columns[4])),
						"model_id"              : parseInt(result.getValue(result.columns[5])),
						"make_id"               : parseInt(result.getValue(result.columns[6])),
						"capacity"              : parseInt(result.getValue(result.columns[7])),
						"serial_number"         : result.getValue(result.columns[8])
			};
			
	        return true;
		});
		
		return [memory];
	}

	return {
		get_equipment    : get_equipment,
		get_port         : get_port,
		get_cpu          : get_cpu,
		get_disk         : get_disk,
		get_memory_record: get_memory_record
	};
});