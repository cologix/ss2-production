/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/2/2018
 */
define([
	"N/record", 
    "N/search", 
    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],	
function(record, search, dwg) {
	function get_kw_peak(record_id) {
		var peak        = {};
		var results     = search.load({ type: "customrecord_clgx_dcim_customer_peak", id: "customsearch_clgx_dw_customer_kw_peak" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			var columns = result.columns;
				
			peak = {
					"peak_id"          : parseInt(result.getValue(columns[0])),
					"customer_id"      : parseInt(result.getValue(columns[1])),
					"facility_id"      : parseInt(result.getValue(columns[2])),
					"subfacility_id"   : parseInt(result.getValue(columns[3])),
					"max_kw"           : result.getValue(columns[4]),
					"peak_kw"          : result.getValue(columns[5]),
					"date"             : dwg.sqlDate(result.getValue(columns[6])),
					"file_id"          : parseInt(result.getValue(columns[7])),
					"last_update"      : result.getValue(columns[8]),
					"email_to_customer": result.getValue(columns[9]),
					"sent_on"          : result.getValue(columns[10])
			};
			
	        return true;
		});
		
		return [peak];
	}
	
    return {
    	get_kw_peak: get_kw_peak
    };
    
});
