/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 *
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   3/2/2018
 */

define(["N/record",
        "N/search",
        "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"],
    function(record, search, dwg) {

        function get_fam(record_id) {
            var fam         = {};
            var results     = search.load({ type: "customrecord_ncfar_asset", id: "customsearch_clgx_dw_fam" });
            results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
            results.run().each(function(result) {
                var columns = result.columns;

                fam = {
                    "fam_id"                        : parseInt(result.getValue(columns[0])),
                    "external_id"                   : result.getValue(columns[3]),
                    "subsidiary_id"                 : parseInt(result.getValue(columns[4])),
                    "facility_id"                   : parseInt(result.getValue(columns[5])),
                    "device_id"                     : parseInt(result.getValue(columns[6])),
                    "panel_type_id"                 : parseInt(result.getValue(columns[7])),
                    "project_id"                    : parseInt(result.getValue(columns[13])),
                    "asset_type_id"                 : parseInt(result.getValue(columns[14])),
                    "parent_asset_id"               : parseInt(result.getValue(columns[12])),
                    "child_asset_id"                : parseInt(result.getValue(columns[55])),
                    "asset_status_id"               : parseInt(result.getValue(columns[25])),
                    "customer_location_id"          : parseInt(result.getValue(columns[26])),
                    "department_id"                 : parseInt(result.getValue(columns[27])),
                    "class_id"                      : parseInt(result.getValue(columns[28])),
                    "location_id"                   : parseInt(result.getValue(columns[29])),
                    "last_depreciation_period_id"   : parseInt(result.getValue(columns[21])),
                    "currency_id"                   : parseInt(result.getValue(columns[51])),
                    "tax_category_id"               : parseInt(result.getValue(columns[52])),
                    "write_off_account_id"          : parseInt(result.getValue(columns[59])),
                    "write_down_account_id"         : parseInt(result.getValue(columns[60])),
                    "disposal_cost_account_id"      : parseInt(result.getValue(columns[61])),
                    "asset_account_id"              : parseInt(result.getValue(columns[56])),
                    "depreciation_account_id"       : parseInt(result.getValue(columns[57])),
                    "warranty_period_id"            : parseInt(result.getValue(columns[72])),
                    "repair_maintenance_category_id": parseInt(result.getValue(columns[75])),
                    "parent_transaction_id"         : parseInt(result.getValue(columns[48])),
                    "depreciation_method_id"        : parseInt(result.getValue(columns[19])),
                    "deprecation_period_id"         : parseInt(result.getValue(columns[21])),
                    "fam"                           : result.getValue(columns[1]),
                    "name"                          : result.getValue(columns[8]),
                    "description"                   : result.getValue(columns[9]),
                    "serial_number"                 : result.getValue(columns[10]),
                    "asset_number"                  : result.getValue(columns[11]),
                    "original_cost"                 : parseFloat(result.getValue(columns[15])),
                    "current_cost"                  : parseFloat(result.getValue(columns[16])),
                    "residual_value"                : parseFloat(result.getValue(columns[17])),
                    "residual_value_percentage"     : result.getValue(columns[18]),
                    "lifetime"                      : parseInt(result.getValue(columns[20])),
                    "lifetime_usage"                : parseInt(result.getValue(columns[22])),
                    "current_net_book_value"        : parseFloat(result.getValue(columns[81])),
                    "cumulative_depreciation"       : parseFloat(result.getValue(columns[80])),
                    "custodian"                     : result.getValue(columns[30]),
                    "physical_location"             : result.getValue(columns[31]),
                    "include_in_reports"            : dwg.get_bit(result.getValue(columns[32])),
                    "purchase_date"                 : dwg.sqlDate(result.getValue(columns[33])),
                    "depreciation_start_date"       : dwg.sqlDate(result.getValue(columns[78])),
                    "depreciation_end_date"         : dwg.sqlDate(result.getValue(columns[79])),
                    "last_depreciation_amount"      : parseFloat(result.getValue(columns[34])),
                    "last_depreciation_date"        : dwg.sqlDate(result.getValue(columns[35])),
                    "target_depreciation_date"      : dwg.sqlDate(result.getValue(columns[36])),
                    "depreciation_active"           : dwg.get_bit(result.getValue(columns[37])),
                    "depreciation_rules"            : parseInt(result.getValue(columns[38])),
                    "revision_rules"                : parseInt(result.getValue(columns[39])),
                    "manufacturer"                  : result.getValue(columns[40]),
                    "model"                         : result.getValue(columns[41]),
                    "date_of_manufacture"           : dwg.sqlDate(result.getValue(columns[42])),
                    "legacy_vendor"                 : result.getValue(columns[43]),
                    "purchase_vendor"               : result.getValue(columns[44]),
                    "purchase_order"                : result.getValue(columns[45]),
                    "legacy_bill"                   : result.getValue(columns[46]),
                    "bill"                          : result.getValue(columns[47]),
                    "prior_year_nbv"                : parseFloat(result.getValue(columns[82])),
                    "financial_year_start"          : result.getValue(columns[49]),
                    "annual_method_entry"           : result.getValue(columns[50]),
                    "accumulated_depreciation_320"  : result.getValue(columns[53]),
                    "net_book_value_320"            : result.getValue(columns[54]),
                    "depreciation_charge_amount"    : result.getValue(columns[58]),
                    "maintenance_type"              : parseInt(result.getValue(columns[62])),
                    "maintenance_company"           : parseInt(result.getValue(columns[63])),
                    "maintenance_contract"          : result.getValue(columns[64]),
                    "pm_contract_start_date"        : dwg.sqlDate(result.getValue(columns[65])),
                    "pm_contract_end_date"          : dwg.sqlDate(result.getValue(columns[66])),
                    "inspection_interval"           : parseInt(result.getValue(columns[68])),
                    "last_inspection_date"          : dwg.sqlDate(result.getValue(columns[69])),
                    "next_inspection_date"          : dwg.sqlDate(result.getValue(columns[70])),
                    "warranty"                      : dwg.get_bit(result.getValue(columns[71])),
                    "warranty_start_date"           : dwg.sqlDate(result.getValue(columns[73])),
                    "warranty_end_date"             : dwg.sqlDate(result.getValue(columns[74])),
                    "inspection_required"           : dwg.get_bit(result.getValue(columns[67])),
                    "revalued_asset"                : dwg.get_bit(result.getValue(columns[23])),
                    "new_asset"                     : dwg.get_bit(result.getValue(columns[24])),
                    "inactive"                      : dwg.get_bit(result.getValue(columns[2]))
                };

                return true;
            });

            return [fam];
        }

        function get_fam_asset_type(record_id) {
            var types   = {};
            var columns = null;
            var filters = new Array();

            filters.push(search.createFilter({ name: "internalid", operator: "anyof", values: record_id }));
            var searchObject = search.load({ type: "customrecord_ncfar_assettype", id: "customsearch_clgx_dw_fam_asset_type", filters: filters });
            searchObject.run().each(function(result) {
                columns = result.columns;

                types = {
                    "asset_type_id"                   : parseInt(result.getValue(columns[0])),
                    "asset_type"                      : result.getValue(columns[1]),
                    "description"                     : result.getValue(columns[2]),
                    "accounting_method"               : parseInt(result.getValue(columns[3])),
                    "asset_account"                   : parseInt(result.getValue(columns[4])),
                    "asset_lifetime"                  : parseInt(result.getValue(columns[6])),
                    "asset_type_convention_1"         : parseInt(result.getValue(columns[7])),
                    "asset_type_convention_2"         : parseInt(result.getValue(columns[8])),
                    "asset_type_convention_3"         : parseInt(result.getValue(columns[9])),
                    "asset_type_convention_4"         : parseInt(result.getValue(columns[10])),
                    "asset_type_convention_5"         : parseInt(result.getValue(columns[11])),
                    "asset_type_convention_6"         : parseInt(result.getValue(columns[12])),
                    "asset_type_convention_7"         : parseInt(result.getValue(columns[13])),
                    "asset_type_convention_8"         : parseInt(result.getValue(columns[14])),
                    "asset_type_convention_9"         : parseInt(result.getValue(columns[15])),
                    "asset_type_depreciation_method_1": parseInt(result.getValue(columns[16])),
                    "asset_type_depreciation_method_2": parseInt(result.getValue(columns[17])),
                    "asset_type_depreciation_method_3": parseInt(result.getValue(columns[18])),
                    "asset_type_depreciation_method_4": parseInt(result.getValue(columns[19])),
                    "asset_type_depreciation_method_5": parseInt(result.getValue(columns[20])),
                    "asset_type_depreciation_method_6": parseInt(result.getValue(columns[21])),
                    "asset_type_depreciation_method_7": parseInt(result.getValue(columns[22])),
                    "asset_type_depreciation_method_8": parseInt(result.getValue(columns[23])),
                    "asset_type_depreciation_method_9": parseInt(result.getValue(columns[24])),
                    "asset_type_lifetime_1"           : parseInt(result.getValue(columns[25])),
                    "asset_type_lifetime_2"           : parseInt(result.getValue(columns[26])),
                    "asset_type_lifetime_3"           : parseInt(result.getValue(columns[27])),
                    "asset_type_lifetime_4"           : parseInt(result.getValue(columns[28])),
                    "asset_type_lifetime_5"           : parseInt(result.getValue(columns[29])),
                    "asset_type_lifetime_6"           : parseInt(result.getValue(columns[30])),
                    "asset_type_lifetime_7"           : parseInt(result.getValue(columns[31])),
                    "asset_type_lifetime_8"           : parseInt(result.getValue(columns[32])),
                    "asset_type_lifetime_9"           : parseInt(result.getValue(columns[33])),
                    "asset_type_residual_percentage_1": parseFloat(result.getValue(columns[34])),
                    "asset_type_residual_percentage_2": parseFloat(result.getValue(columns[35])),
                    "asset_type_residual_percentage_3": parseFloat(result.getValue(columns[36])),
                    "asset_type_residual_percentage_4": parseFloat(result.getValue(columns[37])),
                    "asset_type_residual_percentage_5": parseFloat(result.getValue(columns[38])),
                    "asset_type_residual_percentage_6": parseFloat(result.getValue(columns[39])),
                    "asset_type_residual_percentage_7": parseFloat(result.getValue(columns[40])),
                    "asset_type_residual_percentage_8": parseFloat(result.getValue(columns[41])),
                    "asset_type_residual_percentage_9": parseFloat(result.getValue(columns[42])),
                    "asset_type_store_history"        : dwg.get_bit(result.getValue(columns[43])),
                    "convention"                      : parseInt(result.getValue(columns[44])),
                    "custodian"                       : parseInt(result.getValue(columns[45])),
                    "depreciation_account"            : parseInt(result.getValue(columns[46])),
                    "depreciation_active"             : dwg.get_bit(result.getValue(columns[47])),
                    "depreciation_charge_account"     : parseInt(result.getValue(columns[48])),
                    "depreciation_period"             : parseInt(result.getValue(columns[49])),
                    "depreciation_rules"              : parseInt(result.getValue(columns[50])),
                    "description"                     : result.getValue(columns[51]),
                    "disposal_cost_account"           : parseInt(result.getValue(columns[52])),
                    "disposal_item"                   : parseInt(result.getValue(columns[53])),
                    "financial_year_start"            : parseInt(result.getValue(columns[54])),
                    "inspection"                      : dwg.get_bit(result.getValue(columns[55])),
                    "inspection_period"               : parseInt(result.getValue(columns[56])),
                    "residual_percentage"             : parseFloat(result.getValue(columns[57])),
                    "revision_rules"                  : parseInt(result.getValue(columns[58])),
                    "supplier"                        : result.getValue(columns[59]),
                    "warranty"                        : dwg.get_bit(result.getValue(columns[60])),
                    "warranty_period"                 : parseInt(result.getValue(columns[61])),
                    "write_down_account"              : parseInt(result.getValue(columns[62])),
                    "write_off_account"               : parseInt(result.getValue(columns[63]))
                };

                return true;
            });

            return types;
        }

        function get_fam_asset_types() {
            var types   = new Array();
            var columns = null;

            var searchObject = search.load({ type: "customrecord_ncfar_assettype", id: "customsearch_clgx_dw_fam_asset_type" });
            searchObject.run().each(function(result) {
                columns = result.columns;

                types.push({
                    "asset_type_id"                   : parseInt(result.getValue(columns[0])),
                    "asset_type"                      : result.getValue(columns[1]),
                    "description"                     : result.getValue(columns[2]),
                    "accounting_method"               : parseInt(result.getValue(columns[3])),
                    "asset_account"                   : parseInt(result.getValue(columns[4])),
                    "asset_lifetime"                  : parseInt(result.getValue(columns[6])),
                    "asset_type_convention_1"         : parseInt(result.getValue(columns[7])),
                    "asset_type_convention_2"         : parseInt(result.getValue(columns[8])),
                    "asset_type_convention_3"         : parseInt(result.getValue(columns[9])),
                    "asset_type_convention_4"         : parseInt(result.getValue(columns[10])),
                    "asset_type_convention_5"         : parseInt(result.getValue(columns[11])),
                    "asset_type_convention_6"         : parseInt(result.getValue(columns[12])),
                    "asset_type_convention_7"         : parseInt(result.getValue(columns[13])),
                    "asset_type_convention_8"         : parseInt(result.getValue(columns[14])),
                    "asset_type_convention_9"         : parseInt(result.getValue(columns[15])),
                    "asset_type_depreciation_method_1": parseInt(result.getValue(columns[16])),
                    "asset_type_depreciation_method_2": parseInt(result.getValue(columns[17])),
                    "asset_type_depreciation_method_3": parseInt(result.getValue(columns[18])),
                    "asset_type_depreciation_method_4": parseInt(result.getValue(columns[19])),
                    "asset_type_depreciation_method_5": parseInt(result.getValue(columns[20])),
                    "asset_type_depreciation_method_6": parseInt(result.getValue(columns[21])),
                    "asset_type_depreciation_method_7": parseInt(result.getValue(columns[22])),
                    "asset_type_depreciation_method_8": parseInt(result.getValue(columns[23])),
                    "asset_type_depreciation_method_9": parseInt(result.getValue(columns[24])),
                    "asset_type_lifetime_1"           : parseInt(result.getValue(columns[25])),
                    "asset_type_lifetime_2"           : parseInt(result.getValue(columns[26])),
                    "asset_type_lifetime_3"           : parseInt(result.getValue(columns[27])),
                    "asset_type_lifetime_4"           : parseInt(result.getValue(columns[28])),
                    "asset_type_lifetime_5"           : parseInt(result.getValue(columns[29])),
                    "asset_type_lifetime_6"           : parseInt(result.getValue(columns[30])),
                    "asset_type_lifetime_7"           : parseInt(result.getValue(columns[31])),
                    "asset_type_lifetime_8"           : parseInt(result.getValue(columns[32])),
                    "asset_type_lifetime_9"           : parseInt(result.getValue(columns[33])),
                    "asset_type_residual_percentage_1": parseFloat(result.getValue(columns[34])),
                    "asset_type_residual_percentage_2": parseFloat(result.getValue(columns[35])),
                    "asset_type_residual_percentage_3": parseFloat(result.getValue(columns[36])),
                    "asset_type_residual_percentage_4": parseFloat(result.getValue(columns[37])),
                    "asset_type_residual_percentage_5": parseFloat(result.getValue(columns[38])),
                    "asset_type_residual_percentage_6": parseFloat(result.getValue(columns[39])),
                    "asset_type_residual_percentage_7": parseFloat(result.getValue(columns[40])),
                    "asset_type_residual_percentage_8": parseFloat(result.getValue(columns[41])),
                    "asset_type_residual_percentage_9": parseFloat(result.getValue(columns[42])),
                    "asset_type_store_history"        : dwg.get_bit(result.getValue(columns[43])),
                    "convention"                      : parseInt(result.getValue(columns[44])),
                    "custodian"                       : parseInt(result.getValue(columns[45])),
                    "depreciation_account"            : parseInt(result.getValue(columns[46])),
                    "depreciation_active"             : dwg.get_bit(result.getValue(columns[47])),
                    "depreciation_charge_account"     : parseInt(result.getValue(columns[48])),
                    "depreciation_period"             : parseInt(result.getValue(columns[49])),
                    "depreciation_rules"              : parseInt(result.getValue(columns[50])),
                    "description"                     : result.getValue(columns[51]),
                    "disposal_cost_account"           : parseInt(result.getValue(columns[52])),
                    "disposal_item"                   : parseInt(result.getValue(columns[53])),
                    "financial_year_start"            : parseInt(result.getValue(columns[54])),
                    "inspection"                      : dwg.get_bit(result.getValue(columns[55])),
                    "inspection_period"               : parseInt(result.getValue(columns[56])),
                    "residual_percentage"             : parseFloat(result.getValue(columns[57])),
                    "revision_rules"                  : parseInt(result.getValue(columns[58])),
                    "supplier"                        : result.getValue(columns[59]),
                    "warranty"                        : dwg.get_bit(result.getValue(columns[60])),
                    "warranty_period"                 : parseInt(result.getValue(columns[61])),
                    "write_down_account"              : parseInt(result.getValue(columns[62])),
                    "write_off_account"               : parseInt(result.getValue(columns[63]))
                });

                return true;
            });

            return types;
        }


        function get_fam_depreciation_method(record_id) {
            var methods = {};
            var columns = null;
            var filters = new Array();

            filters.push(search.createFilter({ name: "internalid", operator: "anyof", values: record_id }));
            var searchObject = search.load({ type: "customrecord_ncfar_deprmethod", id: "customsearch_clgx_dw_fam_deprmethod", filters: filters });
            searchObject.run().each(function(result) {
                columns = result.columns;

                methods = {
                    "deprmethod_id"                  : parseInt(result.getValue(columns[0])),
                    "deprmethod"                     : result.getValue(columns[1]),
                    "depreciation_method_description": result.getValue(columns[2]),
                    "depreciation_period"            : parseInt(result.getValue(columns[3])),
                    "end_period_number"              : parseInt(result.getValue(columns[4])),
                    "next_depreciation_method"       : parseInt(result.getValue(columns[5])),
                    "depreciation_formula"           : result.getValue(columns[6]),
                    "formula_representation"         : result.getValue(columns[7]),
                    "accrual_convention"             : parseInt(result.getValue(columns[8])),
                    "final_period_convention"        : parseInt(result.getValue(columns[9]))
                };

                return true;
            });

            return methods;
        }

        function get_fam_depreciation_methods() {
            var methods = new Array();
            var columns = null;

            var searchObject = search.load({ type: "customrecord_ncfar_deprmethod", id: "customsearch_clgx_dw_fam_deprmethod" });
            searchObject.run().each(function(result) {
                columns = result.columns;

                methods.push({
                    "deprmethod_id"                  : parseInt(result.getValue(columns[0])),
                    "deprmethod"                     : result.getValue(columns[1]),
                    "depreciation_method_description": result.getValue(columns[2]),
                    "depreciation_period"            : parseInt(result.getValue(columns[3])),
                    "end_period_number"              : parseInt(result.getValue(columns[4])),
                    "next_depreciation_method"       : parseInt(result.getValue(columns[5])),
                    "depreciation_formula"           : result.getValue(columns[6]),
                    "formula_representation"         : result.getValue(columns[7]),
                    "accrual_convention"             : parseInt(result.getValue(columns[8])),
                    "final_period_convention"        : parseInt(result.getValue(columns[9]))
                });

                return true;
            });

            return methods;
        }


        function get_fam_repair_maintenance_category(record_id) {
            var categories = {};
            var columns    = null;

            var searchObject = search.load({ type: "customrecord_repairmaintcategory", id: "customsearch_clgx_dw_fam_maintcategory" });
            searchObject.run().each(function(result) {
                columns = result.columns;

                categories = {
                    "category_id"  : parseInt(result.getValue(columns[0])),
                    "category_name": result.getValue(columns[1]),
                    "asset_type_id": parseInt(result.getValue(columns[2]))
                };

                return true;
            });

            return categories;
        }


        function get_fam_repair_maintenance_categories() {
            var categories = new Array();
            var columns    = null;

            var searchObject = search.load({ type: "customrecord_repairmaintcategory", id: "customsearch_clgx_dw_fam_maintcategory" });
            searchObject.run().each(function(result) {
                columns = result.columns;

                categories.push({
                    "category_id"  : parseInt(result.getValue(columns[0])),
                    "category_name": result.getValue(columns[1]),
                    "asset_type_id": parseInt(result.getValue(columns[2]))
                });

                return true;
            });

            return categories;
        }

        return {
            get_fam: get_fam,
            get_fam_asset_type: get_fam_asset_type,
            get_fam_asset_types: get_fam_asset_types,
            get_fam_depreciation_method: get_fam_depreciation_method,
            get_fam_depreciation_methods: get_fam_depreciation_methods,
            get_fam_repair_maintenance_category: get_fam_repair_maintenance_category,
            get_fam_repair_maintenance_categories: get_fam_repair_maintenance_categories
        };
    });


