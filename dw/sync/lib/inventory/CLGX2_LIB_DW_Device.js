/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_device(record_id) {
		var device     = {};
		var results     = search.load({ type: "customrecord_clgx_dcim_devices", id: "customsearch_clgx_dw_device" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			
			var ids = (result.getValue(result.columns[22])).split(",");
			var chains = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				var chain_id = parseInt(ids[i]);
				if(chain_id){
					chains.push({
						"device_id": 		            record_id,
						"device_power_chain_id": 		chain_id
					})
				}
			}
			device = {
				"device_id"                    : parseInt(result.getValue(result.columns[0])),
				"device"                       : result.getValue(result.columns[1]),
				"inactive"                     : dwg.get_bit(result.getValue(result.columns[2])),
				"external_id"                  : result.getValue(result.columns[3]),
				"parent_id"                    : parseInt(result.getValue(result.columns[4])),
				"pair_id"                      : parseInt(result.getValue(result.columns[5])),
				"facility_id"                  : parseInt(result.getValue(result.columns[6])),
				"virtual"                      : dwg.get_bit(result.getValue(result.columns[7])),
				"deleted"                      : dwg.get_bit(result.getValue(result.columns[8])),
				"capacity_tree_id"             : parseInt(result.getValue(result.columns[9])),
				"tree_root"                    : dwg.get_bit(result.getValue(result.columns[10])),
				"derated_capacity"             : parseInt(result.getValue(result.columns[11])),
				"real_power_point_id"          : parseInt(result.getValue(result.columns[12])),
				"real_power_value"             : parseFloat(result.getValue(result.columns[13])),
				"device_category_id"           : parseFloat(result.getValue(result.columns[14])),
				"apparent_power_output_id"     : parseInt(result.getValue(result.columns[15])),
				"energy_id"                    : parseInt(result.getValue(result.columns[16])),
				"real_power_output_point_id"   : parseInt(result.getValue(result.columns[17])),
				"real_power_nameplate_capacity": parseFloat(result.getValue(result.columns[18])),
				"real_power_derated_capacity"  : parseFloat(result.getValue(result.columns[19])),
				"nameplate_breaker_capacity"   : parseFloat(result.getValue(result.columns[20])),
				"derated_breaker_capacity"     : parseFloat(result.getValue(result.columns[21])),
				"has_powers"                   : dwg.get_bit(result.getValue(result.columns[22])),
				"device_power_chains"          : chains,
				"device_points"                : get_device_points(record_id)
			};
	        return true;
		});
		return [device];
	}
	
	function get_device_points(record_id) {
		var points = [];
		var results = search.load({ type: "customrecord_clgx_dcim_points", id: "customsearch_clgx_dw_device_point" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_dcim_points_device", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {
			var average = parseFloat(result.getValue(result.columns[7])).toString().replace(/e\+[0-9]+/, "");
			
			points.push({
						"point_id"        : parseInt(result.getValue(result.columns[0])),
						"point"           : result.getValue(result.columns[1]),
						"inactive"        : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"     : result.getValue(result.columns[3]),
						"device_id"       : parseInt(result.getValue(result.columns[4])),
						"units_id"        : parseInt(result.getValue(result.columns[5])),
						"deleted"         : dwg.get_bit(result.getValue(result.columns[6])),
						"average"         : Number(average)
			});
	        return true;
		});
		return points;
	}

	return {
		get_device: get_device
	};
});




