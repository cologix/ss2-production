/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_vxc(record_id) {
		try {
			var vxc     = {};
			var rec = record.load({ type: 'customrecord_cologix_vxc', id: record_id });
			
			var ports = [];
			var port0 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_provider_switch_1"}));
			if(port0){
				ports.push({
					"vxc_id"                     : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_provider_switch_1"})),
					"type"                       : "Provider Switch",
					"sort"                       : 0
				});
			}
			var port1 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_access_switch_1"}));
			if(port1){
				ports.push({
					"vxc_id"                     : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_access_switch_1"})),
					"type"                       : "Access Switch 1",
					"sort"                       : 1
				});
			}
			var port2 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_access_switch_2"}));
			if(port2){
				ports.push({
					"vxc_id"                     : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_access_switch_2"})),
					"type"                       : "Access Switch 2",
					"sort"                       : 2
				});
			}
			var port3 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_access_switch_3"}));
			if(port3){
				ports.push({
					"vxc_id"                      : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_access_switch_3"})),
					"type"                       : "Access Switch 3",
					"sort"                       : 3
				});
			}
			
			
			var xcs = [];
			var a = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_cloud_connect_port"}));
			if(a){
				xcs.push({
					"vxc_id"                     : record_id,
					"xc_id"                      : parseInt(rec.getValue({ fieldId: "custrecord_clgx_cloud_connect_port"})),
					"sequence_id"                : 1
				});
			}
			var z = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_cloud_connect_port_z"}));
			if(z){
				xcs.push({
					"vxc_id"                     : record_id,
					"xc_id"                      : parseInt(rec.getValue({ fieldId: "custrecord_clgx_cloud_connect_port_z"})),
					"sequence_id"                : 10
				});
			}
			
			vxc = {
					"vxc_id"                     : parseInt(rec.getValue({ fieldId: "id"})),
					"vxc"                        : rec.getValue({ fieldId: "name"}),
					"inactive"                   : dwg.get_bit(rec.getValue({ fieldId: "isinactive"})),
					"external_id"                : rec.getValue({ fieldId: "externalid"}),
					"service_id"                 : parseInt(rec.getValue({ fieldId: "custrecord_cologix_vxc_service"})),
					"facility_id"                : parseInt(rec.getValue({ fieldId: "custrecord_cologix_vxc_facility"})),
					"carrier_id"                 : parseInt(rec.getValue({ fieldId: "custrecord_cologix_vxc_carrier_name"})),
					"carrier_circuit"            : rec.getValue({ fieldId: "	custrecord_cologix_vxc_carriercirct"}),
					"vxc_type_id"                : parseInt(rec.getValue({ fieldId: "custrecord_cologix_vxc_type"})),
					"vxc_circuit_type_id"        : parseInt(rec.getValue({ fieldId: "custrecord_cologix_vxc_circuit_type"})),
					"status_id"                  : parseInt(rec.getValue({ fieldId: "custrecord_cologix_vxc_status"})),
					"service_order_id"           : parseInt(rec.getValue({ fieldId: "	custrecord_cologix_service_order"})),
					"legacy_id"                  : rec.getValue({ fieldId: "custrecord_clgx_vxc_lagid"}),
					"msft_tag"                   : rec.getValue({ fieldId: "custrecord_clgx_vxc_msft_stag"}),
					"cloud_provider_id"          : parseInt(rec.getValue({ fieldId: "custrecord_clgx_cloud_provider"})),
					"s_key"                      : rec.getValue({ fieldId: "custrecord_clxg_cc_skey"}),
					"speed_id"                   : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vxc_speed"})),
					"cloud_port_label"           : rec.getValue({ fieldId: "custrecord_clgx_vxc_cc_port_label"}),
					"clgx_stag"                  : rec.getValue({ fieldId: "custrecord_clgx_vxc_stag"}),
					"vxc_ports"                  : ports,
					"vxc_xcs"                    : xcs
			};
		} catch(ex) {
			
		}
		
		return [vxc];
	}

	return {
		get_vxc: get_vxc
	};
});


