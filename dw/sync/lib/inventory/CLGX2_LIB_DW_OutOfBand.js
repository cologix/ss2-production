/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   2/12/2018
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"],
function(record, search, dwg) {
    function get_oob(record_id) {
    	var oob = new Object();
		
		var rec = record.load({ type: "customrecord_clgx_oob_facility", id: record_id });
		oob.oob_id             = parseInt(rec.getValue("id"));
		oob.oob_name           = rec.getValue("altname");
		oob.service_order_id   = parseInt(rec.getValue("custrecord_clgx_oob_service_order"));
		oob.service_id         = parseInt(rec.getValue("custrecord_clgx_oob_service_id"));
		oob.facility_id        = parseInt(rec.getValue("custrecord_clgx_oob_facility"));
		oob.space_id           = parseInt(rec.getValue("custrecord_clgx_oob_space"));
		oob.firewall_type_id   = parseInt(rec.getValue("custrecord_clgx_oob_firewall"));
		oob.dmz_switch_port_id = parseInt(rec.getValue("custrecord_clgx_oob_dmz_switch_port"));
		oob.connection_type_id = parseInt(rec.getValue("custrecord_clgx_oob_conn_type"));
		oob.public_ip_id       = parseInt(rec.getValue("custrecord_clgx_oob_pub_ip_addr"));
		oob.switch_ip_id       = parseInt(rec.getValue("custrecord_clgx_oob_cust_ip_addr"));
		oob.subnet_mask_id     = parseInt(rec.getValue("custrecord_clgx_oob_subnet_mask"));
		oob.default_gateway_id = parseInt(rec.getValue("custrecord_clgx_oob_def_router"));
		oob.vlan_id            = parseInt(rec.getValue("custrecord_clgx_oob_vlan"));
		oob.tcp_port           = parseInt(rec.getValue("custrecord_clgx_oob_tcp_port"));
		oob.udp_port           = parseInt(rec.getValue("custrecord_clgx_oob_udp_port"));
		oob.oob_status_id      = parseInt(rec.getValue("custrecord_clgx_oob_status"));
    	
    	return oob;
    }
    
    return {
        get_oob: get_oob
    };
    
});
