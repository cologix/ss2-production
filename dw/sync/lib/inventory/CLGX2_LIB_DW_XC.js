/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_xc(record_id) {
		
		var xc     = {};
		
		try {

			var rec = record.load({ type: 'customrecord_cologix_crossconnect', id: record_id });
			
			var results = search.load({ type: 'customrecord_cologix_crossconnect', id: "customsearch_clgx_dw_xc_ids" });
			results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
			results.run().each(function(result) {
				var vlan_id = parseInt(result.getValue(result.columns[1]));
		        return true;
			});
			
			var ports = [];
			var a = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_a_end_port"}));
			if(a){
				ports.push({
					"xc_id"                      : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_a_end_port"})),
					"end_port"                   : rec.getValue({ fieldId: "custrecord_clgx_a_end"}),
					"sequence_id"                : 1
				});
			}
			var b = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_b_end_port"}));
			if(b){
				ports.push({
					"xc_id"                      : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_b_end_port"})),
					"end_port"                   : rec.getValue({ fieldId: "custrecord_clgx_b_end"}),
					"sequence_id"                : 2
				});
			}
			var c = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_c_end_port"}));
			if(c){
				ports.push({
					"xc_id"                      : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_c_end_port"})),
					"end_port"                   : rec.getValue({ fieldId: "custrecord_clgx_c_end"}),
					"sequence_id"                : 3
				});
			}
			var z = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_z_end_port"}));
			if(z){
				ports.push({
					"xc_id"                      : record_id,
					"active_port_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_z_end_port"})),
					"end_port"                   : rec.getValue({ fieldId: "custrecord_clgx_z_end"}),
					"sequence_id"                : 10
				});
			}
			
			
			var vrrp = [];
			var vrrp1 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp1"}));
			if(vrrp1){
				vrrp.push({
					"xc_id"                      : record_id,
					"group_id"                   : 1,
					"vrrp_group_id"              : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp1"})),
					"subnet_mask"                : parseInt(rec.getValue({ fieldId: "custrecord_clgx_subnet_mask1"})),
					"ip_address_block"           : rec.getValue({ fieldId: "custrecord_clgx_ip_address1"}),
					"bgp_prefix"                 : rec.getValue({ fieldId: "custrecord_clgx_bgp_prefix1"})
				});
			}
			var vrrp2 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp2"}));
			if(vrrp2){
				vrrp.push({
					"xc_id"                      : record_id,
					"group_id"                   : 2,
					"vrrp_group_id"              : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp2"})),
					"subnet_mask"                : parseInt(rec.getValue({ fieldId: "custrecord_clgx_subnet_mask2"})),
					"ip_address_block"           : rec.getValue({ fieldId: "custrecord_clgx_ip_address2"}),
					"bgp_prefix"                 : rec.getValue({ fieldId: "custrecord_clgx_bgp_prefix2"})
				});
			}
			var vrrp3 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp3"}));
			if(vrrp3){
				vrrp.push({
					"xc_id"                      : record_id,
					"group_id"                   : 3,
					"vrrp_group_id"              : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp3"})),
					"subnet_mask"                : parseInt(rec.getValue({ fieldId: "custrecord_clgx_subnet_mask3"})),
					"ip_address_block"           : rec.getValue({ fieldId: "custrecord_clgx_ip_address3"}),
					"bgp_prefix"                 : rec.getValue({ fieldId: "custrecord_clgx_bgp_prefix3"})
				});
			}
			var vrrp4 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp4"}));
			if(vrrp4){
				vrrp.push({
					"xc_id"                      : record_id,
					"group_id"                   : 4,
					"vrrp_group_id"              : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp4"})),
					"subnet_mask"                : parseInt(rec.getValue({ fieldId: "custrecord_clgx_subnet_mask4"})),
					"ip_address_block"           : rec.getValue({ fieldId: "custrecord_clgx_ip_address4"}),
					"bgp_prefix"                 : rec.getValue({ fieldId: "custrecord_clgx_bgp_prefix4"})
				});
			}
			var vrrp5 = 	parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp5"}));
			if(vrrp5){
				vrrp.push({
					"xc_id"                      : record_id,
					"group_id"                   : 5,
					"vrrp_group_id"              : parseInt(rec.getValue({ fieldId: "custrecord_clgx_vrrp5"})),
					"subnet_mask"                : parseInt(rec.getValue({ fieldId: "custrecord_clgx_subnet_mask5"})),
					"ip_address_block"           : rec.getValue({ fieldId: "custrecord_clgx_ip_address5"}),
					"bgp_prefix"                 : rec.getValue({ fieldId: "custrecord_clgx_bgp_prefix5"})
				});
			}
			
			xc = {
					"xc_id"                      : parseInt(rec.getValue({ fieldId: "id"})),
					"xc"                         : rec.getValue({ fieldId: "name"}),
					"inactive"                   : dwg.get_bit(rec.getValue({ fieldId: "isinactive"})),
					"external_id"                : rec.getValue({ fieldId: "externalid"}),
					"service_id"                 : parseInt(rec.getValue({ fieldId: "custrecord_cologix_xc_service"})),
					"facility_id"                : parseInt(rec.getValue({ fieldId: "custrecord_clgx_xc_facility"})),
					"carrier_id"                 : parseInt(rec.getValue({ fieldId: "custrecord_cologix_carrier_name"})),
					"carrier_circuit"            : rec.getValue({ fieldId: "custrecord_cologix_xc_carriercirct"}),
					"xc_type_id"                 : parseInt(rec.getValue({ fieldId: "custrecord_cologix_xc_type"})),
					"xc_circuit_type_id"         : parseInt(rec.getValue({ fieldId: "custrecord_cologix_xc_circuit_type"})),
					"status_id"                  : parseInt(rec.getValue({ fieldId: "custrecord_cologix_xc_status"})),
					"service_order_id"           : parseInt(rec.getValue({ fieldId: "custrecord_xconnect_service_order"})),
					"legacy_id"                  : rec.getValue({ fieldId: "custrecord_clgx_legacy_xc_id"}),
					
					"vlan_id"                    : parseInt(rec.getValue({ fieldId: "custrecord_clgx_xc_vlan"})),
					"speed_id"                   : parseInt(rec.getValue({ fieldId: "custrecord_clgx_speed"})),
					"provisioned"                : dwg.sqlDate(rec.getValue({ fieldId: "custrecord_clgx_net_prov_date"})),
					"bgp_prefix"                 : rec.getValue({ fieldId: "custrecord_clgx_xc_bgp_prefix"}),
					"bgp_as_nbr"                 : parseInt(rec.getValue({ fieldId: "custrecord_clgx_bgp_asno"})),
					"subnet_mask_id"             : parseInt(rec.getValue({ fieldId: "custrecord_clgx_subnet_mask"})),
					"bandwith_police"            : parseInt(rec.getValue({ fieldId: "custrecord_clgx_bndwth_police"})),
					"storm_control_unicast"      : parseInt(rec.getValue({ fieldId: "custrecord_clgx_storm_control_unicast"})),
					"lacp_tunneling"             : dwg.get_bit(rec.getValue({ fieldId: "custrecord_clgx_lacp_tunneling"})),
					"ldp_tunneling"              : dwg.get_bit(rec.getValue({ fieldId: "custrecord_clgx_ldp_tunneling"})),
					"span_tree_protocol"         : dwg.get_bit(rec.getValue({ fieldId: "custrecord_clgx_span_tree_protocol"})),
					"customer_routed"            : dwg.get_bit(rec.getValue({ fieldId: "custrecord_clgx_cust_route"})),
					"customer_route_ip"          : parseInt(rec.getValue({ fieldId: "custrecord_clgx_cust_route_ip"})),
					"s_key"                      : rec.getValue({ fieldId: "custrecord_clxg_cc_skey"}),
					"cloud_port_label"           : rec.getValue({ fieldId: "custrecord_clgx_xc_cloud_port_label"}),
					"note"                       : rec.getValue({ fieldId: "custrecord_clgx_xc_note"}),
					
					"truck_port"                 : dwg.get_bit(rec.getValue({ fieldId: "custrecord_clgx_truck_port"})),
					"port_id"                    : rec.getValue({ fieldId: "custrecord_clgx_port_id"}),
					"xc_type_xc_id"              : parseInt(rec.getValue({ fieldId: "custrecord_clgx_xc_type"})),
					
					"prtg_id"                    : rec.getValue({ fieldId: "custrecord_clgx_prtg_id"}),
					"prtg_port_desc"             : rec.getValue({ fieldId: "custrecord_clgx_prtg_port_desc"}),
					"device_name"                : rec.getValue({ fieldId: "custrecord_clgx_device_name_xc"}),
					"bandwidth_vlan"             : rec.getValue({ fieldId: "custrecord_clgx_bandwidth_vlan"}),
					"xc_ports"                   : ports,
					"xc_vrrp"                    : vrrp,
					"xc_paths"                   : get_xc_paths(record_id)
			};
			
		}
		catch (error) {

		}
		
		return [xc];
	}
	
	function get_xc_paths(record_id) {
		var paths = [];
		var results = search.load({ type: "customrecord_cologix_xcpath", id: "customsearch_clgx_dw_xc_path" });
		results.filters.push(search.createFilter({ name: "custrecord_cologix_xcpath_crossconnect", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			paths.push({
						"xcpath_id"                  : parseInt(result.getValue(result.columns[0])),
						"xcpath_sequence_id"         : parseInt(result.getValue(result.columns[1])),
						"inactive"                   : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"                : result.getValue(result.columns[3]),
						"xc_id"                      : parseInt(result.getValue(result.columns[4])),
						"space_id"                   : parseInt(result.getValue(result.columns[5])),
						//"space_rack_id"              : parseInt(result.getValue(result.columns[6])),
						"xcpath_panel_id"            : parseInt(result.getValue(result.columns[6])),
						"xcpath_connector_id"        : parseInt(result.getValue(result.columns[7])),
						"xcpath_slot_id"             : parseInt(result.getValue(result.columns[8])),
						"xcpath_utilisation_id"      : parseInt(result.getValue(result.columns[9])),
						"tie_cable"                  : dwg.get_bit(result.getValue(result.columns[10])),
						"port"                       : result.getValue(result.columns[11])
			});
	        return true;
		});
		return paths;
	}
	
	function get_xc_path(record_id) {
		var paths = [];
		var results = search.load({ type: "customrecord_cologix_xcpath", id: "customsearch_clgx_dw_xc_path" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "ANYOF", values: record_id }));
		results.run().each(function(result) {	
			paths.push({
						"xcpath_id"                  : parseInt(result.getValue(result.columns[0])),
						"xcpath_sequence_id"         : parseInt(result.getValue(result.columns[1])),
						"inactive"                   : dwg.get_bit(result.getValue(result.columns[2])),
						"external_id"                : result.getValue(result.columns[3]),
						"xc_id"                      : parseInt(result.getValue(result.columns[4])),
						"space_id"                   : parseInt(result.getValue(result.columns[5])),
						//"space_rack_id"              : parseInt(result.getValue(result.columns[6])),
						"xcpath_panel_id"            : parseInt(result.getValue(result.columns[6])),
						"xcpath_connector_id"        : parseInt(result.getValue(result.columns[7])),
						"xcpath_slot_id"             : parseInt(result.getValue(result.columns[8])),
						"xcpath_utilisation_id"      : parseInt(result.getValue(result.columns[9])),
						"tie_cable"                  : dwg.get_bit(result.getValue(result.columns[10])),
						"port"                       : result.getValue(result.columns[11])
			});
	        return true;
		});
		return paths;
	}

	return {
		get_xc: get_xc,
		get_xc_path: get_xc_path
	};
});


