/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   3/16/2018
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"],
function(record, search, dwg) {
    function get_managed_service(record_id) {
    	var ms      = new Object();
    	var columns = null;
    	var note    = "";
    	
    	var recordObject = search.load({ type: "customrecord_clgx_managed_services", id: "customsearch_clgx_dw_managed_service" });
    	recordObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", values: [record_id] }));
    	
    	recordObject.run().each(function(result) {
    		columns = result.columns;
    		note    = dwg.get_null(result.getValue("custrecord_clgx_ms_note"));
    		
    		ms = {
    			"managed_service_id"     : parseInt(result.getValue("internalid")),
    			"service_id"             : parseInt(result.getValue("custrecord_clgx_ms_service")),
    			"service_order_id"       : parseInt(result.getValue("custrecord_clgx_ms_service_order")),
    			"server_id"              : parseInt(result.getValue("custrecord_clgx_ms_server")),
    			"managed_service_type_id": parseInt(result.getValue("custrecord_clgx_managed_services_type")),
    			"host_name"              : result.getValue("custrecord_clgx_ms_hostname"),
    			"facility_id"            : parseInt(result.getValue("custrecord_clgx_ms_facility")),
    			"replication_enabled"    : dwg.get_bit(result.getValue("custrecord_clgx_ms_replication_enabled")),
    			"replication_vault_id"   : parseInt(result.getValue("custrecord_clgx_ms_replication_vault")),
    			"vault_id"               : parseInt(result.getValue("custrecord_clgx_ms_vault")),
    			"volume_name"            : result.getValue("custrecord_ms_volume_name"),
    			"storage_vlan"           : result.getValue("custrecord_clgx_ms_storage_vlan"),
    			"virtualdc_id"           : parseInt(result.getValue("custrecord_clgx_ms_attached_vdc")),
    			"cpu_ghz"                : result.getValue("custrecord_clgx_ms_cpu_ghz"),
    			"gbofram"                : result.getValue("custrecord_clgx_ms_gb_ram"),
    			"virtualdc_name"         : result.getValue("custrecord_clgx_ms_virtualdc_name"),
    			"virtualdc_provider"     : result.getValue("custrecord_clgx_ms_provider_vdc"),
    			"number_of_cpus"         : result.getValue("custrecord_clgx_ms_number_cpu"),
    			"gbofdisk"               : result.getValue("custrecord_clgx_ms_gb_disk_space"),
    			"ismanaged"              : dwg.get_bit(result.getValue("custrecord_clgx_ms_managed")),
    			"legacy_id"              : parseInt(result.getValue("custrecord_clgx_ms_legacy_id")),
    			"isvirtual"              : dwg.get_bit(result.getValue("custrecord_clgx_ms_virtual")),
    			"ipmi"                   : result.getValue("custrecord_clgx_ms_ipmi"),
    			"router_switch_id"       : parseInt(result.getValue("custrecord_clgx_ms_router_switch")),
    			"replication_hostname"   : result.getValue("custrecord_clgx_ms_replication_host"),
    			"device_name"            : result.getValue("custrecord_clgx_ms_device_name"),
    			"ip_address"             : result.getValue("custrecord_clgx_ip_address"),
    			"ipmi_vpn"               : result.getValue("custrecord_clgx_ms_ipmi_vpn"),
    			"cluster_id"             : parseInt(result.getValue("custrecord_clgx_ms_cluster")),
    			"number_of_processors"   : result.getValue("custrecord_clgx_ms_number_processors"),
    			"cloud_vault_id"         : parseInt(result.getValue("custrecord_clgx_cloud_vault")),
    			"lun_size_gb"            : result.getValue("custrecord_clgx_ms_lun_size"),
    			"provisioned_date"       : dwg.sqlDate(result.getValue("custrecord_clgx_ms_prov_date")),
    			"note"                   : note == null ? note : note.replace(/'/g, "’"),
    			"firewall_id"            : parseInt(result.getValue("custrecord_clgx_ms_firewall"))
    		};
    	});
    	
    	return [ms];
    }
    
    return {
        get_managed_service: get_managed_service
    };
    
});
