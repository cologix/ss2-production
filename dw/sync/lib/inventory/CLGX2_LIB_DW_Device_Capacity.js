/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_device_capacity (record_id) {
		var capacity     = {};
		var results     = search.load({ type: "customrecord_clgx_dcim_devices_capacity", id: "customsearch_clgx_dw_device_capacity" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			capacity = {
				"device_capacity_id"   : parseInt(result.getValue(result.columns[0])),
				"device_id"            : parseInt(result.getValue(result.columns[1])),
				"capacity_date"        : dwg.sqlDate(result.getValue(result.columns[2])),
				"actual_load"          : parseFloat(result.getValue(result.columns[3])),
				"derated_capacity"     : parseFloat(result.getValue(result.columns[4])),
				"powers_chains"        : parseInt(result.getValue(result.columns[5])),
				"powers_fams"          : parseInt(result.getValue(result.columns[6])),
				"fail_total_used"      : parseFloat(result.getValue(result.columns[7])),
				"fail_total_sold"      : parseFloat(result.getValue(result.columns[8])),
				"fail_max_used"        : parseFloat(result.getValue(result.columns[9])),
				"fail_max_sold"        : parseFloat(result.getValue(result.columns[10])),
				"sum_used"             : parseFloat(result.getValue(result.columns[11])),
				"sum_sold"             : parseFloat(result.getValue(result.columns[12])),
				"available_used"       : parseFloat(result.getValue(result.columns[13])),
				"available_sold"       : parseFloat(result.getValue(result.columns[14])),
				"device_capacity_failed" : parseFailedCapacity(JSON.parse(result.getValue(result.columns[15])), parseInt(result.getValue(result.columns[1])), parseInt(result.getValue(result.columns[0])))
			};
	        return true;
		});
		return [capacity];
	}

	function parseFailedCapacity(capacityFailedObject, deviceID, deviceCapacityID) {
		if(capacityFailedObject) {
			var finalObject  = new Array();
			var objectLength = capacityFailedObject.length;
			
			for(var i = 0; i < objectLength; i++) {
				if(capacityFailedObject[i].chain_id != null) {
					capacityFailedObject[i].device_capacity_id = deviceCapacityID;
					capacityFailedObject[i].device_id          = deviceID;
					
					finalObject.push(capacityFailedObject[i]);
				}
			}
			
			return finalObject;
		}
	}
	
	return {
		get_device_capacity: get_device_capacity
	};
});

