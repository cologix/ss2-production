/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_power(record_id) {
		var power     = {};
		var results     = search.load({ type: "customrecord_clgx_power_circuit", id: "customsearch_clgx_dw_power" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			
			var fams = [];
			var fam1 = parseInt(result.getValue(result.columns[37]));
			var fam2 = parseInt(result.getValue(result.columns[38]));
			var fam3 = parseInt(result.getValue(result.columns[39]));
			var fam4 = parseInt(result.getValue(result.columns[40]));
			var fam5 = parseInt(result.getValue(result.columns[41]));
			if (fam1){
				fams.push({"power_id": record_id, "fam_id": fam1, "type_id": 1});
			}
			if (fam2){
				fams.push({"power_id": record_id, "fam_id": fam2, "type_id": 2});
			}
			if (fam3){
				fams.push({"power_id": record_id, "fam_id": fam3, "type_id": 3});
			}
			if (fam4){
				fams.push({"power_id": record_id, "fam_id": fam4, "type_id": 4});
			}
			if (fam5){
				fams.push({"power_id": record_id, "fam_id": fam5, "type_id": 5});
			}
			
			var brkr = parseInt(result.getValue(result.columns[12]));
			var panel_type = parseInt(result.getValue(result.columns[42]));
			var line_id = parseInt(result.getValue(result.columns[11]));
			if (!panel_type){
				brkr = 0;
			}
			if(panel_type == 2 || panel_type == 12){
				if(line_id == 1 || line_id == 2 || line_id == 3){
					brkr = line_id;
				}
				else if(line_id == 9){
					brkr = 2;
				}
				else{
					brkr = 1;
				}
			}

			
			var gap = get_breaker_gap (panel_type, line_id);
			var points = [];
			
			var ids = (result.getValue(result.columns[43])).split(",");
			for(var i = 0; ids != null && i < ids.length; i++) {
				var point_id = parseInt(ids[i]);
				var breaker = brkr + i*gap;
				if(point_id){
					points.push({
						"power_id": 		record_id,
						"point_id": 		point_id,
						"type_id": 			1,
						"breaker": 			breaker,
						"phase": 			get_breaker_phase (breaker, panel_type, line_id)
					})
				}
			}
			var ids = (result.getValue(result.columns[44])).split(",");
			for(var i = 0; ids != null && i < ids.length; i++) {
				var point_id = parseInt(ids[i]);
				var breaker = brkr + i*gap;
				if(point_id){
					points.push({
						"power_id": 		record_id,
						"point_id": 		point_id,
						"type_id": 			2,
						"breaker": 			breaker,
						"phase": 			get_breaker_phase (breaker, panel_type, line_id)
					})
				}
			}
			var ids = (result.getValue(result.columns[45])).split(",");
			for(var i = 0; ids != null && i < ids.length; i++) {
				var point_id = parseInt(ids[i]);
				var breaker = brkr + i*gap;
				if(point_id){
					points.push({
						"power_id": 		record_id,
						"point_id": 		point_id,
						"type_id": 			3,
						"breaker": 			breaker,
						"phase": 			get_breaker_phase (breaker, panel_type, line_id)
					})
				}
			}
			var ids = (result.getValue(result.columns[46])).split(",");
			for(var i = 0; ids != null && i < ids.length; i++) {
				var point_id = parseInt(ids[i]);
				var breaker = brkr + i*gap;
				if(point_id){
					points.push({
						"power_id": 		record_id,
						"point_id": 		point_id,
						"type_id": 			4,
						"breaker": 			breaker,
						"phase": 			get_breaker_phase (breaker, panel_type, line_id)
					})
				}
			}
			var ids = (result.getValue(result.columns[47])).split(",");
			for(var i = 0; ids != null && i < ids.length; i++) {
				var point_id = parseInt(ids[i]);
				var breaker = brkr + i*gap;
				if(point_id){
					points.push({
						"power_id": 		record_id,
						"point_id": 		point_id,
						"type_id": 			5,
						"breaker": 			breaker,
						"phase": 			get_breaker_phase (breaker, panel_type)
					})
				}
			}
			
			power = {
				"power_id"                  : parseInt(result.getValue(result.columns[0])),
				"power"                     : result.getValue(result.columns[1]),
				"inactive"                  : dwg.get_bit(result.getValue(result.columns[2])),
				"external_id"               : result.getValue(result.columns[3]),
				"facility_id"               : parseInt(result.getValue(result.columns[4])),
				"sales_order_id"            : parseInt(result.getValue(result.columns[5])),
				"service_id"                : parseInt(result.getValue(result.columns[6])),
				"status_id"                 : parseInt(result.getValue(result.columns[7])),
				"pair_id"                   : parseInt(result.getValue(result.columns[8])),
				"space_id"                  : parseInt(result.getValue(result.columns[9])),
				"module_id"                 : parseInt(result.getValue(result.columns[10])),
				"line_id"                   : line_id,
				"breaker_id"                : brkr,
				"amps_id"                   : parseInt(result.getValue(result.columns[13])),
				"volts_id"                  : parseInt(result.getValue(result.columns[14])),
				"phase_id"                  : parseInt(result.getValue(result.columns[15])),
				"amps_inst_id"              : parseInt(result.getValue(result.columns[16])),
				"serial_id"                 : parseInt(result.getValue(result.columns[17])),
				"device_id"                 : parseInt(result.getValue(result.columns[18])),
				"overage_email"             : dwg.get_bit(result.getValue(result.columns[19])),
				"overage_email_date"        : dwg.sqlDate(result.getValue(result.columns[20])),
				"overage_days"              : parseInt(result.getValue(result.columns[21])),
				"provisioned"               : dwg.sqlDate(result.getValue(result.columns[22])),
				"replicated"                : dwg.sqlDate(result.getValue(result.columns[23])),
				"disconected"               : dwg.sqlDate(result.getValue(result.columns[24])),
				"amps_phase_a"              : parseFloat(result.getValue(result.columns[25])),
				"amps_phase_a_ab"           : parseFloat(result.getValue(result.columns[26])),
				"amps_phase_b"              : parseFloat(result.getValue(result.columns[27])),
				"amps_phase_b_ab"           : parseFloat(result.getValue(result.columns[28])),
				"amps_phase_c"              : parseFloat(result.getValue(result.columns[29])),
				"amps_phase_c_ab"           : parseFloat(result.getValue(result.columns[30])),
				"amps_max"                  : parseFloat(result.getValue(result.columns[31])),
				"amps_max_ab"               : parseFloat(result.getValue(result.columns[32])),
				"amps_percent"              : parseFloat(result.getValue(result.columns[33])),
				"amps_percent_ab"           : parseFloat(result.getValue(result.columns[34])),
				"kw"                        : parseFloat(result.getValue(result.columns[35])),
				"kw_ab"                     : parseFloat(result.getValue(result.columns[36])),
				"kva"                       : parseFloat(result.getValue(result.columns[48])),
				"contracted_kva"            : parseFloat(result.getValue(result.columns[50])),
				"power_chain_id"            : parseInt(result.getValue(result.columns[49])),
				"panel_type"                : panel_type,
				"power_junction_fam"        : fams,
				"power_junction_device_point": points
			};
	        return true;
		});
		return [power];
	}
	
	function get_breaker_phase (breaker, panel_type, line_id){
		
		var mod3 = breaker % 3;
		var mod6 = breaker % 6;
		var phase = '';
		if(panel_type == 1 || panel_type == 7){ // Traditional, Virtual Traditional, Cyberex, Virtual Cyberex
			if(mod6 == 1 || mod6 == 2){
				phase = 'A';
			}
			if(mod6 == 3 || mod6 == 4){
				phase = 'B';
			}
			if(mod6 == 5 || mod6 == 0){
				phase = 'C';
			}
		}
		else if (panel_type == 2 || panel_type == 3 || panel_type == 4 || panel_type == 8 || panel_type == 5 || panel_type == 6 ||  panel_type == 9 || panel_type == 10|| panel_type == 12){ 
			
			if(mod3 == 1){
				phase = 'A';
			}
			if(mod3 == 2){
				phase = 'B';
			}
			if(mod3 == 0){
				phase = 'C';
			}
		}
		else{
			phase = 'ABC';
		}
		return phase;
	}
	function get_breaker_gap (panel_type, line_id){
		var gap = 1;
		if(panel_type == 1 || panel_type == 6 || panel_type == 7 || panel_type == 4 || panel_type == 8 || ((panel_type == 2 || panel_type == 12) && line_id == 10)){
			gap = 2;
		}
		return gap;
	}
	
	return {
		get_power: get_power
	};
});
	 	 
