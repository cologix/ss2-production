/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_space(record_id) {
		var space     = {};
		var results     = search.load({ type: "customrecord_cologix_space", id: "customsearch_clgx_dw_space" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			space = {
				"space_id"                  : parseInt(result.getValue(result.columns[0])),
				"space"                     : result.getValue(result.columns[1]),
				"inactive"                  : dwg.get_bit(result.getValue(result.columns[2])),
				"external_id"               : result.getValue(result.columns[3]),
				"facility_id"               : parseInt(result.getValue(result.columns[4])),
				"sales_order_id"            : parseInt(result.getValue(result.columns[5])),
				"service_id"                : parseInt(result.getValue(result.columns[6])),
				"type_id"                   : parseInt(result.getValue(result.columns[7])),
				"status_id"                 : parseInt(result.getValue(result.columns[8])),
				"floor_id"                  : parseInt(result.getValue(result.columns[9])),
				"room_id"                   : parseInt(result.getValue(result.columns[10])),
				"cage_id"                   : parseInt(result.getValue(result.columns[11])),
				"aisle_id"                  : parseInt(result.getValue(result.columns[12])),
				"rack_id"                   : parseInt(result.getValue(result.columns[13])),
				"rack_unit_id"              : parseInt(result.getValue(result.columns[14])),
				"height_id"                 : parseInt(result.getValue(result.columns[15])),
				"building"                  : result.getValue(result.columns[16]),
				"square_feet"               : parseFloat(result.getValue(result.columns[17])),
				"depth"                     : result.getValue(result.columns[18]),
				"size_id"                   : parseInt(result.getValue(result.columns[19]))
			};
	        return true;
		});
		return [space];
	}
	return {
		get_space: get_space
	};
});
