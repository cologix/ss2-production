/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   8/7/2017
 * Script File:	CLGX2_DW_JSON_ContactLib.js
 * Script Name:	CLGX2_DW_JSON_ContactLib
 * Script Type:	Library
 * 
 * @NApiVersion 2.x
 * @NModuleScope public
 */
require("CLGX2_DW_JSON_ContactLib", ['N/record', 'N/search'],
function(record, search) {
	
	/**
	 * Returns a JSON array of field keys and values.
	 * 
	 * @param {object} recordObject - The open record object.
	 * @return {string}
	 */
	function getRecordJSON(recordObject) {
		var name            = recordObject.getValue({ fieldId: "entityid" }); //Name
		var inactive        = recordObject.getValue({ fieldId: "isinactive" }); //Inactive
		var firstName       = recordObject.getValue({ fieldId: "firstname" }); //First Name
		var lastName        = recordObject.getValue({ fieldId: "lastname" }); //Last Name
		var jobTitle        = recordObject.getValue({ fieldId: "title" }); //Job Title
		var subsidiary      = recordObject.getValue({ fieldId: "subsidiary" }); //Subsidiary
		var company         = recordObject.getValue({ fieldId: "company" }); //Company
		var email           = recordObject.getValue({ fieldId: "email" }); //Email
		var mobilePhone     = recordObject.getValue({ fieldId: "mobilephone" }); //Mobile Phone
		var phone           = recordObject.getValue({ fieldId: "phone" }); //Phone
		var officePhone     = recordObject.getValue({ fieldId: "officephone" }); //Office Phone
		var portalUserId    = recordObject.getValue({ fieldId: "custentity_clgx_modifier" }); //Portal User ID
		var roleId          = recordObject.getValue({ fieldId: "role" }); //Role ID
		var secondaryRoleId = recordObject.getValue({ fieldId: "custentity_clgx_contact_secondary_role" }); //Secondary Role ID
		var portalRights    = recordObject.getValue({ fieldId: "custentity_clgx_cp_user_rights_json" }); //Portal Rights
		var matrixContactId = recordObject.getValue({ fieldId: "custentity_clgx_legacy_contact_id" }); //Matrix Contact ID
		
		var recordJson = JSON.stringify({
			"contact_id"     : recordObject.id,
			"contact"        : name,
			"inactive"       : inactive,
			"first_name"     : firstName,
			"last_name"      : lastName,
			"job_title"      : jobTitle,
			"subsidiary_id"  : subsidiary,
			"customer_id"    : company,
			"email"          : email,
			"mobile"         : mobilePhone,
			"phone"          : phone,
			"phone2"         : officePhone,
			"portal_user_id" : portalUserId,
			"role1_id"       : roleId,
			"role2_id"       : secondaryRoleId,
			"portal_rights"  : portalRights,
			"matrix_id"      : matrixContactId
		});
		
		return recordJson;
	}
	
	
    return {
        getRecord: getRecordJSON
    };
    
});
