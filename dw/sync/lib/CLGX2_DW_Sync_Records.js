/**
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   9/1/2017
 * Script File:	CLGX2_DW_Sync_Records.js
 * Script Name:	CLGX2_DW_Sync_Records
 * Script Type:	Library
 * 
 * @NApiVersion 2.x
 * @NModuleScope public
 */

// ============================================================== CLGX_DW_SYNC_GEOGRAPHY ============================================================== //

function get_clocation() {
	
	var clocation = [];
	var results = search.load({ type: "customrecord_clgx_consolidate_locations", id: "customsearch_clgx_dw_clocation" });
	results.run().each(function(result) {
		clocation.push({
			"clocation_id":		parseInt(result.getValue(result.columns[0])),
			"clocation":			result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return clocation;
}

function get_subsidiary() {
	var subsidiary = [];
	var results = search.load({ type: "subsidiary", id: "customsearch_clgx_dw_subsidiary" });
	results.run().each(function(result) {
		subsidiary.push({
			"subsidiary_id":	parseInt(result.getValue(result.columns[0])),
			"subsidiary":		result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"parent_id":		parseInt(result.getValue(result.columns[4])),
			"currency_id":		parseInt(result.getValue(result.columns[5])),
			"address1":			result.getValue(result.columns[6]),
			"address2":			result.getValue(result.columns[7]),
			"address3":			result.getValue(result.columns[8]),
			"city":				result.getValue(result.columns[9]),
			"zip":				result.getValue(result.columns[10]),
			"state":			result.getValue(result.columns[11]),
			"country":			result.getValue(result.columns[12]),
			"elimination":		result.getValue(result.columns[13])
		});
        return true;
	});
	return subsidiary;
}

function get_location() {
	var location = [];
	var results = search.load({ type: "location", id: "customsearch_clgx_dw_location" });
	results.run().each(function(result) {
		location.push({
			"location_id":		parseInt(result.getValue(result.columns[0])),
			"location":			result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"subsidiary_id":	parseInt(result.getValue(result.columns[4])),
			"rlocation_id":		parseInt(result.getValue(result.columns[5])),
			"address1":			result.getValue(result.columns[6]),
			"address2":			result.getValue(result.columns[7]),
			"address3":			result.getValue(result.columns[8]),
			"city":				result.getValue(result.columns[9]),
			"zip":				result.getValue(result.columns[10]),
			"state":			result.getValue(result.columns[11]),
			"country":			result.getValue(result.columns[12]),
			"phone":			result.getValue(result.columns[13])
		});
        return true;
	});
	return location;
}
	
function get_location_junction_clocation() {
	var location_junction_clocation = [];
	var results = search.load({ type: "customrecord_clgx_consolidate_locations", id: "customsearch_clgx_dw_loc_junction_cloc" });
	results.run().each(function(result) {
		location_junction_clocation.push({
			"clocation_id":		'text',
			"location_id":		parseInt(result.getValue(result.columns[1])),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return location_junction_clocation;
}

function get_facility(location) {
	var facility = [];
	var results = search.load({ type: "customrecord_cologix_facility", id: "customsearch_clgx_dw_facility" });
	results.run().each(function(result) {
		facility.push({
			"facility_id":		parseInt(result.getValue(result.columns[0])),
			"facility":			result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"subsidiary_id":	0,
			"customer_id":		parseInt(result.getValue(result.columns[5])),
			"market_id":		parseInt(result.getValue(result.columns[6])),
			"location_id":		parseInt(result.getValue(result.columns[7])),
			"building_clli_id":	parseInt(result.getValue(result.columns[8])),
			"building_number_id":parseInt(result.getValue(result.columns[9])),
			"address1":			result.getValue(result.columns[10]),
			"address2":			result.getValue(result.columns[11]),
			"city":				result.getValue(result.columns[12]),
			"zip":				result.getValue(result.columns[13]),
			"state":			result.getText(result.columns[14]),
			"country":			result.getText(result.columns[15])
		});
        return true;
	});
	for ( var i = 0; i < facility.length; i++ ) {
		var obj = _.find(location, function(arr){ return (arr.location_id == facility[i].location_id) ; });
		if(obj){
			facility[i].subsidiary_id = obj.subsidiary_id;
		}
	}
	return facility;
}

function get_market(facility) {
	var mlist = get_list('customlist_clgx_market');
	var market = [];
	for ( var i = 0; i < mlist.length; i++ ) {
		var obj = _.find(facility, function(arr){ return (arr.market_id == mlist[i].id) ; });
		market.push({
			"market_id":	mlist[i].id,
			"market":		mlist[i].name,
			"subsidiary_id":obj.subsidiary_id,
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
	}
	return market;
}

function get_rlocation(location) {
	var rlist = get_list('customlist_clgx_reporting_location');
	var rlocation = [];
	for ( var i = 0; i < rlist.length; i++ ) {
		var obj = _.find(location, function(arr){ return (arr.rlocation_id == rlist[i].id) ; });
		if(obj){
			var subsidiary_id = obj.subsidiary_id;
		} else {
			var subsidiary_id = null;
		}
		rlocation.push({
			"rlocation_id":	rlist[i].id,
			"rlocation":	rlist[i].name,
			"subsidiary_id":subsidiary_id,
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
	}
	return rlocation;
}

function get_department() {
	var department = [];
	var results = search.load({ type: "department", id: "customsearch_clgx_dw_department" });
	results.run().each(function(result) {
		
		var department_id = parseInt(result.getValue(result.columns[0]));
		var rec = record.load({ type: "department", id: department_id});
		var parent_id  = parseInt(rec.getValue({ fieldId: "parent" }));
		var dep_subs  = rec.getValue({ fieldId: "subsidiary" });
		
		department.push({
			"department_id":	department_id,
			"department":		result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"parent_id":		parent_id,
			"subsidiary_id":	parseInt(dep_subs[0])
		});
        return true;
	});
	return department;
}
	
// ============================================================== CLGX_DW_SYNC_ENTITIES ============================================================== //

function get_customer_status() {
	var customer_status = [];
	var results = search.create({
		type: search.Type.CUSTOMER,
		filters: [],
		columns: [
			{"name":"status","summary":"GROUP"}
		]
	});
	results.run().each(function(result) {
		customer_status.push({
			"status_id":		parseInt(result.getValue({ name: "status", summary: "GROUP" })),
			"status":			result.getText({ name: "status", summary: "GROUP" }),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return customer_status;
}

function get_customer_segment() {
	var customer_segment = [];
	var results = search.create({
		type: search.Type.CUSTOMER,
		filters: [
			{name: "category",operator: "noneof",values: "@NONE@"}
		],
		columns: [
			{"name":"category","summary":"GROUP"}
		]
	});
	results.run().each(function(result) {
		customer_segment.push({
			"segment_id":		parseInt(result.getValue({ name: "category", summary: "GROUP" })),
			"segment":			result.getText({ name: "category", summary: "GROUP" }),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return customer_segment;
}

function get_language() {
	var language = [];
	var results = search.create({
		type: "customer",
		filters: [
			{name: "language",operator: "noneof",values: "@NONE@"}
		],
		columns: [
			{"name":"language","summary":"GROUP"}
		]
	});
	results.run().each(function(result) {
		language.push({
			"language_id":		result.getValue({ name: "language", summary: "GROUP" }),
			"language":			result.getText({ name: "language", summary: "GROUP" }),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return language;
}
	
function get_vendor_category() {
	var vendor_category = [];
	var results = search.create({
		type: "vendor",
		filters: [
			{name: "category",operator: "noneof",values: "@NONE@"}
		],
		columns: [
			{"name":"category","summary":"GROUP"}
		]
	});
	results.run().each(function(result) {
		vendor_category.push({
			"category_id":		parseInt(result.getValue({ name: "category", summary: "GROUP" })),
			"category":			result.getText({ name: "category", summary: "GROUP" }),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return vendor_category;
}

function get_employee() {
	var employee = [];
	var results = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx_dw_employee" });
	results.run().each(function(result) {
		employee.push({
			"employee_id":		parseInt(result.getValue(result.columns[0])),
			"employee":			result.getValue(result.columns[1]).replace(/'/g, ""),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"first_name":		result.getValue(result.columns[4]).replace(/'/g, ""),
			"last_name":		result.getValue(result.columns[5]).replace(/'/g, ""),
			"title":			result.getValue(result.columns[6]),
			"subsidiary_id":	parseInt(result.getValue(result.columns[7])),
			"department_id":	parseInt(result.getValue(result.columns[8])),
			"location_id":		parseInt(result.getValue(result.columns[9])),
			"facility_id":		parseInt(result.getValue(result.columns[10])),
			"supervisor_id":	parseInt(result.getValue(result.columns[11])),
			"hired_by_id":		parseInt(result.getValue(result.columns[12])),
			"support_rep":		result.getValue(result.columns[13]),
			"sales_rep":		result.getValue(result.columns[14]),
			"email":			result.getValue(result.columns[15]),
			"phone":			result.getValue(result.columns[16]),
			"mobile":			result.getValue(result.columns[17]),
			"skype":			result.getValue(result.columns[18]),
		});
        return true;
	});
	return employee;
}

// ============================================================== CLGX_DW_SYNC_ACCOUNTING ============================================================== //

function get_classification() {
	var classification = [];
	var results = search.load({ type: 'classification', id: "customsearch_clgx_dw_item_class" });
	results.run().each(function(result) {
		var class_id = parseInt(result.getValue(result.columns[0]));
		var rec = record.load({ type: "classification", id: class_id});
		var parent_id  = parseInt(rec.getValue({ fieldId: "parent" }));
		var dep_subs  = rec.getValue({ fieldId: "subsidiary" });
		classification.push({
			"class_id":			class_id,
			"class":				result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"parent_id":			parent_id,
			"subsidiary_id":		parseInt(dep_subs[0])
		});
        
        return true;
	});
	return classification;
}

function get_account() {
	var account = [];
	var results = search.load({ type: 'account', id: "customsearch_clgx_dw_account" });
	results.run().each(function(result) {
		var account_id = parseInt(result.getValue(result.columns[0]));
		var names = result.getValue(result.columns[1]).split(":");
		var name = (names[names.length-1]).trim();
		//name = name.replace(/["'\(\)]/g, "");
		name = name.replace(/'/g, "");
		//var rec = record.load({ type: "account", id: account_id});
		//var parent_id  = parseInt(rec.getValue({ fieldId: "parent" }));
		account.push({
			"account_id":	account_id,
			"account":		name,
			"number":		parseInt(result.getValue(result.columns[2])),
			"inactive":		result.getValue(result.columns[3]),
			"external_id":	result.getValue(result.columns[4]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			//"parent_id":	parent_id,
			"parent_id":	null,
			"acc_type_id":	result.getValue(result.columns[5]),
			"acc_type":		result.getValue(result.columns[6]),
			"balance":		round(parseFloat(result.getValue(result.columns[7]))),
			"description":	result.getValue(result.columns[8]).replace(/'/g, "")
		});
        return true;
	});
	return account;
}

function get_account_type(account) {
	var account_type_ids = _.compact(_.uniq(_.pluck(account, 'acc_type_id')));
	var account_type = [];
	for ( var i = 0; i < account_type_ids.length; i++ ) {
		var obj = _.find(account, function(arr){ return (arr.acc_type_id == account_type_ids[i]) ; });
		account_type.push({
			"account_type_id":	obj.acc_type_id,
			"account_type":		obj.acc_type,
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		})
	}
	return account_type;
}

function get_item() {
	var item = [];
	var results = search.load({ type: 'item', id: "customsearch_clgx_dw_item" });
	results.run().each(function(result) {
		var names = result.getValue(result.columns[1]).split(":");
		var name = (names[names.length-1]).trim();
		item.push({
			"item_id":			parseInt(result.getValue(result.columns[0])),
			"item":				name,
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"name":				result.getValue(result.columns[4]),
			"parent_id":		parseInt(result.getValue(result.columns[5])),
			"class_id":			parseInt(result.getValue(result.columns[6])),
			"category_id":		parseInt(result.getValue(result.columns[7])),
			"expenseaccount_id":parseInt(result.getValue(result.columns[8])),
			"incomeaccount_id":	parseInt(result.getValue(result.columns[9])),
			"amps_id":			parseInt(result.getValue(result.columns[10])),
			"volts_id":			parseInt(result.getValue(result.columns[11])),
			"item_standard_id":	parseInt(result.getValue(result.columns[12]))
		});
        
        return true;
	});
	return item;
}

function get_currency() {
	var currency = [];
	var results = search.create({
		type: "currency",
		filters: [],
		columns: [
			{"name":"internalid"},
			{"name":"name"},
			{"name":"isinactive"},
			{"name":"symbol"},
			{"name":"exchangerate"}
		]
	});
	results.run().each(function(result) {
		currency.push({
			"currency_id":		parseInt(result.getValue({name: "internalid"})),
			"currency":			result.getValue({name: "name"}),
			"isinactive":		result.getValue({name: "isinactive"}),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"symbol":			result.getValue({name: "symbol"}),
			"exchangerate":		parseFloat(result.getValue({name: "exchangerate"}))
		});
        return true;
	});
	return currency;
}

function get_billing_schedule() {
	var billing_schedule = [];
	var results = search.load({ type: 'billingschedule', id: "customsearch_clgx_dw_billing_schedule" });
	results.run().each(function(result) {
		billing_schedule.push({
			"billing_schedule_id":	parseInt(result.getValue(result.columns[0])),
			"billing_schedule":		result.getValue(result.columns[1]),
			"inactive":				result.getValue(result.columns[2]),
			"external_id":			result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"initial_amount":		parseInt(result.getValue(result.columns[4])),
			"frequency":			result.getValue(result.columns[5]),
			"repeat_every":			parseInt(result.getValue(result.columns[6])),
			"bs_count":				parseInt(result.getValue(result.columns[7]))
		});
        return true;
	});
	return billing_schedule;
}

function get_accounting_period() {
	var accounting_period = [];
	var results = search.load({ type: 'accountingperiod', id: "customsearch_clgx_dw_accounting_period" });
	results.run().each(function(result) {
		accounting_period.push({
			"period_id":		parseInt(result.getValue(result.columns[0])),
			"period":			result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"parent_id":		parseInt(result.getValue(result.columns[3])),
			"period_year":		result.getValue(result.columns[4]),
			"period_quarter":	result.getValue(result.columns[5]),
			"period_start":		sqlDate(result.getValue(result.columns[6])),
			"period_end":		sqlDate(result.getValue(result.columns[7]))
		});
        return true;
	});
	return accounting_period;
}

// ============================================================== CLGX_DW_SYNC_TRANSACTIONS ============================================================== //

function get_status() {
	var status = [];
	var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_transaction_status" });
	results.run().each(function(result) {
		status.push({
			"type_id":		result.getValue(result.columns[0]),
			"type":			result.getText(result.columns[0]),
			"status_id":	result.getValue(result.columns[1]),
			"status":		result.getText(result.columns[2]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return status;
}

function get_forecast_type() {
	var forecast_type = [];
	var results = search.create({
		type: "opportunity",
		filters: [],
		columns: [{"name":"forecasttype", summary: "GROUP"}]
	});
	results.run().each(function(result) {
		forecast_type.push({
			"forecast_type_id":		parseInt(result.getValue({name: "forecasttype", summary: "GROUP"})),
			"forecast_type":		result.getText({name: "forecasttype", summary: "GROUP"}),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		});
        return true;
	});
	return forecast_type;
}

function get_lead_source() {
	var lead_source = [];
	var results = search.create({
		type: "opportunity",
		filters: [],
		columns: [{"name":"leadsource", summary: "GROUP"}]
	});
	results.run().each(function(result) {
		var lead_source_id = parseInt(result.getValue({name: "leadsource", summary: "GROUP"}));
		if(lead_source_id){
			lead_source.push({
				"lead_source_id":	parseInt(result.getValue({name: "leadsource", summary: "GROUP"})),
				"lead_source":		result.getText({name: "leadsource", summary: "GROUP"}),
				"source_id": 		1,
				"user_id": 			runtime.getCurrentUser().id,
				"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
			});
		}
        return true;
	});
	return lead_source;
}

function get_contract_terms() {
	var contract_terms = [];
	var results = search.load({ type: 'customrecord_clgx_contract_terms', id: "customsearch_clgx_dw_so_contract_terms" });
	results.run().each(function(result) {
		contract_terms.push({
			"contract_terms_id":		parseInt(result.getValue(result.columns[0])),
			"contract_terms":		result.getValue(result.columns[1]),
			"inactive":				result.getValue(result.columns[2]),
			"source_id": 			1,
			"user_id": 				runtime.getCurrentUser().id,
			"datetime": 				moment().format('YYYY-MM-DD h:mm:ss'),
			"language_id":			parseInt(result.getValue(result.columns[3])),
			"language":				result.getText(result.columns[3]),
			"terms":					result.getValue(result.columns[4]).replace(/'/g, "’")
		});
        return true;
	});
	return contract_terms;
}

function get_language_id(contract_terms) {
	var language_ids = _.compact(_.uniq(_.pluck(contract_terms, 'language_id')));
	var language = [];
	for ( var i = 0; i < language_ids.length; i++ ) {
		var obj = _.find(contract_terms, function(arr){ return (arr.language_id == language_ids[i]) ; });
		language.push({
			"language_id":	obj.language_id,
			"language":		obj.language
		})
	}
	return language;
}


// ============================================================== CLGX_DW_SYNC_VENDOR ============================================================== //

function get_addresses(id) {
	var addresses = [];
	var results = search.load({ type: "entity", id: "customsearch_clgx_dw_entity_address"});
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		addresses.push({
			"company_id":		parseInt(result.getValue(result.columns[0])),
			"company_type":		result.getValue(result.columns[1]),
			"address_id":		parseInt(result.getValue(result.columns[2])),
			"id":				parseInt(result.getValue(result.columns[3])),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"label":			result.getValue(result.columns[4]),
			"attention":		result.getValue(result.columns[5]),
			"addressee":		result.getValue(result.columns[6]),
			"pon":				result.getValue(result.columns[7]),
			"address1":			result.getValue(result.columns[8]),
			"address2":			result.getValue(result.columns[9]),
			"address3":			result.getValue(result.columns[10]),
			"city":				result.getValue(result.columns[11]),
			"state_id":			result.getValue(result.columns[12]),
			"country_id":		result.getValue(result.columns[13]),
			"zip":				result.getValue(result.columns[14]),
			"phone":			result.getValue(result.columns[15]),
			"billing":			result.getValue(result.columns[16]),
			"shipping":			result.getValue(result.columns[17])
		});
        return true;
	});
	return addresses;
}

function get_vendor(id) {
	var vendor = {};
	var results = search.load({ type: 'vendor', id: "customsearch_clgx_dw_vendor" });
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		var names = result.getValue(result.columns[1]).split(":");
		var name = (names[names.length-1]).trim();
		name = name.replace(/'/g, "");
		vendor = {
			"vendor_id":		parseInt(result.getValue(result.columns[0])),
			"vendor":			name,
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"subsidiary_id":	parseInt(result.getValue(result.columns[4])),
			"category_id":		parseInt(result.getValue(result.columns[5])),
			"payment_type_id":	parseInt(result.getValue(result.columns[6])),
			"email_pay_notif":	result.getText(result.columns[7]),
			"individual":		result.getValue(result.columns[8]),
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 		moment().format('YYYY-MM-DD h:mm:ss'),
			"addresses":		get_addresses(id)
		};
        return true;
	});
	return vendor;
}


function get_partner(id) {
	var partner = {};
	var results = search.load({ type: 'partner', id: "customsearch_clgx_dw_partner" });
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		
		var partner_id = parseInt(result.getValue(result.columns[0]));
		var names = result.getValue(result.columns[1]).split(":");
		var name = (names[names.length-1]).trim();
		name = name.replace(/'/g, "");
		
		var ids = 		(result.getValue(result.columns[14])).split(",");
		var territories = [];
		for(var i = 0; ids != null && i < ids.length; i++) {
			var territory_id = parseInt(ids[i]);
			if(territory_id){
				territories.push({
					"partner_id": 	partner_id,
					"territory_id": territory_id,
					"source_id": 		1,
					"user_id": 			runtime.getCurrentUser().id,
					"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
				})
			}
		}
		
		partner = {
			"partner_id":			partner_id,
			"partner":				name,
			"inactive":				result.getValue(result.columns[2]),
			"external_id":			result.getValue(result.columns[3]),
			"source_id": 		1,
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss'),
			"subsidiary_id":		parseInt(result.getValue(result.columns[4])),
			"parent_id":			parseInt(result.getValue(result.columns[5])),
			"sales_rep_id":			parseInt(result.getValue(result.columns[6])),
			"channel_rep_id":		parseInt(result.getValue(result.columns[7])),
			"individual":			result.getValue(result.columns[8]),
			"initial_terms_id":		parseInt(result.getValue(result.columns[9])),
			"initial_percentage":	result.getValue(result.columns[10]),
			"renewal_terms_id":		parseInt(result.getValue(result.columns[11])),
			"renewal_percentage":	result.getValue(result.columns[12]),
			"contract_notes":		result.getValue(result.columns[13]),
			"addresses":			get_addresses(id),
			"territories":			territories
		};
        return true;
	});
	return partner;
}

function get_customer(id) {
	var customer = {};
	var results = search.load({ type: 'customer', id: "customsearch_clgx_dw_customer" });
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		
		var customer_id = parseInt(result.getValue(result.columns[0]));
		var names = result.getValue(result.columns[1]).split(":");
		var name = (names[names.length-1]).trim();
		name = name.replace(/'/g, "");
		
		var ids = (result.getValue(result.columns[43])).split(",");
		var aditional_segments = [];
		for(var i = 0; ids != null && i < ids.length; i++) {
			var aditional_segment_id = parseInt(ids[i]);
			if(aditional_segment_id){
				aditional_segments.push({
					"customer_id": 	customer_id,
					"aditional_segment_id": aditional_segment_id
				})
			}
		}
		var ids = (result.getValue(result.columns[44])).split(",");
		var sla_terms = [];
		for(var i = 0; ids != null && i < ids.length; i++) {
			var sla_terms_id = parseInt(ids[i]);
			if(sla_terms_id){
				sla_terms.push({
					"customer_id": 	customer_id,
					"sla_terms_id": sla_terms_id
				})
			}
		}
		var ids = (result.getValue(result.columns[45])).split(",");
		var required_compliance = [];
		for(var i = 0; ids != null && i < ids.length; i++) {
			var required_compliance_id = parseInt(ids[i]);
			if(required_compliance_id){
				required_compliance.push({
					"customer_id": 	customer_id,
					"required_compliance_id": required_compliance_id
				})
			}
		}
		customer = {
			"customer_id":			customer_id,
			"customer":				name,
			"inactive":				result.getValue(result.columns[2]),
			"external_id":			result.getValue(result.columns[3]),
			"source_id": 			1,
			"user_id": 				runtime.getCurrentUser().id,
			"datetime": 				moment().format('YYYY-MM-DD h:mm:ss'),
			"type":					result.getValue(result.columns[4]),
			"subsidiary_id":			parseInt(result.getValue(result.columns[5])),
			"sales_rep_id":			parseInt(result.getValue(result.columns[6])),
			"status_id":				parseInt(result.getValue(result.columns[7])),
			"parent_id":				parseInt(result.getValue(result.columns[8])),
			"currency_id":			parseInt(result.getValue(result.columns[9])),
			"collection_agent_id":	parseInt(result.getValue(result.columns[10])),
			"ccm_id":				parseInt(result.getValue(result.columns[11])),
			"partner_id":			parseInt(result.getValue(result.columns[12])),
			"lead_source_id":		parseInt(result.getValue(result.columns[13])),
			"language_id":			parseInt(result.getValue(result.columns[14])),
			"balance":				parseFloat(result.getValue(result.columns[15])),
			"matrix_id":				parseInt(result.getValue(result.columns[16])),
			"authorize_net_id":		parseInt(result.getValue(result.columns[17])),
			"credit_card_enabled":	result.getValue(result.columns[18]),
			"credit_card_paused":	result.getValue(result.columns[19]),
			"payment_terms_id":		parseInt(result.getValue(result.columns[20])),
			"msa_id":				parseInt(result.getValue(result.columns[21])),
			"renewal_term_id":		parseInt(result.getValue(result.columns[22])),
			"pub_perm_id":			parseInt(result.getValue(result.columns[23])),
			"support_pack_id":		parseInt(result.getValue(result.columns[24])),
			"disco_notice_term_id":	parseInt(result.getValue(result.columns[25])),
			"segment_id":			parseInt(result.getValue(result.columns[26])),
			"legacy_id":				result.getValue(result.columns[27]),
			"legacy_bill_acc_id":	result.getValue(result.columns[28]),
			"rate_biz_hours":		parseFloat(result.getValue(result.columns[29])),
			"rate_after_hours":		parseFloat(result.getValue(result.columns[30])),
			"strategis_account":		result.getValue(result.columns[31]),
			"has_bill_contact":		result.getValue(result.columns[32]),
			"audit_prod_team":		result.getValue(result.columns[33]),
			"service_schedule":		result.getValue(result.columns[34]),
			"rate_card":				result.getValue(result.columns[35]),
			"hipaa_baa_valid":		result.getValue(result.columns[36]),
			"hosting_agreement":		result.getValue(result.columns[37]),
			"early_term_penalty":	result.getValue(result.columns[38]),
			"current_month_start":	sqlDate(result.getValue(result.columns[39])),
			"current_month_end":		sqlDate(result.getValue(result.columns[40])),
			"current_month_left":	parseInt(result.getValue(result.columns[41])),
			"hold":					result.getValue(result.columns[42]),
			"addresses":				get_addresses(id),
			"aditional_segments":	aditional_segments,
			"sla_terms":				sla_terms,
			"required_compliance":	required_compliance
		};
        return true;
	});
	return customer;
}

function get_portal_user(id) {
	var portal_user = {};
	var results = search.load({ type: 'customrecord_clgx_modifier', id: "customsearch_clgx_dw_portal_user" });
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		portal_user = {
			"portal_user_id":		parseInt(result.getValue(result.columns[0])),
			"portal_user":			result.getValue(result.columns[1]),
			"inactive":				result.getValue(result.columns[2]),
			"external_id":			result.getValue(result.columns[3]),
			"source_id": 			1,
			"user_id": 				runtime.getCurrentUser().id,
			"datetime": 				moment().format('YYYY-MM-DD h:mm:ss'),
			"language_id":			parseInt(result.getValue(result.columns[4])),
			"super_admin_id":		parseInt(result.getValue(result.columns[5])),
			"terms":				result.getText(result.columns[6]),
			"attempts":				parseInt(result.getValue(result.columns[7])),
			"password":				result.getValue(result.columns[8]),
			"password_date":		sqlDate(result.getValue(result.columns[9])),
			"password_reset":		result.getValue(result.columns[10]),
			"recuperate_date":		sqlDate(result.getValue(result.columns[11])),
			"attempts":				parseInt(result.getValue(result.columns[12])),
			"user_id": 				runtime.getCurrentUser().id,
			"datetime": 			moment().format('YYYY-MM-DD h:mm:ss')
		};
        return true;
	});
	return portal_user;
}

function get_contact(id) {
	var contact = {};
	var results = search.load({ type: 'contact', id: "customsearch_clgx_dw_contact" });
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		
		var portal_user_id = parseInt(result.getValue(result.columns[14]));
		var portal_user = {};
		if(portal_user_id){
			portal_user = get_portal_user(portal_user_id);
		}
		contact = {
			"contact_id":		parseInt(result.getValue(result.columns[0])),
			"contact":			result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 			1,
			"user_id": 				runtime.getCurrentUser().id,
			"datetime": 				moment().format('YYYY-MM-DD h:mm:ss'),
			"company_id":		parseInt(result.getValue(result.columns[4])),
			"company_type":		result.getValue(result.columns[5]),
			"subsidiary_id":	parseInt(result.getValue(result.columns[6])),
			"first_name":		result.getValue(result.columns[7]),
			"last_name":		result.getValue(result.columns[8]),
			"job_title":		result.getValue(result.columns[9]),
			"email":			result.getValue(result.columns[10]),
			"mobile":			result.getValue(result.columns[11]),
			"phone":			result.getValue(result.columns[12]),
			"phone2":			result.getValue(result.columns[13]),
			"role1_id":			parseInt(result.getValue(result.columns[15])),
			"role2_id":			parseInt(result.getValue(result.columns[16])),
			"billing":			result.getValue(result.columns[17]),
			"matrix_id":		parseInt(result.getValue(result.columns[18])),
			"portal_user_id":	portal_user_id,
			"portal_rights":	JSON.parse(result.getValue(result.columns[19])),
			"portal_user":		portal_user
		};
        return true;
	});
	return contact;
}


function get_project(id) {
	var project = {};
	var results = search.load({ type: 'job', id: "customsearch_clgx_dw_project" });
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		project = {
			"project_id":		parseInt(result.getValue(result.columns[0])),
			"project":			result.getValue(result.columns[1]),
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 			1,
			"user_id": 				runtime.getCurrentUser().id,
			"datetime": 				moment().format('YYYY-MM-DD h:mm:ss'),
			"name":				result.getValue(result.columns[4]),
			"subsidiary_id":	parseInt(result.getValue(result.columns[5])),
			"facility_id":		parseInt(result.getValue(result.columns[6])),
			"category_id":		parseInt(result.getValue(result.columns[7])),
			"account":			result.getText(result.columns[8]),
			"start":			sqlDate(result.getValue(result.columns[9])),
			"end":				sqlDate(result.getValue(result.columns[10])),
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 		moment().format('YYYY-MM-DD h:mm:ss')
		};
        return true;
	});
	return project;
}


function get_service(id) {
	var project = {};
	var results = search.load({ type: 'job', id: "customsearch_clgx_dw_service" });
	results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: id })];
	results.run().each(function(result) {
		
		var names = result.getValue(result.columns[1]).split(":");
		var name = (names[names.length-1]).trim();
		name = name.replace(/'/g, "");
		
		project = {
			"service_id":		parseInt(result.getValue(result.columns[0])),
			"service":			name,
			"inactive":			result.getValue(result.columns[2]),
			"external_id":		result.getValue(result.columns[3]),
			"source_id": 			1,
			"user_id": 				runtime.getCurrentUser().id,
			"datetime": 				moment().format('YYYY-MM-DD h:mm:ss'),
			"customer_id":		parseInt(result.getValue(result.columns[4])),
			"sales_order_id":	parseInt(result.getValue(result.columns[5])),
			"subsidiary_id":	parseInt(result.getValue(result.columns[6])),
			"facility_id":		parseInt(result.getValue(result.columns[7])),
			"user_id": 			runtime.getCurrentUser().id,
			"datetime": 		moment().format('YYYY-MM-DD h:mm:ss')
		};
        return true;
	});
	return project;
}



		