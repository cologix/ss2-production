/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   8/7/2017
 * Script File:	CLGX2_DW_JSON_ServiceOrderLib.js
 * Script Name:	CLGX2_DW_JSON_ServiceOrderLib
 * Script Type:	Library
 * 
 * @NApiVersion 2.x
 * @NModuleScope public
 */
define(['N/record', 'N/search'],
function(record, search) {
	function checkNull(value, type) {
		if(type == null) {
			return value == "" ? "null" : value;
		} else if(type == "integer") {
			return value == "" ? -1 : parseInt(value);
		} else if(type == "float") {
			return value == "" ? -1.0 : parseFloat(value);
		}
	}
	
	function getRLocationID(locationID) {
		return null;
	}
	
	function getServiceOrderItems(serviceOrderID) {
		var finalArray = [];
		
		var s = search.create({
			type: search.Type.SALES_ORDER,
			filters: [{
				         name: "taxline",
				         operator: "is",
				         values: ["F"]
			          }, {
			        	 name: "mainline",
			        	 operator: "is",
			        	 values: ["F"]
			          }, {
			        	 name: "internalid", 
			        	 operator: "is", 
			        	 values: [serviceOrderID]
			          }],
			columns: [{
				    	  "name":"internalid",
				    	  "join":"item",
				    	  "label":"item_id"
				      }, {
				    	  "name":"CUSTCOL_CLGX_SO_COL_SERVICE_ID",
				    	  "label":"service_id"
				      }, {
				    	  "name":"internalid",
				    	  "join":"location",
				    	  "label":"location_id"
				      }, {
				    	  "name":"formulanumeric",
				    	  "label":"billing_schedule_id",
				    	  "formula":"{billingschedule.id}"
				      }, {
				    	  "name":"billingschedule",
				    	  "label":"billing_schedule"
				      }, {
				    	  "name":"quantity",
				    	  "label":"qty"
				      }, {
				    	  "name":"custcol_clgx_qty2print",
				    	  "label":"quantity"
				      }, {
				    	  "name":"rate",
				    	  "label":"rate"
				      }, {
				    	  "name":"formulacurrency",
				    	  "label":"amount",
				    	  "formula":"{rate}*{custcol_clgx_qty2print}"
				      }, {
				    	  "name":"custcol_clgx_date_closed",
				    	  "label":"date_closed"
				      }, {
				    	  "name":"custcol_clgx_old_rate",
				    	  "label":"old_rate"
				      }, {
				    	  "name":"formulanumeric",
				    	  "label":"class_id",
				    	  "formula":"{class.id}"
				      }, {
				    	  "name":"classnohierarchy",
				    	  "label":"class"
				      }, {
				    	  "name":"formulanumeric",
				    	  "label":"category_id",
				    	  "formula":"{custcol_cologix_invoice_item_category.id}"
				      }, {
				    	  "name":"custcol_cologix_invoice_item_category",
				    	  "label":"category"
				      }, {
				    	  "name":"memo",
				    	  "label":"description"
				      }, {
				    	  "name":"custcol_clgx_rate_per_day_28",
				    	  "label":"rate28"
				      }, {
				    	  "name":"custcol_cologix_rateperday",
				    	  "label":"rate30"
				      }, {
				    	  "name":"custcol_clgx_rate_per_day_31",
				    	  "label":"rate31"
				      }]
		});
		
		s.run().each(function(result) {
			finalArray.push({
				item_id:             checkNull(result.getValue({ name: "internalid", join: "item" }), "integer"),
				service_id:          checkNull(result.getValue({ name: "CUSTCOL_CLGX_SO_COL_SERVICE_ID" }), "integer"),
				location_id:         checkNull(result.getValue({ name: "internalid", join: "location" }), "integer"),
				billing_schedule_id: checkNull(result.getValue({ name: "billingschedule" }), "integer"),
				billing_schedule:    checkNull(result.getText({ name: "billingschedule" })),
				qty:                 checkNull(result.getValue({ name: "quantity" }), "integer"),
				quantity:            checkNull(result.getValue({ name: "custcol_clgx_qty2print" }), "integer"),
				rate:                checkNull(result.getValue({ name: "rate" }), "float"),
				amount:              checkNull((parseFloat(result.getValue({ name: "rate" })) * parseFloat(result.getValue({ name: "custcol_clgx_qty2print" }))), "float"),
				date_closed:         checkNull(result.getValue({ name: "custcol_clgx_date_closed" })),
				old_rate:            checkNull(result.getValue({ name: "custcol_clgx_old_rate" }), "float"),
				class_id:            checkNull(result.getValue({ name: "class" }), "integer"),
				class:               checkNull(result.getText({ name: "classnohierarchy" })),
				category_id:         checkNull(result.getValue({ name: "custcol_cologix_invoice_item_category" }), "integer"),
				category:            checkNull(result.getText({ name: "custcol_cologix_invoice_item_category" })),
				description:         checkNull(result.getValue({ name: "memo" })),
				rate28:              checkNull(result.getValue({ name: "rate28" }), "float"),
				rate30:              checkNull(result.getValue({ name: "rate30" }), "float"),
				rate31:              checkNull(result.getValue({ name: "rate31" }), "float")
			});
			
			return true;
		});
		
		return finalArray;
	}
	
	function getRecordJSON(recordObject) {
		var items = getServiceOrderItems(recordObject.id);
		
		var finalJson    = {};
		var salesOrderId = recordObject.id;
		
		finalJson = { 
			"sales_order_id"         : checkNull(recordObject.getValue({ fieldId: "internalid" }), "integer"), //Internal ID
			"sales_order"            : checkNull(recordObject.getValue({ fieldId: "tranid" }), "integer"),     //Document Number
			"opportunity_id"         : checkNull(recordObject.getValue({ fieldId: "opportunity" }), "integer"), //Opportunity ID
			"proposal_id"            : checkNull(recordObject.getValue({ fieldId: "createdfrom" }), "integer"), //Proposal ID
			"customer_id"            : checkNull(recordObject.getValue({ fieldId: "entity" }), "integer"), //Customer ID
			"subsidiary_id"          : checkNull(recordObject.getValue({ fieldId: "subsidiary" }), "integer"), //Subsidiary ID
			"location_id"            : checkNull(recordObject.getValue({ fieldId: "location" }), "integer"), //Location ID
			"sales_rep_id"           : checkNull(recordObject.getValue({ fieldId: "salesrep" }), "integer"), //Sales Rep ID
			"engineer_id"            : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_sales_engineer" }), "integer"), //Engineer ID
			"technician_id"          : checkNull(recordObject.getValue({ fieldId: "custbody_cologix_assigned_technician" }), "integer"), //Technician ID
			"currency_id"            : checkNull(recordObject.getValue({ fieldId: "currency" }), "integer"), //Currency ID
			"status_id"              : checkNull(recordObject.getValue({ fieldId: "status" }), "integer"), //Status ID
			"lead_source_id"         : checkNull(recordObject.getValue({ fieldId: "leadsource" }), "integer"), //Lead Source ID
			"order_date"             : checkNull(recordObject.getValue({ fieldId: "trandate" })), //Order Date
			"start_date"             : checkNull(recordObject.getValue({ fieldId: "startdate" })), //Start Date
			"target_install_date"    : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_target_install_date" })), //Target Install Date
			"actual_install_date"    : checkNull(recordObject.getValue({ fieldId: "custbody_cologix_service_actl_instl_dt" })), //Actual Install Date
			"contract_end_date"      : checkNull(recordObject.getValue({ fieldId: "enddate" })), //Contract End Date
			"accelerator_date"       : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_next_aa_date" })), //Accelerator Date
			"ip_burst"               : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_ip_burst" })), //IP Burst
			"ip_burst_rate"          : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_ip_burst_rate" })), //IP Burst Rate
			"gb_rate"                : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_gb_rate" })), //GB Rate
			"metered_power"          : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_pwusg_meter_power_usage" })), //Metered Power
			"so_power_type_id"       : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_power_type_so" }), "integer"), //SO Power Type ID
			"hd_colo"                : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_hd_colo_so" })), //HD Colo
			"high_utilization"       : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_hd_high_utilization" })), //High Utilization
			"high_rate"              : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_hd_high_rate" })), //High Rate
			"ultra_high_utilization" : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_hd_ultra_high_utz" })), //Ultra High Utilization
			"ultra_high_rate"        : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_hd_ultra_high_rate" })), //Ultra High Rate
			"legacy_utilization"     : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_legacy_utilization" })), //Legacy Utilization
			"legacy_rate"            : checkNull(recordObject.getValue({ fieldId: "custbody_clgx_legay_rate" })), //Legacy Rate
			"source"                 : checkNull(recordObject.getValue({ fieldId: "source" })), //Source
			"items"                  : items 
		};
		
		//recordObject.getValue({ fieldId: "" }); //RLocation ID
		
		
		return finalJson;
	}
	
	
    return {
        getRecord: getRecordJSON
    };
    
});
