/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"N/record", 
	"N/runtime", 
	"N/search", 
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],
function(record, runtime, search, moment, dwg) {
	
	/**
	 * Returns a JSON object of equipment lists.
	 * 
	 * @access public
	 * @function getEquipmentList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getEquipmentList(procedureName) {
    	var records = {
    		"response" : "",
    		"proc"     : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_EQUIPMENTS" : procedureName.toUpperCase()),
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"datetime" : moment().format("YYYY-MM-DD H:mm:ss"),
    		"equipment_function"         : dwg.get_list('customlist_clgx_function_types_list'),
    		"equipment_name"             : dwg.get_list('customlist_clgx_equipment_name_list'),
    		"equipment_make"             : dwg.get_list('customlist_clgx_equipment_make_list'),
    		"equipment_model"            : dwg.get_list('customlist_clgx_equipment_model_list'),
    		"equipment_port_count"       : dwg.get_list('customlist_clgx_equip_port_count_list'),
    		"equipment_ip_addr"          : dwg.get_list('customlist_clgx_equipment_ip_addr_lst'),
    		"equipment_type"             : dwg.get_list('customlist_cologix_equip_type_lst'),
    		"equipment_rack_unit"        : dwg.get_list('customlist_cologix_space_rack_unit_lst'),
    		"equipment_status"           : dwg.get_list('customlist_cologix_equip_status_list'),
    		"equipment_cpu_lst"          : dwg.get_list('customlist_clgx_cpu_list'),
    		"equipment_disks_lst"        : dwg.get_list('customlist_clgx_disks_list'),
    		"equipment_memory_lst"       : dwg.get_list('customlist_clgx_memory_list'),
    		"inventory_status"           : dwg.get_list('customlist_cologix_inventory_statuslst'),
    		"xc_sequence"                : dwg.get_list('customlist_cologix_xcpath_sequence_lst'),
    		"managed_service_type"       : dwg.get_list('customlist_clgx_managed_services_type'),
    		"managed_service_cluster"    : dwg.get_list('customlist_clgx_cluster'),
    		"managed_service_cloud_vault": dwg.get_list('customlist_clgx_cloud_vault')
    	};
    	
    	return records;
	}
	
    return {
    	getEquipmentList: getEquipmentList
    };
    
});
