/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"/SuiteScripts/clgx/libraries/underscore-min",
	"/SuiteScripts/clgx/libraries/moment.min",
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],
function(_, moment, dwg) {
	
	/**
	 * Returns a JSON object of churn lists.
	 * 
	 * @access public
	 * @function getChurnList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getChurnList(procedureName) {
    	var records = {
    		"response" : "",
    		"proc"     : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_CHURN" : procedureName.toUpperCase()),
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"datetime" : moment().format("YYYY-MM-DD H:mm:ss"),
    		"churn_reason": dwg.get_list("customlist_clgx_ch_reason"), 
	    	"churn_status": dwg.get_list("customlist_clgx_chlist_status"),
	    	"churn_type"  : dwg.get_list("customlist_clgx_churn_type_list")
    	};
    	
    	return records;
	}
	
    return {
    	getChurnList: getChurnList
    };
    
});
