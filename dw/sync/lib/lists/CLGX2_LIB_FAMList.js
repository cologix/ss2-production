/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", 
    "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_FAM",
    "/SuiteScripts/clgx/libraries/moment.min"
],
function(dwg, dwf, moment) {
	
	/**
	 * Returns a JSON object of fam lists.
	 * 
	 * @access public
	 * @function getFAMList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getFAMList(procedureName) {
    	var records = {
    		"response"                         : "",
    		"proc"                             : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_FAMS" : procedureName.toUpperCase()),
    		"enviro"                           : dwg.get_enviro(),
    		"source_id"                        : 1,
    		"user_id"                          : -4,
    		"datetime"                         : moment().format("YYYY-MM-DD H:mm:ss"),
    		"panel_type"                       : dwg.get_list("customlist_clgx_panel_type"),
    		"fam_accrual_convention"           : dwg.get_list("customlist_fam_list_accrual_convention"),
    		"fam_annual_method_entry"          : dwg.get_list("customlist_ncfar_annualentry"),
    		"fam_asset_proposal_status"        : dwg.get_list("customlist_ncfar_proposalstatus"),
    		"fam_asset_status"                 : dwg.get_list("customlist_ncfar_assetstatus"),
    		"fam_conventions"                  : dwg.get_list("customlist_ncfar_conventions"),
    		"fam_depr_hisory_methods"          : dwg.get_list("customlist_ncfar_deprhistmethods"),
    		"fam_depreciation_active"          : dwg.get_list("customlist_ncfar_depractive"),
    		"fam_depreciation_period"          : dwg.get_list("customlist_ncfar_deprperiod"),
    		"fam_depreciation_rules"           : dwg.get_list("customlist_ncfar_deprrules"),
    		"fam_disposal_type"                : dwg.get_list("customlist_ncfar_disposaltype"),
    		"fam_error_handling_type"          : dwg.get_list("customlist_fam_errorhandlingtype"),
    		"fam_final_period_convention"      : dwg.get_list("customlist_fam_list_final_convention"),
    		"fam_forecast_status"              : dwg.get_list("customlist_fam_forecaststatus"),
    		"fam_month_names"                  : dwg.get_list("customlist_far_monthnames"),
    		"fam_period_convention"            : dwg.get_list("customlist_fam_list_period_convention"),
    		"fam_process_status"               : dwg.get_list("customlist_fam_procstatus"),
    		"fam_report_types"                 : dwg.get_list("customlist_fam_reporttypes"),
    		"fam_revision_rules"               : dwg.get_list("customlist_ncfar_revisionrules"),
    		"fam_schedule_options"             : dwg.get_list("customlist_fam_schedule_options"),
    		"fam_setup_summary"                : dwg.get_list("customlist_fam_setupsummary"),
    		"fam_stage_status"                 : dwg.get_list("customlist_fam_procstagestatus"),
    		"fam_tax_category"                 : dwg.get_list("customlist_famassettaxcategory"),
    		"fam_tax_method_status"            : dwg.get_list("customlist_ncfar_taxmethodstatus"),
    		"fam_transaction_type"             : dwg.get_list("customlist_ncfar_transactiontype"),
    		"fam_unit_of_measurement"          : dwg.get_list("customlist_ncfar_unitmeasurment"),
    		"fam_used_accounts"                : dwg.get_list("customlist_fam_list_usedaccounts"),
    		"fam_maintenance_types"            : dwg.get_list("customlist_clgx_fam_maint_type"),
    		//"fam_asset_types"                  : dwf.get_fam_asset_types()[0],
    		//"fam_depreciation_methods"         : dwf.get_fam_depreciation_methods()[0],
    		//"fam_repair_maintenance_categories": dwf.get_fam_repair_maintenance_categories()[0]
    	};
    	
    	return records;
	}
	
    return {
    	getFAMList: getFAMList
    };
    
});
