/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"N/record", 
    "N/search", 
    "/SuiteScripts/clgx/libraries/moment.min", 
    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global",
    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Budget"
],
function(record, search, moment, dwg, dwb) {
	
	/**
	 * Returns a JSON object of budget lists.
	 * 
	 * @access public
	 * @function getBudgetList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getBudgetList(procedureName) {
		var budgets = dwb.get_budgets();
		
    	var records = {
    		"response" : "",
    		"proc"     : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_BUDGETS" : procedureName.toUpperCase()),
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"datetime" : moment().format("YYYY-MM-DD H:mm:ss"),
    		"records"  : budgets
    	};
    	
    	return records;
	}
	
    return {
    	getBudgetList: getBudgetList
    };
    
});
