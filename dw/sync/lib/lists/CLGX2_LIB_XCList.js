/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"N/record", 
	"N/runtime", 
	"N/search", 
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],
function(rrecord, runtime, search, moment, dwg) {
	
	/**
	 * Returns a JSON object of xc lists.
	 * 
	 * @access public
	 * @function getXCList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getXCList(procedureName) {    	
    	var records = {
    		"response" : "",
    		"proc"     : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_XCS" : procedureName.toUpperCase()),
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"xc_type": 					dwg.get_list('customlist_cologix_xconnect_type_lst'),
    		"xc_circuit_type": 			dwg.get_list('customlist_cologix_xconnect_crctyp_lst'),
    		"xc_vlan": 					dwg.get_list('customlist_clgx_vlan_list'),
    		"xc_speed": 				dwg.get_list('customlist_clgx_active_xc_speed'),
    		"xc_type_xc": 				dwg.get_list('customlist_clgx_xc_type'),
    		"xc_vrrp_group": 			dwg.get_list('customlist_clgx_vrrp_group'),
    		"xc_subnet_mask": 			dwg.get_list('customlist_clgx_subnet_mask_list'),
    		"xcpath_sequence": 			dwg.get_list('customlist_cologix_xcpath_sequence_lst'),
    		"xcpath_panel": 			dwg.get_list('customlist_cologix_xconnect_panelname'),
    		"xcpath_slot": 				dwg.get_list('customlist_cologix_xconnect_slotgroup'),
    		"xcpath_connector": 		dwg.get_list('customlist_cologix_xconnect_cnctrtype'),
    		"xcpath_utilisation": 		dwg.get_list('customlist_cologix_xcpath_utilstat_lst'),
    		"vxc_cloud_provider": 		dwg.get_list('customlist_clgx_cloud_provider')
    	};
    	
    	return records;
	}
	
    return {
    	getXCList: getXCList
    };
    
});
