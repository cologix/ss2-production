/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"N/record", 
	"N/runtime", 
	"N/search", 
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],
function(record, runtime, search, moment, dwg) {
	
	/**
	 * Returns a JSON object of power lists.
	 * 
	 * @access public
	 * @function getPowerList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getPowerList(procedureName) {
		var power_breakers = dwg.get_list('customlist_clgx_power_ckt_breaker_no');
    	power_breakers.push({
    		"id": 0,
    		"name": "0"
    	});
    	
    	var records = {
    		"response" : "",
    		"proc"     : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_POWERS" : procedureName.toUpperCase()),
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"power_modules": 			dwg.get_list('customlist_clgx_power_module_no'),
    		"power_lines": 				dwg.get_list('customlist_clgx_power_module_lines'),
    		"power_breakers": 			power_breakers,
    		"power_amps": 				dwg.get_list('customlist_cologix_power_amps_lst'),
    		"power_volts": 				dwg.get_list('customlist_cologix_power_volts_lst'),
    		"power_phases": 			dwg.get_list('customlist_clgx_voltage_phase'),
    		"power_serials": 			dwg.get_list('customlist_clgx_outlet_box_serial_no'),
    		"power_fam_type": 			[{"id":1, "name":"Generator 1"}, {"id":2, "name":"Generator 2"}, {"id":3, "name":"UPS"}, {"id":4, "name":"GPanel"}, {"id":5, "name":"Busway"}],
    		"power_point_type": 		[{"id":1, "name":"Amps"}, {"id":2, "name":"Kw"}, {"id":3, "name":"KwH"}, {"id":4, "name":"Volts"}, {"id":5, "name":"Power Factor"}]
    	};
    	
    	return records;
	}
	
    return {
    	getPowerList: getPowerList
    };
    
});
