/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"N/record", 
	"N/runtime", 
	"N/search", 
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],
function(record, runtime, search, moment, dwg) {
	
	/**
	 * Returns a JSON object of space lists.
	 * 
	 * @access public
	 * @function getSpaceList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getSpaceList(procedureName) {    	
    	var records = {
    		"response" : "",
    		"proc"     : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_SPACES" : procedureName.toUpperCase()),
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"space_floor": 				dwg.get_list('customlist_clgx_space_floor'),
    		"space_room": 				dwg.get_list('customlist_cologix_space_room_lst'),
    		"space_cage": 				dwg.get_list('customlist_cologix_space_cage_lst'),
    		"space_aisle": 				dwg.get_list('customlist_cologix_space_aislerow_lst'),
    		"space_rack": 				dwg.get_list('customlist_clgx_space_rack'),
    		"space_rack_unit": 			dwg.get_list('customlist_cologix_space_rack_unit_lst'),
    		"space_type": 				dwg.get_list('customlist_cologix_space_type_lst'),
    		"space_height": 			dwg.get_list('customlist_clgx_bay_rack_height_space')
    	};
    	
    	return records;
	}
	
    return {
    	getSpaceList: getSpaceList
    };
    
});
