/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"N/record", 
	"N/search", 
	"/SuiteScripts/clgx/libraries/moment.min", 
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],
function(record, search, moment, dwg) {
	/**
	 * Returns a JSON object of item lists.
	 * 
	 * @access public
	 * @function getItemList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getItemList(procedureName) {
		var item    = get_item();
		
    	var records = {
    		"response" : "",
    		"proc"     : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_ITEMS" : procedureName.toUpperCase()),
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"datetime" : moment().format("YYYY-MM-DD H:mm:ss"),
    		"item"     : item
    	};
    	
    	return records;
	}
	
	function get_item() {
		var item = [];
		var results = search.load({ type: 'serviceitem', id: "customsearch_clgx_dw_item" });
		results.run().each(function(result) {
			
			
			var item_id = parseInt(result.getValue(result.columns[0]));
			var full_name = result.getValue(result.columns[1]);
			var rpt_category = get_rpt_category(item_id,full_name);
			var names = full_name.split(":");
			var name = (names[names.length-1]).trim();
			
			var rec = record.load({ type: 'serviceitem', id: item_id });
			var name_fr = rec.getSublistValue({
			    sublistId: 'translations',
			    fieldId: 'displayname',
			    line: 2
			});
			if(!name_fr){
				name_fr = name;
			}
			//log.debug({ title: "Sync ", details: ' | item_id - '+ item_id + ' | name_fr - '+ name_fr + ' |' });
			
			item.push({
				"item_id":			item_id,
				"item":				name,
				"item_fr":			name_fr,
				"full_name":		full_name,
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"name":				result.getValue(result.columns[4]),
				"parent_id":		parseInt(result.getValue(result.columns[5])),
				"class_id":			parseInt(result.getValue(result.columns[6])),
				"category_id":		parseInt(result.getValue(result.columns[7])),
				"expenseaccount_id":parseInt(result.getValue(result.columns[8])),
				"incomeaccount_id":	parseInt(result.getValue(result.columns[9])),
				"amps_id":			parseInt(result.getValue(result.columns[10])),
				"volts_id":			parseInt(result.getValue(result.columns[11])),
				"item_standard_id":	parseInt(result.getValue(result.columns[12])),
				"quantity":			parseFloat(result.getValue(result.columns[13])),
				"rpt_category":		rpt_category
			});
	        
	        return true;
		});
		return item;
	}
    
    function get_rpt_category(item_id,full_name) {
    	var rep_categ = '';
    	
		if(full_name.indexOf('Interconnection') != -1){
			if(full_name.indexOf('Cloud Connect') != -1){
				rep_categ = 'XC - Cloud Connect';
			}
			else if(full_name.indexOf('Copper') != -1){
				rep_categ = 'XC - Copper';
			}
			else if(full_name.indexOf('Coax') != -1){
				rep_categ = 'XC - Coax';
			}
			else if(full_name.indexOf('Fiber') != -1){
				rep_categ = 'XC - Fiber';
			}
			else if(full_name.indexOf('POTS') != -1){
				rep_categ = 'XC - POTS';
			}
			else {
				rep_categ = 'XC - Other';
			}
		} 
		else if(full_name.indexOf('Space') != -1){
			if(full_name.indexOf('Cabinet') != -1){
				rep_categ = 'Space - Cabinets';
			} 
			else if(full_name.indexOf('Foot Print') != -1){
				if(full_name == 'Space : Foot Print'){
					rep_categ = 'Space - Cage';
				}
				else if(full_name.indexOf('Cage') != -1){
					rep_categ = 'Space - Cage';
				}
				else {
					rep_categ = 'Space - Other';
				}
			} 
			else{
				rep_categ = 'Space - Other';
			}
		} 
		else if(full_name.indexOf('Power') != -1){
			rep_categ = 'Power';
		}
		else if(full_name.indexOf('Standard Connections') != -1){
			rep_categ = 'Standard Connections';
		}
		else if(full_name.indexOf('Network') != -1){
			rep_categ = 'Network';
		}
		else if(full_name.indexOf('Managed Services') != -1){
			rep_categ = 'Managed Services';
		}
		else if(full_name.indexOf('Disaster') != -1){
			rep_categ = 'Disaster Recovery';
		}
		else{
			rep_categ = 'Other';
		}
    	return rep_categ;
    }
	
    return {
    	getItemList: getItemList
    };
    
});
