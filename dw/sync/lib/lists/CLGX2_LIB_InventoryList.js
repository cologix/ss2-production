/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/10/2018
 */
define([
	"N/search",
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", 
    "/SuiteScripts/clgx/libraries/moment.min", 
    "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_OutOfBand"
],
function(search, dwg, moment, dwoob) {
	
	/**
	 * Returns a JSON object of inventory lists.
	 * 
	 * @access public
	 * @function getInventoryList
	 * 
	 * @param {string} procedureName
	 * @returns {string}
	 */
	function getInventoryList(procedureName) {
    	var records = {
    		"response"            : "",
    		"proc"                : (typeof procedureName === "undefined" ? "CLGX_DW_SYNC_INVENTORY" : procedureName.toUpperCase()),
    		"enviro"              : dwg.get_enviro(),
    		"source_id"           : 1,
    		"user_id"             : -4,
    		"datetime"            : moment().format("YYYY-MM-DD H:mm:ss"),
    		"oob_firewalls"       : dwg.get_list("customlist_clgx_firewall_list"), //Firewall List
			"oob_dsp"             : dwg.get_list("customlist_clgx_dmz_switch_port_list"), //OOB DMZ Switch Port
			"oob_ict"             : dwg.get_list("customlist_clgx_oob_xc_type"), //OOB Interconnection Type
			"oob_pip"             : dwg.get_list("customlist_clgx_oob_pub_ip_addr_list"), //OOB Public IP Address List
			"oob_cip"             : dwg.get_list("customlist_clgx_oob_cust_ip_addr_lst"), //OOB Customer IP Address List
			"oob_snm"             : dwg.get_list("customlist_clgx_subnet_mask_list"), //OOB Subnet Mask list
			"oob_drg"             : dwg.get_list("customlist_clxg_oob_def_router_list"), //OOB Default Router List
			"oob_vlan"            : dwg.get_list("customlist_clgx_vlan_list"), //VLAN List
			"inventory_status"    : dwg.get_list("customlist_cologix_inventory_statuslst"), //Inventory Status
			"cloud_vault"         : dwg.get_list("customlist_clgx_cloud_vault"),
			"managed_service_type": dwg.get_list("customlist_clgx_managed_services_type"),
			"cluster"             : dwg.get_list("customlist_clgx_cluster"),
			"oob"                 : get_outofbands()
    	};
    	
    	return records;
	}
	
	function get_outofbands() {
		var bands = new Array();
		
		var searchObject = search.load({ type: "customrecord_clgx_oob_facility", id: "customsearch_clgx_dw_outofband" });
		searchObject.run().each(function(result) {
			bands.push(dwoob.get_oob(result.getValue(result.columns[0])));
			
			return true;
		});
		
		return bands;
	}
	
    return {
    	getInventoryList: getInventoryList
    };
    
});
