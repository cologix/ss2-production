/**
 * @NApiVersion 2.x
 */

/*require.config({
	paths: { 
		"underscore" : "/SuiteScripts/clgx/libraries/underscore-min",
		"moment"     : "/SuiteScripts/clgx/libraries/moment.min"
	}
});*/

define(["N/record", "N/search", 
	    "/SuiteScripts/clgx/libraries/underscore-min", 
	    "/SuiteScripts/clgx/libraries/moment.min", 
	    "N/file", "N/task"], 
function(record, search, _, moment, file, task) {

    function get_addresses(record_id) {
		var addresses = [];
		var results = search.load({ type: "entity", id: "customsearch_clgx_dw_entity_address"});
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			
			address_id = parseInt(result.getValue(result.columns[2]));
			if(address_id){
				addresses.push({
					"address_id":		address_id,
					"entity_id":			parseInt(result.getValue(result.columns[0])),
					"id":				parseInt(result.getValue(result.columns[3])),
					"label":				result.getValue(result.columns[4]),
					"attention":			result.getValue(result.columns[5]),
					"addressee":			result.getValue(result.columns[6]),
					"pon":				result.getValue(result.columns[7]),
					"address1":			result.getValue(result.columns[8]),
					"address2":			result.getValue(result.columns[9]),
					"address3":			result.getValue(result.columns[10]),
					"city":				result.getValue(result.columns[11]),
					"state_id":			result.getValue(result.columns[12]),
					"country_id":		result.getValue(result.columns[13]),
					"zip":				result.getValue(result.columns[14]),
					"phone":				result.getValue(result.columns[15]),
					"billing":			get_bit(result.getValue(result.columns[16])),
					"shipping":			get_bit(result.getValue(result.columns[17]))
				});
			}

	        return true;
		});
		return addresses;
	}
	
	function get_enviro() {
		
		return 'prod';
		//return 'dev';
		
	}
	
	function sqlDate(date,time) {
		if(date){
			if(time){
				return moment(date).format('YYYY-MM-DD H:mm:ss');
			} else {
				return moment(date).format('YYYY-M-D');
			}
		} else {
			return null;
		}
	}
	
	function round(value) {
		  //return Number(Math.round(value+'e'+2)+'e-'+2);
		  return parseFloat((parseFloat(value)).toFixed(4))
	}
	function round6(value) {
		  //return Number(Math.round(value+'e'+2)+'e-'+2);
		  return parseFloat((parseFloat(value)).toFixed(6))
	}
	
	function is_mrc(class_id) {
		//var rec = record.load({ type: "item", id: itemid});
		//var id  = parseInt(rec.getValue({ fieldId: "class" }));
		// check class parent to be recurring  (1) or hard code all recurring class ids?
		var ids = [1,4,5,6,7,8,9,10,11,12,13,14]; // all Recurring classes ids
		var ismrc = 1;
		if(ids.indexOf(class_id) == -1){
			ismrc = 0;
		} 
		return ismrc;
	}
	function is_discount(itemid) {
		//var rec = record.load({ type: "item", id: itemid});
		//var name  = rec.getValue({ fieldId: "name" }); // if name contains 'Discount' or hard code discounts items ids ?
		var ids = [531,533,534,535,537,549,551]; // all discounts items ids
		var isdisc = 0;
		if(ids.indexOf(itemid) != -1){
			isdisc = 1;
		} 
		return isdisc;
	}
	
	function get_totals(items) {
		var nrc_total = 0;
		var mrc_total = 0;
		var sub_total = 0;
		var tax_total = 0;
		var discount = 0;
		var total = 0;
		for ( var i = 0; items != null && i < items.length; i++ ) {
			if(items[i].main_line == 0){
				total += items[i].amount;
				if(items[i].tax_line == 0){
					sub_total += items[i].amount;
					if(items[i].is_mrc == 1){
						mrc_total += items[i].amount;
					} else {
						nrc_total += items[i].amount;
					}
				} else {
					tax_total += items[i].amount;
				}
				if(items[i].discount_line == 1){
					discount += items[i].amount;
				}
			}
		}
		return{
			"nrc_total": 	round(nrc_total),
			"mrc_total": 	round(mrc_total),
			"sub_total": 	round(sub_total),
			"tax_total": 	round(tax_total),
			"discount": 		round(discount),
			"total": 		round(total)
		}
	}
	
	function set_queue_flags(queue_id, processed, error){
		record.submitFields({
		    type: "customrecord_clgx_dw_record_queue",
		    id: queue_id,
		    values: {custrecord_clgx_dw_rq_processed: processed, custrecord_clgx_dw_rq_error: error},
		    options: {enableSourcing: false,ignoreMandatoryFields : true}
		});
	}
	
	function set_record_flags(rec_type, record_id, processed, error){
		
		// entities
		if(rec_type == 'vendor' || rec_type == 'partner' || rec_type == 'customer' || rec_type == 'contact' || rec_type == 'job' || rec_type == 'service' || rec_type == 'project'){
			if(rec_type == 'service' || rec_type == 'project'){
				rec_type = 'job';
			}
			record.submitFields({
			    type: rec_type,
			    id: record_id,
			    values: {custentity_clgx_dw_processed: processed, custentity_clgx_dw_error: error},
			    options: {enableSourcing: false,ignoreMandatoryFields : true}
			});
		}
		// transactions
		if(rec_type ==  'opportunity' || rec_type == 'proposal' || rec_type == 'salesorder' || rec_type == 'invoice' || rec_type == 'journalentry' || rec_type == 'creditmemo'){
			if(rec_type == 'proposal'){
				rec_type = 'estimate';
			}
			record.submitFields({
			    type: rec_type,
			    id: record_id,
			    values: {custbody_clgx_dw_rq_processed: processed, custbody_clgx_dw_rq_error: error},
			    options: {enableSourcing: false,ignoreMandatoryFields : true}
			});
		}
		if(rec_type == 'cinvoice'){
			record.submitFields({
			    type: 'customrecord_clgx_consolidated_invoices',
			    id: record_id,
			    values: {custrecord_clgx_cinvoice_dw_processed: processed, custrecord_clgx_cinvoice_dw_error: error},
			    options: {enableSourcing: false,ignoreMandatoryFields : true}
			});
		}

		// inventory
		
		
		
	}
	
	function get_list(list) {
		var arr = [];
		var s = search.create({
			type: list,
			filters: [],
			columns: [{"name":"internalid"},{"name":"name"}]
		});
		s.run().each(function(result) {
			arr.push({
				id: 				parseInt(result.getValue({ name: "internalid"})),
				name:			result.getValue({ name: "name" })
			});
			return true;
		});
		return arr;
	}
	
	function get_bit(value) {
		var return_bit = 1;
		if(value == null || value == 'F' || value == '' || value == 'No' || value == false || value == 'false' || value == 0){
			return_bit = 0;
		}
		return return_bit;
	}
	
	function get_null(value) {
		if(value == '- None -' || value == ''){
			return null;
		}
		return value;
	}

	function is_empty(obj) {
	    for(var key in obj) {
	        if (obj.hasOwnProperty(key)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	function strip_html(string) {
		return string.replace(/(<([^>]+)>)/ig, "");
	}
	
	function objectToCSV(obj) {
		var array = typeof obj != "object" ? JSON.parse(obj) : obj;
        var str = "";

        for (var i = 0; i < array.length; i++) {
            var line = "";
            for (var index in array[i]) {
                if (line != "") line += ","

                line += array[i][index];
            }

            str += line + "\r\n";
        }

        return str;
	}
	
	
	function getRecordTypeID(recordType) {
		var recordID = "";
		
		if(recordType == "Opprtnty") {
			recordID = "opportunity";
		} else if (recordType == "Estimate") {
			recordID = "estimate";
		} else if (recordType == "SalesOrd") {
			recordID = "salesorder";
		} else if (recordType == "CustInvc") {
			recordID = "invoice";
		} else if (recordType == "CustCred") {
			recordID = "creditmemo";
		} else if (recordType == "Journal") {
			recordID = "journalentry";
		} else if (recordType == "PurchOrd") {
			recordID = "purchaseorder";
		} else if (recordType == "CustPymt") {
			recordID = "customerpayment";
		} else if (recordType == "VendBill") {
			recordID = "vendorbill";
		} else if (recordType == "VendPymt") {
			recordID = "vendorpayment";
		} else if (recordType == "Contact") {
			recordID = "contact";
		} else if (recordType == "CustJob") {
			recordID = "job";
		} else if (recordType == "Partner") {
			recordID = "partner";
		}
		
		return recordID;
	}
	
	
	/**
	 * Schedules a script.
	 * 
	 * @access private
	 * @function scheduleScript
	 * 
	 * @libraries N/task
	 * 
	 * @param {Object} paramObject
	 * @returns void
	 */
	function scheduleScript(paramObject) {
		var scriptTask          = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
		
    	scriptTask.scriptId     = paramObject.scriptID;
    	scriptTask.deploymentId = paramObject.deploymentID;
    	
    	if(paramObject.params !== undefined) {
    		scriptTask.params = paramObject.params;
    	}
    	
    	var scriptTaskId = scriptTask.submit();
	}
	
	
	/**
	 * @typedef QueueRecordParamObject
	 * @property {Number} id - The record's internal id in NetSuite.
	 * @property {Number} uid - The user's internal id that is creating the record.
	 * @property {String} type - The record type in NetSuite.
	 * @property {String} event - The action we are performing on the data warehouse.
	 * @property {Boolean} processed - Whether or not the record has been processed.
	 * @property {Number} error - The error code from the data warehouse.
	 * @property {Boolean} historical - Value that determines whether or not to process the record in the historical queue.
	 * @property {Boolean} etl - Value that determines whether or not to process the record in the ETL queue.
	 */
	/**
	 * Creates a Data Warehouse queue record.
	 * 
	 * @access private
	 * @function createQueueRecord
	 * 
	 * @libraries N/record
	 * 
	 * @param {QueueRecordParamObject} paramObject
	 * @returns {null}
	 */
	function createQueueRecord(paramObject) {
		if(typeof paramObject !== undefined || paramObject != null) {
			if(paramObject.debug !== undefined && paramObject.debug == true) {
				log.debug({ title: "createQueueRecord - Debug Log", details: paramObject });
			} else {
				var queue = record.create({ type: "customrecord_clgx_dw_record_queue" });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: parseInt(paramObject.id) });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: paramObject.uid });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: paramObject.type });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: paramObject.event });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: paramObject.processed });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_error",     value: paramObject.error });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_hist",      value: paramObject.historical });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_etl",       value: paramObject.etl });
				queue.save();
			}
		}
	}
	
	
	/**
	 * @typedef CreateJSONParamObject
	 * @property {String} name - The name of the file in the file cabinet.
	 * @property {Number} folder - The internal id of the destination folder in the file cabinet.
	 * @property {String} content - The contents of the file.
	 */
	/**
	 * Creates a JSON file.
	 * 
	 * @access private
	 * @function createJSONFile
	 * 
	 * @libraries N/file
	 * 
	 * @param {CreateJSONParamObject} paramObject
	 * @returns {Number}
	 */
	function createJSONFile(paramObject) {
		var fileObject = file.create({
			name: paramObject.name,
			fileType: file.Type.JSON,
			folder: paramObject.folder,
			contents: JSON.stringify(paramObject.content)
		});
		
		return fileObject.save();
	}
	
	
	return {
		get_addresses	 : get_addresses,
		get_enviro	     : get_enviro,
		sqlDate			 : sqlDate,
		round			 : round,
		round6			 : round6,
		set_queue_flags	 : set_queue_flags,
		set_record_flags : set_record_flags,
		get_list		 : get_list,
		get_bit			 : get_bit,
		get_null		 : get_null,
		is_mrc			 : is_mrc,
		is_empty		 : is_empty,
		is_discount 	 : is_discount,
		get_totals	 	 : get_totals,
		strip_html       : strip_html,
		objectToCSV      : objectToCSV,
		getRecordTypeID  : getRecordTypeID,
		scheduleScript   : scheduleScript,
		createQueueRecord: createQueueRecord,
		createJSONFile   : createJSONFile
	};
});