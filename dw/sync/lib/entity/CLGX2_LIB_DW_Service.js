/**
 * @NApiVersion 2.x
 */

define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_service(record_id) {
		var service = {};
		var results = search.load({ type: 'job', id: "customsearch_clgx_dw_service" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			var names = result.getValue(result.columns[1]).split(":");
			var name = (names[names.length-1]).trim();
			service = {
				"service_id"        : parseInt(result.getValue(result.columns[0])),
				"service"           : name,
				"inactive"          : dwg.get_bit(result.getValue(result.columns[2])),
				"external_id"       : result.getValue(result.columns[3]),
				"customer_id"       : parseInt(result.getValue(result.columns[4])),
				"sales_order_id"    : parseInt(result.getValue(result.columns[5])),
				"subsidiary_id"     : parseInt(result.getValue(result.columns[6])),
				"facility_id"       : parseInt(result.getValue(result.columns[7]))
			};
	        return true;
		});
		return [service];
	}
	
    function get_project(record_id) {
		var project = {};
		var results = search.load({ type: 'job', id: "customsearch_clgx_dw_project" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			project = {
				"project_id"       : parseInt(result.getValue(result.columns[0])),
				"project"          : result.getValue(result.columns[1]),
				"inactive"         : dwg.get_bit(result.getValue(result.columns[2])),
				"external_id"      : result.getValue(result.columns[3]),
				"name"             : result.getValue(result.columns[4]),
				"subsidiary_id"    : parseInt(result.getValue(result.columns[5])),
				"facility_id"      : parseInt(result.getValue(result.columns[6])),
				"category_id"      : parseInt(result.getValue(result.columns[7])),
				"account"          : result.getValue(result.columns[8]),
				"start"            : dwg.sqlDate(result.getValue(result.columns[9])),
				"end"              : dwg.sqlDate(result.getValue(result.columns[10])),
				"estimated_budget" : parseFloat(result.getValue(result.columns[11])),
				"status"           : parseInt(result.getValue(result.columns[12]))
			};
			
	        return true;
		});
		
		return [project];
	}
	
    return {
		get_service: get_service,
		get_project: get_project
	};
});