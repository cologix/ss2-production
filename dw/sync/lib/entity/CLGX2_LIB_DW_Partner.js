/**
 * @NApiVersion 2.x
 */

define(["N/record",  "N/search",  "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	function get_partner(record_id) {
		var partner = {};
		try {
			var rec  = record.load({ type: record.Type.PARTNER, id: record_id });
			
			var ids = (rec.getValue({ fieldId: "custentity_clgx_partner_territories" }));
			var territories = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				territories.push({
					"partner_id"   : record_id,
					"territory_id" : parseInt(ids[i])
				})
			}
			partner = {
					"partner_id"           : record_id,
					"partner"              : rec.getValue({ fieldId:              "entityid" }),
					"inactive"             : dwg.get_bit(rec.getValue({ fieldId:  "isinactive" })),
					"external_id"          : rec.getValue({ fieldId:              "externalid" }),
					"subsidiary_id"        : parseInt(rec.getValue({ fieldId:     "subsidiary" })),
					"parent_id"            : parseInt(rec.getValue({ fieldId:     "parent" })),
					"sales_rep_id"         : parseInt(rec.getValue({ fieldId:     "custentity_clgx_sales_rep" })),
					"channel_rep_id"       : parseInt(rec.getValue({ fieldId:     "custentity_clgx_channel_rep" })),
					"individual"           : dwg.get_bit(rec.getValue({ fieldId:  "isperson" })),
					"initial_terms_id"     : parseInt(rec.getValue({ fieldId:     "custentity_clgx_partner_contract_terms" })),
					"initial_percentage"   : rec.getValue({ fieldId:              "custentity_clgx_partner_percent" }),
					"renewal_terms_id"     : parseInt(rec.getValue({ fieldId:     "custentity_clgx_partner_renew_terms" })),
					"renewal_percentage"   : rec.getValue({ fieldId:              "custentity_clgx_partner_renew_percent" }),
					"contract_notes"       : (rec.getValue({ fieldId:             "custentity_clgx_partner_notes" })).replace(/"/g, ""),
					"addresses"            : dwg.get_addresses(record_id),
					"territories"          : territories
				};
		}
		catch (error) {}
		return [partner];
	}
	return {
		get_partner: get_partner
	};
});