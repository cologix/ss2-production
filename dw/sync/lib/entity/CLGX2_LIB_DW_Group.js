/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzemski - alex.guzenski@cologix.com
 * @date   2/22/2018
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"],
function(record, search, dwg) {
	function get_group(record_id) {
		try {
			var group       = new Object();
			var groupObject = record.load({ type: "entitygroup", id: record_id });
			
			group.group_id        = parseInt(record_id);
			group.name            = groupObject.getValue("groupname");
			group.type            = groupObject.getValue("grouptypename");
			group.saved_search_id = parseInt(groupObject.getValue("savedsearch"));
			group.owner_id        = parseInt(groupObject.getValue("groupowner"));
			group.owner_email     = groupObject.getValue("email");
			group.comments        = groupObject.getValue("comments");
			group.isprivate       = dwg.get_bit(groupObject.getValue("isprivate"));
			group.restrictto      = parseInt(groupObject.getValue("restrictiongroup"));
			group.onlyowneredit   = groupObject.getValue("restrictedtoowner");
			group.inactive        = dwg.get_bit(groupObject.getValue("isinactive"));
			group.issupport       = dwg.get_bit(groupObject.getValue("issupportrep"));
			group.issales         = dwg.get_bit(groupObject.getValue("issalesrep"));
			group.members         = get_group_members(record_id);
			
			return [group];
		} catch(ex) {}
	}
	
	function get_group_members(record_id) {
		var members      = new Array();
		var searchObject = search.load({ type: "entitygroup", id: "customsearch_clgx_dw_group_member" });
		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", values: record_id }));
		searchObject.run().each(function(result) {
			var columns = result.columns;
			
			members.push({
				"group_id"  : parseInt(result.getValue(columns[0])),
				"entity_id" : parseInt(result.getValue(columns[1])),
				"type"      : result.getValue(columns[2]),
				"phone"     : result.getValue(columns[3]),
				"hasbounced": dwg.get_bit(result.getValue(columns[4])),
				"inactive"  : dwg.get_bit(result.getValue(columns[5]))
			});
			
			return true;
		});
		
		return members;
	}
	
    return {
        get_group: get_group
    };
    
});
