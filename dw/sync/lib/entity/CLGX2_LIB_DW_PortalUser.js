/**
 * @NApiVersion 2.x
 */


define(["N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	
	function get_portal_user(record_id) {
		var portal_user = {};
		var results = search.load({ type: "customrecord_clgx_modifier", id: "customsearch_clgx_dw_portal_user" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			portal_user = {
					"portal_user_id"  : parseInt(result.getValue(result.columns[0])),
					"portal_user"     : result.getValue(result.columns[1]),
					"inactive"        : dwg.get_bit(result.getValue(result.columns[2])),
					"external_id"     : result.getValue(result.columns[3]),
					"language_id"     : parseInt(result.getValue(result.columns[4])),
					"super_admin_id"  : parseInt(result.getValue(result.columns[5])),
					"terms"           : dwg.get_bit(result.getText(result.columns[6])),
					"attempts"        : parseInt(result.getValue(result.columns[7])),
					"password"        : result.getValue(result.columns[8]),
					"password_date"   : dwg.sqlDate(result.getValue(result.columns[9])),
					"password_reset"  : dwg.get_bit(result.getValue(result.columns[10])),
					"recuperate_date" : dwg.sqlDate(result.getValue(result.columns[11]))
			};
			
	        return true;
		});
		
		return [portal_user];
	}
	
	return {
		get_portal_user: get_portal_user
	};
});