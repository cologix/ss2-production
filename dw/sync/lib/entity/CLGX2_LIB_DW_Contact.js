/**
 * @NApiVersion 2.x
 */

define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {

	
	
	function get_portal_user(record_id) {
		var portal_user = {};
		try {
			var rec  = record.load({ type: 'customrecord_clgx_modifier', id: record_id });
			portal_user = {
					"portal_user_id"  : record_id,
					"portal_user"     : rec.getValue({ fieldId:              "name" }),
					"inactive"        : dwg.get_bit(rec.getValue({ fieldId:  "isinactive" })),
					"external_id"     : rec.getValue({ fieldId:              "externalid" }),
					"language_id"     : parseInt(rec.getValue({ fieldId:     "custrecord_clgx_modifier_language" })),
					"super_admin_id"  : parseInt(rec.getValue({ fieldId:     "custrecord_clgx_modifier_super_admin" })),
					"terms"           : dwg.get_bit(rec.getValue({ fieldId:  "custrecord_clgx_modifier_accepted_terms" })),
					"attempts"        : parseInt(rec.getValue({ fieldId:     "custrecord_clgx_modifier_login_attempts" })),
					"password"        : rec.getValue({ fieldId:              "custrecord_clgx_modifier_password" }),
					"password_date"   : dwg.sqlDate(rec.getValue({ fieldId:  "custrecord_clgx_modifier_password_date" })),
					"password_reset"  : dwg.get_bit(rec.getValue({ fieldId:  "custrecord_clgx_modifier_reset_password" })),
					"recuperate_date" : dwg.sqlDate(rec.getValue({ fieldId:  "custrecord_clgx_modifier_password_redate" }))
				};
		}
		catch (error) {}
		return [portal_user];
	}

	function get_contact(record_id) {
		
		var contact = {};
		try {
			var rec  = record.load({ type: record.Type.CONTACT, id: record_id });
			
			var company_id = parseInt(rec.getValue({ fieldId: "company" }));
			if(!company_id) { // if null, maybe is employee contact, so check employee subsidiary and hard code company id and type
				company_id = get_employee_company(record_id);
			}
			var company_type = 'CustJob';
			if(company_id) {
				var columns    = search.lookupFields({type: search.Type.ENTITY, id: company_id, columns: ["type"]});
				var company_type   = null;
				if(columns["type"][0]){
					company_type   = columns["type"][0].value;
				}
			}
			
			var portal_user_id = parseInt(rec.getValue({ fieldId: "custentity_clgx_modifier" }));
			var portal_user = {};
			if(portal_user_id){
				portal_user = get_portal_user(portal_user_id);
			}
			
			contact = {
					"contact_id"           : record_id,
					"contact"              : rec.getValue({ fieldId:              "entityid" }),
					"inactive"             : dwg.get_bit(rec.getValue({ fieldId:  "isinactive" })),
					"external_id"          : rec.getValue({ fieldId:              "externalid" }),
					"company_id"           : company_id,
					"company_type"         : company_type,
					"subsidiary_id"        : parseInt(rec.getValue({ fieldId:     "subsidiary" })),
					"first_name"           : rec.getValue({ fieldId:              "firstname" }),
					"last_name"            : rec.getValue({ fieldId:              "lastname" }),
					"job_title"            : rec.getValue({ fieldId:              "title" }),
					"email"                : rec.getValue({ fieldId:              "email" }),
					"mobile"               : rec.getValue({ fieldId:              "mobilephone" }),
					"phone"                : rec.getValue({ fieldId:              "phone" }),
					"phone2"               : rec.getValue({ fieldId:              "altphone" }) || null,
					"role1_id"             : parseInt(rec.getValue({ fieldId:     "contactrole" })),
					"role2_id"             : parseInt(rec.getValue({ fieldId:     "custentity_clgx_contact_secondary_role" })),
					"billing"              : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_is_billing_contact" })),
					"matrix_id"            : parseInt(rec.getValue({ fieldId:     "custentity_clgx_legacy_contact_id" })),
					"portal_user_id"       : portal_user_id,
					"portal_rights"        : dwg.get_null(rec.getValue({ fieldId: "custentity_clgx_cp_user_rights_json" })),
					"portal_user"          : portal_user
				};
		}
		catch (error) {}
		return [contact];
		

		
		/*
		var contact     = {};
		var results     = search.load({ type: 'contact', id: "customsearch_clgx_dw_contact" });
		results.filters = [search.createFilter({ name: "internalid", operator: "IS", values: record_id })];
		results.run().each(function(result) {
			
			var contact_id = parseInt(result.getValue(result.columns[0]));
			var company_id = parseInt(result.getValue(result.columns[4]));
			
			var company_type = 'CustJob';
			if(company_id) {
				var columns    = search.lookupFields({type: search.Type.ENTITY, id: company_id, columns: ["type"]});
				var company_type   = null;
				if(columns["type"][0]){
					company_type   = columns["type"][0].value;
				}
			}
			//log.debug({ title: "get_contact - company_id - 1", details: company_id });
			if(!company_id) { // if null, maybe is employee contact, so check employee subsidiary and hard code company id and type
				//log.debug({ title: "get_contact - company_id - 2", details: company_id });
				company_id = get_employee_company(contact_id);
				//log.debug({ title: "get_contact - company_id - 3", details: company_id });
			}

			var columns    = search.lookupFields({type: search.Type.CONTACT, id: contact_id, columns: ["role"]});
			var role1_id   = null;
			if(columns["role"][0]){
				var role1_id   = parseInt(columns["role"][0].value);
			}
    		
    			var portal_user_id = parseInt(result.getValue(result.columns[13]));
			var portal_user = {};
			if(portal_user_id){
				portal_user = get_portal_user(portal_user_id);
			}
			
			var portal_rights = result.getValue(result.columns[17]);
			if(portal_rights == '- None -' || portal_rights == '' || portal_rights == null){
				portal_rights = null;
			}
			
			contact = {
				"contact_id":		contact_id,
				"contact":			result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"company_id":		company_id,
				"company_type":		company_type,
				"subsidiary_id":		parseInt(result.getValue(result.columns[5])),
				"first_name":		result.getValue(result.columns[6]),
				"last_name":			result.getValue(result.columns[7]),
				"job_title":			result.getValue(result.columns[8]),
				"email":				result.getValue(result.columns[9]),
				"mobile":			result.getValue(result.columns[10]),
				"phone":				result.getValue(result.columns[11]),
				"phone2":			result.getValue(result.columns[12]),
				"role1_id":			role1_id,
				"role2_id":			parseInt(result.getValue(result.columns[14])),
				"billing":			dwg.get_bit(result.getValue(result.columns[15])),
				"matrix_id":			parseInt(result.getValue(result.columns[16])),
				"portal_user_id":	portal_user_id,
				"portal_rights":		portal_rights,
				"portal_user":		portal_user
			};
	        return true;
		});
		return [contact];
		*/
	}

	/**
	 * Returns the company ID from a contact's related employee record depending on subsidiary.
	 * 
	 * @param {Integer} record_id
	 * @return {Integer}
	 */
	function get_employee_company(record_id) {
		var companyID = null;
		var subsidiary = [];
		var results = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx_dw_employee" });
		results.filters.push(search.createFilter({ name: "internalid", operator: "IS", values: record_id }));
		results.run().each(function(result) {
			subsidiary.push(parseInt(result.getValue(result.columns[7])));
	        return true;
		});
		
		if(subsidiary && subsidiary.length > 0) {
			if(subsidiary[0] == 9){ // Cologix Inc
				companyID = 2763;
			}
			else if(subsidiary[0] == 5){ // Cologix US
				companyID = 2790;
			}
			else if(subsidiary[0] == 6){ // Cologix Canada
				companyID = 2789;
			}
			else{
				// 
			}
		}
		return companyID;
	}
	
	return {
		get_contact: get_contact,
		get_portal_user: get_portal_user
	};
});