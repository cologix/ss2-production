/**
 * @NApiVersion 2.x
 */


define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	function get_customer(record_id) {
		
		var customer = {};
		try {
			var rec  = record.load({ type: record.Type.CUSTOMER, id: record_id });
			
			var parent_id = parseInt(rec.getValue({ fieldId: "parent" }));
			if(parent_id == record_id){
				parent_id = null;
			}
			
			var ids = (rec.getValue({ fieldId: "custentity_clgx_addtinl_cat" }));
			var segment_secondary = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				segment_secondary.push({
					"customer_id"   : record_id,
					"segment_id"    : parseInt(ids[i])
				})
			}
			var ids = (rec.getValue({ fieldId: "custentity_clgx_sla_terms_customer" }));
			var sla_terms = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				sla_terms.push({
					"customer_id"   : record_id,
					"sla_terms_id"  : parseInt(ids[i])
				})
			}			
			var ids = (rec.getValue({ fieldId: "custentity_clgx_required_compliance_cust" }));
			var required_compliance = [];
			for(var i = 0; ids != null && i < ids.length; i++) {
				required_compliance.push({
					"customer_id"              : record_id,
					"required_compliance_id"   : parseInt(ids[i])
				})
			}
			customer = {
					"customer_id"          : record_id,
					"customer"             : rec.getValue({ fieldId:              "entityid" }),
					"inactive"             : dwg.get_bit(rec.getValue({ fieldId:  "isinactive" })),
					"external_id"          : rec.getValue({ fieldId:              "externalid" }),
					"type"                 : rec.getValue({ fieldId:              "stage" }),
					"subsidiary_id"        : parseInt(rec.getValue({ fieldId:     "subsidiary" })),
					"sales_rep_id"         : parseInt(rec.getValue({ fieldId:     "salesrep" })),
					"status_id"            : parseInt(rec.getValue({ fieldId:     "entitystatus" })),
					"parent_id"            : parent_id,
					"currency_id"          : parseInt(rec.getValue({ fieldId:     "currency" })),
					"collection_agent_id"  : parseInt(rec.getValue({ fieldId:     "custentity_clgx_collection_cust" })),
					"ccm_id"               : parseInt(rec.getValue({ fieldId:     "custentity_clgx_cust_ccm" })),
					"partner_id"           : parseInt(rec.getValue({ fieldId:     "partner" })),
					"lead_source_id"       : parseInt(rec.getValue({ fieldId:     "leadsource" })),
					"language_id"          : parseInt(rec.getValue({ fieldId:     "language" })),
					"balance"              : parseFloat(rec.getValue({ fieldId:   "custentity_clgx_customer_balance" })),
					"matrix_id"            : dwg.get_null(rec.getValue({ fieldId: "custentity_clgx_matrix_entity_id" })),
					"authorize_net_id"     : dwg.get_null(rec.getValue({ fieldId: "custentity_clgx_customer_authorize_id" })),
					"credit_card_enabled"  : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_cc_enabled" })),
					"credit_card_paused"   : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_cc_paused" })),
					"payment_terms_id"     : parseInt(rec.getValue({ fieldId:     "terms" })),
					"legacy_id"            : dwg.get_null(rec.getValue({ fieldId: "custentity_cologix_legacy_customerid" })),
					"legacy_bill_acc_id"   : dwg.get_null(rec.getValue({ fieldId: "custentity_cologix_billin_ac_id" })),
					"segment_id"           : parseInt(rec.getValue({ fieldId:     "category" })),
					"rate_biz_hours"       : parseFloat(rec.getValue({ fieldId:   "custentity_clgx_normal_hourly_rate" })),
					"rate_after_hours"     : parseFloat(rec.getValue({ fieldId:   "custentity_clgx_after_bus_hour" })),
					"strategis_account"    : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_target_account" })),
					"has_bill_contact"     : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_has_billing_contact" })),
					"audit_prod_team"      : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_audit_product" })),
					"msa_id"               : parseInt(rec.getValue({ fieldId:     "custentity_clgx_msa" })),
					"service_schedule"     : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_ss_flag" })),
					"rate_card"            : rec.getValue({ fieldId:              "custentity_clgx_rate_card" }),
					"hipaa_baa_valid"      : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_hipaa_baa" })),
					"hosting_agreement"    : dwg.get_bit(rec.getValue({ fieldId:  "custentity_clgx_hosting_agreement" })),
					"pub_perm_id"          : parseInt(rec.getValue({ fieldId:     "custentity_clgx_pub_perm" })),
					"renewal_term_id"      : parseInt(rec.getValue({ fieldId:     "custentity_clgx_cust_renewal_term" })),
					"disco_notice_term_id" : parseInt(rec.getValue({ fieldId:     "custentity_cglx_disco_notice_terms" })),
					"early_term_penalty"   : rec.getValue({ fieldId:              "custentity_cglx_early_term_penalty" }),
					"support_pack_id"      : parseInt(rec.getValue({ fieldId:     "custentity_clgx_customer_rh_pack" })),
					"current_month_start"  : dwg.sqlDate(rec.getValue({ fieldId:  "custentity_clgx_customer_rh_currmthstart" })),
					"current_month_end"    : dwg.sqlDate(rec.getValue({ fieldId:  "custentity_clgx_customer_rh_currmthend" })),
					"current_month_left"   : parseInt(rec.getValue({ fieldId:     "custentity_clgx_customer_rh_currmthtl" })),
					"hold"                 : dwg.get_bit(rec.getValue({ fieldId:  "creditholdoverride" })),
					"rate_card_expiration" : dwg.sqlDate(rec.getValue({ fieldId: "custentity_clgx_cust_rc_exp_date" })),
					"addresses"            : dwg.get_addresses(record_id),
					"segment_secondary"    : segment_secondary,
					"sla_terms"            : sla_terms,
					"required_compliance"  : required_compliance
				};
		}
		catch (error) {}
		return [customer];

	}
	
	return {
		get_customer: get_customer
	};
});