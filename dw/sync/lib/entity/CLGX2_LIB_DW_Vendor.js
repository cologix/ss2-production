/**
 * @NApiVersion 2.x
 */

define(["N/record",  "N/search",  "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"], 
function(record, search, dwg) {
	function get_vendor(record_id) {
		var vendor = {};
		try {
			var rec  = record.load({ type: record.Type.VENDOR, id: record_id });
			vendor = {
					"vendor_id"            : record_id,
					"vendor"               : rec.getValue({ fieldId:              "entityid" }),
					"inactive"             : dwg.get_bit(rec.getValue({ fieldId:  "isinactive" })),
					"external_id"          : rec.getValue({ fieldId:              "externalid" }),
					"subsidiary_id"        : parseInt(rec.getValue({ fieldId:     "subsidiary" })),
					"category_id"          : parseInt(rec.getValue({ fieldId:     "category" })),
					"payment_type_id"      : parseInt(rec.getValue({ fieldId:     "custentity_clgx_vendor_pymnt_type" })),
					"email_pay_notif"      : dwg.get_null(rec.getValue({ fieldId: "custentity_2663_email_address_notif" })),
					"individual"           : dwg.get_bit(rec.getValue({ fieldId:  "isperson" })),
					"addresses"            : dwg.get_addresses(record_id)
				};
		}
		catch (error) {}
		return [vendor];
	}
	return {
		get_vendor: get_vendor
	};
});