/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @date 3/26/2018
 * @author Alex Guzenski - alex.guzenski@cologix.com
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", "/SuiteScripts/clgx/libraries/moment.min"],
function(record, search, dwg, moment) {
	/**
	 * Returns a JSON representation of a case record in NetSuite.
	 * 
	 * @param {number} record_id - The case record's internal id.
	 * @returns {( object | null )}
	 */
	function get_case(record_id) {
		
		var caseObj = {};
		try {
			var rec          = record.load({ type: "supportcase", id: record_id });
			var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_case" });
			searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", values: record_id }));
			
			searchObject.run().each(function(result) {
				var columns    = result.columns;
				var assignedto = parseInt(result.getValue(columns[13]));
				var title      = dwg.get_null(result.getValue(columns[2]));
				
				caseObj = {
					"case_id"                     : parseInt(record_id),
					"case"                        : parseInt(result.getValue(columns[1])),
					"title"                       : title == null ? title : title.replace(/'/g, "’"),
					"contact_email"               : dwg.get_null(result.getValue(columns[3])),
					"contact_phone"               : dwg.get_null(rec.getValue("phone")),
					"company_id"                  : parseInt(result.getValue(columns[4])),
					"company_type"                : result.getValue(columns[5]),
					"contact_id"                  : parseInt(result.getValue(columns[6])),
					"stage_id"                    : result.getValue(columns[7]),
					"status_id"                   : parseInt(result.getValue(columns[8])),
					"subsidiary_id"               : parseInt(result.getValue(columns[9])),
					"origin_id"                   : parseInt(result.getValue(columns[10])),
					"case_type_id"                : parseInt(result.getValue(columns[11])),
					"sub_case_type_id"            : parseInt(result.getValue(columns[12])),
					"assignedto_id"               : assignedto,
					"assignedto_type"             : get_employee_type(assignedto),
					"priority_id"                 : parseInt(result.getValue(columns[14])),
					"cross_connect_id"            : parseInt(result.getValue(columns[15])),
					"space_id"                    : parseInt(result.getValue(columns[16])),
					"fam_asset_id"                : parseInt(result.getValue(columns[17])),
					"power_circuit_id"            : parseInt(result.getValue(columns[18])),
					"language_id"                 : parseInt(result.getValue(columns[19])),
					"parent_case_id"              : parseInt(result.getValue(columns[20])),
					"churn_status_id"             : parseInt(result.getValue(columns[21])),
					"power_mmr_id"                : parseInt(result.getValue(columns[22])),
					"space_mmr_id"                : parseInt(result.getValue(columns[23])),
					"network_mmr_id"              : parseInt(result.getValue(columns[24])),
					"interconnection_mmr_id"      : parseInt(result.getValue(columns[25])),
					"total_interconnection_mmr_id": parseInt(result.getValue(columns[26])),
					"churn_type_id"               : parseInt(result.getValue(columns[27])),
					"other_mmr_id"                : parseInt(result.getValue(columns[28])),
					"service_order_id"            : parseInt(result.getValue(columns[29])),
					"managed_service_id"          : parseInt(result.getValue(columns[30])),
					"facility_id"                 : parseInt(result.getValue(columns[31])),
					"profile_id"                  : parseInt(result.getValue(columns[32])),
					"company_security_id"         : dwg.get_null(result.getValue(columns[33])),
					"category_id"                 : parseInt(result.getValue(columns[34])),
					"mmt_id"                      : parseInt(result.getValue(columns[35])),
					"dcim_device_id"              : parseInt(result.getValue(columns[36])),
					"consolidated_invoice_id"     : parseInt(result.getValue(columns[37])),
					"date_created"                : dwg.sqlDate(result.getValue(columns[38]).trim()),
					"incident_date"               : dwg.sqlDate(result.getValue(columns[39]).trim()),
					"start_date"                  : dwg.sqlDate(result.getValue(columns[40]).trim()),
					"end_date"                    : dwg.sqlDate(result.getValue(columns[41]).trim()),
					"date_closed"                 : dwg.sqlDate(result.getValue(columns[42]).trim()),
					"end_time"                    : dwg.get_null(result.getValue(columns[43]).trim()),
					"scheduled_start_time"        : dwg.get_null(result.getValue(columns[44]).trim()),
					"scheduled_followup_date"     : dwg.sqlDate(result.getValue(columns[45]).trim()),
					"urgent_start_date"           : dwg.sqlDate(result.getValue(columns[46]).trim()),
					"urgent_end_date"             : dwg.sqlDate(result.getValue(columns[47]).trim()),
					"mop_start_date"              : dwg.sqlDate(result.getValue(columns[48]).trim()),
					"accounting_period_id"        : parseInt(result.getValue(columns[49])),
					"auto_case_id"                : parseInt(dwg.get_null(result.getValue(columns[50]))),
					"isnotificationrequired"      : dwg.get_bit(result.getValue(columns[52])),
					"ishelpdesk"                  : dwg.get_bit(result.getValue(columns[53])),
					"iscasepastdue"               : dwg.get_bit(result.getValue(columns[54])),
					"iscasenotclosed"             : dwg.get_bit(result.getValue(columns[55])),
					"helpdesk_request"            : dwg.get_bit(dwg.get_null(result.getValue(columns[56]))),
					"waiting_response"            : dwg.get_bit(result.getValue(columns[57])),
					"credit_remittance"           : dwg.get_bit(dwg.get_null(result.getValue(columns[58]))),
					"suspend_alarm"               : dwg.get_bit(result.getValue(columns[59])),
					"messages"                    : get_case_message(record_id),
					"activities"                  : get_case_activities(record_id),
					"user_notes"                  : get_case_notes(record_id),
					"escalations"                 : get_case_escalations(record_id)
					//"messages"                    : [],
					//"activities"                  : [],
					//"user_notes"                  : [],
					//"escalations"                 : [],
				};
				
				return true;
			});
			
		} catch(ex) {
			log.debug({ title: "Error: get_case", details: ex });
		}
		return [caseObj];
	}
	
	
	function get_case_children(record_id) {
		var cases = new Array();
		
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_case" });
		searchObject.filters.push(search.createFilter({ name: "custevent_clgx_parent_id", operator: "anyOf", values: record_id }));
		
		searchObject.run().each(function(result) {
			var columns = result.columns;
			cases.push(parseInt(result.getValue(columns[0])));
			return true;
		});
		
		return cases;
	}
	
	
	function get_case_message(record_id) {
		var messages = new Array();
		
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_case_messages" });
		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", values: record_id }));
		
		searchObject.run().each(function(result) {
			var columns = result.columns;
			var subject = dwg.get_null(result.getValue(columns[5]));
			
			messages.push({
				"case_id"       : parseInt(record_id),
				"message_id"    : parseInt(result.getValue(columns[1])),
				"author_id"     : parseInt(result.getValue(columns[2])),
				"recipient_id"  : parseInt(result.getValue(columns[3])),
				"recipient_type": get_recipient_type(result.getValue(columns[3])),
				"type_id"       : parseInt(result.getValue(columns[4])),
				"subject"       : subject == null ? subject : subject.replace(/'/g, "’"),
				"cc"            : dwg.get_null(result.getValue(columns[6])),
				"bcc"           : dwg.get_null(result.getValue(columns[7])),
				"message"       : escape(result.getValue(columns[8])),
				"date"          : result.getValue(columns[9]),
				"isinternal"    : dwg.get_bit(result.getValue(columns[10])),
				"isincoming"    : dwg.get_bit(result.getValue(columns[11])),
				"issent"        : dwg.get_bit(result.getValue(columns[12])),
				"hasfiles"      : dwg.get_bit(result.getValue(columns[13]))
			});
			
			return true;
		});
		
		return messages;
	}
	
	
	function get_case_activities(record_id) {
		var activities = new Array();
		
		var searchObject = search.load({ type: "activity", id: "customsearch_clgx_dw_case_activity" });
		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", join: "case", values: record_id }));
		
		searchObject.run().each(function(result) {
			var columns     = result.columns;
			var entity_id   = parseInt(result.getValue(columns[4]));
			var entity_type = get_recipient_type(entity_id);
			var title       = dwg.get_null(result.getValue(columns[9]));
			
			activities.push({
				"case_id"     : parseInt(record_id),
				"activity_id" : parseInt(result.getValue(columns[1])),
				"owner_id"    : parseInt(result.getValue(columns[2])),
				"employee_id" : parseInt(result.getValue(columns[3])),
				"entity_id"   : entity_id,
				"entity_type" : entity_type, 
				"contact_id"  : parseInt(result.getValue(columns[5])),
				"status"      : result.getValue(columns[6]),
				"facility_id" : parseInt(result.getValue(columns[7])),
				"type"        : result.getValue(columns[8]),
				"title"       : title == null ? title : title.replace(/'/g, "’"),
				"start_time"  : result.getValue(columns[10]),
				"start_date"  : dwg.sqlDate(result.getValue(columns[11])),
				"end_date"    : dwg.sqlDate(result.getValue(columns[12]))
			});
			
			return true;
		});
		
		return activities;
	}
	
	
	function get_case_notes(record_id) {
		var notes = new Array();
		
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_case_notes" });
		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", values: record_id }));
		
		searchObject.run().each(function(result) {
			var columns = result.columns;
			
			notes.push({
				"case_id": parseInt(record_id),
				"note_id": parseInt(result.getValue(columns[1])),
				"type_id": parseInt(result.getValue(columns[2])),
				"title"  : result.getValue(columns[3]).replace(/'/g, "’"),
				"author" : result.getValue(columns[4]),
				"company": result.getValue(columns[5]),
				"memo"   : result.getValue(columns[6]).replace(/'/g, "’"),
				"date"   : dwg.sqlDate(result.getValue(columns[7]))
			});
			
			return true;
		});
		
		return notes;
	}
	
	
	function get_case_escalations(record_id) {
		var escalations = new Array();
		
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_case_escalations" });
		searchObject.filters.push(search.createFilter({ name: "internalid", operator: "anyOf", values: record_id }));
		
		searchObject.run().each(function(result) {
			var columns = result.columns;
			
			escalations.push({
				"case_id"    : parseInt(record_id),
				"entity_id"  : parseInt(result.getValue(columns[1])),
				"entity_type": get_escalatee_entity(result.getValue(columns[1])),
				"entity"     : result.getValue(columns[2])
			});
			
			return true;
		});
		
		return escalations;
	}
	
	
	/**
	 * Returns the type of recipient record.
	 * 
	 * @param {number} recipient_id - The recipient's internal id.
	 * @returns {(string | null)}
	 */
	function get_recipient_type(recipient_id) {
		var type = null;
		
		if(type == null) {
			try {
				record.load({ type: "customer", id: recipient_id });
				type = "customer";
			} catch(ex) {}
		}
		
		if(type == null) {
			try {
				record.load({ type: "vendor", id: recipient_id });
				type = "vendor";
			} catch(ex) {}
		}
		
		if(type == null) {
			try {
				record.load({ type: "partner", id: recipient_id });
				type = "partner";
			} catch(ex) {}
		}
		
		return type;
	}
	
	
	/**
	 * Returns what type of record the case was assigned to.
	 * 
	 * @param {number} employee_id - The employee or employee group's internal id.
	 * @returns {(string | null)}
	 */
	function get_employee_type(employee_id) {
		var type = "";
		
		if(type == "") {
			try {
				record.load({ type: "employee", id: employee_id });
				type = "employee";
			} catch(ex) {}
		}
		
		if(type == "") {
			try {
				record.load({ type: "entitygroup", id: employee_id });
				type = "group";
			} catch(ex) {}
		}
		
		return type;
	}
	
	
	function get_escalatee_entity(entity_id) {
		var type = "";
		
		if(type == "") {
			try {
				record.load({ type: "employee", id: entity_id });
				type = "employee";
			} catch(ex) {}
		}
		
		if(type == "") {
			try {
				record.load({ type: "entitygroup", id: entity_id });
				type = "group";
			} catch(ex) {}
		}
		
		if(type == "") {
			try {
				record.load({ type: "vendor", id: entity_id });
				type = "vendor";
			} catch(ex) {}
		}
		
		if(type == "") {
			try {
				record.load({ type: "partner", id: entity_id });
				type = "partner";
			} catch(ex) {}
		}
		
		return type;
	} 
	
	
    return {
        get_case: get_case
    };
    
});
