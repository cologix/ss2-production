/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	10/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Account.js
 * Script Name	:	CLGX2_SS_DW_Sync_Account
 * Script ID	:   customscript_clgx2_ss_dw_sync_account
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_account
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/https",
		"N/record", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/underscore-min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
		
function(file, https, record, search, _, moment, dwg) {

	function execute(scriptContext) {
		
	    	var account = get_account();
	    	var records = {
	    		"response": 			'',
	    		"proc": 				'CLGX_DW_SYNC_ACCOUNT',
	    		"enviro": 				dwg.get_enviro(),
	    		"source_id": 			1,
	    		"user_id": 				-4,
	    		"datetime": 			moment().format('YYYY-MM-DD H:mm:ss'),
	    		"fam_used_accounts": 	dwg.get_list('customlist_fam_list_usedaccounts'),
	    		"account": 				account.account,
	    		"account_type": 		account.account_type,
	    		"account_subsidiary": 	account.account_subsidiary,
	    		"account_fam": 			account.account_fam
	    	};
    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}
		
		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

	function get_account() {
		var account = [];
		var account_type = [];
		var account_type_ids = [];
		var subsidiary = [];
		var fam = [];
		var results = search.load({ type: 'account', id: "customsearch_clgx_dw_account" });
		results.run().each(function(result) {
			
			var account_id = parseInt(result.getValue(result.columns[0]));
			var names = result.getValue(result.columns[1]).split(":");
			var name = (names[names.length-1]).trim();
			//name = name.replace(/["'\(\)]/g, "");
			name = name.replace(/'/g, "");
			
			var acc_type_id = result.getValue(result.columns[5]);
			var acc_type = result.getValue(result.columns[6]);
			if(account_type_ids.indexOf(acc_type_id) == -1){
				account_type_ids.push(acc_type_id);
				account_type.push({
					"account_type_id":	acc_type_id,
					"account_type":		acc_type
				})
			}
			
			var rec = record.load({ type: "account", id: account_id});
			var parent_id  = parseInt(rec.getValue({ fieldId: "parent" }));
			
			var ids  = rec.getValue({ fieldId: "subsidiary" });
			for(var i = 0; ids != null && i < ids.length; i++) {
				var subsidiary_id = parseInt(ids[i]);
				if(subsidiary_id){
					subsidiary.push({
						"account_id": 		account_id,
						"subsidiary_id": 	subsidiary_id
					})
				}
			}
			
			var ids  = rec.getValue({ fieldId: "custrecord_fam_account_showinfixedasset" });
			for(var i = 0; ids != null && i < ids.length; i++) {
				var fam_id = parseInt(ids[i]);
				if(fam_id){
					fam.push({
						"account_id": 		account_id,
						"fam_id": 			fam_id
					})
				}
			}

			account.push({
				"account_id":	account_id,
				"account":		name,
				"number":		parseInt(result.getValue(result.columns[2])),
				"inactive":		dwg.get_bit(result.getValue(result.columns[3])),
				"external_id":	result.getValue(result.columns[4]),
				"parent_id":	parent_id,
				"acc_type_id":	result.getValue(result.columns[5]),
				"acc_type":		result.getValue(result.columns[6]),
				"balance":		dwg.round(parseFloat(result.getValue(result.columns[7]))),
				"description":	result.getValue(result.columns[8]).replace(/'/g, "")
			});
	        return true;
		});
		
		return {
			"account":account,
			"account_type":account_type,
			"account_subsidiary":subsidiary,
			"account_fam":fam
		};
	}

});
