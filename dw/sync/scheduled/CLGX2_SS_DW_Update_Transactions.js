/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 8/28/2018
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", "N/runtime", "N/file", "/SuiteScripts/clgx/libraries/lodash.min"],
function(record, search, dwg, runtime, file, _) {
    function execute(scriptContext) {
    	var sc              = runtime.getCurrentScript();
    	
    	var paramObject          = new Object();
    	paramObject.scriptID     = sc.id;
		paramObject.deploymentID = sc.deploymentId;
	    paramObject.recordType   = sc.getParameter("custscript_clgx2_ut_sst");
	    paramObject.savedSearch  = sc.getParameter("custscript_clgx2_ut_ss");
	    paramObject.fileID       = parseInt(sc.getParameter("custscript_clgx2_ut_fid"));
    	paramObject.initialIndex = parseInt(sc.getParameter("custscript_clgx2_ut_idx"));
    	paramObject.currentIndex = 0;
    	paramObject.pageSize     = parseInt(sc.getParameter("custscript_clgx2_ut_ps"));
    	
    	if(paramObject.fileID) {
    		processRecordFile({ fileID: paramObject.fileID });
    	} else {
    		var records = buildRecordFile(paramObject);
    		var fileID  = dwg.createJSONFile({ name: "clgx-transaction-update.json", folder: 5453175, content: records });
    		
    		dwg.scheduleScript({
				scriptID    : 1635,
				deploymentID: "customdeploy_clgx2_ss_dw_updt_trans", 
				params      : { custscript_clgx2_ut_fid: fileID }
			});
    	}
    }
    
    
    function processRecordFile(paramObject) {
    	var fileObject    = file.load({ id: paramObject.fileID });
		var currentObject = JSON.parse(fileObject.getContents());
		var usage         = runtime.getCurrentScript().getRemainingUsage();
		var recordCount   = currentObject.length;
		
		for(var r = 0; r < recordCount; r++) {
			var currentRecord = currentObject.pop();
			dwg.createQueueRecord({ 
				id        : currentRecord.id,
				uid       : -4,
				type      : currentRecord.type, 
				event     : "create",
				processed : false,
				error     : 0,
				historical: false,
				etl       : false
			});
			
			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 300 ) {
				break;
			}
		}
		
		if(currentObject.length > 0) {
			var fileID  = dwg.createJSONFile({ name: "clgx-transaction-update.json", folder: 5453175, content: currentObject });
			
			dwg.scheduleScript({
				scriptID    : runtime.getCurrentScript().id,
				deploymentID: runtime.getCurrentScript().deploymentId,
				params      : { custscript_clgx2_ut_fid: fileID } 
			});
		} else {
			dwg.createJSONFile({ name: "clgx-transaction-update.json", folder: 5453175, content: currentObject });
		}
    }
    
    
    /**
     * Handles initial file creation and rescheduling.
     * 
     * @access private
     * @function buildRecordFile
     * 
     * @param {ParamObject} paramObject
     * @returns null
     */
    function buildRecordFile(paramObject) {
    	var recordArray     = new Array();
    	
    	var processResults  = null;
    	var existingContent = "";
    	var globalIndex     = 0;
    	
    	var filters         = new Array();
    	var columns         = new Array();
    	var records         = new Array();
    	
    	var searchObject  = search.load({ id: paramObject.savedSearch }).run();
    	var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
    	
    	processResults = processResultSet(paramObject, searchResults);
    	
    	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
    		recordArray = processResults.records;
    	}
    	
    	while (processResults.nextPage == true) {
    		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
    		processResults = processResultSet(paramObject, searchResults);
    		recordArray = recordArray.concat(processResults.records);
    	}
    	
    	return recordArray;
    }
    
    
    /**
     * Processes each page of results.
     * 
     * @access private
     * @function processResultSet
     * 
     * @param {ParamObject} paramObject
     * @param {Object} searchResults
     * @returns {Object}
     */
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var records      = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		records.push({ 
    			"id": parseInt(searchResults[r].getValue(searchResults[r].columns[0])), 
    			"type": getRecordTypeID(searchResults[r].getText(searchResults[r].columns[1]))
    		});
    		
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, records: records }; 
    }
    

    function getRecordTypeID(recordType) {
    	if(recordType == "Invoice") {
    		return "invoice";
    	} else if(recordType == "Journal") {
    		return "journalentry";
    	} else if(recordType == "Credit Memo") {
    		return "creditmemo";
    	} else {
    		return "";
    	}
    }
    
    return {
        execute: execute
    };
    
});