/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Lists.js
 * Script Name	:	CLGX2_SS_DW_Sync_Lists
 * Script ID	:   customscript_clgx2_ss_dw_sync_lists
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_lists
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define(["N/task", "N/runtime"],
		
function(task, runtime) {
	
	function execute(scriptContext) {
		if(runtime.envType === runtime.EnvType.PRODUCTION) {
			try {
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1307;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_geography';
				var scriptTaskId = scriptTask.submit();
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1308;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_entities';
				var scriptTaskId = scriptTask.submit();
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1310;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_accounting';
				var scriptTaskId = scriptTask.submit();
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1337;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_account';
				var scriptTaskId = scriptTask.submit();
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1338;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_items';
				var scriptTaskId = scriptTask.submit();
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1311;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_transact';
				var scriptTaskId = scriptTask.submit();	
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1454;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_budget2018';
				var scriptTaskId = scriptTask.submit();	
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1437;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_equipments';
				var scriptTaskId = scriptTask.submit();	
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1439;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_spaces';
				var scriptTaskId = scriptTask.submit();	
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1440;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_powers';
				var scriptTaskId = scriptTask.submit();	
				
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1442;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_xcs';
				var scriptTaskId = scriptTask.submit();	
				
				var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			    scriptTask.scriptId = 1450;
			    scriptTask.params = {custscript_clgx2_dw_changed_entity_page : 0};
			    scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_chng_ent';
			    var scriptTaskId = scriptTask.submit();
		    	
				var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			    scriptTask.scriptId = 1447;
			    scriptTask.params = {custscript_clgx2_dw_changed_tran_page : 0};
			    scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_chng_tran';
			    var scriptTaskId = scriptTask.submit();
		    	
			    var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			    scriptTask.scriptId = 1477;
			    scriptTask.deploymentId = 'customdeploy_clgx_ss_dw_sync_crm';
			    var scriptTaskId = scriptTask.submit();
					
			    var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			    scriptTask.scriptId = 1478;
			    scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_inventory';
			    var scriptTaskId = scriptTask.submit();
			    	
			    var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			    scriptTask.scriptId = 1487;
			    scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_churn';
			    var scriptTaskId = scriptTask.submit();
		    	
			    var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			    scriptTask.scriptId = 1488;
			    scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_fam';
			    var scriptTaskId = scriptTask.submit();
			    
				/*
				var scriptTask = task.create({taskType: 'SCHEDULED_SCRIPT'});
				scriptTask.scriptId = 1444;
				scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_audit';
				var scriptTaskId = scriptTask.submit();	
				*/
			} catch (error) {}
		}
    }

    return {
        execute: execute
    };
    
});
