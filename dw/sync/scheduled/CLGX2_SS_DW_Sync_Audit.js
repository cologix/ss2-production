/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		    :	1/4/2018
 * Script File	:	CLGX2_SS_DW_Sync_Audit.js
 * Script Name	:	CLGX2_SS_DW_Sync_Audit
 * Script ID 	:   customscript_clgx2_ss_dw_sync_audit
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_audit
 *
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
        "N/file",
        "N/task",
        "N/https",
        "N/record",
        "N/runtime"
    ],
    function(file, task, https, record, runtime) {

        function execute(scriptContext) {

            var scriptObj = runtime.getCurrentScript();
            var deployment = scriptObj.deploymentId;

            var enviro = 'prod';
            var status = 'create';
            var cf_script = 'clgx_dw_get_audit_ids.cfm'

            // CLGX2_SS_DW_Sync_Audit_Entities
            if(deployment == 'customdeploy_clgx2_ss_dw_sync_audit_ent'){
                var proc = 'CLGX_DW_AUDIT_ENTITY';
                cf_script = 'clgx_dw_get_json.cfm';
            }

            // CLGX2_SS_DW_Sync_Audit_Opportunities
            if(deployment == 'customdeploy_clgx2_ss_dw_sync_audit_oppt'){
                var rec_type = 'opportunity';
                var proc = 'CLGX_DW_AUDIT_OPPORTUNITY';
            }
            // CLGX2_SS_DW_Sync_Audit_Proposals
            if(deployment == 'customdeploy_clgx2_ss_dw_sync_audit_prop'){
                var rec_type = 'proposal';
                var proc = 'CLGX_DW_AUDIT_PROPOSAL';
            }
            // CLGX2_SS_DW_Sync_Audit_SalesOrders
            if(deployment == 'customdeploy_clgx2_ss_dw_sync_audit_sos'){
                var rec_type = 'proposal';
                var proc = 'CLGX_DW_AUDIT_SALESORDER';
            }
            // CLGX2_SS_DW_Sync_Audit_Invoices
            if(deployment == 'customdeploy_clgx2_ss_dw_sync_audit_inv'){
                var rec_type = 'proposal';
                var proc = 'CLGX_DW_AUDIT_INVOICE';
            }



            var body = {
                "proc":   proc,
                "enviro": enviro,
                "status": status
            };
            var arr = [];
            try {
                var requestURL = https.post({
                    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/' + cf_script,
                    body: JSON.stringify(body)
                });
                arr = JSON.parse(requestURL.body);
            } catch (error) {}

            log.debug({ title: "Audit", details: arr.length});

            if(deployment == 'customdeploy_clgx2_ss_dw_sync_audit_ent'){
                for ( var i = 0; i < arr.length; i++ ) {
                    //if(arr[i].Sync_Status == 'create'){
                    var queue = record.create({ type: "customrecord_clgx_dw_record_queue" });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: arr[i].ID                     });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: -4                            });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: arr[i].Record_Type            });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: arr[i].Sync_Status            });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: false                         });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_error", 	 value: 0                             });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_hist", 	 value: false                         });
                    queue.save();
                    //}
                    var scriptObj = runtime.getCurrentScript();
                    var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
                    var index = i + 1;
                    log.debug({ title: "Audit", details: ' | index : ' + index + ' / ' + arr.length + ' | rec_type : '+ arr[i].Record_Type + ' | record_id : '+ arr[i].ID + ' | status : '+ arr[i].Sync_Status + '  |'});
                }
            }
            else{
                for ( var i = 0; i < arr.length; i++ ) {

                    var queue = record.create({ type: "customrecord_clgx_dw_record_queue" });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: arr[i]                        });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: -4                            });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: rec_type                      });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: status                        });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: false                         });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_error", 	 value: 0                             });
                    queue.setValue({ fieldId: "custrecord_clgx_dw_rq_hist", 	 value: false                         });
                    queue.save();

                    var scriptObj = runtime.getCurrentScript();
                    var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
                    var index = i + 1;
                    log.debug({ title: "Audit", details: ' | index : ' + index + ' / ' + arr.length + ' | rec_type : '+ rec_type + ' | record_id : '+ arr[i] + ' | status : '+ status + '  |'});
                }
            }
        }

        return {
            execute: execute
        };

    });

