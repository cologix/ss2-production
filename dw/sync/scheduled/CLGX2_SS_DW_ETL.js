/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   6/21/2018
 */
define(["N/record", "N/https", "N/file", "N/search", "N/runtime", "N/task", "/SuiteScripts/clgx/libraries/lodash.min", "/SuiteScripts/clgx/libraries/moment.min"],
function(record, https, file, search, runtime, task, _, moment) {
	/**
	 * @typedef ParamObject
	 * @property {Number} scriptID - The internal id of the current script.
	 * @property {Number} deploymentID - The deployment id of the current script.
	 * @property {String} dwTable - The table we are pulling data from in the data warehouse.
	 * @property {String} recordType - The record type we are searching for in NetSuite.
	 * @property {Number} fileID - The array storage file internal id in NetSuite.
	 * @property {String} fileName - The name of the temporary file in NetSuite.
	 * @property {Number} initialIndex - The initial index of records we are processing.
	 * @property {Number} currentIndex - The current index of records we are currently processing.
	 * @property {Number} pageSize - The page size of each block of records we will process.
	 */
	
    function execute(scriptContext) {
    	try {
    		var sc = runtime.getCurrentScript();
    		
    		var paramObject = new Object();
    		paramObject.scriptID     = sc.id;
    		paramObject.deploymentID = sc.deploymentId;
    		paramObject.dwTable      = sc.getParameter("custscript_clgx2_dw_nsetl_dwt");
    		paramObject.dwTableKey   = sc.getParameter("custscript_clgx2_dw_nsetl_dwtk");
    	    paramObject.recordType   = sc.getParameter("custscript_clgx2_dw_nsetl_record");
    	    paramObject.savedSearch  = sc.getParameter("custscript_clgx2_dw_nsetl_search");
    	    paramObject.fileID       = parseInt(sc.getParameter("custscript_clgx2_dw_nsetl_fid"));
    	    paramObject.fileName     = sc.getParameter("custscript_clgx2_dw_nsetl_fn");
        	paramObject.initialIndex = parseInt(sc.getParameter("custscript_clgx2_dw_nsetl_idx"));
        	paramObject.currentIndex = 0;
        	paramObject.pageSize     = parseInt(sc.getParameter("custscript_clgx2_dw_nsetl_ps"));
        	
        	if(paramObject.fileID) {
        		processRecordFile({ fileID: paramObject.fileID, recordType: paramObject.recordType });
        	} else {
        		var nsids = createNSIDArray(paramObject);
            	var dwids = createDWIDArray(paramObject);
            	
            	var snsids = _.sortBy(nsids);
            	var sdwids = _.sortBy(dwids);
            	
            	var dadds    = _.difference(snsids, sdwids);
            	var ddeletes = _.difference(sdwids, snsids);
            	
            	var adds    = _.map(dadds, function(value, key) { return { id: dadds[key], type: paramObject.recordType, event: "create" }; });
            	var deletes = _.map(ddeletes, function(value, key) { return { id: ddeletes[key], type: paramObject.recordType, event: "delete" }; });
            	
            	var records = adds.concat(deletes);
            	
            	var calculatedUsage = (records.length * 2);
            	
            	if(calculatedUsage >= sc.getRemainingUsage()) {
            		log.error({ title: "Usage Error", details: "Not enough usage points to complete processing (" + calculatedUsage + "/" + sc.getRemainingUsage() + ")" });
            		
            		var fileID = createJSONFile({
            			type: paramObject.recordType, 
            			content: records
            		});
            		
            		scheduleScript({
            			scriptID: sc.id,
            			deploymentID: sc.deploymentId,
            			params: { custscript_clgx2_dw_nsetl_fid: fileID }
            		});
            	} else {
            		if(records.length > 0) { createQueueRecords(records); }
            		log.audit({ title: paramObject.recordType + " Summary", details: "Added: " + adds.length + " - Deleted: " + deletes.length + " - Remaining Usage: " + sc.getRemainingUsage() });
            	}
        	}
    	} catch(ex) {
    		log.error({ title: "CLGX2_SS_DW_ETL_NS", details: ex });
    	}
    }
    
    
    /**
     * Process the JSON file created.
     *  
     * @access private
     * @function processRecordFile
     * 
     * @libraries N/file, N/record, N/task
     *  
     * @param {Object} paramObject
     * @property {Number} fileID
     * @property {String} recordType
     * @returns void
     */
    function processRecordFile(paramObject) {
    	var fileObject    = file.load({ id: paramObject.fileID });
    	var currentObject = JSON.parse(fileObject.getContents());
    	var usage         = runtime.getCurrentScript().getRemainingUsage();
    	var recordCount   = (usage / 2);
	
    	for(var r = 0; r < recordCount; r++) {
    		var currentRecord = currentObject.pop();
    		createQueueRecord({ id: currentRecord.id, type: currentRecord.type, event: currentRecord.event });
		
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 300 ) {
    			break;
    		}
    	}
	
    	if(currentObject.length > 0) {
    		var fileID = createJSONFile({ type: paramObject.recordType, content: currentObject });
			scheduleScript({
				scriptID    : runtime.getCurrentScript().id,
				deploymentID: runtime.getCurrentScript().deploymentId,
				params      : { custscript_clgx2_dw_nsetl_fid: paramObject.fileID } 
			});
    	} else {
    		deleteJSONFile(paramObject.fileID);
    		
    		scheduleScript({
				scriptID: 1309,
				deploymentID: "customdeploy_clgx2_ss_dw_queue_etl", 
			});
    	}
    }
    
    
    /**
     * Adds records to the data warehouse queue for processing.
     * 
     * @access private
     * @function createQueueRecords
     * 
     * @param {Object} recordObjects
     * 
     * @returns null
     */
    function createQueueRecords(recordObjects) {
    	if(recordObjects) {
    		var arrayLength = recordObjects.length;
    		for(var i = 0; i < arrayLength; i++) {
    			if(recordExistsInQueue(recordObjects[i].id, recordObjects[i].type, recordObjects[i].event) == false) {
    				var recordObject = record.create({ type: "customrecord_clgx_dw_record_queue" });
        			recordObject.setValue("custrecord_clgx_dw_rq_rid", recordObjects[i].id);
        			recordObject.setValue("custrecord_clgx_dw_rq_uid", "-4");
        			recordObject.setValue("custrecord_clgx_dw_rq_rt", recordObjects[i].type);
        			recordObject.setValue("custrecord_clgx_dw_rq_et", recordObjects[i].event);
        			recordObject.setValue("custrecord_clgx_dw_rq_processed", false);
        			recordObject.setValue("custrecord_clgx_dw_rq_error", "0");
        			recordObject.setValue("custrecord_clgx_dw_rq_hist", false);
        			recordObject.setValue("custrecord_clgx_dw_rq_etl", true);
        			recordObject.save();
    			}
    		}
    	}
    }
    
    
    /**
	 * Checks if the record being saved is currently sitting unprocessed in the queue.
	 * 
	 * @access private
	 * @function recordExistsInQueue
	 * 
	 * @param {number} recordId   - The Internal ID of the current record.
	 * @param {string} recordType - The record type of the current record.
	 * @return {boolean}
	 */
	function recordExistsInQueue(recordId, recordType, eventType) {
		try {
			var s = search.create({ 
				type: "customrecord_clgx_dw_record_queue",
				columns: ["internalid"],
				filters: [["custrecord_clgx_dw_rq_hist", "isnot", "T"], "and",
				["custrecord_clgx_dw_rq_rt", "contains", recordType], "and",
				["custrecord_clgx_dw_rq_rid", "equalto", recordId]]
			});
			var count = 0;
			s.run().each(function(result) { count = count + 1; });
			if(count > 0) {
				log.debug({ title: "recordExistsInQueue", details: "Skipping " + recordType + " (" + recordId + ")" + ". Record exists and has not been processed." });
				return true; 
			}
		} catch(ex) {
			log.error({ title: "recordExistsInQueue Error", details: ex });
		}
		
		return false;
	}
    
    
    /**
     * Creates an array of record ids from the data warehouse database.
     * 
     * @access private
     * @function createDWIDArray
     * 
     * @param {ParamObject} paramObject
     * 
     * @returns {Array}
     */
    function createDWIDArray(paramObject) {
    	if(paramObject.recordType == "contact") {
    		var vendorContact   = https.get({ url: "https://lucee-nnj3.dev.nac.net/dw/sync/clgx_dw_audit.cfm?record=ENT.Vendor_Contact&key=Vendor_Contact_ID" });
    		var partnerContact  = https.get({ url: "https://lucee-nnj3.dev.nac.net/dw/sync/clgx_dw_audit.cfm?record=ENT.Partner_Contact&key=Partner_Contact_ID" });
    		var customerContact = https.get({ url: "https://lucee-nnj3.dev.nac.net/dw/sync/clgx_dw_audit.cfm?record=ENT.Customer_Contact&key=Customer_Contact_ID" });
    		
    		var vendorContactBody   = JSON.parse(vendorContact.body);
    		var partnerContactBody  = JSON.parse(partnerContact.body);
    		var customerContactBody = JSON.parse(customerContact.body);
    		
    		var result = vendorContactBody.concat(partnerContactBody).concat(customerContactBody);
        	return result;
    	} else if(paramObject.recordType == "invoice") {
    		var responseObject = https.get({ url: "https://lucee-nnj3.dev.nac.net/dw/sync/clgx_dw_audit_invoice.cfm?date=%27" + moment().subtract(2, "months").format("MM/DD/YYYY") + "%27"});
        	return JSON.parse(responseObject.body);
    	} else {
    		var responseObject = https.get({ url: "https://lucee-nnj3.dev.nac.net/dw/sync/clgx_dw_audit.cfm?record=" + paramObject.dwTable + "&key=" + paramObject.dwTableKey });
        	return JSON.parse(responseObject.body);
    	}
    }
    
    
    /**
     * Handles initial file creation and rescheduling.
     * 
     * @access private
     * @function createIDFile
     * 
     * @param {ParamObject} paramObject
     * @returns null
     */
    function createNSIDArray(paramObject) {
    	var recordIDArray   = new Array();
    	
    	var processResults  = null;
    	var existingContent = "";
    	var globalIndex     = 0;
    	
    	var filters         = new Array();
    	var columns         = new Array();
    	var ids             = new Array();
    	
    	//filters = buildSearchFilters(paramObject.recordType);
    	//columns.push(search.createColumn("internalid"));
    	
    	var searchObject  = search.load({ id: paramObject.savedSearch }).run();
    	var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
    	
    	processResults = processResultSet(paramObject, searchResults);
    	
    	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
    		recordIDArray = processResults.ids;
    	}
    	
    	while (processResults.nextPage == true) {
    		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
    		processResults = processResultSet(paramObject, searchResults);
    		recordIDArray = recordIDArray.concat(processResults.ids);
    	}
    	
    	return recordIDArray;
    }
    
    
    /**
     * Processes each page of results.
     * 
     * @access private
     * @function processResultSet
     * 
     * @param {ParamObject} paramObject
     * @param {Object} searchResults
     * @returns {Object}
     */
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var ids          = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		ids.push(parseInt(searchResults[r].getValue(searchResults[r].columns[0])));
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }
    
    
    /**
	 * Creates a JSON file.
	 * 
	 * @access private
	 * @function createJSONFile
	 * 
	 * @libraries N/file
	 * 
	 * @param {Object} paramObject
	 * @returns {Number}
	 */
	function createJSONFile(paramObject) {
		var fileObject = file.create({
			name: "clgx-etl-" + paramObject.type + ".json",
			fileType: file.Type.JSON,
			folder: 5453175,
			contents: JSON.stringify(paramObject.content)
		});
		
		return fileObject.save();
	}
	
	
	/**
	 * Deletes a file.
	 * 
	 * @access private
	 * @function deleteJSONFile
	 * 
	 * @libraries N/file
	 * 
	 * @param {Number} id
	 * @returns void
	 */
	function deleteJSONFile(fileID) {
		file.delete({ id: fileID });
	}
	
	
	/**
	 * Schedules a script.
	 * 
	 * @access private
	 * @function scheduleScript
	 * 
	 * @libraries N/task
	 * 
	 * @param {Object} paramObject
	 * @returns void
	 */
	function scheduleScript(paramObject) {
		var scriptTask          = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
		
    	scriptTask.scriptId     = paramObject.scriptID;
    	scriptTask.deploymentId = paramObject.deploymentID;
    	
    	if(paramObject.params !== undefined) {
    		scriptTask.params = paramObject.params;
    	}
    	
    	var scriptTaskId = scriptTask.submit();
	}
    
    
    /**
     * Returns whether or not a record is a transaction type.
     * 
     * @access private
     * @function isTransaction
     * 
     * @param {String} recordType
     * @returns boolean
     */
    function isTransaction(recordType) {
    	if(recordType == "advintercompanyjournalentry"       || recordType == "assemblybuild"             || recordType == "assemblyunbuild" 
    			|| recordType == "binworksheet"              || recordType == "bintransfer"               || recordType == "cashrefund" 
    			|| recordType == "cashsale"                  || recordType == "charge"                    || recordType == "check"
    			|| recordType == "creditmemo"                || recordType == "customerdeposit"           || recordType == "customerpayment"
    			|| recordType == "customerrefund"            || recordType == "deposit"                   || recordType == "depositapplication"
    			|| recordType == "estimate"                  || recordType == "expensereport"             || recordType == "inboundshipment"
    			|| recordType == "intercompanyjournalentry"  || recordType == "intercompanytransferorder" || recordType == "inventoryadjustment"
    			|| recordType == "inventorycostrevaluation"  || recordType == "inventorytransfer"         || recordType == "invoice"
    			|| recordType == "itemdemandplan"            || recordType == "itemfulfillment"           || recordType == "itemreceipt"
    			|| recordType == "itemsupplyplan"            || recordType == "journalentry"              || recordType == "manufacturingoperationtask"
                || recordType == "paycheck"                  || recordType == "paycheckjournal"
    			|| recordType == "purchaseorder"             || recordType == "purchaserequisition"       || recordType == "returnauthorization"
    			|| recordType == "salesorder"                || recordType == "statisticaljournalentry"   || recordType == "transferorder"
    			|| recordType == "vendorbill"                || recordType == "vendorcredit"              || recordType == "vendorpayment"
    			|| recordType == "vendorreturnauthorization" || recordType == "workorder"                 || recordType == "workorderclose"
    			|| recordType == "workordercompletion"       || recordType == "workorderissue") {
    		return true;
    	}
    	
    	return false;
    }
    
    
    /**
     * Builds an array of search filters based on the record type.
     * 
     * @access private
     * @function buildSearchFilters
     * 
     * @param {String} recordType
     * @returns {Object}
     */
    function buildSearchFilters(recordType) {
    	var finalArray = new Array();
    	
    	if(isTransaction(recordType)) {
    		finalArray.push(search.createFilter({ name: "mainline", operator: "IS", value: true }));
    	} else if(recordType == "customer") {
    		finalArray.push(search.createFilter({ name: "entitystatus", operator: "anyOf", values: [13, 16] }));
    	}
    	
    	return finalArray;
    }
    
    
    return {
        execute: execute
    };
    
});
