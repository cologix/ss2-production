/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Equipments.js
 * Script Name	:	CLGX2_SS_DW_Sync_Equipments
 * Script ID	:   customscript_clgx2_ss_dw_sync_equipments
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_equipments
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, moment, dwg) {

	
    function execute(scriptContext) {

    	var records = {
	    		"response": 				'',
	    		"proc": 					'CLGX_DW_SYNC_EQUIPMENTS',
	    		"enviro": 					dwg.get_enviro(),
	    		"source_id": 				1,
	    		"user_id": 					-4,
	    		"datetime": 				moment().format('YYYY-MM-DD H:mm:ss'),
	    		"equipment_function": 		dwg.get_list('customlist_clgx_function_types_list'),
	    		"equipment_name": 			dwg.get_list('customlist_clgx_equipment_name_list'),
	    		"equipment_make": 			dwg.get_list('customlist_clgx_equipment_make_list'),
	    		"equipment_model": 			dwg.get_list('customlist_clgx_equipment_model_list'),
	    		"equipment_port_count": 	dwg.get_list('customlist_clgx_equip_port_count_list'),
	    		"equipment_ip_addr": 		dwg.get_list('customlist_clgx_equipment_ip_addr_lst'),
	    		"equipment_type": 			dwg.get_list('customlist_cologix_equip_type_lst'),
	    		"equipment_rack_unit": 		dwg.get_list('customlist_cologix_space_rack_unit_lst'),
	    		"equipment_status": 		dwg.get_list('customlist_cologix_equip_status_list'),
	    		"equipment_cpu_lst": 		dwg.get_list('customlist_clgx_cpu_list'),
	    		"equipment_disks_lst": 		dwg.get_list('customlist_clgx_disks_list'),
	    		"equipment_memory_lst": 	dwg.get_list('customlist_clgx_memory_list'),
	    		"inventory_status": 		dwg.get_list('customlist_cologix_inventory_statuslst'),
	    		"xc_sequence": 				dwg.get_list('customlist_cologix_xcpath_sequence_lst'),
	    		"managed_service_type": 	dwg.get_list('customlist_clgx_managed_services_type'),
	    		"managed_service_cluster": 	dwg.get_list('customlist_clgx_cluster'),
	    		"managed_service_cloud_vault": dwg.get_list('customlist_clgx_cloud_vault')	
    	};
    	
	    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}

		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

});
