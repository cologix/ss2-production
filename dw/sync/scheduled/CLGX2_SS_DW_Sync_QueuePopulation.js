/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/9/2018
 */
define(["N/task", "N/runtime", "N/search", "N/file", "N/record"],
function(task, runtime, search, file, record) {
    function execute(scriptContext) {
    	var rt = runtime.getCurrentScript();
    	
    	var paramObject              = new Object();
    	paramObject.savedSearchID    = rt.getParameter("custscript_clgx2_dw_qp_ss");
    	paramObject.queueRecordType  = rt.getParameter("custscript_clgx2_dw_qp_qrt");
    	paramObject.queueEventType   = rt.getParameter("custscript_clgx2_dw_qp_qe");
    	paramObject.actionType       = parseInt(rt.getParameter("custscript_clgx2_dw_qp_at"));
    	paramObject.fileID           = parseInt(rt.getParameter("custscript_clgx2_dw_qp_fid"));
    	paramObject.initialIndex     = parseInt(rt.getParameter("custscript_clgx2_dw_qp_idx"));
    	paramObject.currentIndex     = 0;
    	paramObject.pageSize         = parseInt(rt.getParameter("custscript_clgx2_dw_qp_ps"));
    	paramObject.recordInfo       = getRecordInfo(paramObject.queueRecordType);

    	
    	log.debug({ title: "paramObject", details: paramObject });
    	
    	if(paramObject.actionType == 1) {
    		log.debug({ title: "Delete", details: "Delete" });
    		deleteQueueRecords(paramObject);
    	} else if(paramObject.actionType == 2) {
    		createQueueRecords(paramObject);
    		log.debug({ title: "Create", details: "Create" });
    	} else if(paramObject.actionType == 3) {
    		log.debug({ title: "Reset", details: "Reset" });
    		resetQueueRecords(paramObject);
    	} else if(paramObject.actionType == 4) {
    		createIDFile(paramObject);
    		log.debug({ title: "Create File", details: "Create File" });
    	}
    }
    
    
    /**
     * Handles initial file creation and rescheduling.
     */
    function createIDFile(paramObject) {
    	var recordIDArray   = new Array();
    	
    	var processResults  = null;
    	var existingContent = "";
    	var globalIndex     = 0;
    	
    	var filters         = new Array();
    	var columns         = new Array();
    	var ids             = new Array();
    	
    	columns.push(search.createColumn("internalid"));
    	
    	//var searchObject  = search.load({ type: paramObject.queueRecordType, id: paramObject.savedSearchID, columns: columns }).run();
    	var searchObject  = search.load({ id: paramObject.savedSearchID, columns: columns }).run();
    	var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
    	
    	processResults = processResultSet(paramObject, searchResults);
    	
    	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
    		recordIDArray = processResults.ids;
    	} else {
    		var existingFile = file.load({ id: paramObject.fileID });
    		existingContent  = JSON.parse(existingFile.getContents());
    		recordIDArray    = recordIDArray.concat(existingContent).concat(processResults.ids);
    	}
    	
    	while (processResults.nextPage == true) {
    		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
    		processResults = processResultSet(paramObject, searchResults);
    		recordIDArray = recordIDArray.concat(processResults.ids);
    	}
    	
    	log.debug({ title: "createIDFile - recordIDArray", details: recordIDArray.length });
    	log.debug({ title: "createIDFile - processResults", details: processResults });
    	
    	var fileObject = file.create({ 
			name: paramObject.queueRecordType + "_ids.json", 
			fileType: file.Type.PLAINTEXT,
			folder: 5453175,
			contents: JSON.stringify(recordIDArray)
		});
    	
    	var fileID = fileObject.save();
    	
    	log.debug({ title: "createIDFile - fileID", details: fileID });
    	
    	if(processResults.reschedule == true) {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = 1490;
    		t.deploymentId = "customdeploy_clgx2_ss_dw_sync_queuepop";
    		t.params       = {
    			custscript_clgx2_dw_qp_ss : paramObject.savedSearchID,
    			custscript_clgx2_dw_qp_rt : paramObject.queueRecordType,
    			custscript_clgx2_dw_qp_qe : paramObject.queueEventType,
    			custscript_clgx2_dw_qp_at : paramObject.actionType,
    			custscript_clgx2_dw_qp_fid: paramObject.fileID,
    			custscript_clgx2_dw_qp_idx: paramObject.currentIndex
    		};
    		
    		t.submit();
    	}
    }
    
    
    /**
     * Processes each page of results.
     */
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var ids          = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		ids.push(searchResults[r].getValue(searchResults[r].columns[0]));
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }
    
    
    /**
     * Loads a JSON file from the file cabinet and adds the records to the data warehouse record queue.
     */
    function createQueueRecords(paramObject) {
    	try {
    		var usage    = runtime.getCurrentScript().getRemainingUsage();
    		
    		var idFile   = file.load({ id: paramObject.fileID });
    		var ids      = JSON.parse(idFile.getContents());
    		var idLength = ids.length;
    		
    		paramObject.currentIndex = paramObject.initialIndex;
    		
    		for(var i = parseInt(paramObject.initialIndex); i < idLength; i++) {
    			var q = record.create({ type: "customrecord_clgx_dw_record_queue" });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: parseInt(ids[i]) });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: "-4" });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: paramObject.queueRecordType });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: getEventNameFromTypeID(paramObject.queueEventType) });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: false });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_failed",    value: 0 });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_hist",      value: true });
        		q.setValue({ fieldId: "custrecord_clgx_dw_rq_etl",       value: false });
        		q.save({ ignoreMandatoryFields: false, enableSourcing: false });
        		
        		paramObject.currentIndex = paramObject.currentIndex + 1;
        		
        		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
        			break;
        		}
    		}
    		
    		if(usage <= 200) {
    			var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
        		t.scriptId     = 1490;
        		t.deploymentId = "customdeploy_clgx2_ss_dw_sync_queuepop";
        		t.params       = {
        			custscript_clgx2_dw_qp_ss : paramObject.savedSearchID,
        			custscript_clgx2_dw_qp_rt : paramObject.queueRecordType,
        			custscript_clgx2_dw_qp_qe : paramObject.queueEventType,
        			custscript_clgx2_dw_qp_at : paramObject.actionType,
        			custscript_clgx2_dw_qp_fid: paramObject.fileID,
        			custscript_clgx2_dw_qp_idx: paramObject.currentIndex
        		};
        		
        		t.submit();
    		}
    	} catch(ex) {
    		log.debug({ title: "createQueueRecords - Error", details: ex });
    	}
    }
    
    
    /**
     * Loads a saved search and deletes the specified records from the data warehouse record queue.
     */
    function deleteQueueRecords(paramObject) {
    	try {
    		var usage        = runtime.getCurrentScript().getRemainingUsage();
    		
    		var searchObject = search.load({ type: paramObject.queueRecordType, id: paramObject.savedSearchID });
    		searchObject.run().each(function(result) {    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
        			return false;
        		}

    			record.delete({ type: paramObject.queueRecordType, id: result.getValue(result.columns[0]) });
    			
    			return true;
    		});
    		
    		if(usage <= 200) {
    			var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
        		t.scriptId     = 1490;
        		t.deploymentId = "customdeploy_clgx2_ss_dw_sync_queuepop";
        		t.params       = {
        			custscript_clgx2_dw_qp_ss : paramObject.savedSearchID,
        			custscript_clgx2_dw_qp_rt : paramObject.queueRecordType,
        			custscript_clgx2_dw_qp_qe : paramObject.queueEventType,
        			custscript_clgx2_dw_qp_at : paramObject.actionType,
        			custscript_clgx2_dw_qp_fid: paramObject.fileID,
        			custscript_clgx2_dw_qp_idx: paramObject.currentIndex
        		};
        		
        		t.submit();
    		}
    	} catch(ex) {
    		log.debug({ title: "deleteQueueRecords - Error", details: ex });
    	}
    }
    
    
    /**
     * Loads a saved search and resets the specified records from the data warehouse record queue.
     */
    function resetQueueRecords(paramObject) {
    	try {
    		var searchObject = search.load({ type: paramObject.queueRecordType, id: paramObject.savedSearchID });
    		searchObject.run().each(function(result) {
    			log.debug({ title: "Reset Record", details: "Reset record: " + result.getValue("internalid") });
    			//record.submitRecord({});
    			return true;
    		});
    	} catch(ex) {
    		log.debug({ title: "resetQueueRecords - Error", details: ex });
    	}
    }
    
    
    /**
     * Returns information about the specified record to be added to the queue.
     * 
     * @param {String} recordType - The type of record we want to get information for.
     * @returns {Object}
     */
    function getRecordInfo(recordType) {
    	var finalObject = {};
    	
    	if(recordType === "vendor") {
    		finalObject = { 
    			type: "vendor", 
    			recordSearch: "vendor", 
    			fileID: 9203946
    		};
    	}
    	
    	if(recordType === "partner") {
    		finalObject = { 
        		type: "partner", 
        		recordSearch: "partner", 
        		fileID: 9204446
        	};
    	}
    	
    	if(recordType === "customer") {
    		finalObject = { 
            	type: "customer", 
            	recordSearch: "customer", 
            	fileID: 9206650
            };
    	}
    	
    	if(recordType === "contact") {
    		finalObject = { 
            	type: "contact", 
            	recordSearch: "contact", 
            	fileID: 7770612
            };
    	}
    	
    	if(recordType === "portal_user") {
    		finalObject = { 
            	type: "customrecord_clgx_modifier", 
            	recordSearch: "customrecord_clgx_modifier", 
            	fileID: 7849139
            };
    	}
    	
    	if(recordType === "service") {
    		finalObject = { 
            	type: "job", 
            	recordSearch: "job", 
            	fileID: 7715562
            };
    	}
    	
    	if(recordType === "project") {
    		finalObject = { 
            	type: "job", 
            	recordSearch: "job", 
            	fileID: 9527127
            };
    	}
    	
    	if(recordType === "opportunity") {
    		finalObject = { 
            	type: "opportunity", 
            	recordSearch: "transaction", 
            	fileID: 9240919
            };
    	}
    	
    	if(recordType === "proposal") {
    		finalObject = { 
            	type: "estimate", 
            	recordSearch: "transaction", 
            	fileID: 7711011
            };
    	}
    	
    	if(recordType === "salesorder") {
    		finalObject = { 
            	type: "salesorder", 
            	recordSearch: "transaction", 
            	fileID: 7714862
            };
    	}
    	
    	if(recordType === "invoice") {
    		finalObject = { 
            	type: "invoice", 
            	recordSearch: "transaction", 
            	fileID: 7912280
            };
    	}
    	
    	if(recordType === "cinvoice") {
    		finalObject = { 
            	type: "customrecord_clgx_consolidated_invoices", 
            	recordSearch: "customrecord_clgx_consolidated_invoices", 
            	fileID: 7782894
            };
    	}
    	
    	if(recordType === "journalentry") {
    		finalObject = { 
            	type: "journalentry", 
            	recordSearch: "transaction", 
            	fileID: 7803804
            };
    	}
    	
    	if(recordType === "payment") {
    		finalObject = { 
            	type: "customerpayment", 
            	recordSearch: "transaction", 
            	fileID: 7945953
            };
    	}
    	
    	if(recordType === "purchaseorder") {
    		finalObject = { 
            	type: "purchaseorder", 
            	recordSearch: "transaction", 
            	fileID: 7954926
            };
    	}
    	
    	if(recordType === "creditmemo") {
    		finalObject = { 
            	type: "creditmemo", 
            	recordSearch: "transaction", 
            	fileID: 7820401
            };
    	}
    	
    	if(recordType === "vendorbill") {
    		finalObject = { 
            	type: "vendorbill", 
            	recordSearch: "transaction", 
            	fileID: 9111883
            };
    	}
    	
    	if(recordType === "vendorcredit") {
    		finalObject = { 
            	type: "vendorcredit", 
            	recordSearch: "transaction", 
            	fileID: 9115495
            };
    	}
    	
    	if(recordType === "vendorpayment") {
    		finalObject = { 
            	type: "vendorpayment", 
            	recordSearch: "transaction", 
            	fileID: 9115796
            };
    	}
    	
    	if(recordType === "forecast") {
    		finalObject = { 
            	type: "customrecord_clgx_cap_bud_forecast", 
            	recordSearch: "customrecord_clgx_cap_bud_forecast", 
            	fileID: 9342701
            };
    	}
    	
    	if(recordType === "device") {
    		finalObject = { 
            	type: "customrecord_clgx_dcim_devices", 
            	recordSearch: "customrecord_clgx_dcim_devices", 
            	fileID: 8806401
            };
    	}
    	
    	if(recordType === "fam") {
    		finalObject = { 
            	type: "customrecord_ncfar_asset", 
            	recordSearch: "customrecord_ncfar_asset", 
            	fileID: 8810804
            };
    	}
    	
    	if(recordType === "space") {
    		finalObject = { 
            	type: "customrecord_cologix_space", 
            	recordSearch: "customrecord_cologix_space", 
            	fileID: 8412663
            };
    	}
    	
    	if(recordType === "power") {
    		finalObject = { 
            	type: "customrecord_clgx_power_circuit", 
            	recordSearch: "customrecord_clgx_power_circuit", 
            	fileID: 8816388
            };
    	}
    	
    	if(recordType === "xc") {
    		finalObject = { 
            	type: "customrecord_cologix_crossconnect", 
            	recordSearch: "customrecord_cologix_crossconnect", 
            	fileID: 8842641
            };
    	}
    	
    	if(recordType === "vxc") {
    		finalObject = { 
            	type: "customrecord_cologix_vxc", 
            	recordSearch: "customrecord_cologix_vxc", 
            	fileID: 8845042
            };
    	}
    	
    	if(recordType === "equipment") {
    		finalObject = { 
            	type: "customrecord_cologix_equipment", 
            	recordSearch: "customrecord_cologix_equipment", 
            	fileID: 8421472
            };
    	}
    	
    	if(recordType === "case") {
    		finalObject = { 
            	type: "supportcase", 
            	recordSearch: "supportcase", 
            	fileID: 9814281
            };
    	}
    	
    	if(recordType === "churn") {
    		finalObject = {
    			type: "churn",
    			recordSearch: "customrecord_cologix_churn",
    			fileID: 9969333
    		};
    	}
    	
    	return finalObject;
    }
    
    
    function getEventNameFromTypeID(typeID) {
    	if(typeID == 1) {
    		return "create";
    	} else if(typeID == 2) {
    		return "delete";
    	}
    }
    
    return {
        execute: execute
    };
    
});
