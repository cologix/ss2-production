/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   2/8/2018
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/libraries/moment.min", "N/file", "N/https", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"],
function(record, search, moment, file, https, dwg) {	
	/**
	 * Returns an array of CRM profiles.
	 * @returns {array}
	 */
	function get_crm_profiles() {
		var profiles     = new Array();
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_crm_profile" });
		
		searchObject.run().each(function(result) {
			profiles.push({ "profile_id": result.getValue(result.columns[0]), "profile": result.getValue(result.columns[1]) });
			return true;
		});
		
		return profiles;
	}
	
	
	/**
	 * Returns an array of CRM origins.
	 * @returns {array}
	 */
	function get_crm_origin() {
		var origins      = new Array()
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_crm_origin" });
		
		searchObject.run().each(function(result) {
			origins.push({ "origin_id": result.getValue(result.columns[0]), "origin": result.getValue(result.columns[1]) });
			return true;
		});
		
		return origins;
	}
	
	
	/**
	 * Returns an array of CRM categories.
	 * @returns {array}
	 */
	function get_crm_category() {
		var category     = new Array()
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_crm_category" });
		
		searchObject.run().each(function(result) {
			category.push({ "category_id": result.getValue(result.columns[0]), "category": result.getValue(result.columns[1]) });
			return true;
		});
		
		return category;
	}
	
	
	/**
	 * Returns an array of CRM sub-categories.
	 * @returns {array}
	 */
	function get_crm_subcategory() {
		var category     = new Array()
		var searchObject = search.load({ type: "customrecord_cologix_sub_case_type", id: "customsearch_clgx_dw_crm_subcategory" });
		
		searchObject.run().each(function(result) {
			category.push({ "sub_category_id": result.getValue(result.columns[0]), "sub_category": result.getValue(result.columns[1]) });
			return true;
		});
		
		return category;
	}
	
	/**
	 * Returns an array of CRM stages.
	 * @returns {array}
	 */
	function get_crm_stage() {
		var stage = new Array();
		
		stage.push({ "stage_id": "CLOSED",    "stage": "Closed"    });
		stage.push({ "stage_id": "ESCALATED", "stage": "Escalated" });
		stage.push({ "stage_id": "OPEN",      "stage": "Open"      });
		
		return stage;
	}
	
	
	/**
	 * Returns an array of CRM statuses.
	 * @returns {array}
	 */
	function get_crm_status() {
		var status = new Array();
		
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_crm_status" });
		
		searchObject.run().each(function(result) {
			status.push({ "status_id": result.getValue(result.columns[0]), "status": result.getValue(result.columns[1]) });
			return true;
		});
		
		return status;
	}
	
	
	/**
	 * Returns an array of CRM priorities.
	 * @returns {array}
	 */
	function get_crm_priority() {
		var priority = new Array();
		
		var searchObject = search.load({ type: "supportcase", id: "customsearch_clgx_dw_crm_priority" });
		
		searchObject.run().each(function(result) {
			priority.push({ "priority_id": result.getValue(result.columns[0]), "priority": result.getValue(result.columns[1]) });
			return true;
		});
		
		return priority;
	}
	
	
    function execute() {
    	var profiles     = get_crm_profiles();
    	var origins      = get_crm_origin();
    	var categories   = get_crm_category();
    	var subcategory  = get_crm_subcategory();
    	var stages       = get_crm_stage();
    	var status       = get_crm_status();
    	var priorities   = get_crm_priority();
    	
    	var records = {
    			"response"         : "",
    			"proc"             : "CLGX_DW_SYNC_CRM",
    			"enviro"           : dwg.get_enviro(),
    			"source_id"        : 1,
    			"user_id"          : -4,
    			"datetime"         : moment().format("YYYY-MM-DD H:mm:ss"),
    			"crm_profiles"     : profiles,
    			"crm_origins"      : origins,
    			"crm_categories"   : categories,
    			"crm_subcategories": subcategory,
    			"crm_stages"       : stages,
    			"crm_status"       : status,
    			"crm_priorities"   : priorities
    	};
    	
    	try {
	    	var requestURL = https.post({
	    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
	    	    body: JSON.stringify(records)
	    	});
    		records.response = JSON.parse(requestURL.body);
    	}
    	catch (error) {
    		log.error({ title: "Post Error", details: error });
    	}

    	var fileObj = file.create({
    		name: records.proc + ".json",
    		fileType: file.Type.PLAINTEXT,
    		contents: JSON.stringify(records),
    		description: records.proc,
    		encoding: file.Encoding.UTF8,
    		folder: 5453175,
    		isOnline: false
    	});
    	
    	var fileId = fileObj.save();
    	
    	log.debug({ title: "fileId", details: fileId });
    }
    
    return {
        execute: execute
    };
    
});
