/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   2/7/2018
 */
define(["N/record", "N/search", "N/runtime", "N/task"],
function(record, search, runtime, task) {
	/**
	 * Loads a search and adds all entity records to the DW sync queue from the last 3 days.
	 * 
	 * @returns null
	 */
	function processEntities() {
		
		var scriptObj = runtime.getCurrentScript();
		var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
		var page = scriptObj.getParameter({name: "custscript_clgx2_dw_changed_case_page"});
		
		log.debug({ title: "Sync Start - ", details: "====================================================" });
		log.debug({ title: "CLGX2_SS_DW_Sync_Changed_Cases", details: " | Page : " + page + "  |" });
		
		var range = 1000;
		var start = page * range;
		var end   = start + range;
		
		var mySearch = search.load({ type: "supportcase", id: "customsearch_clgx_dw_changed_cases" });
		var searchResult = mySearch.run().getRange({start: start, end: end});
	    var nbr = searchResult.length;
	    for (var i = 0; i < nbr; i++) {
			var result     = searchResult[i];
			var columns    = result.columns;
			var recordID   = parseInt(result.getValue(columns[0]));
			if(recordType != null && recordType != "") {
				var queue = record.create({ type: "customrecord_clgx_dw_record_queue"             });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: recordID      });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: -4            });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: "supportcase" });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: "create"      });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: false         });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_error", 	 value: 0             });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_hist", 	 value: false         });
				queue.save();
				
			}
			
			usage = 10000 - parseInt(scriptObj.getRemainingUsage());
    		log.debug({ title: "CLGX2_SS_DW_Sync_Changed_Cases", details: " | Page : " + page + " | start : " + start + " | end : " + end + " | Index : " + i + " / " + nbr + " | Record Type: " + recordType + " | Record ID: " + recordID + " | Usage : " + usage + "  |" });
		}
		if(nbr == range){ // there might be other records left on the next page, so reschedule script for the next page
	    		page = page + 1;
    			//log.debug({ title: "CLGX2_SS_DW_Sync_Changed_Entities", details: ' | Page : ' + page + '  |' });
    			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
		    	scriptTask.scriptId = 1450;
		    	scriptTask.params = {custscript_clgx2_dw_changed_case_page : page};
		    	scriptTask.deploymentId = "customdeploy_clgx2_ss_dw_sync_chng_case";
		    	var scriptTaskId = scriptTask.submit();
	    }
	}
	
	
    function execute(scriptContext) {
    	try {
    		processEntities();
    	} catch(ex) {
    		log.error({ title: "CLGX2_SS_DW_Sync_Changed_Cases - execute", details: ex });
    	}
    }

    return {
        execute: execute
    };
    
});
