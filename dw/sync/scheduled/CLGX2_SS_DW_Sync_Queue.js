/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Queue.js
 * Script Name	:	CLGX2_SS_DW_Sync_Queue
 * Script ID	:   customscript_clgx2_ss_dw_sync_queue
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_queue
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */


define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/underscore-min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", 
		
		"/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Vendor", 
		"/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Partner", 
		"/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Customer", 
		"/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Contact", 
		"/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Service",
		
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Opportunity", 
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Proposal", 
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_SalesOrder", 
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Invoice", 
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_JournalEntry",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_PurchaseOrder",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Payment",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_CreditMemo",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_VendorBill",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_VendorPayment",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_VendorCredit",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Forecast",
		
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Equipment",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Space",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Power",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_FAM",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Device",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_XC",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_VXC",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_OutOfBand",
		
		"/SuiteScripts/clgx/dw/sync/lib/crm/CLGX2_LIB_DW_Case",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Churn",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_Device_Capacity",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_ManagedService",
		"/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_CustomerKWPeak",
		"/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_CustomerRefund"
	],
function(file, task, https, record, runtime, search, _, moment, dwg, dwvendor, dwpartner, dwcustomer, dwcontact, dwservice, dwoppty, dwproposal, dwso, dwinvoice, dwjournal, dwpurchase, dwpayment, dwcredit, dwvendbill, dwvendpay, dwvendcredit, dwforecast, dwequipment, dwspace, dwpower, dwfam, dwdevice, dwxc, dwvxc, dwob, dwsc, dwch, dwdc, dwms, dwcp, dwcr) {
	
	function execute(scriptContext) {
		
		if(runtime.envType === runtime.EnvType.PRODUCTION) {
			
			log.debug({ title: "Sync Start - ", details: "====================================================" });
			
			try{
				
				var enviro = dwg.get_enviro();
				
				var scriptObj = runtime.getCurrentScript();
				var deployment = scriptObj.deploymentId;
				var ss = '';
				
				// CLGX_DW_Queue_Current
				if(deployment == 'customdeploy_clgx2_ss_dw_queue_current'){ // deployment and saved search for current records in queue
					ss = 'customsearch_clgx_dw_queue_current';
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 50; // number of record to be processed per script run
				}
				
				// CLGX_DW_Queue_Current_Failed
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_failed'){ // deployment and saved search for current failed records in queue
					ss = 'customsearch_clgx_dw_queue_current_fail';
					var archive = 0; // delete or keep queue record
					var repeat = 0; // continue, reschedule script
					var range = 100; // number of record to be processed per script run
				}
				
				// CLGX_DW_Queue_Current_Invoices
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_invoices'){ // deployment and saved search for current invoice records
					ss = 'customsearch_clgx_dw_queue_current_inv';
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 50; // number of record to be processed per script run
				}
				
				// CLGX2_SS_DW_Queue_Historical_1
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_hist_1'){ // deployment and saved search for historical records in queue
					ss = 'customsearch_clgx_dw_queue_hist_1';
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 200; // number of record to be processed per script run
				}
				
				// CLGX2_SS_DW_Queue_Historical_2
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_hist_2'){ // deployment and saved search for historical records in queue
					ss = 'customsearch_clgx_dw_queue_hist_2';
					var archive = 1; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 100; // number of record to be processed per script run
				}
				
				// CLGX2_SS_DW_Queue_Backlog_1
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_bl_1'){ // deployment and saved search for historical records in queue
					//ss = 'customsearch_clgx_dw_queue_bl_1';
					ss = "customsearch_clgx_dw_queue_current_fail";
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 140; // number of record to be processed per script run
				}
				
				// CLGX2_SS_DW_Queue_Backlog_2
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_bl_2'){ // deployment and saved search for historical records in queue
					ss = 'customsearch_clgx_dw_queue_bl_2';
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 70; // number of record to be processed per script run
				}
				
				// CLGX2_SS_DW_Queue_ETL
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_etl'){ // deployment and saved search for historical records in queue
					ss = 'customsearch_clgx_dw_queue_etl';
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 10; // number of record to be processed per script run
				}
				
				// CLGX2_SS_DW_Queue_Power
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_powers'){ // deployment and saved search for historical records in queue
					ss = 'customsearch_clgx_dw_queue_powers';
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 200; // number of record to be processed per script run
				}
				
				// CLGX2_SS_DW_Queue_Device
				else if(deployment == 'customdeploy_clgx2_ss_dw_queue_device'){ // deployment and saved search for historical records in queue
					ss = 'customsearch_clgx_dw_queue_devices';
					var archive = 0; // delete or keep queue record
					var repeat = 1; // continue, reschedule script
					var range = 100; // number of record to be processed per script run
				}
				
				//CLGX2_SS_DW_Queue_Mass_Update
				else if(deployment == 'customdeploy_clgx2_ss_dw_mass_update'){
					ss = 'customsearch_clgx_dwmu_processing_queue';
					var archive = 0; // delete or keep queue record
					var repeat  = 1; // continue, reschedule script
					var range   = 100; // number of record to be processed per script run
				}
	
				var mySearch = search.load({ id: ss });
				var searchResult = mySearch.run().getRange({start: 0,end: range});
			    var nbr = searchResult.length;
			    if(nbr > range){ // if search result > range, limit to range 
			    		nbr = range;
			    }
			    
			    var usage = runtime.getCurrentScript().getRemainingUsage();
			    
				for (var i = 0; i < nbr; i++) {
					if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 5000) {
						break;
					}
				
			    	var result = searchResult[i];
			    	
			    	var queue_id = parseInt(result.getValue(result.columns[0]));
			    	var record_id = parseInt(result.getValue(result.columns[2]));
			    		
			    	var rec_type = result.getValue(result.columns[1]);
			    		
					if(rec_type == 'lead' || rec_type == 'prospect'){
						rec_type = 'customer';
					}
					if(rec_type == 'customrecord_clgx_modifier'){
						rec_type = 'portal_user';
					}
					if(rec_type == 'estimate'){
						rec_type = 'proposal';
					}
					if(rec_type == 'job'){
						rec_type = 'service';
						try{
							var rec = record.load({ type: 'JOB', id: record_id });
							var isproj = rec.getValue({ fieldId: "custentity_cologix_int_prj" });
							if(isproj == true || isproj == 'T' || isproj == 'Yes'){
								rec_type = 'project';
							}
						}
						catch (error) {
							
						}
					}
					if(rec_type == 'customrecord_clgx_consolidated_invoices'){
						rec_type = 'cinvoice';
					}
	
					if(rec_type == 'customrecord_cologix_equipment'){
						rec_type = 'equipment';
					}
					if(rec_type == "customrecord_clgx_active_port") {
						rec_type = "port";
					}
					if(rec_type == "customrecord_clgx_cpu") {
						rec_type = "cpu";
					}
					if(rec_type == "customrecord_clgx_disks") {
						rec_type = "disk";
					}
					if(rec_type == "customrecord_clgx_memory") {
						rec_type = "memory";
					}
					if(rec_type == 'customrecord_cologix_space'){
						rec_type = 'space';
					}
					if(rec_type == 'customrecord_clgx_dcim_devices'){
						rec_type = 'device';
					}
					if(rec_type == 'customrecord_clgx_dcim_devices_capacity') {
						rec_type = 'device_capacity';
					}
					if(rec_type == 'customrecord_ncfar_asset'){
						rec_type = 'fam';
					}
					if(rec_type == 'customrecord_clgx_power_circuit'){
						rec_type = 'power';
					}
					if(rec_type == 'customrecord_cologix_crossconnect'){
						rec_type = 'xc';
					}
					if(rec_type == "customrecord_cologix_xcpath") {
						rec_type = "xc_path";
					}
					if(rec_type == 'customrecord_cologix_vxc'){
						rec_type = 'vxc';
					}
					if(rec_type == 'customrecord_clgx_cap_bud_forecast'){
						rec_type = 'forecast';
					}
					if(rec_type == 'customrecord_clgx_oob_facility') {
						rec_type = 'outofband';
					}
					if(rec_type == "customrecord_cologix_churn") {
						rec_type = "churn";
					}
					if(rec_type == "customrecord_clgx_dcim_customer_peak") {
						rec_type = "customer_peak";
					}
					if(rec_type == 'customrecord_clgx_managed_services') {
						rec_type = 'managed_service';
					}
					if(rec_type == 'supportcase') {
						rec_type = 'case';
					}
					
					var event = result.getValue(result.columns[3]);
			    	var user_id = parseInt(result.getValue(result.columns[4]));
			    	var date = dwg.sqlDate(result.getValue(result.columns[5]),true);
			    	var json = null;
			    	
			    //	if(rec_type != "supportcase") {
			    		json = get_record(rec_type, record_id, event, user_id, date, enviro, deployment);
			    //	}
			    		
			    	var processed = 'T';
			    	var err = 0;
			    		
			    	if(json != null && dwg.is_empty(json.record[0]) && event != 'delete'){
			    	//if(dwg.is_empty(json.record[0])){
				    	err = -4;
				    	dwg.set_queue_flags(queue_id, processed, err);
				    	log.debug({ title: "Sync" + rec_type, details: ' | queue_id - '+ record_id + ' | record does not exist  |' });
			    	} 
			    	else {
			    		//log.debug({ title: "Sync", details: JSON.stringify(json) });
			    		
			    		try{
			    			
		    				requestURL = https.post({
			    				url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
			    				body: JSON.stringify(json)
			    			});
		    				
		    				json.response = JSON.parse(requestURL.body);
					    	err = parseInt(json.response.ERROR);
					    	
			    			/*
			    			var requestURL = null;
			    			if(rec_type == "supportcase") {
			    				//log.debug({ title: "", details: rec_type });
			    				requestURL = https.get({ url: "https://system.na2.netsuite.com/app/site/hosting/scriptlet.nl?script=1344&deploy=1&event=create&type=case&id=" + record_id });
			    				json.response = JSON.parse(requestURL.body);
						    	err = parseInt(json.response.ERROR);
						    	
			    			} else {
			    				requestURL = https.post({
				    				url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
				    				body: JSON.stringify(json)
				    			});
			    				
			    				json.response = JSON.parse(requestURL.body);
						    	err = parseInt(json.response.ERROR);
			    			}	    			
					    	*/
			    			
					    	//log.debug({ title: "Sync", details: json.response });
					    	
					    	if(err != 0 || archive == 1){
						    	try{
						    		dwg.set_queue_flags(queue_id, processed, err);
						    	}
						    	catch (error) {
						    		err = -3;
						    		log.debug({ title: "Sync" + rec_type, details: ' | queue_id - '+ record_id + ' | queue can not be updated  |' });
						    	}
					    	} else {
					    		if(ss == "customsearch_clgx_dwmu_processing_queue") {
					    			var objRecord = record.delete({
						    		    type: 'customrecord_clgx_dw_mu_queue', 
						    		    id: queue_id,
						    		});
					    		} else {
					    			var objRecord = record.delete({
						    		    type: 'customrecord_clgx_dw_record_queue', 
						    		    id: queue_id,
						    		});
					    		}
					    	}
				   	}
				   	catch (error) {
				   		//log.debug({ title: "Sync Error", details: error });
				   		log.debug({ title: "Sync " + rec_type, details: ' | record_id - '+ record_id + ' | post error  |' });
				   		processed = 'F';
				   		err = -2;
				   	}
				   	
				   	
					   	
			    	}
			    		
			    	var scriptObj = runtime.getCurrentScript();
			    	var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
			        var index = i + 1;
			        log.debug({ title: "Queue Sync ", details: ' | index : ' + index + ' of ' + nbr + ' | rec_type : '+ rec_type + ' | record_id : '+ record_id + ' | event : '+ event + ' | processed : '+ processed + ' | err : '+ err + ' | usage : '+ usage + '  |' });
			    }						
			    
			    // if there are still records to process, re-schedule script
			    if(nbr && repeat){
				    	var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
				    	scriptTask.scriptId = 1309;
				    	scriptTask.deploymentId = deployment;
				    	scriptTask.params = {searchId: 'customscript_clgx2_ss_dw_sync_queue'};
				    	var scriptTaskId = scriptTask.submit();
			    }
		    
		   	}
		   	catch (error) {
		   		var err = error.name;
		   		//log.debug({ title: "ERROR", details: err });
		        if (err == 'SSS_USAGE_LIMIT_EXCEEDED') {
		        		log.debug({ title: "ERROR", details: 'Re-schedule script because of ' + err });
				    	var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
				    	scriptTask.scriptId = 1309;
				    	scriptTask.deploymentId = deployment;
				    	scriptTask.params = {searchId: 'customscript_clgx2_ss_dw_sync_queue'};
				    	var scriptTaskId = scriptTask.submit();
		        } else {
		            throw error;
		        }
		   	}
		}
    }

    return {
        execute: execute
    };

    function get_record(rec_type, record_id, event, user_id, date, enviro, deployment) {
    	var procedureName = rec_type;
    	
    	var json = {
    		"response" : "",
    		"proc"     : "CLGX_DW_SYNC_" + procedureName.toUpperCase(),
    		"enviro"   : enviro,
    		"user_id"  : user_id,
    		"source_id": 1,
    		"datetime" : moment().format('YYYY-MM-DD H:mm:ss'),
    		"event"    : event,
    		"rec_type" : rec_type,
    		"record_id": record_id,
    		"record"   : []
    	};
    	
    	//log.debug({ title: "json", details: json });
		
		try {
		
			// entities
			if(rec_type == 'vendor'){
				json.record = dwvendor.get_vendor(record_id);
			}
			if(rec_type == 'partner'){
				json.record = dwpartner.get_partner(record_id);
			}
			if(rec_type == 'customer'){
				json.record = dwcustomer.get_customer(record_id);
			}
			if(rec_type == 'contact'){
				json.record = dwcontact.get_contact(record_id);
			}
			if(rec_type == 'portal_user'){
				json.record = dwcontact.get_portal_user(record_id);
			}
			if(rec_type == 'service' || rec_type == 'project'){
				if(rec_type == 'service'){
					procedureName = "service";
					json.record = dwservice.get_service(record_id);
				} else {
					procedureName = "project";
					json.record = dwservice.get_project(record_id);
				}
			}
			
			// transactions
			if(rec_type == 'opportunity'){
				json.record = dwoppty.get_opportunity(record_id);
			}
			if(rec_type == 'proposal'){
				json.record = dwproposal.get_proposal(record_id);
			}
			if(rec_type == 'salesorder'){
				json.record = dwso.get_salesorder(record_id);
			}
			if(rec_type == 'invoice'){
				json.record = dwinvoice.get_invoice(record_id);
			}
			if(rec_type == 'cinvoice'){
				json.record = dwinvoice.get_cinvoice(record_id);
			}
			if(rec_type == 'journalentry'){
				json.record = dwjournal.get_journalentry(record_id);
			}
			if(rec_type == 'purchaseorder'){
				json.record = dwpurchase.get_purchaseorder(record_id);
			}
			if(rec_type == 'customerpayment'){
				json.record = dwpayment.get_payment(record_id);
			}
			if(rec_type == 'creditmemo'){
				json.record = dwcredit.get_creditmemo(record_id);
			}
			if(rec_type == 'vendorbill'){
				json.record = dwvendbill.get_vendorbill(record_id);
			}
			if(rec_type == 'vendorpayment'){
				json.record = dwvendpay.get_vendorpayment(record_id);
			}
			if(rec_type == 'vendorcredit'){
				json.record = dwvendcredit.get_vendorcredit(record_id);
			}
			if(rec_type == 'forecast'){
				json.record = dwforecast.get_forecast(record_id);
			}
			
			// inventory
			if(rec_type == 'space'){
				json.record = dwspace.get_space(record_id);
			}
			if(rec_type == 'equipment'){
				json.record = dwequipment.get_equipment(record_id);
			}
			if(rec_type == "port") {
				//Sends the parent equipment object.
				procedureName = "equipment";
				if(event == "delete") {
					json.record = dwequipment.get_equipment(record_id);
				} else {
					var equipmentID = dwequipment.get_port(record_id)[0].equipment_id; 
					json.record     = dwequipment.get_equipment(equipmentID);
				}
			}
			if(rec_type == "cpu") {
				procedureName = "equipment";
				json.record = dwequipment.get_cpus(record_id);
			}
			if(rec_type == "disk") {
				procedureName = "equipment";
				json.record = dwequipment.get_disks(record_id);
			}
			if(rec_type == "memory") {
				procedureName = "equipment";
				json.record = dwequipment.get_memory(record_id);
			}
			if(rec_type == 'device'){
				json.record = dwdevice.get_device(record_id);
			}
			if(rec_type == 'device_capacity') {
				json.record = dwdc.get_device_capacity(record_id);
			}
			if(rec_type == 'fam'){
				json.record = dwfam.get_fam(record_id);
			}			
			if(rec_type == 'power'){
				json.record = dwpower.get_power(record_id);
			}			
			if(rec_type == 'xc'){
				json.record = dwxc.get_xc(record_id);
			}
			if(rec_type == "xc_path") {
				json.record = dwxc.get_xc_path(record_id);
			}
			if(rec_type == 'vxc'){
				json.record = dwvxc.get_vxc(record_id);
			}
			if(rec_type == 'outofband') {
				json.record = dwob.get_oob(record_id);
			}
			if(rec_type == "case") {
				json.record = dwsc.get_case(record_id);
			}
			if(rec_type == "churn") {
				json.record = dwch.get_churn(record_id);
			}
			if(rec_type == "customer_peak") {
				json.record = dwcp.get_kw_peak(record_id);
			}
			if(rec_type == "managed_service") {
				json.record = dwms.get_managed_service(record_id);
			}
			if(rec_type == "customerrefund") {
				json.record = dwcr.get_customerrefund(record_id);
			}
			
			json.proc = "CLGX_DW_SYNC_" + procedureName.toUpperCase();
		} catch(ex) {
			log.debug({ title: "get_record - Error", details: ex });
		}
		
		return json;
	    }

});
