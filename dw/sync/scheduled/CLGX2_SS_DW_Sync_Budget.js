/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author		: Alex Guzenski - alex.guzenski@cologix.com
 * @date		: 1/26/2018
 * Script File	: CLGX2_SS_DW_Sync_Budget.js
 * Script Name	: CLGX2_SS_DW_Sync_Budget
 * Script ID	: customscript_clgx2_ss_dw_sync_budget
 * Deployment ID: customdeploy_clgx2_ss_dw_sync_budget
 */
define(["N/runtime",
	    "N/file", 
	    "N/https",
	    "N/record", 
	    "N/search", 
	    "/SuiteScripts/clgx/libraries/moment.min", 
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global",
	    "/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_Budget"],
function(runtime, file, https, record, search, moment, dwg, dwb) {
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	
    	var budgets = dwb.get_budgets();
    	
    	var records = {
    		"response" : "",
    		"proc"     : "CLGX_DW_SYNC_BUDGETS",
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"datetime" : moment().format('YYYY-MM-DD H:mm:ss'),
    		"records"  : budgets
        };
    	
    	try {
    		var requestURL = https.post({
	    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
	    	    body: JSON.stringify(records)
	    	});
    		
    		records.response = JSON.parse(requestURL.body);
    	} catch(ex) {
    		records.response = 'Post error';
    	}
    	
    	var fileObj = file.create({
			name: records.proc + "_" + runtime.getCurrentScript().getParameter("custscript_clgx2_dw_budget_budgetyear") + ".json",
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
    }

    return {
        execute: execute
    };
    
});
