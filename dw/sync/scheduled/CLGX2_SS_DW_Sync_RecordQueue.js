/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/task", "N/runtime"],
function(task, runtime) {
    function execute(scriptContext) {
    	
    }
    
    return {
        execute: execute
    };
    
});
