/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Accounting.js
 * Script Name	:	CLGX2_SS_DW_Sync_Accounting
 * Script ID	:   customscript_clgx2_ss_dw_sync_accounting
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_accounting
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/https",
		"N/record", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
		
function(file, https, record, search, moment, dwg) {

	function execute(scriptContext) {
		
		var records = {
			"response": 				'',
			"proc": 					'CLGX_DW_SYNC_ACCOUNTING',
			"enviro": 					dwg.get_enviro(),
			"source_id": 				1,
			"user_id": 					-4,
			"datetime": 				moment().format('YYYY-MM-DD H:mm:ss'),
			"item_standard": 			dwg.get_list('customlist_clgx_list_standard'),
			"item_category": 			get_item_category(),
    		"volts": 					dwg.get_list('customlist_cologix_power_volts_lst'),
    		"amps": 					dwg.get_list('customlist_cologix_power_amps_lst'),
    		"item_class": 				get_classification(),
    		"currency": 				get_currency(),
    		"billing_schedule": 		get_billing_schedule(),
    		"accounting_period": 		get_accounting_period()
    	};
		
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}
		
		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
    }

    return {
        execute: execute
    };

	function get_classification() {
		var classification = [];
		var results = search.load({ type: 'classification', id: "customsearch_clgx_dw_item_class" });
		results.run().each(function(result) {
			var class_id = parseInt(result.getValue(result.columns[0]));
			var rec = record.load({ type: "classification", id: class_id});
			var parent_id  = parseInt(rec.getValue({ fieldId: "parent" }));
			var dep_subs  = rec.getValue({ fieldId: "subsidiary" });
			classification.push({
				"class_id":			class_id,
				"class":			result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"parent_id":		parent_id,
				"subsidiary_id":	parseInt(dep_subs[0])
			});
	        
	        return true;
		});
		return classification;
	}

	function get_currency() {
		var currency = [];
		var results = search.create({
			type: "currency",
			filters: [],
			columns: [
				{"name":"internalid"},
				{"name":"name"},
				{"name":"isinactive"},
				{"name":"symbol"},
				{"name":"exchangerate"}
			]
		});
		results.run().each(function(result) {
			currency.push({
				"currency_id":		parseInt(result.getValue({name: "internalid"})),
				"currency":			result.getValue({name: "name"}),
				"isinactive":		dwg.get_bit(result.getValue({name: "isinactive"})),
				"symbol":			result.getValue({name: "symbol"}),
				"exchangerate":		parseFloat(result.getValue({name: "exchangerate"}))
			});
            return true;
		});
		return currency;
	}

	function get_billing_schedule() {
		var billing_schedule = [];
		var results = search.load({ type: 'billingschedule', id: "customsearch_clgx_dw_billing_schedule" });
		results.run().each(function(result) {
			billing_schedule.push({
				"billing_schedule_id":	parseInt(result.getValue(result.columns[0])),
				"billing_schedule":		result.getValue(result.columns[1]),
				"inactive":				dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":			result.getValue(result.columns[3]),
				"initial_amount":		parseInt(result.getValue(result.columns[4])),
				"frequency":			result.getValue(result.columns[5]),
				"repeat_every":			parseInt(result.getValue(result.columns[6])),
				"bs_count":				parseInt(result.getValue(result.columns[7]))
			});
	        return true;
		});
		return billing_schedule;
	}

	function get_accounting_period() {
		var accounting_period = [];
		var results = search.load({ type: 'accountingperiod', id: "customsearch_clgx_dw_accounting_period" });
		results.run().each(function(result) {
			accounting_period.push({
				"period_id":		parseInt(result.getValue(result.columns[0])),
				"period":			result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"parent_id":		parseInt(result.getValue(result.columns[3])),
				"period_year":		result.getValue(result.columns[4]),
				"period_quarter":	result.getValue(result.columns[5]),
				"period_start":		dwg.sqlDate(result.getValue(result.columns[6])),
				"period_end":		dwg.sqlDate(result.getValue(result.columns[7])),
				
				"y_id"         : parseInt(moment(result.getValue(result.columns[6])).format('YYYYY')),
				"yy"           : moment(result.getValue(result.columns[6])).format('YY'),
				"yyyy"         : moment(result.getValue(result.columns[6])).format('YYYY'),
				
				"m_id"         : moment(result.getValue(result.columns[6])).format('MM'),
				"mm"           : moment(result.getValue(result.columns[6])).format('MMM'),
				"mmmm"         : moment(result.getValue(result.columns[6])).format('MMM')
				
			});
	        return true;
		});
		return accounting_period;
	}
	
	function get_item_category (){
		var list = dwg.get_list('customlist_cologix_item_category_list');
		var item_category = [];
		for ( var i = 0; list != null && i < list.length; i++ ) {
			item_category.push({
				"id": 		list[i].id,
				"name": 	list[i].name,
				"name_fr": 	get_item_category_fr (list[i].id, list[i].name)
			});
		}
		return item_category;
	}
	
	function get_item_category_fr (id, name){

	    var fr = '';
	    switch(id) {
	        case 1:
	        	fr = 'Location d\'équipement';
	            break;
	        case 2:
	        	fr = 'Vente d\'équipement';
	            break;
	        case 3:
	        	fr = 'Service d\'installation';
	            break;
	        case 4:
	        	fr = 'Interconnexion';
	            break;
	        case 5:
	        	fr = 'Réseau';
	            break;
	        case 6:
	        	fr = 'Autre non Récurent';
	            break;
	        case 7:
	        	fr = 'Autre Récurent';
	            break;
	        case 8:
	        	fr = 'Électricité';
	            break;
	        case 9:
	        	fr = 'Support Technique';
	            break;
	        case 10:
	        	fr = 'Espace';
	            break;
	        case 11:
	        	fr = 'Interconnexion virtuelle';
	            break;
	        case 12:
	        	fr = 'Reprise après Sinistre';
	            break;
	        case 13:
	        	fr = 'Services Gérés';
	            break;
	        default:
	        	fr = name;
	    }
	    return fr;
	}


});
