/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/11/2018
 */
define(["N/record", "N/search", "N/runtime", "N/task"],
function(record, search, runtime, task) {
	/**
	 * Checks if the record being saved is currently sitting unprocessed in the queue.
	 * 
	 * @param {number} recordId   - The Internal ID of the current record.
	 * @param {string} recordType - The record type of the current record.
	 * @return boolean
	 */
	function recordExistsInQueue(recordId, recordType, eventType) {
		var s = search.create({ 
			type: "customrecord_clgx_dw_record_queue",
			columns: ["internalid"],
			filters: [["custrecord_clgx_dw_rq_processed", "is", "F"], "and",
			["custrecord_clgx_dw_rq_rt", "contains", recordType], "and",
			["custrecord_clgx_dw_rq_et", "contains", eventType], "and",
			["custrecord_clgx_dw_rq_rid", "equalto", recordId]]
		});
		var count = 0;
		s.run().each(function(result) { count = count + 1; });
		if(count > 0) { return true; }
		
		return false;
	}
	
	
	/**
	 * Returns the native record id string.
	 * @param {string} type
	 * @returns {string}
	 */
	function returnNativeRecordType(type) {
		if(type != null) {
			var nativeType = "";
			
			if(type == "Contact") {
				nativeType = record.Type.CONTACT;
			} else if(type == "Customer") {
				nativeType = record.Type.CUSTOMER;
			} else if(type == "Vendor") {
				nativeType = record.Type.VENDOR;
			} else if(type == "Partner") {
				nativeType = record.Type.PARTNER;
			} else if(type == "Service") {
				nativeType = "service";
			} else {
				nativeType = null;
			}

			return nativeType;
		}
	}
	
	
	/**
	 * Loads a search and adds all entity records to the DW sync queue from the last 3 days.
	 * 
	 * @returns null
	 */
	function processEntities() {
		
		var scriptObj = runtime.getCurrentScript();
		var usage = 10000 - parseInt(scriptObj.getRemainingUsage());
		var page = scriptObj.getParameter({name: 'custscript_clgx2_dw_changed_entity_page'});
		
		log.debug({ title: "Sync Start - ", details: "====================================================" });
		log.debug({ title: "CLGX2_SS_DW_Sync_Changed_Entities", details: ' | Page : ' + page + '  |' });
		
		var range = 1000;
		var start = page * range;
		var end   = start + range;
		
		var mySearch = search.load({ type: "entity", id: "customsearch_clgx_dw_changed_entities" });
		var searchResult = mySearch.run().getRange({start: start, end: end});
	    var nbr = searchResult.length;
	    for (var i = 0; i < nbr; i++) {
			var result     = searchResult[i];
			var columns    = result.columns;
			var recordID   = parseInt(result.getValue(columns[0]));
			var recordType = returnNativeRecordType(result.getValue(columns[1]));
			if(recordType != null && recordType != "") {
				
				var queue = record.create({ type: "customrecord_clgx_dw_record_queue"             });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: recordID      });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: -4            });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: recordType    });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: "create"      });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: false         });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_error", 	 value: 0             });
				queue.setValue({ fieldId: "custrecord_clgx_dw_rq_hist", 	 value: false         });
				queue.save();
				
			} else{

			}
			usage = 10000 - parseInt(scriptObj.getRemainingUsage());
    		log.debug({ title: "CLGX2_SS_DW_Sync_Changed_Entities", details: ' | Page : ' + page + ' | start : ' + start + ' | end : ' + end + ' | Index : ' + i + ' / ' + nbr + " | Record Type: " + recordType + " | Record ID: " + recordID + ' | Usage : '+ usage + '  |' });
		}
		if(nbr == range){ // there might be other records left on the next page, so reschedule script for the next page
	    		page = page + 1;
    			//log.debug({ title: "CLGX2_SS_DW_Sync_Changed_Entities", details: ' | Page : ' + page + '  |' });
    			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
		    	scriptTask.scriptId = 1450;
		    	scriptTask.params = {custscript_clgx2_dw_changed_entity_page : page};
		    	scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_chng_ent';
		    	var scriptTaskId = scriptTask.submit();
	    }
	}
	
	
    function execute(scriptContext) {
    	try {
    		processEntities();
    	} catch(ex) {
    		log.error({ title: "CLGX2_SS_DW_Sync_Missed - Entities - execute", details: ex });
    	}
    }

    return {
        execute: execute
    };
    
});
