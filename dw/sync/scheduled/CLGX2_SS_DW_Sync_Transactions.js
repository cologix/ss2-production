/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Transactions.js
 * Script Name	:	CLGX2_SS_DW_Sync_Transactions
 * Script ID	:   customscript_clgx2_ss_dw_sync_transact
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_transact
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/https",
		"N/record", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
		
function(file, https, record, search, moment, dwg) {

	function execute(scriptContext) {
    	
    	var opportunity_status = [];
    	var proposal_status = [];
    	var sales_order_status = [];
    	var invoice_status = [];
    	var credit_memo_status = [];
    	var journal_status = [];
    	var purchase_order_status = [];
    	var payment_status = [];
    	var vendorbill_status = [];
    	var vendorpayment_status = [];
    	
    	var status = get_status();
    	
    	for (var i = 0; i < status.length; i++ ) {
    		
    		switch(status[i].type_id) {

	    	    case 'Opprtnty':
	    	    	opportunity_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	    	    
	    	    case 'Estimate':
	    	    	proposal_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	    	        
	    	    case 'SalesOrd':
	    	    	sales_order_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	    	    
	    	    case 'CustInvc':
	    	    	invoice_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	    	    
	    	    case 'CustCred':
	    	    	credit_memo_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	        	    
	    	    case 'Journal':
	    	    	journal_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	    	    
	    	    case 'PurchOrd':
	    	    	purchase_order_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	    	    
	    	    case 'CustPymt':
	    	    	payment_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
		    	    
	    	    case 'VendBill':
	    	    	vendorbill_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
		    	    
	    	    case 'VendPymt':
	    	    	vendorpayment_status.push({
	    				"status_id":		status[i].status_id,
	    				"status":			status[i].status
	    			});
	    	        break;
	    	        
	    	    default:
	    	        //
    		}
    	}
		
		
		var contract_terms = get_contract_terms();
    		var records = {
    	    		"response": 						'',
    				"proc": 						'CLGX_DW_SYNC_TRANSACTIONS',
    				"enviro": 					dwg.get_enviro(),
    				"source_id": 				1,
    				"user_id": 					-4,
    				"datetime": 					moment().format('YYYY-MM-DD H:mm:ss'),
    				"sale_type": 				dwg.get_list('customlist_cologix_sale_type'),
        			"forecast_type": 			get_forecast_type(),
        			"lead_source": 				get_lead_source(),
        			"loss_reason": 				dwg.get_list('winlossreason'),
        			"power_usage_type": 			dwg.get_list('customlist_clgx_pwr_usage_type'),
        			"accelerator_type": 			dwg.get_list('customlist_clgx_aa_type_list'),
        			"cinvoice_output": 			dwg.get_list('customlist_ouput'),
        			"credit_memo_reason": 		dwg.get_list('customlist_clgx_credit_memo_reason'),
        			"opportunity_status": 		opportunity_status,
        			"proposal_status": 			proposal_status,
        			"sales_order_status": 		sales_order_status,
        			"invoice_status": 			invoice_status,
        			"credit_memo_status": 		credit_memo_status,
        			"journal_status": 			journal_status,
        			"purchase_order_status": 	purchase_order_status,
        			"payment_status": 			payment_status,
        			"vendorbill_status": 		vendorbill_status,
        			"vendorpayment_status": 		vendorpayment_status,
        			//"approval_status": 			get_approval_status(),
        			"contract_terms": 			contract_terms.terms,
        			"language_id": 				contract_terms.language
    		};
	    	
    		try {
    		    	var requestURL = https.post({
    		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
    		    	    body: JSON.stringify(records)
    		    	});
    	    		records.response = JSON.parse(requestURL.body);
    		}
    		catch (error) {
    			records.response = 'Post error';
    		}
		
		
		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

	function get_status() {
		var status = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx_dw_transaction_status" });
		results.run().each(function(result) {
			status.push({
				"type_id":		result.getValue(result.columns[0]),
				"type":			result.getText(result.columns[0]),
				"status_id":		result.getValue(result.columns[1]),
				"status":		result.getText(result.columns[2])
			});
	        return true;
		});
		return status;
	}
	
	function get_forecast_type() {
		var forecast_type = [];
		var results = search.create({
			type: "opportunity",
			filters: [],
			columns: [{"name":"forecasttype", summary: "GROUP"}]
		});
		results.run().each(function(result) {
			forecast_type.push({
				"forecast_type_id":		parseInt(result.getValue({name: "forecasttype", summary: "GROUP"})),
				"forecast_type":			result.getText({name: "forecasttype", summary: "GROUP"})
			});
            return true;
		});
		return forecast_type;
	}
	
	function get_approval_status() {
		var incoterms = [];
		var results = search.create({
			type: "vendorbill",
			filters: [],
			columns: [{"name":"approvalstatus", summary: "GROUP"}]
		});
		results.run().each(function(result) {
			incoterms.push({
				"approvalstatus_id":		parseInt(result.getValue({name: "approvalstatus", summary: "GROUP"})),
				"approvalstatus":			result.getText({name: "approvalstatus", summary: "GROUP"})
			});
            return true;
		});
		return incoterms;
	}
	
	function get_lead_source() {
		var lead_source = [];
		var results = search.load({ type: 'campaign', id: "customsearch_clgx_dw_lead_source" });
		results.run().each(function(result) {
			lead_source.push({
				"lead_source_id":		parseInt(result.getValue(result.columns[0])),
				"lead_source":			result.getValue(result.columns[1]),
				"inactive":				dwg.get_bit(result.getValue(result.columns[2]))
			});
	        return true;
		});
		return lead_source;
	}
	
	function get_contract_terms() {
		var terms = [];
		var language_ids = [];
		var language = [];
		var results = search.load({ type: 'customrecord_clgx_contract_terms', id: "customsearch_clgx_dw_so_contract_terms" });
		results.run().each(function(result) {
			
			var lang_id = parseInt(result.getValue(result.columns[3]));
			var lang_name = result.getText(result.columns[3]);
			if(language_ids.indexOf(lang_id) == -1){
				language_ids.push(lang_id);
				language.push({
					"language_id":	lang_id,
					"language":		lang_name
				});
			}
			terms.push({
				"contract_terms_id":	parseInt(result.getValue(result.columns[0])),
				"contract_terms":		result.getValue(result.columns[1]),
				"inactive":				dwg.get_bit(result.getValue(result.columns[2])),
				"language_id":			lang_id,
				"language":				lang_name,
				"terms":				result.getValue(result.columns[4]).replace(/'/g, "’")
			});
	        return true;
		});
		return {
			"terms":	terms,
			"language":	language
		}
		contract_terms;
	}


});
