/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 9/6/2018
 */
define(["/SuiteScripts/clgx/dw/sync/lib/transaction/CLGX2_LIB_DW_ExchangeRate", 
	    "/SuiteScripts/clgx/libraries/moment.min",
	    "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global",
	    "N/file"],
function(dwer, moment, dwg, file) {
    function execute(scriptContext) {
    	var rates = dwer.get_exchange_rate()
    	
    	var records = {
    		"response" : "",
    		"proc"     : "CLGX_DW_SYNC_EXCHANGE_RATE",
    		"enviro"   : dwg.get_enviro(),
    		"source_id": 1,
    		"user_id"  : -4,
    		"datetime" : moment().format('YYYY-MM-DD H:mm:ss'),
    		"records"  : rates
        };
    	
    	try {
    		var requestURL = https.post({
	    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
	    	    body: JSON.stringify(records)
	    	});
    		
    		records.response = JSON.parse(requestURL.body);
    	} catch(ex) {
    		records.response = 'Post error';
    	}
    	
    	var fileObj = file.create({
			name: records.proc + ".json",
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
    	
		var fileId = fileObj.save();
    }

    
    return {
        execute: execute
    };
    
});
