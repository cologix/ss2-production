/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   3/1/2018
 */
define([
	"N/file",
	"N/https",
	"/SuiteScripts/clgx/libraries/underscore-min",
	"/SuiteScripts/clgx/libraries/moment.min",
	"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
],
function(file, https, _, moment, dwg) {
    function execute(scriptContext) {
    	var records = {
	    	"response"    : "",
	    	"proc"        : "CLGX_DW_SYNC_CHURN",
	    	"enviro"      : dwg.get_enviro(),
	    	"source_id"   : 1,
	    	"user_id"     : -4,
	    	"datetime"    : moment().format("YYYY-MM-DD H:mm:ss"),
	    	"churn_reason": dwg.get_list("customlist_clgx_ch_reason"), 
	    	"churn_status": dwg.get_list("customlist_clgx_chlist_status"),
	    	"churn_type"  : dwg.get_list("customlist_clgx_churn_type_list")
	    };
    	
		try {
		    var requestURL = https.post({
		        url: "https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm",
		        body: JSON.stringify(records)
		    });
		    
		    records.response = JSON.parse(requestURL.body);
		} catch (error) {
			records.response = "Post error";
		}
		
		var fileObj = file.create({
			name: records.proc + ".json",
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		
		var fileId = fileObj.save();
    }

    return {
        execute: execute
    };
    
});
