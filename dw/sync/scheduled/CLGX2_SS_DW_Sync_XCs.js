/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_XCs.js
 * Script Name	:	CLGX2_SS_DW_Sync_XCs
 * Script ID	:   customscript_clgx2_ss_dw_sync_xcs
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_xcs
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, moment, dwg) {

	
    function execute(scriptContext) {

    	var records = {
	    		"response": 				'',
	    		"proc": 					'CLGX_DW_SYNC_XCS',
	    		"enviro": 					dwg.get_enviro(),
	    		"source_id": 				1,
	    		"user_id": 					-4,
	    		"datetime": 				moment().format('YYYY-MM-DD H:mm:ss'),
	    		"xc_type": 					dwg.get_list('customlist_cologix_xconnect_type_lst'),
	    		"xc_circuit_type": 			dwg.get_list('customlist_cologix_xconnect_crctyp_lst'),
	    		"xc_vlan": 					dwg.get_list('customlist_clgx_vlan_list'),
	    		"xc_speed": 				dwg.get_list('customlist_clgx_active_xc_speed'),
	    		"xc_type_xc": 				dwg.get_list('customlist_clgx_xc_type'),
	    		"xc_vrrp_group": 			dwg.get_list('customlist_clgx_vrrp_group'),
	    		"xc_subnet_mask": 			dwg.get_list('customlist_clgx_subnet_mask_list'),
	    		"xcpath_sequence": 			dwg.get_list('customlist_cologix_xcpath_sequence_lst'),
	    		"xcpath_panel": 			dwg.get_list('customlist_cologix_xconnect_panelname'),
	    		"xcpath_slot": 				dwg.get_list('customlist_cologix_xconnect_slotgroup'),
	    		"xcpath_connector": 		dwg.get_list('customlist_cologix_xconnect_cnctrtype'),
	    		"xcpath_utilisation": 		dwg.get_list('customlist_cologix_xcpath_utilstat_lst'),
	    		"vxc_cloud_provider": 		dwg.get_list('customlist_clgx_cloud_provider')
    	};
	    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}

		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

});
