/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Spaces.js
 * Script Name	:	CLGX2_SS_DW_Sync_Spaces
 * Script ID	:   customscript_clgx2_ss_dw_sync_spaces
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_spaces
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, moment, dwg) {

	
    function execute(scriptContext) {

    	var records = {
	    		"response": 				'',
	    		"proc": 					'CLGX_DW_SYNC_SPACES',
	    		"enviro": 					dwg.get_enviro(),
	    		"source_id": 				1,
	    		"user_id": 					-4,
	    		"datetime": 				moment().format('YYYY-MM-DD H:mm:ss'),
	    		"space_floor": 				dwg.get_list('customlist_clgx_space_floor'),
	    		"space_room": 				dwg.get_list('customlist_cologix_space_room_lst'),
	    		"space_cage": 				dwg.get_list('customlist_cologix_space_cage_lst'),
	    		"space_aisle": 				dwg.get_list('customlist_cologix_space_aislerow_lst'),
	    		"space_rack": 				dwg.get_list('customlist_clgx_space_rack'),
	    		"space_rack_unit": 			dwg.get_list('customlist_cologix_space_rack_unit_lst'),
	    		"space_type": 				dwg.get_list('customlist_cologix_space_type_lst'),
	    		"space_height": 			dwg.get_list('customlist_clgx_bay_rack_height_space'),
	    		"cabinet_size":             dwg.get_list('customlist_clgx_space_cab_size')
    	};
    	
	    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}

		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

});
