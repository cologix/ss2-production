/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @module Cologix/Data Warehouse/Scheduled/CLGX2_SS_DW_Sync_Merged
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   1/31/2018
 */
define(["N/record", "N/search", "N/runtime"],
function(record, search, runtime) {
	/**
	 * @typedef {Object} QueueObject
	 * @property {number} internalid - The internal id of the NetSuite record.
	 * @property {string} type - The NetSuite record type.
	 */
	
	/**
	 * Returns the internal NetSuite record type.
	 * 
	 * @access private
	 * @function getRecordType
	 * 
	 * @param {string} searchRecordType
	 * @returns {(string | null)}
	 */
	function getRecordType(searchRecordType) {
		if(searchRecordType !== null && searchRecordType !== "") {
			var finalType = "";
			
			if(searchRecordType === "Contact") {
				finalType = record.Type.CONTACT;
			} else if(searchRecordType === "CustJob") {
				finalType = record.Type.CUSTOMER;
			} else if(searchRecordType === "Partner") {
				finalType = record.Type.PARTNER;
			}
			
			return finalType;
		}
	}
	
	
	/**
	 * Creates a Data Warehouse queue record.
	 * 
	 * @param {QueueObject} paramObject
	 * @returns {null}
	 */
	function createQueueRecord(paramObject) {
		if(typeof paramObject !== undefined && paramObject != null) {
			var queue = record.create({ type: "customrecord_clgx_dw_record_queue" });
			queue.setValue({ name: "custrecord_clgx_dw_rq_rid",       value: parseInt(paramObject.internalid) });
			queue.setValue({ name: "custrecord_clgx_dw_rq_uid",       value: "-4" });
			queue.setValue({ name: "custrecord_clgx_dw_rq_rt",        value: paramObject.type });
			queue.setValue({ name: "custrecord_clgx_dw_rq_et",        value: "delete" });
			queue.setValue({ name: "custrecord_clgx_dw_rq_processed", value: false });
			queue.setValue({ name: "custrecord_clgx_dw_rq_error",     value: "0" });
			queue.setValue({ name: "custrecord_clgx_dw_rq_hist",      value: false });
			queue.save();
		}
	}
	
	
	function execute(context) {
		var scriptRuntime   = runtime.getCurrentScript();
		var savedSearch     = scriptRuntime.getParameter("custscript_clgx2_dw_sync_merged_ss");
		var savedSearchType = scriptRuntime.getParameter("custscript_clgx2_dw_sync_merged_sst");
		
		if((savedSearch != null && savedSearch != "") || (savedSearchType != null && savedSearchType != "")) {
			var tmpObject    = new Object();
			var searchObject = search.load({ type: savedSearchType, id: savedSearch });
			searchObject.run().each(function(result) {
				if(savedSearchType === "supportcase") {
					tmpObject.internalid = result.getValue(result.columns[1]).match(/([0-9][0-9])([^\s]+)/)[0]; //Grabs the internal id of the merged record.
					tmpObject.type       = "supportcase";
				} else {
					tmpObject.internalid = result.getValue(result.columns[2]).match(/([0-9][0-9])([^\s]+)/)[0]; //Grabs the internal id of the merged record.
					tmpObject.type       = getRecordType(result.getValue(result.columns[1]));
				}
				
				createQueueRecord(tmpObject);
				
				return true;
			});
		} else {
			throw error.create({ name: "CLGX_ERROR", message: "Missing required parameter(s): Saved Search/Saved Search Type" });
		}
	}
	
    return {
        execute: execute
    };
    
});
