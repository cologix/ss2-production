/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @module Cologix/Data Warehouse/Scheduled/CLGX2_SS_DW_ETL_Merged
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   7/9/2018
 */
define(["N/record", "N/search", "N/runtime", "/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global", "N/file", "N/task", "/SuiteScripts/clgx/libraries/lodash.min"],
function(record, search, runtime, dwg, file, task, _) {
	/**
	 * @typedef {Object} QueueObject
	 * @property {number} internalid - The internal id of the NetSuite record.
	 * @property {string} type - The NetSuite record type.
	 */
	function execute(context) {
		try {
			var scriptRuntime   = runtime.getCurrentScript();
			var savedSearch     = scriptRuntime.getParameter("custscript_clgx2_dw_etl_merged_ss");
			var savedSearchType = scriptRuntime.getParameter("custscript_clgx2_dw_etl_merged_sst");
			var fileID          = scriptRuntime.getParameter("custscript_clgx2_dw_etl_merged_fid");
			
			var recordObj = buildRecordList({			
				recordType: savedSearchType,
				searchID: savedSearch,
				lowerCase: true
			});
			
			if(recordObj.length > 0) {
				if(savedSearchType == "supportcase") {
					processCaseRecords(savedSearchType, recordObj);
				} else {
					if(!fileID) {
						var fileID = buildRecordFile(savedSearchType, recordObj);
						scheduleScript({
							scriptID    : runtime.getCurrentScript().id,
							deploymentID: runtime.getCurrentScript().deploymentId,
							params      : { custscript_clgx2_dw_etl_merged_fid: fileID } 
						});
					} else {
						processRecordFile({ fileID: fileID, recordType: savedSearchType });
					}
				}
			}
		} catch(ex) {
			log.error({ title: "Error", details: ex });
		}
	}
	
	
	/**
	 * Processes support case records.
	 * 
	 * @access private
	 * @function processCaseRecords
	 * 
	 * @param {String} recordType
	 * @param {Object} recordObject
	 * @returns void
	 */
	function processCaseRecords(recordType, recordObject) {
		var mergedObj   = buildMergedRecords(recordType, recordObject);
		var recordCount = mergedObj.length
		
		for(var r = 0; r < recordCount; r++) {
			var currentRecord = mergedObj.pop();
			createQueueRecord({ id: currentRecord.id, type: currentRecord.type, event: currentRecord.event });
			
			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 300 ) {
				break;
			}
		}
	}
	
	
	/**
	 * Creates a file of records based off the current parent record.
	 * 
	 * @access private
	 * @function buildRecordFile
	 * 
	 * @param {String} recordType
	 * @param {Object} recordObject
	 * @returns {Object}
	 */
	function buildRecordFile(recordType, recordObject) {
		var adds    = new Array();
		var deletes = new Array();
		
		var mergedObj = buildMergedRecords(recordType, recordObject);
		
		var transactions = processTransactionRecords(mergedObj.adds);
		var cases        = processCRMRecords(mergedObj.adds);
		var cinvoices    = processCInvoiceRecords(mergedObj.adds);
		var contacts     = processContactRecords(mergedObj.adds);
		var churn        = processChurnRecords(mergedObj.adds);
		var peak         = processKWPeakRecords(mergedObj.adds);
		
		var finalObject = new Array();

		for(var a = 0; a < mergedObj.adds.length; a++) {
			var tCount = transactions[mergedObj.adds[a].id].length;
			for(var ta = 0; ta < tCount; ta++) {
				finalObject.push(transactions[mergedObj.adds[a].id][ta]);
			}
			
			var cCount = cases[mergedObj.adds[a].id].length;
			for(var ta = 0; ta < cCount; ta++) {
				finalObject.push(cases[mergedObj.adds[a].id][ta]);
			}
			
			var ciCount = cinvoices[mergedObj.adds[a].id].length;
			for(var ta = 0; ta < ciCount; ta++) {
				finalObject.push(cinvoices[mergedObj.adds[a].id][ta]);
			}
			
			var coCount = contacts[mergedObj.adds[a].id].length;
			for(var ta = 0; ta < coCount; ta++) {
				finalObject.push(contacts[mergedObj.adds[a].id][ta]);
			}
			
			var chCount = churn[mergedObj.adds[a].id].length;
			for(var ch = 0; ch < chCount; ch++) {
				finalObject.push(churn[mergedObj.adds[a].id][ch]);
			}
			
			var pCount  = peak[mergedObj.adds[a].id].length;
			for(var p = 0; p < pCount; p++) {
				finalObject.push(peak[mergedObj.adds[a].id][p]);
			}
		}
		
		return createJSONFile({ type: recordType, content: finalObject });
	}
	
	
	/**
	 * Process the JSON file created.
	 * 
	 * @access private
	 * @function processRecordFile
	 * 
	 * @libraries N/file, N/record, N/task
	 * 
	 * @param {Number} fileID
	 * @returns void
	 */
	function processRecordFile(paramObject) {
		var fileObject    = file.load({ id: paramObject.fileID });
		var currentObject = JSON.parse(fileObject.getContents());
		var usage         = runtime.getCurrentScript().getRemainingUsage();
		var recordCount   = (usage / 2);
		
		for(var r = 0; r < recordCount; r++) {
			var currentRecord = currentObject.pop();
			createQueueRecord({ id: currentRecord.id, type: currentRecord.type, event: currentRecord.event });
			
			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 300 ) {
				break;
			}
		}
		
		if(currentObject.length > 0) {
			var fileID = createJSONFile({ type: paramObject.recordType, content: currentObject });
			scheduleScript({
				scriptID    : runtime.getCurrentScript().id,
				deploymentID: runtime.getCurrentScript().deploymentId,
				params      : { custscript_clgx2_dw_etl_merged_fid: fileID } 
			});
		} else {
			scheduleScript({
				scriptID: 1309,
				deploymentID: "customdeploy_clgx2_ss_dw_queue_etl", 
			});
		}
	}
	
	
	/**
	 * Creates a JSON file.
	 * 
	 * @access private
	 * @function createJSONFile
	 * 
	 * @libraries N/file
	 * 
	 * @param {Object} paramObject
	 * @returns {Number}
	 */
	function createJSONFile(paramObject) {
		var fileObject = file.create({
			name: "clgx-etl-merged-" + paramObject.type + ".json",
			fileType: file.Type.JSON,
			folder: 5453175,
			contents: JSON.stringify(paramObject.content)
		});
		
		return fileObject.save();
	}
	
	
	/**
	 * Schedules a script.
	 * 
	 * @access private
	 * @function scheduleScript
	 * 
	 * @libraries N/task
	 * 
	 * @param {Object} paramObject
	 * @returns void
	 */
	function scheduleScript(paramObject) {
		var scriptTask          = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
		
    	scriptTask.scriptId     = paramObject.scriptID;
    	scriptTask.deploymentId = paramObject.deploymentID;
    	
    	if(paramObject.params !== undefined) {
    		scriptTask.params = paramObject.params;
    	}
    	
    	var scriptTaskId = scriptTask.submit();
	}
	
	
	/**
	 * Creates a Data Warehouse queue record.
	 * 
	 * @access private
	 * @function createQueueRecord
	 * 
	 * @libraries N/record
	 * 
	 * @param {QueueObject} paramObject
	 * @returns {null}
	 */
	function createQueueRecord(paramObject) {
		if(typeof paramObject !== undefined || paramObject != null) {
			var queue = record.create({ type: "customrecord_clgx_dw_record_queue" });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rid",       value: parseInt(paramObject.id) });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_uid",       value: "-4" });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_rt",        value: paramObject.type });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_et",        value: paramObject.event });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_processed", value: false });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_error",     value: "0" });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_hist",      value: false });
			queue.setValue({ fieldId: "custrecord_clgx_dw_rq_etl",       value: true });
			queue.save();
		}
	}
	
	
	/**
	 * Returns two arrays for adding and deleting the parent records.
	 * 
	 * @access private
	 * @function buildMergedRecords
	 * 
	 * @param {String} recordType
	 * @param {Object} recordObject
	 * 
	 * @returns {Object}
	 */
	function buildMergedRecords(recordType, recordObject) {
		var recordLength = recordObject.length;
		var adds         = new Array();
		var deletes      = new Array();
		
		for(var r = 0; r < recordLength; r++) {
			adds.push({    id: parseInt(recordObject[r].newid), type: recordType, event: "create" });
			deletes.push({ id: parseID(recordObject[r].note),   type: recordType, event: "delete" });
		}
		
		return _.uniqWith(adds, _.isEqual).concat(_.uniqWith(deletes, _.isEqual));
	}
	
	
	/**
	 * Returns an object of transactions based on the customer.
	 * 
	 * @access private
	 * @function processTransactionRecords
	 * 
	 * @libraries N/search, CLGX2_LIB_DW_Global
	 * 
	 * @param {Object} recordObject
	 * @returns {Object}
	 */
	function processTransactionRecords(recordObject) {
		var recordLength  = recordObject.length;
		var gTransactions = new Object();
		
		for(var r = 0; r < recordLength; r++) {
			gTransactions[recordObject[r].id] = new Array();
			
			var transactions = buildRecordList({			
				recordType: "transaction",
				columns: [
					search.createColumn({ name: "internalid", label: "id"}),
					search.createColumn({ name: "type",       label: "type" })
				],
				filters: [
					search.createFilter({ name: "mainline", operator: "is", values: true }),
					search.createFilter({ name: "entity",   operator: "is", values: parseInt(recordObject[r].id) })
				]
			});
			
			var transactionCount = transactions.length;
			for(var t = 0; t < transactionCount; t++) {
				var tmp   = new Object();
				tmp.id    = transactions[t].id;
				tmp.type  = dwg.getRecordTypeID(transactions[t].type);
				tmp.event = "create";
				
				gTransactions[recordObject[r].id].push(tmp);
			}
		}
		
		return gTransactions;
	}
	
	
	/**
	 * Returns an object of crm records based on the customer.
	 * 
	 * @access private
	 * @function processCRMRecords
	 * 
	 * @libraries N/search
	 * 
	 * @param {Object} recordObject
	 * @returns {Object}
	 */
	function processCRMRecords(recordObject) {
		var recordLength = recordObject.length;
		var records = new Object();
		
		for(var r = 0; r < recordLength; r++) {
			records[recordObject[r].id] = new Array();
			
			var tmpRecords = buildRecordList({			
				recordType: "supportcase",
				columns: [
					search.createColumn({ name: "internalid", label: "id"})
				],
				filters: [
					search.createFilter({ name: "company", operator: "is", values: parseInt(recordObject[r].id) })
				]
			});
			
			var count = tmpRecords.length;
			for(var t = 0; t < count; t++) {
				var tmp   = new Object();
				tmp.id    = tmpRecords[t].id;
				tmp.type  = "supportcase";
				tmp.event = "create";
				
				records[recordObject[r].id].push(tmp);
			}
		}
		
		return records;
	}
	
	
	/**
	 * Returns an object of cinvoice records based on the customer.
	 * 
	 * @access private
	 * @function processCInvoiceRecords
	 * 
	 * @libraries N/search
	 * 
	 * @param {Object} recordObject
	 * @returns {Object}
	 */
	function processCInvoiceRecords(recordObject) {
		var recordLength = recordObject.length;
		var cinvoices = new Object();
		
		for(var r = 0; r < recordLength; r++) {
			cinvoices[recordObject[r].id] = new Array();
			
			var transactions = buildRecordList({			
				recordType: "customrecord_clgx_consolidated_invoices",
				columns: [
					search.createColumn({ name: "internalid", label: "id"})
				],
				filters: [
					search.createFilter({ name: "custrecord_clgx_consol_inv_customer", operator: "is", values: parseInt(recordObject[r].id) })
				]
			});
			
			var transactionCount = transactions.length;
			for(var t = 0; t < transactionCount; t++) {
				var tmp   = new Object();
				tmp.id    = transactions[t].id;
				tmp.type  = "customrecord_clgx_consolidated_invoices";
				tmp.event = "create";
				
				cinvoices[recordObject[r].id].push(tmp);
			}
		}
		
		return cinvoices;
	}
	
	
	/**
	 * Returns an object of contact records based on the customer.
	 * 
	 * @access private
	 * @function processContactRecords
	 * 
	 * @libraries N/search
	 * 
	 * @param {Object} recordObject
	 * @returns {Object}
	 */
	function processContactRecords(recordObject) {
		var recordLength = recordObject.length;
		var records = new Object();
		
		for(var r = 0; r < recordLength; r++) {
			records[recordObject[r].id] = new Array();
			
			var tmpRecords = buildRecordList({			
				recordType: "contact",
				columns: [
					search.createColumn({ name: "internalid", label: "id"})
				],
				filters: [
					search.createFilter({ name: "company", operator: "is", values: parseInt(recordObject[r].id) })
				]
			});
			
			var count = tmpRecords.length;
			for(var t = 0; t < count; t++) {
				var tmp   = new Object();
				tmp.id    = tmpRecords[t].id;
				tmp.type  = "contact";
				tmp.event = "create";
				
				records[recordObject[r].id].push(tmp);
			}
		}
		
		return records;
	}
	
	
	/**
	 * Returns an object of churn records based on the customer.
	 * 
	 * @access private
	 * @function processChurnRecords
	 * 
	 * @libraries N/search
	 * 
	 * @param {Object} recordObject
	 * @returns {Object}
	 */
	function processChurnRecords(recordObject) {
		var recordLength = recordObject.length;
		var records = new Object();
		
		for(var r = 0; r < recordLength; r++) {
			records[recordObject[r].id] = new Array();
			
			var tmpRecords = buildRecordList({			
				recordType: "customrecord_cologix_churn",
				columns: [
					search.createColumn({ name: "internalid", label: "id"})
				],
				filters: [
					search.createFilter({ name: "custrecord_clgx_churn_customer", operator: "is", values: parseInt(recordObject[r].id) })
				]
			});
			
			var count = tmpRecords.length;
			for(var t = 0; t < count; t++) {
				var tmp   = new Object();
				tmp.id    = tmpRecords[t].id;
				tmp.type  = "customrecord_cologix_churn";
				tmp.event = "create";
				
				records[recordObject[r].id].push(tmp);
			}
		}
		
		return records;
	}
	
	
	/**
	 * Returns an object of kw peak records based on the customer.
	 * 
	 * @access private
	 * @function processKWPeakRecords
	 * 
	 * @libraries N/search
	 * 
	 * @param {Object} recordObject
	 * @returns {Object}
	 */
	function processKWPeakRecords(recordObject) {
		var recordLength = recordObject.length;
		var records = new Object();
		
		for(var r = 0; r < recordLength; r++) {
			records[recordObject[r].id] = new Array();
			
			var tmpRecords = buildRecordList({			
				recordType: "customrecord_clgx_dcim_customer_peak",
				columns: [
					search.createColumn({ name: "internalid", label: "id"})
				],
				filters: [
					search.createFilter({ name: "custrecord_clgx_dcim_peak_customer", operator: "is", values: parseInt(recordObject[r].id) })
				]
			});
			
			var count = tmpRecords.length;
			for(var t = 0; t < count; t++) {
				var tmp   = new Object();
				tmp.id    = tmpRecords[t].id;
				tmp.type  = "customrecord_clgx_dcim_customer_peak";
				tmp.event = "create";
				
				records[recordObject[r].id].push(tmp);
			}
		}
		
		return records;
	}
	
	
	/**
	 * Builds a list of records from a saved search or manually.
	 * 
	 * @access private
	 * @function buildRecordList
	 * 
	 * @libraries N/search, N/runtime
	 * 
	 * @param {Object} searchParams
	 * @returns {Object}
	 */
	function buildRecordList(searchParams) {
		var sc      = runtime.getCurrentScript();
		var records = new Array();
		
		var paramObject          = new Object();
		paramObject.scriptID     = sc.id;
		paramObject.deploymentID = sc.deploymentId;
    	paramObject.initialIndex = 0;
    	paramObject.currentIndex = 0;
    	paramObject.pageSize     = 1000;
		
		var searchObject = null;
		
		if(searchParams.searchID !== undefined) {
			searchObject = search.load({ type: searchParams.recordType, id: searchParams.searchID });
			
			if(searchParams.filters !== undefined) {
				var filterCount = searchParams.filters.length;
				for(var f = 0; f < filterCount; f++) { searchObject.filters.push(searchParams.filters[f]); }
			}
			
			if(searchParams.columns !== undefined) {
				var columnCount = searchParams.columns.length;
				for(var c = 0; c < columnCount; c++) { searchObject.columns.push(searchParams.columns[c]); }
			}
			
			searchObject = searchObject.run();
		} else {
			searchObject = search.create({ type: searchParams.recordType, columns: (searchParams.columns === undefined ? [] : searchParams.columns), filters: (searchParams.filters === undefined ? [] : searchParams.filters) }).run();
		}
		
		var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
    	processResults    = processResultSet(searchParams, paramObject, searchResults);
    	
    	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
    		records = processResults.ids;
    	}
    	
    	while (processResults.nextPage == true) {
    		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
    		processResults = processResultSet(searchParams, paramObject, searchResults);
    		records        = records.concat(processResults.ids);
    	}
		
		return records;
	}
	
	
	/**
     * Processes each page of results.
     * 
     * @access private
     * @function processResultSet
     * 
     * @param {Object} searchObject
     * @param {Object} paramObject
     * @param {Object} searchResults
     * @returns {Object}
     */
	function processResultSet(searchParams, paramObject, searchResults) {
    	var usage      = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex = 0;
    	var reschedule = false;
    	var nextPage   = false;
    	var ids        = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		var columnLength = searchResults[r].columns.length;
    		var tmpObject    = {};
    		
    		for(var c = 0; c < columnLength; c++) {
    			if(searchParams.lowerCase == true) {
    				tmpObject[searchResults[r].columns[c].label.toLowerCase()] = searchResults[r].getValue(searchResults[r].columns[c]).toLowerCase();
    			} else {
    				tmpObject[searchResults[r].columns[c].label] = searchResults[r].getValue(searchResults[r].columns[c]);
    			}
    		}
    		
			ids.push(tmpObject);
    		
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }
	
	
	/**
	 * Parses the internal id of a record from a merged system note.
	 * 
	 * @access private
	 * @function parseID
	 * 
	 * @param {String} noteString
	 * @return {Number}
	 */
	function parseID(noteString) {
		return parseInt(noteString.match(/([0-9][0-9])([^\s]+)/)[0]);
	}
	
	
    return {
        execute: execute
    };
    
});
