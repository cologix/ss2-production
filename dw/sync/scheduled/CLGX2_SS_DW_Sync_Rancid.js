/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType ScheduledScript
 */
define(["N/task", "N/file", "N/search", "N/runtime"],
function(task, file, search, runtime) {
    function execute(context) {
    	try {
    		var sc                = runtime.getCurrentScript();
        	var savedSearchRecord = sc.getParameter("custscript_clgx2_rancid_ssr");
        	var savedSearchName   = sc.getParameter("custscript_clgx2_rancid_ss");
        	var fileName          = sc.getParameter("custscript_clgx2_rancid_fn");
        	var folderID          = sc.getParameter("custscript_clgx2_rancid_fid");
        	
        	var finalString  = "";
        	var searchObject = search.load({ type: savedSearchRecord, id: savedSearchName });
        	searchObject.run().each(function(result) {
        		var name = result.getText(result.columns[1]);
        		var make = result.getValue(result.columns[2]);
        		
        		finalString += name.toString() + ',' + make.toString() + ',up\n';
        		
        		return true;
        	});
        	
        	
        	
        	var fileObj = file.create({
    			name    : fileName,
    		    fileType: file.Type.PLAINTEXT,
    		    contents: finalString,
    		    folder  : folderID,
    		    isOnline: false
    		});
        	var fileID = fileObj.save();
        	
        	
        	
        	var scriptTask      = task.create({taskType: "SCHEDULED_SCRIPT" });
        	scriptTask.scriptId = 1582;
        	scriptTask.params   = {
        		custscript_clgx2_ss_ftpapi_fid: fileID, //File internal id
        		custscript_clgx2_ss_ftpapi_fn : fileName, //What the file will be called on the FTP server
        		custscript_clgx2_ss_ftpapi_dir: "rancid" //The directory where the file will be saved.
        	};
        	var scriptTaskId    = scriptTask.submit();
        	
        	
    	} catch(ex) {
    		log.error({ title: "Error", details: ex });
    	}
    }
    
    return {
    	execute: execute
    };
});
