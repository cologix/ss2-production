/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType ScheduledScript
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   2/13/2018
 */
define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global",
        "/SuiteScripts/clgx/dw/sync/lib/inventory/CLGX2_LIB_DW_OutOfBand"
	],
function(file, task, https, record, runtime, search, moment, dwg, dwoob) {
	function execute(context) {
		var records = {
			"response"            : "",
			"proc"                : "CLGX_DW_SYNC_INVENTORY",
			"enviro"              : dwg.get_enviro(),
			"source_id"           : 1,
			"user_id"             : -4,
			"datetime"            : moment().format("YYYY-MM-DD H:mm:ss"),
			//"oob_firewalls"       : dwg.get_list("customlist_clgx_firewall_list"), //Firewall List
			//"oob_dsp"             : dwg.get_list("customlist_clgx_dmz_switch_port_list"), //OOB DMZ Switch Port
			//"oob_ict"             : dwg.get_list("customlist_clgx_oob_xc_type"), //OOB Interconnection Type
			//"oob_pip"             : dwg.get_list("customlist_clgx_oob_pub_ip_addr_list"), //OOB Public IP Address List
			//"oob_cip"             : dwg.get_list("customlist_clgx_oob_cust_ip_addr_lst"), //OOB Customer IP Address List
			//"oob_snm"             : dwg.get_list("customlist_clgx_subnet_mask_list"), //OOB Subnet Mask list
			//"oob_drg"             : dwg.get_list("customlist_clxg_oob_def_router_list"), //OOB Default Router List
			//"oob_vlan"            : dwg.get_list("customlist_clgx_vlan_list"), //VLAN List
			//"inventory_status"    : dwg.get_list("customlist_cologix_inventory_statuslst"), //Inventory Status
			//"cloud_vault"         : dwg.get_list("customlist_clgx_cloud_vault"),
			//"managed_service_type": dwg.get_list("customlist_clgx_managed_services_type"),
			//"cluster"             : dwg.get_list("customlist_clgx_cluster"),
			//"oob"                 : get_outofbands(),
			"device_category"     : dwg.get_list("customlist_clgx_device_category"),
			"device_point_units"  : dwg.get_list("customlist_clgx_cp_environmental_units")
			
	    };
		
		try {
			var requestURL = https.post({ url: "https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm", body: JSON.stringify(records) });
    			records.response = JSON.parse(requestURL.body);
		} catch (error) {
			records.response = "Post error";
		}
		
		var fileObj = file.create({
			name: records.proc + ".json",
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		
		var fileId = fileObj.save();
	}
	
	
	function get_outofbands() {
		var bands = new Array();
		
		var searchObject = search.load({ type: "customrecord_clgx_oob_facility", id: "customsearch_clgx_dw_outofband" });
		searchObject.run().each(function(result) {
			bands.push(dwoob.get_oob(result.getValue(result.columns[0])));
			
			return true;
		});
		
		return bands;
	}
	
    return {
        execute: execute
    };
    
});
