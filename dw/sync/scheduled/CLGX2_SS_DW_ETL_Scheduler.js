/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   6/21/2018
 */
define(["N/task"],
function(task) {
    function execute(scriptContext) {
    	try {
    		
    		//Vendor
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_0";
    		taskObject.submit();
    		
    		//Partner
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_1";
    		taskObject.submit();
    		
    		//Customer
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_2";
    		taskObject.submit();
    		
    		//Contact
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_3";
    		taskObject.submit();
    		
    		//Opportunity
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_4";
    		taskObject.submit();
    		
    		//Proposal
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_5";
    		taskObject.submit();
    		
    		//Service
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_6";
    		taskObject.submit();
    		
    		//Service Order
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_7";
    		taskObject.submit();
    		
    		//Invoice
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_8";
    		taskObject.submit();
    		
    		//CInvoice
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_9";
    		taskObject.submit();
    		
    		//Journal Entry
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_10";
    		taskObject.submit();
    		
    		//Credit Memo
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_11";
    		taskObject.submit();
    		
    		//Space
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_12";
    		taskObject.submit();
    		
    		//Device
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_13";
    		taskObject.submit();
    		
    		//Power
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_14";
    		taskObject.submit();
    		
    		//XC
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_15";
    		taskObject.submit();
    		
    		//VXC
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_16";
    		taskObject.submit();
    		
    		//Equipment
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_17";
    		taskObject.submit();
    		
    		//Churn
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_18";
    		taskObject.submit();
    		
    		//Customer Payment
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_19";
    		taskObject.submit();
    		
    		//Customer Refund
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_20";
    		taskObject.submit();
    		
    		//Project
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_25";
    		taskObject.submit();
    		
    		//Purchase Order
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_26";
    		taskObject.submit();
    		
    		//Vendor Bill
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_27";
    		taskObject.submit();
    		
    		//Vendor Credit
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_28";
    		taskObject.submit();
    		
    		//Vendor Payment
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_29";
    		taskObject.submit();
    		
    		//FAM Asset
    		var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_30";
    		taskObject.submit();
    		
    		
    		//Active Port
    		/*var taskObject = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		taskObject.scriptId     = "1585";
    		taskObject.deploymentId = "customdeploy_clgx2_ss_dw_etl_21";
    		taskObject.submit();*/
    	} catch(ex) {
    		log.error({ title: "CLGX2_SS_DW_ETL_NS", details: ex });
    	}
    }
    
    
    return {
        execute: execute
    };
    
});
