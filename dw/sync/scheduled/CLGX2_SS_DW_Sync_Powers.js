/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Powers.js
 * Script Name	:	CLGX2_SS_DW_Sync_Powers
 * Script ID	:   customscript_clgx2_ss_dw_sync_powers
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_powers
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, moment, dwg) {

	
    function execute(scriptContext) {

    	var power_breakers = dwg.get_list('customlist_clgx_power_ckt_breaker_no');
    	power_breakers.push({
    		"id": 0,
    		"name": "0"
    	});
    	
    	var power_amps = dwg.get_list('customlist_cologix_power_amps_lst');
    	for ( var i = 0; i < power_amps.length; i++ ) {
    		power_amps[i]['amps'] = parseInt(power_amps[i].name.replace(/[^0-9]/g,''));
		}
    	var power_volts = dwg.get_list('customlist_cologix_power_volts_lst');
    	for ( var i = 0; i < power_volts.length; i++ ) {
    		power_volts[i]['volts'] = parseInt(power_volts[i].name.replace(/[^0-9]/g,''));
    		
		}
    	
    	var records = {
	    		"response":             '',
	    		"proc":                 'CLGX_DW_SYNC_POWERS',
	    		"enviro":               dwg.get_enviro(),
	    		"source_id":            1,
	    		"user_id":             -4,
	    		"datetime":            moment().format('YYYY-MM-DD H:mm:ss'),
	    		"power_modules":       dwg.get_list('customlist_clgx_power_module_no'),
	    		"power_lines":         dwg.get_list('customlist_clgx_power_module_lines'),
	    		"power_breakers":      power_breakers,
	    		"power_amps":          power_amps,
	    		"power_volts":         power_volts,
	    		"power_phases":        dwg.get_list('customlist_clgx_voltage_phase'),
	    		"power_serials":       dwg.get_list('customlist_clgx_outlet_box_serial_no'),
	    		"power_fam_type":      [{"id":1, "name":"Generator 1"}, {"id":2, "name":"Generator 2"}, {"id":3, "name":"UPS"}, {"id":4, "name":"Panel"}, {"id":5, "name":"Busway"}],
	    		"power_point_type":    [{"id":1, "name":"Amps"}, {"id":2, "name":"Kw"}, {"id":3, "name":"KwH"}, {"id":4, "name":"Volts"}, {"id":5, "name":"Power Factor"}],
	    		"device_capacity_trees"   : get_capacity_trees(),
	    		"device_power_chains"     : dwg.get_list('customlist_clgx_power_chains')		
    	};
    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}

		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

    function get_amps() {
    	
    	var amps = dwg.get_list('customlist_cologix_power_amps_lst');
    	
    	
    	
    	
    }

    function get_volts() {
    	
    	var volts = dwg.get_list('customlist_cologix_power_amps_lst');
    	
    	
    	
    	
    	
    }
    
	function get_capacity_trees() {
		var employee = [];
		var results = search.load({ type: 'customrecord_clgx_power_capacity_tree', id: "customsearch_clgx_dw_device_capacity_tre" });
		results.run().each(function(result) {
			employee.push({
				"capacity_tree_id"    : parseInt(result.getValue(result.columns[0])),
				"capacity_tree"       : result.getValue(result.columns[1]).replace(/'/g, ""),
				"inactive"            : dwg.get_bit(result.getValue(result.columns[2])),
				"external_id"         : result.getValue(result.columns[3]),
				"facility_id"         : parseInt(result.getValue(result.columns[4]))
			});
	        return true;
		});
		return employee;
	}
	
	
});
