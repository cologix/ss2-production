/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync.js
 * Script Name	:	CLGX2_SS_DW_Sync
 * Script ID	:   customscript_clgx2_ss_dw_sync
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define(["N/task", "N/runtime"],
		
function(task, runtime) {
	
	function execute(scriptContext) {

		try {
			
			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			scriptTask.scriptId = 1307;
			scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_geography';
			var scriptTaskId = scriptTask.submit();
			
			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			scriptTask.scriptId = 1308;
			scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_entities';
			var scriptTaskId = scriptTask.submit();
			
			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			scriptTask.scriptId = 1310;
			scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_accounting';
			var scriptTaskId = scriptTask.submit();
			
			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			scriptTask.scriptId = 1311;
			scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_transact';
			var scriptTaskId = scriptTask.submit();	
			
			var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
			scriptTask.scriptId = 1444;
			scriptTask.deploymentId = 'customdeploy_clgx2_ss_dw_sync_audit';
			var scriptTaskId = scriptTask.submit();	
			
		}
		catch (error) {

		}
		
    }

    return {
        execute: execute
    };
    
});
