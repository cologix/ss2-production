/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Entities.js
 * Script Name	:	CLGX2_SS_DW_Sync_Entities
 * Script ID	:   customscript_clgx2_ss_dw_sync_entities
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_entities
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/https",
		"N/record", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global",
		"/SuiteScripts/clgx/dw/sync/lib/entity/CLGX2_LIB_DW_Group"
	],
		
function(file, https, record, search, moment, dwg, dwgg) {
	
	function execute(scriptContext) {
    	
		var records = {
				"response"                 : '',
				"proc"                     : 'CLGX_DW_SYNC_ENTITIES',
				"enviro"                   : dwg.get_enviro(),
				"source_id"                : 1,
				"user_id"                  : -4,
				"datetime"                 : moment().format('YYYY-MM-DD H:mm:ss'),
				"data_source"              : get_data_source(),
				"customer_status"          : get_customer_status(),
				"customer_segment"         : get_customer_segment(),
				"payment_terms"            : dwg.get_list('term'),
				"msa_type"                 : dwg.get_list('customlist_clgx_msa_types'),
				"pub_perm"                 : dwg.get_list('customlist_clgx_pub_perm_list'),
				"renewal_term"             : dwg.get_list('customlist_clgx_cust_renewal_term'),
				"disco_notice_term"        : dwg.get_list('customlist_clgx_disco_notice_terms'),
				"sla_terms"                : dwg.get_list('customlist_clgx_sla_terms'),
				"required_compliance"      : dwg.get_list('customlist_clgx_compliance_employee'),
				"language"                 : get_language(),
				"vendor_category"          : get_vendor_category(),
				"vendor_payment_type"      : dwg.get_list('customlist_clgx_pymnt_types'),
				"partner_terms"            : dwg.get_list('customlist_clgx_partner_terms'),
				"partner_territory"        : dwg.get_list('customlist_clgx_partner_territories'),
				"contact_role"             : dwg.get_list('contactrole'),
				"contact_secondary_role"   : dwg.get_list('customlist_clgx_secondary_role'),
				"project_status"           : dwg.get_list('customlist_clgx_project_status'),
				"project_category"         : dwg.get_list('customlist_clgx_capex_category'),
				"employee"                 : get_employee(),
				"group"                    : get_groups(),
				"entity_status"            : get_entity_status()
    		};
    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}
		
		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

	
	function get_data_source() {
		var arr = [
				{"source_id":1, "source":"Netsuite API"},
				{"source_id":2, "source":"Customer Portal API"},
				{"source_id":3, "source":"ETL CSV"},
				{"source_id":4, "source":"ETL ODBC"}
		];
		return arr;
	}
	
	function get_customer_status() {
		var customer_status = [];
		var results = search.create({
			type: search.Type.CUSTOMER,
			filters: [],
			columns: [
				{"name":"status","summary":"GROUP"}
			]
		});
		results.run().each(function(result) {
			customer_status.push({
				"status_id":		parseInt(result.getValue({ name: "status", summary: "GROUP" })),
				"status":			result.getText({ name: "status", summary: "GROUP" })
			});
            return true;
		});
		return customer_status;
	}

	function get_customer_segment() {
		var customer_segment = [];
		var results = search.create({
			type: search.Type.CUSTOMER,
			filters: [
				{name: "category",operator: "noneof",values: "@NONE@"}
			],
			columns: [
				{"name":"category","summary":"GROUP"}
			]
		});
		results.run().each(function(result) {
			customer_segment.push({
				"segment_id":		parseInt(result.getValue({ name: "category", summary: "GROUP" })),
				"segment":			result.getText({ name: "category", summary: "GROUP" })
			});
            return true;
		});
		return customer_segment;
	}
	
	function get_payment_terms() {
		var payment_terms = [];
		var results = search.create({
			type: search.Type.CUSTOMER,
			filters: [],
			columns: [
				{"name":"terms","summary":"GROUP"}
			]
		});
		results.run().each(function(result) {
			var payment_terms_id = parseInt(result.getValue({ name: "terms", summary: "GROUP" }));
			if(payment_terms_id){
				payment_terms.push({
					"payment_terms_id":	parseInt(result.getValue({ name: "terms", summary: "GROUP" })),
					"payment_terms":		result.getText({ name: "terms", summary: "GROUP" })
				});
			}
            return true;
		});
		return payment_terms;
	}
	
	function get_language() {
		var language = [];
		var results = search.create({
			type: "customer",
			filters: [
				{name: "language",operator: "noneof",values: "@NONE@"}
			],
			columns: [
				{"name":"language","summary":"GROUP"}
			]
		});
		results.run().each(function(result) {
			language.push({
				"language_id":		result.getValue({ name: "language", summary: "GROUP" }),
				"language":			result.getText({ name: "language", summary: "GROUP" })
			});
            return true;
		});
		return language;
	}
		
	function get_vendor_category() {
		var vendor_category = [];
		var results = search.create({
			type: "vendor",
			filters: [
				{name: "category",operator: "noneof",values: "@NONE@"}
			],
			columns: [
				{"name":"category","summary":"GROUP"}
			]
		});
		results.run().each(function(result) {
			vendor_category.push({
				"category_id":		parseInt(result.getValue({ name: "category", summary: "GROUP" })),
				"category":			result.getText({ name: "category", summary: "GROUP" })
			});
            return true;
		});
		return vendor_category;
	}

	function get_employee() {
		var employee = [];
		var results = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx_dw_employee" });
		results.run().each(function(result) {
			employee.push({
				"employee_id":		parseInt(result.getValue(result.columns[0])),
				"employee":			result.getValue(result.columns[1]).replace(/'/g, ""),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"first_name":		result.getValue(result.columns[4]).replace(/'/g, ""),
				"last_name":			result.getValue(result.columns[5]).replace(/'/g, ""),
				"title":				result.getValue(result.columns[6]),
				"subsidiary_id":		parseInt(result.getValue(result.columns[7])),
				"department_id":		parseInt(result.getValue(result.columns[8])),
				"location_id":		parseInt(result.getValue(result.columns[9])),
				"facility_id":		parseInt(result.getValue(result.columns[10])),
				"supervisor_id":		parseInt(result.getValue(result.columns[11])),
				"hired_by_id":		parseInt(result.getValue(result.columns[12])),
				"support_rep":		dwg.get_bit(result.getValue(result.columns[13])),
				"sales_rep":			dwg.get_bit(result.getValue(result.columns[14])),
				"email":				result.getValue(result.columns[15]),
				"phone":				result.getValue(result.columns[16]),
				"mobile":			result.getValue(result.columns[17]),
				"skype":				result.getValue(result.columns[18]),
			});
	        return true;
		});
		return employee;
	}
	
	function get_groups() {
		var groups = [];
		
		var searchObject = search.load({ type: "entitygroup", id: "customsearch_clgx_dw_group" });
		searchObject.run().each(function(result) {
			groups.push(dwgg.get_group(result.getValue(result.columns[0])));
			return true;
		});
		
		return groups;
	}
	
	function get_entity_status() {
		var statuses = [];
		
		var columns = new Array();
		columns.push(search.createColumn("internalid"));
		columns.push(search.createColumn("name"));
		columns.push(search.createColumn("externalid"));
		columns.push(search.createColumn("isinactive"));
		columns.push(search.createColumn("includeinleadreports"));
		columns.push(search.createColumn("probability"));
		columns.push(search.createColumn("description"));
		
		var searchObject = search.create({ type: "customerstatus", columns: columns });
		searchObject.run().each(function(result) {
			statuses.push({ 
				status_id       : result.getValue("internalid"), 
				status          : result.getValue("name"),
				externalid      : result.getValue("externalid"),
				isinactive      : dwg.get_bit(result.getValue("isinactive")),
				includeinreports: dwg.get_bit(result.getValue("includeinleadreports")),
				probability     : result.getValue("probability"),
				description     : result.getValue("description")
			});
			
			return true;
		});
		
		return statuses;
		
		/*return [{"status_id":"14","status":"Closed Lost"},
			    {"status_id":"16","status":"Lost Customer"},
			    {"status_id":"21","status":"Qualified"},
			    {"status_id":"9","status":"Budgetary Quote"},
			    {"status_id":"10","status":"Proposal"},
			    {"status_id":"11","status":"In Negotiation"},
			    {"status_id":"13","status":"Closed Won"}];*/
	}

});
