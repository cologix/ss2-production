/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date			:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Items.js
 * Script Name	:	CLGX2_SS_DW_Sync_Items
 * Script ID		:   customscript_clgx2_ss_dw_sync_items
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_items
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/https",
		"N/record", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
		
function(file, https, record, search, moment, dwg) {

	function execute(scriptContext) {
		
		var item = get_item();
		var records = {
			"response": 	'',
			"proc": 		'CLGX_DW_SYNC_ITEMS',
			"enviro": 		dwg.get_enviro(),
			"source_id": 	1,
			"user_id": 		-4,
			"datetime": 	moment().format('YYYY-MM-DD H:mm:ss'),
			"item":			item
    		};
    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}
		
		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

    function get_item() {
		var item = [];
		var results = search.load({ type: 'item', id: "customsearch_clgx_dw_item" });
		results.run().each(function(result) {
			var item_id = parseInt(result.getValue(result.columns[0]));
			
			//log.debug({ title: "item_id ", details: ' | item_id - '+ item_id + ' |' });
			var full_name = result.getValue(result.columns[1]);
			var rpt_category = get_rpt_category(item_id,full_name) || "";
			var names = full_name.split(":");
			var name = (names[names.length-1]).trim();
			
			var record_type = get_item_type(result.getValue(result.columns[14]));
			var rec = record.load({ type: record_type, id: item_id });
			var name_fr = rec.getSublistValue({
			    sublistId: 'translations',
			    fieldId: 'displayname',
			    line: 2
			});
			if(!name_fr){
				name_fr = name;
			}
			
			//log.debug({ title: "item_id ", details: ' | item_id - '+ item_id + ' | name_fr - '+ name_fr + ' |' });
			
			item.push({
				"item_id":			item_id,
				"item":				name,
				"item_fr":			name_fr,
				"full_name":		full_name,
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"name":				result.getValue(result.columns[4]),
				"parent_id":		parseInt(result.getValue(result.columns[5])),
				"class_id":			parseInt(result.getValue(result.columns[6])),
				"category_id":		parseInt(result.getValue(result.columns[7])),
				"expenseaccount_id":parseInt(result.getValue(result.columns[8])),
				"incomeaccount_id":	parseInt(result.getValue(result.columns[9])),
				"amps_id":			parseInt(result.getValue(result.columns[10])),
				"volts_id":			parseInt(result.getValue(result.columns[11])),
				"item_standard_id":	parseInt(result.getValue(result.columns[12])),
				"quantity":			parseFloat(result.getValue(result.columns[13])),
				"rpt_category":		rpt_category
			});
	        
	        return true;
		});
		return item;
	}
    
    function get_item_type(type) {
    	var record_type = "";
    	
    	if(type == "Discount") {
    		record_type = "discountitem";
    	} else if(type == "NonInvtPart") {
    		record_type = "noninventoryitem";
    	} else if(type == "OthCharge") {
    		record_type = "otherchargeitem";
    	} else if(type == "Service") {
    		record_type = "serviceitem";
    	}
    	
    	return record_type;
    }
    
    function get_rpt_category(item_id,full_name) {
    	var rep_categ = '';
    	
		if(full_name.indexOf('Interconnection') != -1){
			if(full_name.indexOf('Cloud Connect') != -1){
				rep_categ = 'XC - Cloud Connect';
			}
			else if(full_name.indexOf('Copper') != -1){
				rep_categ = 'XC - Copper';
			}
			else if(full_name.indexOf('Coax') != -1){
				rep_categ = 'XC - Coax';
			}
			else if(full_name.indexOf('Fiber') != -1){
				rep_categ = 'XC - Fiber';
			}
			else if(full_name.indexOf('POTS') != -1){
				rep_categ = 'XC - POTS';
			}
			else {
				rep_categ = 'XC - Other';
			}
		} 
		else if(full_name.indexOf('Space') != -1){
			if(full_name.indexOf('Cabinet') != -1){
				rep_categ = 'Space - Cabinets';
			} 
			else if(full_name.indexOf('Foot Print') != -1){
				if(full_name == 'Space : Foot Print'){
					rep_categ = 'Space - Cage';
				}
				else if(full_name.indexOf('Cage') != -1){
					rep_categ = 'Space - Cage';
				}
				else {
					rep_categ = 'Space - Other';
				}
			} 
			else{
				rep_categ = 'Space - Other';
			}
		} 
		else if(full_name.indexOf('Power') != -1){
			rep_categ = 'Power';
		}
		else if(full_name.indexOf('Standard Connections') != -1){
			rep_categ = 'Standard Connections';
		}
		else if(full_name.indexOf('Network') != -1){
			rep_categ = 'Network';
		}
		else if(full_name.indexOf('Managed Services') != -1){
			rep_categ = 'Managed Services';
		}
		else if(full_name.indexOf('Disaster') != -1){
			rep_categ = 'Disaster Recovery';
		}
		else{
			rep_categ = 'Other';
		}
    	return rep_categ;
    }
    

});
