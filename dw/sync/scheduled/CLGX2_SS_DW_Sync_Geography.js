/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_DW_Sync_Geography.js
 * Script Name	:	CLGX2_SS_DW_Sync_Geography
 * Script ID	:   customscript_clgx2_ss_dw_sync_geography
 * Deployment ID:   customdeploy_clgx2_ss_dw_sync_geography
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, moment, dwg) {

	
    function execute(scriptContext) {
    	
	    	var subsidiary = get_subsidiary();
	    	var location = get_location(subsidiary);
	    	var facility = get_facility(location);
	    	var departments = get_department();
	    	
	    	var records = {
	    			"response": 					'',
	    			"proc": 						'CLGX_DW_SYNC_GEOGRAPHY',
	    			"enviro": 						dwg.get_enviro(),
	    			"source_id": 					1,
	    			"user_id": 						-4,
	    			"datetime": 					moment().format('YYYY-MM-DD H:mm:ss'),
	    			"clocation": 					get_clocation(),
	    			"subsidiary": 					subsidiary,
	    			"market": 						get_market(facility),
	    			"rlocation": 					get_rlocation(location),
	    			"location": 					location,
	    			"location_junction_clocation":	get_location_junction_clocation(),
	    			"building_number": 				dwg.get_list('customlist_cologix_facility_bldgnumber'),
	    			"building_clli": 				dwg.get_list('customlist_cologix_facility_clli_lst'),
	    			"facility": 					facility,
	    			"department": 					departments.department,
	    			"department_subsidiary": 		departments.subsidiary
	    	};
	    	
		try {
		    	var requestURL = https.post({
		    	    url: 'https://lucee-nnj3.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm',
		    	    body: JSON.stringify(records)
		    	});
	    		records.response = JSON.parse(requestURL.body);
		}
		catch (error) {
			records.response = 'Post error';
		}

		var fileObj = file.create({
			name: records.proc + '.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(records),
		    description: records.proc,
		    encoding: file.Encoding.UTF8,
		    folder: 5453175,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
    }

    return {
        execute: execute
    };

	function get_clocation() {
		
		var clocation = [];
		var results = search.load({ type: "customrecord_clgx_consolidate_locations", id: "customsearch_clgx_dw_clocation" });
		results.run().each(function(result) {
			clocation.push({
				"clocation_id":		parseInt(result.getValue(result.columns[0])),
				"clocation":		result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3])
			});
            return true;
		});
		return clocation;
	}

	function get_subsidiary() {
		var subsidiary = [];
		var results = search.load({ type: "subsidiary", id: "customsearch_clgx_dw_subsidiary" });
		results.run().each(function(result) {
			subsidiary.push({
				"subsidiary_id":	parseInt(result.getValue(result.columns[0])),
				"subsidiary":		result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"parent_id":		parseInt(result.getValue(result.columns[4])),
				"currency_id":		parseInt(result.getValue(result.columns[5])),
				"address1":			result.getValue(result.columns[6]),
				"address2":			result.getValue(result.columns[7]),
				"address3":			result.getValue(result.columns[8]),
				"city":				result.getValue(result.columns[9]),
				"zip":				result.getValue(result.columns[10]),
				"state":			result.getValue(result.columns[11]),
				"country":			result.getValue(result.columns[12]),
				"elimination":		dwg.get_bit(result.getValue(result.columns[13]))
			});
            return true;
		});
		return subsidiary;
	}

	function get_location() {
		var location = [];
		var results = search.load({ type: "location", id: "customsearch_clgx_dw_location" });
		results.run().each(function(result) {
			
			var location_id = parseInt(result.getValue(result.columns[0]));
			var rec = record.load({ type: 'location', id: location_id });
			var subsidiary_id = rec.getValue({ fieldId: "subsidiary" });
			
			location.push({
				"location_id":		location_id,
				"location":			result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"subsidiary_id":	parseInt(subsidiary_id),
				"rlocation_id":		parseInt(result.getValue(result.columns[5])),
				"address1":			result.getValue(result.columns[6]),
				"address2":			result.getValue(result.columns[7]),
				"address3":			result.getValue(result.columns[8]),
				"city":				result.getValue(result.columns[9]),
				"zip":				result.getValue(result.columns[10]),
				"state":			result.getValue(result.columns[11]),
				"country":			result.getValue(result.columns[12]),
				"phone":			result.getValue(result.columns[13])
			});
            return true;
		});
		return location;
	}
		
	function get_location_junction_clocation() {
		var location_junction_clocation = [];
		var results = search.load({ type: "customrecord_clgx_consolidate_locations", id: "customsearch_clgx_dw_loc_junction_cloc" });
		results.run().each(function(result) {
			location_junction_clocation.push({
				"clocation_id":		parseInt(result.getValue(result.columns[0])),
				"location_id":		parseInt(result.getValue(result.columns[1]))
			});
            return true;
		});
		return location_junction_clocation;
	}

	function get_facility(location) {
		var facility = [];
		var results = search.load({ type: "customrecord_cologix_facility", id: "customsearch_clgx_dw_facility" });
		results.run().each(function(result) {
			
			var subsidiary_id = null;
			var location_id = parseInt(result.getValue(result.columns[7]));
			for ( var i = 0; i < location.length; i++ ) {
				if(location[i].location_id == location_id){
					subsidiary_id = location[i].subsidiary_id;
				}
			}
			facility.push({
				"facility_id":		parseInt(result.getValue(result.columns[0])),
				"facility":			result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"subsidiary_id":	subsidiary_id,
				"customer_id":		parseInt(result.getValue(result.columns[5])),
				"market_id":		parseInt(result.getValue(result.columns[6])),
				"location_id":		location_id,
				"building_clli_id":	parseInt(result.getValue(result.columns[8])),
				"building_number_id":parseInt(result.getValue(result.columns[9])),
				"address1":			result.getValue(result.columns[10]),
				"address2":			result.getValue(result.columns[11]),
				"city":				result.getValue(result.columns[12]),
				"zip":				result.getValue(result.columns[13]),
				"state":			result.getText(result.columns[14]),
				"country":			result.getText(result.columns[15])
			});
            return true;
		});

		return facility;
	}

	function get_market(facility) {
		var mlist = dwg.get_list('customlist_clgx_market');
		var market = [];
		for ( var i = 0; i < mlist.length; i++ ) {
			var subsidiary_id = null;
			for ( var j = 0; j < facility.length; j++ ) {
				if(facility[j].market_id == mlist[i].id){
					subsidiary_id = facility[j].subsidiary_id;
				}
			}
			market.push({
				"market_id":		mlist[i].id,
				"market":			mlist[i].name,
				"subsidiary_id":	subsidiary_id
			});
		}
		return market;
	}

	function get_rlocation(location) {
		var rlist = dwg.get_list('customlist_clgx_reporting_location');
		var rlocation = [];
		for ( var i = 0; i < rlist.length; i++ ) {
			var subsidiary_id = null;
			for ( var j = 0; j < location.length; j++ ) {
				if(location[j].rlocation_id == rlist[i].id){
					subsidiary_id = location[j].subsidiary_id;
				}
			}
			rlocation.push({
				"rlocation_id":			rlist[i].id,
				"rlocation":			rlist[i].name,
				"subsidiary_id":		subsidiary_id
			});
		}
		return rlocation;
	}

	function get_department() {
		var department = [];
		var subsidiary = [];
		var results = search.load({ type: "department", id: "customsearch_clgx_dw_department" });
		results.run().each(function(result) {
			
			var department_id = parseInt(result.getValue(result.columns[0]));
			var rec = record.load({ type: "department", id: department_id});
			var parent_id  = parseInt(rec.getValue({ fieldId: "parent" }));
			var ids  = rec.getValue({ fieldId: "subsidiary" });
			for(var i = 0; ids != null && i < ids.length; i++) {
				var subsidiary_id = parseInt(ids[i]);
				if(subsidiary_id){
					subsidiary.push({
						"department_id": 	department_id,
						"subsidiary_id": 	subsidiary_id
					})
				}
			}
			department.push({
				"department_id":	department_id,
				"department":		result.getValue(result.columns[1]),
				"inactive":			dwg.get_bit(result.getValue(result.columns[2])),
				"external_id":		result.getValue(result.columns[3]),
				"parent_id":		parent_id
			});
            return true;
		});
		return {
			"department": department,
			"subsidiary": subsidiary,
		};
	}
		

});
