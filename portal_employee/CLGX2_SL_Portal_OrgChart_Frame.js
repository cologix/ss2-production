/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   5/21/2018
 */
define(["N/ui/serverWidget"],
function(widget) {
    function onRequest(context) {
    	var form       = widget.createForm({ title: "Organization Chart" });
    	var frameField = form.addField({ id: "custpage_clgx_html", label: "iframe", type: "inlinehtml" });
    	frameField.defaultValue = "<iframe name=\"portal\" id=\"portal\" src=\"/app/site/hosting/restlet.nl?script=1534&deploy=1&compid=1337135\" height=\"600px\" width=\"1200px\" frameborder=\"0\" scrolling=\"no\"></iframe>";
    	context.response.writePage(form);
    }

    return {
        onRequest: onRequest
    };
    
});
