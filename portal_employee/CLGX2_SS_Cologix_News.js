/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   4/20/2018
 */
define(["N/https"],
function(https) {
    function execute(scriptContext) {
    	
    }

    return {
        execute: execute
    };
    
});
