/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module EPortal/Lib/CLGX2_LIB_Core
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   5/3/2018
 */
define(["N/runtime", "N/search", "/SuiteScripts/clgx/libraries/lodash.min"],
function(runtime, search, _) {
	/**
	 * Finds and replaces an element in a string using RegExp
	 * 
	 * @access public
	 * @function replaceElement
	 * 
	 * @param {string} haystack - The string we are searching.
	 * @param {string} find - The element we are looking for.
	 * @param {string} replace - The string that replaces what has been found.
	 * @return {string}
	 */
	function replaceElement(haystack, find, replace) {
		return haystack.replace(new RegExp(find, "g"), replace);
	}
	
	/**
	 * Builds the main menu of the EPortal.
	 * 
	 * @access public
	 * @function buildPrimaryMenu
	 * 
	 * @returns {string}
	 */
	function buildPrimaryMenu() {
		var finalMenu     = "";
		
		var userContext = runtime.getCurrentUser();
		
		var contextObject = {
			currentUser    : userContext.id,
			currentRole    : userContext.role,
			currentLanguage: userContext.getPreference("language")
		};
		
		var columns         = new Array();
		columns.push(search.createColumn("internalid"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_parent"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_sort_order"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_title"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_description"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_type"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_fileid"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_href"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_all_roles"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_roles"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_employees"));
		
		var filters         = new Array();
		filters.push(search.createFilter({ name: "isinactive",                      operator: "is",    values: false }));
		filters.push(search.createFilter({ name: "custrecord_clgx_menu_sys_parent", operator: "anyOf", values: 13    }));
		
		var searchObject = search.create({ type: "customrecord_clgx_menus_system",  filters: filters,  columns: columns });
		searchObject.run().each(function(result) {
			var internalID   = result.getValue("internalid");
			var hasAllRights = result.getValue("custrecord_clgx_menu_sys_all_roles");
			var employees    = result.getValue("custrecord_clgx_menu_sys_employees");
			var roles        = result.getValue("custrecord_clgx_menu_sys_roles");
			var title        = null;
			var description  = null;
			
			if(contextObject.currentLanguage == "en_US" || contextObject.currentLanguage == "en") {
				title       = result.getValue("custrecord_clgx_menu_sys_title");
				description = result.getValue("custrecord_clgx_menu_sys_description");
			} else {
				title       = result.getValue("custrecord_clgx_menu_sys_title_fr");
				description = result.getValue("custrecord_clgx_menu_sys_description_fr");
			}
			
			if(hasAllRights == true || ((_.includes(roles, contextObject.currentRole.toString())) || (_.includes(employees, contextObject.currentUser.toString())))) {
				var arguments = "";
				
				if(result.getValue("custrecord_clgx_menu_sys_type") == 1) { arguments = "&id=" + result.getValue("custrecord_clgx_menu_sys_fileid"); }
				
				finalMenu += "<li>";
				
				if(result.getValue("custrecord_clgx_menu_sys_href") == "#" || result.getValue("custrecord_clgx_menu_sys_href") == "") {
					finalMenu += "<a class=\"drop\" href=\"#\" title=\"" + description + "\">" + title + "</a>";
				} else {
					finalMenu += "<a class=\"drop\" href=\"" + result.getValue("custrecord_clgx_menu_sys_href") + arguments + "\" title=\"" + description + "\">" + title + "</a>";
				}
				
				//finalMenu += buildSubMenu(contextObject, internalID);
				finalMenu += "</li>";
			}
			
			return true;
		});
		
		return finalMenu;
	}
	
	
	/**
	 * Builds a sub-menu based off the main menu
	 * 
	 * @access public
	 * @function buildSubMenu
	 * 
	 * @param {object} contextObject
	 * @param {object} columns
	 * @param {object} filters
	 * 
	 * @returns {string}
	 */
	function buildSubMenu(contextObject, parentID) {
		var finalMenu = "";
		
		var columns   = new Array();
		columns.push(search.createColumn("internalid"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_parent"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_sort_order"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_title"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_description"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_type"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_fileid"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_href"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_all_roles"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_roles"));
		columns.push(search.createColumn("custrecord_clgx_menu_sys_employees"));
		
		var filters   = new Array();
		filters.push(search.createFilter({ name: "isinactive",                      operator: "is",    values: false    }));
		filters.push(search.createFilter({ name: "custrecord_clgx_menu_sys_parent", operator: "anyOf", values: parentID }));
		
		finalMenu += "<div class=\"dropdown_1column\">\n";
		finalMenu += "<div class=\"col_1_firstcolumn\">\n";
		finalMenu += "<ul class=\"levels\">\n";
		
		var searchObject = search.create({ type: "customrecord_clgx_menus_system", columns: columns, filters: filters });
		searchObject.run().each(function(result) {
			var internalID   = result.getValue("internalid");
			var hasAllRights = result.getValue("custrecord_clgx_menu_sys_all_roles");
			var userArray    = result.getValue("custrecord_clgx_menu_sys_employees").split(",");
			var roleArray    = result.getValue("custrecord_clgx_menu_sys_roles").split(",");
			
			var title        = null;
			var description  = null;
			
			if(contextObject.currentLanguage == "en_US" || contextObject.currentLanguage == "en") {
				title       = result.getValue("custrecord_clgx_menu_sys_title");
				description = result.getValue("custrecord_clgx_menu_sys_description");
			} else {
				title       = result.getValue("custrecord_clgx_menu_sys_title_fr");
				description = result.getValue("custrecord_clgx_menu_sys_description_fr");
			}
			
			if(hasAllRights == true || ((_.includes(roleArray, contextObject.currentRole)) || (_.includes(userArray, contextObject.currentUser)))) {
				var arguments = "";
				
				if(result.getValue("custrecord_clgx_menu_sys_type") == 1) { arguments = "&id=" + result.getValue("custrecord_clgx_menu_sys_fileid"); }
				
				finalMenu += "<li>";
				
				if(result.getValue("custrecord_clgx_menu_sys_href") == "#" || result.getValue("custrecord_clgx_menu_sys_href") == "") {
					finalMenu += "<a class=\"drop\" href=\"#\" title=\"" + description + "\">" + title + "</a>";
				} else {
					finalMenu += "<a class=\"drop\" href=\"" + result.getValue("custrecord_clgx_menu_sys_href") + arguments + "\" title=\"" + description + "\">" + title + "</a>";
				}
				
				finalMenu += "</li>";
			}
			
			return true;
		});
		
		finalMenu += "</ul>\n";
		finalMenu += "</div>\n";
		finalMenu += "</div>\n";
		
		return finalMenu;
	}
	
	
    return {
        replaceElement: replaceElement,
        buildPrimaryMenu: buildPrimaryMenu,
        buildSubMenu: buildSubMenu
    };
    
});
