/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 * 
 * @module EPortal/CLGX2_RL_Portal_OrgChart
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   5/4/2018
 */
define(["N/runtime", "N/search", "N/file", "/SuiteScripts/clgx/portal_employee/lib/CLGX2_LIB_Core"],
function(runtime, search, file, core) {
    /**
     * Function called upon sending a GET request to the RESTlet.
     *
     * @param {Object} requestParams - Parameters from HTTP request URL; parameters will be passed into function as an Object (for all supported content types)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.1
     */
    function doGet(requestParams) {
    	try {
    		var scriptContext      = runtime.getCurrentScript();
    		var fileObject         = file.load({ id: scriptContext.getParameter("custscript_clgx2_rl_ep_fileid") });
    		
    		var jsonObject         = new Object();
    		jsonObject["id"]       = 1900301;
    		jsonObject["name"]     = "<b style=\"color: gold;\">Bill Fathers</b><br /><i>Chief Executive Officer</i>";
    		jsonObject["data"]     = new Object();
    		jsonObject["children"] = buildEmployeeObject(1900301);

    		return core.replaceElement(fileObject.getContents(), "{empoyeeOrgChartData}", JSON.stringify(jsonObject));
    	} catch(ex) {
    		return JSON.stringify(ex);
    	}
    }
    
    
    /**
     * Recursive function for building the JSON of parent-child employees.
     * 
     * @access private
     * @function buildEmployeeObject
     * 
     * @param {number} supervisorID - The internal id of the employees supervisor.
     * @returns {object}
     */
    function buildEmployeeObject(supervisorID) {
    	var jsonObject    = new Array();
    	var tmpJsonObject = new Object();
    	
    	var columns    = new Array();
    	var filters    = new Array();
    	
    	columns.push(search.createColumn("internalid"));
    	columns.push(search.createColumn("entityid"));
    	columns.push(search.createColumn("title"));
    	columns.push(search.createColumn("supervisor"));
    	filters.push(search.createFilter({ name: "isinactive", operator: "is",     values: false }));
    	filters.push(search.createFilter({ name: "supervisor", operator: "anyOf",  values: supervisorID }));
    	filters.push(search.createFilter({ name: "internalid", operator: "noneOf", values: 12827 }));
    	
    	var searchObject = search.create({ type: "employee", columns: columns, filters: filters }).run();
    	
    	searchObject.each(function(result) {
    		tmpJsonObject             = new Object();
    		tmpJsonObject["id"]       = result.getValue("internalid");
    		tmpJsonObject["name"]     = "<b style=\"color: gold;\">" + result.getValue("entityid") + "</b><br /><i>" + result.getValue("title") + "</i>";
    		tmpJsonObject["data"]     = new Object();
    		tmpJsonObject["children"] = buildEmployeeObject(result.getValue("internalid"));
    		jsonObject.push(tmpJsonObject);
    		
    		return true;
    	});
    	
    	return jsonObject;
    }

    return {
        "get": doGet
    };
    
});
