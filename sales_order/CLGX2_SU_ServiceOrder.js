/**
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   7/13/2017
 * 
 * Script Name  : CLGX2_SU_ServiceOrder
 * Script File  : CLGX2_SU_ServiceOrder.js
 * Script ID    : customscript_clgx2_su_serviceorder
 * Deployment ID: customdeploy_clgx2_su_serviceorder
 * Deployed To  : Service Order
 * Event Type   : Create
 * 
 * 
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/search", "N/log", "N/record"],
function(search, log, record) {
	/**
	 * Returns the Internal IDs of the purchase orders linked to a proposal.
	 * 
	 * @param {number} proposalID
	 * @return array
	 */
	function findPurchaseOrderRecords(proposalID) {
		var poSearch = search.create({
			type: search.Type.PURCHASE_ORDER,
			columns: ["internalid", "createdfrom"],
			filters: [["mainline", "is", "T"], "and", 
			          ["custbody_clgx_proposal_no", "is", proposalID]]
		});
		
		
		var finalIdArray = [];
		poSearch.run().each(function(result) {
			finalIdArray.push(result.getValue("internalid"));
			
			return true;
		});
		
		return finalIdArray;
	}
	
	
	/**
	 * Updates the specified purchase order's service order field.
	 * 
	 * @param {number} serviceOrderID  - The internal ID of the service order.
	 * @param {number} purchaseOrderID - The internal ID of a purchase order.
	 * @return void
	 */
	function updatePurchaseOrderRecord(serviceOrderID, purchaseOrderID) {
		var po = record.load({
			type: record.Type.PURCHASE_ORDER,
			id: purchaseOrderID
		});
		

		var proposalField     = po.getValue("custbody_clgx_proposal_no");
		var serviceOrderField = po.getValue("custbody_clgx_so_link_pur");
		
		if((proposalField != null || proposalField != "") && (serviceOrderField == null || serviceOrderField == "")) {
			po.setValue({ fieldId: "custbody_clgx_so_link_pur", value: serviceOrderID });
			po.save();
		}
	}
	
	
	/**
	 * After Submit entry point.
	 * 
	 * @param {object} context - The script execution context.
	 * @return void
	 */
    function afterSubmit(context) {
    	try {
    		var serviceOrderRecord = context.newRecord;
    		var serviceOrderID     = serviceOrderRecord.id;
    		var proposalID         = serviceOrderRecord.getValue("createdfrom");
    		
    		var purchaseOrders     = findPurchaseOrderRecords(proposalID);
        	var purchaseOrderCount = purchaseOrders.length;
        		
        	for(var po = 0; po < purchaseOrderCount; po++) {
        		updatePurchaseOrderRecord(serviceOrderID, purchaseOrders[po]);
        	}
    	} catch(e) {
    		log.debug({title: "After Submit Error", details: e});
    	}
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
