/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/record", "N/runtime", "N/search", "N/email"],
function(record, runtime, search, email) {
	function execute() {
		try {
			var scriptContext = runtime.getCurrentScript();
			var salesOrderID  = parseInt(scriptContext.getParameter("custscript_clgx2_1740_soid"));
			
			if(salesOrderID) {
				var recordObject = record.load({ type: "salesorder", id: salesOrderID });
				var itemCount    = recordObject.getLineCount("item");
				var orderStatus  = recordObject.getValue("status");
				
				for(var i = 0; i < itemCount; i++) {
					var serviceID = recordObject.getSublistValue({ sublistId: "item", fieldId: "custcol_clgx_so_col_service_id", line: i });
					
					// save pair service / service order to custom record to process - added 11/10/2013
					if (serviceID != '' && serviceID != null && serviceID != -1 && orderStatus != 'closed' && orderStatus != 'cancelled' && orderStatus != 'fullyBilled'){
						var serviceSO = search.lookupFields({ type: "job", id: serviceID, columns: "custentity_cologix_service_order" });
						
						if(parseInt(serviceSO["custentity_cologix_service_order"][0].value) != salesOrderID) {
							var queueRecord = record.create({ type: "customrecord_clgx_services_to_update" });
							queueRecord.setValue({ fieldId: "custrecord_clgx_services_to_update_serv", value: serviceID    });
							queueRecord.setValue({ fieldId: "custrecord_clgx_services_to_update_so",   value: salesOrderID });
							queueRecord.save({ enableSourcing: true, ignoreMandatoryFields: true });
						}
					}
				}
			} else {
				log.error({ title: "ERROR", details: "Missing parameter value for: Sales Order ID" });
			}
		} catch(ex) {
			emailError({
				author: 1349020,
				recipients: [1349020],
				subject: "CLGX2_SS_ServiceOrder_Services - Error",
				body: "Error on script <b>CLGX2_SS_ServiceOrder_Services</b>: <br /><br /><b>Service Order ID: </b> " + runtime.getCurrentScript().getParameter("custscript_clgx2_1740_soid") + "<br />Error: " + ex
			});
		}
	}
	
	
	/**
	 * Emails an error to the specified user(s).
	 * 
	 * @access private
	 * @function emailError
	 * 
	 * @param {Object} paramObject
	 * @returns null
	 */
	function emailError(paramObject) {
		if(paramObject) {
			email.send({
				author: paramObject.author,
				recipients: paramObject.recipients,
				subject: paramObject.subject,
				body: paramObject.body
			});
		}
	}
	
    return {
        execute: execute
    };
});
