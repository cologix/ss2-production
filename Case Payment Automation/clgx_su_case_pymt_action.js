/**
 * @NApiVersion 2.x
 */
define(['N/record', 'N/currentRecord'],
/**
 * @param {record} record
 * @param {currentRecord} currentRecord
 */
function(record, currentRecord) {
   
	function createPayment(context){
		var caseRecord = currentRecord.get();
		var caseId = caseRecord.id;
		
		var url = '/app/accounting/transactions/custpymt.nl'
			
		url += '?&clgx_case_id=' + caseId;
			
		window.open(url);
        
        
	}
	
    return {
        createPayment : createPayment
    };
    
});
