/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/ui/serverWidget', 'N/runtime'],
/**
 * @param {redirect} redirect
 * @param {serverWidget} serverWidget
 */
function(serverWidget, runtime) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(context) {
    	
    	if(context.type == context.UserEventType.VIEW){
	    	var form = context.form;
	    	var caseRecord = context.newRecord;
	    	var caseType = caseRecord.getValue('category');
	    	var caseStatus = caseRecord.getValue('status');
	    	var user = runtime.getCurrentUser();
	    	var role = user.role;
	    	var userCenter = user.roleCenter;
//	    	log.debug(userCenter);
//	    	log.debug(role);
	    	if(role == 1088 || userCenter == 'ACCOUNTCENTER'){
	    		if(caseType == '11'){
			    	if(caseStatus != 5){
					     	form.clientScriptModulePath = 'SuiteScripts/clgx/Case Payment Automation/clgx_su_case_pymt_action.js';
					    	form.addButton({
					    		id: 'custpage_payment_button',
					    		label: 'Create Payment',
					    		functionName: 'createPayment'
					    	});
			    	}
		    	}
	    	}
    	}
    	
    }
    
	
    return {
        beforeLoad: beforeLoad
    };
    
});
