/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/redirect', 'N/runtime'],
/**
 * @param {record} record
 */
function(record, redirect, runtime) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(context) {
    	var form = context.form;
    	var payment = context.newRecord;
    	var request = context.request;

    	if(context.type == context.UserEventType.CREATE){
	    	var originCaseId = request.parameters.clgx_case_id;
	    	payment.setValue('custbody_clgx_pmnt_created_from_case', originCaseId);
	    	var accountRadio = form.getField('undepfunds');
	    	accountRadio.defaultValue = 'T';	
	    }
    } 	
        /**
         * Function definition to be triggered before record is loaded.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type
         * @Since 2015.2
         */
    	
    	function afterSubmit(context) {
    		
        	var payment = context.newRecord;
        	var paymentId = payment.id;      	
        	var originCaseId = payment.getValue('custbody_clgx_pmnt_created_from_case');
        	var closeCase = payment.getValue('custbody_clgx_close_case');
      	
        	if(originCaseId != null){
		    	var originCase = record.load({
					type: record.Type.SUPPORT_CASE,
					id: originCaseId,
					isDynamic: true
				});
		    	
		    	var userId = runtime.getCurrentUser().id;
		    	var caseAssigned = originCase.getValue({fieldId: 'assigned'});
		    	var caseSubType = originCase.getValue({fieldId: 'custevent_cologix_sub_case_type'});
		    	
		    	if(caseSubType == '54'){
			    	if(userId != caseAssigned){
			    		originCase.setValue('assigned', userId);
			    	}
		    	}
		    	
		    	
		    	if(closeCase){
		    		originCase.setValue('status', 5);
		    	}
		    	
		    	originCase.save();
        	}
		    	
	            redirect.toRecord({
	                type: record.Type.SUPPORT_CASE,
	                id: originCaseId
	            });
		    	
        	
    	}   	

    

    return {
        beforeLoad: beforeLoad,
        afterSubmit: afterSubmit
    };
    
});
