/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record'],
/**
 * @param {record} record
 */
function(record) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
	
    function pageInit(context) {
    	var payment = context.currentRecord;
    	var caseId = payment.getValue('custbody_clgx_pmnt_created_from_case');
    	if(caseId){
	    	var originCase = record.load({
				type: record.Type.SUPPORT_CASE,
				id: caseId,
				isDynamic: true
			});
    			
			var paymentCustomer = originCase.getValue({fieldId: 'company'});			
			payment.setValue('customer', paymentCustomer);
    	}
    }

    return {
        pageInit: pageInit,
    };
    
});
