/**
 * @NApiVersion 2.0
 * @NScriptType bankStatementParserPlugin
 */
define(['N/file', 'N/log', 'N/record','N/search', 
	"/SuiteScripts/clgx/libraries/lodash.min", "/SuiteScripts/clgx/libraries/moment.min", 
	"N/runtime", "N/task", "N/email"],
	function (file, log, record, search,  _, moment, runtime, task, email) {
      return {
         parseBankStatement: function (context) {
        	var currency = 1; //USD checks
			var errors = '';
			var code = 0;
			var checkCount = 0;
			var getNext = true;
			var lineNumber = 0;
			var parsedCheckData = [];
			var payments = [];
			var paymentCount = 0;
			var location = 40;
			var customer = 2771523;
			try {
			//log.debug({title: 'begin bankStatementParserPlugin',details: "bankStatementParserPlugin"});
	        	var depositDate = moment().format('M/D/YYYY');
	        	var fileObject = context.input.file;
	        	fileObject.name   = "JPFile " + moment().format('lll') + ".txt";
	        	var filename = fileObject.name;
	        	fileObject.folder = 17220013;
	        	var fileID = fileObject.save();
	        	var existingFile = file.load({ id: fileID });
	        	var iterator = fileObject.lines.iterator();
	        	var item = iterator.next();
	        	lineNumber++;
	        	var payAccount = 519;
				while (!item.done) 
				{
					var invoiceFromFile = [];	
					code = this._getRecordCode(item.value);

					if (code == '5')
					{
						depositDate = this._formatDateNetSuite2(item.value.substring(14, 20).trim());
					}
					else if (code == '7')
					{
						var batchTotal = (parseFloat(item.value.substring(24, 33).trim()))/ Math.pow(10, 2);
						var batch = item.value.substring(1, 4).trim();

					}  
//					else if (code == '8')
//					{	
//						var FileTotal = (parseFloat(item.value.substring(35, 45).trim()))/ Math.pow(10, 2);
//			       		log.debug({
//		    				"title" : "FileTotal",
//		    				"details" : FileTotal
//		    			});
//					}  
					else if (code == '6')
					{
						var custName = '';
				
						var amount = parseFloat(item.value.substring(8, 17).trim());
						var amount2 = parseFloat(item.value.substring(7, 17).trim());
						var scaledAmount = amount / Math.pow(10, 2);
                        var totalPaymentAmount = item.value.substring(8, 17) ? scaledAmount : null;
                   	
                        
                        var batchNumber = item.value.substring(1, 4).trim();

						var itemNumber = item.value.substring(4, 7).trim();
						var checkAmount = (parseFloat(totalPaymentAmount.toString())).toFixed(2);
						var transactionType = item.value.substring(17, 20);

						custName = item.value.substring(20, 60).replace(/^(0+)/g, '').trim();
						var checkNumber = item.value.substring(60, 75).trim();
						checkCount++;
						item = iterator.next();
						lineNumber++;
						var peekCode = this._getRecordCode(item.value);

						//recordtype = 4 (overflow) can be an unlimited amount of records with extended data for recordtype = 6 records
						while (!item.done && peekCode == '4')
						{

							var invNumber = item.value.substring(11,27).replace(/^(0+)/g, '').trim();
						
							//strip leading zeroes
							var invNumberInt = invNumber.replace(/^(0+)/g, '');
							//remove dot
							invNumberInt = invNumberInt.replace('.', '');
							var invInt = parseInt(invNumberInt.toString());
							var invoiceString = '';
							
			
							if (invInt > 9999999999)
							{
								invoiceString = (Math.floor(invInt/1000000)).toString().trim() + '.' + (invInt%1000000).toString().trim();
							}                 
							else if (invInt > 999999999)
							{
								invoiceString = (Math.floor(invInt/100000)).toString().trim() + '.' + (invInt%100000).toString().trim();
							}
							else if (invInt > 99999999)
							{
								invoiceString = (Math.floor(invInt/10000)).toString().trim() + '.' + (invInt%10000).toString().trim();
							}
							
							if (!(isNaN(invoiceString)) && invoiceString.length > 7)
							{
								invoiceFromFile.push(invoiceString.trim());
							}
							var originatortoBeneficiary = item.value.substring(43,142).replace(/\s+/g, '').replace(/\./g, '').replace(/\D/g,' ');

							var invoicesRemarks = '';

							if (originatortoBeneficiary.length > 0)
					        {
		                         invoicesRemarks = originatortoBeneficiary.split(' ');
		                     }
					          
							for (var index = 0; index < invoicesRemarks.length; index++) 
						       {
						           var element = invoicesRemarks[index];
						           var front = '';
						           var back =  '';
						           var invoiceString = '';
						           if (element)
						           {
							          	var elementInv = parseInt(element.toString());
							          	if (elementInv > 9999999999 )
							          	{
							          		front = (Math.floor(elementInv/1000000)).toString().trim();
								          	back =  (elementInv%1000000).toString().trim();
								          	invoiceString = front + '.' + back;
							          	}                     
							          	else if (elementInv > 999999999)
										{
							          	  	 front = (Math.floor(elementInv/100000)).toString().trim();
								          	 back =  (elementInv%100000).toString().trim();
								          	invoiceString = front + '.' + back;
										}
							          	else if (invInt < 99999999)
										{
											front = (Math.floor(elementInv/10000)).toString().trim();
								          	back =  (elementInv%10000).toString().trim();
								          	invoiceString = front + '.' + back;
										}
							          	if (invoiceString.length > 7)
							          	{	
							          		invoiceFromFile.push(invoiceString);
							          	}
						           }
						       }

						

							item = iterator.next();
							lineNumber++;
							peekCode = this._getRecordCode(item.value);

						}

						invoiceFromFile = _.uniq(invoiceFromFile);
						parsedCheckData.push({
								'batch_Number' : batchNumber,
								 'item_Number' : itemNumber,
								 'check_Amount' : checkAmount,
								'check_Number' : checkNumber,
								'invoice_Numbers': invoiceFromFile,
								'file_Name': filename,
								'tranDate': depositDate,
								'customer_name': custName,
								'trans_type': transactionType,
								'pay_acct': payAccount,
								'currency': currency,
								'acct_location': location,
								'customer': customer
								});
						paymentCount++;
						getNext = false;
						} //if (code == '6')

						if (getNext)
						{
							item = iterator.next();
							lineNumber++;
						}
						else
						{
							getNext = true;
						}

					}// while (!item.done)		


				var paramObject = new Object();
				var saved_search = 'customsearch_clgx2_consolidatedforpay';
				var filename2 = 'JPParsedData ' + moment().format('lll') + '.json';

	        	var fileObject = file.create({
	    			name: filename2,
	    			fileType: file.Type.PLAINTEXT,
	    			folder: 17220013,
	    			contents: JSON.stringify(parsedCheckData)
	    		});

	        	var fileID = fileObject.save();
	        

	        	this._scheduleScript({ scriptId: "1925", params: { custscript_clgx2_1878_current_index: 0,
																   custscript_clgx2_1878_savedsearch: saved_search,
																   custscript_clgx2_1878_fileid: fileID,
																   custscript_clgx2_1878_currencyid: 1,
																   custscript_clgx2_1878_subsidiary: 5,
																   custscript_clgx2_1878_location: 53,
																   custscript_clgx2_1878_customer: 'JPM BAI2',
																   custscript_clgx2_1878_aracct: 122,
																   custscript_clgx2_1878_depositfileid: 0} });


		}//try
				catch (ex) {
					var documentedException = {
						cause: ex,
						lineNumber: lineNumber,
						lineText: item,
						currentState: 'currentState',
						currentRecord: 'currentRecord'
					};
					log.error({title: 'MAIN LOOP EXCEPTION', details: documentedException});
					throw documentedException;
				}
         },
     	_scheduleScript:  function _scheduleScript(paramObject) {
        	try {

        		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
        		t.scriptId     = paramObject.scriptId;
        		t.deploymentId = paramObject.deploymentId;
        		t.params       = paramObject.params;
        		t.submit();

        	} catch(ex) {
        		log.debug({ title: "scheduleScript - Error", details: ex });
        	}

     	},
         _formatDateNetSuite2: function _formatDateNetSuite2(date) {
				try {
					var year = parseInt(date.substring(0, 2), 10),
						month = parseInt(date.substring(2, 4), 10),
						day = parseInt(date.substring(4, 6), 10);
					if (year < 60) {
						year += 2000;
					}
					else {
						year += 1900;
					}

					return year.toString()
						+ '/' + (month < 10 ? '0' : '') + month.toString()
						+ '/' + (day < 10 ? '0' : '') + day.toString();
				}
				catch (ex) {
					throw {message: 'Could not parse date', date: date, cause: ex};
				}
			},

			/**
			 * Parses the first character of a line to determine the record code
			 * @param {string} line line of text
			 * @returns {string} record code
			 * @private
			 */
			_getRecordCode: function _getRecordCode(line) {

				return line.substring(0, 1);
			},

			getStandardTransactionCodes: function (context) {
			},


      }
   }
);
