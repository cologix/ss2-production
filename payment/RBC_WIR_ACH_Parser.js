/**
 * @NApiVersion 2.0
 * @NScriptType bankStatementParserPlugin
 */
define(['N/file', 'N/log', 'N/record','N/search', 
	"/SuiteScripts/clgx/libraries/lodash.min", "/SuiteScripts/clgx/libraries/moment.min", 
	"N/runtime", "N/task", "N/email"],
	function (file, log, record, search,  _, moment, runtime, task, email) {
      return {
         parseBankStatement: function (context) {
        	var currency = 3; //CAD default
    		var errors = '';
			var code = 0;
			var overflowing = 0;
			var getNext = true;
			var lineNumber = 0;
			var parsedCheckData = [];
			var payments = [];
			var paymentCount = 0;
			var itemNumber = 0;
			var location = 9;
			try {
	
	        	var depositDate = moment().format('M/D/YYYY');
	        	var fileObject = context.input.file;
	        	fileObject.name   = "RBCFile ACH WIR " + moment().format('lll') + ".txt";
	        	var filename = fileObject.name;
	        	fileObject.folder = 17220014;
	        	var fileID = fileObject.save();
	        	var existingFile = file.load({ id: fileID });
	        	var iterator = fileObject.lines.iterator();
	        	var item = iterator.next();
	        	lineNumber++;
	        	var line;
	        	var payAccount = 0;
	        	var batchNumber;
	        	var customer;
				while (!item.done) 
				{
					var invoiceFromFile = [];	
					code = this._getRecordCode(item.value);
					if (code == '02')
					{	
						line = item.value.split(',');
						depositDate = this._formatDateNetSuite2(line[4].trim());
					
					}
					else if (code == '03')
					{
						line = item.value.split(',');
						if (line[1].length > 10)
						{
							var acc = line[1].substring(10);
							if (acc == '1030709')
							{
								payAccount = 241;
								currency = 3;
								location = 9;
								batchNumber = '10004';
								customer = 2824067;
							}
							else if (acc == '1035872')
							{
								payAccount = 434;
								currency = 3;
								location = 9;
								batchNumber = '10024';
								customer = 2824067;
							}
							else if (acc == '1216977')
							{
								payAccount = 314;
								currency = 3;
								location = 9;
								batchNumber = '10009';
								customer = 2824067;
							}
							else if (acc == '4003745')
							{
								payAccount = 433;
								currency = 1;
								location = 9;
								batchNumber = '10023';
								customer = 2824066;   //CAD BANK WITH USD PAYMENT
							}
							
						}
						
//						1030709  id = 241 10004 Bank Accounts : RBC - Montreal Lockbox CAD 1030709
//						1035872  id = 434 10024 Bank Accounts : RBC - Toronto Lockbox CAD 1035872
//						1216977  id = 314  10009 Bank Accounts : RBC - Toronto AP CAD 1216977
//						4003745  id = 433 10023 Bank Accounts : RBC - Toronto USD 4003745
					}
					else if (code == '49')
					{
						payAccount = 0;
					}
					else if (code == '16')
					{
						line = item.value.split(',');
						//log.debug({title: 'line', details: line});
						//195 Incoming Money Transfer
						//174 Other Deposit
						//142 ACH Credit Received
						//399 Miscellaneous Credit
						if (line[1].trim() == '195' || line[1].trim() == '174'|| line[1].trim() == '142' || line[1].trim() == '399' )  
						{
						
						
						var transactionType = 'WIR';//WIR type = 195, ACH 399
						if (line[1].trim() == '399' || line[1].trim() == '142')
						{
							transactionType = 'ACH';
						}
						var amount = (parseFloat(line[2]))/Math.pow(10, 2);
						var totalPaymentAmount = amount;
                     
						itemNumber = itemNumber + 1;
						var checkAmount = (parseFloat(totalPaymentAmount.toString())).toFixed(2);
						var checkNumber = '';
						if (line[7] != null)
						{
							checkNumber = line[7].trim();
                           checkNumber = line[7].trim().replace('/', '');
						}

						item = iterator.next();
						lineNumber++;
						var peekCode = this._getRecordCode(item.value);

						//recordtype = 88 (overflow) can be an unlimited amount of records with extended data for recordtype = 16 records, only take info from 1st 88 record
						//while (!item.done && peekCode == '88')
					    if (!item.done && peekCode == '88')
						{
							line = item.value.split(',');
							var custName = '';
							if (line[1] != null && line[1].length > 0)
							{
								custName = line[1].replace(/^(0+)/g, '').trim();
							}
							var invNumber = '';
							if (line[4] != null && line[4].length > 0)
							{
								invNumber = line[4].replace(/^(0+)/g, '').trim();
                              	custName = custName + '||' + line[4].trim();
							}

							var invNumberInt = '';
							//strip leading zeroes
							if (invNumber != null)
							{
								invNumberInt = invNumber.replace(/^(0+)/g, '');
							}
							//remove dot
							var invInt = '';
							if (invNumberInt != null)
							{
								invNumberInt = invNumberInt.replace('.', '');
                                invInt = parseInt(invNumberInt.toString());
							}
							var invoiceString = '';
							if (invInt > 9999999999)
							{
								invoiceString = (Math.floor(invInt/1000000)).toString().trim() + '.' + (invInt%1000000).toString().trim();
							}                 
							else if (invInt > 999999999)
							{
								invoiceString = (Math.floor(invInt/100000)).toString().trim() + '.' + (invInt%100000).toString().trim();
							}
							else if (invInt > 99999999)
							{
								invoiceString = (Math.floor(invInt/10000)).toString().trim() + '.' + (invInt%10000).toString().trim();
							}
							if (!(isNaN(invoiceString)) && invoiceString.length > 7)
							{
								invoiceFromFile.push(invoiceString.trim());
							}

 						    item = iterator.next();
							lineNumber++;
							peekCode = this._getRecordCode(item.value);

						}
						while (!item.done && peekCode == '88')
					    {
					    	line = item.value.split(',');
					    	if (line[1] != null && line[1].length > 0)
							{
					    		custName = custName + '||' + line[1].trim();
							}
					    	if (line[2] != null && line[2].length > 0)
							{
					    		custName = custName + '||' + line[2].trim();
							}
					    	item = iterator.next();
							lineNumber++;
							peekCode = this._getRecordCode(item.value);
					    }
					    if (custName.length > 500)
					    {
					    	custName = custName.substring(0,499);
					    }
						invoiceFromFile = _.uniq(invoiceFromFile);
						parsedCheckData.push({
								'batch_Number' : batchNumber,
								 'item_Number' : itemNumber,
								 'check_Amount' : checkAmount,
								'check_Number' : checkNumber,
								'invoice_Numbers': invoiceFromFile,
								'file_Name': filename,
								'tranDate': depositDate,
								'customer_name': custName,
								'trans_type': transactionType,
								'pay_acct': payAccount,
								'currency': currency,
								'acct_location': location,
								'customer': customer
								});
							paymentCount++;
							getNext = false;
							
							}
						} //if (code == '16')
			           
						if (getNext)
						{
							item = iterator.next();
							lineNumber++;
						}
						else 
						{
							getNext = true;
						}
				
					}// while (!item.done)		

				var paramObject = new Object();
				var saved_search = 'customsearch_clgx2_consolidatedforpay';
				var filename =  'RBCParsedData ACH WIR ' + moment().format('lll') + '.json';
				
	        	var fileObject = file.create({
	    			name: filename, 
	    			fileType: file.Type.PLAINTEXT,
	    			folder: 17220014,
	    			contents: JSON.stringify(parsedCheckData)
	    		});
	        	
	        	var fileID = fileObject.save();
				this._scheduleScript({ scriptId: "1925", params: { custscript_clgx2_1878_current_index: 0,
																   custscript_clgx2_1878_savedsearch: saved_search,
																   custscript_clgx2_1878_fileid: fileID,
																   custscript_clgx2_1878_currencyid: 3, 
																   custscript_clgx2_1878_subsidiary: 6,
																   custscript_clgx2_1878_location: 9,  
																   custscript_clgx2_1878_customer: 'RBC ACH & WIR', 
																   custscript_clgx2_1878_aracct: 122,
																   custscript_clgx2_1878_depositfileid: 0} });
		}//try
				catch (ex) {
					var documentedException = {
						cause: ex,
						lineNumber: lineNumber,
						lineText: item,
						currentState: 'currentState',
						currentRecord: 'currentRecord'
					};
					log.error({title: 'MAIN LOOP EXCEPTION', details: documentedException});
					throw documentedException;
				}
         },
     	_scheduleScript:  function _scheduleScript(paramObject) {
        	try {
        		
        		
        		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
        		t.scriptId     = paramObject.scriptId;
        		t.deploymentId = paramObject.deploymentId;
        		t.params       = paramObject.params;
        		t.submit();
        	} catch(ex) {
        		log.debug({ title: "scheduleScript - Error", details: ex });
        	}

     	},
         _formatDateNetSuite2: function _formatDateNetSuite2(date) {
				try {
					var year = parseInt(date.substring(0, 2), 10),
						month = parseInt(date.substring(2, 4), 10),
						day = parseInt(date.substring(4, 6), 10);
					if (year < 60) {
						year += 2000;
					}
					else {
						year += 1900;
					}

					return year.toString()
						+ '/' + (month < 10 ? '0' : '') + month.toString()
						+ '/' + (day < 10 ? '0' : '') + day.toString();
				}
				catch (ex) {
					throw {message: 'Could not parse date', date: date, cause: ex};
				}
			},

			/**
			 * Parses the first character of a line to determine the record code
			 * @param {string} line line of text
			 * @returns {string} record code
			 * @private
			 */
			_getRecordCode: function _getRecordCode(line) {
	
				return line.substring(0, 2);
			},
			
			getStandardTransactionCodes: function (context) {
			},

      }
   }
);
