/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
/**
 @author:         Liz Soucy - liz.soucy@cologix.com
 @date:           03/04/2020
 @description:    Payments
 Script Name:     CLGX2_SS_Populate_PaymentRecords
 Script ID:       customscript_clgx2_populate_pay_records
 Link:            /app/common/scripting/script.nl?id=1738
 */

define(["N/record", 
    "N/search", 
    "N/runtime",
    "N/file", 
    "N/task", 
    "/SuiteScripts/clgx/libraries/lodash.min",
    "/SuiteScripts/clgx/libraries/moment.min",
    "N/email"
    ],
    function(record, search, runtime, file, task, _, moment, email) {
        function execute(context) {
            try {

                var scriptRuntime = runtime.getCurrentScript();

                var depfileID = '';
                var bankName = '';
                var paramObject = new Object();
                paramObject.currentIndex = scriptRuntime.getParameter("custscript_clgx2_1878_current_index");
                paramObject.savedSearch = scriptRuntime.getParameter("custscript_clgx2_1878_savedsearch");
                paramObject.fileID = scriptRuntime.getParameter("custscript_clgx2_1878_fileid");
                paramObject.currencyID = scriptRuntime.getParameter("custscript_clgx2_1878_currencyid");
                paramObject.subsidiary = scriptRuntime.getParameter("custscript_clgx2_1878_subsidiary");
                paramObject.location = scriptRuntime.getParameter("custscript_clgx2_1878_location");
                paramObject.customer = scriptRuntime.getParameter("custscript_clgx2_1878_customer");
                paramObject.aracct = scriptRuntime.getParameter("custscript_clgx2_1878_aracct");
                paramObject.depositfileID = scriptRuntime.getParameter("custscript_clgx2_1878_depositfileid");


                var reschedule = createPaymentRecords(paramObject);

                if (reschedule) {
                    //log.debug({
                    //   "title": "rescheduled",
                    //   "details": reschedule
                    // });
                    paramObject.depositfileID = depfileID;
                    scheduleScript(paramObject);
                }
            } catch (ex) {
                log.error({title: "Error SS_Populate_PaymentRecords", details: ex});
            }
        }

        function createPaymentRecords(paramObject) {
            try {

                var usage = runtime.getCurrentScript().getRemainingUsage();
                var existingFile = file.load({id: paramObject.fileID});
                var existingContent = JSON.parse(existingFile.getContents());
//                log.debug({
//                   "title": "existingContent",
//                   "details": existingContent
//                 });

          
                var deposits = [];
                if (paramObject.depositfileID != 0) {
                   var depositFile = file.load({id: paramObject.depositfileID});
                   deposits = JSON.parse(depositFile.getContents());
                }
                var paymentAmount = 0;
				var match = true;
                var totalCheckAmount = 0;
              	var totalBilledAmount = 0;
                var checkNumber = '';
                var invoices = [];
                var filteredInvoices= [];
                var fileName = '';
                var batchItem = '';
                var tranDate = moment().format('M/D/YYYY');
                var consolidated = [];
                var invoiceReferences = '';
                var ids = [];
                var payment;
                var currencyID = paramObject.currencyID;
                var beginIndex = paramObject.currentIndex;
                var transactionType = '';
                var location = paramObject.location;
				var subsidiary = paramObject.subsidiary;
				var account = 0;
				var unDeposited = 'T';
				var aracct = parseInt(paramObject.aracct);
					
                if (existingContent.length > 0) {

                	bankName = paramObject.customer;

                    for (var k = beginIndex; k < existingContent.length; k++) {
                    	match = true;
                        usage = runtime.getCurrentScript().getRemainingUsage();
                        if (usage <= 200) 
                        {
                            break;
                        }
                        paramObject.currentIndex++;

                        invoices = [];
                        invoiceReferences = '';
                        location = paramObject.location;
        				subsidiary = paramObject.subsidiary;

                        consolidated = existingContent[k].invoice_Numbers;
                        consolidated = _.uniq(consolidated);
                        consolidated = _.remove(consolidated, function (n) {
                            return n != "99999.99999";
                        });
                        totalCheckAmount = parseFloat(existingContent[k].check_Amount).toFixed(2);
                        tranDate = existingContent[k].tranDate;
                        checkNumber = existingContent[k].check_Number;
                        filename = existingContent[k].file_Name;
                       
                        transactionType = existingContent[k].trans_type || '';
                        account = 0;
                        location = paramObject.location;
                        unDeposited = 'T';

                        var batchID = existingContent[k].batch_Number;
                        batchItem = 'Batch:' + batchID + ' Item:' + existingContent[k].item_Number;
						var remarks = existingContent[k].customer_name;
                        currencyID = parseInt(existingContent[k].currency);

						if (!(consolidated.length > 0))
						{
                          match = false;
                        }
					    if (consolidated.length > 0) {
                            //ConsolidatedInvoices Exist, find associated invoices
                            var consolidatedInvoices = search.load({id: paramObject.savedSearch});
                            var defaultFilters = consolidatedInvoices.filterExpression;
                            if (defaultFilters.length > 0)
                                defaultFilters.push("AND");
                            for (var j = 0; j < consolidated.length; j++) {
                                defaultFilters.push(['custbody_consolidate_inv_nbr', 'contains', consolidated[j]]);
                                if (consolidated.length > 0 && j < consolidated.length - 1) {
                                    defaultFilters.push("OR");
                                }
                                invoiceReferences += '[' + consolidated[j].toString() + "] ";
                            }
            		  		
            		  		
                            consolidatedInvoices.filterExpression = defaultFilters;
                            consolidatedInvoices.run().each(
                                function (result) {
                                    if (parseFloat(result.getValue(result.columns[1])) > 0) {
                                        invoices.push({
                                            'invoice_id': parseInt(result.getValue(result.columns[0])),
                                            'invoice_amount': (parseFloat(result.getValue(result.columns[1]))).toFixed(2),
                                            'customer_id': parseInt(result.getValue(result.columns[4])),
                                            'location_id': parseInt(result.getValue(result.columns[5])),
                                            'subsidiary_id': parseInt(result.getValue(result.columns[6])),
                                            'consolidated_invoice_number': result.getValue(result.columns[7]),
                                            'currency_id': result.getValue(result.columns[8]),
                                        });
                                    }
                                    return true;
                                });
                            
						if (!(invoices.length > 0)){
                          match = false;
                        }

						//Loop through for each customer
						totalBilledAmount = 0;
					    totalBilledAmount = invoices.reduce(function (prev, cur) {
                             return (parseFloat(prev)) + (parseFloat(cur.invoice_amount));
                         }, 0);
					    totalBilledAmount = parseFloat(totalBilledAmount).toFixed(2);
						var paymentID = 0;
						
						var custID = 0;
						var customerIDs = _.uniq(_.map(invoices, 'customer_id'));
						var remainingCheckAmount = parseFloat(totalCheckAmount).toFixed(2);
						
						var applyToInvoices = false;
						if (totalBilledAmount == totalCheckAmount)
						{
							applyToInvoices = true;
						}
						var custCount = customerIDs.length;
						if (custCount > 1 && applyToInvoices == false)
						{
							match = false;
						}
						if (custCount == 1)
						{
							  match = true;
							  currencyID = invoices[0].currency_id;
							  subsidiary = invoices[0].subsidiary_id;
							  location = invoices[0].location_id;
							  
							  if ((transactionType == 'WIR' || transactionType == 'ACH') && (parseInt(currencyID) == parseInt(existingContent[k].currency)))
		                       {
								  account = existingContent[k].pay_acct;
								  location = existingContent[k].acct_location;
								  unDeposited = 'F';
		                       }

							  
							  paymentAmount = totalCheckAmount;
							  payment = record.create({type: 'customerpayment'});
                              payment.setValue({fieldId: 'currency', value: currencyID});
                              payment.setValue({fieldId: 'exchangerate', value: 1});
                              payment.setValue({fieldId: 'customer', value: invoices[0].customer_id});
                              payment.setValue({fieldId: 'payment', value: paymentAmount});
                              payment.setValue({fieldId: 'trandate', value: new Date(tranDate)});
                              payment.setValue({fieldId: 'subsidiary', value: subsidiary});
                              payment.setValue({fieldId: 'memo', value: 'Bank Payment Received ' + transactionType});
                              payment.setValue({fieldId: 'location', value: location});
                              payment.setValue({fieldId: 'aracct', value: aracct}); 
                              payment.setValue({fieldId: 'undepfunds', value: unDeposited});
                              payment.setValue({fieldId: 'checknum', value: checkNumber});
                              payment.setValue({fieldId: 'custbody_clgx_bg_payment_file', value: filename});
                              payment.setValue({fieldId: 'custbody_clgx_bg_payment_trans_id', value: batchItem.trim() + ' ' + remarks.trim()});
                              payment.setValue({fieldId: 'custbody_clgx_payment_ci_num', value: invoiceReferences});
                              payment.setValue({fieldId: 'autoapply', value: false});
                              if (account > 0)
                              {
                                  payment.setValue({fieldId: 'account', value: account });  
                              }
                       
                              paymentID = payment.save({enableSourcing: false,isDynamic: true, ignoreMandatoryFields: true });

                              deposits.push({
                                  'batch': batchID,
                                  'paymentID': paymentID,
                                  'amount': parseFloat(paymentAmount),
                                  'depositDate': new Date(tranDate),
                                  'location': parseInt(location),
                                  'subsidiary': parseInt(subsidiary)
                                });
                              
                              if(applyToInvoices == true)
                              {
                              	  var remainingAmount = parseFloat(totalCheckAmount).toFixed(2);
                                  customerBillPayment = record.load({type: 'customerpayment',id: paymentID, isDynamic: true});
                                  
                              	   for (var i = 0; i < invoices.length; i++) 
                              	   {
                                         var linePay = customerBillPayment.findSublistLineWithValue({sublistId: 'apply',fieldId: 'internalid',value: invoices[i].invoice_id});
                                         var lineNum = customerBillPayment.selectLine({sublistId: 'apply',line: linePay});
                                         var amt = parseFloat(invoices[i].invoice_amount).toFixed(2);

                                         if (remainingAmount > 0 && amt > 0) 
                                         {
                                             var invoicePayAmount = parseFloat(Math.min(remainingAmount, amt)).toFixed(2);
                                             customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'apply',line: lineNum,value: true});
                                             customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'doc',line: lineNum,value: invoices[i].invoice_id});
                                             customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'amount',line: lineNum,value: invoicePayAmount});
                                             customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'refnum',line: lineNum,value: invoices[i].invoice_id});
                                             customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'applydate',line: lineNum,value: new Date(tranDate)});
                                             customerBillPayment.commitLine({sublistId: 'apply',line: lineNum});

                                             remainingAmount = parseFloat(remainingAmount - invoicePayAmount).toFixed(2);
                                         }
                              	   }
                                  customerBillPayment.save();
                              }
						}//if (custCount == 1)
						//more than one customer and billed = paid, each cust gets full payment amount
						else if (custCount > 1 && applyToInvoices == true )
						{
							match = true;
							var remainingCheckAmount = totalCheckAmount;
							 for (var c = 0; c < customerIDs.length; c++) 
							  {
							
								  var paymentAmount = 0;
								  custID = parseInt(customerIDs[c]);
								  filteredInvoices = _.filter(invoices, function(o) {
										return o.customer_id == custID;
									});
								  for (var i = 0; i < filteredInvoices.length; i++)
								  {		
									  paymentAmount = parseFloat(parseFloat(paymentAmount) + parseFloat(filteredInvoices[i].invoice_amount)).toFixed(2);
									  location = filteredInvoices[i].location_id;
									  subsidiary = filteredInvoices[i].subsidiary_id;
									  currencyID = filteredInvoices[i].currency_id;
								  }
								  
								if ( paymentAmount > 0 && remainingCheckAmount >= paymentAmount)
								{  

								 payment = record.create({type: 'customerpayment'});
		                         payment.setValue({fieldId: 'currency', value: currencyID});
		                         payment.setValue({fieldId: 'exchangerate', value: 1});
		                         payment.setValue({fieldId: 'customer', value: custID});
		                         payment.setValue({fieldId: 'payment', value: parseFloat(paymentAmount).toFixed(2)});
		                         payment.setValue({fieldId: 'trandate', value: new Date(tranDate)});
		                         payment.setValue({fieldId: 'subsidiary', value: subsidiary});
		                         payment.setValue({fieldId: 'memo', value: 'Bank Payment Received ' + transactionType});
		                         payment.setValue({fieldId: 'location', value: location});
		                         payment.setValue({fieldId: 'aracct', value: aracct});  
		                         payment.setValue({fieldId: 'undepfunds', value: unDeposited});
		                         payment.setValue({fieldId: 'checknum', value: checkNumber});
		                         payment.setValue({fieldId: 'custbody_clgx_bg_payment_file', value: filename});
		                         payment.setValue({fieldId: 'custbody_clgx_bg_payment_trans_id', value: batchItem.trim() + ' ' + remarks.trim()});
		                         payment.setValue({fieldId: 'custbody_clgx_payment_ci_num', value: invoiceReferences});
                                 payment.setValue({fieldId: 'autoapply', value: false});
		                         if (account > 0)
	                              {
	                                  payment.setValue({fieldId: 'account', value: account });  
	                              }
		                         paymentID = payment.save({enableSourcing: false,isDynamic: true,ignoreMandatoryFields: true});
		                         remainingCheckAmount = parseFloat(remainingCheckAmount).toFixed(2) - parseFloat(paymentAmount).toFixed(2);
		                         
		                       	 remainingAmount = parseFloat(paymentAmount).toFixed(2);
                                 customerBillPayment = record.load({type: 'customerpayment',id: paymentID,isDynamic: true});
                                 
                                 for (var i = 0; i < filteredInvoices.length; i++) {
                                       var linePay = customerBillPayment.findSublistLineWithValue({sublistId: 'apply',fieldId: 'internalid',value: filteredInvoices[i].invoice_id});
                                       var lineNum = customerBillPayment.selectLine({sublistId: 'apply',line: linePay});
                                       var amt = (parseFloat(filteredInvoices[i].invoice_amount)).toFixed(2);

                                       if (remainingAmount > 0 && amt > 0) {
                                           var invoicePayAmount = parseFloat(Math.min(remainingAmount, amt)).toFixed(2);
                                           customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'apply',line: lineNum,value: true});
                                           customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'doc',line: lineNum,value: filteredInvoices[i].invoice_id});
                                           customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'amount',line: lineNum,value: invoicePayAmount});
                                           customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'refnum',line: lineNum,value: filteredInvoices[i].invoice_id});
                                           customerBillPayment.setCurrentSublistValue({sublistId: 'apply',fieldId: 'applydate',line: lineNum,value: new Date(tranDate)});
                                           customerBillPayment.commitLine({sublistId: 'apply',line: lineNum});

                                           remainingAmount = parseFloat(remainingAmount - invoicePayAmount).toFixed(2);
                                       }//if (remainingAmount > 0 && amt > 0)

                                   } //for (var i = 0; i < filteredInvoices.length; i++) 
                                  customerBillPayment.save();
                                   deposits.push({
                                       'batch': batchID,
                                       'paymentID': paymentID,
                                       'amount': parseFloat(paymentAmount),
                                       'depositDate': new Date(tranDate),
                                       'location': parseInt(location),
                                       'subsidiary': parseInt(subsidiary)
                                     });
								  }//if (paymentAmount > 0){
							  	}//for (var c = 0; c < customerIDs.length; c++) 
							 if (remainingCheckAmount > 0)
							 {
								 customer = existingContent[k].customer;
								 currencyID = parseInt(existingContent[k].currency);
								 paymentAmount = remainingCheckAmount;
								 payment = record.create({type: 'customerpayment'});
		                         payment.setValue({fieldId: 'currency', value: currencyID});
		                         payment.setValue({fieldId: 'exchangerate', value: 1});
		                         payment.setValue({fieldId: 'customer', value: customer});
		                         payment.setValue({fieldId: 'payment', value: parseFloat(paymentAmount).toFixed(2)});
		                         payment.setValue({fieldId: 'trandate', value: new Date(tranDate)});
		                         payment.setValue({fieldId: 'subsidiary', value: subsidiary});
		                         payment.setValue({fieldId: 'memo', value: 'Bank Payment Received ' + transactionType});
		                         payment.setValue({fieldId: 'location', value: location});
		                         payment.setValue({fieldId: 'aracct', value: aracct});  
		                         payment.setValue({fieldId: 'undepfunds', value: unDeposited});
		                         payment.setValue({fieldId: 'checknum', value: checkNumber});
		                         payment.setValue({fieldId: 'custbody_clgx_bg_payment_file', value: filename});
		                         payment.setValue({fieldId: 'custbody_clgx_bg_payment_trans_id', value: batchItem.trim() + ' ' + remarks.trim()});
		                         payment.setValue({fieldId: 'custbody_clgx_payment_ci_num', value: invoiceReferences});
                                 payment.setValue({fieldId: 'autoapply', value: false});
		                         if (account > 0)
	                              {
	                                  payment.setValue({fieldId: 'account', value: account });  
	                              }
		                         paymentID = payment.save({enableSourcing: false,isDynamic: true,ignoreMandatoryFields: true});
		                         deposits.push({
                                     'batch': batchID,
                                     'paymentID': paymentID,
                                     'amount': parseFloat(paymentAmount),
                                     'depositDate': new Date(tranDate),
                                     'location': parseInt(location),
                                     'subsidiary': parseInt(subsidiary)
                                   });
							 }
							}//else if (custCount > 1 && applyToInvoices == true )
	
					    } //if (consolidated.length > 0)     
                                    


					    //Customer is set to unassigned, Payment goes to Review
                        if (match == false) 
                        {
                        	subsidiary = paramObject.subsidiary;
                      	  	location = paramObject.location;
                      	  	currencyID = parseInt(existingContent[k].currency);
                      	  if (transactionType == 'WIR' || transactionType == 'ACH')
                      	  {
							  account = existingContent[k].pay_acct;
							  location = existingContent[k].acct_location;
							  unDeposited = 'F';
	                       }

                        	
							var customer = existingContent[k].customer;
							
                        	paymentAmount = totalCheckAmount;
                            payment = record.create({type: 'customerpayment'});
                            payment.setValue({fieldId: 'currency', value: currencyID});
                            payment.setValue({fieldId: 'exchangerate', value: 1});
                            payment.setValue({fieldId: 'customer', value: customer});
                            payment.setValue({fieldId: 'payment', value: parseFloat(paymentAmount).toFixed(2)});
                            payment.setValue({fieldId: 'trandate', value: new Date(tranDate)});
                            payment.setValue({fieldId: 'subsidiary', value: subsidiary});
                            payment.setValue({fieldId: 'memo', value: 'Bank Payment Received ' + transactionType});
                            payment.setValue({fieldId: 'location', value: location});
                            payment.setValue({fieldId: 'aracct', value: aracct});
                            payment.setValue({fieldId: 'undepfunds', value: unDeposited});
                            payment.setValue({fieldId: 'checknum', value: checkNumber});
                            payment.setValue({fieldId: 'custbody_clgx_bg_payment_file', value: filename});
                            payment.setValue({fieldId: 'custbody_clgx_bg_payment_trans_id', value: batchItem.trim() + ' ' + remarks.trim()});
                            payment.setValue({fieldId: 'custbody_clgx_payment_ci_num', value: invoiceReferences});
                            payment.setValue({fieldId: 'autoapply', value: false});
                            if (account > 0)
                            {
                                payment.setValue({fieldId: 'account', value: account });  
                            }
                            paymentID = payment.save();
                            deposits.push({
                                'batch': batchID,
                                'paymentID': paymentID,
                                'amount': parseFloat(paymentAmount),
                                'depositDate': new Date(tranDate),
                                'location': parseInt(location),
                                'subsidiary': parseInt(subsidiary)
                              });
                        }//if (match == false) 
                        
    					
						
                        
                    }//for (var k = beginIndex; k < existingContent.length; k++)
                }//if (existingContent.length > 0){

                if (usage <= 200 && existingContent.length > k) 
                {
                    var depositFile = 'DepositData.json';
                    var fileDepositObject = file.create({
                        name: depositFile,
                        fileType: file.Type.PLAINTEXT,
                       folder: 13974528,
                       contents: JSON.stringify(deposits)
                    });
                     depfileID = fileDepositObject.save();


                    return true; //Reschedule
                } 
                else 
                {
                    //TODO make deposits from deposit objects
                    //createDepositRecords(deposits);

                	var html = "Payments were parsed from " + bankName + " bank file for Deposit Date " + tranDate +  "<br/><br/>";
                	var batchID;
                	var batches = _.uniq(_.map(deposits, 'batch'));
                	var grandTotal = 0;
                	var grandCount = 0;
                	for (var b = 0; b < batches.length; b++) 
					  {
						  
                		batchID = batches[b];
						  filteredDeposits = _.filter(deposits, function(o) {
								return o.batch == batchID;
							});
						  var totalBatchAmount = filteredDeposits.reduce(function (prev, cur) {
	                             return (parseFloat(prev)) + (parseFloat(cur.amount));
	                         }, 0);
						  var count = filteredDeposits.length;
						  html = html +  "Batch: " + batchID + " Batch Amount: $" + parseFloat(totalBatchAmount).toFixed(2) + " Payment Count: " + count + "<br/>";
						  grandTotal = grandTotal + totalBatchAmount;
						  grandCount = grandCount + count;
						  for (var f = 0; f < filteredDeposits.length; f++)
						  {
							  html = html + "Batch: " + batchID +  " PaymentID #: " + filteredDeposits[f].paymentID  + " Amount: $" + parseFloat(filteredDeposits[f].amount).toFixed(2) + "<br/>";
						  }
						  html = html + "<br/>";
					  }
					var emails = [];
					var results = search.load({ type: search.Type.EMPLOYEE, id: "customsearch_clgx2_rl_jpm_bai2_emails" });
					results.run().each(function(result) {
						emails.push(parseInt(result.getValue(result.columns[0])));
						return true;
					});
		
					sendEmail({
						recipients : emails,
	        			subject: tranDate + " " + bankName + " Payments Parsed",
	        			body:  html  + "Total Payment Count: " + grandCount + " Total Payment Amount : $" + parseFloat(grandTotal).toFixed(2) +  "<br/><br/>" });

                	
                    return false;
                }
            } catch (ex) 
            {
                log.debug({title: "createPaymentRecords - Error", details: ex});
            }

            return false;
        }

        
        
        function sendEmail(paramObject) {
        	if(paramObject) {
		
        		var currentUser = runtime.getCurrentUser();
        		paramObject.recipients.push(currentUser.id);
        		paramObject.recipients = _.uniq(paramObject.recipients);
		
        		email.send({
        			author    : currentUser.id,
        			recipients: paramObject.recipients,
        			subject   : paramObject.subject,
        			body      : paramObject.body
        		});
        	}
        }
        /**
         * Schedules a script.
         *
         * @access private
         * @function scheduleScript
         *
         * @libraries N/task
         *
         * @param {Object} paramObject
         * @returns void
         */
        function scheduleScript(paramObject) {
            var scriptTask = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
            scriptTask.scriptId = runtime.getCurrentScript().id;
            scriptTask.deploymentId = paramObject.deploymentID;

            scriptTask.params = {
                custscript_clgx2_1878_current_index: paramObject.currentIndex,
                custscript_clgx2_1878_savedsearch: paramObject.savedSearch,
                custscript_clgx2_1878_fileid: paramObject.fileID,
                custscript_clgx2_1878_currencyid: paramObject.currencyID,
                custscript_clgx2_1878_subsidiary: paramObject.subsidiary,
                custscript_clgx2_1878_location: paramObject.location,
                custscript_clgx2_1878_customer: paramObject.customer,
                custscript_clgx2_1878_aracct: paramObject.aracct,
                custscript_clgx2_1878_depositfileid: paramObject.depositfileID,
            };
            var scriptTaskId = scriptTask.submit();
        }






//TODO
//function createDepositRecords(deposits) {
//           log.debug({
//               title: 'createDepositRecords',
//               details: deposits
//           });
//
//           try {
//           	var payments = [];
//               var depositDate = moment().format('M/D/YYYY');
//               var currency = 1;
//               if (deposits.length > 0) {
//                   //get each batch for a single deposit record
//                   var batches = _.uniq(_.map(deposits, 'batch'));
//
//                   for (var i = 0; i < batches.length; i++) {
//                       //get total of all payments for batch deposit header
////                       var totalDepositAmount = deposits.reduce(function (prev, cur) {
////                           return cur.batch == batches[i] ?
////                               (parseFloat(prev)) + (parseFloat(cur.amount)) : 0;
////                       }, 0);
//
//                   	payments = [];
//                       for (var j = 0; j < deposits.length; j++) {
//
//                           if (parseInt(deposits[j].batch) == parseInt(batches[i])) {
//                               //all payment info for Deposit line items
//                               payments.push({
//                                   'paymentID': deposits[j].paymentID,
//                                   'amount': parseFloat(deposits[j].amount),
//                                   'depositDate': deposits[j].depositDate,
//                                   'location': deposits[j].location,
//                                   'subsidiary': deposits[j].subsidiary,
//                                   'batch': deposits[j].batch
//                               });
//                               depositDate = deposits[j].depositDate;
//                           }//if (deposits[j].batch == batches[i]){
//                       }//for (var j = 0; j < deposits.length; j++)
//
//                       //can only create a deposit if there are paymentIDs to record on the sublist: payment
//                      if (payments.length > 0) {
//                           log.debug({title: 'payments', details: payments});
//
//
//                           var myDeposit = record.create({type: 'deposit'});
//                           myDeposit.setValue({fieldId: 'account', value: parseInt(519)});
//                           myDeposit.setValue({fieldId: 'currency', value: 1});
//                           myDeposit.setValue({fieldId: 'trandate', value: new Date()});
//                           myDeposit.setValue({fieldId: 'location', value: parseInt(53)});
//                           myDeposit.setValue({fieldId: 'subsidiary', value: parseInt(5)});
//                           log.debug({title: 'myDeposit', details: myDeposit});
//
//                           var count = myDeposit.getLineCount({ sublistId: 'payment' });
//                           log.debug({title: 'count', details: count});
//
//                           for (var g = 1; g <= 10; g++){
//                           	for (var k = 0; k < payments.length; k++) {
//                           		 log.debug({title: 'payments', details: payments[k]});
//                           		 log.debug({title: 'payments', details: payments[k].paymentID});
//
//                           		if (myDeposit.getSublistValue('payment', 'id', g) == payments[k].paymentID)
//                           		{
//                           			myDeposit.setSublistValue('payment','deposit',g, true);
//                           		}
//                           	}//for (var k = 0; k < payments.length; k++) {
//                           }//for (var g = 1; g <= count; g++){
//
//                           myDeposit.save();
//                           log.debug({title: 'myDeposit', details: JSON.stringify(myDeposit)});
//
//                     }//if (payments.length > 0)
//
//                   }//for (var i = 0; i < batches.length; i++)
//
//               } //if (deposits.length > 0){
//
//
//           }//try
//
//           catch (ex) {
//               log.debug({title: "createDepositRecords - Error", details: ex});
//           }
//
//       }//function createDepositRecords(deposits){


        return {
            execute: execute
        };

    });
