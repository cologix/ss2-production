/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * 
 * @author Liz Soucy liz.soucy@cologix.com
 * @date 3/13/2020
 * 
 * @ExecutionContext Client (Edit)
 */
define(["/SuiteScripts/clgx/libraries/moment.min", "N/format", "/SuiteScripts/clgx/libraries/lodash.min"],
function(moment, format, _) {
	var payAccount;
    function fieldChanged(context) {
    	try {
    		if(context.fieldId == "customer") 
    		{
        		var cr  = context.currentRecord;
        		var memo = cr.getValue('memo');
        		var undeposited = cr.getValue('undepfunds');
        	    if (memo.search('Bank') > -1 && undeposited == 'F') 
        		{
        		payAccount = cr.getValue("account");
        		}
    		}
    	} catch(ex) {
    		log.error({ title: "fieldChanged - Error", details: ex });
    	}
    }
    
    function postSourcing(context) {
    	var currentRecord = context.currentRecord;
    	if(context.fieldId == "customer") 
    	{
    		log.debug({ title: "postSourcing", details: context });
    		var memo = currentRecord.getValue('memo');
    		var undeposited = currentRecord.getValue('undepfunds');
    	    if (memo.search('Bank') > -1 && undeposited == 'F')
    		{
    			currentRecord.setValue({ fieldId: "account", value: payAccount });
    		}
    	
    	}
    	
    }
    
    return {
        fieldChanged: fieldChanged,
        postSourcing: postSourcing
    };
    
});
