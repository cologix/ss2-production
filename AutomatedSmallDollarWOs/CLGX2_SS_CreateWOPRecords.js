/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/task'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {task} task
 */
function(record, runtime, search, task) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(context) {
		var rt = runtime.getCurrentScript();
    	
    	var paramObject           = new Object();
    	paramObject.actionType    = rt.getParameter("custscript_clgx_1902_at");
    	log.debug({title: 'Action Type', details: paramObject.actionType});
    	
    	if(paramObject.actionType == 'refresh'){
    		deleteWOPs();
    		createWOPs();
    	}
    	
    }
    
    function deleteWOPs(){
    	var savedSearchID = "customsearch_clgx_wop_queue_unpro";
    	
    	var usage = runtime.getCurrentScript().getRemainingUsage();
		
		var searchObject = search.load({ id: savedSearchID });
		searchObject.run().each(function(result) {
			var wopRecord = result.getValue("internalid");
			record.delete({ type: 'customrecord_clgx_write_off_proposal', id: wopRecord });
			
			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 500) {
				return false;
			}//Reschedule
			
			return true;
		});
    }
    
    function createWOPs(){
   		var savedSearchID = "customsearch_clgx_wop_create_queue";
		
		var usage = runtime.getCurrentScript().getRemainingUsage();
		
		var searchObject = search.load({ id: savedSearchID });
		searchObject.run().each(function(result) {
			var recType = result.getValue("type");
			log.debug({title: 'Type', details: recType});
			if(recType == 'CustInvc'){
				var invoice = parseInt(result.getValue("internalid"));
				var wopID	= createUnderPayWOP(result);
			} else if(recType == 'CustPymt'){
				var payment = parseInt(result.getValue("internalid"));
				var wopID	= createOverPayWOP(result);
			}
			
			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 500) {
				return false;
			}//Reschedule
			
			return true;
		});
    }
		
    function createOverPayWOP(resultObject){
    	
    	var recordObject = record.create({ type: 'customrecord_clgx_write_off_proposal', isDynamic: true });
  	
    	
    	
    	recordObject.setValue({fieldId:"custrecord_clgx_wo_cust_name", value: resultObject.getValue(resultObject.columns[2]) });
    	log.debug({title: 'Column Value', details: resultObject.getValue(resultObject.columns[2])});
    	recordObject.setValue({fieldId:"custrecord_clgx_wop_over", value: true});
    	recordObject.setValue({fieldId:"custrecord_clgx_wop_crm_location", value: resultObject.getValue('location')});
    	recordObject.setValue({fieldId:"custrecord_clgx_wo_native_inv", value: resultObject.getValue('internalid')});
    	recordObject.setValue({fieldId:"custrecord_clgx_wo_balance", value: -Math.abs(resultObject.getValue('fxamountremaining'))});
    	recordObject.setValue({fieldId:"custrecord_clgx_wop_customer_sub", value: resultObject.getValue('subsidiary')});
    	
    	return recordObject.save({ enableSourcing: true, ignoreMandatoryFields: false });
    
    }
    
    function createUnderPayWOP(resultObject){
    	var recordObject = record.create({ type: 'customrecord_clgx_write_off_proposal', isDynamic: true });
    	
    	
    	recordObject.setValue({fieldId:"custrecord_clgx_wo_cust_name", value: resultObject.getValue(resultObject.columns[2]) })
    	log.debug({title: 'Column Value', details: resultObject.getValue(resultObject.columns[2])});
    	recordObject.setValue({fieldId:"custrecord_clgx_wop_under", value: true});
    	recordObject.setValue({fieldId:"custrecord_clgx_wo_balance", value: resultObject.getValue('fxamountremaining')});
    	recordObject.setValue({fieldId:"custrecord_clgx_wo_native_inv", value: resultObject.getValue('internalid')});
    	recordObject.setValue({fieldId:"custrecord_clgx_wo_ci", value: resultObject.getValue('custbody_consolidate_inv_cinvoice')});
    	recordObject.setValue({fieldId:"custrecord_clgx_wop_crm_location", value: resultObject.getValue('location')});
    	
    	return recordObject.save({ enableSourcing: true, ignoreMandatoryFields: false });
    }

    return {
        execute: execute
    };
    
});
