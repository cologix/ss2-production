/**
 * @author Katy Jones - katy.jones@cologix.com
 * @date 9/12/2019
 */
define(["N/search", "/SuiteScripts/clgx/libraries/lodash.min", "N/task", "N/email", "N/runtime", "N/record", "/SuiteScripts/clgx/libraries/moment.min"],
function(search, _, task, email, runtime, record, moment) {
	/**
     * Returns a JSON string of queue records.
     * 
     * @access public
     * @function getQueueRecords
     * 
     * @param {string} searchID
     * @return {Object}
     */
    function getQueueRecords(searchID, filterArray) {
    	try {
    		var filters = new Array();

    		var finalObject  = new Array();
    		var searchObject = search.load({ id: searchID });
    		
    		if(filterArray) {
    			_.forEach(filterArray, function(object, key) {
    				searchObject.filters.push(search.createFilter({ name: object.name, operator: object.operator, values: [parseInt(object.value)] }));
    			});
    		}
    		
    		searchObject.run().each(function(result) {
    			finalObject.push({
    				selected           		: 0,
    				queueid            		: parseInt(result.getValue("internalid")),
    				consolidatedinvoice  	: result.getText("custrecord_clgx_wo_ci"),
    				invoice       			: result.getText("custrecord_clgx_wo_native_inv"),
    				customer    			: result.getText("custrecord_clgx_wo_cust_name"),
    				balance  				: parseFloat(result.getValue("custrecord_clgx_wo_balance")).toFixed(2),
    				date					: result.getValue("custrecord_clgx_wop_trans_date")
    			});
    			
    			return true;
    		});
    		
    		return finalObject;
    	} catch(ex) {
    		log.error({ title: "getQueueRecords - Error", details: ex });
    	}
    }
    
    
    /**
     * Returns a single record as an object.
     * 
     * @access public
     * @function getQueueRecord
     * 
     * @param {Number} recordID
     * @return {Object}
     */
    function getQueueRecord(recordID) {
    	if(recordID) {
    		var recordObject = record.load({ type: "customrecord_clgx_write_off_proposal", id: recordID });
    		
    		return {
				selected           		: 0,
				queueid            		: recordID,
				consolidatedinvoice  	: result.getText("custrecord_clgx_wo_ci"),
				invoice       			: result.getText("custrecord_clgx_wo_native_inv"),
				customer    			: result.getText("custrecord_clgx_wo_cust_name"),
				balance  				: parseFloat(result.getValue("custrecord_clgx_wo_balance")).toFixed(2),
				date					: result.getValue("custrecord_clgx_wop_trans_date")
    		};
    	}
    }
    
    function updateObjectProperty(paramObject) {
    	if(paramObject) {
    		if(paramObject.records.length != null) {
    			_.forEach(paramObject.records, function(value, key) {
            		paramObject.records[key][paramObject.property] = paramObject.value;
            	});
    		} else {
            	paramObject.records[paramObject.property] = paramObject.value;
    		}
    		
    		return paramObject.records;
    	}
    }
    
    function scheduleScript(paramObject) {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = paramObject.scriptId;
    		t.deploymentId = paramObject.deploymentId;
    		t.params       = paramObject.params;
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }
    
    function getBufferedQueueRecords(searchID, filterArray, pageObject) {
    	try {
    		var paramObject = new Object();
    		paramObject.currentIndex = 0;
    		paramObject.pageSize     = 1000;
    		
    		var filters = new Array();

    		var finalObject  = new Array();
    		var searchObject = search.load({ id: searchID });
    		
    		if(filterArray) {
    			_.forEach(filterArray, function(object, key) {
    				searchObject.filters.push(search.createFilter({ name: object.name, operator: object.operator, values: [parseInt(object.value)] }));
    			});
    		}
    		
    		searchObject = searchObject.run();
    		
    		var searchResults = searchObject.getRange({ start: 0, end: 1000 });
    		processResults    = processResultSet(paramObject, searchResults);
    		finalObject       = processResults.ids;
    		
        	while (processResults.nextPage == true) {
        		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
        		processResults = processResultSet(paramObject, searchResults);
        		finalObject    = finalObject.concat(processResults.ids);
        	}
    		
    		return finalObject;
    	} catch(ex) {
    		log.error({ title: "getBufferedQueueRecords - Error", details: ex });
    	}
    }
    
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var ids          = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		ids.push({
				selected           : 0,
				queueid            : parseInt(searchResults[r].getValue("internalid")),
				consolidatedinvoice  : searchResults[r].getText("custrecord_clgx_wo_ci"),
				invoice       : searchResults[r].getText("custrecord_clgx_wo_native_inv"),
				customer    : searchResults[r].getText("custrecord_clgx_wo_cust_name"),
				balance  : parseFloat(searchResults[r].getValue("custrecord_clgx_wo_balance")).toFixed(2),
				date					: searchResults[r].getValue("custrecord_clgx_wop_trans_date")
			});
    		
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }
    
    return {
    	getQueueRecords: getQueueRecords,
    	getQueueRecord: getQueueRecord,
    	updateObjectProperty: updateObjectProperty,
    	scheduleScript: scheduleScript,
    	getBufferedQueueRecords: getBufferedQueueRecords,
    	processResultSet: processResultSet
    	}
});