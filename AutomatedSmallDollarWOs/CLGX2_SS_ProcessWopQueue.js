/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Katy Jones - katy.jones@cologix.com
 * @date 12/20/2019
 */
define(['N/record', 'N/runtime', 'N/search', 'SuiteScripts/clgx/AutomatedSmallDollarWOs/lib/CLGX2_WOP_Lib', "/SuiteScripts/clgx/libraries/lodash.min",	"/SuiteScripts/clgx/libraries/moment.min"],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {lib} lib
 * @param {_} _
 * @param {moment} moment
 */
function(record, runtime, search, lib, _, moment) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(context) {
		var rt = runtime.getCurrentScript();
    	
    	var paramObject           = new Object();
    	paramObject.savedSearchID = "";
    	paramObject.actionType    = rt.getParameter("custscript_clgx_1903_at");
    	paramObject.queueIDs      = rt.getParameter("custscript_clgx_1903_qids") || null;
    	
    	if(paramObject.actionType == "writeoff"){
    		markWriteOffs(paramObject);		
    		createWriteOff();
    	}
    	
    	function markWriteOffs(paramObject){
    	if(paramObject && paramObject.queueIDs !== undefined) {
    		var queueRecords = JSON.parse(paramObject.queueIDs);
    		var queueCount   = queueRecords.length;
    		
    			for(var q = 0; q < queueCount; q++) {
    				record.submitFields({ type: "customrecord_clgx_write_off_proposal", id: queueRecords[q], values: {custrecord_clgx_wo_approve : true } });
    			}
    		}
    	}
        
        function createWriteOff(){
       		var savedSearchID = "customsearch_clgx_wop_write_off_je";
    		var journalEntryLocation = [];
    		var finalJEGrouping = [];
    		var usage = runtime.getCurrentScript().getRemainingUsage();
    		
    		var searchObject = search.load({ id: savedSearchID });
    		searchObject.run().each(function(result) {
     			
    			var writeOffObject = {
    					wop_id        : parseInt(result.getValue("internalid")),
            			subsidiary    : result.getValue("custrecord_clgx_wop_customer_sub"),
            			customer	  : result.getValue("custrecord_clgx_wo_cust_name"),
            			amount        : result.getValue("custrecord_clgx_wo_balance"),
            			memo          : "Small Dollar Write Off",
            			location      : parseInt(result.getValue("custrecord_clgx_wop_crm_location")),
            			origintran    : result.getValue("custrecord_clgx_wo_native_inv"),
            			tranmemo	  : result.getText("custrecord_clgx_wo_native_inv")
            			
    			}
    			
    			log.debug({title: 'Write Off Object', details: writeOffObject});
    			
    			journalEntryLocation.push(writeOffObject);
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 500) {
    				return false;
    			}//Reschedule
    			
    			
    			return true;
    		});
    		
			log.debug({title: 'JE Array', details: journalEntryLocation});
    		
        	var groupBySubsidiary = _.groupBy(journalEntryLocation, 'subsidiary');
        	log.debug({title: 'Group By Subsidiary', details: groupBySubsidiary})
        	
        	_.each(groupBySubsidiary, function(value, key){
        		var locationProcessing = [];
        		
        		for(var i=0; i<value.length; i++){
        			var locationObj = value[i];
        			locationProcessing.push(locationObj);
        		}
        		finalJEGrouping.push(locationProcessing);
        		return true;
        	});
        	
        	for(var i = 0; i<finalJEGrouping.length; i++){
        		var journal = createJE(finalJEGrouping[i]);
        		
        		var groupByWop = _.groupBy(finalJEGrouping[i], 'wop_id');
        		log.debug({title: 'Grouped by Customer', details: groupByWop});
        		
        		var paymentData = Object.keys(groupByWop).map(function(key) {
		    		return [String(key), groupByWop[key]];
		    	});
        		
        		log.debug({title: 'Payment Data length', details: paymentData.length});
        		for(j=0; j<paymentData.length; j++){
        			var indPaymentData = paymentData[j];
        			log.debug({title: 'indPaymentData', details: indPaymentData});
        			var paymentProcessIndex = indPaymentData[1];
        			log.debug({title: 'paymentProcessIndex', details: paymentProcessIndex});
        			var appliedWriteOff = applyWriteOff(paymentProcessIndex, journal);
        			log.debug({title: "Applied Payment", details: appliedWriteOff});
        			log.debug({title: "Payment I Length", details: paymentProcessIndex.length});
        			try{
	        			for(k=0; k<paymentProcessIndex.length; k++){
	        				log.debug({title: "WOP ID", details: paymentProcessIndex[k].wop_id});
	        				record.submitFields({ type: "customrecord_clgx_write_off_proposal", id: paymentProcessIndex[k].wop_id, values: { custrecord_clgx_wop_processed : true } });
	        			}
        			} catch(ex){
                		log.error({ title: "updateWOPWithPaymentID- error", details: ex });
        			}

        		}
        		
        	}

        }
        
        function createJE(journalObject){
 
        		var journal = record.create({ type: "journalentry", isDynamic: true});
        		
        		journal.setValue({ fieldId: "exchangerate",                  value:  "1"});
        		journal.setValue({ fieldId: "trandate",                      value:  new Date() });
        		journal.setValue({ fieldId: "custbody_clgx_inservice_entry", value:  true});
        		journal.setValue({ fieldId: "subsidiary",                    value:  journalObject[0].subsidiary});
        		
        		
        	
//create 11000 account line
        	for(var i=0; i<journalObject.length; i++){
        		var lineMemo = journalObject[i].memo + ' ' + journalObject[i].tranmemo;
        		
        		journal.selectNewLine({ sublistId: "line"});
        		journal.setCurrentSublistValue({ sublistId: "line", fieldId: "account",                         value:  122});
        		journal.setCurrentSublistValue({ sublistId: "line", fieldId: "debit",                           value:  null });
	        	journal.setCurrentSublistValue({ sublistId: "line", fieldId: "credit",                          value:  journalObject[i].amount });
        		journal.setCurrentSublistValue({ sublistId: "line", fieldId: "memo",                            value:  lineMemo});
        		journal.setCurrentSublistValue({ sublistId: "line", fieldId: "entity",                          value:  journalObject[i].customer});
        		journal.setCurrentSublistValue({ sublistId: "line", fieldId: "department",                      value:  6});
        		journal.setCurrentSublistValue({ sublistId: "line", fieldId: "class",                           value:  1});
        		journal.setCurrentSublistValue({ sublistId: "line", fieldId: "location",                       	value:  journalObject[i].location});
        		journal.commitLine({ sublistId: "line" });	        		     	
        	
        	
//create 11001 account line
				journal.selectNewLine({ sublistId: "line"});
				journal.setCurrentSublistValue({ sublistId: "line", fieldId: "account",                         value:  228});
				journal.setCurrentSublistValue({ sublistId: "line", fieldId: "debit",                           value:  journalObject[i].amount });
				journal.setCurrentSublistValue({ sublistId: "line", fieldId: "credit",                          value:  null });
				journal.setCurrentSublistValue({ sublistId: "line", fieldId: "memo",                            value:  lineMemo});
				journal.setCurrentSublistValue({ sublistId: "line", fieldId: "department",                      value:  6});
				journal.setCurrentSublistValue({ sublistId: "line", fieldId: "class",                           value:  1});
				journal.setCurrentSublistValue({ sublistId: "line", fieldId: "location",                        value:  journalObject[i].location});
				journal.commitLine({ sublistId: "line" });	
			}
        	
        	journal.save();
        	
        	var journalId = journal.id;
        	
//update WOP records with JE ID        	
        	for(var i=0; i<journalObject.length; i++){
        		try {
            		record.submitFields({ type: "customrecord_clgx_write_off_proposal", id: journalObject[i].wop_id, values: {  custrecord_clgx_wop_je : journalId, custrecord_clgx_wop_je_processed: true } });
            	} catch(ex) {
            		log.error({ title: "updateWOPRecordOverPay - Error", details: ex });
            	}
        	}
        	
        	return journalId;

        }
        
        function applyWriteOff(paymentObject, journal){
        	var payment = record.create({ type: record.Type.CUSTOMER_PAYMENT, isDynamic: true});
        	log.debug({title: 'Payment Object', details: paymentObject});
        	log.debug({title: 'Journal Id', details: journal});
        	var creditArray = new Array();
        	var applyArray = new Array();
        	var runningAmount = 0;
        	
        	payment.setValue({ fieldId: "trandate",                      value:  new Date() });
        	payment.setValue({ fieldId: "customer",                      value:  paymentObject[0].customer });
        	log.debug({title: 'Customer', details: paymentObject[0].customer});
        	payment.setValue({ fieldId: "location",                      value:  paymentObject[0].location });     	
        	payment.setValue({ fieldId: "undepfunds",                    value:  'T' });  
        	payment.setValue({ fieldId: "autoapply",					 value: false});
        	log.debug({title: 'Location', details: paymentObject[0].location}); 
        	
        	log.debug({title: 'Payment Object Length', details: paymentObject.length});
        	for(i=0; i<paymentObject.length; i++){
            	log.debug({title: 'Payment Amount', details: paymentObject[i].amount});
        		if(paymentObject[i].amount < 0){
        			creditArray.push(paymentObject[i].origintran);
        			applyArray.push(journal);
        		} else if (paymentObject[i].amount > 0) {
        			creditArray.push(journal);
        			applyArray.push(paymentObject[i].origintran);
        		}	
        	}     	
        	
        	try{
	        	for(i=0; i<creditArray.length; i++){
		        	var applyLine = payment.findSublistLineWithValue({
		    			sublistId: 'credit',
		    			fieldId: 'internalid',
		    			value: creditArray[i]
		    		});
		    		
		    		log.debug({title: "Credit Line", details: applyLine});
		    		
		    		payment.selectLine({ sublistId: 'credit', line: applyLine });
		    		payment.setCurrentSublistValue({ sublistId: 'credit', fieldId: 'apply', value: true });
		    		payment.commitLine({ sublistId: 'credit' });
	        	}
        	} catch(ex){
        		log.error({ title: "creditApply- error", details: ex });
        	}
        	try{
	        	for(i=0; i<applyArray.length; i++){
		        	var applyLine = payment.findSublistLineWithValue({
		    			sublistId: 'apply',
		    			fieldId: 'internalid',
		    			value: applyArray[i]
		    		});
		    		
		    		log.debug({title: "Apply Line", details: applyLine});
		    		
		    		payment.selectLine({ sublistId: 'apply', line: applyLine });
		    		payment.setCurrentSublistValue({ sublistId: 'apply', fieldId: 'apply', value: true });
		    		payment.commitLine({ sublistId: 'apply' });
	        	}        
        	} catch(ex){
        		log.error({ title: "apply- error", details: ex });
        	}
        		
        	var paymentId = payment.save();
        	log.debug({title: 'Payment ID', details: paymentId});
        	
        	return paymentId;
        	
        }
    	
    }

    return {
        execute: execute
    };
    
});
