/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/file', 'SuiteScripts/clgx/AutomatedSmallDollarWOs/lib/CLGX2_WOP_Lib'],
/**
 * @param {file} file
 */
function(file, lib) {
	    function onRequest(context) {
	    	try {
	    		var fileObject = file.load({ id: 24711566});
				var fileHtml   = fileObject.getContents();
											
				context.response.write(fileHtml);
	    	} catch(ex) {
	    		log.error({ title: "onRequest - Error", details: ex });
	    	}
	    }

    return {
        onRequest: onRequest
    };
    
});
