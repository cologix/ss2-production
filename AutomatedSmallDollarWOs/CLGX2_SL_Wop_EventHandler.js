/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'SuiteScripts/clgx/AutomatedSmallDollarWOs/lib/CLGX2_WOP_Lib'],
/**
 * @param {record} record
 * @param {search} search
 */
function(record, search, lib) {
    function onRequest(context) {
    	try {
    		var ro = {};
        	ro.response   = context.response;
    		ro.request    = context.request;
    		ro.eventType  = ro.request.parameters.event    || null;
    		ro.actionType = ro.request.parameters.action   || null;
    		ro.data       = ro.request.parameters.data     || null;
    		ro.date       = ro.request.parameters.date     || null;
    		ro.tranid     = ro.request.parameters.tranid   || null;
    		ro.parentid   = ro.request.parameters.parentid || null;
    		ro.mergeids   = ro.request.parameters.mergeids || null;
    		
    		
    		if(ro.eventType == "get") {
    			if(ro.actionType == "unprocessedqueue") {
    				var recordArray = lib.getBufferedQueueRecords("customsearch_clgx_wop_queue_unpro");
    				ro.response.write(JSON.stringify(recordArray));
    			}
    		}
    		
			if(ro.actionType == "writeoff") {
				lib.scheduleScript({ scriptId: "1903", params: { custscript_clgx_1903_at: "writeoff", custscript_clgx_1903_qids: ro.data } });
				ro.response.write("Write-offs are being created now.");
			}
			
			if(ro.actionType == "refresh") {
				lib.scheduleScript({ scriptId: "1902", params: { custscript_clgx_1902_at: "refresh" } });
				ro.response.write("Queue is being refreshed.");
			}
    		
    		
    	} catch(ex) {
    		log.error({ title: "onRequest - Error", details: ex });
    	}
    }
    
    return {
        onRequest: onRequest
    };
    
});