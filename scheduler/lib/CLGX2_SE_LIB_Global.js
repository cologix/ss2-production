/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * 
 * @module CLGX/ScheduledScript
 * @date 9/20/2018
 */
define([],
function() {
	/**
	 * Delays script execution for the specified number of minutes.
	 * 
	 * @access private
	 * @function sleep
	 * 
	 * @libraries none
	 * 
	 * @param {Number} minutes [required] - The number of minutes expressed as a whole integer from 1 to 14.
	 * @returns null
	 */
	function sleep (minutes) {
		var start = new Date().getTime();
		while (new Date().getTime() < start + minutes * 60000);
	}
	
    return {
        time: {
        	sleep: sleep
        }
    };
    
});
