/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @module CLGX/SchedulerEngine
 * 
 * @date 9/20/2018
 */
define(["N/record", "N/search", "N/runtime", "N/task", "/SuiteScripts/clgx/scheduler/lib/CLGX2_SE_LIB_Global"],
function(record, search, runtime, task, global) {
    function execute(context) {
    	try {
    		var sc           = runtime.getCurrentScript();
    		var delay        = sc.getParameter("custscript_clgx2_se_delay");
    		var queueSearch  = sc.getParameter("custscript_clgx2_se_scheduler_search");
        	var searchObject = search.load({ id: queueSearch });
        	
        	searchObject.run().each(function(result) {
        		var recordObject = record.load({ type: "customrecord_cologix_general_queue", id: result.getValue("internalid") });
        		
        		scheduleScript({
        			scriptID    : result.getValue("custrecord_clgx_gq_script"),
        			deploymentID: result.getValue("custrecord_clgx_gq_script_deployment"),
        			params: result.getValue("custrecord_clgx_gq_json_structure")
        		});
        		
        		return true;
        	});
        	
        	global.time.sleep(delay);
        	scheduleScript({ scriptID: sc.id, deploymentID: sc.deploymentId });
    	} catch(ex) {
    		log.error({ title: "Execution Error", details: ex });
    	}
    }
    
    
    /**
	 * Schedules a script.
	 * 
	 * @access private
	 * @function scheduleScript
	 * 
	 * @libraries N/task
	 * 
	 * @param {Object} paramObject
	 * @returns void
	 */
	function scheduleScript(paramObject) {
		var scriptTask          = task.create({taskType: task.TaskType.SCHEDULED_SCRIPT});
    	scriptTask.scriptId     = paramObject.scriptID;
    	scriptTask.deploymentId = paramObject.deploymentID;
    	
    	if(paramObject.params !== undefined) { scriptTask.params = JSON.parse(paramObject.params); }
    	
    	scriptTask.submit();
	}
	
    return {
        execute: execute
    };
    
});
