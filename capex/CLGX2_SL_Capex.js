/**
 * 
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   9/1/2017
 * @description Testing various pieces of code for new functionalities. Displaying various temporary results. Small volume mass updates.
 * 
 * Script Name  : CLGX2_SL_Capex
 * Script File  : CLGX2_SL_Capex.js
 * Script ID    : customscript_clgx2_sl_capex
 * Deployment ID: customdeploy_clgx2_sl_capex
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1451&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, _, moment, dwg) {

    function onRequest(context) {
		
	    var response = context.response;
	    var request = context.request;
	    
	    var projects = get_projects();
	    var periods = get_periods();
	    var pos = get_pos();
	    var actuals = get_actuals();
	    
	    var projects_ids = _.merge(_.uniqBy(_.map(pos, 'project_id')), _.uniqBy(_.map(actuals, 'project_id')));

	    var projects_arr = []
	    for (var i = 0; i < projects_ids.length; i++ ) {
	    	
	    	var project = _.find(projects, function(o) { return o.project_id == projects_ids[i]; })
	    	
	    	var pos_project = _.filter(pos, function(o) { return o.project_id == projects_ids[i]; });
	    	var actuals_project = _.filter(actuals, function(o) { return o.project_id == projects_ids[i]; });
	    	
	    	var periods_ids = _.merge(_.uniqBy(_.map(pos_project, 'period_id')), _.uniqBy(_.map(actuals_project, 'period_id')));
	    	
	    	var project_periods = [];
	    	for (var j = 0; j < periods_ids.length; j++ ) {
	    		
	    		var amount_pos = 0;
	    		var amount_actuals = 0;
	    		
	    		var period = _.find(periods, function(o) { return o.period_id == periods_ids[j]; });
	    		var period_pos = _.find(pos, function(o) { return o.project_id == projects_ids[i] && o.period_id == periods_ids[j]; });
	    		if(period_pos){
	    			amount_pos = period_pos.amount;
	    		}
	    		var period_actuals = _.find(actuals, function(o) { return o.project_id == projects_ids[i] && o.period_id == periods_ids[j]; });
	    		if(period_actuals){
	    			amount_actuals = period_actuals.amount;
	    		}
	    		if(period_pos || period_actuals){
	    			project_periods.push({
		    			"period_id"      : period.period_id,
					"year"           : period.year,
					"nodeid"         : period.month_id,
					"node"           : period.month,
					"nodetype"       : "month",
					"project"        : project.project,
					"subsidiary"     : project.subsidiary,
					"facility"       : project.facility,
					"category"       : project.category,
					"amount_pos"     : amount_pos,
					"amount_actuals" : amount_actuals,
					"leaf"           : true,
					"faicon"         : "calendar-o"
		    			
		    		});
	    		}
	    	}
	    	
	    	var uniq_years = _.uniqBy(_.map(project_periods, 'year')).reverse();
	    	var years = [];
	    	for (var j = 0; j < uniq_years.length; j++ ) {
	    		var months = _.filter(project_periods, function(o) { return o.year == uniq_years[j]; }).reverse();
	    		
	    		//months = _.map(months, function(object) {
	    		//	  return _.pick(object, ['nodeid', 'node', 'nodetype', 'amount_pos', 'amount_actuals', 'leaf', 'faicon']);
	    		//});
	    		
	    		years.push({
	    			"nodeid"          : uniq_years[j],
	    			"node"            : uniq_years[j],
	    			"nodetype"        : "year",
				"project"         : project.project,
				"subsidiary"      : project.subsidiary,
				"facility"        : project.facility,
				"category"        : project.category,
				"amount_pos"      : round(_.sum(_.map(months, 'amount_pos'))),
				"amount_actuals"  : round(_.sum(_.map(months, 'amount_actuals'))),
				"expanded"        : false,
				"faicon"          : "calendar",
	    			"children"        : months
	    		});
	    	}
	    	
	    	projects_arr.push({
				"nodeid"             : project.project_id,
				"node"               : project.project,
				"nodetype"           : "project",
				"subsidiary_id"      : project.subsidiary_id,
				"subsidiary"         : project.subsidiary,
				"facility_id"        : project.facility_id,
				"facility"           : project.facility,
				"category_id"        : project.category_id,
				"category"           : project.category,
				"description"        : project.description,
				"start"              : project.start,
				"end"                : project.end,
				"amount_pos"         : round(_.sum(_.map(years, 'amount_pos'))),
				"amount_actuals"     : round(_.sum(_.map(years, 'amount_actuals'))),
				"expanded"           : false,
				"faicon"             : "gear",
				"children"           : years
			});

	    }
	    
	    var facilities_ids = _.uniqBy(_.map(projects_arr, 'facility_id'));
	    var facilities = [];
	    for (var i = 0; i < facilities_ids.length; i++ ) {
	    	var facility_projects = _.filter(projects_arr, function(o) { return o.facility_id == facilities_ids[i]; });
	    	facilities.push({
				"nodeid"             : facility_projects[0].facility_id,
				"node"               : facility_projects[0].facility,
				"nodetype"           : "facility",
				"subsidiary_id"      : facility_projects[0].subsidiary_id,
				"subsidiary"         : facility_projects[0].subsidiary,
				"amount_pos"         : round(_.sum(_.map(facility_projects, 'amount_pos'))),
				"amount_actuals"     : round(_.sum(_.map(facility_projects, 'amount_actuals'))),
				"expanded"           : false,
				"faicon"             : "building-o",
				"children"           : facility_projects
			});
	    }
	    facilities = _.sortBy(facilities, [function(o) { return o.node; }]);
	    
	    var subsidiaries_ids = _.uniqBy(_.map(facilities, 'subsidiary_id'));
	    var subsidiaries = [];
	    for (var i = 0; i < subsidiaries_ids.length; i++ ) {
	    	var subsidiaries_projects = _.filter(facilities, function(o) { return o.subsidiary_id == subsidiaries_ids[i]; });
	    	subsidiaries.push({
				"nodeid"             : subsidiaries_projects[0].subsidiary_id,
				"node"               : subsidiaries_projects[0].subsidiary,
				"nodetype"           : "subsidiary",
				"amount_pos"         : round(_.sum(_.map(subsidiaries_projects, 'amount_pos'))),
				"amount_actuals"     : round(_.sum(_.map(subsidiaries_projects, 'amount_actuals'))),
				"expanded"           : true,
				"faicon"             : "building",
				"children"           : subsidiaries_projects
			});
	    }
	    subsidiaries = _.sortBy(subsidiaries, [function(o) { return o.node; }]);
	    
	    var tree = {
	    		"text"      : ".",
	    		"children"  : subsidiaries
	    }
	    
	    //response.write(JSON.stringify(tree));
	    
		var fileObj = file.load({
		    id: 9017175
		});
		html = fileObj.getContents();
		html = html.replace(new RegExp('{dataProjects}','g'), JSON.stringify(tree));
		response.write( html );
		
    }
    return {
        onRequest: onRequest
    };
    
    function get_pos() {
    	
		var pos = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_pos" });
		results.run().each(function(result) {
			pos.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				//"project"            : result.getText(result.columns[0]),
				"period_id"          : parseInt(result.getValue(result.columns[1])),
				//"period"             : result.getText(result.columns[1]),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return pos;
	}
    
    function get_actuals() {
    	
		var actuals = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_actuals" });
		results.run().each(function(result) {
			actuals.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				//"project"            : result.getText(result.columns[0]),
				"period_id"          : parseInt(result.getValue(result.columns[1])),
				//"period"             : result.getText(result.columns[1]),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return actuals;
	}
    
    function get_projects() {
    	
		var projects = [];
		var results = search.load({ type: 'job', id: "customsearch_clgx2_capex_projects" });
		results.run().each(function(result) {
			
			var facility_id = parseInt(result.getValue(result.columns[3]));
			var facility = result.getText(result.columns[3]);
			if(!facility_id){
				facility_id = 0;
				facility = "- None -";
			}
			projects.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"project"            : result.getValue(result.columns[1]),
				"subsidiary_id"      : parseInt(result.getValue(result.columns[2])),
				"subsidiary"         : result.getText(result.columns[2]),
				"facility_id"        : facility_id,
				"facility"           : facility,
				"category_id"        : parseInt(result.getValue(result.columns[4])),
				"category"           : result.getText(result.columns[4]),
				"description"        : result.getValue(result.columns[5]),
				"start"              : result.getValue(result.columns[6]),
				"end"                : result.getValue(result.columns[7]),
				"estimated"          : parseFloat(result.getValue(result.columns[8]))
			});
	        return true;
		});
		return projects;
	}
    
    function get_periods() {
		var periods = [];
		var results = search.load({ type: 'accountingperiod', id: "customsearch_clgx_dw_accounting_period" });
		results.run().each(function(result) {
			var isyear = result.getValue(result.columns[4]);
			var isquarter = result.getValue(result.columns[5]);
			var mstart = moment(result.getValue(result.columns[6]));
			if(!isyear && !isquarter){
				periods.push({
					"period_id"    : parseInt(result.getValue(result.columns[0])),
					//"period"       : result.getValue(result.columns[1]),
					"year"         : parseInt(moment(result.getValue(result.columns[6])).format('YYYY')),
					"month_id"     : moment(result.getValue(result.columns[6])).format('MM'),
					"month"        : moment(result.getValue(result.columns[6])).format('MMM')
				});
			}
	        return true;
		});
		return periods;
	}

	function round(value) {
		  //return Number(Math.round(value+'e'+2)+'e-'+2);
		  return parseFloat((parseFloat(value)).toFixed(4))
	}
	
});
