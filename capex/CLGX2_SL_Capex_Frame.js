//-------------------------------------------------------------------------------------------------
//	Script File:	    CLGX2_SL_Capex_Frame.js
//	Script Name:   	CLGX2_SL_Capex_Frame
//	Script Id:      customscript_clgx2_sl_capex_frame
//	Script Type:	    Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		    1/26/2018
//	URL:	            /app/site/hosting/scriptlet.nl?script=1453&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_capex_frame (request, response){
	try {
		
		var roleid = nlapiGetRole();
		var closed = 0;
		if(closed == 1 && (roleid != -5 && roleid != 3 && roleid != 18)){ // if module is closed any not admin using it
			var arrParam = new Array();
			arrParam['custscript_internal_message'] = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
			nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
		}
		var formFrame = nlapiCreateForm('Capex Report');
		var fieldFrame = formFrame.addField('custpage_capex_frame_html','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="capex" id="capex" src="/app/site/hosting/scriptlet.nl?script=1479&deploy=1" height="565px" width="1450px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		response.writePage( formFrame );
	}
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}