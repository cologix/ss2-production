/**
 * 
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   9/1/2017
 * @description Testing various pieces of code for new functionalities. Displaying various temporary results. Small volume mass updates.
 * 
 * Script Name  : CLGX2_SL_Capex_Grid
 * Script File  : CLGX2_SL_Capex_Grid.js
 * Script ID    : customscript_clgx2_sl_capex_grid
 * Deployment ID: customdeploy_clgx2_sl_capex_grid
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1451&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, _, moment, dwg) {

    function onRequest(context) {
		
	    var response = context.response;
	    var request = context.request;
	    
	    var refresh = 0;
	    if(refresh){

		    var per = get_per();
		    //var periods_ids = _.map(per, 'period_id');
		    var pos = get_pos();
		    var act = get_act();
			var prj = get_prj();
		    
			var projects = [];
			for (var i = 0; i < prj.length; i++ ) {
				for (var j = 0; j < per.length; j++ ) {
					
					var obj_pos = _.find(pos, function(o) { return o.project_id == prj[i].project_id && o.period_id == per[j].period_id; });
					var obj_act = _.find(act, function(o) { return o.project_id == prj[i].project_id && o.period_id == per[j].period_id; });
					//var obj_frc = _.find(frc, function(o) { return o.project_id == prj[i].project_id && o.period_id == per[j].period_id; });
					
					if(obj_pos || obj_act){
						var obj = {
							"project_id"         : prj[i].project_id,
							"project"            : prj[i].project,
							//"subsidiary_id"      : prj[i].subsidiary_id,
							"subsidiary"         : prj[i].subsidiary,
							//"facility_id"        : prj[i].facility_id,
							"facility"           : prj[i].facility,
							//"category_id"        : prj[i].category_id,
							"category"           : prj[i].category,
							"description"        : prj[i].description,
							"start"              : prj[i].start,
							"end"                : prj[i].end,
							"estimated"          : prj[i].estimated,
							"year"               : per[j].year,
							"month"              : per[j].month,
							"pos"                : 0,
							"actuals"            : 0,
							"forecast"           : 0,
						};
						if(obj_pos){
							obj.pos = obj_pos.amount;
						}
						if(obj_act){
							obj.actuals = obj_act.amount;
						}
						projects.push(obj);
					}
				}
			}
		    
		    var fileObj = file.create({
				name: 'capex.json',
			    fileType: file.Type.PLAINTEXT,
			    contents: JSON.stringify(projects),
			    //description: records.proc,
			    encoding: file.Encoding.UTF8,
			    folder: 5926903,
			    isOnline: false
			});
			var fileId = fileObj.save();
			

	    } else {
	    	
			var fileObj = file.load({
			    id: 9312458
			});
			projects = JSON.parse(fileObj.getContents());
	    	
	    }
	    
	    //response.write(JSON.stringify(projects));
	    
		var fileObj = file.load({
		    id: 9310758
		});
		html = fileObj.getContents();
		html = html.replace(new RegExp('{dataProjects}','g'), JSON.stringify(projects));
	    
		response.write( html );
		
    }
    return {
        onRequest: onRequest
    };

    function get_pos() {
    	
		var pos = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_pos" });
		//results.filters = [search.createFilter({ name: "postingperiod", operator: "anyof", values: periods_ids })];
		results.run().each(function(result) {
			pos.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				//"project"            : result.getText(result.columns[0]),
				"period_id"          : parseInt(result.getValue(result.columns[1])),
				//"period"             : result.getText(result.columns[1]),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return pos;
	}
    
    function get_act() {
    	
		var actuals = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_actuals" });
		//results.filters = [search.createFilter({ name: "postingperiod", operator: "anyof", values: periods_ids })];
		results.run().each(function(result) {
			actuals.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				//"project"            : result.getText(result.columns[0]),
				"period_id"          : parseInt(result.getValue(result.columns[1])),
				//"period"             : result.getText(result.columns[1]),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return actuals;
	}
    
    function get_prj() {
    	
		var projects = [];
		var results = search.load({ type: 'job', id: "customsearch_clgx2_capex_projects" });
		results.run().each(function(result) {
			
			var facility_id = parseInt(result.getValue(result.columns[3]));
			var facility = result.getText(result.columns[3]);
			if(!facility_id){
				facility_id = 0;
				facility = "- None -";
			}
			projects.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"project"            : result.getValue(result.columns[1]),
				//"subsidiary_id"      : parseInt(result.getValue(result.columns[2])),
				"subsidiary"         : result.getText(result.columns[2]),
				//"facility_id"        : facility_id,
				"facility"           : facility,
				//"category_id"        : parseInt(result.getValue(result.columns[4])),
				"category"           : result.getText(result.columns[4]),
				"description"        : result.getValue(result.columns[5]),
				"start"              : result.getValue(result.columns[6]),
				"end"                : result.getValue(result.columns[7]),
				"estimated"          : parseFloat(result.getValue(result.columns[8])),
				//"periods"            : periods
			});
	        return true;
		});
		return projects;
	}
    
    function get_per() {
		var periods = [];
		
		var start = moment().startOf('year').subtract('years',4).subtract('days',1).format('M/D/YYYY');
		var end = moment().startOf('year').add('years',1).subtract('days',1).format('M/D/YYYY');
		
		var results = search.load({ type: 'accountingperiod', id: "customsearch_clgx_dw_accounting_period" });
		results.filters = [search.createFilter({ name: "startdate", operator: "within", values: [start,end] })];
		//results.filters = [search.createFilter({ name: "startdate", operator: "before", values: end })];
		results.run().each(function(result) {
			var isyear = result.getValue(result.columns[4]);
			var isquarter = result.getValue(result.columns[5]);
			var mstart = moment(result.getValue(result.columns[6]));
			if(!isyear && !isquarter){
				periods.push({
					"period_id"    : parseInt(result.getValue(result.columns[0])),
					//"period"       : result.getValue(result.columns[1]),
					"year"         : parseInt(moment(result.getValue(result.columns[6])).format('YYYY')),
					"month_id"     : moment(result.getValue(result.columns[6])).format('MM'),
					"month"        : moment(result.getValue(result.columns[6])).format('MMM'),
					//"start"        : result.getValue(result.columns[6]),
					//"pos"          : 0,
					//"actuals"      : 0,
					//"budget"       : 0,
				});
			}
	        return true;
		});
		return periods;
	}

	function round(value) {
		  //return Number(Math.round(value+'e'+2)+'e-'+2);
		  return parseFloat((parseFloat(value)).toFixed(4))
	}
	
});
