/**
 * 
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   9/1/2017
 * @description Testing various pieces of code for new functionalities. Displaying various temporary results. Small volume mass updates.
 * 
 * Script Name  : CLGX2_SL_Capex_Project.js
 * Script File  : CLGX2_SL_Capex_Project
 * Script ID    : customscript_clgx2_sl_capex_project
 * Deployment ID: customdeploy_clgx2_sl_capex_project
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1481&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, _, moment, dwg) {

    function onRequest(context) {
		
	    var response = context.response;
	    var request = context.request;
	    
	    var project_id = request.parameters.project_id || 0;
	    var project = "";
	    if(project_id){
		    	var fields = search.lookupFields({type: search.Type.CUSTOMER, id: project_id, columns: ["entityid"]});
		    	project = fields.entityid;
	    }
	    
	    var years = [];
	    var start_year = parseInt(moment().startOf('year').subtract('years',4).format('YYYY'));
	    var start_date = moment().startOf('year').subtract('years',4).subtract('days',1).format('M/D/YY');
	    for (var i = 0; i < 6; i++ ) {
	    		years.push(start_year + i);
	    }
	    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	    
	    var bills = get_bills(project_id, start_date);
	    var credits = get_credits(project_id, start_date);
	    var journals = get_journals(project_id, start_date);
	    
	    log.debug({ title: "Invoices", details: '| journals ' + JSON.stringify(journals) + ' |' });
	    
	    
	    var caplabor = get_journals_cl(project_id, start_date);
	    var forecasts = get_forecasts(project_id, start_date);
	    var pos = get_pos(project_id, start_date);
	    
	    var arr_bills = get_arr_sums(bills, years, months, "amount");
		var arr_credits = get_arr_sums(credits, years, months, "amount");
		var arr_journals = get_arr_sums(journals, years, months, "amount");
		
		log.debug({ title: "Invoices", details: '| arr_journals ' + JSON.stringify(arr_journals) + ' |' });

		
		var arr_caplabor = get_arr_sums(caplabor, years, months, "amount");
		var arr_actuals = [];
		for (var y = 0; y < years.length; y++ ) {
			var arr_m = [];
			for (var m = 0; m < months.length; m++ ) {
				arr_m.push(arr_bills[y][m] + arr_credits[y][m] + arr_journals[y][m] + arr_caplabor[y][m]);
			}
			arr_actuals.push(arr_m);
		}
		var arr_forecasts = get_arr_sums(forecasts, years, months, "amount");
		var arr_variance = [];
		for (var y = 0; y < years.length; y++ ) {
			var arr_m = [];
			for (var m = 0; m < months.length; m++ ) {
				arr_m.push(arr_actuals[y][m] - arr_forecasts[y][m]);
			}
			arr_variance.push(arr_m);
		}
		var arr_unbilled = get_arr_sums(pos, years, months, "unbilled");
		var arr_billed = get_arr_sums(pos, years, months, "billed");
		var arr_pos = get_arr_sums(pos, years, months, "amount");
		
		var objLog = get_obj_sums("Journals", arr_journals, years, months);
		log.debug({ title: "Invoices", details: '| objLog ' + JSON.stringify(objLog) + ' |' });
		
		var arr_tran = [];
		arr_tran.push(get_obj_sums("Bills", arr_bills, years, months));
		arr_tran.push(get_obj_sums("Credits", arr_credits, years, months));
		arr_tran.push(get_obj_sums("Journals", arr_journals, years, months));
		arr_tran.push(get_obj_sums("CapLabor", arr_caplabor, years, months));
		arr_tran.push(get_obj_sums("Actuals", arr_actuals, years, months));
		arr_tran.push(get_obj_sums("Forecasts", arr_forecasts, years, months));
		arr_tran.push(get_obj_sums("Variance", arr_variance, years, months));
		arr_tran.push(get_obj_sums("Unbilled", arr_unbilled, years, months));
		arr_tran.push(get_obj_sums("Billed", arr_billed, years, months));
		arr_tran.push(get_obj_sums("POs", arr_pos, years, months));
		
		
	    var fileObj = file.load({
		    id: 9398305
		});
		var html = fileObj.getContents();
		html = html.replace(new RegExp('{project}','g'), project);
		html = html.replace(new RegExp('{dataYears}','g'), JSON.stringify(years));
		html = html.replace(new RegExp('{dataMonths}','g'), JSON.stringify(months));
        html = html.replace(new RegExp('{dataPOs}','g'), JSON.stringify(pos));
        html = html.replace(new RegExp('{dataBills}','g'), JSON.stringify(bills));
        html = html.replace(new RegExp('{dataCredits}','g'), JSON.stringify(credits));
        html = html.replace(new RegExp('{dataJournals}','g'), JSON.stringify(journals));
        html = html.replace(new RegExp('{dataCapLabor}','g'), JSON.stringify(caplabor));
        html = html.replace(new RegExp('{dataPeriods}','g'), JSON.stringify(arr_tran));
        response.write( html );

    }
    return {
        onRequest: onRequest
    };

    function get_arr_sums(arr_t,years,months,amount){
		var arr_y = [];
		
		for (var y = 0; y < years.length; y++ ) {
			var arr_m = [];
			for (var m = 0; m < months.length; m++ ) {
				var sum = 0;
				var arr_t_y_m = _.filter(arr_t, function(o) { return o.year == years[y] && o.month == months[m]; });
				if(arr_t_y_m){
					sum = _.sumBy(arr_t_y_m, amount);
				}
				arr_m.push(sum);
			}
			arr_y.push(arr_m);
		}
		
		return arr_y;
	}
	
	function get_obj_sums(type, arr, years, months){
		var total = 0;
		var obj = {};
		obj["type"] = type;
		for (var y = 0; y < years.length; y++ ) {
			var key  = 'y' + y;
			obj[key] = round(_.sum(arr[y]));
			total += _.sum(arr[y]);
			for (var m = 0; m < months.length; m++ ) {
				var key  = 'y' + y + '_m' + m;
				obj[key] = arr[y][m];
			}
		}
		obj["total"] = round(total);
		return obj;
	}

	function sum_obj_values( obj ) {
		  var sum = 0;
		  for( var el in obj ) {
		    if( obj.hasOwnProperty( el ) ) {
		      sum += parseFloat( obj[el] );
		    }
		  }
		  return sum;
	}
	
	function get_pos(project_id,start_date) {
		var pos = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_proj_pos" });
		results.filters.push(search.createFilter({ name: "custbody_cologix_project_name", operator: "is", values: project_id }));
		results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[3]);
			pos.push({
				"project_id"    : parseInt(result.getValue(result.columns[0])),
				"po_id"         : parseInt(result.getValue(result.columns[1])),
				"po"            : ((result.getValue(result.columns[2])).replace(/,/g, " ")).replace(/;/g, " "),
				"period"        : period,
				"year"          : parseInt(period.substring(4,9)),
				"month"         : period.substring(0,3),
				"vendor_id"     : parseInt(result.getValue(result.columns[4])),
				"vendor"        : ((result.getValue(result.columns[5])).replace(/,/g, " ")).replace(/;/g, " "),
				"billed"        : round(parseFloat(result.getValue(result.columns[6]))),
				"unbilled"      : round(parseFloat(result.getValue(result.columns[7]))),
				"amount"        : round(parseFloat(result.getValue(result.columns[8])))
			});
	        return true;
		});
		return pos;
	}
    
    function get_bills(project_id,start_date) {
		var bills = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_proj_bills" });
		results.filters.push(search.createFilter({ name: "custcol_clgx_journal_project_id", operator: "is", values: project_id }));
		results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[3]);
			bills.push({
				"project_id"    : parseInt(result.getValue(result.columns[0])),
				"bill_id"       : parseInt(result.getValue(result.columns[1])),
				"bill"           : ((result.getValue(result.columns[2])).replace(/,/g, " ")).replace(/;/g, " "),
				"period"        : period,
				"year"          : parseInt(period.substring(4,9)),
				"month"         : period.substring(0,3),
				"vendor_id"     : parseInt(result.getValue(result.columns[4])),
				"vendor"        : ((result.getValue(result.columns[5])).replace(/,/g, " ")).replace(/;/g, " "),
				"po_id"         : parseInt(result.getValue(result.columns[6])),
				"po"            : ((result.getValue(result.columns[7])).replace(/,/g, " ")).replace(/;/g, " "),
				"amount"        : round(parseFloat(result.getValue(result.columns[8])))
			});
	        return true;
		});
		return bills;
	}
    
    function get_credits(project_id,start_date) {
		var credits = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_proj_credits" });
		results.filters.push(search.createFilter({ name: "custcol_clgx_journal_project_id", operator: "is", values: project_id }));
		results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[3]);
			credits.push({
				"project_id"    : parseInt(result.getValue(result.columns[0])),
				"credit_id"     : parseInt(result.getValue(result.columns[1])),
				"credit"        : ((result.getValue(result.columns[2])).replace(/,/g, " ")).replace(/;/g, " "),
				"period"        : period,
				"year"          : parseInt(period.substring(4,9)),
				"month"         : period.substring(0,3),
				"vendor_id"     : parseInt(result.getValue(result.columns[4])),
				"vendor"        : ((result.getValue(result.columns[5])).replace(/,/g, " ")).replace(/;/g, " "),
				"po_id"         : parseInt(result.getValue(result.columns[6])),
				"po"            : ((result.getValue(result.columns[7])).replace(/,/g, " ")).replace(/;/g, " "),
				"amount"        : round(parseFloat(result.getValue(result.columns[8])))
			});
	        return true;
		});
		return credits;
	}
    
    function get_journals(project_id,start_date) {
		var journals = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_proj_journals" });
		results.filters.push(search.createFilter({ name: "custcol_clgx_journal_project_id", operator: "is", values: project_id }));
		results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[4]);
			journals.push({
				"project_id"    : parseInt(result.getValue(result.columns[0])),
				"journal_id"    : parseInt(result.getValue(result.columns[1])),
				"journal"       : result.getValue(result.columns[2]),
				"year"          : parseInt(result.getValue(result.columns[3])),
				"period"        : result.getText(result.columns[4]),
				"month"         : period.substring(0,3),
				"memo"          : ((result.getValue(result.columns[5])).replace(/,/g, " ")).replace(/;/g, " "),
				"amount"        : round(parseFloat(result.getValue(result.columns[6])))
			});
	        return true;
		});
		return journals;
	}
    
    function get_journals_cl(project_id,start_date) {
		var journals = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_proj_journals_c" });
		results.filters.push(search.createFilter({ name: "custcol_clgx_journal_project_id", operator: "is", values: project_id }));
		results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[4]);
			journals.push({
				"project_id"    : parseInt(result.getValue(result.columns[0])),
				"journal_id"    : parseInt(result.getValue(result.columns[1])),
				"journal"       : result.getValue(result.columns[2]),
				"year"          : parseInt(result.getValue(result.columns[3])),
				"period"        : result.getText(result.columns[4]),
				"month"         : period.substring(0,3),
				"memo"          : ((result.getValue(result.columns[5])).replace(/,/g, " ")).replace(/;/g, " "),
				"amount"        : round(parseFloat(result.getValue(result.columns[6])))
			});
	        return true;
		});
		return journals;
	}
    
    function get_forecasts(project_id,start_date) {
		var forecasts = [];
		var results = search.load({ type: 'customrecord_clgx_cap_bud_forecast', id: "customsearch_clgx2_capex_proj_forecasts" });
		results.filters.push(search.createFilter({ name: "custrecord_clgx_cap_bud_fore_project_id", operator: "is", values: project_id }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[1]);
			forecasts.push({
				"project_id"    : parseInt(result.getValue(result.columns[0])),
				"period"        : period,
				"year"          : parseInt(period.substring(4,9)),
				"month"         : period.substring(0,3),
				"amount"        : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return forecasts;
	}

	function round(value) {
		  //return Number(Math.round(value+'e'+2)+'e-'+2);
		  return parseFloat((parseFloat(value)).toFixed(4))
	}
	
	
});
