/**
 * 
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   9/1/2017
 * @description Testing various pieces of code for new functionalities. Displaying various temporary results. Small volume mass updates.
 * 
 * Script Name  : CLGX2_SL_Capex_Projects.js
 * Script File  : CLGX2_SL_Capex_Projects
 * Script ID    : customscript_clgx2_sl_capex_projects
 * Deployment ID: customdeploy_clgx2_sl_capex_projects
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1479&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, _, moment, dwg) {

    function onRequest(context) {
		
	    var response = context.response;
	    var request = context.request;

		var fileObj = file.load({
		    id: 9312458
		});
		json = fileObj.getContents();

	    var years = [];
	    var start_year = parseInt(moment().startOf('year').subtract('years',4).format('YYYY'));
	    for (var i = 0; i < 6; i++ ) {
	    		years.push(start_year + i);
	    }
	    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	    
	    
		var fileObj = file.load({
		    id: 9389430
		});
		html = fileObj.getContents();
		html = html.replace(new RegExp('{arrProjects}','g'), json);
		html = html.replace(new RegExp('{dataYears}','g'), JSON.stringify(years));
		html = html.replace(new RegExp('{dataMonths}','g'), JSON.stringify(months));
		
		response.write( html );
		
    }
    return {
        onRequest: onRequest
    };
	
});
