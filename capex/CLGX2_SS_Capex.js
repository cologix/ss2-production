/**
 * @author		:	Dan Tansanu - dan.tansanu@cologix.com
 * @date		:	9/5/2017
 * Script File	:	CLGX2_SS_Capex.js
 * Script Name	:	CLGX2_SS_Capex
 * Script ID	:   customscript_clgx2_ss_capex
 * Deployment ID:   customdeploy_clgx2_ss_capex
 * 
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min"
		
	],
function(file, task, https, record, runtime, search, _, moment) {
	
	function execute(context) {
		
		log.debug({ title: "Sync Start", details: "====================================================" });
		
		var response = context.response;
		var request = context.request;
		
	    var years = [];
	    var start_year = parseInt(moment().startOf('year').subtract('years',4).format('YYYY'));
	    var start_date = moment().startOf('year').subtract('years',4).subtract('days',1).format('M/D/YY');
	    for (var i = 0; i < 6; i++ ) {
	    		years.push(start_year + i);
	    }
	    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	    
	    var currentPeriodID = getCurrentAccountingPeriod();
	    
	    var periods = get_periods();
		var fileObj = file.create({
			name: 'periods.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(periods),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
	    var projects = get_projects();
		var fileObj = file.create({
			name: 'projects.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(projects),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
	    
	    var pos_years = get_pos_years(currentPeriodID);
		var fileObj = file.create({
			name: 'pos_years.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(pos_years),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
	    var pos_months = get_pos_months(currentPeriodID);
		var fileObj = file.create({
			name: 'pos_months.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(pos_months),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
	    
	    var actuals_years = get_actuals_years(currentPeriodID);
		var fileObj = file.create({
			name: 'actuals_years.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(actuals_years),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
	    var actuals_months = get_actuals_months(currentPeriodID);
		var fileObj = file.create({
			name: 'actuals_months.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(actuals_months),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
	    
	    var forecasts_years = get_forecasts_years();
		var fileObj = file.create({
			name: 'forecasts_years.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(forecasts_years),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
	    var forecasts_months = get_forecasts_months();
		var fileObj = file.create({
			name: 'forecasts_months.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(forecasts_months),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
	    
		var arr = [];
		for (var i = 0; i < projects.length; i++ ) {
			
			var obj_pos_y1     = _.find(pos_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[0]; });
			var obj_pos_y1_m01 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[0]; });
			var obj_pos_y1_m02 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[1]; });
			var obj_pos_y1_m03 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[2]; });
			var obj_pos_y1_m04 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[3]; });
			var obj_pos_y1_m05 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[4]; });
			var obj_pos_y1_m06 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[5]; });
			var obj_pos_y1_m07 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[6]; });
			var obj_pos_y1_m08 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[7]; });
			var obj_pos_y1_m09 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[8]; });
			var obj_pos_y1_m10 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[9]; });
			var obj_pos_y1_m11 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[10]; });
			var obj_pos_y1_m12 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[11]; });
			
			var obj_pos_y2     = _.find(pos_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[1]; });
			var obj_pos_y2_m01 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[0]; });
			var obj_pos_y2_m02 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[1]; });
			var obj_pos_y2_m03 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[2]; });
			var obj_pos_y2_m04 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[3]; });
			var obj_pos_y2_m05 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[4]; });
			var obj_pos_y2_m06 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[5]; });
			var obj_pos_y2_m07 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[6]; });
			var obj_pos_y2_m08 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[7]; });
			var obj_pos_y2_m09 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[8]; });
			var obj_pos_y2_m10 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[9]; });
			var obj_pos_y2_m11 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[10]; });
			var obj_pos_y2_m12 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[11]; });
			
			var obj_pos_y3     = _.find(pos_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[2]; });
			var obj_pos_y3_m01 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[0]; });
			var obj_pos_y3_m02 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[1]; });
			var obj_pos_y3_m03 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[2]; });
			var obj_pos_y3_m04 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[3]; });
			var obj_pos_y3_m05 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[4]; });
			var obj_pos_y3_m06 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[5]; });
			var obj_pos_y3_m07 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[6]; });
			var obj_pos_y3_m08 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[7]; });
			var obj_pos_y3_m09 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[8]; });
			var obj_pos_y3_m10 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[9]; });
			var obj_pos_y3_m11 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[10]; });
			var obj_pos_y3_m12 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[11]; });
			
			var obj_pos_y4     = _.find(pos_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[3]; });
			var obj_pos_y4_m01 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[0]; });
			var obj_pos_y4_m02 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[1]; });
			var obj_pos_y4_m03 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[2]; });
			var obj_pos_y4_m04 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[3]; });
			var obj_pos_y4_m05 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[4]; });
			var obj_pos_y4_m06 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[5]; });
			var obj_pos_y4_m07 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[6]; });
			var obj_pos_y4_m08 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[7]; });
			var obj_pos_y4_m09 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[8]; });
			var obj_pos_y4_m10 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[9]; });
			var obj_pos_y4_m11 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[10]; });
			var obj_pos_y4_m12 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[11]; });
			
			var obj_pos_y5     = _.find(pos_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[4]; });
			var obj_pos_y5_m01 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[0]; });
			var obj_pos_y5_m02 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[1]; });
			var obj_pos_y5_m03 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[2]; });
			var obj_pos_y5_m04 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[3]; });
			var obj_pos_y5_m05 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[4]; });
			var obj_pos_y5_m06 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[5]; });
			var obj_pos_y5_m07 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[6]; });
			var obj_pos_y5_m08 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[7]; });
			var obj_pos_y5_m09 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[8]; });
			var obj_pos_y5_m10 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[9]; });
			var obj_pos_y5_m11 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[10]; });
			var obj_pos_y5_m12 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[11]; });
			
			var obj_pos_y6     = _.find(pos_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[5]; });
			var obj_pos_y6_m01 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[0]; });
			var obj_pos_y6_m02 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[1]; });
			var obj_pos_y6_m03 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[2]; });
			var obj_pos_y6_m04 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[3]; });
			var obj_pos_y6_m05 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[4]; });
			var obj_pos_y6_m06 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[5]; });
			var obj_pos_y6_m07 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[6]; });
			var obj_pos_y6_m08 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[7]; });
			var obj_pos_y6_m09 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[8]; });
			var obj_pos_y6_m10 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[9]; });
			var obj_pos_y6_m11 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[10]; });
			var obj_pos_y6_m12 = _.find(pos_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[11]; });
			
			
			
			var obj_act_y1     = _.find(actuals_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[0]; });
			var obj_act_y1_m01 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[0]; });
			var obj_act_y1_m02 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[1]; });
			var obj_act_y1_m03 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[2]; });
			var obj_act_y1_m04 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[3]; });
			var obj_act_y1_m05 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[4]; });
			var obj_act_y1_m06 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[5]; });
			var obj_act_y1_m07 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[6]; });
			var obj_act_y1_m08 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[7]; });
			var obj_act_y1_m09 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[8]; });
			var obj_act_y1_m10 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[9]; });
			var obj_act_y1_m11 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[10]; });
			var obj_act_y1_m12 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[11]; });
			
			var obj_act_y2     = _.find(actuals_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[1]; });
			var obj_act_y2_m01 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[0]; });
			var obj_act_y2_m02 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[1]; });
			var obj_act_y2_m03 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[2]; });
			var obj_act_y2_m04 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[3]; });
			var obj_act_y2_m05 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[4]; });
			var obj_act_y2_m06 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[5]; });
			var obj_act_y2_m07 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[6]; });
			var obj_act_y2_m08 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[7]; });
			var obj_act_y2_m09 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[8]; });
			var obj_act_y2_m10 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[9]; });
			var obj_act_y2_m11 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[10]; });
			var obj_act_y2_m12 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[11]; });
			
			var obj_act_y3     = _.find(actuals_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[2]; });
			var obj_act_y3_m01 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[0]; });
			var obj_act_y3_m02 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[1]; });
			var obj_act_y3_m03 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[2]; });
			var obj_act_y3_m04 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[3]; });
			var obj_act_y3_m05 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[4]; });
			var obj_act_y3_m06 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[5]; });
			var obj_act_y3_m07 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[6]; });
			var obj_act_y3_m08 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[7]; });
			var obj_act_y3_m09 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[8]; });
			var obj_act_y3_m10 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[9]; });
			var obj_act_y3_m11 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[10]; });
			var obj_act_y3_m12 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[11]; });
			
			var obj_act_y4     = _.find(actuals_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[3]; });
			var obj_act_y4_m01 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[0]; });
			var obj_act_y4_m02 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[1]; });
			var obj_act_y4_m03 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[2]; });
			var obj_act_y4_m04 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[3]; });
			var obj_act_y4_m05 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[4]; });
			var obj_act_y4_m06 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[5]; });
			var obj_act_y4_m07 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[6]; });
			var obj_act_y4_m08 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[7]; });
			var obj_act_y4_m09 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[8]; });
			var obj_act_y4_m10 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[9]; });
			var obj_act_y4_m11 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[10]; });
			var obj_act_y4_m12 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[11]; });
			
			var obj_act_y5     = _.find(actuals_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[4]; });
			var obj_act_y5_m01 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[0]; });
			var obj_act_y5_m02 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[1]; });
			var obj_act_y5_m03 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[2]; });
			var obj_act_y5_m04 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[3]; });
			var obj_act_y5_m05 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[4]; });
			var obj_act_y5_m06 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[5]; });
			var obj_act_y5_m07 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[6]; });
			var obj_act_y5_m08 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[7]; });
			var obj_act_y5_m09 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[8]; });
			var obj_act_y5_m10 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[9]; });
			var obj_act_y5_m11 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[10]; });
			var obj_act_y5_m12 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[11]; });
			
			var obj_act_y6     = _.find(actuals_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[5]; });
			var obj_act_y6_m01 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[0]; });
			var obj_act_y6_m02 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[1]; });
			var obj_act_y6_m03 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[2]; });
			var obj_act_y6_m04 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[3]; });
			var obj_act_y6_m05 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[4]; });
			var obj_act_y6_m06 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[5]; });
			var obj_act_y6_m07 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[6]; });
			var obj_act_y6_m08 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[7]; });
			var obj_act_y6_m09 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[8]; });
			var obj_act_y6_m10 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[9]; });
			var obj_act_y6_m11 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[10]; });
			var obj_act_y6_m12 = _.find(actuals_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[11]; });
			
			
			var obj_frc_y1     = _.find(forecasts_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[0]; });
			var obj_frc_y1_m01 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[0]; });
			var obj_frc_y1_m02 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[1]; });
			var obj_frc_y1_m03 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[2]; });
			var obj_frc_y1_m04 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[3]; });
			var obj_frc_y1_m05 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[4]; });
			var obj_frc_y1_m06 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[5]; });
			var obj_frc_y1_m07 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[6]; });
			var obj_frc_y1_m08 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[7]; });
			var obj_frc_y1_m09 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[8]; });
			var obj_frc_y1_m10 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[9]; });
			var obj_frc_y1_m11 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[10]; });
			var obj_frc_y1_m12 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[0] && o.month == months[11]; });
			
			var obj_frc_y2     = _.find(forecasts_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[1]; });
			var obj_frc_y2_m01 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[0]; });
			var obj_frc_y2_m02 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[1]; });
			var obj_frc_y2_m03 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[2]; });
			var obj_frc_y2_m04 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[3]; });
			var obj_frc_y2_m05 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[4]; });
			var obj_frc_y2_m06 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[5]; });
			var obj_frc_y2_m07 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[6]; });
			var obj_frc_y2_m08 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[7]; });
			var obj_frc_y2_m09 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[8]; });
			var obj_frc_y2_m10 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[9]; });
			var obj_frc_y2_m11 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[10]; });
			var obj_frc_y2_m12 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[1] && o.month == months[11]; });
			
			var obj_frc_y3 = _.find(forecasts_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[2]; });
			var obj_frc_y3_m01 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[0]; });
			var obj_frc_y3_m02 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[1]; });
			var obj_frc_y3_m03 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[2]; });
			var obj_frc_y3_m04 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[3]; });
			var obj_frc_y3_m05 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[4]; });
			var obj_frc_y3_m06 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[5]; });
			var obj_frc_y3_m07 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[6]; });
			var obj_frc_y3_m08 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[7]; });
			var obj_frc_y3_m09 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[8]; });
			var obj_frc_y3_m10 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[9]; });
			var obj_frc_y3_m11 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[10]; });
			var obj_frc_y3_m12 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[2] && o.month == months[11]; });
			
			var obj_frc_y4     = _.find(forecasts_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[3]; });
			var obj_frc_y4_m01 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[0]; });
			var obj_frc_y4_m02 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[1]; });
			var obj_frc_y4_m03 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[2]; });
			var obj_frc_y4_m04 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[3]; });
			var obj_frc_y4_m05 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[4]; });
			var obj_frc_y4_m06 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[5]; });
			var obj_frc_y4_m07 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[6]; });
			var obj_frc_y4_m08 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[7]; });
			var obj_frc_y4_m09 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[8]; });
			var obj_frc_y4_m10 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[9]; });
			var obj_frc_y4_m11 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[10]; });
			var obj_frc_y4_m12 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[3] && o.month == months[11]; });
			
			var obj_frc_y5     = _.find(forecasts_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[4]; });
			var obj_frc_y5_m01 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[0]; });
			var obj_frc_y5_m02 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[1]; });
			var obj_frc_y5_m03 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[2]; });
			var obj_frc_y5_m04 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[3]; });
			var obj_frc_y5_m05 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[4]; });
			var obj_frc_y5_m06 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[5]; });
			var obj_frc_y5_m07 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[6]; });
			var obj_frc_y5_m08 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[7]; });
			var obj_frc_y5_m09 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[8]; });
			var obj_frc_y5_m10 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[9]; });
			var obj_frc_y5_m11 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[10]; });
			var obj_frc_y5_m12 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[4] && o.month == months[11]; });
			
			var obj_frc_y6     = _.find(forecasts_years, function(o) { return o.project_id == projects[i].project_id && o.year == years[5]; });
			var obj_frc_y6_m01 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[0]; });
			var obj_frc_y6_m02 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[1]; });
			var obj_frc_y6_m03 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[2]; });
			var obj_frc_y6_m04 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[3]; });
			var obj_frc_y6_m05 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[4]; });
			var obj_frc_y6_m06 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[5]; });
			var obj_frc_y6_m07 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[6]; });
			var obj_frc_y6_m08 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[7]; });
			var obj_frc_y6_m09 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[8]; });
			var obj_frc_y6_m10 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[9]; });
			var obj_frc_y6_m11 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[10]; });
			var obj_frc_y6_m12 = _.find(forecasts_months, function(o) { return o.project_id == projects[i].project_id && o.year == years[5] && o.month == months[11]; });
			
			
		
			var obj = {
				"project_id"         : projects[i].project_id,
				"project"            : projects[i].project,
				"subsidiary"         : projects[i].subsidiary,
				"facility"           : projects[i].facility,
				"category"           : projects[i].category,
				"status"             : projects[i].status,
				"description"        : projects[i].description,
				"start"              : projects[i].start,
				"end"                : projects[i].end,
				"estimated"          : projects[i].estimated,
				
				"pos"                : 0,
				"act"                : 0,
				"frc"                : 0,
				"var"                : 0,
				
				"pos_y1"             : 0,
				"pos_y1_m01"         : 0,
				"pos_y1_m02"         : 0,
				"pos_y1_m03"         : 0,
				"pos_y1_m04"         : 0,
				"pos_y1_m05"         : 0,
				"pos_y1_m06"         : 0,
				"pos_y1_m07"         : 0,
				"pos_y1_m08"         : 0,
				"pos_y1_m09"         : 0,
				"pos_y1_m10"         : 0,
				"pos_y1_m11"         : 0,
				"pos_y1_m12"         : 0,
				
				"act_y1"             : 0,
				"act_y1_m01"         : 0,
				"act_y1_m02"         : 0,
				"act_y1_m03"         : 0,
				"act_y1_m04"         : 0,
				"act_y1_m05"         : 0,
				"act_y1_m06"         : 0,
				"act_y1_m07"         : 0,
				"act_y1_m08"         : 0,
				"act_y1_m09"         : 0,
				"act_y1_m10"         : 0,
				"act_y1_m11"         : 0,
				"act_y1_m12"         : 0,
				
				"frc_y1"             : 0,
				"frc_y1_m01"         : 0,
				"frc_y1_m02"         : 0,
				"frc_y1_m03"         : 0,
				"frc_y1_m04"         : 0,
				"frc_y1_m05"         : 0,
				"frc_y1_m06"         : 0,
				"frc_y1_m07"         : 0,
				"frc_y1_m08"         : 0,
				"frc_y1_m09"         : 0,
				"frc_y1_m10"         : 0,
				"frc_y1_m11"         : 0,
				"frc_y1_m12"         : 0,
				
				"var_y1"             : 0,
				"var_y1_mo1"         : 0,
				"var_y1_mo2"         : 0,
				"var_y1_mo3"         : 0,
				"var_y1_mo4"         : 0,
				"var_y1_mo5"         : 0,
				"var_y1_mo6"         : 0,
				"var_y1_mo7"         : 0,
				"var_y1_mo8"         : 0,
				"var_y1_mo9"         : 0,
				"var_y1_m10"         : 0,
				"var_y1_m11"         : 0,
				"var_y1_m12"         : 0,
				
				
				
				
				"pos_y2"             : 0,
				"pos_y2_m01"         : 0,
				"pos_y2_m02"         : 0,
				"pos_y2_m03"         : 0,
				"pos_y2_m04"         : 0,
				"pos_y2_m05"         : 0,
				"pos_y2_m06"         : 0,
				"pos_y2_m07"         : 0,
				"pos_y2_m08"         : 0,
				"pos_y2_m09"         : 0,
				"pos_y2_m10"         : 0,
				"pos_y2_m11"         : 0,
				"pos_y2_m12"         : 0,
				
				"act_y2"             : 0,
				"act_y2_m01"         : 0,
				"act_y2_m02"         : 0,
				"act_y2_m03"         : 0,
				"act_y2_m04"         : 0,
				"act_y2_m05"         : 0,
				"act_y2_m06"         : 0,
				"act_y2_m07"         : 0,
				"act_y2_m08"         : 0,
				"act_y2_m09"         : 0,
				"act_y2_m10"         : 0,
				"act_y2_m11"         : 0,
				"act_y2_m12"         : 0,
				
				"frc_y2"             : 0,
				"frc_y2_m01"         : 0,
				"frc_y2_m02"         : 0,
				"frc_y2_m03"         : 0,
				"frc_y2_m04"         : 0,
				"frc_y2_m05"         : 0,
				"frc_y2_m06"         : 0,
				"frc_y2_m07"         : 0,
				"frc_y2_m08"         : 0,
				"frc_y2_m09"         : 0,
				"frc_y2_m10"         : 0,
				"frc_y2_m11"         : 0,
				"frc_y2_m12"         : 0,
				
				"var_y2"             : 0,
				"var_y2_mo1"         : 0,
				"var_y2_mo2"         : 0,
				"var_y2_mo3"         : 0,
				"var_y2_mo4"         : 0,
				"var_y2_mo5"         : 0,
				"var_y2_mo6"         : 0,
				"var_y2_mo7"         : 0,
				"var_y2_mo8"         : 0,
				"var_y2_mo9"         : 0,
				"var_y2_m10"         : 0,
				"var_y2_m11"         : 0,
				"var_y2_m12"         : 0,
				
				
				
				
				
				
				"pos_y3"             : 0,
				"pos_y3_m01"         : 0,
				"pos_y3_m02"         : 0,
				"pos_y3_m03"         : 0,
				"pos_y3_m04"         : 0,
				"pos_y3_m05"         : 0,
				"pos_y3_m06"         : 0,
				"pos_y3_m07"         : 0,
				"pos_y3_m08"         : 0,
				"pos_y3_m09"         : 0,
				"pos_y3_m10"         : 0,
				"pos_y3_m11"         : 0,
				"pos_y3_m12"         : 0,
				
				"act_y3"             : 0,
				"act_y3_m01"         : 0,
				"act_y3_m02"         : 0,
				"act_y3_m03"         : 0,
				"act_y3_m04"         : 0,
				"act_y3_m05"         : 0,
				"act_y3_m06"         : 0,
				"act_y3_m07"         : 0,
				"act_y3_m08"         : 0,
				"act_y3_m09"         : 0,
				"act_y3_m10"         : 0,
				"act_y3_m11"         : 0,
				"act_y3_m12"         : 0,
				
				"frc_y3"             : 0,
				"frc_y3_m01"         : 0,
				"frc_y3_m02"         : 0,
				"frc_y3_m03"         : 0,
				"frc_y3_m04"         : 0,
				"frc_y3_m05"         : 0,
				"frc_y3_m06"         : 0,
				"frc_y3_m07"         : 0,
				"frc_y3_m08"         : 0,
				"frc_y3_m09"         : 0,
				"frc_y3_m10"         : 0,
				"frc_y3_m11"         : 0,
				"frc_y3_m12"         : 0,
				
				"var_y3"             : 0,
				"var_y3_mo1"         : 0,
				"var_y3_mo2"         : 0,
				"var_y3_mo3"         : 0,
				"var_y3_mo4"         : 0,
				"var_y3_mo5"         : 0,
				"var_y3_mo6"         : 0,
				"var_y3_mo7"         : 0,
				"var_y3_mo8"         : 0,
				"var_y3_mo9"         : 0,
				"var_y3_m10"         : 0,
				"var_y3_m11"         : 0,
				"var_y3_m12"         : 0,
				
				
				
				
				"pos_y4"             : 0,
				"pos_y4_m01"         : 0,
				"pos_y4_m02"         : 0,
				"pos_y4_m03"         : 0,
				"pos_y4_m04"         : 0,
				"pos_y4_m05"         : 0,
				"pos_y4_m06"         : 0,
				"pos_y4_m07"         : 0,
				"pos_y4_m08"         : 0,
				"pos_y4_m09"         : 0,
				"pos_y4_m10"         : 0,
				"pos_y4_m11"         : 0,
				"pos_y4_m12"         : 0,
				
				"act_y4"             : 0,
				"act_y4_m01"         : 0,
				"act_y4_m02"         : 0,
				"act_y4_m03"         : 0,
				"act_y4_m04"         : 0,
				"act_y4_m05"         : 0,
				"act_y4_m06"         : 0,
				"act_y4_m07"         : 0,
				"act_y4_m08"         : 0,
				"act_y4_m09"         : 0,
				"act_y4_m10"         : 0,
				"act_y4_m11"         : 0,
				"act_y4_m12"         : 0,
				
				"frc_y4"             : 0,
				"frc_y4_m01"         : 0,
				"frc_y4_m02"         : 0,
				"frc_y4_m03"         : 0,
				"frc_y4_m04"         : 0,
				"frc_y4_m05"         : 0,
				"frc_y4_m06"         : 0,
				"frc_y4_m07"         : 0,
				"frc_y4_m08"         : 0,
				"frc_y4_m09"         : 0,
				"frc_y4_m10"         : 0,
				"frc_y4_m11"         : 0,
				"frc_y4_m12"         : 0,
				
				"var_y4"             : 0,
				"var_y4_mo1"         : 0,
				"var_y4_mo2"         : 0,
				"var_y4_mo3"         : 0,
				"var_y4_mo4"         : 0,
				"var_y4_mo5"         : 0,
				"var_y4_mo6"         : 0,
				"var_y4_mo7"         : 0,
				"var_y4_mo8"         : 0,
				"var_y4_mo9"         : 0,
				"var_y4_m10"         : 0,
				"var_y4_m11"         : 0,
				"var_y4_m12"         : 0,
				
				
				
				
				
				"pos_y5"             : 0,
				"pos_y5_m01"         : 0,
				"pos_y5_m02"         : 0,
				"pos_y5_m03"         : 0,
				"pos_y5_m04"         : 0,
				"pos_y5_m05"         : 0,
				"pos_y5_m06"         : 0,
				"pos_y5_m07"         : 0,
				"pos_y5_m08"         : 0,
				"pos_y5_m09"         : 0,
				"pos_y5_m10"         : 0,
				"pos_y5_m11"         : 0,
				"pos_y5_m12"         : 0,
				
				"act_y5"             : 0,
				"act_y5_m01"         : 0,
				"act_y5_m02"         : 0,
				"act_y5_m03"         : 0,
				"act_y5_m04"         : 0,
				"act_y5_m05"         : 0,
				"act_y5_m06"         : 0,
				"act_y5_m07"         : 0,
				"act_y5_m08"         : 0,
				"act_y5_m09"         : 0,
				"act_y5_m10"         : 0,
				"act_y5_m11"         : 0,
				"act_y5_m12"         : 0,
				
				"frc_y5"             : 0,
				"frc_y5_m01"         : 0,
				"frc_y5_m02"         : 0,
				"frc_y5_m03"         : 0,
				"frc_y5_m04"         : 0,
				"frc_y5_m05"         : 0,
				"frc_y5_m06"         : 0,
				"frc_y5_m07"         : 0,
				"frc_y5_m08"         : 0,
				"frc_y5_m09"         : 0,
				"frc_y5_m10"         : 0,
				"frc_y5_m11"         : 0,
				"frc_y5_m12"         : 0,
				
				"var_y5"             : 0,
				"var_y5_mo1"         : 0,
				"var_y5_mo2"         : 0,
				"var_y5_mo3"         : 0,
				"var_y5_mo4"         : 0,
				"var_y5_mo5"         : 0,
				"var_y5_mo6"         : 0,
				"var_y5_mo7"         : 0,
				"var_y5_mo8"         : 0,
				"var_y5_mo9"         : 0,
				"var_y5_m10"         : 0,
				"var_y5_m11"         : 0,
				"var_y5_m12"         : 0,
				
				"pos_y6"             : 0,
				"pos_y6_m01"         : 0,
				"pos_y6_m02"         : 0,
				"pos_y6_m03"         : 0,
				"pos_y6_m04"         : 0,
				"pos_y6_m05"         : 0,
				"pos_y6_m06"         : 0,
				"pos_y6_m07"         : 0,
				"pos_y6_m08"         : 0,
				"pos_y6_m09"         : 0,
				"pos_y6_m10"         : 0,
				"pos_y6_m11"         : 0,
				"pos_y6_m12"         : 0,
				
				"act_y6"             : 0,
				"act_y6_m01"         : 0,
				"act_y6_m02"         : 0,
				"act_y6_m03"         : 0,
				"act_y6_m04"         : 0,
				"act_y6_m05"         : 0,
				"act_y6_m06"         : 0,
				"act_y6_m07"         : 0,
				"act_y6_m08"         : 0,
				"act_y6_m09"         : 0,
				"act_y6_m10"         : 0,
				"act_y6_m11"         : 0,
				"act_y6_m12"         : 0,
				
				"frc_y6"             : 0,
				"frc_y6_m01"         : 0,
				"frc_y6_m02"         : 0,
				"frc_y6_m03"         : 0,
				"frc_y6_m04"         : 0,
				"frc_y6_m05"         : 0,
				"frc_y6_m06"         : 0,
				"frc_y6_m07"         : 0,
				"frc_y6_m08"         : 0,
				"frc_y6_m09"         : 0,
				"frc_y6_m10"         : 0,
				"frc_y6_m11"         : 0,
				"frc_y6_m12"         : 0,
				
				"var_y6"             : 0,
				"var_y6_mo1"         : 0,
				"var_y6_mo2"         : 0,
				"var_y6_mo3"         : 0,
				"var_y6_mo4"         : 0,
				"var_y6_mo5"         : 0,
				"var_y6_mo6"         : 0,
				"var_y6_mo7"         : 0,
				"var_y6_mo8"         : 0,
				"var_y6_mo9"         : 0,
				"var_y6_m10"         : 0,
				"var_y6_m11"         : 0,
				"var_y6_m12"         : 0
			};
			
			if(obj_pos_y1){obj.pos_y1 = obj_pos_y1.amount;}
			if(obj_pos_y1_m01){obj.pos_y1_m01 = obj_pos_y1_m01.amount;}
			if(obj_pos_y1_m02){obj.pos_y1_m02 = obj_pos_y1_m02.amount;}
			if(obj_pos_y1_m03){obj.pos_y1_m03 = obj_pos_y1_m03.amount;}
			if(obj_pos_y1_m04){obj.pos_y1_m04 = obj_pos_y1_m04.amount;}
			if(obj_pos_y1_m05){obj.pos_y1_m05 = obj_pos_y1_m05.amount;}
			if(obj_pos_y1_m06){obj.pos_y1_m06 = obj_pos_y1_m06.amount;}
			if(obj_pos_y1_m07){obj.pos_y1_m07 = obj_pos_y1_m07.amount;}
			if(obj_pos_y1_m08){obj.pos_y1_m08 = obj_pos_y1_m08.amount;}
			if(obj_pos_y1_m09){obj.pos_y1_m09 = obj_pos_y1_m09.amount;}
			if(obj_pos_y1_m10){obj.pos_y1_m10 = obj_pos_y1_m10.amount;}
			if(obj_pos_y1_m11){obj.pos_y1_m11 = obj_pos_y1_m11.amount;}
			if(obj_pos_y1_m12){obj.pos_y1_m12 = obj_pos_y1_m12.amount;}
			
			if(obj_pos_y2){obj.pos_y2 = obj_pos_y2.amount;}
			if(obj_pos_y2_m01){obj.pos_y2_m01 = obj_pos_y2_m01.amount;}
			if(obj_pos_y2_m02){obj.pos_y2_m02 = obj_pos_y2_m02.amount;}
			if(obj_pos_y2_m03){obj.pos_y2_m03 = obj_pos_y2_m03.amount;}
			if(obj_pos_y2_m04){obj.pos_y2_m04 = obj_pos_y2_m04.amount;}
			if(obj_pos_y2_m05){obj.pos_y2_m05 = obj_pos_y2_m05.amount;}
			if(obj_pos_y2_m06){obj.pos_y2_m06 = obj_pos_y2_m06.amount;}
			if(obj_pos_y2_m07){obj.pos_y2_m07 = obj_pos_y2_m07.amount;}
			if(obj_pos_y2_m08){obj.pos_y2_m08 = obj_pos_y2_m08.amount;}
			if(obj_pos_y2_m09){obj.pos_y2_m09 = obj_pos_y2_m09.amount;}
			if(obj_pos_y2_m10){obj.pos_y2_m10 = obj_pos_y2_m10.amount;}
			if(obj_pos_y2_m11){obj.pos_y2_m11 = obj_pos_y2_m11.amount;}
			if(obj_pos_y2_m12){obj.pos_y2_m12 = obj_pos_y2_m12.amount;}
			
			if(obj_pos_y3){obj.pos_y3 = obj_pos_y3.amount;}
			if(obj_pos_y3_m01){obj.pos_y3_m01 = obj_pos_y3_m01.amount;}
			if(obj_pos_y3_m02){obj.pos_y3_m02 = obj_pos_y3_m02.amount;}
			if(obj_pos_y3_m03){obj.pos_y3_m03 = obj_pos_y3_m03.amount;}
			if(obj_pos_y3_m04){obj.pos_y3_m04 = obj_pos_y3_m04.amount;}
			if(obj_pos_y3_m05){obj.pos_y3_m05 = obj_pos_y3_m05.amount;}
			if(obj_pos_y3_m06){obj.pos_y3_m06 = obj_pos_y3_m06.amount;}
			if(obj_pos_y3_m07){obj.pos_y3_m07 = obj_pos_y3_m07.amount;}
			if(obj_pos_y3_m08){obj.pos_y3_m08 = obj_pos_y3_m08.amount;}
			if(obj_pos_y3_m09){obj.pos_y3_m09 = obj_pos_y3_m09.amount;}
			if(obj_pos_y3_m10){obj.pos_y3_m10 = obj_pos_y3_m10.amount;}
			if(obj_pos_y3_m11){obj.pos_y3_m11 = obj_pos_y3_m11.amount;}
			if(obj_pos_y3_m12){obj.pos_y3_m12 = obj_pos_y3_m12.amount;}
			
			if(obj_pos_y4){obj.pos_y4 = obj_pos_y4.amount;}
			if(obj_pos_y4_m01){obj.pos_y4_m01 = obj_pos_y4_m01.amount;}
			if(obj_pos_y4_m02){obj.pos_y4_m02 = obj_pos_y4_m02.amount;}
			if(obj_pos_y4_m03){obj.pos_y4_m03 = obj_pos_y4_m03.amount;}
			if(obj_pos_y4_m04){obj.pos_y4_m04 = obj_pos_y4_m04.amount;}
			if(obj_pos_y4_m05){obj.pos_y4_m05 = obj_pos_y4_m05.amount;}
			if(obj_pos_y4_m06){obj.pos_y4_m06 = obj_pos_y4_m06.amount;}
			if(obj_pos_y4_m07){obj.pos_y4_m07 = obj_pos_y4_m07.amount;}
			if(obj_pos_y4_m08){obj.pos_y4_m08 = obj_pos_y4_m08.amount;}
			if(obj_pos_y4_m09){obj.pos_y4_m09 = obj_pos_y4_m09.amount;}
			if(obj_pos_y4_m10){obj.pos_y4_m10 = obj_pos_y4_m10.amount;}
			if(obj_pos_y4_m11){obj.pos_y4_m11 = obj_pos_y4_m11.amount;}
			if(obj_pos_y4_m12){obj.pos_y4_m12 = obj_pos_y4_m12.amount;}
			
			if(obj_pos_y5){obj.pos_y5 = obj_pos_y5.amount;}
			if(obj_pos_y5_m01){obj.pos_y5_m01 = obj_pos_y5_m01.amount;}
			if(obj_pos_y5_m02){obj.pos_y5_m02 = obj_pos_y5_m02.amount;}
			if(obj_pos_y5_m03){obj.pos_y5_m03 = obj_pos_y5_m03.amount;}
			if(obj_pos_y5_m04){obj.pos_y5_m04 = obj_pos_y5_m04.amount;}
			if(obj_pos_y5_m05){obj.pos_y5_m05 = obj_pos_y5_m05.amount;}
			if(obj_pos_y5_m06){obj.pos_y5_m06 = obj_pos_y5_m06.amount;}
			if(obj_pos_y5_m07){obj.pos_y5_m07 = obj_pos_y5_m07.amount;}
			if(obj_pos_y5_m08){obj.pos_y5_m08 = obj_pos_y5_m08.amount;}
			if(obj_pos_y5_m09){obj.pos_y5_m09 = obj_pos_y5_m09.amount;}
			if(obj_pos_y5_m10){obj.pos_y5_m10 = obj_pos_y5_m10.amount;}
			if(obj_pos_y5_m11){obj.pos_y5_m11 = obj_pos_y5_m11.amount;}
			if(obj_pos_y5_m12){obj.pos_y5_m12 = obj_pos_y5_m12.amount;}
			
			if(obj_pos_y6){obj.pos_y6 = obj_pos_y6.amount;}
			if(obj_pos_y6_m01){obj.pos_y6_m01 = obj_pos_y6_m01.amount;}
			if(obj_pos_y6_m02){obj.pos_y6_m02 = obj_pos_y6_m02.amount;}
			if(obj_pos_y6_m03){obj.pos_y6_m03 = obj_pos_y6_m03.amount;}
			if(obj_pos_y6_m04){obj.pos_y6_m04 = obj_pos_y6_m04.amount;}
			if(obj_pos_y6_m05){obj.pos_y6_m05 = obj_pos_y6_m05.amount;}
			if(obj_pos_y6_m06){obj.pos_y6_m06 = obj_pos_y6_m06.amount;}
			if(obj_pos_y6_m07){obj.pos_y6_m07 = obj_pos_y6_m07.amount;}
			if(obj_pos_y6_m08){obj.pos_y6_m08 = obj_pos_y6_m08.amount;}
			if(obj_pos_y6_m09){obj.pos_y6_m09 = obj_pos_y6_m09.amount;}
			if(obj_pos_y6_m10){obj.pos_y6_m10 = obj_pos_y6_m10.amount;}
			if(obj_pos_y6_m11){obj.pos_y6_m11 = obj_pos_y6_m11.amount;}
			if(obj_pos_y6_m12){obj.pos_y6_m12 = obj_pos_y6_m12.amount;}
			
			
			
			if(obj_act_y1){obj.act_y1 = obj_act_y1.amount;}
			if(obj_act_y1_m01){obj.act_y1_m01 = obj_act_y1_m01.amount;}
			if(obj_act_y1_m02){obj.act_y1_m02 = obj_act_y1_m02.amount;}
			if(obj_act_y1_m03){obj.act_y1_m03 = obj_act_y1_m03.amount;}
			if(obj_act_y1_m04){obj.act_y1_m04 = obj_act_y1_m04.amount;}
			if(obj_act_y1_m05){obj.act_y1_m05 = obj_act_y1_m05.amount;}
			if(obj_act_y1_m06){obj.act_y1_m06 = obj_act_y1_m06.amount;}
			if(obj_act_y1_m07){obj.act_y1_m07 = obj_act_y1_m07.amount;}
			if(obj_act_y1_m08){obj.act_y1_m08 = obj_act_y1_m08.amount;}
			if(obj_act_y1_m09){obj.act_y1_m09 = obj_act_y1_m09.amount;}
			if(obj_act_y1_m10){obj.act_y1_m10 = obj_act_y1_m10.amount;}
			if(obj_act_y1_m11){obj.act_y1_m11 = obj_act_y1_m11.amount;}
			if(obj_act_y1_m12){obj.act_y1_m12 = obj_act_y1_m12.amount;}
			
			if(obj_act_y2){obj.act_y2 = obj_act_y2.amount;}
			if(obj_act_y2_m01){obj.act_y2_m01 = obj_act_y2_m01.amount;}
			if(obj_act_y2_m02){obj.act_y2_m02 = obj_act_y2_m02.amount;}
			if(obj_act_y2_m03){obj.act_y2_m03 = obj_act_y2_m03.amount;}
			if(obj_act_y2_m04){obj.act_y2_m04 = obj_act_y2_m04.amount;}
			if(obj_act_y2_m05){obj.act_y2_m05 = obj_act_y2_m05.amount;}
			if(obj_act_y2_m06){obj.act_y2_m06 = obj_act_y2_m06.amount;}
			if(obj_act_y2_m07){obj.act_y2_m07 = obj_act_y2_m07.amount;}
			if(obj_act_y2_m08){obj.act_y2_m08 = obj_act_y2_m08.amount;}
			if(obj_act_y2_m09){obj.act_y2_m09 = obj_act_y2_m09.amount;}
			if(obj_act_y2_m10){obj.act_y2_m10 = obj_act_y2_m10.amount;}
			if(obj_act_y2_m11){obj.act_y2_m11 = obj_act_y2_m11.amount;}
			if(obj_act_y2_m12){obj.act_y2_m12 = obj_act_y2_m12.amount;}
			
			if(obj_act_y3){obj.act_y3 = obj_act_y3.amount;}
			if(obj_act_y3_m01){obj.act_y3_m01 = obj_act_y3_m01.amount;}
			if(obj_act_y3_m02){obj.act_y3_m02 = obj_act_y3_m02.amount;}
			if(obj_act_y3_m03){obj.act_y3_m03 = obj_act_y3_m03.amount;}
			if(obj_act_y3_m04){obj.act_y3_m04 = obj_act_y3_m04.amount;}
			if(obj_act_y3_m05){obj.act_y3_m05 = obj_act_y3_m05.amount;}
			if(obj_act_y3_m06){obj.act_y3_m06 = obj_act_y3_m06.amount;}
			if(obj_act_y3_m07){obj.act_y3_m07 = obj_act_y3_m07.amount;}
			if(obj_act_y3_m08){obj.act_y3_m08 = obj_act_y3_m08.amount;}
			if(obj_act_y3_m09){obj.act_y3_m09 = obj_act_y3_m09.amount;}
			if(obj_act_y3_m10){obj.act_y3_m10 = obj_act_y3_m10.amount;}
			if(obj_act_y3_m11){obj.act_y3_m11 = obj_act_y3_m11.amount;}
			if(obj_act_y3_m12){obj.act_y3_m12 = obj_act_y3_m12.amount;}
			
			if(obj_act_y4){obj.act_y4 = obj_act_y4.amount;}
			if(obj_act_y4_m01){obj.act_y4_m01 = obj_act_y4_m01.amount;}
			if(obj_act_y4_m02){obj.act_y4_m02 = obj_act_y4_m02.amount;}
			if(obj_act_y4_m03){obj.act_y4_m03 = obj_act_y4_m03.amount;}
			if(obj_act_y4_m04){obj.act_y4_m04 = obj_act_y4_m04.amount;}
			if(obj_act_y4_m05){obj.act_y4_m05 = obj_act_y4_m05.amount;}
			if(obj_act_y4_m06){obj.act_y4_m06 = obj_act_y4_m06.amount;}
			if(obj_act_y4_m07){obj.act_y4_m07 = obj_act_y4_m07.amount;}
			if(obj_act_y4_m08){obj.act_y4_m08 = obj_act_y4_m08.amount;}
			if(obj_act_y4_m09){obj.act_y4_m09 = obj_act_y4_m09.amount;}
			if(obj_act_y4_m10){obj.act_y4_m10 = obj_act_y4_m10.amount;}
			if(obj_act_y4_m11){obj.act_y4_m11 = obj_act_y4_m11.amount;}
			if(obj_act_y4_m12){obj.act_y4_m12 = obj_act_y4_m12.amount;}
			
			if(obj_act_y5){obj.act_y5 = obj_act_y5.amount;}
			if(obj_act_y5_m01){obj.act_y5_m01 = obj_act_y5_m01.amount;}
			if(obj_act_y5_m02){obj.act_y5_m02 = obj_act_y5_m02.amount;}
			if(obj_act_y5_m03){obj.act_y5_m03 = obj_act_y5_m03.amount;}
			if(obj_act_y5_m04){obj.act_y5_m04 = obj_act_y5_m04.amount;}
			if(obj_act_y5_m05){obj.act_y5_m05 = obj_act_y5_m05.amount;}
			if(obj_act_y5_m06){obj.act_y5_m06 = obj_act_y5_m06.amount;}
			if(obj_act_y5_m07){obj.act_y5_m07 = obj_act_y5_m07.amount;}
			if(obj_act_y5_m08){obj.act_y5_m08 = obj_act_y5_m08.amount;}
			if(obj_act_y5_m09){obj.act_y5_m09 = obj_act_y5_m09.amount;}
			if(obj_act_y5_m10){obj.act_y5_m10 = obj_act_y5_m10.amount;}
			if(obj_act_y5_m11){obj.act_y5_m11 = obj_act_y5_m11.amount;}
			if(obj_act_y5_m12){obj.act_y5_m12 = obj_act_y5_m12.amount;}
			
			if(obj_act_y6){obj.act_y6 = obj_act_y6.amount;}
			if(obj_act_y6_m01){obj.act_y6_m01 = obj_act_y6_m01.amount;}
			if(obj_act_y6_m02){obj.act_y6_m02 = obj_act_y6_m02.amount;}
			if(obj_act_y6_m03){obj.act_y6_m03 = obj_act_y6_m03.amount;}
			if(obj_act_y6_m04){obj.act_y6_m04 = obj_act_y6_m04.amount;}
			if(obj_act_y6_m05){obj.act_y6_m05 = obj_act_y6_m05.amount;}
			if(obj_act_y6_m06){obj.act_y6_m06 = obj_act_y6_m06.amount;}
			if(obj_act_y6_m07){obj.act_y6_m07 = obj_act_y6_m07.amount;}
			if(obj_act_y6_m08){obj.act_y6_m08 = obj_act_y6_m08.amount;}
			if(obj_act_y6_m09){obj.act_y6_m09 = obj_act_y6_m09.amount;}
			if(obj_act_y6_m10){obj.act_y6_m10 = obj_act_y6_m10.amount;}
			if(obj_act_y6_m11){obj.act_y6_m11 = obj_act_y6_m11.amount;}
			if(obj_act_y6_m12){obj.act_y6_m12 = obj_act_y6_m12.amount;}
			
			
			
			
			

			if(obj_frc_y1){obj.frc_y1 = obj_frc_y1.amount;}
			if(obj_frc_y1_m01){obj.frc_y1_m01 = obj_frc_y1_m01.amount;}
			if(obj_frc_y1_m02){obj.frc_y1_m02 = obj_frc_y1_m02.amount;}
			if(obj_frc_y1_m03){obj.frc_y1_m03 = obj_frc_y1_m03.amount;}
			if(obj_frc_y1_m04){obj.frc_y1_m04 = obj_frc_y1_m04.amount;}
			if(obj_frc_y1_m05){obj.frc_y1_m05 = obj_frc_y1_m05.amount;}
			if(obj_frc_y1_m06){obj.frc_y1_m06 = obj_frc_y1_m06.amount;}
			if(obj_frc_y1_m07){obj.frc_y1_m07 = obj_frc_y1_m07.amount;}
			if(obj_frc_y1_m08){obj.frc_y1_m08 = obj_frc_y1_m08.amount;}
			if(obj_frc_y1_m09){obj.frc_y1_m09 = obj_frc_y1_m09.amount;}
			if(obj_frc_y1_m10){obj.frc_y1_m10 = obj_frc_y1_m10.amount;}
			if(obj_frc_y1_m11){obj.frc_y1_m11 = obj_frc_y1_m11.amount;}
			if(obj_frc_y1_m12){obj.frc_y1_m12 = obj_frc_y1_m12.amount;}
			
			if(obj_frc_y2){obj.frc_y2 = obj_frc_y2.amount;}
			if(obj_frc_y2_m01){obj.frc_y2_m01 = obj_frc_y2_m01.amount;}
			if(obj_frc_y2_m02){obj.frc_y2_m02 = obj_frc_y2_m02.amount;}
			if(obj_frc_y2_m03){obj.frc_y2_m03 = obj_frc_y2_m03.amount;}
			if(obj_frc_y2_m04){obj.frc_y2_m04 = obj_frc_y2_m04.amount;}
			if(obj_frc_y2_m05){obj.frc_y2_m05 = obj_frc_y2_m05.amount;}
			if(obj_frc_y2_m06){obj.frc_y2_m06 = obj_frc_y2_m06.amount;}
			if(obj_frc_y2_m07){obj.frc_y2_m07 = obj_frc_y2_m07.amount;}
			if(obj_frc_y2_m08){obj.frc_y2_m08 = obj_frc_y2_m08.amount;}
			if(obj_frc_y2_m09){obj.frc_y2_m09 = obj_frc_y2_m09.amount;}
			if(obj_frc_y2_m10){obj.frc_y2_m10 = obj_frc_y2_m10.amount;}
			if(obj_frc_y2_m11){obj.frc_y2_m11 = obj_frc_y2_m11.amount;}
			if(obj_frc_y2_m12){obj.frc_y2_m12 = obj_frc_y2_m12.amount;}
			
			if(obj_frc_y3){obj.frc_y3 = obj_frc_y3.amount;}
			if(obj_frc_y3_m01){obj.frc_y3_m01 = obj_frc_y3_m01.amount;}
			if(obj_frc_y3_m02){obj.frc_y3_m02 = obj_frc_y3_m02.amount;}
			if(obj_frc_y3_m03){obj.frc_y3_m03 = obj_frc_y3_m03.amount;}
			if(obj_frc_y3_m04){obj.frc_y3_m04 = obj_frc_y3_m04.amount;}
			if(obj_frc_y3_m05){obj.frc_y3_m05 = obj_frc_y3_m05.amount;}
			if(obj_frc_y3_m06){obj.frc_y3_m06 = obj_frc_y3_m06.amount;}
			if(obj_frc_y3_m07){obj.frc_y3_m07 = obj_frc_y3_m07.amount;}
			if(obj_frc_y3_m08){obj.frc_y3_m08 = obj_frc_y3_m08.amount;}
			if(obj_frc_y3_m09){obj.frc_y3_m09 = obj_frc_y3_m09.amount;}
			if(obj_frc_y3_m10){obj.frc_y3_m10 = obj_frc_y3_m10.amount;}
			if(obj_frc_y3_m11){obj.frc_y3_m11 = obj_frc_y3_m11.amount;}
			if(obj_frc_y3_m12){obj.frc_y3_m12 = obj_frc_y3_m12.amount;}
			
			if(obj_frc_y4){obj.frc_y4 = obj_frc_y4.amount;}
			if(obj_frc_y4_m01){obj.frc_y4_m01 = obj_frc_y4_m01.amount;}
			if(obj_frc_y4_m02){obj.frc_y4_m02 = obj_frc_y4_m02.amount;}
			if(obj_frc_y4_m03){obj.frc_y4_m03 = obj_frc_y4_m03.amount;}
			if(obj_frc_y4_m04){obj.frc_y4_m04 = obj_frc_y4_m04.amount;}
			if(obj_frc_y4_m05){obj.frc_y4_m05 = obj_frc_y4_m05.amount;}
			if(obj_frc_y4_m06){obj.frc_y4_m06 = obj_frc_y4_m06.amount;}
			if(obj_frc_y4_m07){obj.frc_y4_m07 = obj_frc_y4_m07.amount;}
			if(obj_frc_y4_m08){obj.frc_y4_m08 = obj_frc_y4_m08.amount;}
			if(obj_frc_y4_m09){obj.frc_y4_m09 = obj_frc_y4_m09.amount;}
			if(obj_frc_y4_m10){obj.frc_y4_m10 = obj_frc_y4_m10.amount;}
			if(obj_frc_y4_m11){obj.frc_y4_m11 = obj_frc_y4_m11.amount;}
			if(obj_frc_y4_m12){obj.frc_y4_m12 = obj_frc_y4_m12.amount;}
			
			if(obj_frc_y5){obj.frc_y5 = obj_frc_y5.amount;}
			if(obj_frc_y5_m01){obj.frc_y5_m01 = obj_frc_y5_m01.amount;}
			if(obj_frc_y5_m02){obj.frc_y5_m02 = obj_frc_y5_m02.amount;}
			if(obj_frc_y5_m03){obj.frc_y5_m03 = obj_frc_y5_m03.amount;}
			if(obj_frc_y5_m04){obj.frc_y5_m04 = obj_frc_y5_m04.amount;}
			if(obj_frc_y5_m05){obj.frc_y5_m05 = obj_frc_y5_m05.amount;}
			if(obj_frc_y5_m06){obj.frc_y5_m06 = obj_frc_y5_m06.amount;}
			if(obj_frc_y5_m07){obj.frc_y5_m07 = obj_frc_y5_m07.amount;}
			if(obj_frc_y5_m08){obj.frc_y5_m08 = obj_frc_y5_m08.amount;}
			if(obj_frc_y5_m09){obj.frc_y5_m09 = obj_frc_y5_m09.amount;}
			if(obj_frc_y5_m10){obj.frc_y5_m10 = obj_frc_y5_m10.amount;}
			if(obj_frc_y5_m11){obj.frc_y5_m11 = obj_frc_y5_m11.amount;}
			if(obj_frc_y5_m12){obj.frc_y5_m12 = obj_frc_y5_m12.amount;}
			
			if(obj_frc_y6){obj.frc_y6 = obj_frc_y6.amount;}
			if(obj_frc_y6_m01){obj.frc_y6_m01 = obj_frc_y6_m01.amount;}
			if(obj_frc_y6_m02){obj.frc_y6_m02 = obj_frc_y6_m02.amount;}
			if(obj_frc_y6_m03){obj.frc_y6_m03 = obj_frc_y6_m03.amount;}
			if(obj_frc_y6_m04){obj.frc_y6_m04 = obj_frc_y6_m04.amount;}
			if(obj_frc_y6_m05){obj.frc_y6_m05 = obj_frc_y6_m05.amount;}
			if(obj_frc_y6_m06){obj.frc_y6_m06 = obj_frc_y6_m06.amount;}
			if(obj_frc_y6_m07){obj.frc_y6_m07 = obj_frc_y6_m07.amount;}
			if(obj_frc_y6_m08){obj.frc_y6_m08 = obj_frc_y6_m08.amount;}
			if(obj_frc_y6_m09){obj.frc_y6_m09 = obj_frc_y6_m09.amount;}
			if(obj_frc_y6_m10){obj.frc_y6_m10 = obj_frc_y6_m10.amount;}
			if(obj_frc_y6_m11){obj.frc_y6_m11 = obj_frc_y6_m11.amount;}
			if(obj_frc_y6_m12){obj.frc_y6_m12 = obj_frc_y6_m12.amount;}
			
			

			
			obj.var_y1 = obj.act_y1 - obj.frc_y1;
			obj.var_y1_m01 = obj.act_y1_m01 - obj.frc_y1_m01;
			obj.var_y1_m02 = obj.act_y1_m02 - obj.frc_y1_m02;
			obj.var_y1_m03 = obj.act_y1_m03 - obj.frc_y1_m03;
			obj.var_y1_m04 = obj.act_y1_m04 - obj.frc_y1_m04;
			obj.var_y1_m05 = obj.act_y1_m05 - obj.frc_y1_m05;
			obj.var_y1_m06 = obj.act_y1_m06 - obj.frc_y1_m06;
			obj.var_y1_m07 = obj.act_y1_m07 - obj.frc_y1_m07;
			obj.var_y1_m08 = obj.act_y1_m08 - obj.frc_y1_m08;
			obj.var_y1_m09 = obj.act_y1_m09 - obj.frc_y1_m09;
			obj.var_y1_m10 = obj.act_y1_m10 - obj.frc_y1_m10;
			obj.var_y1_m11 = obj.act_y1_m11 - obj.frc_y1_m11;
			obj.var_y1_m12 = obj.act_y1_m12 - obj.frc_y1_m12;
			
			obj.var_y2 = obj.act_y2 - obj.frc_y2;
			obj.var_y2_m01 = obj.act_y2_m01 - obj.frc_y2_m01;
			obj.var_y2_m02 = obj.act_y2_m02 - obj.frc_y2_m02;
			obj.var_y2_m03 = obj.act_y2_m03 - obj.frc_y2_m03;
			obj.var_y2_m04 = obj.act_y2_m04 - obj.frc_y2_m04;
			obj.var_y2_m05 = obj.act_y2_m05 - obj.frc_y2_m05;
			obj.var_y2_m06 = obj.act_y2_m06 - obj.frc_y2_m06;
			obj.var_y2_m07 = obj.act_y2_m07 - obj.frc_y2_m07;
			obj.var_y2_m08 = obj.act_y2_m08 - obj.frc_y2_m08;
			obj.var_y2_m09 = obj.act_y2_m09 - obj.frc_y2_m09;
			obj.var_y2_m10 = obj.act_y2_m10 - obj.frc_y2_m10;
			obj.var_y2_m11 = obj.act_y2_m11 - obj.frc_y2_m11;
			obj.var_y2_m12 = obj.act_y2_m12 - obj.frc_y2_m12;
			
			obj.var_y3 = obj.act_y3 - obj.frc_y3;
			obj.var_y3_m01 = obj.act_y3_m01 - obj.frc_y3_m01;
			obj.var_y3_m02 = obj.act_y3_m02 - obj.frc_y3_m02;
			obj.var_y3_m03 = obj.act_y3_m03 - obj.frc_y3_m03;
			obj.var_y3_m04 = obj.act_y3_m04 - obj.frc_y3_m04;
			obj.var_y3_m05 = obj.act_y3_m05 - obj.frc_y3_m05;
			obj.var_y3_m06 = obj.act_y3_m06 - obj.frc_y3_m06;
			obj.var_y3_m07 = obj.act_y3_m07 - obj.frc_y3_m07;
			obj.var_y3_m08 = obj.act_y3_m08 - obj.frc_y3_m08;
			obj.var_y3_m09 = obj.act_y3_m09 - obj.frc_y3_m09;
			obj.var_y3_m10 = obj.act_y3_m10 - obj.frc_y3_m10;
			obj.var_y3_m11 = obj.act_y3_m11 - obj.frc_y3_m11;
			obj.var_y3_m12 = obj.act_y3_m12 - obj.frc_y3_m12;
			
			obj.var_y4 = obj.act_y4 - obj.frc_y4;
			obj.var_y4_m01 = obj.act_y4_m01 - obj.frc_y4_m01;
			obj.var_y4_m02 = obj.act_y4_m02 - obj.frc_y4_m02;
			obj.var_y4_m03 = obj.act_y4_m03 - obj.frc_y4_m03;
			obj.var_y4_m04 = obj.act_y4_m04 - obj.frc_y4_m04;
			obj.var_y4_m05 = obj.act_y4_m05 - obj.frc_y4_m05;
			obj.var_y4_m06 = obj.act_y4_m06 - obj.frc_y4_m06;
			obj.var_y4_m07 = obj.act_y4_m07 - obj.frc_y4_m07;
			obj.var_y4_m08 = obj.act_y4_m08 - obj.frc_y4_m08;
			obj.var_y4_m09 = obj.act_y4_m09 - obj.frc_y4_m09;
			obj.var_y4_m10 = obj.act_y4_m10 - obj.frc_y4_m10;
			obj.var_y4_m11 = obj.act_y4_m11 - obj.frc_y4_m11;
			obj.var_y4_m12 = obj.act_y4_m12 - obj.frc_y4_m12;
			
			obj.var_y5     = obj.act_y5 - obj.frc_y5;
			obj.var_y5_m01 = obj.act_y5_m01 - obj.frc_y5_m01;
			obj.var_y5_m02 = obj.act_y5_m02 - obj.frc_y5_m02;
			obj.var_y5_m03 = obj.act_y5_m03 - obj.frc_y5_m03;
			obj.var_y5_m04 = obj.act_y5_m04 - obj.frc_y5_m04;
			obj.var_y5_m05 = obj.act_y5_m05 - obj.frc_y5_m05;
			obj.var_y5_m06 = obj.act_y5_m06 - obj.frc_y5_m06;
			obj.var_y5_m07 = obj.act_y5_m07 - obj.frc_y5_m07;
			obj.var_y5_m08 = obj.act_y5_m08 - obj.frc_y5_m08;
			obj.var_y5_m09 = obj.act_y5_m09 - obj.frc_y5_m09;
			obj.var_y5_m10 = obj.act_y5_m10 - obj.frc_y5_m10;
			obj.var_y5_m11 = obj.act_y5_m11 - obj.frc_y5_m11;
			obj.var_y5_m12 = obj.act_y5_m12 - obj.frc_y5_m12;
			
			obj.var_y6     = obj.act_y6 - obj.frc_y6;
			obj.var_y6_m01 = obj.act_y6_m01 - obj.frc_y6_m01;
			obj.var_y6_m02 = obj.act_y6_m02 - obj.frc_y6_m02;
			obj.var_y6_m03 = obj.act_y6_m03 - obj.frc_y6_m03;
			obj.var_y6_m04 = obj.act_y6_m04 - obj.frc_y6_m04;
			obj.var_y6_m05 = obj.act_y6_m05 - obj.frc_y6_m05;
			obj.var_y6_m06 = obj.act_y6_m06 - obj.frc_y6_m06;
			obj.var_y6_m07 = obj.act_y6_m07 - obj.frc_y6_m07;
			obj.var_y6_m08 = obj.act_y6_m08 - obj.frc_y6_m08;
			obj.var_y6_m09 = obj.act_y6_m09 - obj.frc_y6_m09;
			obj.var_y6_m10 = obj.act_y6_m10 - obj.frc_y6_m10;
			obj.var_y6_m11 = obj.act_y6_m11 - obj.frc_y6_m11;
			obj.var_y6_m12 = obj.act_y6_m12 - obj.frc_y6_m12;

			
			
			obj.pos = obj.pos_y1 + obj.pos_y2 + obj.pos_y3 + obj.pos_y4 + obj.pos_y5 + obj.pos_y6;
			obj.act = obj.act_y1 + obj.act_y2 + obj.act_y3 + obj.act_y4 + obj.act_y5 + obj.act_y6;
			obj.frc = obj.frc_y1 + obj.frc_y2 + obj.frc_y3 + obj.frc_y4 + obj.frc_y5 + obj.frc_y6;
			obj.var = obj.act - obj.frc;
			
			arr.push(obj);
			
		}

		var fileObj = file.create({
			name: 'capex.json',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(arr),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();
		
		
		var csv = get_csv(arr, years, months);
		var fileObj = file.create({
			name: 'capex.csv',
		    fileType: file.Type.PLAINTEXT,
		    contents: JSON.stringify(csv),
		    description: 'Capex Report',
		    encoding: file.Encoding.UTF8,
		    folder: 5926903,
		    isOnline: false
		});
		var fileId = fileObj.save();

    }
	
    function get_pos_years(period_id) {
		var pos = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_pos_year" });
		//results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.filters.push(search.createFilter({ name: "postingperiod", operator: "equalto", values: period_id }));
		results.run().each(function(result) {
			pos.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return pos;
	}
    function get_pos_months(period_id) {
		var pos = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_pos_month" });
		//results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.filters.push(search.createFilter({ name: "postingperiod", operator: "equalto", values: period_id }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[2]);
			pos.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"month"              : period.substring(0,3),
				"period_id"          : parseInt(result.getValue(result.columns[2])),
				"period"             : period,
				"amount"             : round(parseFloat(result.getValue(result.columns[3])))
			});
	        return true;
		});
		return pos;
	}
    
    
    function get_actuals_years(period_id) {
		var actuals = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_actuals_year" });
		//results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.filters.push(search.createFilter({ name: "postingperiod", operator: "equalto", values: period_id }));
		results.run().each(function(result) {
			actuals.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return actuals;
	}
    function get_actuals_months(period_id) {
		var actuals = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_actuals_month" });
		//results.filters.push(search.createFilter({ name: "trandate", operator: "after", values: start_date }));
		results.filters.push(search.createFilter({ name: "postingperiod", operator: "equalto", values: period_id }));
		results.run().each(function(result) {
			var period = result.getText(result.columns[2]);
			actuals.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"month"              : period.substring(0,3),
				"period_id"          : parseInt(result.getValue(result.columns[2])),
				"period"             : period,
				"amount"             : round(parseFloat(result.getValue(result.columns[3])))
			});
	        return true;
		});
		return actuals;
	}
    
    function get_forecasts_years() {
		var forecasts = [];
		var results = search.load({ type: 'customrecord_clgx_cap_bud_forecast', id: "customsearch_clgx2_capex_forecasts_year" });
		results.run().each(function(result) {
			forecasts.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return forecasts;
	}
    function get_forecasts_months() {
		var forecasts = [];
		var results = search.load({ type: 'customrecord_clgx_cap_bud_forecast', id: "customsearch_clgx2_capex_forecasts_month" });
		results.run().each(function(result) {
			var period = result.getText(result.columns[2]);
			forecasts.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"month"              : period.substring(0,3),
				"period_id"          : parseInt(result.getValue(result.columns[2])),
				"period"             : period,
				"amount"             : round(parseFloat(result.getValue(result.columns[3])))
			});
	        return true;
		});
		return forecasts;
	}
    
    function get_projects() {
    	
		var projects = [];
		var results = search.load({ type: 'job', id: "customsearch_clgx2_capex_projects" });
		results.run().each(function(result) {
			
			var facility_id = parseInt(result.getValue(result.columns[3]));
			var facility = result.getText(result.columns[3]);
			if(!facility_id){
				facility_id = 0;
				facility = "- None -";
			}
			projects.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"project"            : result.getValue(result.columns[1]).replace(/,/g, " "),
				"subsidiary"         : result.getText(result.columns[2]).replace(/,/g, " "),
				"facility"           : facility,
				"category"           : result.getText(result.columns[4]),
				"description"        : result.getValue(result.columns[5]).replace(/,/g, " "),
				"start"              : result.getValue(result.columns[6]),
				"end"                : result.getValue(result.columns[7]),
				"estimated"          : parseFloat(result.getValue(result.columns[8])),
				"status"             : result.getText(result.columns[9])
			});
	        return true;
		});
		return projects;
	}
    
    
    function get_periods() {
		var periods = [];
		var start = moment().startOf('year').subtract('years',4).subtract('days',1).format('M/D/YYYY');
		var end = moment().startOf('year').add('years',1).subtract('days',1).format('M/D/YYYY');
		var results = search.load({ type: 'accountingperiod', id: "customsearch_clgx_dw_accounting_period" });
		results.filters = [search.createFilter({ name: "startdate", operator: "within", values: [start,end] })];
		//results.filters = [search.createFilter({ name: "startdate", operator: "before", values: end })];
		results.run().each(function(result) {
			var isyear = result.getValue(result.columns[4]);
			var isquarter = result.getValue(result.columns[5]);
			var mstart = moment(result.getValue(result.columns[6]));
			if(!isyear && !isquarter){
				var year = moment(result.getValue(result.columns[6])).format('YYYY');
				var month = moment(result.getValue(result.columns[6])).format('YYYY');
				periods.push({
					"period_id"    : parseInt(result.getValue(result.columns[0])),
					"period"       : result.getValue(result.columns[1]),
					"year"         : parseInt(moment(result.getValue(result.columns[6])).format('YYYY')),
					"month_id"     : parseInt(moment(result.getValue(result.columns[6])).format('M')),
					"month"        : moment(result.getValue(result.columns[6])).format('MMM')
				});
			}
	        return true;
		});
		return periods;
	}
    

    function get_csv(arr, years, months) {
    	
    		var CSV = ""
        CSV += 'subsidiary,';
        	CSV += 'facility,';
        	CSV += 'project,';
        	CSV += 'category,';
        	CSV += 'description,';
        	CSV += 'start,';
        	CSV += 'end,';
        	CSV += 'estimate,';
        	
        	CSV += 'totals_pos,';
        	CSV += 'totals_actuals,';
        	CSV += 'totals_forecast,';
        	CSV += 'totals_variance,';
        	
        	for (var i = 0; i < years.length; i++ ) {
            	CSV += years[i] + '_pos,';
            	CSV += years[i] + '_actuals,';
            	CSV += years[i] + '_forecast,';
            	CSV += years[i] + '_variance,';
            	for (var j = 0; j < months.length; j++ ) {
                	CSV += months[j] + '_' + years[i] + '_pos,';
                	CSV += months[j] + '_' + years[i] + '_actuals,';
                	CSV += months[j] + '_' + years[i] + '_forecast,';
                	CSV += months[j] + '_' + years[i] + '_variance,';
            	}
        	}
        	CSV += String.fromCharCode(13);
        	
        	for (var i = 0; i < arr.length; i++ ) {
	    		

            	CSV += arr[i].subsidiary + ',';
            	CSV += arr[i].facility + ',';
            	CSV += arr[i].project + ',';
            	CSV += arr[i].category + ',';
            	CSV += arr[i].description + ',';
            	CSV += arr[i].start + ',';
            	CSV += arr[i].end + ',';
            	CSV += arr[i].estimate + ',';
            	
            	CSV += arr[i].pos + ',' + arr[i].act + ',' + arr[i].frc + ',' + arr[i].var + ',';
            	
            	CSV += arr[i].pos_y1 + ',' + arr[i].act_y1 + ',' + arr[i].frc_y1 + ',' + arr[i].var_y1 + ',';
            	CSV += arr[i].pos_y1_m01 + ',' + arr[i].act_y1_m01 + ',' + arr[i].frc_y1_m01 + ',' + arr[i].var_y1_m01 + ',';
            	CSV += arr[i].pos_y1_m02 + ',' + arr[i].act_y1_m02 + ',' + arr[i].frc_y1_m02 + ',' + arr[i].var_y1_m02 + ',';
            	CSV += arr[i].pos_y1_m03 + ',' + arr[i].act_y1_m03 + ',' + arr[i].frc_y1_m03 + ',' + arr[i].var_y1_m03 + ',';
            	CSV += arr[i].pos_y1_m04 + ',' + arr[i].act_y1_m04 + ',' + arr[i].frc_y1_m04 + ',' + arr[i].var_y1_m04 + ',';
            	CSV += arr[i].pos_y1_m05 + ',' + arr[i].act_y1_m05 + ',' + arr[i].frc_y1_m05 + ',' + arr[i].var_y1_m05 + ',';
            	CSV += arr[i].pos_y1_m06 + ',' + arr[i].act_y1_m06 + ',' + arr[i].frc_y1_m06 + ',' + arr[i].var_y1_m06 + ',';
            	CSV += arr[i].pos_y1_m07 + ',' + arr[i].act_y1_m07 + ',' + arr[i].frc_y1_m07 + ',' + arr[i].var_y1_m07 + ',';
            	CSV += arr[i].pos_y1_m08 + ',' + arr[i].act_y1_m08 + ',' + arr[i].frc_y1_m08 + ',' + arr[i].var_y1_m08 + ',';
            	CSV += arr[i].pos_y1_m09 + ',' + arr[i].act_y1_m09 + ',' + arr[i].frc_y1_m09 + ',' + arr[i].var_y1_m09 + ',';
            	CSV += arr[i].pos_y1_m10 + ',' + arr[i].act_y1_m10 + ',' + arr[i].frc_y1_m10 + ',' + arr[i].var_y1_m10 + ',';
            	CSV += arr[i].pos_y1_m11 + ',' + arr[i].act_y1_m11 + ',' + arr[i].frc_y1_m11 + ',' + arr[i].var_y1_m11 + ',';
            	CSV += arr[i].pos_y1_m12 + ',' + arr[i].act_y1_m12 + ',' + arr[i].frc_y1_m12 + ',' + arr[i].var_y1_m12 + ',';
            	
            	
            	CSV += arr[i].pos_y2 + ',' + arr[i].act_y2 + ',' + arr[i].frc_y2 + ',' + arr[i].var_y2 + ',';
            	CSV += arr[i].pos_y2_m01 + ',' + arr[i].act_y2_m01 + ',' + arr[i].frc_y2_m01 + ',' + arr[i].var_y2_m01 + ',';
            	CSV += arr[i].pos_y2_m02 + ',' + arr[i].act_y2_m02 + ',' + arr[i].frc_y2_m02 + ',' + arr[i].var_y2_m02 + ',';
            	CSV += arr[i].pos_y2_m03 + ',' + arr[i].act_y2_m03 + ',' + arr[i].frc_y2_m03 + ',' + arr[i].var_y2_m03 + ',';
            	CSV += arr[i].pos_y2_m04 + ',' + arr[i].act_y2_m04 + ',' + arr[i].frc_y2_m04 + ',' + arr[i].var_y2_m04 + ',';
            	CSV += arr[i].pos_y2_m05 + ',' + arr[i].act_y2_m05 + ',' + arr[i].frc_y2_m05 + ',' + arr[i].var_y2_m05 + ',';
            	CSV += arr[i].pos_y2_m06 + ',' + arr[i].act_y2_m06 + ',' + arr[i].frc_y2_m06 + ',' + arr[i].var_y2_m06 + ',';
            	CSV += arr[i].pos_y2_m07 + ',' + arr[i].act_y2_m07 + ',' + arr[i].frc_y2_m07 + ',' + arr[i].var_y2_m07 + ',';
            	CSV += arr[i].pos_y2_m08 + ',' + arr[i].act_y2_m08 + ',' + arr[i].frc_y2_m08 + ',' + arr[i].var_y2_m08 + ',';
            	CSV += arr[i].pos_y2_m09 + ',' + arr[i].act_y2_m09 + ',' + arr[i].frc_y2_m09 + ',' + arr[i].var_y2_m09 + ',';
            	CSV += arr[i].pos_y2_m10 + ',' + arr[i].act_y2_m10 + ',' + arr[i].frc_y2_m10 + ',' + arr[i].var_y2_m10 + ',';
            	CSV += arr[i].pos_y2_m11 + ',' + arr[i].act_y2_m11 + ',' + arr[i].frc_y2_m11 + ',' + arr[i].var_y2_m11 + ',';
            	CSV += arr[i].pos_y2_m12 + ',' + arr[i].act_y2_m12 + ',' + arr[i].frc_y2_m12 + ',' + arr[i].var_y2_m12 + ',';
            	
            	CSV += arr[i].pos_y3 + ',' + arr[i].act_y3 + ',' + arr[i].frc_y3 + ',' + arr[i].var_y3 + ',';
            	CSV += arr[i].pos_y3_m01 + ',' + arr[i].act_y3_m01 + ',' + arr[i].frc_y3_m01 + ',' + arr[i].var_y3_m01 + ',';
            	CSV += arr[i].pos_y3_m02 + ',' + arr[i].act_y3_m02 + ',' + arr[i].frc_y3_m02 + ',' + arr[i].var_y3_m02 + ',';
            	CSV += arr[i].pos_y3_m03 + ',' + arr[i].act_y3_m03 + ',' + arr[i].frc_y3_m03 + ',' + arr[i].var_y3_m03 + ',';
            	CSV += arr[i].pos_y3_m04 + ',' + arr[i].act_y3_m04 + ',' + arr[i].frc_y3_m04 + ',' + arr[i].var_y3_m04 + ',';
            	CSV += arr[i].pos_y3_m05 + ',' + arr[i].act_y3_m05 + ',' + arr[i].frc_y3_m05 + ',' + arr[i].var_y3_m05 + ',';
            	CSV += arr[i].pos_y3_m06 + ',' + arr[i].act_y3_m06 + ',' + arr[i].frc_y3_m06 + ',' + arr[i].var_y3_m06 + ',';
            	CSV += arr[i].pos_y3_m07 + ',' + arr[i].act_y3_m07 + ',' + arr[i].frc_y3_m07 + ',' + arr[i].var_y3_m07 + ',';
            	CSV += arr[i].pos_y3_m08 + ',' + arr[i].act_y3_m08 + ',' + arr[i].frc_y3_m08 + ',' + arr[i].var_y3_m08 + ',';
            	CSV += arr[i].pos_y3_m09 + ',' + arr[i].act_y3_m09 + ',' + arr[i].frc_y3_m09 + ',' + arr[i].var_y3_m09 + ',';
            	CSV += arr[i].pos_y3_m10 + ',' + arr[i].act_y3_m10 + ',' + arr[i].frc_y3_m10 + ',' + arr[i].var_y3_m10 + ',';
            	CSV += arr[i].pos_y3_m11 + ',' + arr[i].act_y3_m11 + ',' + arr[i].frc_y3_m11 + ',' + arr[i].var_y3_m11 + ',';
            	CSV += arr[i].pos_y3_m12 + ',' + arr[i].act_y3_m12 + ',' + arr[i].frc_y3_m12 + ',' + arr[i].var_y3_m12 + ',';
            	
            	CSV += arr[i].pos_y4 + ',' + arr[i].act_y4 + ',' + arr[i].frc_y4 + ',' + arr[i].var_y4 + ',';
            	CSV += arr[i].pos_y4_m01 + ',' + arr[i].act_y4_m01 + ',' + arr[i].frc_y4_m01 + ',' + arr[i].var_y4_m01 + ',';
            	CSV += arr[i].pos_y4_m02 + ',' + arr[i].act_y4_m02 + ',' + arr[i].frc_y4_m02 + ',' + arr[i].var_y4_m02 + ',';
            	CSV += arr[i].pos_y4_m03 + ',' + arr[i].act_y4_m03 + ',' + arr[i].frc_y4_m03 + ',' + arr[i].var_y4_m03 + ',';
            	CSV += arr[i].pos_y4_m04 + ',' + arr[i].act_y4_m04 + ',' + arr[i].frc_y4_m04 + ',' + arr[i].var_y4_m04 + ',';
            	CSV += arr[i].pos_y4_m05 + ',' + arr[i].act_y4_m05 + ',' + arr[i].frc_y4_m05 + ',' + arr[i].var_y4_m05 + ',';
            	CSV += arr[i].pos_y4_m06 + ',' + arr[i].act_y4_m06 + ',' + arr[i].frc_y4_m06 + ',' + arr[i].var_y4_m06 + ',';
            	CSV += arr[i].pos_y4_m07 + ',' + arr[i].act_y4_m07 + ',' + arr[i].frc_y4_m07 + ',' + arr[i].var_y4_m07 + ',';
            	CSV += arr[i].pos_y4_m08 + ',' + arr[i].act_y4_m08 + ',' + arr[i].frc_y4_m08 + ',' + arr[i].var_y4_m08 + ',';
            	CSV += arr[i].pos_y4_m09 + ',' + arr[i].act_y4_m09 + ',' + arr[i].frc_y4_m09 + ',' + arr[i].var_y4_m09 + ',';
            	CSV += arr[i].pos_y4_m10 + ',' + arr[i].act_y4_m10 + ',' + arr[i].frc_y4_m10 + ',' + arr[i].var_y4_m10 + ',';
            	CSV += arr[i].pos_y4_m11 + ',' + arr[i].act_y4_m11 + ',' + arr[i].frc_y4_m11 + ',' + arr[i].var_y4_m11 + ',';
            	CSV += arr[i].pos_y4_m12 + ',' + arr[i].act_y4_m12 + ',' + arr[i].frc_y4_m12 + ',' + arr[i].var_y4_m12 + ',';
            	
            	CSV += arr[i].pos_y5 + ',' + arr[i].act_y5 + ',' + arr[i].frc_y5 + ',' + arr[i].var_y5 + ',';
            	CSV += arr[i].pos_y5_m01 + ',' + arr[i].act_y5_m01 + ',' + arr[i].frc_y5_m01 + ',' + arr[i].var_y5_m01 + ',';
            	CSV += arr[i].pos_y5_m02 + ',' + arr[i].act_y5_m02 + ',' + arr[i].frc_y5_m02 + ',' + arr[i].var_y5_m02 + ',';
            	CSV += arr[i].pos_y5_m03 + ',' + arr[i].act_y5_m03 + ',' + arr[i].frc_y5_m03 + ',' + arr[i].var_y5_m03 + ',';
            	CSV += arr[i].pos_y5_m04 + ',' + arr[i].act_y5_m04 + ',' + arr[i].frc_y5_m04 + ',' + arr[i].var_y5_m04 + ',';
            	CSV += arr[i].pos_y5_m05 + ',' + arr[i].act_y5_m05 + ',' + arr[i].frc_y5_m05 + ',' + arr[i].var_y5_m05 + ',';
            	CSV += arr[i].pos_y5_m06 + ',' + arr[i].act_y5_m06 + ',' + arr[i].frc_y5_m06 + ',' + arr[i].var_y5_m06 + ',';
            	CSV += arr[i].pos_y5_m07 + ',' + arr[i].act_y5_m07 + ',' + arr[i].frc_y5_m07 + ',' + arr[i].var_y5_m07 + ',';
            	CSV += arr[i].pos_y5_m08 + ',' + arr[i].act_y5_m08 + ',' + arr[i].frc_y5_m08 + ',' + arr[i].var_y5_m08 + ',';
            	CSV += arr[i].pos_y5_m09 + ',' + arr[i].act_y5_m09 + ',' + arr[i].frc_y5_m09 + ',' + arr[i].var_y5_m09 + ',';
            	CSV += arr[i].pos_y5_m10 + ',' + arr[i].act_y5_m10 + ',' + arr[i].frc_y5_m10 + ',' + arr[i].var_y5_m10 + ',';
            	CSV += arr[i].pos_y5_m11 + ',' + arr[i].act_y5_m11 + ',' + arr[i].frc_y5_m11 + ',' + arr[i].var_y5_m11 + ',';
            	CSV += arr[i].pos_y5_m12 + ',' + arr[i].act_y5_m12 + ',' + arr[i].frc_y5_m12 + ',' + arr[i].var_y5_m12 + ',';
            	
            	CSV += arr[i].pos_y6 + ',' + arr[i].act_y6 + ',' + arr[i].frc_y6 + ',' + arr[i].var_y6 + ',';
            	CSV += arr[i].pos_y6_m01 + ',' + arr[i].act_y6_m01 + ',' + arr[i].frc_y6_m01 + ',' + arr[i].var_y6_m01 + ',';
            	CSV += arr[i].pos_y6_m02 + ',' + arr[i].act_y6_m02 + ',' + arr[i].frc_y6_m02 + ',' + arr[i].var_y6_m02 + ',';
            	CSV += arr[i].pos_y6_m03 + ',' + arr[i].act_y6_m03 + ',' + arr[i].frc_y6_m03 + ',' + arr[i].var_y6_m03 + ',';
            	CSV += arr[i].pos_y6_m04 + ',' + arr[i].act_y6_m04 + ',' + arr[i].frc_y6_m04 + ',' + arr[i].var_y6_m04 + ',';
            	CSV += arr[i].pos_y6_m05 + ',' + arr[i].act_y6_m05 + ',' + arr[i].frc_y6_m05 + ',' + arr[i].var_y6_m05 + ',';
            	CSV += arr[i].pos_y6_m06 + ',' + arr[i].act_y6_m06 + ',' + arr[i].frc_y6_m06 + ',' + arr[i].var_y6_m06 + ',';
            	CSV += arr[i].pos_y6_m07 + ',' + arr[i].act_y6_m07 + ',' + arr[i].frc_y6_m07 + ',' + arr[i].var_y6_m07 + ',';
            	CSV += arr[i].pos_y6_m08 + ',' + arr[i].act_y6_m08 + ',' + arr[i].frc_y6_m08 + ',' + arr[i].var_y6_m08 + ',';
            	CSV += arr[i].pos_y6_m09 + ',' + arr[i].act_y6_m09 + ',' + arr[i].frc_y6_m09 + ',' + arr[i].var_y6_m09 + ',';
            	CSV += arr[i].pos_y6_m10 + ',' + arr[i].act_y6_m10 + ',' + arr[i].frc_y6_m10 + ',' + arr[i].var_y6_m10 + ',';
            	CSV += arr[i].pos_y6_m11 + ',' + arr[i].act_y6_m11 + ',' + arr[i].frc_y6_m11 + ',' + arr[i].var_y6_m11 + ',';
            	CSV += arr[i].pos_y6_m12 + ',' + arr[i].act_y6_m12 + ',' + arr[i].frc_y6_m12 + ',' + arr[i].var_y6_m12 + ',';
            	
            	CSV += String.fromCharCode(13);

	    	}
	    	
        	return CSV;
    }

    
	function round(value) {
		  return parseFloat((parseFloat(value)).toFixed(4))
	}
	
	
	function getCurrentAccountingPeriod() {
		var currentDate = moment().format("MMM Y");
		
		var columns = new Array();
		columns.push(search.createColumn("internalid"));
		
		var filters = new Array();
		filters.push(search.createFilter({ name: "periodname", operator: "contains", values: [currentDate.toString()] }));
		
		var searchObject = search.create({ type: "accountingperiod", filters: filters, columns: columns });
		
		var accountingPeriodID = 0;
		searchObject.run().each(function(result) {
			accountingPeriodID = result.getValue("internalid");
		});
		
		return accountingPeriodID.toString();
	}
	
	
    return {
        execute: execute
    };
    

});

