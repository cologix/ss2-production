/**
 * 
 * @author Dan Tansanu - dan.tansanu@cologix.com
 * @date   9/1/2017
 * @description Testing various pieces of code for new functionalities. Displaying various temporary results. Small volume mass updates.
 * 
 * Script Name  : CLGX2_SL_Capex_Projects.js
 * Script File  : CLGX2_SL_Capex_Projects
 * Script ID    : customscript_clgx2_sl_capex_projects
 * Deployment ID: customdeploy_clgx2_sl_capex_projects
 * Internal URL	: /app/site/hosting/scriptlet.nl?script=1479&deploy=1
 * 
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

define([
		"N/file", 
		"N/task", 
		"N/https",
		"N/record", 
		"N/runtime", 
		"N/search", 
		"/SuiteScripts/clgx/libraries/lodash.min", 
		"/SuiteScripts/clgx/libraries/moment.min", 
		"/SuiteScripts/clgx/dw/sync/lib/CLGX2_LIB_DW_Global"
	],
function(file, task, https, record, runtime, search, _, moment, dwg) {

    function onRequest(context) {
		
	    var response = context.response;
	    var request = context.request;

	    var years = [];
	    var start_year = parseInt(moment().startOf('year').subtract('years',4).format('YYYY'));
	    for (var i = 0; i < 5; i++ ) {
	    		years.push(start_year + i);
	    }
	    
	    //var per = get_per();
	    //var periods_ids = _.map(per, 'period_id');
	    var prj = get_prj();
	    var pos = get_pos();
	    var act = get_act();
	    var frc = get_frc();
		
		var projects = [];
		for (var i = 0; i < prj.length; i++ ) {
			
			var obj_pos_y1 = _.find(pos, function(o) { return o.project_id == prj[i].project_id && o.year == years[0]; });
			var obj_pos_y2 = _.find(pos, function(o) { return o.project_id == prj[i].project_id && o.year == years[1]; });
			var obj_pos_y3 = _.find(pos, function(o) { return o.project_id == prj[i].project_id && o.year == years[2]; });
			var obj_pos_y4 = _.find(pos, function(o) { return o.project_id == prj[i].project_id && o.year == years[3]; });
			var obj_pos_y5 = _.find(pos, function(o) { return o.project_id == prj[i].project_id && o.year == years[4]; });
			
			var obj_act_y1 = _.find(act, function(o) { return o.project_id == prj[i].project_id && o.year == years[0]; });
			var obj_act_y2 = _.find(act, function(o) { return o.project_id == prj[i].project_id && o.year == years[1]; });
			var obj_act_y3 = _.find(act, function(o) { return o.project_id == prj[i].project_id && o.year == years[2]; });
			var obj_act_y4 = _.find(act, function(o) { return o.project_id == prj[i].project_id && o.year == years[3]; });
			var obj_act_y5 = _.find(act, function(o) { return o.project_id == prj[i].project_id && o.year == years[4]; });
			
			var obj_frc_y1 = _.find(frc, function(o) { return o.project_id == prj[i].project_id && o.year == years[0]; });
			var obj_frc_y2 = _.find(frc, function(o) { return o.project_id == prj[i].project_id && o.year == years[1]; });
			var obj_frc_y3 = _.find(frc, function(o) { return o.project_id == prj[i].project_id && o.year == years[2]; });
			var obj_frc_y4 = _.find(frc, function(o) { return o.project_id == prj[i].project_id && o.year == years[3]; });
			var obj_frc_y5 = _.find(frc, function(o) { return o.project_id == prj[i].project_id && o.year == years[4]; });
			
			//if(obj_pos_y1 || obj_pos_y2 || obj_pos_y3 || obj_pos_y4 || obj_pos_y5 || obj_act_y1 || obj_act_y2 || obj_act_y3 || obj_act_y4 || obj_act_y5){
				var obj = {
					"project_id"         : prj[i].project_id,
					"project"            : prj[i].project,
					"subsidiary"         : prj[i].subsidiary,
					"facility"           : prj[i].facility,
					"category"           : prj[i].category,
					"status"             : prj[i].status,
					"description"        : prj[i].description,
					"start"              : prj[i].start,
					"end"                : prj[i].end,
					"estimated"          : prj[i].estimated,
					
					"pos"                : 0,
					"act"                : 0,
					"frc"                : 0,
					"var"                : 0,
					
					"pos_y1"             : 0,
					"act_y1"             : 0,
					"frc_y1"             : 0,
					"var_y1"             : 0,
					
					"pos_y2"             : 0,
					"act_y2"             : 0,
					"frc_y2"             : 0,
					"var_y2"             : 0,
					
					"pos_y3"             : 0,
					"act_y3"             : 0,
					"frc_y3"             : 0,
					"var_y3"             : 0,
					
					"pos_y4"             : 0,
					"act_y4"             : 0,
					"frc_y4"             : 0,
					"var_y4"             : 0,
					
					"pos_y5"             : 0,
					"act_y5"             : 0,
					"frc_y5"             : 0,
					"var_y5"             : 0
				};
				
				if(obj_pos_y1){obj.pos_y1 = obj_pos_y1.amount;}
				if(obj_pos_y2){obj.pos_y2 = obj_pos_y2.amount;}
				if(obj_pos_y3){obj.pos_y3 = obj_pos_y3.amount;}
				if(obj_pos_y4){obj.pos_y4 = obj_pos_y4.amount;}
				if(obj_pos_y5){obj.pos_y5 = obj_pos_y5.amount;}
				
				if(obj_act_y1){obj.act_y1 = obj_act_y1.amount;}
				if(obj_act_y2){obj.act_y2 = obj_act_y2.amount;}
				if(obj_act_y3){obj.act_y3 = obj_act_y3.amount;}
				if(obj_act_y4){obj.act_y4 = obj_act_y4.amount;}
				if(obj_act_y5){obj.act_y5 = obj_act_y5.amount;}
				
				if(obj_frc_y1){obj.frc_y1 = obj_frc_y1.amount;}
				if(obj_frc_y2){obj.frc_y2 = obj_frc_y2.amount;}
				if(obj_frc_y3){obj.frc_y3 = obj_frc_y3.amount;}
				if(obj_frc_y4){obj.frc_y4 = obj_frc_y4.amount;}
				if(obj_frc_y5){obj.frc_y5 = obj_frc_y5.amount;}
				
				obj.var_y1 = obj.act_y1 - obj.frc_y1;
				obj.var_y2 = obj.act_y2 - obj.frc_y2;
				obj.var_y3 = obj.act_y3 - obj.frc_y3;
				obj.var_y4 = obj.act_y4 - obj.frc_y4;
				obj.var_y5 = obj.act_y5 - obj.frc_y5;
				
				obj.pos = obj.pos_y1 + obj.pos_y2 + obj.pos_y3 + obj.pos_y4 + obj.pos_y5;
				obj.act = obj.act_y1 + obj.act_y2 + obj.act_y3 + obj.act_y4 + obj.act_y5;
				obj.frc = obj.frc_y1 + obj.frc_y2 + obj.frc_y3 + obj.frc_y4 + obj.frc_y5;
				obj.var = obj.act - obj.frc;
				
				projects.push(obj);
			//}
		}

		var fileObj = file.load({
		    id: 9389430
		});
		html = fileObj.getContents();
		html = html.replace(new RegExp('{arrProjects}','g'), JSON.stringify(projects));
		html = html.replace(new RegExp('{arrYears}','g'), JSON.stringify(years));
		
		response.write( html );
		
    }
    return {
        onRequest: onRequest
    };

    function get_pos() {
		var pos = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_pos_year" });
		results.run().each(function(result) {
			pos.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return pos;
	}
    
    function get_act() {
		var actuals = [];
		var results = search.load({ type: 'transaction', id: "customsearch_clgx2_capex_actuals_year" });
		results.run().each(function(result) {
			actuals.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return actuals;
	}
    
    function get_frc() {
		var forecasts = [];
		var results = search.load({ type: 'customrecord_clgx_cap_bud_forecast', id: "customsearch_clgx2_capex_forecasts_year" });
		results.run().each(function(result) {
			forecasts.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"year"               : parseInt(result.getValue(result.columns[1])),
				"amount"             : round(parseFloat(result.getValue(result.columns[2])))
			});
	        return true;
		});
		return forecasts;
	}
    
    function get_prj() {
    	
		var projects = [];
		var results = search.load({ type: 'job', id: "customsearch_clgx2_capex_projects" });
		results.run().each(function(result) {
			
			var facility_id = parseInt(result.getValue(result.columns[3]));
			var facility = result.getText(result.columns[3]);
			if(!facility_id){
				facility_id = 0;
				facility = "- None -";
			}
			projects.push({
				"project_id"         : parseInt(result.getValue(result.columns[0])),
				"project"            : result.getValue(result.columns[1]).replace(/,/g, " "),
				"subsidiary"         : result.getText(result.columns[2]).replace(/,/g, " "),
				"facility"           : facility,
				"category"           : result.getText(result.columns[4]),
				"description"        : result.getValue(result.columns[5]).replace(/,/g, " "),
				"start"              : result.getValue(result.columns[6]),
				"end"                : result.getValue(result.columns[7]),
				"estimated"          : parseFloat(result.getValue(result.columns[8])),
				"status"             : result.getText(result.columns[9])
			});
	        return true;
		});
		return projects;
	}
    
    function get_per() {
		var periods = [];
		
		var start = moment().startOf('year').subtract('years',4).subtract('days',1).format('M/D/YYYY');
		var end = moment().startOf('year').add('years',1).subtract('days',1).format('M/D/YYYY');
		
		var results = search.load({ type: 'accountingperiod', id: "customsearch_clgx_dw_accounting_period" });
		results.filters = [search.createFilter({ name: "startdate", operator: "within", values: [start,end] })];
		//results.filters = [search.createFilter({ name: "startdate", operator: "before", values: end })];
		results.run().each(function(result) {
			var isyear = result.getValue(result.columns[4]);
			var isquarter = result.getValue(result.columns[5]);
			var mstart = moment(result.getValue(result.columns[6]));
			if(!isyear && !isquarter){
				periods.push({
					"period_id"    : parseInt(result.getValue(result.columns[0])),
					"year"         : parseInt(moment(result.getValue(result.columns[6])).format('YYYY')),
					"month_id"     : moment(result.getValue(result.columns[6])).format('MM'),
					"month"        : moment(result.getValue(result.columns[6])).format('MMM')
				});
			}
	        return true;
		});
		return periods;
	}

	function round(value) {
		  return parseFloat((parseFloat(value)).toFixed(4))
	}
	
	
});
