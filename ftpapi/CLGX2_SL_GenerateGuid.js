/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/sftp", "N/ui/serverWidget"],

function(sftp, sw) {
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	var request = context.request;
    	
    	if(request.method === "GET") {
    		var form = sw.createForm({title: "CLGX SFTP GUID Generator"});
    		
        	form.addCredentialField({
        		id: 'custfield_sftp_password_token',
                label: 'SFTP Raw Password:',
                restrictToScriptIds: ['customscript_clgx2_ss_ftpapi'],
                restrictToDomains: ['70.47.8.102'],
                restrictToCurrentUser: false
        	});
        	
        	form.addSubmitButton();
        	context.response.writePage(form);
    	}
    	
    	if(request.method === "POST") {
    		var token = request.parameters.custfield_sftp_password_token;
    		log.debug({title: "SFTP Token", details: token});
    	}
    }

    return {
        onRequest: onRequest
    };
    
});
