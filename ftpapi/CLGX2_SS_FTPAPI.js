/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date   6/18/2018
 */
define(["N/sftp", "N/file", "N/runtime", "N/error"],
function(sftp, file, runtime, error) {
	/**
	 * @typedef ParamObject
	 * @property {Object} ns
	 * @property {Integer} fileID
	 * @property {Object} ftp
	 * @property {String} name
	 * @property {String} ip
	 * @property {String} dir
	 * @property {String} username
	 * @property {String} guid
	 * @property {String} signature
	 */
	
    function execute(scriptContext) {
    	var sc          = runtime.getCurrentScript();
    	
    	var paramObject = {
    		"ns" : { 
    			"fileID": sc.getParameter("custscript_clgx2_ss_ftpapi_fid") 
    		},
    		"ftp": { 
    			"name"     : sc.getParameter("custscript_clgx2_ss_ftpapi_fn"),
    			"ip"       : sc.getParameter("custscript_clgx2_ss_ftpapi_ip"),
    			"dir"      : sc.getParameter("custscript_clgx2_ss_ftpapi_dir"),
    			"username" : sc.getParameter("custscript_clgx2_ss_ftpapi_un"),
    			"guid"     : sc.getParameter("custscript_clgx2_ss_ftpapi_guid"),
    			"signature": sc.getParameter("custscript_clgx2_ss_ftpapi_signature")
    		}
    	};
    	
    	
    	try {
    		uploadFileToFTP(paramObject);
    	} catch(ex) {
    		log.error({ title: "Error", details: ex });
    	}
    }

    
    /**
     * Uploads a file cabinet file to an SFTP server.
     * 
     * @param {ParamObject} paramObject
     * @returns {null}
     */
    function uploadFileToFTP(paramObject) {
		//var sftpGuid    = "ac1575b5efae4526bacc278184fed57e";
		//var sftpHostKey = "AAAAB3NzaC1yc2EAAAADAQABAAABAQCWIF9ndjKowM5gKLbimnedsnZTqFHZ8HlGl0MtpITBOdsBUxLFZcyBtJsvU5Idu78PIcYYkr5TxYWpmAenSVfxHX/fzu+D5+j+M1zeoZNQdvYYHCbdPVomieU0PQAN0EDFs/e48DF8L0wkSY+cYEYNvXopb63yMb+UshMgHr4ZMunrzrgzLtLcBALP70GOccQ/3goWauRBocrjWbu4WBIzKAIOwPBcovBSvG8sbifMuh8SeHNOPQjmc9K/ltz9MNxXiUe/HaA0cFNTlj9j7B343nDcTIU0WCKTWQZZuqbMPRf2I7/6ni8RBDpqUWEr3Es4EvJzkCmSjzVBnVXMVFwP";
		
		try {
			var con = sftp.createConnection({
				username: paramObject.ftp.username,
				passwordGuid: paramObject.ftp.guid,
				url: paramObject.ftp.ip,
				hostKey: paramObject.ftp.signature
			});
			
			con.upload({
				directory: paramObject.ftp.dir,
				filename: paramObject.ftp.name,
				file: file.load({ id: paramObject.ns.fileID }),
				replaceExisting: true
			});
		} catch(ex) {
			log.debug({ title: "CLGX2_SS_FTPAPI - uploadFileToFTP", details: "Error: " + ex });
		}
	}
    
    
    return {
        execute: execute
    };
    
});
