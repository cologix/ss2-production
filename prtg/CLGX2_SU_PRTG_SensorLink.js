/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @file        CLGX2_SU_PRTG_SensorLink.js
 * @script      CLGX2_SU_PRTG_SensorLink
 * @deployment  CLGX2_SU_PRTG_SensorLink
 * @records     Interconnection|Active Port
 * @actions     Create|Edit
 * @author      Alex Guzenski - alex.guzenski@cologix.com
 * @description Populates a hyperlink field on the Interconnection and Active Port records.
 * @date        10/18/2017
 * 
 * @revision 1.0 - Initial release.
 * @revision 1.1 - Updated the cross connect criteria to leave the PRTG Link field blank if no PRTG ID has been populated.
 */
define(["N/record"],
function(record) {
    function beforeSubmit(context) {
    	try {
    		var nr = context.newRecord;
        	
        	if(nr.type == "customrecord_cologix_crossconnect") {
        		var prtgId = nr.getValue({ fieldId: "custrecord_clgx_prtg_id" });
        		var xcType = nr.getValue({ fieldId: "custrecord_cologix_xc_type" });
        		
        		if((prtgId != null && prtgId != "") && (xcType != null && xcType != "")) {
        			/*
        			 * Populate the link if the cross connect type is one of:
        			 * Bandwidth - IP
        			 * Bandwidth - Peering
        			 * Bandwidth - VRRP
        			 */
        			if(xcType == 25 || xcType == 26 || xcType == 27) {
        				nr.setValue({ fieldId: "custrecord_clgx_link_to_prtg", value: "https://10.250.119.20/sensor.htm?id=" + prtgId + "&tabid=1" });
        			}
        		}
        		
        		var port = nr.getValue("custrecord_clgx_a_end_port");
        		
        		//Get data from the active port record.
        		if(port != null && port != "") {
        			var pr = record.load({ type: "customrecord_clgx_active_port", id: port });
        			
        			var equipment = pr.getValue("custrecord_clgx_active_port_equipment");
        			if(equipment != null && equipment != "") {
        				var er = record.load({ type: "customrecord_cologix_equipment", id: equipment });
        				nr.setValue({ fieldId: "custrecord_clgx_device_name_xc", value: er.getValue("custrecord_clgx_equipment_name") });
        			}
        		}
        	}
        	
        	if(nr.type == "customrecord_clgx_active_port") {
        		var port = nr.getValue({ fieldId: "custrecord_clgx_prtg_sensor_id_port" });
        		
        		if(port != null && port != "") {
        			nr.setValue({ fieldId: "custrecord_prtg_link_port", value: "https://10.250.119.20/sensor.htm?id=" + port + "&tabid=1" });
        		} else {
        			nr.setValue({ fieldId: "custrecord_prtg_link_port", value: "" });
        		}
        	}
    	} catch(ex) {
    		log.error({ title: "Error", details: ex });
    	}
    }

    return {
        beforeSubmit: beforeSubmit
    };
    
});
