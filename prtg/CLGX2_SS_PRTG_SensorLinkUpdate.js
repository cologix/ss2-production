/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/record", "N/search"],
function(record, search) {
    function execute(context) {
    	var searchObject = search.load({ type: "customrecord_cologix_crossconnect", id: "customsearch5546" });
    	
    	searchObject.run().each(function(result) {
    		log.debug({ title: "Interconnection ID", details: result.id });
    		var interconnection = record.load({ type: "customrecord_cologix_crossconnect", id: result.id });
    		interconnection.save();
    		
    		return true;
    	});
    }

    return {
    	execute: execute
    };
    
});
