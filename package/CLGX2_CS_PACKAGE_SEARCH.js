/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord', 'N/record', 'N/ui/dialog', 'N/search', 'N/ui/message'],
/**
 * @param {currentRecord} currentRecord
 * @param {record} record
 * @param {dialog} dialog
 * @param {search} search
 * 
 */
function(currentRecord, record, dialog, search, message) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(context) {
    		
    		var pageMessage = context.request.parameters.custom_packmessage;
    		
    		if(pageMessage){
		    	var myMsg = message.create({
		            title: "My Title", 
		            message: pageMessage, 
		            type: message.Type.CONFIRMATION
		        });
		        
		        // will disappear after 5s
		        myMsg.show({
		            duration: 5000
		        }); 
	        }
    }
	
	function printBadges(context){
		var badgePage = currentRecord.get();
		var numLines = badgePage.getLineCount({sublistId: 'custpage_clgx2_package_received_today'});
		var packagesToPrint = [];
		
		for(var i = 0; i<numLines; i++){
			var selected = badgePage.getSublistValue({
				sublistId: 'custpage_clgx2_package_received_today',
				fieldId: 'custpage_clgx2_pckg_print',
				line: i
			});
			if(selected === true){
				var packageId = badgePage.getSublistValue({
					sublistId: 'custpage_clgx2_package_received_today',
					fieldId: 'custpage_clgx2_pckg_rt_id',
					line: i
				});
				packagesToPrint.push(packageId);
			}
		}	
		
//		var packagesArray = packagesToPrint !== null ? packagesToPrint.split('\u0002') : [];
		
		console.log([packagesToPrint]);
		
		var url = '/app/site/hosting/scriptlet.nl?script=1880&deploy=1&custom_id=';
			
			url += packagesToPrint;
				
			window.open(url);
	}
	
	function saveRecord (context){
		var searchForm = currentRecord.get();
		var trackingNum = searchForm.getValue('custpage_clgx2_tracking_search');
		if(!trackingNum){
	   		dialog.alert({
    			title: 'No Tracking Number',
    			message: 'Please enter a tracking number to search.'
    		});
    		return false;
		} else {
			
	    	var searchFilter = search.createFilter({
	    		name: 'custrecord_clgx_pkg_tracking_num',
	    		operator: search.Operator.IS,
	    		values: trackingNum    		
	    	});
	    	
	    	var packageRecord = '';
			var searchObject = search.create({ type: "customrecord_clgx_package", columns: ['custrecord_clgx_pkg_tracking_num', 'internalid', 'custrecord_clgx_pkg_delivery_status'], filters: searchFilter});
			searchObject.run().each(function(result) {
				packageRecord = {
						id: 	result.getValue('internalid'),
						status:	result.getValue('custrecord_clgx_pkg_delivery_status'),
						statustxt: result.getText('custrecord_clgx_pkg_delivery_status')
				}
				return true;
			});
			
			if(packageRecord.status == 3 || packageRecord.status == 2){
				
		   		dialog.alert({
	    			title: 'Package already received.',
	    			message: 'This package has already been received. It cannot be received again.'
	    		});
		   		
		   		return false;
			}
		}
		return true;
	}
	
	
	function fieldChanged (context) {
		
		var field = context.fieldId;
		if(field === 'custpage_clgx2_facility_filter_print'){
			var printPage = currentRecord.get();
			
			var facilityFilter = printPage.getValue('custpage_clgx2_facility_filter_print');
			console.log(facilityFilter);
			var url = '/app/site/hosting/scriptlet.nl?script=1884&deploy=1&custom_packfilt=';
			
			url += facilityFilter;	
			location.replace(url);
		}
	}

    return {
//    	pageInit	: pageInit,
        printBadges : printBadges,
//        refreshList : refreshList,
        fieldChanged : fieldChanged,
        saveRecord : saveRecord  
    };
    
});
