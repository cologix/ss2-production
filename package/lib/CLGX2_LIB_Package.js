/**
 * @NApiVersion 2.0
 * @NModuleScope SameAccount
 */
define(["N/record", "N/search", "/SuiteScripts/clgx/libraries/lodash.min", "/SuiteScripts/clgx/libraries/moment.min", "N/file", "N/task"],
function(record, search, _, moment, file, task) {
	
	/**
	 * @typedef {Object} CreateRecordObject
	 */
	
	/**
	 * @typedef {Object} UpdateRecordObject
	 * @property {Number} id - The internal id of the package record
	 * @property {Object} fields - A key-value pair list of fields to update.
	 */
	
	/**
	 * @typedef {Object} ListRecordObject
	 */
	
	/**
	 * @typedef {Object} DetailParamObject
	 * @property {Number} id - The internal id of the package record. 
	 */
	
	/**
	 * @typedef {Object} FacilityListObject
	 * @property {Number} companyID - The internal id of the company to pull valid facilities for.  
	 */
	
	
	
	/**
	 * Creates a package record.
	 * 
	 * @access public
	 * @function createPackageRecord
	 * 
	 * @param {CreateRecordObject} recordObject
	 * @returns {Number}
	 */
	function createPackageRecord(recordObject) {
		if(recordObject) {
			var newRecordObject = record.create({ type: "customrecord_clgx_package" });
			
			_.forEach(recordObject, function(value, key) {
				newRecordObject.setValue({ fieldId: key, value: value });
			});
			
			return newRecordObject.save();
		}
	}
	
	
	/**
	 * Creates a record.
	 * 
	 * @access public
	 * @function createRecord
	 * 
	 * @param {CreateRecordObject} recordObject
	 * @param {String} recordObject.type
	 * @param {Boolean} recordObject.dynamic
	 * @param {Object} recordObject.fields
	 * @param {Object} recordObject.sublist
	 * @param {String} recordObject.sublist.list
	 * @param {Object} recordObject.sublist.items
	 * @returns {Number}
	 */
	function createRecord(recordObject) {
		if(recordObject) {
			var newRecordObject = record.create({ 
				type: recordObject.type, 
				isDynamic: (recordObject.dynamic == undefined ? false : recordObject.dynamic) 
			});
			
			_.forEach(recordObject.fields, function(value, key) {
				newRecordObject.setValue({ fieldId: key, value: value });
			});
			
			if(recordObject.sublist !== undefined) {
				_.forEach(recordObject.sublist.items, function(value, key) {
					newRecordObject.selectLine({ sublistId: recordObject.sublist.list, line: 0 });
					newRecordObject.setCurrentSublistValue({ sublistId: recordObject.sublist.list, fieldId: key, value: value });
					newRecordObject.commitLine({ sublistId: recordObject.sublist.list });
				});
			}
			
			return newRecordObject.save();
		}
	}
	
	
	/**
	 * Updates a package record.
	 * 
	 * @access public
	 * @function updatePackageRecord
	 * 
	 * @libraries N/record, lodash
	 * @usage 6
	 * 
	 * @param {UpdateRecordObject} recordObject
	 * @returns {Number}
	 */
	function updatePackageRecord(recordObject) {
		if(recordObject) {
			var newRecordObject = record.load({ type: "customrecord_clgx_package", id: recordObject.id });
			
			_.forEach(recordObject.fields, function(value, key) {
				newRecordObject.setValue({ fieldId: key, value: value });
			});
			
			return newRecordObject.save();
		}
	}
	
	
	/**
	 * Updates a record.
	 * 
	 * @access public
	 * @function updateRecord
	 * 
	 * @libraries N/record, lodash
	 * @usage 6
	 * 
	 * @param {UpdateRecordObject} recordObject
	 * @returns {Number}
	 */
	function updateRecord(recordObject) {
		if(recordObject) {
			var newRecordObject = record.load({ type: recordObject.type, id: recordObject.id });
			
			_.forEach(recordObject.fields, function(value, key) {
				newRecordObject.setValue({ fieldId: key, value: value });
			});
			
			return newRecordObject.save();
		}
	}
	
	
	/**
	 * Returns a list of package records.
	 * 
	 * @access public
	 * @function listPackageRecords
	 * 
	 * @param {ListRecordObject} paramObject
	 * @returns {Array}
	 */
	function listPackageRecords(paramObject) {
		if(paramObject) {
			var returnObject = new Array();
			
			var filters = new Array();
			filters.push(search.createFilter({ name: "custrecord_clgx_pkg_receiving_company", operator: "is", values: [paramObject.companyID] }));
			
			if(paramObject.filter) {
				filters.push(search.createFilter({ name: "custrecord_clgx_pkg_delivery_status", operator: "anyof", values: paramObject.filter }));
			}
			
			var columns = new Array();
			columns.push(search.createColumn("internalid"));
			columns.push(search.createColumn("created"));
			columns.push(search.createColumn("custrecord_clgx_pkg_delivery_status"));
			columns.push(search.createColumn("custrecord_clgx_pkg_tracking_num"));
			columns.push(search.createColumn("custrecord_clgx_pkg_receiving_company"));
			columns.push(search.createColumn("custrecord_clgx_pkg_package_type"));
			columns.push(search.createColumn("custrecord_clgx_pkg_storage_facility"));
			columns.push(search.createColumn("custrecord_clgx_pkg_shipping_carrier"));
			columns.push(search.createColumn("custrecord_clgx_pkg_estimated_delivery"));
			columns.push(search.createColumn("custrecord_clgx_pkg_attention"));
			columns.push(search.createColumn("custrecord_clgx_pkg_internal_track"));
			columns.push(search.createColumn("custrecord_clgx_pkg_delivery_date"));
			columns.push(search.createColumn("custrecord_clgx_pkg_release_date"));
			columns.push(search.createColumn("custrecord_clgx_pkg_claimed_by"));
			columns.push(search.createColumn("custrecord_clgx_package_address_full"));
			columns.push(search.createColumn("custrecord_clgx_pkg_customer_notes"));
			columns.push(search.createColumn("custrecord_clgx_pkg_storage_notes"));
			columns.push(search.createColumn("custrecord_clgx_pkg_release_notes"));
			columns.push(search.createColumn("custrecord_clgx_pkg_company_contact"));
			//columns.push(search.createColumn("custrecord_clgx_pkg_company_contact_oth"));
			columns.push(search.createColumn("custrecord_clgx_pkg_sending_vendor"));
			
			var searchObject = search.create({ type: "customrecord_clgx_package", filters: filters, columns: columns });
			searchObject.run().each(function(result) {
              	var recipient = result.getText("custrecord_clgx_pkg_company_contact");
				
				if(!result.getValue("custrecord_clgx_pkg_company_contact") && !result.getText("custrecord_clgx_pkg_company_contact")) {
					recipient = result.getValue("custrecord_clgx_pkg_attention");
				}
              
				returnObject.push({
					id                    : parseInt(result.getValue("internalid")),
					company_id            : parseInt(result.getValue("custrecord_clgx_pkg_receiving_company")),
					company               : result.getText("custrecord_clgx_pkg_receiving_company"),
					facility_id           : parseInt(result.getValue("custrecord_clgx_pkg_storage_facility")),
					facility              : result.getText("custrecord_clgx_pkg_storage_facility"),
					package_type_id       : parseInt(result.getValue("custrecord_clgx_pkg_package_type")),
					package_type          : result.getText("custrecord_clgx_pkg_package_type"),
					carrier_id            : parseInt(result.getValue("custrecord_clgx_pkg_shipping_carrier")),
					carrier               : result.getText("custrecord_clgx_pkg_shipping_carrier"),
					expected_delivery_date: moment(result.getValue("custrecord_clgx_pkg_estimated_delivery")),
					recipient_id          : result.getValue("custrecord_clgx_pkg_company_contact"),
					recipient             : recipient,
					//recipient_other       : result.getText("custrecord_clgx_pkg_company_contact_oth"),
					recipient_other       : result.getValue("custrecord_clgx_pkg_attention"),
					createdate            : moment(result.getValue("created")),
					tracking_number       : result.getValue("custrecord_clgx_pkg_tracking_num"),
					internal_tracking     : result.getValue("custrecord_clgx_pkg_internal_track"),
					status                : result.getText("custrecord_clgx_pkg_delivery_status"),
					status_id             : parseInt(result.getValue("custrecord_clgx_pkg_delivery_status")),
					deliverydate          : result.getValue("custrecord_clgx_pkg_delivery_date"),
					claimdate             : result.getValue("custrecord_clgx_pkg_release_date"),
					claimedby             : result.getText("custrecord_clgx_pkg_claimed_by"),
					pickupaddress         : result.getValue("custrecord_clgx_package_address_full"),
					sending_vendor        : result.getValue("custrecord_clgx_pkg_sending_vendor"),
					sendernotes           : result.getValue("custrecord_clgx_pkg_customer_notes"),
					storagenotes          : result.getValue("custrecord_clgx_pkg_storage_notes"),
					claimnotes            : result.getValue("custrecord_clgx_pkg_release_notes"),
					createdby_id          : result.getValue("custrecord_clgx_pkg_created_by"),
					createdby             : result.getText("custrecord_clgx_pkg_created_by")
				});
				
				return true;
			});
			
			return { data: returnObject, count: returnObject.length };
		}
	}
	
	
	/**
	 * Returns a package's detail information.
	 * 
	 * @access public
	 * @function getPackageRecordDetail
	 * 
	 * @param {DetailParamObject} paramObject
	 * @returns {Object}
	 */
	function getPackageRecordDetail(paramObject) {
		if(paramObject) {
			var returnObject = new Object();
			var recordObject = record.load({ type: "customrecord_clgx_package", id: parseInt(paramObject.id) });
			
			var createDate = recordObject.getValue("created");
			if(createDate) { createDate = moment(createDate).format("M/D/YYYY"); }
			
			var deliveryDate = recordObject.getValue("custrecord_clgx_pkg_delivery_date");
			if(deliveryDate) { deliveryDate = moment(deliveryDate).format("M/D/YYYY"); }
			
			var claimDate = recordObject.getValue("custrecord_clgx_pkg_release_date");
			if(claimDate) { claimDate = moment(claimDate).format("M/D/YYYY"); }
			
			var expectedDate = recordObject.getValue("custrecord_clgx_pkg_estimated_delivery");
			if(expectedDate) { expectedDate = moment(expectedDate).format("M/D/YYYY"); }
			
			returnObject = {
				id                     : parseInt(paramObject.id),
				company                : parseInt(recordObject.getValue("custrecord_clgx_pkg_receiving_company")),
				facility_id            : parseInt(recordObject.getValue("custrecord_clgx_pkg_storage_facility")),
				facility               : recordObject.getText("custrecord_clgx_pkg_storage_facility"),
				package_type_id        : parseInt(recordObject.getValue("custrecord_clgx_pkg_package_type")),
				package_type           : recordObject.getText("custrecord_clgx_pkg_package_type"),
				carrier_id             : parseInt(recordObject.getValue("custrecord_clgx_pkg_shipping_carrier")),
				carrier                : recordObject.getText("custrecord_clgx_pkg_shipping_carrier"),
				estimated_delivery_date: expectedDate,
				recipient              : recordObject.getText("custrecord_clgx_pkg_company_contact"),
				recipient_id           : recordObject.getValue("custrecord_clgx_pkg_company_contact"),
				recipient_other        : recordObject.getValue("custrecord_clgx_pkg_company_contact_oth"),
				attention              : recordObject.getValue("custrecord_clgx_pkg_attention"),
				createdby              : recordObject.getText("custrecord_clgx_pkg_created_by"),
				createdate             : createDate,
				internal_tracking      : recordObject.getValue("custrecord_clgx_pkg_internal_track"),
				tracking_number        : recordObject.getValue("custrecord_clgx_pkg_tracking_num"),
				status                 : recordObject.getText("custrecord_clgx_pkg_delivery_status"),
				status_id              : parseInt(recordObject.getValue("custrecord_clgx_pkg_delivery_status")),
				deliverydate           : deliveryDate,
				claimdate              : claimDate,
				claimedby              : recordObject.getText("custrecord_clgx_pkg_claimed_by"),
				sending_vendor         : recordObject.getValue("custrecord_clgx_pkg_sending_vendor"),
				pickupaddress          : recordObject.getValue("custrecord_clgx_package_address_full").replace(new RegExp("<br/>", "g"), "\n\r").replace(new RegExp("<br />", "g"), "\n\r"),
				sendernotes            : recordObject.getValue("custrecord_clgx_pkg_customer_notes"),
				storagenotes           : recordObject.getValue("custrecord_clgx_pkg_storage_notes"),
				claimnotes             : recordObject.getValue("custrecord_clgx_pkg_release_notes"),
				relatedcaseid          : recordObject.getValue("custrecord_clgx_package_case") || 0,
				queueid                : recordObject.getValue("custrecord_clgx_pkg_qid") || 0,
				case_status            : recordObject.getValue("custrecord_clgx_pkg_queue_status")
			};
			
			return returnObject;
		}
	}
	
	
	/**
	 * Returns the case attached to the package record.
	 * 
	 * @access public
	 * @function getPackageCase
	 * 
	 * @param {Object} packageDetailObject
	 * 
	 * @return {Object}
	 */
	function getPackageCaseObject(paramObject) {
		if(paramObject) {
			var returnObject = new Object();
			
			if(paramObject.package !== undefined && paramObject.package.relatedcaseid !== 0) {
				var caseObject = record.load({ type: "supportcase", id: paramObject.package.relatedcaseid });
				
				returnObject = {
					id     : parseInt(paramObject.package.relatedcaseid),
					rid    : parseInt(paramObject.package.relatedcaseid).toString(parseInt(paramObject.data.radix)),
					title  : caseObject.getValue("title"),
					number : caseObject.getValue("casenumber"),
					company: caseObject.getValue("company")
				};
			}
			
			return returnObject;
		}
	}
	
	
	/**
	 * Returns a list of package types.
	 * 
	 * @access public
	 * @function general.getPackageTypes
	 * 
	 * @returns {Array}
	 */
	function getPackageTypes() {
		var packageTypeArray = new Array();
		packageTypeArray.push({ value: 0, text: "" });
		
		var columns = new Array();
		columns.push(search.createColumn({ name: "internalid", sort: search.Sort.ASC }));
		columns.push(search.createColumn("name"));
		
		var filters = new Array();
		filters.push(search.createFilter({ name: "isinactive", operator: "is", values: ["F"] }));
		
		var searchObject = search.create({ type: "customlist_clgx_pkg_package_type", columns: columns, filters: filters });
		searchObject.run().each(function(result) {
			packageTypeArray.push({ value: parseInt(result.getValue("internalid")), text: result.getValue("name") });
			return true;
		});
		
		return packageTypeArray;
	}
	
	
	/**
	 * Returns a list of package statuses.
	 * 
	 * @access public
	 * @function general.getPackageStatuses
	 * 
	 * @param {Object} paramObject
	 * @param {Array} paramObject.listFilter
	 * 
	 * @returns {Array}
	 */
	function getPackageStatuses(paramObject) {
		var packageStatusArray = new Array();
		
		var columns = new Array();
		columns.push(search.createColumn("internalid"));
		columns.push(search.createColumn("name"));
		
		var filters = new Array();
		filters.push(search.createFilter({ name: "isinactive", operator: "is", values: ["F"] }));
		
		if(paramObject && paramObject.listFilter) {
			filters.push(search.createFilter({ name: "internalid", operator: "anyof", values: paramObject.listFilter }));
		}
		
		var searchObject = search.create({ type: "customlist_clgx_delivery_status_list", columns: columns, filters: filters });
		searchObject.run().each(function(result) {
			packageStatusArray.push({ value: parseInt(result.getValue("internalid")), text: result.getValue("name") });
			return true;
		});
		
		return packageStatusArray;
	}
	
	
	/**
	 * Returns a list of shipping carriers.
	 * 
	 * @access public
	 * @function general.getShippingCarriers
	 * 
	 * @returns {Array}
	 */
	function getShippingCarriers() {
		var carrierArray = new Array();
		carrierArray.push({ value: 0, text: "" });
		
		var columns = new Array();
		columns.push(search.createColumn("internalid"));
		columns.push(search.createColumn("name"));
		
		var filters = new Array();
		filters.push(search.createFilter({ name: "isinactive", operator: "is", values: ["F"] }));
		
		var searchObject = search.create({ type: "customlist_clgx_pkg_shipment_carrier", columns: columns, filters: filters });
		searchObject.run().each(function(result) {
			carrierArray.push({ value: parseInt(result.getValue("internalid")), text: result.getValue("name") });
			return true;
		});
		
		return carrierArray;
	}
	
	
	/**
	 * Returns a list of customer facilities by the customer's internalid.
	 * 
	 * @access public
	 * @function general.getCustomerFacilities
	 * 
	 * @param {FacilityListObject} paramObject
	 * @returns {Array}
	 */
	function getCustomerFacilities(paramObject) {
		if(paramObject) {
			var facilityArray = new Array();
			var facilityMap   = getFacilityMap();
			
			var columns = new Array();
			columns.push(search.createColumn("internalid"));
			columns.push(search.createColumn("custentity_cologix_facility"));
			
			var filters = new Array();
			filters.push(search.createFilter({ name: "custentity_clgx_is_service_2", operator: "is", values: [1] }));
			filters.push(search.createFilter({ name: "parent", operator: "anyof", values: [paramObject.companyID] }));
			filters.push(search.createFilter({ name: "isinactive", operator: "is", values: ["F"] }));
			
			var searchObject = search.create({ type: "customer", columns: columns, filters: filters });
			searchObject.run().each(function(result) {
				if(result.getValue("custentity_cologix_facility")) {
					var findObject = null;
					if(findObject = _.find(facilityMap, { child: parseInt(result.getValue("custentity_cologix_facility")) })) {
						facilityArray.push({ value: findObject.parent, text: findObject.parent_text });
					} else {
						facilityArray.push({ value: parseInt(result.getValue("custentity_cologix_facility")), text: result.getText("custentity_cologix_facility") });
					}
				}
				
				return true;
			});
			
			return _.orderBy(_.uniqBy(facilityArray, "value"), ["text"], ["ASC"]);
		}
	}
	
	
	/**
	 * Returns an object of facility maps for package routing.
	 * 
	 * @access private
	 * @function getFacilityMap
	 * 
	 * @return {Object}
	 */
	function getFacilityMap() {
		var mapArray = new Array();
		
		var columns = new Array();
		columns.push(search.createColumn("internalid"));
		columns.push(search.createColumn("custrecord_clgx_pkg_rt_parent"));
		columns.push(search.createColumn("custrecord_clgx_pkg_rt_child"));
		
		var searchObject = search.create({ type: "customrecord_clgx_pkg_storage_routes", columns: columns });
		searchObject.run().each(function(result) {
			mapArray.push({ 
				id: parseInt(result.getValue("internalid")), 
				parent: parseInt(result.getValue("custrecord_clgx_pkg_rt_parent")), 
				parent_text: result.getText("custrecord_clgx_pkg_rt_parent"), 
				child: parseInt(result.getValue("custrecord_clgx_pkg_rt_child")),
				child_text: result.getText("custrecord_clgx_pkg_rt_child")
			});
			return true;
		});
		
		return mapArray;
	}
	
	
	/**
     * Returns a list of contacts from a company with the notify shipping enabled.
     * 
     * @access private
     * @function getContacts
     * 
     * @param {Number} companyID
     * @param {Number} order
     * 
     * @returns {Object}
     */
    function getContacts(companyID, order) {
    	var responseArray = new Array();
    	
    	var filters = new Array();
    	var columns = new Array();
    	
    	var searchObject = search.load({ id: "customsearch_clgx_cp_contacts_shipping" });
    	searchObject.filters.push(search.createFilter({ name: "company", operator: "is", values: parseInt(companyID) }));
    	
    	searchObject.run().each(function(result) {
    		var columns = result.columns;
    		
    		var tmpEmailObject   = new Object();
    		var tmpContactObject = new Object();
    		
    		if(order == 1) {
    			if(result.getValue(columns[2]) == "T" || result.getValue(columns[3]) == "T") { //Notify shipping is true
    				var contactName     = result.getValue(columns[4]) + " " + result.getValue(columns[5]); //First name + last name
    				var matrixContactID = result.getValue(columns[1]); //Matrix contact ID
    				tmpEmailObject = {
    					"email": result.getValue(columns[6]),
    					"sms": result.getValue(columns[7])
    				};
    				
    				tmpContactObject = {
    					"value": result.getValue(columns[1]),
    					"value_netsuite": result.getValue(columns[0]),
    					"text": contactName
    				};
    				
    				tmpContactObject[matrixContactID] = tmpEmailObject;
    				responseObject.push(tmpContactObject);
    			}
    		}
    		
    		
    		if(order == 2) {
    			if(result.getValue(columns[2]) == "T") {
    				var contactName = result.getValue(columns[3]) + " " + result.getValue(columns[4]) + "(email:" + result.getValue(columns[5]) + ")";
    				tmpContactObj = {
    					"value": result.getValue(columns[1]),
    					"value_netsuite": result.getValue(columns[0]),
    					"text": contactName
    				};
    				
    				responseArray.push(tmpContactObj);
    			}
    		}
    		
    		
    		if(order == 3) {
    			if(result.getValue(columns[3]) == "T") {
    				var contactName = result.getValue(columns[3]) + " " + result.getValue(columns[4]) + "(email:" + result.getValue(columns[5]) + ";sms:" + result.getValue(columns[6]) + ")";
    				tmpContactObj = {
    					"value": result.getValue(columns[1]),
    					"value_netsuite": result.getValue(columns[0]),
    					"text": contactName
    				};
    				
    				responseArray.push(tmpContactObj);
    			}
    		}
    		
    		
    		if(order == 4) {
    			var contactName     = result.getValue(columns[3]) + " " + result.getValue(columns[4]);
    			var matrixContactID = result.getValue(columns[1]); //Matrix contact ID
    			
    			tmpEmailObject = {
    				"email": result.getValue(columns[5]),
    				"sms": result.getValue(columns[6])
    			};
    			
    			tmpContactObject = {
    				"value": matrixContactID,
    				"value_netsuite": result.getValue(columns[0]),
    				"text": contactName
    			};
    			
    			tmpContactObject[matrixContactID] = tmpEmailObject;
    			responseArray.push(tmpContactObject);
    		}
    		
    		
    		return true;
    	});
    	
    	
    	return responseArray;
    }
    
    
    /**
     * Initializes notification objects for each request.
     * 
     * @access public
     * @function initializeNotifications
     * 
     * @param {Object} paramObject
     * @param {Integer} paramObject.companyID - The current company's internal id.
     * @param {Object} paramObject.responseObject - The object returned to portal.
     * 
     * @return {Object}
     */
    function initializeNotifications(paramObject) {
    	if(paramObject) {
    		paramObject.responseObject["notifications"]       = getContacts(paramObject.companyID, 1);
    		paramObject.responseObject["notifications_email"] = getContacts(paramObject.companyID, 2);
    		paramObject.responseObject["notifications_sms"]   = getContacts(paramObject.companyID, 3);
    		paramObject.responseObject["notifications_all"]   = getContacts(paramObject.companyID, 4);
    		
    		return paramObject.responseObject;
    	}
    }
    
    
    /**
     * Initializes standard request objects for each request.
     * 
     * @access public
     * @function initializePackageResponse
     * 
     * @param {Object} paramObject
     * @param {Array} paramObject.listFilter - The current filter for the status of packages we are viewing.
     * @param {Integer} paramObject.companyID - The current company's internal id.
     * @param {Object} paramObject.responseObject - The object returned to portal.
     * 
     * @return {Object}
     */
    function initializePackageResponse(paramObject) {
    	if(paramObject) {
    		paramObject.responseObject["package"]             = new Object();
    		paramObject.responseObject["package"]["statuses"] = getPackageStatuses({ listFilter: paramObject.listFilter });
    		paramObject.responseObject["package"]["types"]    = getPackageTypes();
    		paramObject.responseObject["package"]["carriers"] = getShippingCarriers();
    		paramObject.responseObject["package"]["facilities"] = getCustomerFacilities({ companyID: paramObject.companyID });
    		
    		return paramObject.responseObject;
    	}
    }
	
    
    /**
	 * Returns the file extension for the file being downloaded to the file cabinet.
	 * 
	 * @access private
	 * @function getFileExtension
	 * 
	 * @param {string} fileName - The full name of the file including extension.
	 * 
	 * @return {string}
	 */
	function getFileExtension(fileName) {
		if(fileName) {
			var nameObj = fileName.split(".");
			var objLength = nameObj.length;
			return ("." + nameObj[objLength - 1]);
		}
	}
	
    
    /**
	 * Returns the NetSuite file type enum based on the file extension.
	 * 
	 * @access private
	 * @function getFileTypeFromExtension
	 * 
	 * @param {string} extension - The file extension. Must be in the format of ".[extension]"
	 * @returns {string}
	 */
	function getFileTypeFromExtension(extension) {
		if(extension) {
			var lowerCaseExtension = extension.toLowerCase();
			
			if(lowerCaseExtension == ".zip") {
				return file.Type.ZIP;
			} else if(lowerCaseExtension == ".pdf") {
				return file.Type.PDF;
			} else if(lowerCaseExtension == ".png") {
				return file.Type.PNGIMAGE;
			} else if(lowerCaseExtension == ".jpg" || lowerCaseExtension == ".jpeg") {
				return file.Type.JPGIMAGE;
			} else if(lowerCaseExtension == ".doc" || lowerCaseExtension == ".docx") {
				return file.Type.WORD;
			} else if(lowerCaseExtension == ".xls" || lowerCaseExtension == ".xlsx") {
				return file.Type.EXCEL;
			} else if(lowerCaseExtension == ".csv") {
				return file.Type.CSV;
			}
		}
	}
    
	
	/**
	 * Downloads a file from the customer portal that was submitted via case.
	 * 
	 * @access public
	 * @function downloadCaseAttachment
	 * 
	 * @param {Object} paramObject
	 * @param {String} paramObject.name - The name of the file.
	 * @param {String} paramObject.contents - The base64 string of file contents.
	 * @param {Number} paramObject.queueID - The internal id of the queue record we are processing.
	 * @param {Number} paramObject.contactID - The internal id of the contact that submitted the file.
	 * @param {Number} paramObject.companyID - The internal id of the company that submitted the file.
	 * 
	 * @returns {Number}
	 */
	function downloadCaseAttachment(paramObject) {
		if(paramObject) {
			if(paramObject.contents) {
				try {
					var caseFileExtension = getFileExtension(paramObject.name);
					var caseFileType      = getFileTypeFromExtension(caseFileExtension);
					
					var caseFile = file.create({ 
						name: ("case-" + paramObject.queueID + "-" + paramObject.contactID + "-" + paramObject.companyID + caseFileExtension), 
						fileType: caseFileType, 
						contents: paramObject.contents.replace("{", "").replace("}", ""), 
						folder: 6343559 
					});
					var fileID = caseFile.save();
					return fileID;
				} catch(ex) {
					log.error({ title: "downloadCaseAttachment - Error", details: ex });
				}
			}
		}
		
		return 0;
	}
	
	
	/**
	 * Schedules a script to be run.
	 * 
     * @access public
     * @function scheduleScript
     * 
     * @param {Object} paramObject
     * @param {Number} paramObject.scriptId - The internal id of the script.
     * @param {Number} paramObject.deploymentId - The internal id of the script's deployment record.
     * @param {Object} paramObject.params - The object used to pass script parameters as a key/value pair.
     * 
     * @return null
     */
    function scheduleScript(paramObject) {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = paramObject.scriptId;
    		
    		if(paramObject.deploymentId !== undefined) {
    			t.deploymentId = paramObject.deploymentId;
    		}
    		
    		t.params       = paramObject.params;
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }
	
	
    return {
    	createPackageRecord      : createPackageRecord,
    	createRecord             : createRecord,
        updatePackageRecord      : updatePackageRecord,
        updateRecord             : updateRecord,
        listPackageRecords       : listPackageRecords,
        getPackageRecordDetail   : getPackageRecordDetail,
        getPackageCaseObject     : getPackageCaseObject,
        initializePackageResponse: initializePackageResponse,
        initializeNotifications  : initializeNotifications,
        
        file: {
        	downloadCaseAttachment: downloadCaseAttachment
        },
        
    	general: {
    		scheduleScript       : scheduleScript,
    		getContacts          : getContacts,
    		getPackageTypes      : getPackageTypes,
    		getShippingCarriers  : getShippingCarriers,
    		getPackageStatuses   : getPackageStatuses,
    		getCustomerFacilities: getCustomerFacilities
        }
    };
    
});
