/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Katy Jones - katy.jones@cologix.com
 * @date 12/11/2019
 */
define(['N/record', 'N/runtime', 'N/search', '/SuiteScripts/clgx/libraries/lodash.min','/SuiteScripts/clgx/libraries/moment.min', 'N/task', 'N/file'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 * @param {_} _
 * @param {moment} moment
 * @param {task} task
 * @param {file} file
 */
function(record, runtime, search, _, moment, task, file) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(context){
    	var rt = runtime.getCurrentScript();
    	
    	var paramObject              		  = new Object();
    	paramObject.savedSearchID    		  = "";
    	paramObject.actionType       		  = rt.getParameter("custscript_clgx2_1893_at");    	
    	paramObject.fileID           		  = parseInt(rt.getParameter("custscript_clgx2_1893_fid"));
    	paramObject.initialIndex     		  = parseInt(rt.getParameter("custscript_clgx2_1893_idx"));
    	paramObject.currentIndex     		  = 0;
    	paramObject.currentInvIndex 		  = parseInt(rt.getParameter("custscript_clgx2_1893_idx"));
    	paramObject.initialInvIndex			  = 0;    	
    	paramObject.currentSubIndex 		  = parseInt(rt.getParameter("custscript_clgx2_1893_lidx"));
    	paramObject.initialSubIndex			  = 0;
    	
    	paramObject.pageSize         		  = parseInt(rt.getParameter("custscript_clgx2_1893_ps"));
    	
    	if(!paramObject.actionType) {
    		paramObject.actionType = "file";
    	}
    	
    	log.debug(paramObject.actionType);
    	
    	if(paramObject.actionType == "file") {
    		paramObject.savedSearchID = "customsearch_clgx_package_inv_queue";
    		var reschedule            = createIDFile(paramObject);
    			
    		if(!reschedule) { 
    			paramObject.currentIndex = 0;
    			paramObject.initialIndex = 0;
    			paramObject.actionType   = "group";
    		}
    			
    		scheduleScript(paramObject);
    	} else if(paramObject.actionType == "group"){
    		var reschedule            = groupInv(paramObject);
    			
    		if(!reschedule) { 
    			paramObject.currentInvIndex = 0;
    			paramObject.initialInvIndex = 0;
    			paramObject.actionType   = "create";
    		}
    			
    		scheduleScript(paramObject);
    	} 
    	else if(paramObject.actionType == "create") {
    		var reschedule            = createInvoices(paramObject);
   			
   			if(reschedule) { 
   				scheduleScript(paramObject);
    		}
    	} 
    	
    	
    }
    
    function createIDFile(paramObject){
    	try {
    		var recordIDArray   = new Array();
        	
        	var processResults  = null;
        	var existingContent = "";
        	var globalIndex     = 0;
        	
        	var filters         = new Array();
        	var columns         = new Array();
        	var ids             = new Array();
        	
        	var searchObject  = search.load({ id: paramObject.savedSearchID }).run();
        	var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
        	
        	log.debug({ title: "searchResults", details: searchResults });
        	
        	processResults = processResultSet(paramObject, searchResults);
        	
        	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
        		recordIDArray = processResults.ids;
        	} else {
        		var existingFile = file.load({ id: paramObject.fileID });
        		existingContent  = JSON.parse(existingFile.getContents());
        		recordIDArray    = recordIDArray.concat(existingContent).concat(processResults.ids);
        	}
        	
        	while (processResults.nextPage == true) {
        		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
        		processResults = processResultSet(paramObject, searchResults);
        		recordIDArray = recordIDArray.concat(processResults.ids);
        	}
        	
        	var fileObject = file.create({ 
    			name: "packagebill_ids.json", 
    			fileType: file.Type.PLAINTEXT,
    			folder: 15771099,
    			contents: JSON.stringify(recordIDArray)
    		});
        	
        	paramObject.fileID = fileObject.save();
        	
        	return processResults.reschedule;
    	} catch(ex) {
    		log.debug({ title: "createIDFile - Error", details: ex });
    	}
    }
    
    /**
     * Processes each page of results.
     */
    function processResultSet(paramObject, searchResults) {
    	try{
	    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    	var localIndex   = 0;
	    	var reschedule   = false;
	    	var nextPage     = false;
	    	var ids          = new Array();
	    	
	    	var resultLength = searchResults.length;
	    	
	    	//Loop through results
	    	for(var r = 0; r < resultLength; r++) {
	    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
	    			break;
	    		}
	    		
		    		ids.push({ 
	    				packageid: 			searchResults[r].getValue('internalid'),
	    				customer: 			searchResults[r].getText('custrecord_clgx_pkg_receiving_company'),
	    				customerid: 		searchResults[r].getValue('custrecord_clgx_pkg_receiving_company'),
	    				facility:			searchResults[r].getValue('custrecord_clgx_pkg_storage_facility'),
	    				location:			searchResults[r].getValue('custrecord_clgx_package_location'),
	    				internaltracking: 	searchResults[r].getValue('custrecord_clgx_pkg_internal_track'),
	    				storagerate : 		searchResults[r].getValue('custrecord_clgx_pkg_storage_rate'),
	    				packagetype: 		searchResults[r].getValue('custrecord_clgx_pkg_package_type'),
	    				daterecieved: 		searchResults[r].getValue('custrecord_clgx_pkg_delivery_date'),
	    				dateclaimed:		searchResults[r].getValue('custrecord_clgx_pkg_release_date'),
	    				startdate: 			searchResults[r].getValue('custrecord_clgx_package_date_charge')
		    		});
	    		
	    		localIndex = localIndex + 1;
	    	}
	    	
	    	if(usage <= 200) {
	    		reschedule = true;
	    		nextPage   = false;
	    	} else if(resultLength < paramObject.pageSize) {
	    		reschedule = false;
	    		nextPage   = false;
	    	} else {
	    		reschedule = false;
	    		nextPage   = true;
	    	}
	    	
	    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
	    	
	    	log.debug({ title: "reschedule", details: reschedule });
	    	log.debug({ title: "nextPage", details: nextPage });
	    	log.debug({ title: "paramObject", details: paramObject });
	    	
	    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    	} catch (ex){
       		log.error({ title: "ProcessResultSet- Error", details: ex });
    	}
    }   
    
    function groupInv(paramObject){
    	try{
	    	var existingFile = file.load({ id: paramObject.fileID });
			var existingContent  = JSON.parse(existingFile.getContents());
			var customerPackageArray =[];
			var custInvArray = [];
			
	    	var groupByFacility = _.groupBy(existingContent, 'facility');
	    	
	    	_.each(groupByFacility, function(value, key){
	    		var locationProcessing = [];
	    		
	    		for(var i=0; i<value.length; i++){
	    			var locationObj = value[i];
	    			locationProcessing.push(locationObj);
	    		}
	    		
	    		customerPackageArray.push(locationProcessing);
	    		return true;
	    	});
	    	
	    	for(var i=0;i<customerPackageArray.length;i++){
	    		var custInv = _.groupBy(customerPackageArray[i], 'customer');
	    		custInvArray.push(custInv);
	    	}
	    	
	    	var fileContent = [];
	    	
	    	for(var i = 0; i<custInvArray.length; i++){
	    		var outerArray = custInvArray[i];
	    		var addContent = Object.keys(outerArray).map(function(key) {
		    		return [String(key), outerArray[key]];
		    	});
	    		fileContent.push(addContent);
	    	}
	    	
	    	var fileObject = file.create({ 
				name: "packagebill_ids.json", 
				fileType: file.Type.PLAINTEXT,
				folder: 15771099,
				contents: JSON.stringify(fileContent)
			});
	    	
	    	paramObject.fileID = fileObject.save();
    	} catch(ex){
       		log.error({ title: "groupInv- Error", details: ex });
    	}
    	
    }
    
    
    function createInvoices(paramObject){
    	try{    		
        	var existingFile 		= file.load({ id: paramObject.fileID });
    		var invObjects  		= JSON.parse(existingFile.getContents());
    		var invObjectsLength 	= invObjects.length;
	    	
	    	var today = moment().format("M/D/YYYY");
	    	var lastDayLastMonth = moment(today).subtract(1, 'months').endOf('month').format("M/D/YYYY");
    		var releaseMonth = moment(releaseDate).format('M');
	    	var lastMonth = moment(lastDayLastMonth).format('M');
    			
    		var usage = runtime.getCurrentScript().getRemainingUsage();
    			    	
    		for(var i = parseInt(paramObject.currentInvIndex); i < invObjectsLength; i++) {  
    			var outerArray = invObjects[i];
    			for(var j = parseInt(paramObject.currentSubIndex); j < outerArray.length; j++){
    				
    				var invData = outerArray[j];
    				var invProcessing = invData[1];
    				var success = createInvoice(invProcessing);
    				log.debug({title: 'Success', details: success});
    				if(success){
    					for(var k = 0; k<invProcessing.length; k++){
    						var packageId = invProcessing[k].packageid;
    						var final = false;
    				    	var releaseDate = invProcessing[k].dateclaimed;
    				    	if(releaseDate){
    				    			final = true;
    				    	}
    		    			
    				    	if(final === true){
	    				    	record.submitFields({
	    		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_final_inv: true, custrecord_clgx_pck_last_mnth_inv: true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
	    		    			});
    				    	} 
    				    	else if(final === false) {
	    				    	record.submitFields({
	    		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_pck_last_mnth_inv: true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
	    		    			});
    				    	}
    					}
    				}
    				paramObject.currentSubIndex = paramObject.currentSubIndex + 1;
            		
    				if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
            			log.debug({title: 'Usage Limit Hit', details: usage});
    					break;
            		}
    			}
    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
           			log.debug({title: 'Usage Limit Hit', details: usage});
        			break;	
        		}
    			
    			else{
	    			paramObject.currentSubIndex = 0;
	    			paramObject.currentInvIndex = paramObject.currentInvIndex + 1;
    			}
        		

    		}
    		
    		paramObject.currentIndex = paramObject.currentIndex + paramObject.currentInvIndex;
    		
    		log.debug({title: 'Final Inv Index', details: paramObject.currentInvIndex});
    		log.debug({title: 'Final Index', details: paramObject.currentIndex});
    		log.debug({title: 'Final Subindex', details: paramObject.currentSubIndex});
    		
    		if(usage <= 200) {
    			return true; //Reschedule
    		}
    	} catch (ex){
       		log.error({ title: "createInvoices- Error", details: ex });
    	}
    	
    }
    
   
    function createInvoice(resultObject){
    	try{
		    if(resultObject.length>0){
    			var custId = resultObject[0].customerid;
		    	var facility = resultObject[0].facility;
		    	var location = resultObject[0].location;
		    	var ciLocation = clgx_return_consolidate_location(location);
		    	var billing = resultObject[0].billing; 
		    	var today = moment().format("M/D/YYYY");
		    	var firstDayNextMonth = new Date(moment().add(1, 'months').startOf('month'));
		    	log.debug({title: 'firstDayNextMonth', details: firstDayNextMonth});
		    	
		    	  
		    	//Build sublist items
		    	var invItemsLen = resultObject.length;
		    	var invItems = [];
		    	for(var i=0; i<invItemsLen; i++){
		    		var itemObj = {
		    				packagetype : resultObject[i].packagetype,
		    				storagerate : resultObject[i].storagerate,
		    				tracking : resultObject[i].internaltracking,
		    				packageid : resultObject[i].packageid,
		    				dateclaimed : resultObject[i].dateclaimed,
		    				startdate: resultObject[i].startdate
		    		  }	
		    		invItems.push(itemObj);
		    	  }
		    	  
		    	  var recordObject = record.create({ type: record.Type.INVOICE, isDynamic: false });
		    	  recordObject.setValue({fieldId:'entity', value: custId});
		    	  recordObject.setValue({fieldId:'location', value: location});
		    	  recordObject.setValue({fieldId:'custbody_clgx_consolidate_locations', value: ciLocation});
		    	  recordObject.setValue({fieldId:'trandate', value: firstDayNextMonth});
		    	  
		    	  //Find and set billing address
		    	  var billingid= '';    	  
		    	  if(billing=='- None -'){
		    		  var billingid = returnBilling(location, custId);
		    	  } if(billingid!=''){
		    		  var custRecord = record.load();
		    		  var NRABs = custRecord.getLineCount({
		    			  sublistId: 'addressbook'
		    		  });
		    		  for(var j = 1; j<=NRABs; j++){
		    			  var internalid = custRecord.getSublistValue({
		    				  sublistId: 'addressbook',
		    				  fieldId: 'internalid',
		    				  line: j
		    			  });
		    			  if(internalid == billingid){
		                      var billaddress=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_text', line:j});
		                      var billaddresslist=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressid', line:j});
		                      var billingaddress_key=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_key', line:j});
		                      var billingaddress_text=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_text', line:j});
		                      var billingaddress_type=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_type', line:j});
		                      var billisresidential=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'isresidential', line:j});
		                      var addressee = custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressee', line:j});
		    			  }
		    		  }
		    		  recordObject.setValue({fieldId:'billaddress', value:billaddress});
		    		  recordObject.setValue({fieldId:'billaddressee', value:addressee});
		    		  recordObject.setValue({fieldId:'billaddresslist', value:billaddresslist});
		    		  recordObject.setValue({fieldId:'billingaddress_key', value:billingaddress_key});
		    		  recordObject.setValue({fieldId:'billingaddress_text', value:billingaddress_text});
		    		  recordObject.setValue({fieldId:'billingaddress_type', value:billingaddress_type});
		    		  recordObject.setValue({fieldId:'billisresidential', value:'F'});
		    	  } else {
		    		  recordObject.setValue({fieldId:'billaddress', value:billing});
		    	  }
		    	  
		    	  
		    	  //Add packages to item sublist
		    	  
		    	  for(var j=0; j<invItemsLen; j++){
		    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'item', line: j, value: 848});
		    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'description', line: j, value: buildMemo(invItems[j].dateclaimed, invItems[j].startdate, invItems[j].tracking)});
		    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'quantity', line: j, value: calculateDays(invItems[j].dateclaimed, invItems[j].startdate)});
		    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'rate', line: j, value: invItems[j].storagerate});
		    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'location', line: j, value: location});
		    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'custcol_clgx_package_id', line: j, value: invItems[j].packageid});
	
		    	  
				    	  //Find and set tax
				    	  var tax = returnTax(location, custId);
				    	  if(tax!=''){
				    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'taxcode', line: j, value: tax[0]});
				    		  recordObject.setSublistValue({sublistId: 'item', fieldId:'taxcode_display', line: j, value: tax[1]});
				    	  }
		
		    	  }
		    	  
		    	  recordObject.save();
		    	  
		    	  return true;
		    }
	    	  
    	  } catch (ex){
         		log.error({ title: "createInvoice- Error", details: ex });
    	  }
    	  
    }	  
         
        
    function returnTax(locationid, customerid){
    	try{
	    	var returntax = '';
	    	var returntaxText = '';
			var customertax = record.load({type: record.Type.CUSTOMER, id: customerid});
			
		    switch(parseInt(locationid)) {
	
		        case 33: // Columbus
		            returntax = customertax.getValue('custentity_clgx_tax_item_col'); // COL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_col'); // COL
		            break;
		        case 34: // COL1&2
		            returntax = customertax.getValue('custentity_clgx_tax_item_col'); // COL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_col'); // COL
		            break;
		        case 39: // COL3
		            returntax = customertax.getValue('custentity_clgx_tax_item_col'); // COL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_col'); // COL
		            break;
		        case 2: // Dallas Infomart
		            returntax = customertax.getValue('custentity_clgx_tax_item_dal'); // DAL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_dal'); // DAL
		            break;
		        case 17: // Dallas Infomart - Ste 2010
		            returntax = customertax.getValue('custentity_clgx_tax_item_dal'); // DAL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_dal'); // DAL
		            break;
		        case 73: // DAL3
		            returntax = customertax.getValue('custentity_clgx_tax_item_dal'); // DAL3
		            returntaxText = customertax.getText('custentity_clgx_tax_item_dal'); // DAL3
		            break;
		        case 31: // JAX1
		            returntax = customertax.getValue('custentity_clgx_tax_item_jax'); // JAX1
		            returntaxText = customertax.getText('custentity_clgx_tax_item_jax'); // JAX1
		            break;
		        case 40: // JAX2
		            returntax = customertax.getValue('custentity_clgx_tax_item_jax'); // JAX2
		            returntaxText = customertax.getText('custentity_clgx_tax_item_jax'); // JAX2
		            break;
		        case 42: // LAK1
		            returntax = customertax.getValue('custentity_clgx_tax_item_lak'); // LAK1
		            returntaxText = customertax.getText('custentity_clgx_tax_item_lak'); // LAK1
		            break;
		        case 16: // MIN1&2
		            returntax = customertax.getValue('custentity_clgx_tax_item_min'); // MIN
		            returntaxText = customertax.getText('custentity_clgx_tax_item_min'); // MIN
		            break;
		        case 35: // MIN3
		            returntax = customertax.getValue('custentity_clgx_tax_item_min'); // MIN
		            returntaxText = customertax.getText('custentity_clgx_tax_item_min'); // MIN
		            break;
		        case 5: // MTL1
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 8: // MTL2
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 9: // MTL3
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 10: // MTL4
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 11: // MTL5
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 12: // MTL6
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 27: // MTL7
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 74: // MTL8
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 77: // MTL9
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 78: // MTL10
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        case 79: // MTL11
		            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
		            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
		            break;
		        ///NNJ
		        case 53: // NNJ1
		            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ1
		            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ1
		            break;
		        case 54: // NNJ2
		            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ2
		            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ2
		            break;
		        case 55: // NNJ3
		            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ3
		            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ3
		            break;
		        case 56: // NNJ4
		            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ4
		            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ4
		            break;
		        case 13: // 156 Front Street West
		            returntax = customertax.getValue('custentity_clgx_tax_item_tor');// TOR
		            returntaxText = customertax.getText('custentity_clgx_tax_item_tor');// TOR
		            break;
		        case 14: // Barrie Office
		            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
		            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
		            break;
		        //TOR
		        case 6: // TOR1
		            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
		            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
		            break;
		        case 15: // TOR2
		            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
		            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
		            break;
		        case 71: //TOR3 
		            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
		            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
		            break;   
		        case 18: // 151 Front Street West - Ste 822
		            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
		            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
		            break;
		        case 28: // 1050 Pender
		            returntax = customertax.getValue('custentity_clgx_tax_item_van'); // VAN
		            returntaxText = customertax.getText('custentity_clgx_tax_item_van'); // VAN
		            break;
		        case 7: // Harbour Centre
		            returntax = customertax.getValue('custentity_clgx_tax_item_van'); // VAN
		            returntaxText = customertax.getText('custentity_clgx_tax_item_van'); // VAN
		            break;
		        case 60: // VAN3
		            returntax = customertax.getValue('custentity_clgx_tax_item_van'); // VAN3
		            returntaxText = customertax.getText('custentity_clgx_tax_item_van'); // VAN3
		            break;
		
		        default:
		            returntax = '';
	
			    }
		    var returntaxArray=[returntax,returntaxText]
		    return returntaxArray;
    	} catch(ex){
     		log.error({ title: "returnTax-Error", details: ex });
    	}
    
    }
    
    function returnBilling(locationid, customerid){
    	try{
	        var returnbilling = '';
	
	        return returnbilling;
    	} catch(ex){
     		log.error({ title: "returnBilling-Error", details: ex });
    	}
    }
    
    function buildMemo(claim, startDate, tracking){
    	try{
	    	var today = moment().format("M/D/YYYY");
	    	var firstDayBillCycle = moment(today).subtract(1, 'months');
	//    	var firstDayBillCycle = moment('10/31/2019');
	    	var lastDayBillCycle = moment(today).subtract(1, 'days');
	//    	var lastDayBillCycle = moment('12/20/2019');
	    	var dateClaimed = claim;
	    	var dateChargeStart = startDate;
	    	var firstChargeDay = '';
	    	var lastChargeDay = '';
	    	var memo = '';
	    	    	
	    	if(!(moment(dateChargeStart).isBefore(firstDayBillCycle) || moment(dateChargeStart).isAfter(lastDayBillCycle))){
	    		firstChargeDay = moment(dateChargeStart).format("M/D/YYYY");
	    	} else {
	    		firstChargeDay = moment(firstDayBillCycle).format("M/D/YYYY");
	    	}
	    	
	    	if(dateClaimed){
		    	if(!(moment(dateClaimed).isBefore(firstDayBillCycle) || moment(dateClaimed).isAfter(lastDayBillCycle))){
		    		lastChargeDay = moment(dateClaimed).format("M/D/YYYY");
		    	} else {
		    		lastChargeDay = moment(lastDayBillCycle).format("M/D/YYYY");
		    	}
	    	} else {
	    		lastChargeDay = moment(lastDayBillCycle).format("M/D/YYYY");
	    	}
	    	
	    	memo = 'Physical Storage Fees for Shipment ' + tracking + ' ' + firstChargeDay + ' - ' + lastChargeDay;
	    	
	    	return memo;
    	} catch (ex){
     		log.error({ title: "buildMemo-Error", details: ex });
    	}
    }
    
    function clgx_return_consolidate_location (locationid){
    	try{
	        var consolocation = 18;
	        switch(parseInt(locationid)) {
	            case 2: // Dallas Infomart
	                consolocation = 18; // DAL
	                break;
	            case 5: // MTL1
	                consolocation = 24; // MTL
	                break;
	            case 6: // 151 Front Street West
	                consolocation = 21; // TOR
	                break;
	            case 7: // Harbour Centre
	                consolocation = 20; // VAN
	                break;
	            case 8: // MTL2
	                consolocation = 24; // MTL
	                break;
	            case 9: // MTL3
	                consolocation = 24; // MTL
	                break;
	            case 10: // MTL4
	                consolocation = 24; // MTL
	                break;
	            case 11: // MTL5
	                consolocation = 24; // MTL
	                break;
	            case 12: // MTL6
	                consolocation = 24; // MTL
	                break;
	            case 13: // 156 Front Street West
	                consolocation = 21; // TOR
	                break;
	            case 14: // Barrie Office
	                consolocation = 21; // TOR
	                break;
	            case 15: // 905 King Street West
	                consolocation = 21; // TOR
	                break;
	            case 16: // MIN1&2
	                consolocation = 22; // MIN
	                break;
	            case 17: // Dallas Infomart - Ste 2010
	                consolocation = 18; // DAL
	                break;
	            case 18: // 151 Front Street West - Ste 822
	                consolocation = 23; // 151 Front Street West - Ste 822
	                break;
	            case 27: // MTL7
	                consolocation = 24; // MTL
	                break;
	            case 28: // 1050 Pender
	                consolocation = 20; // VAN
	                break;
	            case 29: // JAX1
	                consolocation = 25; // JAX1
	                break;
	            case 31: // JAX1
	                consolocation = 25; // JAX1
	                break;
	            case 33: // Columbus
	                consolocation = 26; // COL
	                break;
	            case 34: // COL1&2
	                consolocation = 26; // COL
	                break;
	            case 35: // MIN3
	                consolocation = 22; // MIN
	                break;
	            case 39: // COL3
	                consolocation = 26; // COL
	                break;
	            case 40: // JAX2
	                consolocation = 27; // JAX2
	                break;
	            case 42: // LAK1
	                consolocation = 28; // Jacksonville
	                break;
	            case 52: // NNJ
	                consolocation = 29; // New Jersey
	                break;
	            case 53: // NNJ1
	                consolocation = 29; // New Jersey
	                break;
	            case 54: // NNJ2
	                consolocation = 29; // New Jersey
	                break;
	            case 55: // NNJ3
	                consolocation = 29; // New Jersey
	                break;
	            case 56: // NNJ4
	                consolocation = 29; // New Jersey
	                break;
	            case 74: // MTL8
	                consolocation = 24; // MTL
	                break;
	            case 77: // MTL9
	                consolocation = 24; // MTL
	                break;
	            case 78: // MTL10
	                consolocation = 24; // MTL
	                break;
	            default:
	                consolocation = 18;
	        }
	        return consolocation;
    	} catch(ex){
         	log.error({ title: "consolidateLocation-Error", details: ex });
    	}
    }

    function scheduleScript(paramObject) {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = runtime.getCurrentScript().id;
    		log.debug({title: 'Reschedule Script Index', details: paramObject.currentIndex});
    		t.params       = {
    			custscript_clgx2_1893_at : paramObject.actionType,
    			custscript_clgx2_1893_fid: paramObject.fileID,
        		custscript_clgx2_1893_idx: paramObject.currentIndex,
        		custscript_clgx2_1893_ps : paramObject.pageSize,
        		custscript_clgx2_1893_lidx: paramObject.currentSubIndex
    		};
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }
    
    function calculateDays(claim, start){
    	try{
	    	var today = moment().format("M/D/YYYY");
	    	var firstDayBillCycle = moment(today).subtract(1, 'months');
	//    	var firstDayBillCycle = moment('10/31/2019');
	    	var lastDayBillCycle = moment(today).subtract(1, 'days');
	//    	var lastDayBillCycle = moment('12/20/2019');
	    	var dateClaimed = claim;
	    	var dateChargeStart = start;
	    	var firstChargeDay = '';
	    	var lastChargeDay = '';
	    	var billDays = '';
	    	    	
	    	if(!(moment(dateChargeStart).isBefore(firstDayBillCycle) || moment(dateChargeStart).isAfter(lastDayBillCycle))){
	    		firstChargeDay = dateChargeStart;
	    	} else {
	    		firstChargeDay = firstDayBillCycle;
	    	}
	    	
	    	if(dateClaimed){
		    	if(!(moment(dateClaimed).isBefore(firstDayBillCycle) || moment(dateClaimed).isAfter(lastDayBillCycle))){
		    		lastChargeDay = dateClaimed;
		    	} else {
		    		lastChargeDay = lastDayBillCycle;
		    	}
	    	} else {
	    		lastChargeDay = lastDayBillCycle;
	    	}
	    	
	    	billDays = parseInt(moment(lastChargeDay).diff(firstChargeDay, 'days')+1);
	    	
	    	return billDays;
    	} catch(ex){
     		log.error({ title: "calculateDays- Error", details: ex });
    	}
    }

    return {
        execute: execute
    };
    
});
