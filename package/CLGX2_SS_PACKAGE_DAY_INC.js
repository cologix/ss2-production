/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Katy Jones - katy.jones@cologix.com
 * @date 9/19/2019
 */
define(['N/record', 'N/search', '/SuiteScripts/clgx/libraries/lodash.min', '/SuiteScripts/clgx/libraries/moment.min', 'N/email', 'N/runtime', 'N/task'],
/**
 * @param {record} record
 * @param {search} search
 */
function(record, search, _, moment, email, runtime, task) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(context) {
    	
		var rt = runtime.getCurrentScript();
    	
    	var paramObject           = new Object();
    	paramObject.actionType    = rt.getParameter("custscript_clgx_1880_at");
    	
    	if(!paramObject.actionType){
    		paramObject.actionType = 'start';
    	}
    	
    	log.debug(paramObject.actionType);
    	

    	
    	if (paramObject.actionType == 'start'){
    		startChargeCounter();
    	} 
    	
    	if(paramObject.actionType == 'flag'){
	   		flagRecords();
    	}
    	
    	if(paramObject.actionType == 'increment'){
    		incrementDays();
    	}
    	
//    	sendReminderEmail();
    	
    }
    

    	
    function startChargeCounter(){
    	try{
		    var savedSearchID = 'customsearch_clgx_package_start_counter';
		    var count = 0;
		    var reschedule = false;				
			var searchObject = search.load({ id: savedSearchID });

			searchObject.run().each(function(result) {
				var usage        = runtime.getCurrentScript().getRemainingUsage();
				count++;
				if(count == 4000){
					reschedule = true;
					return false;
				} else if (usage <= 200){
					reschedule = true;
			    	return false;
				} else {		
			
					var packageId = result.getValue('internalid');
					var dateReceived = result.getValue('custrecord_clgx_pkg_delivery_date');
					startChargeDays(packageId, dateReceived);
									
					return true;
					
				}
			});			
			
	    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    	
	    	log.debug({title: 'Function Name', details: 'startChargeCounter'});
	    	log.debug({title: 'Reschedule?', details: reschedule});
	    	log.debug({title: 'Count', details: count});
	    	log.debug({title: 'Usage', details: usage});
			
			if(reschedule == false){
				rescheduleScript('flag');
				return false;
			} else {
				rescheduleScript('start');
				return false;
			}

    	} catch(ex) {
    		log.error({ title: "startChargeCounter- Error", details: ex });
    	}
    	
    }
    	
    function startChargeDays(packageId, dateReceived){
       		try {
       			var chargeStartDate = moment(dateReceived).add(7,'day').format('M/D/YYYY');
           		record.submitFields({ type: "customrecord_clgx_package", id: packageId, values: {  custrecord_clgx_package_over_grace_prd : true, custrecord_clgx_package_date_charge : chargeStartDate} });
           	} catch(ex) {
           		log.error({ title: "startChargeDays- Error", details: ex });
           	}
   	}   
    
    /**
     * Searches for packages that need billing days calculated
     * 
     * @access public
     * @function incrementDays
     * 
     * @return undefined
     */
    
    function incrementDays(){
    	
    	try{	
	    	var count = 0;
	    	var reschedule = false;
	    	var savedSearchID = 'customsearch_clgx_package_charge_counter';
	    	var searchObject = search.load({ id: savedSearchID });
	    	searchObject.run().each(function(result) {
		    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    		count++;
				if(count == 4000){
					reschedule = true;
					return false;
				} else if (usage <= 200){
					reschedule = true;
			    	return false;
				}
				else {
		    		counter(result);   			  			
		    		return true;
				}
	    	});
	    	
	    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    	
	    	log.debug({title: 'Function Name', details: 'incrementDays'})
	    	log.debug({title: 'Reschedule?', details: reschedule});
	    	log.debug({title: 'Count', details: count});
	    	log.debug({title: 'Usage', details: usage});
	    	
	    	if(reschedule == true){
	    		rescheduleScript('increment');
	    		return false;
	    	}
	    	
    	} catch(ex) {
       		log.error({ title: "incrementDays- Error", details: ex });
       	}
    }
    
    /**
     * Calculates the billing days for last month for the package record
     * 
     * @access public
     * @function counter
     * 
     * @param {Object} resultObject
     * @return undefined
     */
    
    function counter(resultObject){
    	try{
	    	var today = moment().format("M/D/YYYY");
	    	var dateChargesBegin = resultObject.getValue('custrecord_clgx_package_date_charge');
	    	var lastMonthCharges = resultObject.getValue('custrecord_clgx_package_bill_days');
	    	var firstDayCurrentMonth = moment(today).startOf('month').format('M/D/YYYY');
	    	var lastDayLastMonth = moment(today).subtract(1, 'months').endOf('month').format("M/D/YYYY");
	    	var firstDayLastMonth = moment(today).subtract(1, 'months').startOf('month').format("M/D/YYYY");
	    	
	    	var releaseDate = resultObject.getValue('custrecord_clgx_pkg_release_date');
	    	
	    	var currentMonth = moment(today).format('M');
	    	var chargeStartMonth = moment(dateChargesBegin).format('M');
	    	var releaseMonth = moment(releaseDate).format('M');
	    	var lastMonth = moment(lastDayLastMonth).format('M');
	    	var startMonth = moment(firstDayLastMonth).format('M');
	    	
	    	var monthToDate = 	parseInt(resultObject.getValue('custrecord_clgx_package_current_days'));
	    	var packageId = 	resultObject.getValue('id');
	
	    	var lastMonthCharges = null;
	    	
	    	if(releaseDate){
	    		if(releaseMonth === currentMonth){
	    			if (chargeStartMonth === lastMonth){
	    				lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(dateChargesBegin, 'days'))+1;
	    				record.submitFields({
		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_bill_days : lastMonthCharges, custrecord_clgx_package_days_calc : true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
		    			});
	    			} else {
	    				lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(firstDayLastMonth, 'days'))+1;
	    				record.submitFields({
		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_bill_days : lastMonthCharges, custrecord_clgx_package_days_calc : true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
		    			});
	    			}
	    		} else if (releaseMonth === lastMonth){
	    			if(chargeStartMonth === lastMonth){
	    				lastMonthCharges = parseInt(moment(releaseDate).diff(dateChargesBegin, 'days'))+1;
	    				record.submitFields({
		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_bill_days : lastMonthCharges, custrecord_clgx_package_days_calc : true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
		    			});    			
	    			} else {
	    				lastMonthCharges = parseInt(moment(releaseDate).format('D'));
	    				record.submitFields({
		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_bill_days : lastMonthCharges, custrecord_clgx_package_days_calc : true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
		    			});    				
	    			}
	    		} else {
					record.submitFields({
	    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_bill_days : lastMonthCharges, custrecord_clgx_package_days_calc : true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
	    			});
	    		}
	    	} else {
	    		if (chargeStartMonth === lastMonth){
					var lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(dateChargesBegin, 'days'));
					record.submitFields({
	    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_bill_days : lastMonthCharges, custrecord_clgx_package_days_calc : true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
	    			});
	    		} else {
					var lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(firstDayLastMonth, 'days'))+1;
					record.submitFields({
	    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_package_bill_days : lastMonthCharges, custrecord_clgx_package_days_calc : true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
	    			});
	    		}
    	}

    	} catch (ex){
    		log.debug({title: 'counter-Error', details: ex});
    	}
    }
    
    function sendReminderEmail(){
    	var savedSearchId = 'customsearch_clgx_package_reminder_email';
    	var searchObject = search.load({ id: savedSearchId });
    	searchObject.run().each(function(result) {
 			buildEmail(result);
    		return true;
    	});
    }
    
    function buildEmail(resultObject){
    	var facility = resultObject.getValue('custrecord_clgx_pkg_storage_facility');
    	var packageType = resultObject.getValue('custrecord_clgx_pkg_package_type');
    	
    	var replyToEmail = buildReplyToMap(facility);
    	
    	var emailTemplateObject  = record.load({ type: "emailtemplate", id: 202});
    	var emailTemplateSubject = emailTemplateObject.getValue("subject");
    	var emailTemplateBody    = emailTemplateObject.getValue("content");
    	
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{trackingnumber}"), resultObject.getValue('custrecord_clgx_pkg_tracking_num'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facility}"), resultObject.getValue('custrecord_clgx_pkg_storage_facility'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{deliverydate}"), resultObject.getValue('custrecord_clgx_pkg_delivery_date'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facilityaddress}"), resultObject.getValue('custrecord_clgx_package_address_full'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{daysinstorage}"), resultObject.getValue('custrecord_clgx_package_total_days'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{storagerate}"), resultObject.getValue('custrecord_clgx_pkg_storage_rate'));
    	
    	var companyContactArray = new Array();
    	var searchObject        = search.load({ id: "customsearch_clgx_pkg_emails" });
    	
    	log.debug({title: 'Package Receiving Company Filter', details: resultObject.getValue("custrecord_clgx_pkg_receiving_company")});
    	searchObject.filters.push(search.createFilter({ name: "company", operator: "anyof", values: [resultObject.getValue("custrecord_clgx_pkg_receiving_company")] }));
    	searchObject.run().each(function(result) {
    		companyContactArray.push(result.getValue("internalid"));
    		log.debug({title: 'Contact Internal Id', details: result.getValue("internalid")})
    		return true;
    	});
    	
    	log.debug({title: 'Company Contact Array', details: companyContactArray});
    	
    	var companyContactArrayLength = companyContactArray.length;
    	if(companyContactArrayLength > 10) {
    		var companyContactObject       = _.chunk(companyContactArray, 10);
    		var companyContactObjectLength = companyContactObject.length;
    		
    		for(var contactSet = 0; contactSet < companyContactArrayLength; contactSet++) {
    			try{
	    			email.send({ 
	    				author: 1937290, 
	    				replyTo: replyToEmail,
	    				recipients: companyContactObject[contactSet], 
	    				subject: emailTemplateSubject, 
	    				body: emailTemplateBody, 
	    				relatedRecords: {
	    					customRecord: {
	    						id: resultObject.id,
	    						recordType: "customrecord_clgx_package"
	    					}
	    				} 
	    			});
    			} catch(ex){
    	    		log.debug({title: 'Reminder Email-Error', details: companyContactObject});
    	    	}
    		}
    	} else {
    			try{
		    		email.send({ 
						author: 1937290, 
						replyTo: replyToEmail,
						recipients: companyContactArray, 
						subject: emailTemplateSubject, 
						body: emailTemplateBody, 
						relatedRecords: {
							customRecord: {
								id: resultObject.id,
								recordType: "customrecord_clgx_package"
							}
						} 
					});
    			} catch(ex){
    	    		log.debug({title: 'Reminder Email-Error', details: companyContactArray});
    	    	}
    	}
 
    }
    
    /**
     * Returns an email based off of the facility on the package record.
     * 
     * @access private
     * @function buildReplyToMap
     * @usage 0
     * 
     * @param {Number} facilityID
     * @returns {String}
     */
    
    function buildReplyToMap(facilityID) {
    	if(facilityID) {
    		//Columbus
    		if(facilityID == 24 || facilityID == 29) { return "col.operations@cologix.com"; }
    		
    		//Dallas
    		if(facilityID == 3 || facilityID == 18 || facilityID == 44) { return "dal.operations@cologix.com"; }
    		
    		//JAX1
    		if(facilityID == 21) { return "jax1.operations@cologix.com"; }
    		
    		//JAX2
    		if(facilityID == 27) { return "jax2.operations@cologix.com"; }
    		
    		//LAK
    		if(facilityID == 28) { return "lak.operations@cologix.com"; }
    		
    		//MIN
    		if(facilityID == 17 || facilityID == 25) { return "min.operations@cologix.com"; }
    		
    		//Montreal
    		if(facilityID == 2 || facilityID == 5 || facilityID == 4 || facilityID == 9 || facilityID == 6 || facilityID == 7 || facilityID == 19 || facilityID == 45 || facilityID == 51 || facilityID == 52 || facilityID == 53) { return "mtl.operations@cologix.com"; }
    		
    		//NNJ
    		if(facilityID == 34 || facilityID == 33 || facilityID == 35 || facilityID == 36) { return "nnj.operations@cologix.com"; }
    		
    		//TOR
    		if(facilityID == 8 || facilityID == 15 || facilityID == 43) { return "tor.operations@cologix.com"; }
    		
    		//VAN
    		if(facilityID == 14 || facilityID == 20 || facilityID == 42) { return "van.operations@cologix.com"; }
    	}
    }
    
    function flagRecords(){
    	try{	
	    	var count = 0;
	    	var savedSearchID = 'customsearch_clgx_package_charge_flag';
		    var reschedule = false;
	    	var searchObject = search.load({ id: savedSearchID });
	    	searchObject.run().each(function(result) {
		    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    		count++;
				if(count == 4000){
					reschedule = true;
					return false;
				} else if (usage <= 200){
					reschedule = true;
			    	return false;
				}
				else {
		    		flagRecord(result);   			  			
		    		return true;
				}
	    	});
	    	
	    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    	
	    	log.debug({title: 'Function Name', details: 'flagRecords'});
	    	log.debug({title: 'Reschedule?', details: reschedule});
	    	log.debug({title: 'Count', details: count});
	    	log.debug({title: 'Usage', details: usage});
	    	
	    	if(reschedule == false){
				rescheduleScript('increment');
				return false;
	    	} else {
	    		rescheduleScript('flag');
	    		return false;
	    	}
	    	
    	} catch(ex) {
       		log.error({ title: "flagRecords- Error", details: ex });
       	}
    }
    
    function flagRecord(result){
    	try{
    		var packageId = result.getValue('id');
    		record.submitFields({type: "customrecord_clgx_package", id: packageId, values: { custrecord_clgx_package_days_calc : false }});
    	} catch (ex){
       		log.error({ title: "unflagRecord- Error", details: ex });
    	}
    }
    
    function rescheduleScript(param){
    	
    	if(param){
	        var scheduledScriptTask = task.create({
	            taskType: task.TaskType.SCHEDULED_SCRIPT,
	            params: {custscript_clgx_1880_at : param}
	        });
    	} else {
	        var scheduledScriptTask = task.create({
	            taskType: task.TaskType.SCHEDULED_SCRIPT
	        });
    	}
        scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
        scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
        return scheduledScriptTask.submit();
    }
    

    
    return {
        execute: execute
    };
    
});
