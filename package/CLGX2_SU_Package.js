/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * 
 * @author Alex Guzenski - alex.guzenski@cologix.com
 * @date 9/26/2019
 */
define(["N/email", "N/render", "N/search", "/SuiteScripts/clgx/libraries/lodash.min", "N/record", "/SuiteScripts/clgx/libraries/moment.min", "N/runtime", "N/cache", "N/redirect", "N/format", "N/ui/serverWidget"],
function(email, render, search, _, record, moment, runtime, cache, redirect, format, serverWidget) {
	function beforeLoad(scriptContext) {
		try {
			if(runtime.executionContext.toLowerCase() == "userinterface" && scriptContext.type == scriptContext.UserEventType.EDIT) {
				var currentScript = runtime.getCurrentScript();
				var newRecord     = scriptContext.newRecord;
				var parameters    = scriptContext.request.parameters;
				
				if(runtime.getCurrentUser().role == -5 || runtime.getCurrentUser().role == 1088) {
					scriptContext.form.getField("custrecord_clgx_pkg_release_date").updateDisplayType({ displayType: serverWidget.FieldDisplayType.NORMAL });
				} else {
					scriptContext.form.getField("custrecord_clgx_pkg_release_date").updateDisplayType({ displayType: serverWidget.FieldDisplayType.INLINE });
				}
				
				if(parameters.stage !== undefined && parameters.stage == "received") {
					var cacheObject = cache.getCache({ name: currentScript.id.toUpperCase() + "_STORE" });
					cacheObject.put({ key: "redirect", value: true });
				}
			}
		} catch(ex) {
			log.error({ title: "beforeLoad - Error", details: ex });
		}
	}
	
	
	function beforeSubmit(scriptContext) {
		try {
			if(scriptContext.type == scriptContext.UserEventType.CREATE || scriptContext.type == scriptContext.UserEventType.EDIT || scriptContext.type == scriptContext.UserEventType.XEDIT) {
				var newRecord     = scriptContext.newRecord;
				var currentScript = runtime.getCurrentScript();
				
				var cacheObject   = cache.getCache({ name: currentScript.id.toUpperCase() + "_STORE" });
				
				//Set default values
				var customerID  = newRecord.getValue("custrecord_clgx_pkg_receiving_company");
				var facility    = newRecord.getValue("custrecord_clgx_pkg_storage_facility");
		    	var packageType = newRecord.getValue("custrecord_clgx_pkg_package_type");
		    	
		    	var pricingObject  = buildPricingObject({ facilityID: facility, packageTypeID: packageType });
		    	var facilityObject = buildFacilityObject(facility);
		    	
		    	var deliveryStatus    = parseInt(newRecord.getValue("custrecord_clgx_pkg_delivery_status"));
		    	var claimEmailSent    = newRecord.getValue("custrecord_clgx_pkg_claim_email_sent");
		    	var deliveryEmailSent = newRecord.getValue("custrecord_clgx_pkg_delivery_email_sent");
		    	
		    	cacheObject.put({ key: "facility_id",     value: facility });
		    	cacheObject.put({ key: "package_type_id", value: packageType });
		    	cacheObject.put({ key: "pricing_object",  value: pricingObject });
		    	cacheObject.put({ key: "facility_object", value: facilityObject });
		    	cacheObject.put({ key: "senddelivery",    value: new String(!deliveryEmailSent) });
		    	cacheObject.put({ key: "sendclaim",       value: new String(!claimEmailSent) });
		    	cacheObject.put({ key: "delivery_status", value: deliveryStatus });
		    	
		    	newRecord.setValue({ fieldId: "custrecord_clgx_pkg_storage_pricing", value: pricingObject.id });
		    	newRecord.setValue({ fieldId: "custrecord_clgx_pkg_storage_rate", value: pricingObject.rate });
		    	newRecord.setValue({ fieldId: "custrecord_clgx_pkg_grace_period", value: pricingObject.graceperiod });
		    	
		    	//Populate the facility address
		    	newRecord.setValue({ 
		    		fieldId: "custrecord_clgx_package_address_full", 
		    		value: facilityObject.fulladdress
		    	});
		    	
		    	
		    	if(!newRecord.getValue("custrecord_clgx_pkg_delivery_date") & (deliveryStatus == 2)) {
					newRecord.setValue({ fieldId: "custrecord_clgx_pkg_delivery_date", value: new Date() });
				}
		    	
				//Received/Stored
				if((deliveryStatus == 2) & (deliveryEmailSent == false)) {
					newRecord.setValue({ fieldId: "custrecord_clgx_pkg_delivery_email_sent", value: true });
				}
				
				//Claimed
				if((deliveryStatus == 3) & (claimEmailSent == false)) {				
					//sendDeliveryEmails({ context: scriptContext, facility: facilityObject, packageType: packageType, pricing: pricingObject });
					newRecord.setValue({ fieldId: "custrecord_clgx_pkg_claim_email_sent", value: true });
					newRecord.setValue({ fieldId: "custrecord_clgx_pkg_released_by", value: runtime.getCurrentUser().id });
				}
				
				//Update the storage type based on options from the customer record.
				try {
					if(customerID) {
						var resultObject = search.lookupFields({ type: "customer", id: customerID, columns: ["custentity_clgx_package_fee_waived"] });
						var storageType  = parseInt(newRecord.getValue("custrecord_clgx_pckg_prepaid_storage"));
						
						if(resultObject.custentity_clgx_packages_fee_waived && storageType !== 1) {
							newRecord.setValue({ fieldId: "custrecord_clgx_pckg_prepaid_storage", value: 3 }); //Contractually Waived
						} else {
							newRecord.setValue({ fieldId: "custrecord_clgx_pckg_prepaid_storage", value: 2 }); //Cologix Storage
						}
					}
				} catch(ex) {
					log.error({ title: "beforeSubmit - Update Storage Type - Error", details: ex });
				}
			}
    	} catch(ex) {
    		log.error({ title: "beforeSubmit - Error", details: ex });
    	}
	}
	
	
	function afterSubmit(scriptContext) {
		try {
			if(scriptContext.type == scriptContext.UserEventType.CREATE || scriptContext.type == scriptContext.UserEventType.EDIT) {
				var newRecord      = scriptContext.newRecord;
				
				var currentScript  = runtime.getCurrentScript();
				var cacheObject    = cache.getCache({ name: currentScript.id.toUpperCase() + "_STORE" });
				
				var facility       = parseInt(cacheObject.get({ key: "facility_id" }));
				var facilityObject = JSON.parse(cacheObject.get({ key: "facility_object" }));
				var pricingObject  = JSON.parse(cacheObject.get({ key: "pricing_object" }));
				
				var deliveryStatus     = parseInt(cacheObject.get({ key: "delivery_status" }));
				var sendDeliveryEmail  = JSON.parse(cacheObject.get({ key: "senddelivery" }));
				var sendClaimEmail     = JSON.parse(cacheObject.get({ key: "sendclaim" }));
				var internalTracking   = (facilityObject["name"] + "-" + newRecord.id);
				var packageType        = JSON.stringify(cacheObject.get({ key: "package_type_id" }));
				var redirectToSuitelet = JSON.parse(cacheObject.get({ key: "redirect" }));
				
				
				var excludedCompanies = [487967, 502186, 963445, 2763, 2789, 2790];
				
				//Received/Stored
				if(((deliveryStatus == 2) & (sendDeliveryEmail == true)) & (excludedCompanies.indexOf(parseInt(newRecord.getValue("custrecord_clgx_pkg_receiving_company"))) == -1)) {
					sendDeliveryEmails({ context: scriptContext, facility: facilityObject, packageType: packageType, internalTracking: internalTracking, pricing: pricingObject });
				}
				
				//Claimed
				if((deliveryStatus == 3) & (sendClaimEmail == true) & (excludedCompanies.indexOf(parseInt(newRecord.getValue("custrecord_clgx_pkg_receiving_company"))) == -1)) {				
					sendDeliveryEmails({ context: scriptContext, facility: facilityObject, packageType: packageType, internalTracking: internalTracking, pricing: pricingObject });
				}
				
				//Set the internal tracking number.
				if(!newRecord.getValue("custrecord_clgx_pkg_internal_track")) {
					record.submitFields({ 
						type: "customrecord_clgx_package", 
						id: newRecord.id, 
						values: { custrecord_clgx_pkg_internal_track: internalTracking }
					});
				}
				
				cacheObject.remove({ key: "facility_id"     });
				cacheObject.remove({ key: "package_type_id" });
				cacheObject.remove({ key: "pricing_object"  });
				cacheObject.remove({ key: "facility_object" });
				cacheObject.remove({ key: "senddelivery"    });
				cacheObject.remove({ key: "sendclaim"       });
				cacheObject.remove({ key: "delivery_status" });
				cacheObject.remove({ key: "redirect"        });
				
				if(redirectToSuitelet) {	
					redirect.toSuitelet({ 
						scriptId: "1884", 
						deploymentId: "1", 
						parameters: {
							"custom_packmessage": "Success:" + newRecord.getValue("custrecord_clgx_pkg_tracking_num").trim() + ",", 
							"custom_packfilt": facility
						} 
					});
				}
			}
		} catch(ex) {
			log.error({ title: "afterSubmit - Error", details: ex });
		}
	}

    
	/**
	 * Sends package delivery notification emails to the company's contacts with the notify checkbox checked.
	 * 
	 * @access private
	 * @function sendDeliveryEmails
	 * @usage (10 + 10(n)) - Base 10 usage plus 10 times every email sent.
	 * 
	 * @param {Object} context
	 * @returns null
	 */
    function sendDeliveryEmails(paramObject) {
    	try {
    		var newRecord = paramObject.context.newRecord;
        	
        	var facility    = paramObject.facility;
        	var packageType = paramObject.packageType;
        	var pricing     = paramObject.pricing;
        	
        	var replyToObject = buildReplyToMap(facility.id);
        	
        	var deliveryStatus = parseInt(newRecord.getValue("custrecord_clgx_pkg_delivery_status"));
        	var emailTemplateObject = null;
        	
        	if(deliveryStatus == 2) {
        		emailTemplateObject  = record.load({ type: "emailtemplate", id: 208 });
        	} else if(deliveryStatus == 3) {
        		emailTemplateObject  = record.load({ type: "emailtemplate", id: 210 });
        	}
        	
        	var emailTemplateSubject = emailTemplateObject.getValue("subject");
        	var emailTemplateBody    = emailTemplateObject.getValue("content");
        	
        	if(deliveryStatus == 2) {
        		var packageType = newRecord.getText("custrecord_clgx_pkg_package_type");
        		if(packageType.indexOf("(") > -1) {
        			var tmpPackageType = packageType.split("(");
        			packageType = tmpPackageType[0].trim();
        		}
        		
        		emailTemplateBody = emailTemplateBody.replace(new RegExp("{packagetype}"), packageType);
        		emailTemplateBody = emailTemplateBody.replace(new RegExp("{shippingcarrier}"), newRecord.getText("custrecord_clgx_pkg_shipping_carrier"));
        		
        		var companyContact   = newRecord.getText("custrecord_clgx_pkg_company_contact");
        		var packageAttention = newRecord.getValue("custrecord_clgx_pkg_attention");
        		var packageRecipient = "";
        		if(!companyContact) { 
        			packageRecipient = packageAttention;
        		} else { 
        			var tmpContact   = companyContact.split(":");
        			packageRecipient = tmpContact[tmpContact.length - 1].trim(); 
        		}
        		
        		emailTemplateBody = emailTemplateBody.replace(new RegExp("{packagerecipient}"), packageRecipient);
        		
        		emailTemplateBody = emailTemplateBody.replace(new RegExp("{trackingnumber}"), newRecord.getValue("custrecord_clgx_pkg_tracking_num"));
        		emailTemplateBody = emailTemplateBody.replace(new RegExp("{internaltracking}"), paramObject.internalTracking);
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facility}"), facility.name);
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{deliverydate}"), moment(newRecord.getValue("custrecord_clgx_pkg_delivery_date")).format("M/D/YYYY"));
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facilityaddress}"), newRecord.getValue("custrecord_clgx_package_address_full"));
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{storagelimit}"), newRecord.getValue("custrecord_clgx_pkg_grace_period"));
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{storagerate}"), newRecord.getValue("custrecord_clgx_pkg_storage_rate").toFixed(2));
        	} else if(deliveryStatus == 3) {
        		var storageCharges = calculateStorageCharges({ context: paramObject.context, pricing: pricing });
        		if(storageCharges <= 0) {
        			storageCharges = "$0";
        		}
        		
        		var claimedBy = newRecord.getText("custrecord_clgx_pkg_claimed_by");
        		if(!claimedBy) {
        			claimedBy = newRecord.getValue("custrecord_clgx_pkg_claimed_by_other");
        		}
        		
        		emailTemplateBody = emailTemplateBody.replace(new RegExp("{trackingnumber}"), newRecord.getValue("custrecord_clgx_pkg_tracking_num"));
        		emailTemplateBody = emailTemplateBody.replace(new RegExp("{internaltracking}"), paramObject.internalTracking);
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facility}"), facility.name);
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{claimedby}"), claimedBy);
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{releasedby}"), newRecord.getText("custrecord_clgx_pkg_released_by"));
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{daysinstorage}"), newRecord.getValue("custrecord_clgx_package_total_days"));
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{charges}"), storageCharges);
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{releasedate}"), moment(newRecord.getValue("custrecord_clgx_pkg_release_date")).format("M/D/YYYY"));
            	emailTemplateBody = emailTemplateBody.replace(new RegExp("{releasenotes}"), newRecord.getValue("custrecord_clgx_pkg_release_notes"));
        	}
        	
        	var companyContactArray = new Array();
        	var searchObject        = search.load({ id: "customsearch_clgx_pkg_emails" });
        	
        	searchObject.filters.push(search.createFilter({ name: "company", operator: "anyof", values: [newRecord.getValue("custrecord_clgx_pkg_receiving_company")] }));
        	
        	searchObject.run().each(function(result) {
        		companyContactArray.push({ id: result.getValue("internalid"), email: result.getValue("email"), notify: result.getValue("custentity_clgx_contact_notify_ship") });
        		return true;
        	});
        	
        	var filteredContactArray              = filterNotifyContacts(companyContactArray); //Filter down to contacts that recieve shipping emails.
        	var filteredCompanyContactArrayLength = filteredContactArray.length;
        	
        	if(filteredCompanyContactArrayLength > 0) {
        		companyContactArray = filteredContactArray;
        	} else {
        		companyContactArray = _.map(companyContactArray, "id");
        	}
        	
        	var companyContactArrayLength = companyContactArray.length;
        	
        	if(companyContactArrayLength > 10) {
        		var companyContactObject       = _.chunk(companyContactArray, 10);
        		var companyContactObjectLength = companyContactObject.length;
        		
        		for(var contactSet = 0; contactSet < companyContactArrayLength; contactSet++) {
        			if(companyContactObject[contactSet]) {
        				email.send({ 
            				author: 432742, 
            				replyTo: replyToObject.email,
            				recipients: companyContactObject[contactSet], 
            				subject: emailTemplateSubject, 
            				body: emailTemplateBody, 
            				relatedRecords: {
            					customRecord: {
            						id: newRecord.id,
            						recordType: "customrecord_clgx_package"
            					}
            				} 
            			});
        			}
        		}
        	} else {
        		email.send({ 
    				author: 432742, 
    				replyTo: replyToObject.email,
    				recipients: companyContactArray, 
    				subject: emailTemplateSubject, 
    				body: emailTemplateBody, 
    				relatedRecords: {
    					customRecord: {
    						id: newRecord.id,
    						recordType: "customrecord_clgx_package"
    					}
    				} 
    			});
        	}
    	} catch(ex) {
    		log.error({ title: "sendDeliveryEmails - Error", details: ex });
    	}
    }
    
    
    /**
     * Returns the dollar amount charged for storage.
     * 
     * @access private
     * @function calculateStorageCharges
     * 
     * @param {Object} paramObject 
     * @returns {Float}
     */
    function calculateStorageCharges(paramObject) {
    	if(paramObject) {
    		var newRecord     = paramObject.context.newRecord;
    		var pricing       = paramObject.pricing;
    		
    		var daysInStorage = parseInt(newRecord.getValue("custrecord_clgx_package_total_days"));
    		var gracePeriod   = parseInt(pricing.graceperiod);
    		var storageRate   = parseFloat(pricing.rate);
    		
    		return parseFloat((daysInStorage - gracePeriod) * storageRate).toFixed(2);
    	}
    }
    
    
    /**
     * Returns an email based off of the facility on the package record.
     * 
     * @access private
     * @function buildReplyToMap
     * @usage 0
     * 
     * @param {Number} facilityID
     * @returns {String}
     */
    function buildReplyToMap(facilityID) {
    	if(facilityID) {
    		//Columbus
    		if(facilityID == 24 || facilityID == 29) { return {id: 1893287, email: "col.operations@cologix.com"}; }
    		
    		//Dallas
    		if(facilityID == 3 || facilityID == 18 || facilityID == 44) { return {id: 1893288, email: "dal.operations@cologix.com"}; }
    		
    		//JAX1
    		if(facilityID == 21) { return {id: 1892683, email: "jax1.operations@cologix.com"}; }
    		
    		//JAX2
    		if(facilityID == 27) { return {id: 1892784, email: "jax2.operations@cologix.com"}; }
    		
    		//LAK
    		if(facilityID == 28) { return {id: 559901, email: "lak.operations@cologix.com"}; }
    		
    		//MIN
    		if(facilityID == 17 || facilityID == 25) { return {id: 1892886, email: "min.operations@cologix.com"}; }
    		
    		//Montreal
    		
    		//MTL1
    		if(facilityID == 2) { return { id: "", email: "mtl.c1@cologix.com" }; }
    		
    		//MTL2
    		if(facilityID == 5) { return { id: "", email: "mtl.c2@cologix.com" }; }
    		
    		//MTL3
    		if(facilityID == 4) { return { id: "", email: "mtl.c3@cologix.com" }; }
    		
    		//MTL4
    		if(facilityID == 9) { return { id: "", email: "mtl.c4@cologix.com" }; }
    		
    		//MTL5
    		if(facilityID == 6) { return { id: "", email: "mtl.c5@cologix.com" }; }
    		
    		//MTL6
    		if(facilityID == 7) { return { id: "", email: "mtl.c6@cologix.com" }; }
    		
    		//MTL7
    		if(facilityID == 19) { return { id: "", email: "mtl.c7@cologix.com" }; }
    		
    		//MTL8
    		if(facilityID == 45) { return { id: "", email: "mtl.operations@cologix.com" }; }
    		
    		//MTL9
    		if(facilityID == 51) { return { id: "", email: "mtl9.operations@cologix.com" }; }
    		
    		//MTL10
    		if(facilityID == 52) { return { id: "", email: "mtl10.operations@cologix.com" }; }
    		
    		//MTL11
    		if(facilityID == 53) { return { id: 1894604, email: "mtl.operations@cologix.com" }; }
    		
    		//NNJ
    		if(facilityID == 34 || facilityID == 33 || facilityID == 35 || facilityID == 36) { return {id: 1876217, email: "nnj.security@cologix.com"}; }
    		
    		//TOR
    		if(facilityID == 8 || facilityID == 15 || facilityID == 43) { return {id: 1894303, email: "tor.operations@cologix.com"}; }
    		
    		//VAN
    		if(facilityID == 14 || facilityID == 20 || facilityID == 42) { return {id: 1893289, email: "van.operations@cologix.com"}; }
    	}
    }
    
    
    /**
     * Builds an object of package pricing.
     * 
     * @access private
     * @function buildPricingObject
     * @usage 0
     * 
     * @param {Object} paramObject
     * @param {Number} paramObject.facilityID
     * @param {Number} paramObject.packageTypeID
     * 
     * @returns {Object}
     */
    function buildPricingObject(paramObject) {
    	try {
    		if(paramObject) {
        		var pricingObject = new Object();
        		
        		var columns = new Array();
        		columns.push(search.createColumn("internalid"));
        		columns.push(search.createColumn("custrecord_clgx_pkg_strg_daily_rate"));
        		columns.push(search.createColumn("custrecord_clgx_pkg_strg_grc_period"));
        		
        		var filters = new Array();
        		filters.push(search.createFilter({ name: "custrecord_clgx_pkg_strg_facility", operator: "anyof", values: paramObject.facilityID }));
        		filters.push(search.createFilter({ name: "custrecord_pkg_strg_package_type", operator: "anyof", values: paramObject.packageTypeID }));
        		
        		var searchObject = search.create({ type: "customrecord_clgx_pkg_storage_pricing", columns: columns, filters: filters });
        		searchObject.run().each(function(result) {
        			pricingObject.id          = result.getValue("internalid");
        			pricingObject.graceperiod = result.getValue("custrecord_clgx_pkg_strg_grc_period");
        			pricingObject.rate        = result.getValue("custrecord_clgx_pkg_strg_daily_rate");
        		});
        		
        		return pricingObject;
        	}
    	} catch(ex) {
    		log.error({ title: "buildPricingObject - Error", details: ex });
    	}
    }
    
    
    /**
     * Returns information from the facility record.
     * 
     * @access private
     * @function buildFacilityObject
     * @usage 2
     * 
     * @param {Number} facilityID
     * @returns {Object}
     */
    function buildFacilityObject(facilityID) {
    	try {
    		if(facilityID) {
        		var facilityObject = new Object();
        		var recordObject   = record.load({ type: "customrecord_cologix_facility", id: facilityID, isDynamic: true });
        		facilityObject.id             = facilityID;
        		facilityObject.name           = recordObject.getValue("name");
        		facilityObject.buildingnumber = recordObject.getValue("custrecord_cologix_location_bldg_number");
        		facilityObject.address1       = recordObject.getValue("custrecord_cologix_location_address1");
        		facilityObject.address2       = recordObject.getValue("custrecord_cologix_location_address2");
        		facilityObject.city           = recordObject.getValue("custrecord_cologix_location_city");
        		facilityObject.state          = recordObject.getText("custrecord_cologix_location_state");
        		facilityObject.zip            = recordObject.getValue("custrecord_cologix_location_postalcode");
        		facilityObject.country        = recordObject.getText("custrecord_cologix_location_country");
        		facilityObject.fulladdress    = facilityObject.address1 + "<br/>" 
        											+ ((!facilityObject.address2) ? "" : facilityObject.address2 + "<br/>") 
        											+ facilityObject.city + ", " 
        											+ facilityObject.state + ", " 
        											+ facilityObject.zip + "<br />"
        											+ facilityObject.country + "<br />";
        		
        		return facilityObject;
        	}
    	} catch(ex) {
    		log.error({ title: "buildFacilityObject - Error", details: ex });
    	}
    }
    
    /**
     * Filters an array of objects based on an object key.
     * 
     *  @access private
     *  @function filterContactArray
     *  
     *  @param {Array} contactArray  
     *  @returns {Array}
     */
    function filterNotifyContacts(contactArray) {
    	if(contactArray) {
    		return _.map(_.filter(contactArray, { "notify": true }), "id");
    	}
    }
    
    return {
    	beforeLoad: beforeLoad,
    	beforeSubmit: beforeSubmit,
    	afterSubmit: afterSubmit
    };
    
});
