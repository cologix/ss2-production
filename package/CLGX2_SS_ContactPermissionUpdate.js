/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/search", "N/record", "N/runtime", "N/task"],
function(search, record, runtime, task) {
    function execute(scriptContext) {
    	try {
    		var sc = runtime.getCurrentScript();
    		var usage = sc.getRemainingUsage();
    		var reschedule = false;
    		
    		var searchObject = search.load({ id: sc.getParameter("custscript_clgx2_ss_cpu_sid") });
    		searchObject.run().each(function(result) {
    			
    			var recordObject = record.load({ type: "contact", id: parseInt(result.getValue("internalid")), isDynamic: false });
        		var jsonString   = recordObject.getValue("custentity_clgx_cp_user_rights_json");
        		
        		if(jsonString) {
        			var jsonObject   = JSON.parse(jsonString);
            		
            		if(!jsonObject.shipments) {
            			if(jsonObject.users == 4) {
            				jsonObject.shipments = jsonObject.users;
            			} else if(jsonObject.visits) {
            				jsonObject.shipments = jsonObject.visits;
            			} else if(jsonObject.cases) {
            				jsonObject.shipments = jsonObject.cases;
            			} else {
            				jsonObject.shipments = 0;
            			}
            		}
        		} else {
        			jsonObject = {"users":0,"security":0,"cases":0,"casesfin":0,"reports":0,"visits":0,"orders":0,"invoices":0,"ccards":0,"colocation":0,"network":0,"domains":0,"managed":0,"shipments":0};
        		}
    			
        		record.submitFields({ type: "contact", id: result.getValue("internalid"), values: { custentity_clgx_cp_user_rights_json: JSON.stringify(jsonObject), options: { enableSourcing: false, ignoreMandatoryFields: true } } });
        		
        		log.debug({ title: result.getValue("internalid"), details: JSON.stringify(jsonObject) });
        		
    			if((usage = sc.getRemainingUsage()) <= 200) {
    				reschedule = true;
    				return false;
    			}
    			
    			return true;
    		});
    		
    		if(reschedule) {
    			log.debug({ title: "rescheduling", details: "rescheduling" });
    			scheduleScript();
    		}
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }

    
    /**
     * @access private
     * @function scheduleScript
     * 
     * @param {ParamObject} paramObject
     * 
     * @return null
     */
    function scheduleScript() {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = runtime.getCurrentScript().id;
    		t.params       = {
    			custscript_clgx2_ss_cpu_sid: "customsearch_clgx_perm_massupdate"
    		};
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }
    
    return {
        execute: execute
    };
    
});
