/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Katy Jones - katy.jones@cologix.com
 * @date 9/19/2019
 */
define(['N/record', 'N/search', '/SuiteScripts/clgx/libraries/lodash.min', '/SuiteScripts/clgx/libraries/moment.min', 'N/email', 'N/runtime'],
/**
 * @param {record} record
 * @param {search} search
 */
function(record, search, _, moment, email, runtime) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(context) {
   		startChargeCounter();
   		incrementDays();
//   		sendReminderEmail();
    }
    

    	
    function startChargeCounter(){
	
    var savedSearchID = 'customsearch_clgx_package_start_counter';
		
	var searchObject = search.load({ id: savedSearchID });
	searchObject.run().each(function(result) {
		var packageId = result.getValue('internalid');
		var dateReceived = result.getValue('custrecord_clgx_pkg_delivery_date');
		startChargeDays(packageId, dateReceived);
						
		return true;
	});
    	
    }
    	
    function startChargeDays(packageId, dateReceived){
       		try {
       			var chargeStartDate = moment(dateReceived).add(7,'day').format('M/D/YYYY');
       			log.debug({title: 'Start charge days', details: chargeStartDate});
           		record.submitFields({ type: "customrecord_clgx_package", id: packageId, values: {  custrecord_clgx_package_over_grace_prd : true, custrecord_clgx_package_date_charge : chargeStartDate} });
           	} catch(ex) {
           		log.error({ title: "startChargeDays- Error", details: ex });
           	}
   	}   
    
    function incrementDays(){
    	var savedSearchID = 'customsearch_clgx_package_charge_counter';
    	var searchObject = search.load({ id: savedSearchID });
//    	log.debug(searchObject);
    	searchObject.run().each(function(result) {
//    		log.debug({title: 'Result Object', details: result});
    		counter(result);   			  			
    		return true;
    	});
    }
    
    function counter(resultObject){
    	var today = moment().format("M/D/YYYY");
       	var compareDays = firstDayTest(today);
    	var dateChargesBegin = resultObject.getValue('custrecord_clgx_package_date_charge');
    	var lastMonthCharges = resultObject.getValue('custrecord_clgx_package_bill_days');
    	var firstDayCurrentMonth = moment(today).startOf('month').format('M/D/YYYY');
    	var lastDayLastMonth = moment(today).subtract(1, 'months').endOf('month').format("M/D/YYYY");
    	var firstDayLastMonth = moment(today).subtract(1, 'months').startOf('month').format("M/D/YYYY");
    	
    	var releaseDate = resultObject.getValue('custrecord_clgx_pkg_release_date');
    	
    	var currentMonth = moment(today).format('M');
    	var chargeStartMonth = moment(dateChargesBegin).format('M');
    	var releaseMonth = moment(releaseDate).format('M');
    	var lastMonth = moment(lastDayLastMonth).format('M');
    	var startMonth = moment(firstDayLastMonth).format('M');
    	
    	var monthToDate = 	parseInt(resultObject.getValue('custrecord_clgx_package_current_days'));
    	var packageId = 	resultObject.getValue('id');

    	var packageRec = record.load({
    		type: 'customrecord_clgx_package',
    		id: packageId,
    		isDynamic: true
    	});
    	
    	//if a new month has begun, move current month value to Last Month Billing days and set Current Month Days to 1
//    	if(compareDays === true){
//    		packageRec.setValue({fieldId : 'custrecord_clgx_package_bill_days', value: monthToDate});
//    		packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: 1});
//    	} else {
//    		//if the package started accruing charges this month, calculate this month's charge days based on the Charge Start Date
//    		if(currentMonth == chargeStartMonth){
//    			if(!releaseDate){
//	    			var chargeDays = parseInt(moment(today).diff(dateChargesBegin, 'days'));
//	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: chargeDays });
//    			} else {
//    				var chargeDays = parseInt(moment(releaseDate).diff(dateChargesBegin, 'days'));
//	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: chargeDays });
//    			}	
//    		} 
//    		//If the months don't match, set the charge days for this month to date
//    		else {
//    			if(!releaseDate){
//	    			var chargeDays = parseInt(moment(today).format('D'));
//	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: chargeDays });
//    			} else {
//    				var chargeDays = parseInt(moment(releaseDate).format('D'));
//	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: chargeDays });
//    			}
//    		}
//    	}
    	
//    	if(!lastMonthCharges && moment(dateChargesBegin).isBefore(moment(firstDayCurrentMonth))) {
//    			if(chargeStartMonth === lastMonth && releaseMonth === lastMonth){
//    				var lastMonthCharges = parseInt(moment(releaseDate).diff(dateChargesBegin,  'days')) + 1;
//    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});
//    			} else if(chargeStartMonth === lastMonth && releaseMonth != lastMonth){
//    				var lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(dateChargesBegin,  'days')) + 1;
//    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});
//    			} else if(chargeStartMonth != lastMonth && releaseMonth === lastMonth){
//    				var lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(firstDayLastMonth, 'days')) + 1;
//    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});
//    			} else {
//    				var lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(firstDayLastMonth, 'days')) + 1;
//    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});
//    				if(releaseMonth === currentMonth && chargeStartMonth != currentMonth){
//    					var monthToDateCharges = parseInt(moment(releaseDate).diff(firstDayCurrentMonth, 'days')) + 1;
//		    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
//    				} else if (releaseMonth === currentMonth && chargeStartMonth === currentMonth){
//    					var monthToDateCharges = parseInt(moment(releaseDate).diff(dateChargesBegin, 'days')) + 1;
//		    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
//    				} else {
//    					var monthToDateCharges = parseInt(moment(today).format('D'));
//		    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
//    				}
//    			} 
//    		} else if (compareDays === true){
//        		packageRec.setValue({fieldId : 'custrecord_clgx_package_bill_days', value: monthToDate});
//        		packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: parseInt(moment(today).format('D'))});
//    		} else {
//    			if(!releaseDate){
//    				if(chargeStartMonth === currentMonth){
//    					var monthToDateCharges = parseInt(moment(today).diff(dateChargesBegin, 'days')) + 1;
//    	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
//    				} else{
//    					packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: parseInt(moment(today).format('D'))});
//    				}
//    			} else if(releaseMonth === currentMonth && chargeStartMonth != currentMonth){
//					var monthToDateCharges = parseInt(moment(releaseDate).diff(firstDayCurrentMonth, 'days')) + 1;
//	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
//    			} else {
//    				var monthToDateCharges = parseInt(moment(dateChargesBegin).diff(releaseDate,  'days'));
//    				packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges});
//    			}
//    		}
    	
    	if(releaseDate){
    		if(releaseMonth === currentMonth){
    			if(chargeStartMonth === currentMonth){
    				var monthToDateCharges = parseInt(moment(releaseDate).diff(dateChargesBegin, 'days'));
	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
    			} else if(chargeStartMonth === lastMonth){
    				var monthToDateCharges = parseInt(moment(releaseDate).format('D'));
	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
    				var lastMonthCharges = parseInt(moment(lastDayLastMonth).diff(dateChargesBegin, 'days'));
    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});
    			} else {
    				var monthToDateCharges = parseInt(moment(releaseDate).format('D'));
	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
    				var lastMonthCharges = Math.ceil(parseInt(moment(firstDayLastMonth).diff(lastDayLastMonth, 'days')));
    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});
    			}
    		} else if (releaseMonth === lastMonth){
    			if(chargeStartMonth === lastMonth){
    				var monthToDateCharges = '';
	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
    				var lastMonthCharges = Math.ceil(parseInt(moment(releaseDate).diff(dateChargesBegin, 'days')));
    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});	    			
    			} else {
    				var monthToDateCharges = '';
	    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
    				var lastMonthCharges = parseInt(moment(releaseDate).format('D'));
    				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});    				
    			}
    		}
    	} else {
    		if(chargeStartMonth === currentMonth){
    			var monthToDateCharges = parseInt(moment(today).diff(dateChargesBegin, 'days'));
    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
    		} else if (chargeStartMonth === lastMonth){
    			var monthToDateCharges = parseInt(moment(today).format('D'));
    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
				var lastMonthCharges = Math.ceil(parseInt(moment(lastDayLastMonth).diff(dateChargesBegin, 'days')));
				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});
    		} else {
				var monthToDateCharges = parseInt(moment(today).format('D'));
    			packageRec.setValue({fieldId : 'custrecord_clgx_package_current_days', value: monthToDateCharges });
				var lastMonthCharges = Math.ceil(parseInt(moment(lastDayLastMonth).diff(firstDayLastMonth, 'days')));
				packageRec.setValue({fieldId: 'custrecord_clgx_package_bill_days', value: lastMonthCharges});   
    		}
    	}
    	
    	packageRec.save();	
    }
    
    function firstDayTest(today){
    	var firstDay = moment().startOf('month').format("M/D/YYYY");
//    	var firstDay = today; //Override first day value to be today to force test of date evaluation
    	if(today === firstDay){
    		return true;
    	} else{
    		return false;
    	}
    }
    
    function sendReminderEmail(){
    	var savedSearchId = 'customsearch_clgx_package_reminder_email';
    	var searchObject = search.load({ id: savedSearchId });
    	searchObject.run().each(function(result) {
 			buildEmail(result);
    		return true;
    	});
    }
    
    function buildEmail(resultObject){
    	var facility = resultObject.getValue('custrecord_clgx_pkg_storage_facility');
    	var packageType = resultObject.getValue('custrecord_clgx_pkg_package_type');
    	
    	var replyToEmail = buildReplyToMap(facility);
    	
    	var emailTemplateObject  = record.load({ type: "emailtemplate", id: 202});
    	var emailTemplateSubject = emailTemplateObject.getValue("subject");
    	var emailTemplateBody    = emailTemplateObject.getValue("content");
    	
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{trackingnumber}"), resultObject.getValue('custrecord_clgx_pkg_tracking_num'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facility}"), resultObject.getValue('custrecord_clgx_pkg_storage_facility'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{deliverydate}"), resultObject.getValue('custrecord_clgx_pkg_delivery_date'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facilityaddress}"), resultObject.getValue('custrecord_clgx_package_address_full'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{daysinstorage}"), resultObject.getValue('custrecord_clgx_package_total_days'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{storagerate}"), resultObject.getValue('custrecord_clgx_pkg_storage_rate'));
    	
    	var companyContactArray = new Array();
    	var searchObject        = search.load({ id: "customsearch_clgx_pkg_emails" });
    	
    	searchObject.filters.push(search.createFilter({ name: "company", operator: "anyof", values: [resultObject.getValue("custrecord_clgx_pkg_receiving_company")] }));
    	searchObject.run().each(function(result) {
    		companyContactArray.push(result.getValue("internalid"));
    		return true;
    	});
    	
    	var companyContactArrayLength = companyContactArray.length;
    	if(companyContactArrayLength > 10) {
    		var companyContactObject       = _.chunk(companyContactArray, 10);
    		var companyContactObjectLength = companyContactObject.length;
    		
    		for(var contactSet = 0; contactSet < companyContactArrayLength; contactSet++) {
    			try{
	    			email.send({ 
	    				author: 1937290, 
	    				replyTo: replyToEmail,
	    				recipients: companyContactObject[contactSet], 
	    				subject: emailTemplateSubject, 
	    				body: emailTemplateBody, 
	    				relatedRecords: {
	    					customRecord: {
	    						id: resultObject.id,
	    						recordType: "customrecord_clgx_package"
	    					}
	    				} 
	    			});
    			} catch(ex){
    	    		log.debug({title: 'Reminder Email-Error', details: companyContactObject});
    	    	}
    		}
    	} else {
    			try{
		    		email.send({ 
						author: 1937290, 
						replyTo: replyToEmail,
						recipients: companyContactArray, 
						subject: emailTemplateSubject, 
						body: emailTemplateBody, 
						relatedRecords: {
							customRecord: {
								id: resultObject.id,
								recordType: "customrecord_clgx_package"
							}
						} 
					});
    			} catch(ex){
    	    		log.debug({title: 'Reminder Email-Error', details: ex});
    	    	}
    	}
 
    }
    
    /**
     * Returns an email based off of the facility on the package record.
     * 
     * @access private
     * @function buildReplyToMap
     * @usage 0
     * 
     * @param {Number} facilityID
     * @returns {String}
     */
    function buildReplyToMap(facilityID) {
    	if(facilityID) {
    		//Columbus
    		if(facilityID == 24 || facilityID == 29) { return {id: 1893287, email: "col.operations@cologix.com"}; }
    		
    		//Dallas
    		if(facilityID == 3 || facilityID == 18 || facilityID == 44) { return {id: 1893288, email: "dal.operations@cologix.com"}; }
    		
    		//JAX1
    		if(facilityID == 21) { return {id: 1892683, email: "jax1.operations@cologix.com"}; }
    		
    		//JAX2
    		if(facilityID == 27) { return {id: 1892784, email: "jax2.operations@cologix.com"}; }
    		
    		//LAK
    		if(facilityID == 28) { return {id: 559901, email: "lak.operations@cologix.com"}; }
    		
    		//MIN
    		if(facilityID == 17 || facilityID == 25) { return {id: 1892886, email: "min.operations@cologix.com"}; }
    		
    		//Montreal
    		
    		//MTL1
    		if(facilityID == 2) { return { id: "", email: "mtl.c1@cologix.com" }; }
    		
    		//MTL2
    		if(facilityID == 5) { return { id: "", email: "mtl.c2@cologix.com" }; }
    		
    		//MTL3
    		if(facilityID == 4) { return { id: "", email: "mtl.c3@cologix.com" }; }
    		
    		//MTL4
    		if(facilityID == 9) { return { id: "", email: "mtl.c4@cologix.com" }; }
    		
    		//MTL5
    		if(facilityID == 6) { return { id: "", email: "mtl.c5@cologix.com" }; }
    		
    		//MTL6
    		if(facilityID == 7) { return { id: "", email: "mtl.c6@cologix.com" }; }
    		
    		//MTL7
    		if(facilityID == 19) { return { id: "", email: "mtl.c7@cologix.com" }; }
    		
    		//MTL8
    		if(facilityID == 45) { return { id: "", email: "mtl.operations@cologix.com" }; }
    		
    		//MTL9
    		if(facilityID == 51) { return { id: "", email: "mtl9.operations@cologix.com" }; }
    		
    		//MTL10
    		if(facilityID == 52) { return { id: "", email: "mtl10.operations@cologix.com" }; }
    		
    		//MTL11
    		if(facilityID == 53) { return { id: 1894604, email: "mtl.operations@cologix.com" }; }
    		
    		//NNJ
    		if(facilityID == 34 || facilityID == 33 || facilityID == 35 || facilityID == 36) { return {id: 1876217, email: "nnj.security@cologix.com"}; }
    		
    		//TOR
    		if(facilityID == 8 || facilityID == 15 || facilityID == 43) { return {id: 1894303, email: "tor.operations@cologix.com"}; }
    		
    		//VAN
    		if(facilityID == 14 || facilityID == 20 || facilityID == 42) { return {id: 1893289, email: "van.operations@cologix.com"}; }
    	}
    }
    
    return {
        execute: execute
    };
    
});
