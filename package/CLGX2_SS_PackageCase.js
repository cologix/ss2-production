/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(["N/search", "N/record", "N/runtime", "/SuiteScripts/clgx/package/lib/CLGX2_LIB_Package", "N/format"],
function(search, record, runtime, lib, format) {
    function execute(scriptContext) {
    	try {
            var srt     = runtime.getCurrentScript();
            var queueID = parseInt(srt.getParameter("custscript_clgx2_1947_qid"));
            
            if(queueID) {
            	processSingleRecord({ queueID: queueID });
            } else {
            	processMultipleRecords();
            }
        } catch(ex) {
            log.error({ title: "execute - Error", details: ex });
        }
    }

    
    /**
     * Processes multiple queue records if a queue id is not passed to the script.
     * 
     * @access private
     * @function processMultipleRecords
     * 
     * @return null
     */
    function processMultipleRecords() {
    	try {
    		var columns = new Array();
    		columns.push(search.createColumn("internalid"));
    		
    		var filters = new Array();
    		filters.push(search.createFilter({ name: "custrecord_clgx_gq_processed", operator: "is", values: ["F"] }));
    		filters.push(search.createFilter({ name: "custrecord_clgx_gq_pid", operator: "isnotempty", values: [] }));
    		
    		var searchObject = search.create({ type: "customrecord_clgx_global_queue", filters: filters, columns: columns });
    		searchObject.run().each(function(result) {
    			log.debug({ title: "queueID", details: result.getValue("internalid") });
    			processSingleRecord({ queueID: parseInt(result.getValue("internalid")) });
    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    				return false;
    			}
    			
    			return true;
    		});
    	} catch(ex) {
    		log.error({ title: "processMultipleRecords - Error", details: ex });
    	}
    }
    
    
    /**
     * Processes a single queue record.
     * 
     * @access private
     * @function processSingleRecord
     * 
     * @param {Object} paramObject
     * @param {Number} paramObject.queueID
     * 
     * @return null
     */
    function processSingleRecord(paramObject) {
    	try {
    		if(paramObject) {
    			var recordObject                = record.load({ type: "customrecord_clgx_global_queue", id: paramObject.queueID });
            	var fileID                      = parseInt(recordObject.getValue("custrecord_clgx_gq_fileid")) || 0;
            	var jsonObject                  = JSON.parse(recordObject.getValue("custrecord_clgx_gq_json"))[0];
            	var packageArray                = recordObject.getValue("custrecord_clgx_gq_pid").split(";") || null;
            	var packageArrayLength          = packageArray.length;
            	var packageArrayFirstItemLength = packageArray[0].length;
            	
            	if(packageArrayFirstItemLength <= 0) { packageArrayLength = 0; }
            	
            	var requestedDate = jsonObject.custevent_cologix_case_sched_followup;
            	if(requestedDate == 0) {
            		requestedDate = "";
            	} else {
            		requestedDate = new Date(jsonObject.custevent_cologix_case_sched_followup);
            	}
            	
            	var requestedTime = jsonObject.custevent_cologix_sched_start_time;
            	if(requestedTime == 0) {
            		requestedTime = "";
            	} else {
            		requestedTime = format.parse({ value: jsonObject.custevent_cologix_sched_start_time, type: format.Type.TIMEOFDAY })
            	}
            	
            	log.debug({ title: "requestedTime", details: requestedTime });
            	
            	//Create case record
            	var caseID = lib.createRecord({ 
            		type: "supportcase",
            		fields: {
            			title: jsonObject.title,
            			company: jsonObject.company,
            			contact: jsonObject.contact,
            			email: jsonObject.email,
            			custevent_cologix_case_sched_followup: requestedDate,
            			custevent_cologix_sched_start_time: requestedTime,
            			custevent_cologix_facility: jsonObject.custevent_cologix_facility,
            			category: jsonObject.category,
            			custevent_cologix_sub_case_type: jsonObject.custevent_cologix_sub_case_type,
            			assigned: jsonObject.assigned,
            			priority: jsonObject.priority,
            			origin: jsonObject.origin
            		}
            	});
            	
            	
            	//Create case message
            	if(fileID == 0) {
            		lib.createRecord({ 
                		type: "message",
                		dynamic: true,
                		fields: {
                			activity: caseID,
                			author: jsonObject.contact,
                			authoremail: jsonObject.email,
                			recipient: jsonObject.company,
                			recipientemail: jsonObject.email,
                			emailed: false,
                			subject: jsonObject.title,
                			message: jsonObject.message
                		}
                	});
            	} else {
            		lib.createRecord({ 
                		type: "message",
                		dynamic: true,
                		fields: {
                			activity: caseID,
                			author: jsonObject.contact,
                			authoremail: jsonObject.email,
                			recipient: jsonObject.company,
                			recipientemail: jsonObject.email,
                			emailed: false,
                			subject: jsonObject.title,
                			message: jsonObject.message
                		},
                		sublist: {
                			list: "mediaitem",
                			items: { internalid: fileID }
                		}
                	});
            	}
            	
            	
            	
            	//Update the queue record
            	record.submitFields({ type: "customrecord_clgx_global_queue", id: paramObject.queueID, values: { custrecord_clgx_gq_caseid: caseID, custrecord_clgx_gq_processed: true } });
            	
            	//Update each package with the case
            	for(var packageIndex = 0; packageIndex < packageArrayLength; packageIndex++) {
            		record.submitFields({ type: "customrecord_clgx_package", id: packageArray[packageIndex], values: { custrecord_clgx_package_case: caseID, custrecord_clgx_pkg_queue_status: "Complete" } });
            	}
    		}
    	} catch(ex) {
    		log.error({ title: "processSingleRecord - Error", details: ex });
    	}
    }
    
    return {
        execute: execute
    };
    
});
