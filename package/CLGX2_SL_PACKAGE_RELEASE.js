/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/redirect', 'N/search', 'N/ui/serverWidget', 'N/runtime', '/SuiteScripts/clgx/libraries/moment.min', 'N/ui/message', '/SuiteScripts/clgx/libraries/lodash.min'],
/**
 * @param {record} record
 * @param {redirect} redirect
 * @param {search} search
 * @param {serverWidget} serverWidget
 */
function(record, redirect, search, serverWidget, runtime, moment, message, _) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
	   
    	if(context.request.method == "GET"){
	    	var request = context.request;
    		var custId = request.parameters.cu || 0;
    		var caseID = request.parameters.ci || 0;
    		
    		//build the form	
			var form = serverWidget.createForm({ title: "Release Packages" });
    		
			//build object array of all packages for the customer that are currently stored
    		var packages = [];
    		
    		if(caseID) {
    			packages = searchCasePackages(caseID);
    			var messageText = "Case #" + caseID + " has been closed. The below packages tied to the case are still open. Please mark them as claimed and add notes if needed."
				var messageObject = message.create({type: message.Type.INFORMATION, message: messageText, duration: 5000});
        		form.addPageInitMessage({message: messageObject});
    		} else {
    			packages = searchCustPackages(custId);
    		}
    		
    		var packageNum = packages.length;
    		

    		form.clientScriptModulePath = 'SuiteScripts/clgx/package/CLGX2_CS_PACKAGE_RELEASE';
			
    		var customerList = buildCustomerList();
    		
			var company = form.addField({
				id: 'custpage_clgx2_package_release_company',
				type: serverWidget.FieldType.SELECT,
				label: 'Customer'
			});
			
			_.forEach(customerList, function(customer) {
    			company.addSelectOption({ value: customer.id, text: customer.name });
    		});
			
			company.defaultValue = custId;
			var companyFilterInfo = record.load({type: record.Type.CUSTOMER, id: custId});
			var companyFilter = companyFilterInfo.getText({ fieldId : 'companyname'});
			company.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
			
			var claimedByContact = form.addField({
				id: 'custpage_clgx2_package_claimed_by',
				type: serverWidget.FieldType.SELECT,
				source: 'contact',
				label: 'Claimed By'
			});
			
			var options = claimedByContact.getSelectOptions({
				filter: companyFilter
			});
			
			var claimedByOther = form.addField({
				id: 'custpage_clgx2_package_claimed_by_other',
				type: serverWidget.FieldType.TEXT,
				label: 'Claimed By (Other)'
			})
			
			//build the sublist
			var packageSublist = form.addSublist({
				id: 'custpage_clgx2_package_list',
			    type : serverWidget.SublistType.LIST,
			    label : 'Stored Packages'
			});
			
			var selectBox = packageSublist.addField({
        		id: 'custpage_clgx2_pckg_select', 
        		type: serverWidget.FieldType.CHECKBOX,
        		label: 'Release'
			});
			
			var packageInternalId = packageSublist.addField({
        		id: 'custpage_clgx2_pckg_re_id', 
        		type: serverWidget.FieldType.TEXT,
        		label: 'Package ID'
			});
			
			var trackingNum = packageSublist.addField({
        		id: 'custpage_clgx2_pckg_re_tn', 
        		type: serverWidget.FieldType.TEXT,
        		label: 'Tracking Number'
			});
			
			var internalTracking = packageSublist.addField({
        		id: 'custpage_clgx2_pckg_re_it', 
        		type: serverWidget.FieldType.TEXT,
        		label: 'Internal Number'
			});
			
			var status = packageSublist.addField({
        		id: 'custpage_clgx2_pckg_re_st', 
        		type: serverWidget.FieldType.SELECT,
        		source: 'customlist_clgx_delivery_status_list',
        		label: 'Status'
			});
			
			var storageNotes = packageSublist.addField({
        		id: 'custpage_clgx2_pckg_re_sn', 
        		type: serverWidget.FieldType.TEXT,
        		label: 'Interal Notes'
			});
			
			var releaseNotes = packageSublist.addField({
        		id: 'custpage_clgx2_pckg_re_rn', 
        		type: serverWidget.FieldType.TEXT,
        		label: 'Release Notes'
			});
			
//			claimedByContact.isMandatory = true;
			releaseNotes.updateDisplayType({ displayType : serverWidget.FieldDisplayType.ENTRY });
			status.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
						
			packageSublist.addMarkAllButtons();
			
			//populate the sublist with items from the packages array
			for(var i=0; i<packageNum; i++){
            	packageSublist.setSublistValue({id: 'custpage_clgx2_pckg_re_id', line: i, value: packages[i].packageId });
            	if(packages[i].internaltracking){
            		packageSublist.setSublistValue({id: 'custpage_clgx2_pckg_re_it', line: i, value: packages[i].internaltracking });
            	}
            	packageSublist.setSublistValue({id: 'custpage_clgx2_pckg_re_tn', line: i, value: packages[i].tracking });
            	packageSublist.setSublistValue({id: 'custpage_clgx2_pckg_re_st', line: i, value: packages[i].status });
            	if(packages[i].internalnotes){
            		packageSublist.setSublistValue({id: 'custpage_clgx2_pckg_re_sn', line: i, value: packages[i].internalnotes });
            	}
			}
    		form.addSubmitButton({label:'Release Packages'});
    		context.response.writePage(form);
    	} else {
			var request = context.request;
			var claimedBy = request.parameters.custpage_clgx2_package_claimed_by;
			var claimedByOther = request.parameters.custpage_clgx2_package_claimed_by_other;
			var releasedBy = runtime.getCurrentUser().id;
			log.debug(releasedBy);
							
			var lineCount = request.getLineCount({ group: 'custpage_clgx2_package_list' });
			
			
			//iterate through sublist lines and identify selected packages
			for(var i = 0; i<lineCount; i++){
				var selected = request.getSublistValue({group: 'custpage_clgx2_package_list', name: 'custpage_clgx2_pckg_select', line: i});

				//update selected line items on the package record
				if(selected == 'T'){
					var updateId = request.getSublistValue({group: 'custpage_clgx2_package_list', name: 'custpage_clgx2_pckg_re_id', line: i });
					var updateNotes = request.getSublistValue({group: 'custpage_clgx2_package_list', name: 'custpage_clgx2_pckg_re_rn', line: i });
					
					var updatePackage = record.load({type: 'customrecord_clgx_package', id: updateId, isDynamic: true});
					updatePackage.setValue('custrecord_clgx_pkg_delivery_status', 3);
					updatePackage.setValue('custrecord_clgx_pkg_release_notes', updateNotes);
					log.debug({title: 'Boolean', details: updatePackage.getValue('custrecord_clgx_package_stop_increment')});
					updatePackage.setValue('custrecord_clgx_package_stop_increment', true);
					if(claimedBy){
						updatePackage.setValue('custrecord_clgx_pkg_claimed_by', claimedBy);
					}
					if(claimedByOther){
						updatePackage.setValue('custrecord_clgx_pkg_claimed_by_other', claimedByOther);
					}
					updatePackage.setValue('custrecord_clgx_pkg_release_date', new Date());
					updatePackage.setValue('custrecord_clgx_pkg_released_by', releasedBy);
					updatePackage.save();
				}
			}
			redirect.redirect({
				url: '/app/site/hosting/scriptlet.nl?script=1883&deploy=1'
			})
    	}
    	
    	
    	function searchCasePackages(caseID) {
            var packageArray = new Array();
        	var searchObject = search.create({ 
        		type: 'customrecord_clgx_package', 
        		columns: ['custrecord_clgx_pkg_tracking_num', 'internalid', 'custrecord_clgx_pkg_internal_track', 'custrecord_clgx_pkg_delivery_status', 'custrecord_clgx_pkg_storage_notes'], 
        		filters: [ 
        			search.createFilter({ name: 'custrecord_clgx_package_case', operator: search.Operator.IS, values: caseID }), 
    		    	search.createFilter({ name: 'custrecord_clgx_pkg_delivery_status', operator: search.Operator.IS, values: 2 })
    		    ]
        	});
        		
        	searchObject.run().each(function(result) {
        		packageArray.push({
    				packageId       : result.getValue('internalid'),
    				internaltracking: result.getValue('custrecord_clgx_pkg_internal_track'),
    				tracking        : result.getValue('custrecord_clgx_pkg_tracking_num'),
    				status          : result.getValue('custrecord_clgx_pkg_delivery_status'),
    				internalnotes   : result.getValue('custrecord_clgx_pkg_storage_notes')
        		});

        		return true;
        	});
        		
        	return packageArray;
    	}
    	
    	function searchCustPackages(custId){
    		        	
        	var packageArray = new Array();
    		var searchObject = search.create({ 
    			type: 'customrecord_clgx_package', 
    			columns: ['custrecord_clgx_pkg_tracking_num', 'internalid', 'custrecord_clgx_pkg_internal_track', 'custrecord_clgx_pkg_delivery_status', 'custrecord_clgx_pkg_storage_notes'], 
    			filters: [ search.createFilter({
				        		name: 'custrecord_clgx_pkg_receiving_company',
				        		operator: search.Operator.IS,
				        		values: custId 		
			    			}), 
			    			search.createFilter({
				        		name: 'custrecord_clgx_pkg_delivery_status',
				        		operator: search.Operator.IS,
				        		values:  2
			    			})
			    			
        	]});
    		searchObject.run().each(function(result) {
    			var packageObject ={
    					packageId : result.getValue('internalid'),
    					internaltracking : result.getValue('custrecord_clgx_pkg_internal_track'),
    					tracking: result.getValue('custrecord_clgx_pkg_tracking_num'),
    					status: result.getValue('custrecord_clgx_pkg_delivery_status'),
    					internalnotes: result.getValue('custrecord_clgx_pkg_storage_notes')
    			}
   			
    			packageArray.push(packageObject);

    			return true;
    		});
    		
    		return packageArray;
    		
    	}
    }
    
    /**
     * Returns an array of active customers in a key/value pair.
     * 
     * @access private
     * @function buildCustomerList
     * 
     * @returns {Array}
     */
    function buildCustomerList() {
    	var excludedCustomers = [487967, 502186, 963445, 2763, 2789, 2790, 2674650];
    	
		var customerArray = new Array();
		customerArray.push({ id: "", name: "" });
		
    	var columns = new Array();
    	columns.push(search.createColumn("internalid"));
    	columns.push(search.createColumn({ name: "companyname", sort: search.Sort.ASC }));
    	
    	
    	var filters = new Array();
    	filters.push(search.createFilter({ name: "isjob",        operator: "is",             values: ["F"]      }));
    	filters.push(search.createFilter({ name: "companyname",  operator: "doesnotcontain", values: ["parent"] }));
    	filters.push(search.createFilter({ name: "entitystatus", operator: "is",             values: ["13"]     }));
    	
    	var searchObject     = search.create({ type: "customer", columns: columns, filters: filters });
    	var searchResults    = searchObject.runPaged({ pageSize: 1000 });
    	var searchPageLength = searchResults.pageRanges.length
    	
    	for(var page = 0; page < searchPageLength; page++) {
    		var pageObject = searchResults.fetch(page);
    		pageObject.data.forEach(function(result) {
    			if(excludedCustomers.indexOf(parseInt(result.getValue("internalid"))) == -1) {
    				customerArray.push({ id: result.getValue("internalid"), name: result.getValue("companyname") });
    			}
    		});
    	}
    	
    	return customerArray;
    }

    return {
        onRequest: onRequest
    };
    
});
