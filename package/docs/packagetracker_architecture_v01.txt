Cologix Package Tracker Documentation
Version: 1
Date: 9/18/19


Sections:
1. High Level
2. Components
	2.1. Scripts
	2.2. Records
	2.3. Saved Searches
3. Customer Portal
4. NetSuite


1. High level:
	This document will provide a high level overview of the package tracker architecture and processes.


2. Components
	The architecture of Cologix's Pacjage tracker is split up into multiple conponents for scalability and maintainability 
	purposes. Each component [script/process] exists independently of the other and has the ability to fail or be taken offline 
	without affecting performance or availability of another.
	
	
2.1. Scripts
	CLGX2_SU_Package: The primary user event script which will handle emailing customers when a package has been delivered or released.
	CLGX2_SS_Package_Daily: This script runs daily and calculates the number of days which the package has been in storage. 
	CLGX2_SS_Package_Invoice: Generates invoices from a package record on the specified date to be sent to the customer.
	

2.2. Records
	Package (customrecord_clgx_package.xml): The primary package record
	Package Storage Pricing (customrecord_clgx_pkg_storage_pricing.xml): The table used to calculate storage rates and the number of days a package can be stored before Cologix starts billing.





create:
{
   facilityid: [integer],
   companyid: [integer],
   contactid: [integer],
   othercontact: [string],
   packagetypeid: [integer],
   courierid: [integer],
   expecteddate: [date],
   notes: [longtext],
   trackingnumbers: [array(string)]
}


Get List:

request:
{ companyid: [integer] }


response:
array(object) [{
   packageid: [integer],
   created: [date/time],
   recipientid: [integer],
   facilityid: [integer],
   packagetypeid: [integer],
   trackingnumber: [string],
   courierid: [integer],
   statusid: [integer],
   received: [date/time],
   claimed: [date/time],
   claimedby: [integer]
}]



Get Package:

request: 
{ 
   companyid: [integer],
   packageid: [integer]
}

response:
{
   packageid: [integer],
   created: [date/time],
   recipient: [string],
   facility: [string],
   packagetype: [string],
   trackingnumber: [string],
   courier: [string],
   status: [string],
   received: [date/time],
   claimed: [date/time],
   claimedby: [string]
}



update:
{
   packageid: [integer],
   facilityid: [integer],
   companyid: [integer],
   contactid: [integer],
   othercontact: [string],
   courierid: [integer],
   expecteddate: [date],
   packagetypeid: [integer],
   statusid: [integer],
   notes: [longtext],
   trackingnumbers: [array(string)]
}

cancel:
{
   packageid: [integer],
   statusid: [integer]
}
