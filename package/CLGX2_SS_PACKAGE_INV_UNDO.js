/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/runtime', 'N/task'],
/**
 * @param {record} record
 * @param {search} search
 */
function(record, search, runtime, task) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	try{
    		deletePackageInv();
    	} catch(ex) {
    		log.error({ title: "execute - Error", details: ex });
    	}
    }
    
    function deletePackageInv(){
    	var searchObject = search.load({ id: "customsearch_clgx_package_invoices_today" });
    	var reschedule = false;
    	var count = 0;
    	searchObject.run().each(function(result) {
	    	var usage        = runtime.getCurrentScript().getRemainingUsage();
//	    	log.debug({title: 'Usage', details: usage});
    		count++;
			if(count == 4000){
				reschedule = true;
				return false;
			} else if (usage <= 200){
				reschedule = true;
		    	return false;
			}
    		try {
    			record.delete({ type: record.Type.INVOICE, id: result.getValue("internalid") });
    			return true;
    		} catch(ex) {
//    	   		log.error({ title: "deletePackageInv - Error", details: ex });
    	   		return true;
    		}
    	});
    	    if(reschedule == true){
				rescheduleScript();
				return false;
			}
    }
    
    function rescheduleScript(){
    	
        var scheduledScriptTask = task.create({
	           taskType: task.TaskType.SCHEDULED_SCRIPT
	    });
    	
        scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
        scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
        return scheduledScriptTask.submit();
    }

    return {
        execute: execute
    };
    
});
