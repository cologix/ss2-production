/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/email', 'N/search', 'N/record', "/SuiteScripts/clgx/libraries/lodash.min", 'N/runtime', 'N/task', 'N/format'],
/**
 * @param {email} email
 * @param {search} search
 * @param {record} record
 */
function(email, search, record, _, runtime, task, format) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	
    	var rt = runtime.getCurrentScript();
    	
    	var paramObject           = new Object();
    	paramObject.initialIndex    = rt.getParameter("custscript_clgx_1923_index");
    	
    	sendReminderEmail(paramObject);
    }
    
    function sendReminderEmail(paramObject){
    	var savedSearchId = 'customsearch_clgx_package_reminder_email';
    	var reschedule = false;
    	var count = 0;
    	var searchObject = search.load({ id: savedSearchId });
    	searchObject.run().each(function(result) {
	    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    	count++;
	    	if (usage <= 200){
				reschedule = true;
		    	return false;
			}
			else {
	 			buildEmail(result);
	    		return true;
			}
    	});
    	
    	if(reschedule == true){
    		rescheduleScript(count)
    	}
    }
    
    function buildEmail(resultObject){
    	var facility = resultObject.getValue('custrecord_clgx_pkg_storage_facility');
    	var packageType = resultObject.getValue('custrecord_clgx_pkg_package_type');
    	
    	var replyToObject = buildReplyToMap(facility);
    	
    	var emailTemplateObject  = record.load({ type: "emailtemplate", id: 209});
    	var emailTemplateSubject = emailTemplateObject.getValue("subject");
    	var emailTemplateBody    = emailTemplateObject.getValue("content");
    	
    	var packageType = newRecord.getText("custrecord_clgx_pkg_package_type");
		if(packageType.indexOf("(") > -1) {
			var tmpPackageType = packageType.split("(");
			packageType = tmpPackageType[0].trim();
		}
    	
		emailTemplateBody = emailTemplateBody.replace(new RegExp("{packagetype}"), packageType);
		emailTemplateBody = emailTemplateBody.replace(new RegExp("{shippingcarrier}"), newRecord.getText("custrecord_clgx_pkg_shipping_carrier"));
		
		var companyContact   = newRecord.getText("custrecord_clgx_pkg_company_contact");
		var packageAttention = newRecord.getValue("custrecord_clgx_pkg_attention");
		var packageRecipient = "";
		if(!companyContact) { 
			packageRecipient = packageAttention; 
		} else { 
			var tmpContact   = companyContact.split(":");
			packageRecipient = tmpContact[tmpContact.length - 1].trim();
		}
		
		emailTemplateBody = emailTemplateBody.replace(new RegExp("{packagerecipient}"), packageRecipient);
    	
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{trackingnumber}"), resultObject.getValue('custrecord_clgx_pkg_tracking_num'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{internaltracking}"), resultObject.getValue('custrecord_clgx_pkg_internal_track'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facility}"), resultObject.getText('custrecord_clgx_pkg_storage_facility'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{deliverydate}"), resultObject.getValue('custrecord_clgx_pkg_delivery_date'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{facilityaddress}"), resultObject.getValue('custrecord_clgx_package_address_full'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{daysinstorage}"), resultObject.getValue('custrecord_clgx_package_total_days'));
    	emailTemplateBody = emailTemplateBody.replace(new RegExp("{storagerate}"), format.format({ value: resultObject.getValue('custrecord_clgx_pkg_storage_rate'), type: format.Type.CURRENCY }));
    	
    	var companyContactArray = new Array();
    	var searchObject        = search.load({ id: "customsearch_clgx_pkg_emails" });
    	
    	searchObject.filters.push(search.createFilter({ name: "company", operator: "anyof", values: [resultObject.getValue("custrecord_clgx_pkg_receiving_company")] }));
    	
    	searchObject.run().each(function(result) {
    		companyContactArray.push({ id: result.getValue("internalid"), email: result.getValue("email"), notify: result.getValue("custentity_clgx_contact_notify_ship") });
    		return true;
    	});
    	
    	var filteredContactArray              = filterNotifyContacts(companyContactArray); //Filter down to contacts that recieve shipping emails.
    	var filteredCompanyContactArrayLength = filteredContactArray.length;
    	
    	if(filteredCompanyContactArrayLength > 0) {
    		companyContactArray = filteredContactArray;
    	} else {
    		companyContactArray = _.map(companyContactArray, "id");
    	}
    	
    	var companyContactArrayLength = companyContactArray.length;
    	
    	try{
		    if(companyContactArrayLength > 10) {
		    	var companyContactObject       = _.chunk(companyContactArray, 10);
		    	var companyContactObjectLength = companyContactObject.length;
		    	
		    	for(var contactSet = 0; contactSet < companyContactArrayLength; contactSet++) {
		    		if(companyContactObject[contactSet]) {
		    			email.send({ 
		       				author: 432742, 
		       				replyTo: replyToObject.email,
		       				recipients: companyContactObject[contactSet], 
		       				subject: emailTemplateSubject, 
		       				body: emailTemplateBody, 
		       				relatedRecords: {
		       					customRecord: {
		       						id: resultObject.id,
		       						recordType: "customrecord_clgx_package"
		       					}
		       				} 
		       			});
		    		}
		    	}
		    } else {
		    	email.send({ 
					author: 432742, 
					replyTo: replyToObject.email,
					recipients: companyContactArray, 
					subject: emailTemplateSubject, 
					body: emailTemplateBody, 
					relatedRecords: {
						customRecord: {
							id: resultObject.id,
							recordType: "customrecord_clgx_package"
						}
					} 
				});
		    }
		    
		    record.submitFields({ type: "customrecord_clgx_package", id: resultObject.id, values: { custrecord_clgx_pckg_reminder_date: new Date() } });
    	} catch(ex) {
    		log.debug({title: 'Reminder Email-Error', details: ex});
    	} 
    }
    
    /**
     * Returns an email based off of the facility on the package record.
     * 
     * @access private
     * @function buildReplyToMap
     * @usage 0
     * 
     * @param {Number} facilityID
     * @returns {String}
     */
    function buildReplyToMap(facilityID) {
    	if(facilityID) {
    		//Columbus
    		if(facilityID == 24 || facilityID == 29) { return {id: 1893287, email: "col.operations@cologix.com"}; }
    		
    		//Dallas
    		if(facilityID == 3 || facilityID == 18 || facilityID == 44) { return {id: 1893288, email: "dal.operations@cologix.com"}; }
    		
    		//JAX1
    		if(facilityID == 21) { return {id: 1892683, email: "jax1.operations@cologix.com"}; }
    		
    		//JAX2
    		if(facilityID == 27) { return {id: 1892784, email: "jax2.operations@cologix.com"}; }
    		
    		//LAK
    		if(facilityID == 28) { return {id: 559901, email: "lak.operations@cologix.com"}; }
    		
    		//MIN
    		if(facilityID == 17 || facilityID == 25) { return {id: 1892886, email: "min.operations@cologix.com"}; }
    		
    		//Montreal
    		if(facilityID == 2 || facilityID == 5 || facilityID == 4 || facilityID == 9 || facilityID == 6 || facilityID == 7 || facilityID == 19 || facilityID == 45 || facilityID == 51 || facilityID == 52 || facilityID == 53) { return {id: 1894604, email: "mtl.operations@cologix.com"}; }
    		
    		//NNJ
    		if(facilityID == 34 || facilityID == 33 || facilityID == 35 || facilityID == 36) { return {id: 1876217, email: "nnj.operations@cologix.com"}; }
    		
    		//TOR
    		if(facilityID == 8 || facilityID == 15 || facilityID == 43) { return {id: 1894303, email: "tor.operations@cologix.com"}; }
    		
    		//VAN
    		if(facilityID == 14 || facilityID == 20 || facilityID == 42) { return {id: 1893289, email: "van.operations@cologix.com"}; }
    	}
    }
    
    function filterNotifyContacts(contactArray) {
    	if(contactArray) {
    		return _.map(_.filter(contactArray, { "notify": true }), "id");
    	}
    }
    
    function rescheduleScript(param){
	   var scheduledScriptTask = task.create({
	            taskType: task.TaskType.SCHEDULED_SCRIPT,
	            params: {custscript_clgx_1923_index : param}
	        });
        scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
        scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
        return scheduledScriptTask.submit();
    }

    return {
        execute: execute
    };
    
});
