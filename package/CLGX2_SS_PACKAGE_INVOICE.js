/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', '/SuiteScripts/clgx/libraries/lodash.min','/SuiteScripts/clgx/libraries/moment.min', 'N/task', 'N/file'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search, _, moment, task, file) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(context){
    	var rt = runtime.getCurrentScript();
    	
    	var paramObject              		  = new Object();
    	paramObject.savedSearchID    		  = "";
    	paramObject.actionType       		  = rt.getParameter("custscript_clgx2_1881_at");    	
    	paramObject.fileID           		  = parseInt(rt.getParameter("custscript_clgx2_1881_fid"));
    	paramObject.initialIndex     		  = parseInt(rt.getParameter("custscript_clgx2_1881_idx"));
    	paramObject.currentIndex     		  = 0;
    	paramObject.currentInvIndex 		  = parseInt(rt.getParameter("custscript_clgx2_1881_idx"));
    	paramObject.initialInvIndex			  = 0;    	
    	paramObject.currentSubIndex 		  = parseInt(rt.getParameter("custscript_clgx2_1881_lidx"));
    	paramObject.initialSubIndex			  = 0;
    	
    	paramObject.pageSize         		  = parseInt(rt.getParameter("custscript_clgx2_1881_ps"));
    	
    	if(!paramObject.actionType) {
    		paramObject.actionType = "file";
    	}
    	
    	log.debug(paramObject.actionType);
    	
    	if(paramObject.actionType == "file") {
    		paramObject.savedSearchID = "customsearch_clgx_package_billing_queue";
    		var reschedule            = createIDFile(paramObject);
    			
    		if(!reschedule) { 
    			paramObject.currentIndex = 0;
    			paramObject.initialIndex = 0;
    			paramObject.actionType   = "group";
    		}
    			
    		scheduleScript(paramObject);
    	} else if(paramObject.actionType == "group"){
    		var reschedule            = groupInv(paramObject);
    			
    		if(!reschedule) { 
    			paramObject.currentInvIndex = 0;
    			paramObject.initialInvIndex = 0;
    			paramObject.actionType   = "create";
    		}
    			
    		scheduleScript(paramObject);
    	} 
    	else if(paramObject.actionType == "create") {
    		var reschedule            = createInvoices(paramObject);
   			
   			if(reschedule) { 
   				scheduleScript(paramObject);
    		}
    	} 
    	
    	
    }
    
    function createIDFile(paramObject){
    	try {
    		var recordIDArray   = new Array();
        	
        	var processResults  = null;
        	var existingContent = "";
        	var globalIndex     = 0;
        	
        	var filters         = new Array();
        	var columns         = new Array();
        	var ids             = new Array();
        	
        	var searchObject  = search.load({ id: paramObject.savedSearchID }).run();
        	var searchResults = searchObject.getRange({ start: paramObject.initialIndex, end: (paramObject.initialIndex + 1000) });
        	
        	processResults = processResultSet(paramObject, searchResults);
        	
        	if(!paramObject.initialIndex && paramObject.initialIndex <= 0) {
        		recordIDArray = processResults.ids;
        	} else {
        		var existingFile = file.load({ id: paramObject.fileID });
        		existingContent  = JSON.parse(existingFile.getContents());
        		recordIDArray    = recordIDArray.concat(existingContent).concat(processResults.ids);
        	}
        	
        	while (processResults.nextPage == true) {
        		searchResults  = searchObject.getRange({ start: paramObject.currentIndex, end: (paramObject.currentIndex + 1000) })
        		processResults = processResultSet(paramObject, searchResults);
        		recordIDArray = recordIDArray.concat(processResults.ids);
        	}
        	
        	var fileObject = file.create({ 
    			name: "packagebill_ids.json", 
    			fileType: file.Type.PLAINTEXT,
    			folder: 13967297,
    			contents: JSON.stringify(recordIDArray)
    		});
        	
        	paramObject.fileID = fileObject.save();
        	
        	return processResults.reschedule;
    	} catch(ex) {
    		log.debug({ title: "createIDFile - Error", details: ex });
    	}
    }
    
    /**
     * Processes each page of results.
     */
    function processResultSet(paramObject, searchResults) {
    	var usage        = runtime.getCurrentScript().getRemainingUsage();
    	var localIndex   = 0;
    	var reschedule   = false;
    	var nextPage     = false;
    	var ids          = new Array();
    	
    	var resultLength = searchResults.length;
    	
    	//Loop through results
    	for(var r = 0; r < resultLength; r++) {
    		if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
    			break;
    		}
    		
    		var validation = validateInv(searchResults[r]);
//    		log.debug({title: 'Validated?', details: validation});
    		
    		if(validation.validate == 'validated'){
	    		ids.push({ 
    				packageid: 			searchResults[r].getValue('internalid'),
    				customer: 			searchResults[r].getText('custrecord_clgx_pkg_receiving_company'),
    				customerid: 		searchResults[r].getValue('custrecord_clgx_pkg_receiving_company'),
    				billing: 			searchResults[r].getValue('custrecord_clgx_package_billing_address'),
    				facility:			searchResults[r].getValue('custrecord_clgx_pkg_storage_facility'),
    				location:			searchResults[r].getValue('custrecord_clgx_package_location'),
    				tracking : 			searchResults[r].getValue('custrecord_clgx_pkg_tracking_num'),
    				internaltracking: 	searchResults[r].getValue('custrecord_clgx_pkg_internal_track'),
    				storagerate : 		searchResults[r].getValue('custrecord_clgx_pkg_storage_rate'),
    				billdays : 			searchResults[r].getValue('custrecord_clgx_package_bill_days'),
    				packagetype: 		searchResults[r].getValue('custrecord_clgx_pkg_package_type'),
    				daterecieved: 		searchResults[r].getValue('custrecord_clgx_pkg_delivery_date'),
    				dateclaimed:		searchResults[r].getValue('custrecord_clgx_pkg_release_date'),
    				startdate: 			searchResults[r].getValue('custrecord_clgx_package_date_charge')
	    		});
    		} 
    		
    		else if(validation.validate == 'error'){
    			var recordId = searchResults[r].getValue('internalid');
    			var errorMessage = validation.message;
    			record.submitFields({
    				type: 'customrecord_clgx_package', id: recordId, values:{custrecord_clgx_package_bill_error: true, custrecord_clgx_package_bill_er_msg: errorMessage},  options: { enableSourcing: false, ignoreMandatoryFields : true }
    			});
    			
    		}
    		
    		localIndex = localIndex + 1;
    	}
    	
    	if(usage <= 200) {
    		reschedule = true;
    		nextPage   = false;
    	} else if(resultLength < paramObject.pageSize) {
    		reschedule = false;
    		nextPage   = false;
    	} else {
    		reschedule = false;
    		nextPage   = true;
    	}
    	
    	paramObject.currentIndex = paramObject.currentIndex + localIndex;
    	
    	log.debug({ title: "reschedule", details: reschedule });
    	log.debug({ title: "nextPage", details: nextPage });
    	log.debug({ title: "paramObject", details: paramObject });
    	
    	return { reschedule: reschedule, nextPage: nextPage, ids: ids }; 
    }   
    
    function groupInv(paramObject){
    	
    	var existingFile = file.load({ id: paramObject.fileID });
		var existingContent  = JSON.parse(existingFile.getContents());
		var customerPackageArray =[];
		var custInvArray = [];
		
    	var groupByFacility = _.groupBy(existingContent, 'facility');
    	
    	_.each(groupByFacility, function(value, key){
    		var locationProcessing = [];
    		
    		for(var i=0; i<value.length; i++){
    			var locationObj = value[i];
    			locationProcessing.push(locationObj);
    		}
    		
    		customerPackageArray.push(locationProcessing);
    		return true;
    	});
    	
    	for(var i=0;i<customerPackageArray.length;i++){
    		var custInv = _.groupBy(customerPackageArray[i], 'customer');
    		custInvArray.push(custInv);
    	}
    	
    	var fileContent = [];
    	
    	for(var i = 0; i<custInvArray.length; i++){
    		var outerArray = custInvArray[i];
    		var addContent = Object.keys(outerArray).map(function(key) {
	    		return [String(key), outerArray[key]];
	    	});
//    		log.debug({title: 'Add Content', details: addContent});
    		fileContent.push(addContent);
    	}
    	
    	var fileObject = file.create({ 
			name: "packagebill_ids.json", 
			fileType: file.Type.PLAINTEXT,
			folder: 13967297,
			contents: JSON.stringify(fileContent)
		});
    	
    	paramObject.fileID = fileObject.save();
    	
    	
    }
    
    
    function createInvoices(paramObject){
    	try{    		
        	var existingFile 		= file.load({ id: paramObject.fileID });
    		var invObjects  		= JSON.parse(existingFile.getContents());
    		var invObjectsLength 	= invObjects.length;
	    	
	    	var today = moment().format("M/D/YYYY");
	    	var lastDayLastMonth = moment(today).subtract(1, 'months').endOf('month').format("M/D/YYYY");
    		var releaseMonth = moment(releaseDate).format('M');
	    	var lastMonth = moment(lastDayLastMonth).format('M');
    			
    		log.debug({title: 'Invoice Object Array Length', details: invObjectsLength});
    		var usage = runtime.getCurrentScript().getRemainingUsage();
    		
//    		paramObject.currentInvIndex = paramObject.initialInvIndex;
//    		paramObject.currentSubIndex = paramObject.initialSubIndex;
    		
    		log.debug({title: 'Starting Index', details: paramObject.currentInvIndex});
    		log.debug({title: 'Starting Subindex', details: paramObject.currentSubIndex});
	    	
    		for(var i = parseInt(paramObject.currentInvIndex); i < invObjectsLength; i++) {  
//    			log.debug({title: 'Current Index', details: paramObject.currentIndex});
    			var outerArray = invObjects[i];
//    			log.debug({title: 'Location length', details: outerArray.length});
    			for(var j = parseInt(paramObject.currentSubIndex); j < outerArray.length; j++){
    				
//        			log.debug({title: 'Current Sub Index', details: paramObject.currentSubIndex});
    				var invData = outerArray[j];
    				var invProcessing = invData[1];
    				var success = createInvoice(invProcessing);
    				if(success){
    					for(var k = 0; k<invProcessing.length; k++){
    						var packageId = invProcessing[k].packageid;
    						var final = false;
    				    	var releaseDate = invProcessing[k].dateclaimed;
    				    	if(releaseDate){
    				    		var releaseMonth = moment(releaseDate).format('M');
    				    		if(releaseMonth === lastMonth){
    				    			final = true;
    				    		}
    				    	}
//    						log.debug({title: 'Package ID' , details: packageId});
    		    			
    				    	if(final === true){
	    				    	record.submitFields({
	    		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_pck_last_mnth_inv: true, custrecord_clgx_package_final_inv: true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
	    		    			});
    				    	} else if(final === false) {
	    				    	record.submitFields({
	    		    				type: 'customrecord_clgx_package', id: packageId, values:{custrecord_clgx_pck_last_mnth_inv: true},  options: { enableSourcing: false, ignoreMandatoryFields : true }
	    		    			});
    				    	}
    					}
    				}
    				paramObject.currentSubIndex = paramObject.currentSubIndex + 1;
            		
    				if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
            			log.debug({title: 'Usage Limit Hit', details: usage});
    					break;
            		}
    			}
    			
    			if((usage = runtime.getCurrentScript().getRemainingUsage()) <= 200) {
           			log.debug({title: 'Usage Limit Hit', details: usage});
        			break;	
        		}
    			
    			else{
	    			paramObject.currentSubIndex = 0;
	    			paramObject.currentInvIndex = paramObject.currentInvIndex + 1;
    			}
        		

    		}
    		
    		paramObject.currentIndex = paramObject.currentIndex + paramObject.currentInvIndex;
    		
    		log.debug({title: 'Final Inv Index', details: paramObject.currentInvIndex});
    		log.debug({title: 'Final Index', details: paramObject.currentIndex});
    		log.debug({title: 'Final Subindex', details: paramObject.currentSubIndex});
    		
    		if(usage <= 200) {
    			return true; //Reschedule
    		}
    	} catch (ex){
       		log.error({ title: "createInvoices- Error", details: ex });
    	}
    	
    }
    
    function validateInv(resultObject){
    	try{
    		var packageId 		= resultObject.getValue('internalid');
    		var customer 		= resultObject.getValue('custrecord_clgx_pkg_receiving_company');
    		var custSub			= resultObject.getValue('custrecord_clgx_package_cust_sub');
    		var locSub			= resultObject.getValue('custrecord_clgx_pkg_loc_sub');
    		var storageRate		= resultObject.getValue('custrecord_clgx_pkg_storage_rate');
    		var billDays		= resultObject.getValue('custrecord_clgx_package_bill_days');
    		var dateClaimed		= resultObject.getValue('custrecord_clgx_pkg_release_date');
    		var startDate		= resultObject.getValue('custrecord_clgx_package_date_charge');
    		var invProcessed	= resultObject.getValue('custrecord_clgx_pck_last_mnth_inv');
    		
    		var validation = {
    				validate : 'validated',
    				message  : 'None'
    		};
			
//check if there is a conflict between subsidiary and location
    		
    		if(custSub != locSub){
    			validation.validate = 'error';
    			validation.message = 'Location/Subsidiary conflict';
    		}
    		
//check that the claim date is not before the receive date
    		
//    		if(moment.(dateClaimed).isBefore(startDate) == true){
//    			validation.validated = false;
//    			validation.message = 'Date claimed is before start charge date.';
//    		}
    		
//check that no invoice is tied to the package for this month			
    		
    		if(invProcessed == true){
    			validation.validate = 'error';
    			validation.message = "This package has already been invoiced for last month's storage";
    		}
    		

    		if(!storageRate){
    			validation.validate = 'error';
    			validation.message = "There is no storage rate for this package. Please contact your administrator.";
    		}
    			
    		return validation;
			
    	} catch (ex) {
     		log.error({ title: "validateInv- Error", details: ex });
    	}
    }
    
   
    function createInvoice(resultObject){
    	try{
	    	var custId = resultObject[0].customerid;
	    	var facility = resultObject[0].facility;
	    	var location = resultObject[0].location;
//	    	log.debug({title: 'location', details: location});
	    	var billing = resultObject[0].billing;  	
	    	  
	    	//Build sublist items
	    	var invItemsLen = resultObject.length;
	    	var invItems = [];
	    	for(var i=0; i<invItemsLen; i++){
	    		var itemAmount = resultObject[i].storagerate * resultObject[i].billdays;
	    		var itemObj = {
	    				packagetype : resultObject[i].packagetype,
	    				storagerate : resultObject[i].storagerate,
	    				tracking : resultObject[i].tracking,
	    				billdays : resultObject[i].billdays,
	    				packageid : resultObject[i].packageid,
	    				amount : itemAmount, 
	    				dateclaimed : resultObject[i].dateclaimed,
	    				datereceived: resultObject[i].daterecieved,
	    				startdate: resultObject[i].startdate
	    		  }	
	    		invItems.push(itemObj);
	    	  }
	    	  
	    	  var recordObject = record.create({ type: record.Type.INVOICE, isDynamic: true});
	    	  recordObject.setValue({fieldId:'entity', value: custId});
	    	  recordObject.setValue({fieldId:'location', value: location});
	    	  
	    	  //Find and set billing address
	    	  var billingid= '';    	  
	    	  if(billing=='- None -'){
	    		  var billingid = returnBilling(location, custId);
	    	  } if(billingid!=''){
	    		  var custRecord = record.load();
	    		  var NRABs = custRecord.getLineCount({
	    			  sublistId: 'addressbook'
	    		  });
	    		  for(var j = 1; j<=NRABs; j++){
	    			  var internalid = custRecord.getSublistValue({
	    				  sublistId: 'addressbook',
	    				  fieldId: 'internalid',
	    				  line: j
	    			  });
	    			  if(internalid == billingid){
	                      var billaddress=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_text', line:j});
	                      var billaddresslist=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressid', line:j});
	                      var billingaddress_key=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_key', line:j});
	                      var billingaddress_text=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_text', line:j});
	                      var billingaddress_type=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressbookaddress_type', line:j});
	                      var billisresidential=custRecord.getSublistValue({sublistId:'addressbook', fieldId:'isresidential', line:j});
	                      var addressee = custRecord.getSublistValue({sublistId:'addressbook', fieldId:'addressee', line:j});
	    			  }
	    		  }
	    		  recordObject.setValue({fieldId:'billaddress', value:billaddress});
	    		  recordObject.setValue({fieldId:'billaddressee', value:addressee});
	    		  recordObject.setValue({fieldId:'billaddresslist', value:billaddresslist});
	    		  recordObject.setValue({fieldId:'billingaddress_key', value:billingaddress_key});
	    		  recordObject.setValue({fieldId:'billingaddress_text', value:billingaddress_text});
	    		  recordObject.setValue({fieldId:'billingaddress_type', value:billingaddress_type});
	    		  recordObject.setValue({fieldId:'billisresidential', value:'F'});
	    	  } else {
	    		  recordObject.setValue({fieldId:'billaddress', value:billing});
	    	  }
	    	  
	    	  
	    	  //Add packages to item sublist
	    	  
	    	  for(var j=0; j<invItemsLen; j++){
	    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'item', line: j, value: 586});
	    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'description', line: j, value: buildMemo(invItems[j].dateclaimed, invItems[j].daterecieved, invItems[j].startdate, invItems[j].tracking)});
	    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'quantity', line: j, value: invItems[j].billdays});
	    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'rate', line: j, value: invItems[j].storagerate});
	    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'location', line: j, value: location});
	    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'amount', line: j, value: invItems[j].amount});
	    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'custcol_clgx_package_id', line: j, value: invItems[j].packageid});
	    	  
			    	  //Find and set tax
			    	  var tax = returnTax(location, custId);
	//		    	  log.debug({title:'Tax', details: tax});
			    	  if(tax!=''){
			    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'taxcode', line: j, value: tax[0]});
			    		  recordObject.setCurrentSublistValue({sublistId: 'item', fieldId:'taxcode_display', line: j, value: tax[1]});
			    	  }
			  
			     recordObject.commitLine({
			    	 sublistId: 'item'
			     })
	
	    	  }
	    	  
	    	  recordObject.save();
	    	  
	    	  return recordObject.id;
	    	  
	    	  
    	  } catch (ex){
         		log.error({ title: "createInvoice- Error", details: ex });
    	  }
    	  
    }	  
         
        
    function returnTax(locationid, customerid){
    	var returntax = '';
    	var returntaxText = '';
		var customertax = record.load({type: record.Type.CUSTOMER, id: customerid});
		
	    switch(parseInt(locationid)) {

	        case 33: // Columbus
	            returntax = customertax.getValue('custentity_clgx_tax_item_col'); // COL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_col'); // COL
	            break;
	        case 34: // COL1&2
	            returntax = customertax.getValue('custentity_clgx_tax_item_col'); // COL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_col'); // COL
	            break;
	        case 39: // COL3
	            returntax = customertax.getValue('custentity_clgx_tax_item_col'); // COL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_col'); // COL
	            break;
	        case 2: // Dallas Infomart
	            returntax = customertax.getValue('custentity_clgx_tax_item_dal'); // DAL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_dal'); // DAL
	            break;
	        case 17: // Dallas Infomart - Ste 2010
	            returntax = customertax.getValue('custentity_clgx_tax_item_dal'); // DAL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_dal'); // DAL
	            break;
	        case 73: // DAL3
	            returntax = customertax.getValue('custentity_clgx_tax_item_dal'); // DAL3
	            returntaxText = customertax.getText('custentity_clgx_tax_item_dal'); // DAL3
	            break;
	        case 31: // JAX1
	            returntax = customertax.getValue('custentity_clgx_tax_item_jax'); // JAX1
	            returntaxText = customertax.getText('custentity_clgx_tax_item_jax'); // JAX1
	            break;
	        case 40: // JAX2
	            returntax = customertax.getValue('custentity_clgx_tax_item_jax'); // JAX2
	            returntaxText = customertax.getText('custentity_clgx_tax_item_jax'); // JAX2
	            break;
	        case 42: // LAK1
	            returntax = customertax.getValue('custentity_clgx_tax_item_lak'); // LAK1
	            returntaxText = customertax.getText('custentity_clgx_tax_item_lak'); // LAK1
	            break;
	        case 16: // MIN1&2
	            returntax = customertax.getValue('custentity_clgx_tax_item_min'); // MIN
	            returntaxText = customertax.getText('custentity_clgx_tax_item_min'); // MIN
	            break;
	        case 35: // MIN3
	            returntax = customertax.getValue('custentity_clgx_tax_item_min'); // MIN
	            returntaxText = customertax.getText('custentity_clgx_tax_item_min'); // MIN
	            break;
	        case 5: // MTL1
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 8: // MTL2
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 9: // MTL3
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 10: // MTL4
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 11: // MTL5
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 12: // MTL6
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 27: // MTL7
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 74: // MTL8
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 77: // MTL9
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 78: // MTL10
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        case 79: // MTL11
	            returntax = customertax.getValue('custentity_clgx_tax_item_mtl'); // MTL
	            returntaxText = customertax.getText('custentity_clgx_tax_item_mtl'); // MTL
	            break;
	        ///NNJ
	        case 53: // NNJ1
	            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ1
	            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ1
	            break;
	        case 54: // NNJ2
	            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ2
	            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ2
	            break;
	        case 55: // NNJ3
	            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ3
	            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ3
	            break;
	        case 56: // NNJ4
	            returntax = customertax.getValue('custentity_clgx_tax_item_nnj'); // NNJ4
	            returntaxText = customertax.getText('custentity_clgx_tax_item_nnj'); // NNJ4
	            break;
	        case 13: // 156 Front Street West
	            returntax = customertax.getValue('custentity_clgx_tax_item_tor');// TOR
	            returntaxText = customertax.getText('custentity_clgx_tax_item_tor');// TOR
	            break;
	        case 14: // Barrie Office
	            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
	            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
	            break;
	        //TOR
	        case 6: // TOR1
	            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
	            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
	            break;
	        case 15: // TOR2
	            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
	            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
	            break;
	        case 71: //TOR3 
	            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // TOR
	            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // TOR
	            break;   
	        case 18: // 151 Front Street West - Ste 822
	            returntax = customertax.getValue('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
	            returntaxText = customertax.getText('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
	            break;
	        case 28: // 1050 Pender
	            returntax = customertax.getValue('custentity_clgx_tax_item_van'); // VAN
	            returntaxText = customertax.getText('custentity_clgx_tax_item_van'); // VAN
	            break;
	        case 7: // Harbour Centre
	            returntax = customertax.getValue('custentity_clgx_tax_item_van'); // VAN
	            returntaxText = customertax.getText('custentity_clgx_tax_item_van'); // VAN
	            break;
	        case 60: // VAN3
	            returntax = customertax.getValue('custentity_clgx_tax_item_van'); // VAN3
	            returntaxText = customertax.getText('custentity_clgx_tax_item_van'); // VAN3
	            break;
	
	        default:
	            returntax = '';

	    }
    var returntaxArray=[returntax,returntaxText]
    return returntaxArray;
    
    }
    
    function returnBilling(locationid, customerid){

        var returnbilling = '';
//Bell Canada        -          MTL1              -               1114
//                       -          MTL2              -               24133
//                       -          MTL3              -               24159
//                       -          MTL7              -               24653

        if(customerid==2340)
        {
            if(locationid==5)
            {
                returnbilling=1114;
            }
            if(locationid==8)
            {
                returnbilling=24133;
            }
            if(locationid==9)
            {
                returnbilling=24159;
            }
            if(locationid==27)
            {
                returnbilling=24653;
            }
        }

//        Fibrenoire           -           MTL3              -              24130
//                             -           MTL4              -              11411
//                             -           MTL5              -              15044
        //   -                                MTL1              -              24128

        if(customerid==1720)
        {
            if(locationid==5)
            {
                returnbilling=24128;
            }
            if(locationid==9)
            {
                returnbilling=24130;
            }
            if(locationid==10)
            {
                returnbilling=11411;
            }
            if(locationid==11)
            {
                returnbilling=15044;
            }
        }
//        Hurricane Electric CA -     TOR1 or TOR2    -           6232
//                           -          VAN1 OR VAN2  -           15313
//                -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 15314
        if(customerid==6501)
        {
            if((locationid==14)||(locationid==15)||(locationid==6))
            {
                returnbilling=6232;
            }
            if((locationid==7)||(locationid==28))
            {
                returnbilling=15313;
            }
            if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
            {
                returnbilling=15314;
            }
        }
//        Hurricane Electric US -   MIN1&2, MIN3      -           6084
//            -    DAL1 & DAL2       -          15315
//            -    COL1&2, COL3    -           23627

        if(customerid==9519)
        {
            if((locationid==16)||(locationid==35))
            {
                returnbilling=6084;
            }
            if((locationid==2)||(locationid==17))
            {
                returnbilling=15315;
            }
            if((locationid==34)||(locationid==39))
            {
                returnbilling=23627;
            }
        }

//        Hydro One Telecom Inc - TOR1 or TOR2    -           1032
//            -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 12949
        if(customerid==1764)
        {
            if((locationid==14)||(locationid==15))
            {
                returnbilling=40342;
            }
            if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
            {
                returnbilling=1032;
            }
        }
        if(customerid==723161)
        {
            if((locationid==14)||(locationid==15))
            {
                returnbilling=40342;
            }

        }

//        Netelligent Hosting      -          MTL1               -         958
//            -          MTL2               -         11243
//            -          MTL3               -         11244
//            -          MTL4               -         11245
//            -          MTL5               -         11246
//            -      TOR1 or TOR2          -           11372
        if(customerid==2191)
        {
            if((locationid==14)||(locationid==15))
            {
                returnbilling=11372;
            }
            if(locationid==5)
            {
                returnbilling=958;
            }
            if(locationid==8)
            {
                returnbilling=11243;
            }
            if(locationid==9)
            {
                returnbilling=11244;
            }
            if(locationid==10)
            {
                returnbilling=11245;
            }
            if(locationid==11)
            {
                returnbilling=11246;
            }

        }
        if(customerid == 906673) {
            returnbilling = 60117;
        }
        if(customerid == 395249) {
            returnbilling = 60118;
        }
        if(customerid == 395254) {
            returnbilling = 60119;
        }
        if(customerid == 1006770) {
            returnbilling = 60120;
        }
        if(customerid==791328)
        {
            returnbilling=52716;
        }
        if(customerid==137226)
        {
            returnbilling=52717;
        }
        if(customerid==473887)
        {
            returnbilling=54877;
        }
        if(customerid==76059)
        {
            returnbilling=25096;
        }
        if(customerid==76059)
        {
            returnbilling=25096;
        }
        if(customerid==1657)
        {
            returnbilling=1106;
        }
        if(customerid==3847)
        {
            returnbilling=1893;
        }
        if(customerid==10859)
        {
            returnbilling=6878;
        }
        if(customerid==401284)
        {
            returnbilling=24771;
        }
        if(customerid==89)
        {
            returnbilling=36;
        }
        if(customerid==8937)
        {
            returnbilling=5733;
        }
        if(customerid==137232)
        {
            returnbilling=15487;
        }
        if(customerid==922783)
        {
            returnbilling=59322;
        }
        if(customerid==922786)
        {
            returnbilling=59328;
        }
        if(customerid==238400)
        {
            returnbilling=59340;
        }
        if(customerid==789277)
        {
            returnbilling=59336;
        }
        return returnbilling;
    }
    
    function getLocation(location){
    	//COL1&2
      if(location==24)
      {
          var loc=34;
      }
      //TOR 156 Front
      if(location== 23)
      {
          var loc=13;
      }
      //Cologix HQ
      if(location== 16)
      {
          var loc=0;
      }
      //DAL1
      if(location==3)
      {
          var loc=2;
      }
      //DAL2
      if(location==18)
      {
          var loc=17;
      }
      //JAX1
      if(location==21)
      {
          var loc=31;
      }
      //JAX2
      if(location==27)
      {
          var loc=40;
      }
      //MIN1&2
      if(location==17)
      {
          var loc=16;
      }
      //MIN3
      if(location==25)
      {
          var loc=35;
      }
      //MTL1
      if(location==2)
      {
          var loc=5;
      }
      //MTL2
      if(location==5)
      {
          var loc=8;
      }
      //MTL3
      if(location==4)
      {
          var loc=9;
      }
      //MTL4
      if(location==9)
      {
          var loc=10;
      }
      //MTL5
      if(location==6)
      {
          var loc=11;
      }
      //MTL6
      if(location==7)
      {
          var loc=12;
      }
      //MTL7
      if(location==19)
      {
          var loc=27;
      }
      //TOR1
      if(location==8)
      {
          var loc=6;
      }
      //TOR2
      if(location==15)
      {
          var loc=15;
      }
      //VAN1
      if(location==14)
      {
          var loc=28;
      }
      //VAN2
      if(location==20)
      {
          var loc=7;
      }
      return loc;
    }
    
    function buildMemo(dateClaimed, dateReceived, startDate, tracking){
    	var today = moment().format("M/D/YYYY");
    	var lastDayLastMonth = moment(today).subtract(1, 'months').endOf('month').format("M/D/YYYY");
    	var firstDayLastMonth = moment(today).subtract(1, 'months').startOf('month').format("M/D/YYYY");
    	var lastMonth = moment(lastDayLastMonth).format('M');
    	var startMonth = moment(startDate).format('M');
    	var claimMonth = moment(dateClaimed).format('M');
    	var thisMonth = moment(today).format('M');
    	var memo = '';
    	
    	if(lastMonth === startMonth){
    		if(!dateClaimed){
    			var firstDayOfMonthCharges = startDate;
    			var lastDayOfMonthCharges = lastDayLastMonth;
    			memo = tracking + ' Storage Charges for ' + firstDayOfMonthCharges + ' - ' + lastDayOfMonthCharges;
    		} else if (claimMonth === lastMonth){
    			var firstDayOfMonthCharges = startDate;
    			var lastDayOfMonthCharges = dateClaimed;
    			memo = tracking + ' Storage Charges for ' + firstDayOfMonthCharges + ' - ' + lastDayOfMonthCharges;
    		} else {
    			var firstDayOfMonthCharges = startDate;
    			var lastDayOfMonthCharges = lastDayLastMonth;
    			memo = tracking + ' Storage Charges for ' + firstDayOfMonthCharges + ' - ' + lastDayOfMonthCharges;
    		}
    	} else {
    		if(!dateClaimed){
    			var firstDayOfMonthCharges = firstDayLastMonth;
    			var lastDayOfMonthCharges = lastDayLastMonth;
    			memo = tracking + ' Storage Charges for ' + firstDayOfMonthCharges + ' - ' + lastDayOfMonthCharges;
    		} else if (claimMonth == thisMonth)  {
    			var firstDayOfMonthCharges = firstDayLastMonth;
    			var lastDayOfMonthCharges = lastDayLastMonth;
    			memo = tracking + ' Storage Charges for ' + firstDayOfMonthCharges + ' - ' + lastDayOfMonthCharges;
    		} else {
    			var firstDayOfMonthCharges = firstDayLastMonth;
    			var lastDayOfMonthCharges = dateClaimed;
    			memo = tracking + ' Storage Charges for ' + firstDayOfMonthCharges + ' - ' + lastDayOfMonthCharges;
    		}
    	}
    	
    	return memo;
    }
    
    function updatePackage(){
    	//if a package fails validation, update the package record with a reason why
    	
    	//if an invoice is 
    	
    }

    function scheduleScript(paramObject) {
    	try {
    		var t          = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
    		t.scriptId     = runtime.getCurrentScript().id;
    		log.debug({title: 'Reschedule Script Index', details: paramObject.currentIndex});
    		t.params       = {
    			custscript_clgx2_1881_at : paramObject.actionType,
    			custscript_clgx2_1881_fid: paramObject.fileID,
        		custscript_clgx2_1881_idx: paramObject.currentIndex,
        		custscript_clgx2_1881_ps : paramObject.pageSize,
        		custscript_clgx2_1881_lidx: paramObject.currentSubIndex
    		};
    		t.submit();
    	} catch(ex) {
    		log.debug({ title: "scheduleScript - Error", details: ex });
    	}
    }

    return {
        execute: execute
    };
    
});
