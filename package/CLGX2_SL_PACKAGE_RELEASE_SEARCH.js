/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/redirect', 'N/search', 'N/ui/serverWidget', '/SuiteScripts/clgx/libraries/lodash.min'],
/**
 * @param {record} record
 * @param {redirect} redirect
 * @param {search} search
 */
function(record, redirect, search, serverWidget, _) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	try{
    		if(context.request.method == "GET") {
        		var form = serverWidget.createForm({ title: "Release Packages" });
        		
        		var customerList = buildCustomerList();
        		
        		var customerName = form.addField({
        			id: 'custpage_clgx2_release_search',
        			//source: 'customer',
        			type: serverWidget.FieldType.SELECT,
        			label: 'Search Customer'
        		});
        		
        		_.forEach(customerList, function(customer) {
        			customerName.addSelectOption({ value: customer.id, text: customer.name });
        		});
        		
        		form.addSubmitButton({label:'Search'});
        		context.response.writePage(form);
    		} else {
    			
    			var parameters         = context.request.parameters;
        		var customerName       = parameters.custpage_clgx2_release_search;
        		
        		redirect.redirect({
    				url: "/app/site/hosting/scriptlet.nl?script=1882&deploy=1",
    				parameters: {cu : customerName}
        		});
    			
    		}
    	} catch(ex){
    		context.response.write(JSON.stringify(ex));
    	}
    }
    
    
    /**
     * Returns an array of active customers in a key/value pair.
     * 
     * @access private
     * @function buildCustomerList
     * 
     * @returns {Array}
     */
    function buildCustomerList() {
    	var excludedCustomers = [487967, 502186, 963445, 2763, 2789, 2790, 2674650];
    	
		var customerArray = new Array();
		customerArray.push({ id: "", name: "" });
		
    	var columns = new Array();
    	columns.push(search.createColumn("internalid"));
    	columns.push(search.createColumn({ name: "companyname", sort: search.Sort.ASC }));
    	
    	var filters = new Array();
    	filters.push(search.createFilter({ name: "isjob",        operator: "is",             values: ["F"]      }));
    	filters.push(search.createFilter({ name: "companyname",  operator: "doesnotcontain", values: ["parent"] }));
    	filters.push(search.createFilter({ name: "entitystatus", operator: "is",             values: ["13"]     }));
    	
    	var searchObject     = search.create({ type: "customer", columns: columns, filters: filters });
    	var searchResults    = searchObject.runPaged({ pageSize: 1000 });
    	var searchPageLength = searchResults.pageRanges.length
    	
    	for(var page = 0; page < searchPageLength; page++) {
    		var pageObject = searchResults.fetch(page);
    		pageObject.data.forEach(function(result) {
    			if(excludedCustomers.indexOf(parseInt(result.getValue("internalid"))) == -1) {
    				customerArray.push({ id: result.getValue("internalid"), name: result.getValue("companyname") });
    			}
    		});
    	}
    	
    	return customerArray;
    }
    

    return {
        onRequest: onRequest
    };
    
});
