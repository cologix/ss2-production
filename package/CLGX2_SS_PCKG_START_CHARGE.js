/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 * 
 * @author Katy Jones - katy.jones@cologix.com
 * @date 9/19/2019
 */
define(['N/record', 'N/search', '/SuiteScripts/clgx/libraries/moment.min', 'N/runtime', 'N/task'],
/**
 * @param {record} record
 * @param {search} search
 * @param {moment} moment
 * @param {runtime} runtime
 * @param {task} task
 */
function(record, search, moment, runtime, task) {
   
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(context) {
    	
		var rt = runtime.getCurrentScript();
    	
    	var paramObject           = new Object();
    	paramObject.actionType    = rt.getParameter("custscript_clgx_1894_at");
    	
    	if(!paramObject.actionType){
    		paramObject.actionType = 'start';
    	}
    	
    	if (paramObject.actionType == 'start'){
    		startChargeCounter();
    	} 
    	
    } 
    	
    function startChargeCounter(){
    	try{
		    var savedSearchID = 'customsearch_clgx_package_start_counter';
		    var count = 0;
		    var reschedule = false;				
			var searchObject = search.load({ id: savedSearchID });

			searchObject.run().each(function(result) {
				var usage        = runtime.getCurrentScript().getRemainingUsage();
				count++;
				if(count == 4000){
					reschedule = true;
					return false;
				} else if (usage <= 200){
					reschedule = true;
			    	return false;
				} else {		
			
					var packageId = result.getValue('internalid');
					var dateReceived = result.getValue('custrecord_clgx_pkg_delivery_date');
					startChargeDays(packageId, dateReceived);
									
					return true;
					
				}
			});			
			
	    	var usage        = runtime.getCurrentScript().getRemainingUsage();
	    	
	    	log.debug({title: 'Function Name', details: 'startChargeCounter'});
	    	log.debug({title: 'Reschedule?', details: reschedule});
	    	log.debug({title: 'Count', details: count});
	    	log.debug({title: 'Usage', details: usage});
			
			if(reschedule == true){
				rescheduleScript('start');
				return false;
			} else {
				
			}

    	} catch(ex) {
    		log.error({ title: "startChargeCounter- Error", details: ex });
    	}
    	
    }
    	
    function startChargeDays(packageId, dateReceived){
       		try {
       			var chargeStartDate = moment(dateReceived).add(3,'day').format('M/D/YYYY');
           		record.submitFields({ type: "customrecord_clgx_package", id: packageId, values: {  custrecord_clgx_package_over_grace_prd : true, custrecord_clgx_package_date_charge : chargeStartDate} });
           	} catch(ex) {
           		log.error({ title: "startChargeDays- Error", details: ex });
           	}
   	}   
    
    function rescheduleScript(param){
    	
    	if(param){
	        var scheduledScriptTask = task.create({
	            taskType: task.TaskType.SCHEDULED_SCRIPT,
	            params: {custscript_clgx_1894_at : param}
	        });
    	} else {
	        var scheduledScriptTask = task.create({
	            taskType: task.TaskType.SCHEDULED_SCRIPT
	        });
    	}
        scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
        scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
        return scheduledScriptTask.submit();
    }
    

    
    return {
        execute: execute
    };
    
});
