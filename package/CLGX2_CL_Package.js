/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(["N/ui/dialog"],
function(dialog) {
	
	function pageInit(scriptContext) {
		try {
			if(scriptContext.mode == "edit") {
				var parameters = getURLParameters();
				if(parameters.stage == "received") {
					scriptContext.currentRecord.setValue({ fieldId: "custrecord_clgx_pkg_delivery_status", value: "2" });
					scriptContext.currentRecord.setValue({ fieldId: "custrecord_clgx_pkg_delivery_date", value: new Date() });
				}
			}
		} catch(ex) {
			log.error({ title: "pageInit - Error", details: ex });
		}
	}
	
	
	/**
     * Validation function to be executed when a field is changed.
     *
     * @param {Object} scriptContext
     *
     * @since 2015.2
     */
	function fieldChanged(scriptContext) {
		if(scriptContext.fieldId == "custrecord_clgx_pkg_delivery_status") {
			var deliveryStatus = scriptContext.currentRecord.getValue("custrecord_clgx_pkg_delivery_status");
			
			if(deliveryStatus == 3) { //Claimed
				scriptContext.currentRecord.setValue({ fieldId: "custrecord_clgx_pkg_release_date", value: new Date() });
			}
		}
	}
	
	
    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {
    	var returnValue   = true;
    	var currentRecord = scriptContext.currentRecord;
    	
    	if(!isReleaseBeforeDelivery(currentRecord)) {
    		returnValue = false;
    	}
    	
    	if(!isClaimDateEmpty(currentRecord)) {
    		returnValue = false;
    	}
    	
    	if(!isStatusClaimed(currentRecord)) {
    		returnValue = false;
    	}
    	
    	return returnValue;
    }
    
    
    /**
     * Returns whether or not the release date is before the delivery date.
     * 
     * @access private
     * @function isReleaseBeforeDelivery
     * 
     * @param {Object} currentRecord
     * @returns {Boolean}
     */
    function isReleaseBeforeDelivery(currentRecord) {
    	var deliveryDate = new Date(currentRecord.getValue("custrecord_clgx_pkg_delivery_date")).getTime();
    	var releaseDate  = new Date(currentRecord.getValue("custrecord_clgx_pkg_release_date")).getTime();
    	
    	if((deliveryDate && releaseDate)) {
    		if(releaseDate < deliveryDate) {
    			dialog.alert({
    				title: "Error",
    				message: "Claim date must be after the delivery date."
    			});
    			
    			return false;
    		}
    	}
    	
    	return true;
    }

    
    /**
     * Returns whether or not the claim date is empty if the status is Claimed.
     * 
     * @access private
     * @function isClaimDateEmpty
     * 
     * @param {Object} currentRecord
     * @returns {Boolean}
     */
    function isClaimDateEmpty(currentRecord) {
    	var deliveryStatus = parseInt(currentRecord.getValue("custrecord_clgx_pkg_delivery_status"));
    	var releaseDate    = currentRecord.getValue("custrecord_clgx_pkg_release_date");
    	
    	if(!releaseDate && (deliveryStatus == 3)) {
    		dialog.alert({
				title: "Error",
				message: "Claim date must be populated."
			});
    		
    		return false;
    	}
    	
    	return true;
    }
    
    /**
     * Returns whether or not the package status is set to claimed if the claim date is populated.
     * 
     * @access private
     * @function isStatusClaimed
     * 
     * @returns {Boolean}
     */
    function isStatusClaimed(currentRecord) {
    	var deliveryStatus = parseInt(currentRecord.getValue("custrecord_clgx_pkg_delivery_status"));
    	var releaseDate    = currentRecord.getValue("custrecord_clgx_pkg_release_date");
    	
    	if(releaseDate && (deliveryStatus != 3)) {
    		dialog.alert({
				title: "Error",
				message: "<b>Package Status</b> must be set to Claimed if a <b>Claim Date</b> is populated."
			});
    		
    		return false;
    	}
    	
    	return true;
    }
    
    /**
	 * Returns a key/value pair of URL parameters.
	 * 
	 * @access private
	 * @function getURLParameters
	 * 
	 * @return {Object}
	 */
	function getURLParameters() {
		var parameters = new Object();
		
		window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
			parameters[key] = value;
		});
		
		return parameters;
	}
    
    return {
    	pageInit: pageInit,
    	fieldChanged: fieldChanged,
        saveRecord: saveRecord
    };
    
});
