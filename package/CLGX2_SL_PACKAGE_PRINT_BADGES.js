/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/render','N/http', 'N/record'],
/**
 * @param {render} render
 */
function(render, serverResponse, record) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	try{
			if(context.request.method == "GET") {	
				
				var packages = context.request.parameters.custom_id;
				var packageArray = packages.split(',');				
				
				var allContent = '';
				for(var j = 0; j<packageArray.length; j++){
					var renderer = render.create();
					renderer.setTemplateByScriptId('CUSTTMPL_CLGX_PACKAGE_INT_TRACK');
					renderer.addRecord(
						    'record', record.load({
							type: 'customrecord_clgx_package',
							id: packageArray[j]
						})
					);
					var stringContent = renderer.renderAsString();
					if(j == 0 && packageArray.length>1){
						stringContent = stringContent.replace('</pdf>', '');
					}
					if(j != 0 && j<packageArray.length && packageArray.length>1){
						stringContent = stringContent.replace('<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">', '');
						stringContent = stringContent.replace('<pdf>', '');
						if(j !== packageArray.length-1){
							stringContent = stringContent.replace('</pdf>', '');
						}
						
					}
					allContent = allContent + stringContent;
				}
				
//				allContent = allContent + '</body></pdf>'
				
				log.debug({title: 'XML Info', details: allContent});
				
//				var file = render.xmlToPdf({ xmlString: allContent });
				
					
				context.response.writeFile(render.xmlToPdf({ xmlString: allContent }));
			}
    	} catch(ex){
    		context.response.write(JSON.stringify(ex));
    	}
    }

    return {
        onRequest: onRequest
    };
    
});
