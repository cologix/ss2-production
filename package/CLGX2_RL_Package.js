/**
 * @NApiVersion 2.0
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
define(["/SuiteScripts/clgx/package/lib/CLGX2_LIB_PKG_Session", "N/search", "/SuiteScripts/clgx/package/lib/CLGX2_LIB_Package", "N/record", "N/format", "/SuiteScripts/clgx/libraries/lodash.min", "N/runtime"],
function(session, search, lib, record, format, _, runtime) {
    /**
     * Function called upon sending a PUT request to the RESTlet.
     *
     * @param {string | Object} requestBody - The HTTP request body; request body will be passed into function as a string when request Content-Type is 'text/plain'
     * or parsed into an Object when request Content-Type is 'application/json' (in which case the body must be a valid JSON)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.2
     */
    var doPut = session.authenticate(function doPut(data, returnObj, srights, companyID, contactID, modifierID, radix, sid) {
    	var responseObject = new Object();
    	
    	responseObject = lib.initializeNotifications({ responseObject: responseObject, companyID: companyID });
    	
    	if(srights.shipments > 0) {
    		if(data.event == "create") {
        		var packageArray              = new Array();
        		var trackingNumberArray       = data.tracking_number.split(",");
        		var trackingNumberArrayLength = trackingNumberArray.length;
        		
        		for(var tn = 0; tn < trackingNumberArrayLength; tn++) {
        			if(trackingNumberArray[tn]) {
        				var expectedDate = data.estimated_delivery_date;
        	    		if(expectedDate) { expectedDate = new Date(data.estimated_delivery_date); }
        	    		
        	    		var recipient = data.recipient;
        	    		if(recipient == 0) { recipient = ""; }
        	    		
        	    		var packageID = lib.createPackageRecord({
        	    			custrecord_clgx_pkg_tracking_num: trackingNumberArray[tn],
        	    			custrecord_clgx_pkg_delivery_status: 1, //Pending delivery
        	    			custrecord_clgx_pkg_package_type: data.package_type,
        	    			custrecord_clgx_pkg_storage_facility: data.facility,
        	    			custrecord_clgx_pkg_shipping_carrier: data.shipping_method,
        	    			custrecord_clgx_pkg_receiving_company: companyID,
        	    			custrecord_clgx_pkg_customer_notes: data.sender_notes,
        	    			custrecord_clgx_pkg_company_contact: recipient,
        	    			custrecord_clgx_pkg_attention: data.recipient_other,
        	    			custrecord_clgx_pkg_created_by: contactID,
        	    			custrecord_clgx_pkg_estimated_delivery: expectedDate,
        	    			custrecord_clgx_pkg_sending_vendor: data.sending_vendor
        	    		});
        	    		
        	    		packageArray.push(packageID);
        			}
        		}
        		
        		log.debug({ title: "packageArray", details: packageArray });
        		log.debug({ title: "packageObject", details: [{
	    			custrecord_clgx_pkg_tracking_num: trackingNumberArray[tn],
	    			custrecord_clgx_pkg_delivery_status: 1, //Pending delivery
	    			custrecord_clgx_pkg_package_type: data.package_type,
	    			custrecord_clgx_pkg_storage_facility: data.facility,
	    			custrecord_clgx_pkg_shipping_carrier: data.shipping_method,
	    			custrecord_clgx_pkg_receiving_company: companyID,
	    			custrecord_clgx_pkg_customer_notes: data.sender_notes,
	    			custrecord_clgx_pkg_company_contact: recipient,
	    			custrecord_clgx_pkg_attention: data.recipient_other,
	    			custrecord_clgx_pkg_created_by: contactID,
	    			custrecord_clgx_pkg_estimated_delivery: expectedDate,
	    			custrecord_clgx_pkg_sending_vendor: data.sending_vendor
	    		}] });
        		
        		//Add case to the general queue
        		if(data.remote_hands == "on") {
        			var caseObject = new Array();
        			
        			var facilityName = search.lookupFields({ type: "customrecord_cologix_facility", id: data.facility, columns: "name" });
        			var contactEmail = search.lookupFields({ type: "contact", id: contactID, columns: "email" });
        			var caseSubject  = (facilityName.name + "-" + packageID + " Remote Hands");
            		
        			/*var subCaseType = "";
        			
        			if (data.case_type == 1) {
        				subCaseType = 5;
                    } else if (data.case_type == 2) {
                    	subCaseType = 6;
                    }*/
        			
        			caseObject = [{
        				"title": caseSubject,
        				"company": companyID,
        				"contact": contactID,
        				"email": contactEmail.email,
        				"custevent_cologix_facility": data.facility,
        				"case_type": 2,
        				"category": "1", //Remote Hands
        				"custevent_cologix_sub_case_type": 6,
        				"assigned": 379486,
        				"priority": data.case_priority,
        				"custevent_cologix_case_sched_followup": data.requested_date,
        				"custevent_cologix_sched_start_time": data.requested_time,
        				"message": data.case_notes,
        				"origin": "-5", //Web
        			}];
        			
        			//Creates a queue record to be processed later
        			var queueID = lib.createRecord({
        				type: "customrecord_clgx_global_queue", 
        				fields: {
        					custrecord_clgx_gq_uid: runtime.getCurrentUser().id,
        					custrecord_clgx_gq_cid: companyID,
        					custrecord_clgx_gq_rt: "supportcase",
        					custrecord_clgx_gq_processed: false,
        					custrecord_clgx_gq_json: JSON.stringify(caseObject),
        					custrecord_clgx_gq_pid: packageArray.join(";")
        				}
        			});
        			
        			//Downloads an attachment from customer portal.
        			var attachmentID = 0;
        			if(parseInt(data.hasattachment) == 1) {	    				
        				try {
        					attachmentID = lib.file.downloadCaseAttachment({ 
    	    					name: data.filename, 
    	    					queueID: queueID, 
    	    					contactID: contactID, 
    	    					companyID: companyID,
    	    					contents: data.file
    	    				});
        					
        					lib.updateRecord({ 
        	    				id: queueID, 
        	    				type: "customrecord_clgx_global_queue",
        	    				fields: { 
        	    					custrecord_clgx_gq_fileid: (attachmentID != 0 ? attachmentID : "")
        	    				}  
        	    			});
        					
        					responseObject["fileDownloaded"] = 1;
        				} catch(ex) {
        					responseObject["fileDownloaded"] = 0;
        					log.error({ title: "lib.file.downloadCaseAttachment - Error", details: ex });
        				}
        			}
        			
        			
        			//Update package record with the queue id
        			var packageArrayLength = packageArray.length;
        			for(var packageIndex = 0; packageIndex < packageArrayLength; packageIndex++) {
        				record.submitFields({ type: "customrecord_clgx_package", id: packageArray[packageIndex], values: { custrecord_clgx_pkg_qid: queueID, custrecord_clgx_pkg_queue_status: "Processing" } });
        			}
        			
        			
        			lib.general.scheduleScript({
        				scriptId: "1912",
        				params: {
        					custscript_clgx2_1947_qid: queueID
        				}
        			});
        		}
        		
        		responseObject["code"]  = "SUCCESS";
        		responseObject["msg"]   = "You are managing shipments.";
        		responseObject["error"] = "F";
        	} else if(data.event == "update") {
        		var expectedDate = data.estimated_delivery_date;
        		if(expectedDate) { expectedDate = new Date(data.estimated_delivery_date); }
        		
        		var recipient = data.recipient;
        		if(recipient == 0) { recipient = ""; }
        		
        		lib.updatePackageRecord({ 
        			id: data.id, 
        			fields: {
        				custrecord_clgx_pkg_storage_facility: data.facility,
        				custrecord_clgx_pkg_package_type: data.package_type,
        				custrecord_clgx_pkg_shipping_carrier: data.shipping_method,
        				custrecord_clgx_pkg_estimated_delivery: expectedDate,
        				custrecord_clgx_pkg_company_contact: recipient,
        				custrecord_clgx_pkg_attention: data.recipient_other,
        				custrecord_clgx_pkg_tracking_num: data.tracking_number,
        				custrecord_clgx_pkg_customer_notes: data.sender_notes,
        				custrecord_clgx_pkg_storage_notes: data.storage_notes,
        				custrecord_clgx_pkg_release_notes: data.claim_notes,
        				custrecord_clgx_pkg_sending_vendor: data.sending_vendor
        			} 
        		});
        		
        		responseObject["code"]  = "SUCCESS";
        		responseObject["msg"]   = "You are managing shipments.";
        		responseObject["error"] = "F";
        	} else if(data.event == "list") {
        		var filter  = [];
        		if(data.filter == "closed") {
        			filter = [3, 4]; //Cancelled and Claimed
        		} else if(data.filter == "opened") {
        			filter = [1, 2]; //Received/Stored and Pending Delivery
        		} else {
        			filter = null;
        		}
        		
        		responseObject = lib.initializePackageResponse({ responseObject: responseObject, companyID: companyID, listFilter: filter });
        		
        		var records = lib.listPackageRecords({ companyID: companyID, filter: filter });
        		
        		responseObject["shipments"]           = new Object();
        		responseObject["shipments"]["data"]   = records.data;
        		responseObject["shipments"]["page"]   = 1;
        		responseObject["shipments"]["pages"]  = 0;
        		responseObject["shipments"]["total"]  = records.count;
        		responseObject["shipments"]["start"]  = 0;
        		responseObject["shipments"]["end"]    = 0;
        		
        		responseObject["code"]  = "SUCCESS";
        		responseObject["msg"]   = "You are managing shipments.";
        		responseObject["error"] = "F";
        	} else if(data.event == "detail") {
        		responseObject = lib.initializePackageResponse({ responseObject: responseObject, companyID: companyID });
        		
        		var packageRecord = lib.getPackageRecordDetail({ id: data.id });
        		packageRecord.caseobject = lib.getPackageCaseObject({ package: packageRecord, data: data });
        		
        		responseObject["shipment"]          = new Object();
        		responseObject["shipment"]["data"]  = packageRecord;
        		
        		responseObject["code"]  = "SUCCESS";
        		responseObject["msg"]   = "You are managing shipment:" + data.id;
        		responseObject["error"] = "F";
        	} else if(data.event == "cancel") {
        		lib.updatePackageRecord({ id: data.id, fields: { custrecord_clgx_pkg_delivery_status: 4 }  });
        		
        		responseObject["code"]  = "SUCCESS";
        		responseObject["msg"]   = "You disabled record:" + data.id;
        		responseObject["error"] = "F";
        	} else if(data.event == "initialize") {
        		responseObject = lib.initializePackageResponse({ responseObject: responseObject, companyID: companyID });
        		
        		responseObject["code"]  = "SUCCESS";
        		responseObject["msg"]   = "Initialized environment variables";
        		responseObject["error"] = "F";
        	}
    	} else {
    		responseObject["code"]  = "NO_RIGHTS_SHIPMENTS";
    		responseObject["msg"]   = "No rights for shipments.";
    		responseObject["error"] = "T";
    	}
    	
    	return responseObject;
    });


    /**
     * Function called upon sending a POST request to the RESTlet.
     *
     * @param {string | Object} requestBody - The HTTP request body; request body will be passed into function as a string when request Content-Type is 'text/plain'
     * or parsed into an Object when request Content-Type is 'application/json' (in which case the body must be a valid JSON)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.2
     */
    var doPost = session.authenticate(function doPost(data, returnObj, srights, companyID, contactID, modifierID, radix, sid) {
    	try {
    		//create shipment record
    		var responseObject = new Object();
    		
    		responseObject["code"]                = "SUCCESS";
    		responseObject["msg"]                 = "You are managing shipments.";
    		responseObject["error"]               = "F";
    	} catch(ex) {
    		log.error({ title: "CLGX2_RL_Package", details: ex });
    		throw ex;
    	}
    	
    	return responseObject;
    });

    return {
        put: doPut,
        post: doPost
    };
    
});
