/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/redirect', 'N/ui/serverWidget', 'N/search','/SuiteScripts/clgx/libraries/moment.min', 'N/runtime', '/SuiteScripts/clgx/libraries/lodash.min'],
/**
 * @param {record} record
 * @param {redirect} redirect
 * @param {serverWidget} serverWidget
 */
function(record, redirect, serverWidget, search, moment, runtime, _) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
		try{
			if(context.request.method == "GET") {
        		var form = serverWidget.createForm({ title: "Enter Tracking Numbers" });
        		form.clientScriptModulePath = 'SuiteScripts/clgx/package/CLGX2_CS_PACKAGE_MULTI_ADD';
        		
    		   	var request = context.request;
        		var trackingNum = request.parameters.tn;
        		
        		//build form
        		
        		var customerList = buildCustomerList();
        		
        		var company = form.addField({
        			id: 'custpage_clgx2_package_company',
        			type: serverWidget.FieldType.SELECT,
        			label: 'Customer'
        		});
        		
        		_.forEach(customerList, function(customer) {
        			company.addSelectOption({ value: customer.id, text: customer.name });
        		});
        		
        		var storageFacility = form.addField({
        			id: 'custpage_clgx2_package_storage_facility',
        			type: serverWidget.FieldType.SELECT,
        			source: 'customrecord_cologix_facility',
        			label: 'Storage Facility'
        		});
        		var internalNotes = form.addField({
        			id: 'custpage_clgx2_package_internal_notes',
        			type: serverWidget.FieldType.TEXTAREA,
        			label: 'Internal Notes'
        		});
        		var carrier = form.addField({
        			id: 'custpage_clgx2_package_carrier',
        			type: serverWidget.FieldType.SELECT,
        			source: 'customlist_clgx_pkg_shipment_carrier', 
        			label: 'Carrier'
        		});
        		
        		var attn = form.addField({
        			id: 'custpage_clgx2_package_attention',
        			type: serverWidget.FieldType.TEXT,
        			label: 'Package Attention'
        		});
        		
        		var storageType = form.addField({ 
        			id: "custpage_clgx_storage_type", 
        			type: serverWidget.FieldType.SELECT, 
        			source: "customlist_clgx_pckg_prepaid_storage", 
        			label: "Storage Type" 
        		});
        		
        		
            	company.isMandatory = true;
            	storageFacility.isMandatory = true;
            	carrier.isMandatory = true;
            	attn.isMandatory = true;
            	storageType.isMandatory = true;
        		
            	//build sublist
        		
            	var trackingNumbers = form.addSublist({ 
            		id: "custpage_clgx2_track_numbers", 
            		type: serverWidget.SublistType.INLINEEDITOR, 
            		label: "Tracking Numbers"
            	});
            	
            	
            	var packageTypeList = buildPackageTypeList();
            	
            	var packageType = trackingNumbers.addField({ 
            		id: "custpage_clgx2_package_type", 
            		type: serverWidget.FieldType.SELECT,
            		label: "Package Type"
            	});
            	
            	_.forEach(packageTypeList, function(type) {
            		packageType.addSelectOption({ value: type.id, text: type.text });
        		});
            	
            	var trackingNumber = trackingNumbers.addField({ 
            		id: "custpage_clgx2_track_number", 
            		type: serverWidget.FieldType.TEXT, 
            		label: "Tracking Number"
            	});	
            	
            	trackingNumber.isMandatory = true;
            	packageType.isMandatory = true;
            	
            	//populate initial sublist value with url parameter tracking number
        		
            	trackingNumbers.setSublistValue({
            		id: 'custpage_clgx2_track_number',
            		line: 0,
            		value: trackingNum
            	});
            	
            	//select the first line by default
            	
        		form.addSubmitButton({label:'Receive'});
        		context.response.writePage(form);
			} else {	
				var request = context.request;
		    	var today = moment().format('M/D/YYYY');
				
				var custId = request.parameters.custpage_clgx2_package_company;
				var facilityId = request.parameters.custpage_clgx2_package_storage_facility;
				var internalNotesContent = request.parameters.custpage_clgx2_package_internal_notes;
				var packageCarrier = request.parameters.custpage_clgx2_package_carrier;
				var attention = request.parameters.custpage_clgx2_package_attention;
				var storage = request.parameters.custpage_clgx_storage_type;
				var lineCount = request.getLineCount({ group: 'custpage_clgx2_track_numbers'});
				
				var trackingFilter = new Array();
				var redirectMessage = new String();
				
				for(var i = 0; i<lineCount; i++){
					var trackingNumValue = request.getSublistValue({
						group: 'custpage_clgx2_track_numbers',
						name: 'custpage_clgx2_track_number',
						line: i
					});
					
					trackingFilter.push(trackingNumValue);
					
					var existingPackage = searchForPackage(trackingNumValue);
					
					if(existingPackage){
						//if there is an existing package with tracking number in the system, then load that package record and update it
						var objRecord = record.load({
						    type: 'customrecord_clgx_package', 
						    id: existingPackage,
						    isDynamic: true
						});
						objRecord.setValue('custrecord_clgx_pckg_prepaid_storage', storage);
						objRecord.setValue('custrecord_clgx_pkg_delivery_status', 2);
						objRecord.save();
					} else {
						//if no existing package is found, create the package record and set the default fields
						var packageTypeValue = request.getSublistValue({
							group: 'custpage_clgx2_track_numbers',
							name: 'custpage_clgx2_package_type',
							line: i
						});
						
						var newPackage = record.create({
							type: 'customrecord_clgx_package',
							isDynamic: true
						});
						
//						log.debug(today);
						
						newPackage.setValue('custrecord_clgx_pkg_receiving_company', custId);
						newPackage.setValue('custrecord_clgx_pkg_storage_facility', facilityId);
						newPackage.setValue('custrecord_clgx_pkg_storage_notes', internalNotesContent);
						newPackage.setValue('custrecord_clgx_pkg_shipping_carrier', packageCarrier);
						newPackage.setValue('custrecord_clgx_pkg_tracking_num', trackingNumValue);
						newPackage.setValue('custrecord_clgx_pkg_package_type', packageTypeValue);
						newPackage.setValue('custrecord_clgx_pkg_attention', attention);
						newPackage.setValue('custrecord_clgx_pckg_prepaid_storage', storage);
//						newPackage.setValue('custrecord_clgx_pkg_delivery_date', today);
						
						var success = newPackage.save();
						
						if(success){
							var confirm = 'Success';
							var trackingParam = trackingNumValue;
							var message = confirm + ':' + trackingParam + ',';
							redirectMessage = redirectMessage + message;
						} else {
							var confirm = 'Failure';
							var trackingParam = trackingNumValue;
							var message = confirm + ':' + trackingParam + ',';
							redirectMessage = redirectMessage + message;
						}					
						
//						log.debug(newPackage.id);
					}
				}
				
				redirect.redirect({
					url: '/app/site/hosting/scriptlet.nl?script=1884&deploy=1',
					parameters: {custom_packfilt : facilityId, custom_packmessage: redirectMessage}
				})
				
				
			}
		} catch(ex){
    		context.response.write(JSON.stringify(ex));
    	}
    }
    
    
    function buildPackageTypeList() {
    	try {
    		var packageTypeArray = new Array();
    		
    		var columns = new Array();
    		columns.push(search.createColumn({ name: "internalid", sort: search.Sort.ASC }));
    		columns.push(search.createColumn("name"));
    		
    		var filters = new Array();
    		filters.push(search.createFilter({ name: "isinactive", operator: "is", values: ["F"] }));
    		
    		var searchObject = search.create({ type: "customlist_clgx_pkg_package_type", columns: columns, filters: filters });
    		searchObject.run().each(function(result) {
    			packageTypeArray.push({ id: parseInt(result.getValue("internalid")), text: result.getValue("name") });
    			return true;
    		});
    		
    		return packageTypeArray;
    	} catch(ex) {
    		log.error({ title: "buildPackageTypeList - Error", details: ex });
    	}
    }
    
    /**
     * Returns an array of active customers in a key/value pair.
     * 
     * @access private
     * @function buildCustomerList
     * 
     * @returns {Array}
     */
    function buildCustomerList() {
    	var excludedCustomers = [487967, 502186, 963445, 2763, 2789, 2790, 2674650];
    	
		var customerArray = new Array();
		customerArray.push({ id: "", name: "" });
		
    	var columns = new Array();
    	columns.push(search.createColumn("internalid"));
    	columns.push(search.createColumn({ name: "companyname", sort: search.Sort.ASC }));
    	
    	
    	var filters = new Array();
    	filters.push(search.createFilter({ name: "isjob",        operator: "is",             values: ["F"]      }));
    	filters.push(search.createFilter({ name: "companyname",  operator: "doesnotcontain", values: ["parent"] }));
    	filters.push(search.createFilter({ name: "entitystatus", operator: "is",             values: ["13"]     }));
    	
    	var searchObject     = search.create({ type: "customer", columns: columns, filters: filters });
    	var searchResults    = searchObject.runPaged({ pageSize: 1000 });
    	var searchPageLength = searchResults.pageRanges.length
    	
    	for(var page = 0; page < searchPageLength; page++) {
    		var pageObject = searchResults.fetch(page);
    		pageObject.data.forEach(function(result) {
    			if(excludedCustomers.indexOf(parseInt(result.getValue("internalid"))) == -1) {
    				customerArray.push({ id: result.getValue("internalid"), name: result.getValue("companyname") });
    			}
    		});
    	}
    	
    	return customerArray;
    }
    
    function searchForPackage(trackingSearch){
   		
    	var searchFilter = search.createFilter({
    		name: 'custrecord_clgx_pkg_tracking_num',
    		operator: search.Operator.IS,
    		values: trackingSearch    		
    	});
    	
    	var packageRecord = '';
		var searchObject = search.create({ type: "customrecord_clgx_package", columns: ['custrecord_clgx_pkg_tracking_num', 'internalid'], filters: searchFilter});
		searchObject.run().each(function(result) {
			packageRecord = result.getValue('internalid');
			return true;
		});
		
		return packageRecord;
    	
    }

    return {
        onRequest: onRequest
    };
    
});
