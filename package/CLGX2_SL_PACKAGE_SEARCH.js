/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/redirect', 'N/search', 'N/ui/serverWidget', '/SuiteScripts/clgx/libraries/moment.min', 'N/ui/message', '/SuiteScripts/clgx/libraries/lodash.min'],
/**
 * @param {redirect} redirect
 * @param {search} search
 * @param {serverWidget} serverWidget
 */
function(redirect, search, serverWidget, moment, message, _) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	try{
    		if(context.request.method == "GET") {
    			
    			var filterParam = context.request.parameters.custom_packfilt;
    			var messageParam = context.request.parameters.custom_packmessage;

        		var form = serverWidget.createForm({ title: "Receive Packages" });
        		
        		if(messageParam){
	    			var messageElements = messageParam.split(',');
	    			var failureTn = '';
	    			var successTn = '';
	
	    			log.debug({title: 'Error Message', details: messageParam});
	    			log.debug({title: 'Message Elements', details: messageElements});
	    			for(var i = 0; i<messageElements.length-1; i++){
	    				var messageObj = messageElements[i].split(':');
	    				if(messageObj[0]==='Success'){
	    					successTn = successTn + '\n'+ messageObj[1];
	    				} else if(messageObj[0]==='Failure'){
	    					failureTn = failureTn + '\n'+ messageObj[1];
	    				}
	    				
	    			}
	    			
	    			log.debug({title: 'Failures', details: failureTn});
	    			log.debug({title: 'Successes', details: successTn});
	    			
	    			if(failureTn){
	    				var failureMessage = 'The following packages were not received successfully. Please contact your administrator ' + failureTn;
	    				var messageObject = message.create({type: message.Type.ERROR, message: failureMessage, duration: 5000});
	            		form.addPageInitMessage({message: messageObject});
	    			}
	    			if(successTn){
	    				var successMessage = 'The following packages were received successfully:' + successTn;
	    				var messageObject = message.create({type: message.Type.CONFIRMATION, message: successMessage, duration: 5000});
	            		form.addPageInitMessage({message: messageObject});
	    			}
    			}
        		
//        		var messageObject = message.create({type: message.Type.INFORMATION, message: messageParam, duration: 5000});
//        		form.addPageInitMessage({message: messageObject});        		
        		
        		var searchgroup = form.addFieldGroup({
        			id: 'searchgroup',
        			label: 'Enter a tracking number to search below'
        		}); 
        		
        		var trackingSearch = form.addField({
        			id: 'custpage_clgx2_tracking_search',
        			type: serverWidget.FieldType.TEXT,
        			label: 'Search Tracking',
        			container: 'searchgroup'
        		});
        		
        		form.addSubmitButton({label:'Search'});	
        		
        		var printGroup = form.addFieldGroup({
        			id: 'printgroup',
        			label: 'Print Labels'
        		}); 
        		
        		var printFilter = form.addField({
        			id: 'custpage_clgx2_facility_filter_print',
        			type: serverWidget.FieldType.SELECT, 
        			label: 'Facility Filter',
        			source: 'customrecord_cologix_facility',
        			container: 'printgroup'
        		});
        		
        		if(filterParam){
        			printFilter.defaultValue = filterParam;
        		}
        		        		
        		var packages = searchTodayPackages(filterParam);
        		var packageNum = packages.length;
        		
        		var recentlyReceived = form.addSublist({
    				id: 'custpage_clgx2_package_received_today',
    			    type : serverWidget.SublistType.LIST,
    			    label : 'Packages Received Today',
    			    container: 'printgroup'
        		});     		
        		
        		//build sublist of packages received today so that users can print labels
    			var selectBox = recentlyReceived.addField({
            		id: 'custpage_clgx2_pckg_print', 
            		type: serverWidget.FieldType.CHECKBOX,
            		label: 'Print'
    			});
    			
    			var packageInternalId = recentlyReceived.addField({
            		id: 'custpage_clgx2_pckg_rt_id', 
            		type: serverWidget.FieldType.TEXT,
            		label: 'Package ID'
    			});
    			
    			var trackingNum = recentlyReceived.addField({
            		id: 'custpage_clgx2_pckg_rt_tn', 
            		type: serverWidget.FieldType.TEXT,
            		label: 'Tracking Number'
    			});
    			
    			var internalTracking = recentlyReceived.addField({
            		id: 'custpage_clgx2_pckg_rt_it', 
            		type: serverWidget.FieldType.TEXT,
            		label: 'Internal Number'
    			});
    			
    			var status = recentlyReceived.addField({
            		id: 'custpage_clgx2_pckg_rt_st', 
            		type: serverWidget.FieldType.SELECT,
            		source: 'customlist_clgx_delivery_status_list',
            		label: 'Status'
    			});
    			
    			var storageNotes = recentlyReceived.addField({
            		id: 'custpage_clgx2_pckg_rt_sn', 
            		type: serverWidget.FieldType.TEXT,
            		label: 'Interal Notes'
    			});
    			
    			recentlyReceived.addMarkAllButtons();
    			
        		form.clientScriptModulePath = 'SuiteScripts/clgx/package/CLGX2_CS_PACKAGE_SEARCH';
    			recentlyReceived.addButton({
    				id: 'custpage_print_labels',
    				label: 'Print',
    				functionName: 'printBadges'
    			});
    			    			
    			status.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
    			
    			//populate the sublist with items from the packages array
    			for(var i=0; i<packageNum; i++){
    				recentlyReceived.setSublistValue({id: 'custpage_clgx2_pckg_rt_id', line: i, value: packages[i].packageId });
                	if(packages[i].internaltracking){
                		recentlyReceived.setSublistValue({id: 'custpage_clgx2_pckg_rt_it', line: i, value: packages[i].internaltracking });
                	}
                	recentlyReceived.setSublistValue({id: 'custpage_clgx2_pckg_rt_tn', line: i, value: packages[i].tracking });
                	recentlyReceived.setSublistValue({id: 'custpage_clgx2_pckg_rt_st', line: i, value: packages[i].status });
                	if(packages[i].internalnotes){
                		recentlyReceived.setSublistValue({id: 'custpage_clgx2_pckg_rt_sn', line: i, value: packages[i].internalnotes });
                	}
    			}
        		
        		context.response.writePage(form);
    		} else {
    			
    			var parameters         = context.request.parameters;
        		var trackingNumber     = parameters.custpage_clgx2_tracking_search;
        		var packageRecord      = searchForPackage(trackingNumber);
    			
        		if(packageRecord) {
        			redirect.toRecord({
        				id: packageRecord,
        				type: "customrecord_clgx_package",
        				isEditMode: true,
        				parameters: {tn : trackingNumber, stage : 'received', cf : 213}
        			});
        		} else {
        			redirect.redirect({
        				url: "/app/site/hosting/scriptlet.nl?script=1881&deploy=1",
        				parameters: {tn : trackingNumber}
        			});
        		}
    		}
    	} catch(ex){
    		context.response.write(JSON.stringify(ex));
    	}
    }
    
    function searchForPackage(trackingSearch){
   		
    	var searchFilter = search.createFilter({
    		name: 'custrecord_clgx_pkg_tracking_num',
    		operator: search.Operator.IS,
    		values: trackingSearch    		
    	});
    	
    	var packageRecord = '';
		var searchObject = search.create({ type: "customrecord_clgx_package", columns: ['custrecord_clgx_pkg_tracking_num', 'internalid'], filters: searchFilter});
		searchObject.run().each(function(result) {
			log.debug(trackingSearch);
			log.debug(result.getValue('internalid'));
			packageRecord = result.getValue('internalid');
			return true;
		});
		
		return packageRecord;
    	
    }
    
	function searchTodayPackages(filterParam){
    	
    	var packageArray = new Array();
		var searchObject = search.create({ 
			type: 'customrecord_clgx_package', 
//			search.createColumn({name: 'lastmodified', sort: search.sort.DESC}),
			columns: ['custrecord_clgx_pkg_tracking_num', 'internalid', 'custrecord_clgx_pkg_internal_track', 'custrecord_clgx_pkg_delivery_status', 'custrecord_clgx_pkg_storage_notes'], 
			filters: [ search.createFilter({
			        		name: 'custrecord_clgx_pkg_delivery_date',
			        		operator: search.Operator.ON,
			        		values: moment().format('M/D/YYYY')
		    			}), 
		    			search.createFilter({
			        		name: 'custrecord_clgx_pkg_delivery_status',
			        		operator: search.Operator.IS,
			        		values:  2
		    			})		    			
    	]});
		if(filterParam){
			var filterObject = search.createFilter({
        		name: 'custrecord_clgx_pkg_storage_facility',
        		operator: search.Operator.IS,
        		values:  filterParam
			});
			searchObject.filters.push(filterObject);
		}		
		searchObject.run().each(function(result) {
			var packageObject ={
					packageId : result.getValue('internalid'),
					internaltracking : result.getValue('custrecord_clgx_pkg_internal_track'),
					tracking: result.getValue('custrecord_clgx_pkg_tracking_num'),
					status: result.getValue('custrecord_clgx_pkg_delivery_status'),
					internalnotes: result.getValue('custrecord_clgx_pkg_storage_notes')
			}
			
			packageArray.push(packageObject);

			return true;
		});
		
		return packageArray;
		
	}

    return {
        onRequest: onRequest
    };
    
});
