/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/ui/dialog', 'N/currentRecord', 'N/log', 'N/search'],
/**
 * @param {dialog} dialog
 * @param {currentRecord} currentRecord
 * @param {log} log
 * @param {search} search
 */
function(dialog, currentRecord, log, search) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(context) {
    	var rec = context.currentRecord;
    	var firstLine = rec.selectLine({
    		sublistId: 'custpage_clgx2_track_numbers',
    		line: 0
    	});
    }

    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(context) {
	    	var packageEntry = context.currentRecord;
	    	var packageListLength = packageEntry.getLineCount({ sublistId: 'custpage_clgx2_track_numbers' });
	    	
	    	for(var i = 0; i<packageListLength; i++){
	    		var packageType = packageEntry.getSublistValue({
	    			sublistId: 'custpage_clgx2_track_numbers',
	    			fieldId: 'custpage_clgx2_package_type',
	    			line: i
	    		});
	    		var trackingNum = packageEntry.getSublistValue({
	    			sublistId: 'custpage_clgx2_track_numbers',
	    			fieldId: 'custpage_clgx2_track_number',
	    			line: i
	    		});

	    		if(!packageType){
	    			dialog.alert({
	    				title: 'Missing Package Type',
	    				message: 'Please select a package type for all lines.'
	    			});
	    			return false;
	    		}
	    		else if(!trackingNum){
	    			dialog.alert({
	    				title: 'Missing tracking number',
	    				message: 'Please input the tracking number for this package in the sublist below.'
	    			});
	    			return false;
	    		} else {
	    	    	var searchFilter = search.createFilter({
	    	    		name: 'custrecord_clgx_pkg_tracking_num',
	    	    		operator: search.Operator.IS,
	    	    		values: trackingNum    		
	    	    	});
	    	    	
	    	    	var packageRecord = '';
	    			var searchObject = search.create({ type: "customrecord_clgx_package", columns: ['custrecord_clgx_pkg_tracking_num', 'internalid', 'custrecord_clgx_pkg_delivery_status'], filters: searchFilter});
	    			searchObject.run().each(function(result) {
	    				packageRecord = {
	    						id: 	result.getValue('internalid'),
	    						status:	result.getValue('custrecord_clgx_pkg_delivery_status'),
	    						statustxt: result.getText('custrecord_clgx_pkg_delivery_status')
	    				}
	    				return true;
	    			});
	    			
	    			if(packageRecord.status == 3 || packageRecord.status == 2){
	    				var message = trackingNum + ' has already been received. It cannot be received again. Please remove this line.';
	    		   		dialog.alert({
	    	    			title: 'Package already received.',
	    	    			message: message
	    	    		});
	    		   		
	    		   		return false;
	    			}
	    		}
	    	}
	    	
	    	
	    	//Checks if the customer is a service
	    	var customerID = packageEntry.getValue("custpage_clgx2_package_company");
	    	console.log(customerID);
	    	
	    	
	    	if(customerID && customerID != -1) {
	    		var customerStatusLookup = search.lookupFields({ type: "customer", id: customerID, columns: ["status"] });
	    		
	    		if(customerStatusLookup.status[0].text.indexOf("SERVICE-") != -1) {
	    			dialog.alert({
	    				title: 'Incorrect Customer Type',
	    				message: 'Please select a customer and not a service.'
	    			});
	    			return false;
	    		}
	    	}
	    	
	    	return true;
    }

    return {
    	pageInit: pageInit,
        saveRecord: saveRecord
    };
    
});
