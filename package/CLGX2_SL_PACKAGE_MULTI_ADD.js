/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/redirect', 'N/ui/serverWidget', 'N/search', '/SuiteScripts/clgx/libraries/moment.min'],
/**
 * @param {record} record
 * @param {redirect} redirect
 * @param {serverWidget} serverWidget
 */
function(record, redirect, serverWidget, search, moment) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
		try{
			if(context.request.method == "GET") {
        		var form = serverWidget.createForm({ title: "Enter Tracking Numbers" });
        		form.clientScriptModulePath = 'SuiteScripts/clgx/package/CLGX2_CS_PACKAGE_MULTI_ADD';
        		
    		   	var request = context.request;
        		var trackingNum = request.parameters.tn;
        		
        		//build form
        		
        		var company = form.addField({
        			id: 'custpage_clgx2_package_company',
        			type: serverWidget.FieldType.SELECT,
        			source: 'customer',
        			label: 'Customer'
        		});
        		
        		var storageFacility = form.addField({
        			id: 'custpage_clgx2_package_storage_facility',
        			type: serverWidget.FieldType.SELECT,
        			source: 'customrecord_cologix_facility',
        			label: 'Storage Facility'
        		});
        		var internalNotes = form.addField({
        			id: 'custpage_clgx2_package_internal_notes',
        			type: serverWidget.FieldType.TEXTAREA,
        			label: 'Internal Notes'
        		});
        		var carrier = form.addField({
        			id: 'custpage_clgx2_package_carrier',
        			type: serverWidget.FieldType.SELECT,
        			source: 'customlist_clgx_pkg_shipment_carrier', 
        			label: 'Carrier'
        		});
        		
        		var attn = form.addField({
        			id: 'custpage_clgx2_package_attention',
        			type: serverWidget.FieldType.TEXT,
        			label: 'Package Attention'
        		});
        		
        		
            	company.isMandatory = true;
            	storageFacility.isMandatory = true;
            	carrier.isMandatory = true;
            	attn.isMandatory = true;
        		
            	//build sublist
        		
            	var trackingNumbers = form.addSublist({ 
            		id: "custpage_clgx2_track_numbers", 
            		type: serverWidget.SublistType.INLINEEDITOR, 
            		label: "Tracking Numbers"
            	});
            	
            	
            	var packageType = trackingNumbers.addField({ 
            		id: "custpage_clgx2_package_type", 
            		type: serverWidget.FieldType.SELECT,
            		source: 'customlist_clgx_pkg_package_type',
            		label: "Package Type"
            	});
            	
            	var trackingNumber = trackingNumbers.addField({ 
            		id: "custpage_clgx2_track_number", 
            		type: serverWidget.FieldType.TEXT, 
            		label: "Tracking Number"
            	});	
            	
            	trackingNumber.isMandatory = true;
            	packageType.isMandatory = true;
            	
            	//populate initial sublist value with url parameter tracking number
        		
            	trackingNumbers.setSublistValue({
            		id: 'custpage_clgx2_track_number',
            		line: 0,
            		value: trackingNum
            	});
            	
            	
            	
        		form.addSubmitButton({label:'Receive'});
        		context.response.writePage(form);
			} else {	
				var request = context.request;
		    	var today = moment().format('M/D/YYYY');
				
				var custId = request.parameters.custpage_clgx2_package_company;
				var facilityId = request.parameters.custpage_clgx2_package_storage_facility;
				var internalNotesContent = request.parameters.custpage_clgx2_package_internal_notes;
				var packageCarrier = request.parameters.custpage_clgx2_package_carrier;
				var attention = request.parameters.custpage_clgx2_package_attention;
				var lineCount = request.getLineCount({ group: 'custpage_clgx2_track_numbers'});
				
				var trackingFilter = new Array();
				
				for(var i = 0; i<lineCount; i++){
					var trackingNumValue = request.getSublistValue({
						group: 'custpage_clgx2_track_numbers',
						name: 'custpage_clgx2_track_number',
						line: i
					});
					
					trackingFilter.push(trackingNumValue);
					
					var existingPackage = searchForPackage(trackingNumValue);
					
					if(existingPackage){
						//if there is an existing package with tracking number in the system, then load that package record and update it
						var objRecord = record.load({
						    type: 'customrecord_clgx_package', 
						    id: existingPackage,
						    isDynamic: true
						});
						objRecord.setValue('custrecord_clgx_pkg_delivery_status', 2);
//						objRecord.setValue('custrecord_clgx_pkg_delivery_date', today);
						objRecord.save();
					} else {	
						//if no existing package is found, create the package record and set the default fields
						var packageTypeValue = request.getSublistValue({
							group: 'custpage_clgx2_track_numbers',
							name: 'custpage_clgx2_package_type',
							line: i
						});
						
						var newPackage = record.create({
							type: 'customrecord_clgx_package',
							isDynamic: true
						});
						
//						log.debug(today);
						
						newPackage.setValue('custrecord_clgx_pkg_receiving_company', custId);
						newPackage.setValue('custrecord_clgx_pkg_storage_facility', facilityId);
						newPackage.setValue('custrecord_clgx_pkg_storage_notes', internalNotesContent);
						newPackage.setValue('custrecord_clgx_pkg_shipping_carrier', packageCarrier);
						newPackage.setValue('custrecord_clgx_pkg_tracking_num', trackingNumValue);
						newPackage.setValue('custrecord_clgx_pkg_package_type', packageTypeValue);
						newPackage.setValue('custrecord_clgx_pkg_attention', attention);
//						newPackage.setValue('custrecord_clgx_pkg_delivery_date', today);
						
						newPackage.save();
//						log.debug(newPackage.id);
					}
				}
				
//				log.debug({title: 'Tracking Numbers Array', details: trackingFilter});
//				
//				var filter = search.createFilter({
//					name: 'custrecord_clgx_pkg_tracking_num',
//					operator: search.Operator.CONTAINS,
//					values: trackingFilter
//				});
//				
//				var afterProcessRedirect = search.create({
//				    id: 'customsearch_test',
//				    type: 'customrecord_clgx_package',
//				    title: 'Packages Received',
//				    columns: [{name:  'internalid'}],
//				    filters: filter
//				});
//				
//				redirect.toSearch({
//					search: afterProcessRedirect
//				});
				redirect.redirect({
					url: '/app/site/hosting/scriptlet.nl?script=1902&deploy=1',
					parameters: {custom_packfilt : facilityId}
				})
				
				
			}
		} catch(ex){
    		context.response.write(JSON.stringify(ex));
    	}
    }
    
    function searchForPackage(trackingSearch){
   		
    	var searchFilter = search.createFilter({
    		name: 'custrecord_clgx_pkg_tracking_num',
    		operator: search.Operator.IS,
    		values: trackingSearch    		
    	});
    	
    	var packageRecord = '';
		var searchObject = search.create({ type: "customrecord_clgx_package", columns: ['custrecord_clgx_pkg_tracking_num', 'internalid'], filters: searchFilter});
		searchObject.run().each(function(result) {
			packageRecord = result.getValue('internalid');
			return true;
		});
		
		return packageRecord;
    	
    }

    return {
        onRequest: onRequest
    };
    
});
