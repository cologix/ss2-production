/**
 * @NApiVersion 2.x
 * @NScriptType MassUpdateScript
 * @NModuleScope SameAccount
 */
define(["N/record"],
function(record) {
    function each(params) {
    	try {
    		var recordObject = record.load({ type: params.type, id: params.id });
    		recordObject.save({ enableSourcing: true, ignoreMandatoryFields: true });
    	} catch(ex) {
    		log.error({ title: "each - Error", details: ex });
    	}
    }

    return {
        each: each
    };
    
});
